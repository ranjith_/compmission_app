﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/BAMaster.master" AutoEventWireup="true" CodeFile="TestGrid.aspx.cs" Inherits="Masters_TestGrid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="col-lg-12">
        <!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Basic datatable</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
                                    <li><asp:Label ID="label_RequestTypeCode"  Font-Bold="true" runat="server"></asp:Label>  &nbsp&nbsp <a id="a_RequestTypeListAdd" runat="server"><i class="icon-pencil6 text-size-small position-left"></i></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							The <code>DataTables</code> is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function. Searching, ordering, paging etc goodness will be immediately added to the table, as shown in this example. <strong>Datatables support all available table styling.</strong>
						</div>

						<table class="table datatable-basic">
							<thead>
								<tr>
									<th>First Name</th>
									<th>Last Name</th>									
								</tr>
							</thead>
							<tbody>
								<asp:PlaceHolder ID="ph_TestGrid" runat="server"></asp:PlaceHolder>							
							</tbody>
						</table>
					</div>
					<!-- /basic datatable -->
    </div>
</asp:Content>

