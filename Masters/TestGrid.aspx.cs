﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Masters_TestGrid : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
       // this.SC = new SessionCustom(true);
        this.PHT = new PhTemplate();

        RMaster RM = new RMaster(Master);
        //RM.SetAddLink("Add Role", "OpenPopUp('Add Role','Role','','','')", RMaster.LinkType.OnClick);
        a_RequestTypeListAdd.Attributes.Add("onclick", "OpenPopUp('Add Test Grid','TestGrid','','','');");
        string str_Sql = "select * from TestGrid";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_TestGrid, "TestGridListTr.txt", "");
    }
}