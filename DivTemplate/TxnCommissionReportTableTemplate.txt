﻿
<table class="table table-lg CommissionTransaction">
	<thead>
		<tr>
			<th>Txn</th>
			<th>Date</th>
			<th>Type</th>
			<th>Customer</th>
                                   
			<th>Product</th>
			<th>Sales Amount</th>
			<th>Commission Rate</th>
			<th>Commission Amount</th>
		</tr>
	</thead>
	<tbody>
			%%tbody%%
	</tbody>

	</table>
