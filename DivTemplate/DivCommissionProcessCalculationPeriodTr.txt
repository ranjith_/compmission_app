﻿<tr>
    <td>%%PeriodSeq%% </td>
    <td>%%plsDate%% - %%pleDate%% </td>
    <td>%%psDate%% - %%peDate%%  </td>
 
    <td><span class="%%LineThrough%%">%%ScheduleToRunOn%%</span></td>
	   <td><span class="badge bg-success-300">%%StatusType%%</span></td>
    <td>%%CalcProcessedOn%%</td>
	<td>
		<div class="form-check form-check-left form-check-switchery form-check-switchery-sm ml-2 mt-1 d-inline"> 
				<label class="form-check-label font-weight-bold "> 
							<input id="chb_IsRunManual-%%PeriodSeq%%" %%ManualRunChecked%%  type="checkbox" onclick="IsRunManual('#chb_IsRunManual-%%PeriodSeq%%','#btn_RunManual-%%PeriodSeq%%')" runat="server" class="form-input-switchery" />
				</label>
		</div>

		<button id="btn_RunManual-%%PeriodSeq%%" type="button" onclick="CalculateCommission(this,'%%MasterPeriodKey%%','%%CommissionKey%%','#Cog-Spinner-%%PeriodSeq%%','%%ScheduledPeriodKey%%');" class="btn btn-success bg-transparent text-success btn-sm btn-xx  %%ShowRunButton%% "><i class="icon-play3 f-1-5em"></i></button>
		<a target="_blank" class="%%ViewCalc%% text-success ml-2 mr-2" href="../Web/CommissionCalculationStatementReport.aspx?CommissionKey=%%CommissionKey%%&PeriodKey=%%MasterPeriodKey%%"><i class="icon-eye f-1-5em"></i></a>
		
		<span class="text-success d-none" id="Cog-Spinner-%%PeriodSeq%%"><i class="icon-spinner6 spinner "></i></span>
	</td>
	
</tr>

