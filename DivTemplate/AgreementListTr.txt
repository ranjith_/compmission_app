﻿<tr>
	<td><a href="../Web/Agreement.aspx?agreement_key=%%AgreementKey%%&isEdit=1">%%AgreementName%%</a></td>
    <td>%%IsDefault%%</td>
	<td>%%CDate%%</td>
	<td>%%UserName%%</td>
	<td class="text-center">
        <div class="list-icons">
            <div class="dropdown">
                <a href="#" class="list-icons-item" data-toggle="dropdown">
                    <i class="icon-menu9"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                                                            
                    <button type="button" onclick="deleteAgreement('%%AgreementKey%%','%%AgreementName%%')" class="dropdown-item"><i class="icon-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </td>
</tr>

