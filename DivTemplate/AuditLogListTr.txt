﻿<tr>
	<!--<td><a href="../Web/AuditlogConfig.aspx?audit_key=%%AuditKey%%">%%AuditName%%</a></td> -->
	<td> <button type="button" onclick="viewAuditLog('%%AuditKey%%')" class="btn btn-link">%%AuditName%%</button></td>
    <td>%%TableName%%</td>
	<td>%%Created_On%%</td>
	<td>%%CreatedByName%% </td>
	
	<td class="text-center">
        <div class="list-icons">
            <div class="dropdown">
                <a href="#" class="list-icons-item" data-toggle="dropdown">
                    <i class="icon-menu9"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                                                            
                    <button type="button" onclick="deleteAuditlog('%%AuditKey%%','%%AuditName%%')" class="dropdown-item"><i class="icon-trash"></i> Delete</button>
                </div>
            </div>
        </div>
    </td>
</tr>

