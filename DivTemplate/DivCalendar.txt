﻿<div class="card-header header-elements-inline ">
<h5 class="card-title">Currency</h5>
<div class="header-elements">
<div class="list-icons">
<a href="../Web/Calender.aspx?calender_key=%%CalenderKey%%&isEdit=1" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> Edit</a>
<a class="list-icons-item" data-action="collapse"></a>
  
</div>
</div>
</div>
   
<div class="card-body">
<div class="row">
 
    
    <div class="col-md-6 mb-3 ">
            <label>Calendar Name</label>
            <br>
            <label><b>%%CalendarName%%</b></label>
    </div>
    <div class="col-md-6 mb-3 ">
            <label> Fascal Year  </label>
            <br>
            <label><b>%%FascalYear%%</b></label>
    </div>
    <div class="col-md-6 mb-3 ">
            <label>Start Date </label>
            <br>
            <label><b>%%SDate%%</b></label>
    </div>
	 <div class="col-md-6 mb-3 ">
            <label> End Date   </label>
            <br>
            <label><b>%%EDate%%</b></label>
    </div>
    <div class="col-md-6 mb-3 ">
            <label>Pay Frequency  </label>
            <br>
            <label><b>%%Frequency%%</b></label>
    </div>
	 
    <div class="col-md-6 mb-3 ">
            <label> Description  </label>
            <br>
            <label><b>%%Description%%</b></label>
    </div>
    <div class="col-md-6 mb-3 ">
            <label>Created On    </label>
            <br>
            <label><b>%%Created_On%%</b></label>
    </div>
			
</div>



</div>

