﻿<div class="card-header header-elements-inline ">
<h5 class="card-title">Menu</h5>
<div class="header-elements">
<div class="list-icons">
<a href="../Web/MenuList.aspx?menu_key=%%MenuKey%%&isEdit=1" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> Edit</a>
<a class="list-icons-item" data-action="collapse"></a>
                                       
                             
</div>
</div>
</div>
    
	

<div class="card-body">
<div class="row">
 <div class="col-md-6 mb-3 ">
 <label>Menu Name</label>
<br>
    <label><b>%%MenuName%%</b></label>
</div>
    
 <div class="col-md-6 mb-3 ">
    <label> Menu Description </label>
    <br>
    <label><b>%%MenuDescription%%</b></label>
</div>
 <div class="col-md-6 mb-3 ">
     <label>Parent Menu </label>
    <br>
    <label><b>%%ParentMenuName%%</b></label>
</div>
 <div class="col-md-6 mb-3 ">
   <label>Menu Type </label>
   <br>
    <label><b>%%MenuType%%</b></label>
</div>
 <div class="col-md-6 mb-3 ">
    <label>Target Url</label>
    <br>
    <label><b>%%TargetUrl%%</b></label>
</div>
 
    <div class="col-md-6 mb-3 ">
    <label> IsActive </label>
    <br>
    <label><b>%%IsActive%%</b></label>
</div>
 <div class="col-md-6 mb-3 ">
     <label>IsNewWindow </label>
    <br>
    <label><b>%%IsNewWindow%%</b></label>
</div>
 <div class="col-md-6 mb-3 ">
   <label>MenuOrder </label>
   <br>
    <label><b>%%MenuOrder%%</b></label>
</div>
 <div class="col-md-6 mb-3 ">
    <label> MenuIcon</label>
    <br>
    <label><b>%%MenuIcon%%</b></label>
</div>

                                            
</div>

</div> 
</div>