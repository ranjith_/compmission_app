﻿
     <tr class="split-%%ID%% split-condition">
            <td>%%ID%%</td>                 
            <td colspan="2">
                <select class="form-control attribute">
                    <option value="">-select one-</option>
					%%TransactionOption%%
                </select>
            </td>
            <td>
                <select onchange="changeSplitType(this,%%ID%%)" class="form-control attribute-type">
                    <option value="">-select type-</option>
                    <option value="1">From Variable</option>
                    <option value="2">From Fileds</option>
                </select>
            </td>
            <td class="td_Variable-%%ID%%">
			<div class="Div_Variable-%%ID%%">

                <input type="text" class="form-control variable-name " />
				</div>
            </td>
            
            <td classs="td_Field-%%ID%%">
			<div class="d-flex Div_Field-%%ID%% ">
			<input type="number" max="100" placeholder="%" class="form-control w-25 percentage number " />
                <select class="form-control transaction-field">
                    <option value="">-select Field-</option>
                   %%TransactionOption%%
                </select>
			</div>
			
            </td>
            <td class="tr-btn">
                <div class="d-inline-block add-recipient">
                   
                    <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx  " onclick="removeSplitCondition(this)"><i class="icon-x"></i></button>             
					 <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addSplitCondition(this,'split-%%ID%%',%%ID%%)"><i class="icon-plus22"></i></button>
                </div>
            </td>
        </tr>