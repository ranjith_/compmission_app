﻿<div class="card-header header-elements-inline">
<h5 class="card-title">Adjustment</h5>
<div class="header-elements">
<div class="list-icons">
<a href="../Web/AuditLogConfig.aspx?audit_key=%%AuditKey%%&isEdit=1" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> Edit</a>
<a class="list-icons-item" data-action="collapse"></a>
                                       
                                        
</div>
</div>
</div>
    
<div class="card-body">
<div class="row">
 
    <div class="col-md-6 mb-3 ">
            <label>Audit Name </label>
            <br>
            <label><b>%%AuditName%%</b></label>
    </div>
    <div class="col-md-6 mb-3 ">
            <label>Table Name </label>
            <br>
            <label><b>%%TableName%%</b></label>
    </div>
    <div class="col-md-6 mb-3 ">
            <label>Insert</label>
            <br>
            <label><b>%%LogInsert%%</b></label>
    </div>
    <div class="col-md-6 mb-3 ">
            <label>Update  </label>
            <br>
            <label><b>%%LogUpdate%%</b></label>
    </div>
    <div class="col-md-6 mb-3 ">
            <label>Delete  </label>
            <br>
            <label><b>%%LogDelete%%</b></label>
    </div>
    



				
</div>





