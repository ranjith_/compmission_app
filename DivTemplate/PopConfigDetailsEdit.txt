﻿<input type="hidden" id="hdn_ConfigDetailKey" name="txt_ConfigDetailKey" value="%%ConfigDetailKey%%"/>
<input type="hidden" id="hdn_ConfigKey" name="txt_ConfigKey" value="%%ConfigKey%%"/>


<div class="container">
 <div class="row">
    <div class="form-group col-md-6">
        <label>Name <span class="text-danger">*</span></label>
        <input ID="txt_Name" name="txt_Name" value="%%Name%%"  runat="server"  type="text" class="form-control" >
                                                
    </div>
                                            
    <div class="form-group col-md-6">
            <label> Value <span class="text-danger">*</span></label>
            <input id="txt_Value" name="txt_Value" value="%%Value%%"  runat="server"  type="text" class="form-control typeahead-basic-type" >
    </div>
    <div class="form-group col-md-6">
        <label>Sequence<span class="text-danger">*</span></label>
        <input id="txt_Seq" name="txt_Seq" value="%%Seq%%"  runat="server"  type="text" class="form-control" >
    </div>
    <div class="form-group col-md-6">
        <label>TextOne<span class="text-danger">*</span></label>
        <input id="txt_TextOne" name="txt_TextOne" value="%%TextOne%%"  runat="server"   type="text" class="form-control" >
    </div>
	<div class="form-group col-md-6">
		<label>TextTwo<span class="text-danger">*</span></label>
		<input id="txt_TextTwo" name="txt_TextTwo" value="%%TextTwo%%"  runat="server"   type="text" class="form-control" >
    </div>   
	<div class="form-group col-md-6">
		<label>TextThree<span class="text-danger">*</span></label>
		<input id="txt_TextThree" name="txt_TextThree" value="%%TextThree%%"  runat="server"   type="text" class="form-control" >
    </div>   
</div>
</div>