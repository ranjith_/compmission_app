﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionCalculationStatementReport.aspx.cs" Inherits="Web_CommissionCalculationStatementReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commisiion calculation Report</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
      <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12 col-md-12">
        
        <div class="card border-top-3 border-top-success-400">
            <div class="card-header header-elements-inline">
            <h5 class="card-title">Commission calculation Report
                
            </h5>
           
            </div>
    
            <div class="card-body">
                 <div class="row">
                     <div class="col-md-5">
                          <span class="text-muted">Commission Name </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_CommissionName" runat="server">Commission Calcualtion Frequence Monthly </h5>
                     </div>
                     <div class="col-md-3">
                         <span class="text-muted">Calcualtion Frequence </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_CalcFrqu" runat="server">Monthly </h5>
                     </div>
                     <div class="col-md-2">
                         <span class="text-muted">Start Date  </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_sDate" runat="server">1/1/2019</h5>
                     </div>
                     <div class="col-md-2">
                         <span class="text-muted">End Date  </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_edate" runat="server">12/31/2019</h5>
                     </div>
                    

                 </div>
                <div class="row">
                  
                    
                     <div class="col-md-6">
                         <span class="text-muted">Plan Frequence </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_PlanFrequency" runat="server"> </h5>
                     </div>
                     <div class="col-md-6">
                         <span class="text-muted">Process Frequency  </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_ProcessFrequency" runat="server"></h5>
                     </div>
                     
                    <div class="col-md-12">
                        <hr />
                    </div>

                 </div>
                    
                <div class="table-responsive">
                    <table class="table datatable-basic">
                        <thead>
                            <tr>
                                <th>Rep Id</th>
                                <th>Rep Name</th>
                                <th>Transaction ID</th>
                                <th>Product ID</th>
                                <th>Sales Amount</th>
                                <th>Commission Rate</th>
                                <th>Commission Amount</th>
                                <th>Calculated On</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:PlaceHolder ID="ph_CalcReport" runat="server"></asp:PlaceHolder>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
          
    </div>
          </div>
    

</asp:Content>

