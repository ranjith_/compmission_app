﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="ImportMaster.aspx.cs" Inherits="Web_ImportMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Import Master</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
<div class="container-fluid">
    <div class="row">
    <!--Data List-->
        <div class="col-lg-12 col-md-12 ">
            <!-- Basic datatable -->
            <div class="card border-top-3 border-top-primary-400">
                <div class="card-header header-elements-inline">
                <h5 class="panel-title">Imports Type
                                   
                </h5>
                    <div class="header-elements">
                        <div class="list-icons">
                                       
                                        
                            <a class="list-icons-item" data-action="collapse"></a>
                            
                        </div>
                    </div>
                </div>
    
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label>Source Table</label>
                        <asp:DropDownList ID="ddl_SourceTable" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Type Name</label>
                        <input type="text" class="form-control" id="txt_TypeName" runat="server" />
                    </div>
                     <div class="form-group col-md-4 my-auto">
                         <button type="button" class="btn btn-primary btn-sm">Generate Type</button>
                     </div>
                </div>
            </div>
            </div>
            
        </div>
                                
                            
        </div>
    </div>
</asp:Content>

