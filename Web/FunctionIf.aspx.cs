﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_FunctionIf : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        if (!IsPostBack)
        {
            string ConditionKey = Guid.NewGuid().ToString();
            hdn_IfKey.Value = ConditionKey;
            loadSourceOrderDropList();
            loadOperator();

            if(Com.GetQueryStringValue("isEdit")=="1" && Com.GetQueryStringValue("ConditionKey") != "")
            {
                hdn_IfKey.Value = Com.GetQueryStringValue("ConditionKey");
                loadConditionToEdit(Com.GetQueryStringValue("ConditionKey"));
                Btn_SaveIfCondition.Visible = false;
                btn_UpdateIfCOndition.Visible = true;
            }
        }
    }

    private void loadConditionToEdit(string ConditionKey)
    {
        string str_sql = "select * from IfCondition where ConditionKey=@ConditionKey";
        SqlCommand sqlcmd = new SqlCommand(str_sql);
        sqlcmd.Parameters.AddWithValue("@ConditionKey", ConditionKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);

        foreach (DataRow dr in dt.Rows)
        {
            txt_value.Value = dr["Value"].ToString();
            txt_ThenCondition.Value = dr["ThenStatement"].ToString();
            txt_ElseCondition.Value = dr["ElseStatement"].ToString();
            ddl_SourceTable.SelectedValue = dr["SourceField"].ToString();
            ddl_Operator.SelectedValue = dr["Operator"].ToString();
          ddl_SourceField.SelectedValue = dr["SourceFieldValue"].ToString();
            //IfCondition (ConditionKey, TableName, TableField, Operator, Value, ThenStatement, ElseStatement, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
            ddl_ValueType.SelectedValue= dr["ValueType"].ToString();


        }
    }

    private void loadOperator()
    {
        string str_Select = "select * from v_Operators";
        SqlCommand cmm = new SqlCommand(str_Select);

        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_Operator, ds.Tables[0], "Text", "Value", true, "--Select Operator--");
    }

    private void loadSourceOrderDropList()
    {
        string str_Select = "select *, Text as Value from v_CommissionRuleTableName";
        SqlCommand cmm = new SqlCommand(str_Select);

        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_SourceTable, ds.Tables[0], "Text", "Text", true, "--Select Source--");

        this.Com.LoadDropDown(ddl_SourceField, ds.Tables[0], "Text", "Text", true, "--Select Source--");
    }

    public string getHtmlOptions(string qry, string value)
    {
        DataAccess DA = new DataAccess();
        // string str_Sql = "select a.AgreementName as Text, a.AgreementKey as Value from Agreement a";
        SqlCommand sqlcmd = new SqlCommand(qry);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string option = "";
        foreach (DataRow dr in dt.Rows)
        {
            string selected = "";
            if (dr["Value"].ToString() == value)
            {
                selected = "selected='selected'";
            }
            option += "<option value='" + dr["Value"].ToString() + "' " + selected + ">" + dr["Text"].ToString() + "</option>";
        }
        return option;
    }

    protected void Btn_SaveIfCondition_ServerClick(object sender, EventArgs e)
    {

        //IfCondition (ConditionKey, TableName, TableField, Operator, Value, ThenStatement, ElseStatement, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "insert into IfCondition (ConditionKey, SourceField, SourceFieldValue, Operator, Value,ValueType, ThenStatement, ElseStatement,CreatedOn, CreatedBy)"
                               + "select @ConditionKey, @SourceField, @SourceFieldValue, @Operator, @Value,@ValueType, @ThenStatement, @ElseStatement, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@ConditionKey", hdn_IfKey.Value);
        cmd.Parameters.AddWithValue("@SourceField", ddl_SourceTable.SelectedValue);
        cmd.Parameters.AddWithValue("@SourceFieldValue", ddl_SourceField.SelectedValue);
        cmd.Parameters.AddWithValue("@Operator", ddl_Operator.SelectedValue);
        cmd.Parameters.AddWithValue("@Value", txt_value.Value);
        cmd.Parameters.AddWithValue("@ValueType", ddl_ValueType.SelectedValue);
        cmd.Parameters.AddWithValue("@ThenStatement", txt_ThenCondition.Value);
        cmd.Parameters.AddWithValue("@ElseStatement", txt_ElseCondition.Value);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);
    }


    protected void btn_UpdateIfCOndition_ServerClick(object sender, EventArgs e)
    {
        //IfCondition (ConditionKey, TableName, TableField, Operator, Value, ThenStatement, ElseStatement, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "Update  IfCondition set  SourceField=@SourceField, SourceFieldValue=@SourceFieldValue, Operator=@Operator, Value=@Value,ValueType=@ValueType, ThenStatement=@ThenStatement," +
            " ElseStatement=@ElseStatement,  ,ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where ConditionKey=@ConditionKey";
                              
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@ConditionKey", hdn_IfKey.Value);
        cmd.Parameters.AddWithValue("@SourceField", ddl_SourceTable.SelectedValue);
        cmd.Parameters.AddWithValue("@SourceFieldValue", ddl_SourceField.SelectedValue);
        cmd.Parameters.AddWithValue("@Operator", ddl_Operator.SelectedValue);
        cmd.Parameters.AddWithValue("@Value", txt_value.Value);
        cmd.Parameters.AddWithValue("@ValueType", ddl_ValueType.SelectedValue);
        cmd.Parameters.AddWithValue("@ThenStatement", txt_ThenCondition.Value);
        cmd.Parameters.AddWithValue("@ElseStatement", txt_ElseCondition.Value);
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);
    }
}