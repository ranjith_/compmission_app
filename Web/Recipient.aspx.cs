﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Recipients : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        if (Request.QueryString["IsNew"] == "1")
        {
            CheckForCustomRecipients("");

        }
        else
        {
            if (panel_CustomRecipient.Controls.Count == 0)
            {
                CheckForCustomRecipients(Request.QueryString["RecipientKey"]);
            }

        }

        if (!IsPostBack)
        {
            loadTerritoryDdl();
            loadManagerDdl();
            loadJobCategoryDdl();
            loadRecipientType();
            if (Request.QueryString["RecipientKey"] != null && Request.QueryString["IsEdit"] == "1")
            {
                loadRecipDetailToEdit(Request.QueryString["RecipientKey"]);
                btn_Save.Visible = false;
                btn_Update.Visible = true;
                
            }


        }
    }

    private void CheckForCustomRecipients(string RecipientKey)
    {

        try
        {
            string str_Sql = "select a.*, b.Name from TableCustomConfig a join IConfigDetails b on a.DataType = b.ConfigDetailKey where a.PrimaryTableName='Recipient' order by Seq;" +
          "select * from RecipientCustom where RecipientKey=@RecipientKey ";
            SqlCommand cmd = new SqlCommand(str_Sql);
            if (RecipientKey == "")
            {
                cmd.Parameters.AddWithValue("@RecipientKey", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@RecipientKey", RecipientKey);
            }

            DataSet ds = this.DA.GetDataSet(cmd);

            if (ds.Tables[0].Rows.Count > 0)
            {
                LoadCustomTransaction(ds);
            }
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

       
    }
    private void LoadCustomTransaction(DataSet ds)
    {
        try
        {

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string Value = "";
                foreach (DataRow dr1 in ds.Tables[1].Rows)
                {
                    foreach (DataColumn cc in ds.Tables[1].Columns)
                    {
                        if (dr["ColumnName"].ToString() == cc.ColumnName.ToString())
                        {
                            Value = dr1[cc.ColumnName.ToString()].ToString();
                        }
                    }
                }


                if (dr["FieldType"].ToString() == "1")
                {
                    AddTextBox(dr["ColumnName"].ToString(), dr["LableName"].ToString(), Value, dr["IsRequired"].ToString());
                }
                else if (dr["FieldType"].ToString() == "2")
                {
                    AddDropDown(dr["ColumnName"].ToString(), dr["DdlKey"].ToString(), dr["LableName"].ToString(), Value, dr["IsRequired"].ToString());
                }

            }
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

    }

    private void AddDropDown(string ColumnName, string DdlKey, string LabelName, string value, string IsRequired)
    {
        string FormGroupDivStart = "<div class='form-group col-md-3'>";
        string FormGroupDivEnd = "</div>";
        string RequiredField = "<span class='text-danger'>*</span>";
        Label lbl = new Label();
        lbl.Text = LabelName;
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl_" + ColumnName;
        ddl.CssClass = "form-control ";
        loadDropDown(ddl, DdlKey);
        panel_CustomRecipient.Controls.Add(new LiteralControl(FormGroupDivStart));
        panel_CustomRecipient.Controls.Add(lbl);
        if (IsRequired == "True")
        {
            panel_CustomRecipient.Controls.Add(new LiteralControl(RequiredField));
        }
        panel_CustomRecipient.Controls.Add(ddl);
        ddl.SelectedValue = value;
        panel_CustomRecipient.Controls.Add(new LiteralControl(FormGroupDivEnd));
    }

    private void loadDropDown(DropDownList ddl, string ddlKey)
    {
        string str_Select = "select Name as Text,configDetailKey as Value  from IconfigDetails where ConfigKey=@ConfigKey order by seq ";
        SqlCommand cmm = new SqlCommand(str_Select);
        cmm.Parameters.AddWithValue("@ConfigKey", ddlKey);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl, ds.Tables[0], "Text", "Value", true, "--Select --");
    }

    private void AddTextBox(string ColumnName, string LabelName, string Value, string IsRequired)
    {
        string FormGroupDivStart = "<div class='form-group col-md-3'>";
        string FormGroupDivEnd = "</div>";
        string RequiredField = "<span class='text-danger'>*</span>";
        Label lbl = new Label();
        lbl.Text = LabelName;
        TextBox tb = new TextBox();
        tb.ID = "txt_" + ColumnName;
        tb.Text = Value;
        tb.CssClass = "form-control ";
        panel_CustomRecipient.Controls.Add(new LiteralControl(FormGroupDivStart));
        panel_CustomRecipient.Controls.Add(lbl);
        if (IsRequired == "True")
        {
            panel_CustomRecipient.Controls.Add(new LiteralControl(RequiredField));
        }
        panel_CustomRecipient.Controls.Add(tb);
        panel_CustomRecipient.Controls.Add(new LiteralControl(FormGroupDivEnd));
    }

    private void loadJobCategoryDdl()
    {
        string str_Select =" select Text, Value from v_JobCategory order by Seq";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(dd_JobCataegory, ds.Tables[0], "Text", "Value", true, "--Select type--");
    }

    private void loadRecipientType()
    {
        string str_Select = "select  Value,  Text from v_RecipientCategory order by Seq";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_RecipientCategoryListItem, ds.Tables[0], "Text", "Value", true, "--Select type--");
    }

    private void loadManagerDdl()
    {
        string str_Select = "select Value,  Text from v_RecipientManager";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_RecipientManager, ds.Tables[0], "Text", "Value", true, "--Select Manager--");
    }

    private void loadTerritoryDdl()
    {
        string str_Select = "select   Value, Text from v_Territory";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_TerritoryListItem, ds.Tables[0], "Text", "Value", true, "--Select  Territory--");
    }

    private void loadRecipDetailToEdit(string recip_key)
    {
        //recipients (RecipientKey, RecipientId, UserId, IsLogin, FirstName, LastName, RecipientType, JobCategory, ReportingManager, RecipientTerritory, JoinDate, PlanStartDate,
        //UserKey, PlanEndDate, Active, Notes, SalaryAmount, PayOutCurrency, AdjustmentAmount, RecoveryAmount, GoalAmount, Address1, Address2, PrimaryPhoneNo, 
        //SecondaryPhoneNo, Email, Country, State, City, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)

        try
        {

            string str_Sql_view = "select * , FORMAT(JoinDate, 'yyyy-MM-dd') as jDate , FORMAT(PlanEndDate, 'yyyy-MM-dd') as peDate,  FORMAT(PlanStartDate, 'yyyy-MM-dd') as psDate  from Recipients where RecipientKey =@RecipientKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@RecipientKey", recip_key);
            DataTable dt = this.DA.GetDataTable(sqlcmd);
            foreach (DataRow dr in dt.Rows)
            {

                txt_RecipientId.Value = dr["RecipientId"].ToString();
                txt_UserId.Value = dr["UserId"].ToString();
                if (dr["IsLogin"].ToString() == "True")
                {
                    chk_IsLogin.Checked = true;
                }
                else
                {
                    chk_IsLogin.Checked = false;
                }

                hdn_RecipientId.Value = dr["RecipientId"].ToString();

                txt_FName.Value = dr["FirstName"].ToString();
                txt_LName.Value = dr["LastName"].ToString();
                ddl_RecipientCategoryListItem.SelectedValue = dr["RecipientType"].ToString();
                ddl_TerritoryListItem.SelectedValue = dr["RecipientTerritory"].ToString();
                dd_JobCataegory.SelectedValue = dr["JobCategory"].ToString();
                ddl_RecipientManager.SelectedValue = dr["ReportingManager"].ToString();
                dp_StartDate.Value = dr["psDAte"].ToString();
                dp_EndDate.Value = dr["peDate"].ToString();
                dp_JoinDate.Value = dr["jDate"].ToString();

                if (dr["Active"].ToString() == "True")
                {
                    chb_IsActive.Checked = true;
                }
                txt_Notes.Value = dr["Notes"].ToString();

                txt_SalaryAmount.Value = dr["SalaryAmount"].ToString();
                ddl_PayOutCurrency.SelectedValue = dr["PayOutCurrency"].ToString();
                txt_AdjustmentAmount.Value = dr["AdjustmentAmount"].ToString();
                txt_RecoveryAmount.Value = dr["RecoveryAmount"].ToString();
                txtGoalAmount.Value = dr["GoalAmount"].ToString();

                txt_Address1.Value = dr["Address1"].ToString();
                txt_Address2.Value = dr["Address2"].ToString();
                txt_PhonePrimary.Value = dr["PrimaryPhoneNo"].ToString();
                txt_PhoneSecondary.Value = dr["SecondaryPhoneNo"].ToString();
                ddl_Country.SelectedValue = dr["Country"].ToString();
                ddl_State.SelectedValue = dr["State"].ToString();
                ddl_city.SelectedValue = dr["City"].ToString();
                txt_ZipCode.Value = dr["ZipCode"].ToString();

                // SalaryAmount, PayOutCurrency, AdjustmentAmount, RecoveryAmount, GoalAmount, Address1, Address2, PrimaryPhoneNo, 
                //SecondaryPhoneNo, Email, Country, State, City, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)

            }
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

    }
   
    [WebMethod]
    public static string IsRepIdExist(string RepId)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();
          
            string str_Sql_view = "select * from Recipients where RecipientId =@RepId";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@RepId", RepId);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string result = "1";

            if (dt.Rows.Count > 0)
            {
                result = "0";
            }
            
            
            return result;
            
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
   


    protected void btn_Save_Click(object sender, EventArgs e)
    {
        //recipients (RecipientKey, RecipientId, UserId, IsLogin, FirstName, LastName, RecipientType, JobCategory, ReportingManager, RecipientTerritory, JoinDate, PlanStartDate,
        //UserKey, PlanEndDate, Active, Notes, SalaryAmount, PayOutCurrency, AdjustmentAmount, RecoveryAmount, GoalAmount, Address1, Address2, PrimaryPhoneNo, 
        //SecondaryPhoneNo, Email, Country, State, City, ZipCode, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)


        try
        {

            string str_Key = Guid.NewGuid().ToString();
            string Plan_Key = Guid.NewGuid().ToString();
            string RepCustomKey = Guid.NewGuid().ToString();
            string IsActive = "0", IsLogin = "0";
            //recipients (RecipientKey, RecipientId, RecipientType, RecipientTerritory, StartDate, UserKey, Employee, RecipientName, JobCategory, RecipientManager, EndDate, RecipientEligible, Notes, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)
            if (chb_IsActive.Checked == true) { IsActive = "1"; }
            if (chk_IsLogin.Checked == true) { IsLogin = "1"; }
            //RecipientPlan (PlanKey, RecipientKey, StartDate, EndDate, AdjustmentKey, AgreementKey, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy, SpecificTerms, CommissionKey)
            string str_Sql = "insert into  Recipients (RecipientKey, RecipientId, UserId, IsLogin, FirstName, LastName, RecipientType, JobCategory," +
                " ReportingManager, RecipientTerritory, JoinDate, PlanStartDate,  PlanEndDate, Active, Notes, SalaryAmount," +
                " PayOutCurrency, AdjustmentAmount, RecoveryAmount, GoalAmount, Address1, Address2, PrimaryPhoneNo, SecondaryPhoneNo, " +
                " Country, State, City,ZipCode, CreatedBy, CreatedOn)" +
                "select @RecipientKey, @RecipientId, @UserId, @IsLogin, @FirstName, @LastName, @RecipientType, @JobCategory, @ReportingManager, " +
                "@RecipientTerritory, @JoinDate, @PlanStartDate,  @PlanEndDate, @Active, @Notes, @SalaryAmount, @PayOutCurrency, @AdjustmentAmount," +
                " @RecoveryAmount, @GoalAmount, @Address1, @Address2, @PrimaryPhoneNo, @SecondaryPhoneNo, @Country, @State, @City, @ZipCode, @CreatedBy, @CreatedOn;" +
                 "insert into RecipientCustom (RepCustomKey,RecipientKey,CreatedBy,CreatedOn)" +
                                "select @RepCustomKey,@RecipientKey,@CreatedBy,@CreatedOn;" +
                                "insert into RecipientPlan (PlanKey, RecipientKey, StartDate, EndDate, CreatedOn, CreatedBy)" +
                                "select NEWID(),@RecipientKey,@PlanStartDate,  @PlanEndDate, @CreatedOn, @CreatedBy";

            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@RecipientKey", str_Key);
            cmd.Parameters.AddWithValue("@RepCustomKey", RepCustomKey);
            cmd.Parameters.AddWithValue("@RecipientId", txt_RecipientId.Value);
            cmd.Parameters.AddWithValue("@UserId", txt_UserId.Value);
            cmd.Parameters.AddWithValue("@IsLogin", IsLogin);
            cmd.Parameters.AddWithValue("@start_date", dp_StartDate.Value.ToString());
            cmd.Parameters.AddWithValue("@FirstName", txt_FName.Value);
            cmd.Parameters.AddWithValue("@LastName", txt_LName.Value);
            if (ddl_RecipientCategoryListItem.SelectedValue == "" || ddl_RecipientCategoryListItem.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@RecipientType", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@RecipientType", ddl_RecipientCategoryListItem.SelectedValue);
            }

            if (dd_JobCataegory.SelectedValue == "" || dd_JobCataegory.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@JobCategory", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@JobCategory", dd_JobCataegory.SelectedValue);
            }

            if (ddl_RecipientManager.SelectedValue == "" || ddl_RecipientManager.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@ReportingManager", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ReportingManager", ddl_RecipientManager.SelectedValue);
            }

            if (ddl_TerritoryListItem.SelectedValue == "" || ddl_TerritoryListItem.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@RecipientTerritory", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@RecipientTerritory", ddl_TerritoryListItem.SelectedValue);
            }

            if (dp_JoinDate.Value == "")
            {
                cmd.Parameters.AddWithValue("@JoinDate", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@JoinDate", dp_JoinDate.Value);
            }



            if (dp_StartDate.Value == "")
            {
                cmd.Parameters.AddWithValue("@PlanStartDate", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PlanStartDate", dp_StartDate.Value);
            }
            if (dp_EndDate.Value == "")
            {
                cmd.Parameters.AddWithValue("@PlanEndDate", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PlanEndDate", dp_EndDate.Value);
            }

            cmd.Parameters.AddWithValue("@Active", IsActive);
            cmd.Parameters.AddWithValue("@notes", txt_Notes.Value);

            //Compensation 
            cmd.Parameters.AddWithValue("@SalaryAmount", txt_SalaryAmount.Value);


            if (ddl_PayOutCurrency.SelectedValue == "" || ddl_PayOutCurrency.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@PayOutCurrency", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PayOutCurrency", ddl_PayOutCurrency.SelectedValue);
            }



            cmd.Parameters.AddWithValue("@AdjustmentAmount", txt_AdjustmentAmount.Value);
            cmd.Parameters.AddWithValue("@RecoveryAmount", txt_RecoveryAmount.Value);
            cmd.Parameters.AddWithValue("@GoalAmount", txtGoalAmount.Value);

            //contact Info
            cmd.Parameters.AddWithValue("@Address1", txt_Address1.Value);
            cmd.Parameters.AddWithValue("@Address2", txt_Address2.Value);
            cmd.Parameters.AddWithValue("@PrimaryPhoneNo", txt_PhonePrimary.Value);
            cmd.Parameters.AddWithValue("@SecondaryPhoneNo", txt_PhoneSecondary.Value);

            if (ddl_Country.SelectedValue == "" || ddl_Country.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@Country", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Country", ddl_Country.SelectedValue);
            }

            if (ddl_State.SelectedValue == "" || ddl_State.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@State", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@State", ddl_State.SelectedValue);
            }
            if (ddl_city.SelectedValue == "" || ddl_city.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@City", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@City", ddl_city.SelectedValue);
            }

            cmd.Parameters.AddWithValue("@ZipCode", txt_ZipCode.Value);
            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            this.DA.ExecuteNonQuery(cmd);

            //to insert the custom fields
            InsertCustomRep(str_Key);

            Response.Redirect("../Web/RecipientList.aspx");
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }


    }

    private void InsertCustomRep(string RecipientKey)
    {
        foreach (Control cc in panel_CustomRecipient.Controls)

        {
            if (cc is TextBox)
            {
                string ColumnName = cc.ID.ToString();
                TextBox txt = (TextBox)panel_CustomRecipient.FindControl(ColumnName);
                string Column = ColumnName.Replace("txt_", "");
                UpdateCustomRecipientFields(RecipientKey, Column, txt.Text.ToString());
            }
            else if (cc is DropDownList)
            {
                string ColumnName = cc.ID.ToString();
                DropDownList ddl = (DropDownList)panel_CustomRecipient.FindControl(ColumnName);
                string value = ddl.SelectedItem.Value.ToString();
                string Column = ColumnName.Replace("ddl_", "");
                UpdateCustomRecipientFields(RecipientKey, Column, value);
            }

        }
    }

    private void UpdateCustomRecipientFields(string RecipientKey, string ColumnName, string Value)
    {
        string str_sql = "update RecipientCustom set " + ColumnName + " =@" + ColumnName + " where RecipientKey=@RecipientKey";
        SqlCommand cmd = new SqlCommand(str_sql);
        if (Value == "")
        {
            cmd.Parameters.AddWithValue("@" + ColumnName, DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@" + ColumnName, Value);
        }

        cmd.Parameters.AddWithValue("@RecipientKey", RecipientKey);
        this.DA.ExecuteNonQuery(cmd);
    }
    protected void btn_Update_Click(object sender, EventArgs e)
    {

        try
        {
            string IsActive = "0", IsLogin = "0";

            string RecipientKey = Request.QueryString["RecipientKey"];
            if (chb_IsActive.Checked == true) { IsActive = "1"; }
            if (chk_IsLogin.Checked == true) { IsLogin = "1"; }

            string str_Sql = "Update Recipients set  RecipientId=@RecipientId, UserId=@UserId, IsLogin=@IsLogin, FirstName=@FirstName, LastName=@LastName, RecipientType=@RecipientType, JobCategory=@JobCategory," +
                " ReportingManager=@ReportingManager, RecipientTerritory=@RecipientTerritory, JoinDate=@JoinDate, PlanStartDate=@PlanStartDate,  PlanEndDate=@PlanEndDate, Active=@Active, Notes=@Notes, SalaryAmount=@SalaryAmount," +
                " PayOutCurrency=@PayOutCurrency, AdjustmentAmount=@AdjustmentAmount, RecoveryAmount=@RecoveryAmount, GoalAmount=@GoalAmount, Address1=@Address1, Address2=@Address2," +
                " PrimaryPhoneNo=@PrimaryPhoneNo, SecondaryPhoneNo=@SecondaryPhoneNo, " +
                " Country=@Country, State=@State, City=@City,ZipCode=@ZipCode, ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where RecipientKey=@RecipientKey";

            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@RecipientKey", RecipientKey);
            cmd.Parameters.AddWithValue("@RecipientId", txt_RecipientId.Value);
            cmd.Parameters.AddWithValue("@UserId", txt_UserId.Value);
            cmd.Parameters.AddWithValue("@IsLogin", IsLogin);
            cmd.Parameters.AddWithValue("@start_date", dp_StartDate.Value.ToString());
            cmd.Parameters.AddWithValue("@FirstName", txt_FName.Value);
            cmd.Parameters.AddWithValue("@LastName", txt_LName.Value);
            if (ddl_RecipientCategoryListItem.SelectedValue == "" || ddl_RecipientCategoryListItem.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@RecipientType", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@RecipientType", ddl_RecipientCategoryListItem.SelectedValue);
            }

            if (dd_JobCataegory.SelectedValue == "" || dd_JobCataegory.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@JobCategory", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@JobCategory", dd_JobCataegory.SelectedValue);
            }

            if (ddl_RecipientManager.SelectedValue == "" || ddl_RecipientManager.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@ReportingManager", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ReportingManager", ddl_RecipientManager.SelectedValue);
            }

            if (ddl_TerritoryListItem.SelectedValue == "" || ddl_TerritoryListItem.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@RecipientTerritory", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@RecipientTerritory", ddl_TerritoryListItem.SelectedValue);
            }



            if (dp_JoinDate.Value == "")
            {
                cmd.Parameters.AddWithValue("@JoinDate", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@JoinDate", dp_JoinDate.Value);
            }
            if (dp_StartDate.Value == "")
            {
                cmd.Parameters.AddWithValue("@PlanStartDate", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PlanStartDate", dp_StartDate.Value);
            }
            if (dp_EndDate.Value == "")
            {
                cmd.Parameters.AddWithValue("@PlanEndDate", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PlanEndDate", dp_EndDate.Value);
            }



            cmd.Parameters.AddWithValue("@Active", IsActive);
            cmd.Parameters.AddWithValue("@notes", txt_Notes.Value);

            //Compensation 
            cmd.Parameters.AddWithValue("@SalaryAmount", txt_SalaryAmount.Value);


            if (ddl_PayOutCurrency.SelectedValue == "" || ddl_PayOutCurrency.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@PayOutCurrency", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PayOutCurrency", ddl_PayOutCurrency.SelectedValue);
            }



            cmd.Parameters.AddWithValue("@AdjustmentAmount", txt_AdjustmentAmount.Value);
            cmd.Parameters.AddWithValue("@RecoveryAmount", txt_RecoveryAmount.Value);
            cmd.Parameters.AddWithValue("@GoalAmount", txtGoalAmount.Value);

            //contact Info
            cmd.Parameters.AddWithValue("@Address1", txt_Address1.Value);
            cmd.Parameters.AddWithValue("@Address2", txt_Address2.Value);
            cmd.Parameters.AddWithValue("@PrimaryPhoneNo", txt_PhonePrimary.Value);
            cmd.Parameters.AddWithValue("@SecondaryPhoneNo", txt_PhoneSecondary.Value);

            if (ddl_Country.SelectedValue == "" || ddl_Country.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@Country", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Country", ddl_Country.SelectedValue);
            }

            if (ddl_State.SelectedValue == "" || ddl_State.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@State", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@State", ddl_State.SelectedValue);
            }
            if (ddl_city.SelectedValue == "" || ddl_city.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@City", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@City", ddl_city.SelectedValue);
            }

            cmd.Parameters.AddWithValue("@ZipCode", txt_ZipCode.Value);
            cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            this.DA.ExecuteNonQuery(cmd);

            InsertCustomRep(RecipientKey);

            Response.Redirect("../Web/RecipientList.aspx");
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

    }

    protected void btn_Excel_ServerClick(object sender, EventArgs e)
    {
        string str_Sql = "select * from recipients ";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);

        DataTable dt = this.DA.GetDataTable(sqlcmd);

        DateTime time = DateTime.Now;
        string CurrentDate_Time = time.ToString("MM/dd/yyyy hh:mm:ss");
        this.OCF.ExporttoExcel(dt, "RecipientsExcel" + CurrentDate_Time + "");
    }


}