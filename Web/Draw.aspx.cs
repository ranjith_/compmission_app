﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Draw : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        if (!IsPostBack)
        {
            loadRecipientList();

            if (Com.GetQueryStringValue("DrawKey") != null && Com.GetQueryStringValue("DrawKey") != "" && Com.GetQueryStringValue("IsEdit") == "1")
            {
                loadDrawToUpdate(Com.GetQueryStringValue("DrawKey"));
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }

       
    }

    private void loadDrawToUpdate(string DrawKey)
    {
        string str_Select = "select *, format(EndDate, 'yyyy-MM-dd') as eDate,format(StartDate, 'yyyy-MM-dd') as sDate,format(RecEndDate, 'yyyy-MM-dd') as reDate,format(RecStartDate, 'yyyy-MM-dd') as rsDate from  Draw where DrawKey=@DrawKey";
        SqlCommand cmd = new SqlCommand(str_Select);
        cmd.Parameters.AddWithValue("@DrawKey", DrawKey);
        DataTable dt = this.DA.GetDataTable(cmd);

        if (dt.Rows.Count > 0)
        {
            foreach(DataRow dr in dt.Rows)
            {
                ddl_SalesRep.SelectedValue = dr["RecipientKey"].ToString();
                ddl_CompType.SelectedValue= dr["Type"].ToString();
                txt_ThresholdAmount.Value = dr["Amount"].ToString();
                dp_StartDate.Value = dr["sDate"].ToString();
                dp_EndDate.Value = dr["eDate"].ToString();
                dp_RecStartDate.Value = dr["rsDate"].ToString();
                dp_RecEndDate.Value = dr["reDate"].ToString();

                if (dr["IsRecoverable"].ToString() == "True")
                {
                    chb_IsRecoverable.Checked = true;
                }
                else
                {
                    chb_IsRecoverable.Checked = false;
                }
            }
        }

    }
    private void loadRecipientList()
    {
        string str_Select = "select RecipientKey as Value, Text from v_RecipientList ";
        SqlCommand cmm = new SqlCommand(str_Select);

        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_SalesRep, ds.Tables[0], "Text", "Value", true, "--Select Rep --");
       
    }
    protected void btn_Save_ServerClick(object sender, EventArgs e)
    {
        ///Draw (DrawKey, RecipientKey, Type, Amount, StartDate, EndDate, IsRecoverable, RecStartDate, RecEndDate, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string DrawKey = Guid.NewGuid().ToString();

        string str_Sql = "";


        if (ddl_CompType.SelectedValue == "1")
        {
            str_Sql = " insert into Draw (DrawKey, RecipientKey, Type, Amount, StartDate, EndDate, CreatedOn, CreatedBy)" +
                "select @DrawKey, @RecipientKey, @Type, @Amount, @StartDate, @EndDate, @CreatedOn, @CreatedBy";
        }
        else if (ddl_CompType.SelectedValue == "2")
        {
            str_Sql = " insert into Draw (DrawKey, RecipientKey, Type, Amount, StartDate, EndDate, IsRecoverable, RecStartDate, RecEndDate, CreatedOn, CreatedBy)" +
                "select @DrawKey, @RecipientKey, @Type, @Amount, @StartDate, @EndDate, @IsRecoverable, @RecStartDate, @RecEndDate, @CreatedOn, @CreatedBy";
        }
        SqlCommand cmd = new SqlCommand(str_Sql);


        cmd.Parameters.AddWithValue("@DrawKey", DrawKey);
        cmd.Parameters.AddWithValue("@RecipientKey", ddl_SalesRep.SelectedValue);
        cmd.Parameters.AddWithValue("@Type", ddl_CompType.SelectedValue);
        cmd.Parameters.AddWithValue("@Amount", txt_ThresholdAmount.Value);
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
        if (chb_IsRecoverable.Checked == true)
        {

            cmd.Parameters.AddWithValue("@IsRecoverable", 1);
            cmd.Parameters.AddWithValue("@RecStartDate", dp_RecStartDate.Value);
            cmd.Parameters.AddWithValue("@RecEndDate", dp_RecEndDate.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsRecoverable", 0);
            cmd.Parameters.AddWithValue("@RecStartDate", DBNull.Value);
            cmd.Parameters.AddWithValue("@RecEndDate", DBNull.Value);
        }
        
        
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        this.DA.ExecuteNonQuery(cmd);
        Response.Redirect("../Web/DrawList.aspx");
    }

    protected void btn_Update_ServerClick(object sender, EventArgs e)
    {
        string str_Sql = "";
        string DrawKey = Com.GetQueryStringValue("DrawKey");

        if (ddl_CompType.SelectedValue == "1")
        {
            str_Sql = " update Draw set  RecipientKey=@RecipientKey, Type=@Type, Amount=@Amount, StartDate=@StartDate, EndDate=@EndDate, ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where DrawKey=DrawKey";
        }
        else if (ddl_CompType.SelectedValue == "2")
        {
            str_Sql = " update Draw set RecipientKey=@RecipientKey, Type=@Type, Amount=@Amount, StartDate=@StartDate, EndDate=@EndDate, IsRecoverable=@IsRecoverable, RecStartDate=@RecStartDate, RecEndDate=@RecEndDate, ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where DrawKey=DrawKey";
        }
        SqlCommand cmd = new SqlCommand(str_Sql);


        cmd.Parameters.AddWithValue("@DrawKey", DrawKey);
        cmd.Parameters.AddWithValue("@RecipientKey", ddl_SalesRep.SelectedValue);
        cmd.Parameters.AddWithValue("@Type", ddl_CompType.SelectedValue);
        cmd.Parameters.AddWithValue("@Amount", txt_ThresholdAmount.Value);
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
        if (chb_IsRecoverable.Checked == true)
        {

            cmd.Parameters.AddWithValue("@IsRecoverable", 1);
            cmd.Parameters.AddWithValue("@RecStartDate", dp_RecStartDate.Value);
            cmd.Parameters.AddWithValue("@RecEndDate", dp_RecEndDate.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsRecoverable", 0);
            cmd.Parameters.AddWithValue("@RecStartDate", DBNull.Value);
            cmd.Parameters.AddWithValue("@RecEndDate", DBNull.Value);
        }


        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        this.DA.ExecuteNonQuery(cmd);
        Response.Redirect("../Web/DrawList.aspx");
    }
}