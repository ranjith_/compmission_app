﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="FunctionGoal.aspx.cs" Inherits="Web_FunctionGoal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <script>
        function UpdateGoal(RangeKey, ColumnName, val) {

            var Value = $(val).val();
            //  console.log(Value);
            $.ajax({
                async: false,
                type: "POST",
                url: "../Web/FunctionMatrix.aspx/UpdateRange",
                data: "{RangeKey:'" + RangeKey + "',ColumnName:'" + ColumnName + "',Value:'" + Value + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(data.d);

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function UpdateGoalValue(ValueKey, val) {
          
            var Value = $(val).val();
            $.ajax({
                async: false,
                type: "POST",
                url: "../Web/FunctionGoal.aspx/UpdateGoalValue",
                data: "{ValueKey:'" + ValueKey + "',Value:'" + Value + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                 //    console.log(data.d);

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }

        function SaveGoal() {
            var GoalKey = $('#hdn_GoalKey').val();
            var GoalName = $('#txt_GoalName').val();

            var valid = validate();

            parent.console.log(valid);

            if (valid == true) {
                alertNotify(' Goal Error', 'Please fill the Goal Fields', 'bg-warning border-warning');
            }
            else {
                parent.SaveGoal(GoalKey, GoalName);
            }

           

        }
        function CancelGoal() {
            parent.CancelGoal();
        }
        function validate() {
            var GoalName = $('#txt_GoalName').val();
            var GoalType = $('#ddl_GoalType').val();
            var GoalBy = $('#ddl_GoalBy').val();
           
            if (GoalName == '' || GoalType == '' || GoalBy == '') {
               // alert('false');
                alertNotify(' Goal Error', '* Fields are manditory', 'bg-warning border-warning');
                return false;
            }
            else {
             //   alert('true');
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 form-group">
                <label>Goal Name <span class="text-danger">*</span></label>
                <input type="text" id="txt_GoalName" required runat="server" class="form-control" /> 

            </div>
            <div class="col-md-3 form-group">
                <label>Goal Scope </label>
               
                <asp:DropDownList ID="ddl_GoalScope" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
             <div class="col-md-3 form-group">
                <label>Goal By <span class="text-danger">*</span></label>
              
                <asp:DropDownList ID="ddl_GoalBy" required runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
            <div class="col-md-3 form-group">
                <label>Goal By <span class="text-danger">*</span></label>
              
                <asp:DropDownList required ID="ddl_GoalType" runat="server" CssClass="form-control">
                    <asp:ListItem Text="Select Goal Type" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Rows only" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Rows & Columns" Value="2"></asp:ListItem>
                </asp:DropDownList>

            </div>
            <div class="col-md-12 form-group my-auto text-right">
           <%--     <button type="button" class="btn btn-primary btn-sm" id="btn_GenerateGoal" runat="server" oncl onserverclick="btn_GenerateGoal_ServerClick">Generate Goal Table</button>--%>
                <asp:Button class="btn btn-primary btn-sm" id="btn_GenerateGoal" runat="server" OnClick="btn_GenerateGoal_ServerClick" OnClientClick="return validate();" Text="Generate Goal " />
                <asp:Button class="btn btn-primary btn-sm" id="btn_UpdateGoal" visible="false" runat="server" OnClick="btn_UpdateGoal_ServerClick" OnClientClick="return validate();" Text="Update Goal " />
              <%--   <button type="button" class="btn btn-primary btn-sm" visible="false" id="btn_UpdateGoal"  onserverclick="btn_UpdateGoal_ServerClick" runat="server" >Update Goal </button>--%>
            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <asp:PlaceHolder ID="ph_GoalUI" runat="server"></asp:PlaceHolder>
                        </tbody>
                    </table>
                </div>
                
            </div>
            <div class="col-md-12 text-right">
                <button id="btn_SaveGoal" runat="server" visible="false"  type="button" class="btn btn-primary btn-sm" onclick="SaveGoal();">Save</button>
            </div>
        </div>
        
    </div>
    <asp:HiddenField ID="hdn_GoalKey" runat="server" ClientIDMode="Static" />
</asp:Content>

