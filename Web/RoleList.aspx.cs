﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_RoleList : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();

        loadRoleList();
    }

    private void loadRoleList()
    {
        string str_Sql = "select *, convert(varchar, CreatedOn, 101) as Created_On  from Role order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_RoleListTr, "RoleListTr.txt", "");
    }
    [WebMethod]
    public static string deleteRole(string RoleKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Role where RoleKey=@RoleKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@RoleKey", RoleKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
}