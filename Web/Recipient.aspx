﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="Recipient.aspx.cs" Inherits="Web_Recipients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Recipient | </title>
    <style>
        .Error-Form-Control{
            border: 1px solid #f54343;
        }
    </style>
    <script>

        function ValidateRepId(element) {
            //alert(Currency_Key);
            let RepId = $(element).val();
            if (RepId != "") {

                let RepIdUpdate = $('#hdn_RecipientId').val();
                $('#btn_Update').removeAttr('disabled');
                if (RepIdUpdate != RepId) {
                    $.ajax({
                        type: "POST",
                        url: "../Web/Recipient.aspx/IsRepIdExist",
                        data: "{RepId:'" + RepId + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == "0") {
                                $('#btn_Save').prop('disabled', 'true');
                                $('#btn_Update').prop('disabled', 'true');
                               
                                $('#txt_RecipientId-error').addClass(' validation-invalid-label');
                                $('#txt_RecipientId-error').removeClass(' validation-valid-label');
                                $('#txt_RecipientId-error').text('Recipient Id Already Exists');

                            }
                            else if (data.d == "1") {
                                $('#btn_Save').removeAttr('disabled');
                                $('#btn_Update').removeAttr('disabled');
                                $('#txt_RecipientId-error').removeClass('validation-invalid-label');
                                $('#txt_RecipientId-error').addClass(' validation-valid-label');
                             //   $('#txt_RecipientId-error').text('Valid Recipient Id');
                               $('#txt_RecipientId-error').addClass('d-none');
                            }

                          
                        },
                        error: function () {
                            alert('Error communicating with server');
                        }
                    });
                }

               
            }

        }

        function IsRepIdExist(element) {

            let RepIdUpdate = $('#hdn_RecipientId').val();
            let RepId = $(element).val();
            if (RepIdUpdate != RepId) {
                $.ajax({
                    type: "POST",
                    url: "../Web/Recipient.aspx/IsRepIdExist",
                    data: "{RepId:'" + RepId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "1") {
                            $(element).addClass('Error-Form-Control');
                            $('#txt_RecipientId-error').addClass('d-none');
                            alertNotify('Invalid Recipient Id', 'Recipient Id ' + RepId + ' is already exist', 'bg-danger border-danger');
                        }
                        else {
                            $(element).removeClass('Error-Form-Control');
                            $('#txt_RecipientId-error').removeClass('d-none');
                        }
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
           
           
         
        }

        function validation() {
            var plan = checkValidPlan();

            if (plan == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function checkValidPlan() {
            let sDate = $('#dp_StartDate').val();
            let eDate = $('#dp_EndDate').val();

            var startDate = new Date(sDate);
            var endDate = new Date(eDate);

            var valid = true;

            if (sDate != '' || eDate != '') {
                valid= checkValidPlanDate();
            }
            else {
                valid = true;
            }

            return valid;
        }

        function checkValidPlanDate() {
            let sDate = $('#dp_StartDate').val();
            let eDate = $('#dp_EndDate').val();

            var startDate = new Date(sDate);
            var endDate = new Date(eDate);

            var valid = true;
            if (sDate != '') {
                if (startDate > endDate) {
                    alertNotify('Invalid End Date', 'Plan End Date is greater than start date', 'bg-danger border-danger');
                    $('#dp_EndDate-error').addClass('d-none');
                    $('#dp_StartDate-error').addClass('d-none');
                    valid= false;
                }
                else {
                    valid = true;
                }
            }
            else {
                alertNotify('Invalid Start Date', 'Please select the start date', 'bg-danger border-danger');
                $('#dp_EndDate-error').addClass('d-none');
                $('#dp_StartDate-error').addClass('d-none');
                valid = false;
            }

            return valid;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="container-fluid">
                            <div class="row">
                                
                                 <!--Input Form-->
                                 <div  id="div_InputForm" runat="server" class="col-lg-12 col-md-12 ">
                                        <!-- Basic layout-->
                            <div class="card border-top-3 border-top-primary-400 ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Recipient Info</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                         <a class="list-icons-item" data-action="collapse"></a>
                                       
                                        <asp:Button  ID="btn_Save"   Text="Save" OnClick="btn_Save_Click" runat="server" OnClientClick="return validation();" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" OnClientClick="return validation();" runat="server" CssClass="btn btn-primary  btn-sm " />
                                       
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-3">
                                                <label>Recipient ID <span class="text-danger">*</span></label>
                                                <input id="txt_RecipientId" onfocusOut="ValidateRepId(this)" required runat="server" type="text" class="form-control" >
                                            </div>
                                             <div class="form-group col-md-3">
                                                    <label class="">User Id <span class="text-danger">(email)*</span>
                                                        <input id="chk_IsLogin" checked  runat="server" type="checkbox" class="">

                                                    </label>
                                                    <input id="txt_UserId" required runat="server" type="email"  class="form-control" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                    <label>First Name <span class="text-danger">*</span></label>
                                                    <input id="txt_FName" required runat="server" type="text" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                    <label>Last Name <span class="text-danger">*</span></label>
                                                    <input id="txt_LName" required runat="server" type="text" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                    <label>Recipient Type <span class="text-danger">*</span></label>
                                                
                                                <asp:DropDownList ID="ddl_RecipientCategoryListItem" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Recipient Job Category</label>
                                                <asp:DropDownList ID="dd_JobCataegory" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Reporting Manager</label>
                                             
                                                <asp:DropDownList ID="ddl_RecipientManager" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                    <label>Recipient Territory <span class="text-danger">*</span></label>
                                                 
                                                <asp:DropDownList ID="ddl_TerritoryListItem" CssClass="form-control" runat="server"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Join Date <span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" id="dp_JoinDate" runat="server" />
                                            </div>
                                            <div class="form-group col-md-3">
                                                    <label>Plan Start Date <span class="text-danger">*</span></label>
                                                    <input id="dp_StartDate" onchange="checkValidPlanDate();" runat="server" type="date" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                    <label>Plan End Date </label>
                                                    <input id="dp_EndDate" onchange="checkValidPlanDate();"  runat="server" type="date" class="form-control" >
                                            </div>
                                             <div class="form-group col-md-3">

                                                <label>Is Active </label>
                                                  <input id="chb_IsActive"  runat="server" type="checkbox" class="form-control">
                                             </div>
                                            <div class="form-group col-md-6">
                                                <label>Notes</label>
                                                <textarea id="txt_Notes"  runat="server" rows="3" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
                                            </div>
    
                                            
                                        </div>
                                        </div>
                                    </div>
                               
                                <!-- /basic layout -->
                                     <!--- Contact Info -->
                                     <div class="card border-top-2 border-top-primary-600 ">
                                        <div class="card-header header-elements-inline">
                                            <h5 class="card-title">Contact Info</h5>
                                            <div class="header-elements">
                                                <div class="list-icons">
                                                   
                                                    <a class="list-icons-item" data-action="collapse"></a>
                                                    <a class="list-icons-item" data-action="reload"></a>
                                        
                                                </div>
                                            </div>
                                        </div>
    
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Address 1</label>
                                                    <textarea id="txt_Address1" runat="server" class="form-control" ></textarea>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Address 2</label>
                                                    <textarea id="txt_Address2" runat="server" class="form-control" ></textarea>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Phone <span>Primary</span></label>
                                                    <input id="txt_PhonePrimary" runat="server" type="text" class="form-control" />
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Phone <span>Secondary</span></label>
                                                    <input id="txt_PhoneSecondary" runat="server" type="text" class="form-control" />
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Country</label>
                                                   <asp:DropDownList ID="ddl_Country" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>State</label>
                                                     <asp:DropDownList ID="ddl_State" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>City</label>
                                                    <asp:DropDownList ID="ddl_city" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Zip Code</label>
                                                    <input id="txt_ZipCode" runat="server" type="text" class="form-control" />
                                                </div>
                                               
                                            </div>
                                        </div>
                                     

                                       </div>

                                     <!-- / Contact Info-->

                                     <!-- Compensation Info -->
                                         <div class="card border-top-2 border-top-primary-300 ">
                                        <div class="card-header header-elements-inline">
                                            <h5 class="card-title">Compensation Info</h5>
                                            <div class="header-elements">
                                                <div class="list-icons">
                                                   
                                                    <a class="list-icons-item" data-action="collapse"></a>
                                                    <a class="list-icons-item" data-action="reload"></a>
                                        
                                                </div>
                                            </div>
                                        </div>
    
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Salary Amount</label>
                                                     <input id="txt_SalaryAmount" runat="server" type="text" class="form-control number" />
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Pay out Currency</label>
                                                     <asp:DropDownList ID="ddl_PayOutCurrency" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Adjustment Amount</label>
                                                     <input id="txt_AdjustmentAmount" runat="server" type="text" class="form-control number" />
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Recovery Amount </label>
                                                     <input id="txt_RecoveryAmount" runat="server" type="text" class="form-control number" />
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>Goal Amount</label>
                                                     <input id="txtGoalAmount" runat="server" type="text" class="form-control number" />
                                                </div>
                                              
                                            </div>
                                        </div>
                                     

                                       </div>
                                     <!--/ -->


                                      <!-- Custom Filed Info -->
                                         <div class="card border-top-2 border-top-primary-300 ">
                                        <div class="card-header header-elements-inline">
                                            <h5 class="card-title">Custom Fields</h5>
                                            <div class="header-elements">
                                                <div class="list-icons">
                                                   
                                                    <a class="list-icons-item" data-action="collapse"></a>
                                                        <a class="btn btn-sm btn-primary" href="TransactionCustomConfig.aspx?PrimaryTableName=Recipient">Add</a>
                                                </div>
                                            </div>
                                        </div>
    
                                        <div class="card-body">
                                            <div class="">
                                                <asp:Panel ID="panel_CustomRecipient" CssClass="row" runat="server"></asp:Panel>
                                              
                                            </div>
                                        </div>
                                     

                                       </div>
                                     <!--/ -->
                                    </div>
                               
                                <!--Input Form-->

                                
                            </div>
                        </div>
    <asp:HiddenField ID="hdn_RecipientId" runat="server" ClientIDMode="Static" />
</asp:Content>

