﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class Web_CommissionReportStatement : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;

    public static object JSONConvert { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        loadFiscalYear();
            if(Com.GetQueryStringValue("RecipientId")!="" && Com.GetQueryStringValue("RecipientId") != null)
            {
                string RecipientId = Com.GetQueryStringValue("RecipientId");
                string PeriodKey = Com.GetQueryStringValue("PeriodKey");
                string FiscalYear= Com.GetQueryStringValue("FiscalYear");
            ddl_FiscalYear.SelectedValue = FiscalYear;
            hdn_PeriodKey.Value = PeriodKey;
            loadCommisssionReport(RecipientId, PeriodKey);
            img_CompanyLogo.Src = "../UploadFile/Logo/" + SC.CompanyLogo;
            loadRecipientInfo(Com.GetQueryStringValue("RecipientId"), PeriodKey);
            div_Invoice.Visible = true;
            }
       
    }
    private void loadRecipientInfo(string RecipientId, string PeriodKey)
    {
        string str_Sql = "select * from v_RecipientDetials where RecipientId=@RecipientId;select a.PeriodKey, b.FascalYear, CONCAT(format(a.StartDate,'MM/dd/yyyy'),'  -  ',format(a.EndDate,'MM/dd/yyyy')) as Period from Period a join Calendar b on a.CalendarKey=b.CalendarKey where a.PeriodKey=@PeriodKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@RecipientId", RecipientId);
        cmd.Parameters.AddWithValue("@PeriodKey", PeriodKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        //string RecipientInfo = "";
        //string RecipirntInfoTemplate = this.PHT.ReadFileToString("DivComissionRecipientInfo.txt");
        //foreach (DataRow dr in ds.Tables[0].Rows)
        //{
        //    RecipientInfo = this.PHT.ReplaceVariableWithValue(dr, RecipirntInfoTemplate);
        //}
        // this.PHT.LoadGridItem(ds, ph_RecipientInfo, "DivComissionRecipientInfo.txt", "");
        // RecipientInfo = RecipientInfo.Replace("%%%%", Com.GetQueryStringValue(""));

        string Title = "";
        foreach (DataRow dr2 in ds.Tables[1].Rows)
        {
            lbl_Period.Text = dr2["Period"].ToString();
            lbl_FiscalYear.Text = dr2["FascalYear"].ToString();
            Title += dr2["FascalYear"].ToString() + " - " + dr2["Period"].ToString() + " | ";
        }
        foreach (DataRow dr1 in ds.Tables[0].Rows)
        {
            lbl_RecipientId.Text = dr1["RecipientId"].ToString();
            lbl_RecipientName.Text = dr1["RecipientName"].ToString();
            lbl_TerritoryId.Text = dr1["TerritoryId"].ToString();
            lbl_Position.Text = dr1["Position"].ToString();
            Title += dr1["RecipientId"].ToString() +" - " + dr1["RecipientName"].ToString();
        }
        lbl_CommisionStatementTitle.Text = Title;
    }
    private void loadFiscalYear()
    {
        string str_Select = "select distinct(PeriodYear) as Value, PeriodYear as Text from PeriodMaster where PeriodKey in (select PeriodKey from CalculationPayOut)";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_FiscalYear, ds.Tables[0], "Text", "Value", true, "--Select Year --");
    }

    private void loadCommisssionReport(string recipientId, string periodKey)
    {
        string str_Sql = "p_RecipientCommissionReport";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@RecipientId", recipientId);
        cmd.Parameters.AddWithValue("@PeriodKey", periodKey);
        DataSet ds = this.DA.GetDataSet(cmd);

        string DivReport="", DivCommissionDetail = "", DivCommissionTxnSet="";

        foreach(DataRow dr1 in ds.Tables[0].Rows)
        {
            DivCommissionDetail = CommissionDetails(dr1["Commissionkey"].ToString(), ds.Tables[3]);

            DivCommissionTxnSet =CommissionTransaction(dr1["Commissionkey"].ToString(), ds);

            DivReport += DivCommissionDetail + DivCommissionTxnSet;
        }

        ph_ReportCommissions.Controls.Add(new LiteralControl(DivReport));

        this.PHT.LoadGridItem(ds, ph_PayOutTr, "CommissionReportPayOutTr.txt", "tbl2");
        this.PHT.LoadGridItem(ds, ph_TotalPayOutTr, "CommissionReportTotalPayOutTr.txt", "tbl4");
        //TotalRoundPayOut
        lbl_TotalPayOut.Text = ds.Tables[4].Rows[0]["TotalRoundPayOut"].ToString();
         

    }
    public string CommissionTransaction(string ComissionKey, DataSet ds1)
    {
        string DivCommissionTransaction = "", DivCommissionSummarizedPayOut = "", DivTransactionSumarizedTemplate="", TxnCommissionReportTableTemplate ="", DivTransactionTemplate ="", DivSummarizedPayOutTemplate="";

        TxnCommissionReportTableTemplate= this.PHT.ReadFileToString("TxnCommissionReportTableTemplate.txt");
        DivTransactionTemplate = this.PHT.ReadFileToString("DivTransactionTemplate.txt");
        DivTransactionSumarizedTemplate = this.PHT.ReadFileToString("txnCommissionReportSummarizedTr.txt");

        foreach (DataRow dr2 in ds1.Tables[1].Select("CommissionKey='" + ComissionKey + "'"))
        {
            DivCommissionTransaction+=this.PHT.ReplaceVariableWithValue(dr2, DivTransactionTemplate);
        }
        foreach (DataRow dr3 in ds1.Tables[2].Select("CommissionKey='" + ComissionKey + "'"))
        {
            DivCommissionTransaction += this.PHT.ReplaceVariableWithValue(dr3, DivTransactionSumarizedTemplate);
        }
        TxnCommissionReportTableTemplate = TxnCommissionReportTableTemplate.Replace("%%tbody%%", DivCommissionTransaction);

            return TxnCommissionReportTableTemplate;
        //txnCommissionReportSummarizedTr
    }

    public string CommissionDetails(string ComissionKey, DataTable dt2)
    {
        string DivCommissionDetailTemplate = "";
        
        DivCommissionDetailTemplate = this.PHT.ReadFileToString("DivCommissionDetailReportTemplate.txt");
        foreach (DataRow dr2 in dt2.Select("CommissionKey='" + ComissionKey + "'"))
        {
            DivCommissionDetailTemplate = this.PHT.ReplaceVariableWithValue(dr2, DivCommissionDetailTemplate);
        }
        return DivCommissionDetailTemplate;
    }

    [WebMethod]
    public static string loadOptionPeriod(string Year)
    {
        DataAccess DA = new DataAccess();
        string str_Sql = "select PeriodKey as Value, Concat(format(StartDate,'MM/dd/yyyy'),' - ', format(EndDate,'MM/dd/yyyy')) as Text from PeriodMaster where PeriodKey in (select PeriodKey from CalculationPayOut) and PeriodYear=@Year";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("Year", Year);
        DataTable dt = DA.GetDataTable(sqlcmd);
        string optionRole = "";
        foreach (DataRow dr in dt.Rows)
        {

            optionRole += "<option value='" + dr["Value"].ToString() + "'>" + dr["Text"].ToString() + "</option>";
        }
        return optionRole;
    }

    [WebMethod]
    public static string LoadRecipients(string FiscalYear,string PeriodKey)
    {
        DataAccess DA = new DataAccess();
        PhTemplate PHT = new PhTemplate();
        string str_Sql = "select distinct(SalesRepID), concat(b.FirstName,' ',b.LastName) AS RecipientName, a.PeriodKey,c.Text as Role from CalculationPayOut a join Recipients b on a.SalesRepId=b.RecipientId  join v_RecipientCategory c on b.RecipientType=c.value where PeriodKey=@PeriodKey  order by SalesRepID ";
       /// string str_Sql = "select distinct(SalesRepID), concat(b.FirstName,' ',b.LastName) AS RecipientName, a.PeriodKey,c.Text as Role from CalculationPayOut a join Recipients b on a.SalesRepId=b.RecipientId  join v_RecipientCategory c on b.RecipientType=c.value where PeriodKey=@PeriodKey order by SalesRepID ";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("PeriodKey", PeriodKey);
        DataTable dt = DA.GetDataTable(sqlcmd);
        string RecipientTrTemplate = "", RecipientListTr="";

        RecipientTrTemplate = PHT.ReadFileToString("DivRecipientTrTemplate.txt");
        foreach (DataRow dr in dt.Rows)
        {
            RecipientListTr+= PHT.ReplaceVariableWithValue(dr, RecipientTrTemplate);
        }
        RecipientListTr = RecipientListTr.Replace("%%FiscalYear%%", FiscalYear);

        //JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        //Dictionary<string, object> childRow;
        //foreach (DataRow row in dt.Rows)
        //{
        //    childRow = new Dictionary<string, object>();
        //    foreach (DataColumn col in dt.Columns)
        //    {
        //        childRow.Add(col.ColumnName, row[col]);
        //    }
        //    parentRow.Add(childRow);
        //}
        //return jsSerializer.Serialize(parentRow);

        return RecipientListTr;
    }
}