﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionProcedure : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.Com = new Common();

       
        loadProcedureList();
        if (!IsPostBack)
        {
            if (Request.QueryString["procedure_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadProcedureDetailToEdit(Request.QueryString["procedure_key"]);
                // div_AduitLog.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }

    private void loadProcedureDetailToEdit(string procedure_key)
    {
        string str_Sql_view = "select * " +
           " from CommissionProcedure where  ProcedureKey =@ProcedureKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ProcedureKey", procedure_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {
            txt_ProcedureName.Value = dr["ProcedureName"].ToString();
          

            txt_Desc.Value = dr["Description"].ToString();

        }
    }

    private void loadProcedureList()
    {
        string str_Sql = "select *, convert(varchar, a.CreatedOn, 101) as Created_On, (select FirstName from users where UserKey=a.CreatedBy) as CreatedByName from CommissionProcedure a   order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_CommissionProcedure, "CommissionProcedureListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string str_Key = Guid.NewGuid().ToString();
        // CommissionProcedure (ProcedureKey, ProcedureName, Description, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "insert into CommissionProcedure  (ProcedureKey, ProcedureName, Description, CreatedOn, CreatedBy)"
                            + "select @ProcedureKey, @ProcedureName, @Description, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@ProcedureKey", str_Key);
        cmd.Parameters.AddWithValue("@ProcedureName", txt_ProcedureName.Value);
      
        cmd.Parameters.AddWithValue("@Description", txt_Desc.Value);

        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string procedure_key = Request.QueryString["procedure_key"];

        //  CommissionVariables  (VariableKey, VariableName, DataType, DefaultValue, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "UPDATE CommissionProcedure set   ProcedureName=@ProcedureName, Description=@Description,  " +
            "ModifiedOn=@ModifiedOn, ModifiedBy =@ModifiedBy where  ProcedureKey =@ProcedureKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);


        cmd.Parameters.AddWithValue("@ProcedureName", txt_ProcedureName.Value);

        cmd.Parameters.AddWithValue("@Description", txt_Desc.Value);




        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@ProcedureKey", procedure_key);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/CommissionProcedure.aspx?procedure_key=" + procedure_key + "");
    }
}