﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Customers : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();

        loadCustomerList();
        if (Request.QueryString["cust_key"] != null)
        {
            viewCustomerDetails(Request.QueryString["cust_key"]);
            div_CustomerDetails.Visible = true;
            div_CustomerDetails.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Visible = false;
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["cust_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadCustomerDetailToEdit(Request.QueryString["cust_key"]);
               // div_CustomerDetails.Visible = false;
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }
    [WebMethod]
    public static string viewCustomer(string cust_key)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select * from Customers where CustomerKey =@CustomerKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@CustomerKey", cust_key);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivCustomerView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    private void loadCustomerDetailToEdit(string cust_key)
    {
        string str_Sql_view = "select * from Customers where CustomerKey =@CustomerKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CustomerKey", cust_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {
            //  (CustomerKey CustomerName,CustomerId CustomerEmail CustomerType Mobile Country State City Address CreatedBy CreatedOn ModifiedBy ModifiedOn)
            txt_CustomerId.Value= dr["CustomerId"].ToString();
            hdn_CustomerId.Value= dr["CustomerId"].ToString();
            txt_CustomerName.Value = dr["CustomerName"].ToString();
            txt_CustomerEmail.Value = dr["CustomerEmail"].ToString();
            txt_CustomerType.Value = dr["CustomerType"].ToString();
            txt_Mobile.Value = dr["Mobile"].ToString();
            dd_Country.Value = dr["Country"].ToString();
            dd_State.Value = dr["State"].ToString();
            dd_City.Value = dr["City"].ToString();
            txt_Address.Value = dr["Address"].ToString();
           

        }
    }

    private void viewCustomerDetails(string cust_key)
    {
        string str_Sql_view = "select * from Customers where CustomerKey =@CustomerKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CustomerKey", cust_key);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_CustomerDetails, "DivCustomerView.txt", "");
    }

    private void loadCustomerList()
    {
        string str_Sql = "select * from Customers order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_CustomersTr, "CustomersListTr.txt", "");
    }
    [WebMethod]
    public static string deleteCustomer(string CustKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Customers where CustomerKey=@CustomerKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@CustomerKey", CustKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string ValidateCustomerId(String CustomerId)
    {
        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select * from Customers where CustomerId =@CustomerId";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@CustomerId", CustomerId);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string result = "1";

            if (dt.Rows.Count > 0)
            {
                result = "0";
            }


            return result;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    protected void btn_Excel_ServerClick(object sender, EventArgs e)
    {
        string str_Sql = "select * from Customers ";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);

        DataTable dt = this.DA.GetDataTable(sqlcmd);

        DateTime time = DateTime.Now;
        string CurrentDate_Time = time.ToString("MM/dd/yyyy hh:mm:ss");
        this.OCF.ExporttoExcel(dt, "CustomersExcel" + CurrentDate_Time + "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
       
        string str_Key = Guid.NewGuid().ToString();
        //string trans_Key = Guid.NewGuid().ToString();
        string str_Sql = "insert into Customers(CustomerKey ,CustomerName ,CustomerId,CustomerEmail ,CustomerType ,Mobile ,Country ,State ,City ,Address ,CreatedBy ,CreatedOn )"
                            + "select @CustomerKey , @CustomerName , @CustomerId,@CustomerEmail , @CustomerType , @Mobile , @Country , @State , @City , @Address , @CreatedBy , @CreatedOn ";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CustomerKey", str_Key);
        cmd.Parameters.AddWithValue("@CustomerName", txt_CustomerName.Value);
        cmd.Parameters.AddWithValue("@CustomerId", txt_CustomerId.Value);
        cmd.Parameters.AddWithValue("@CustomerEmail", txt_CustomerEmail .Value);
        cmd.Parameters.AddWithValue("@CustomerType", txt_CustomerType.Value);
        cmd.Parameters.AddWithValue("@Mobile", txt_Mobile.Value);
        cmd.Parameters.AddWithValue("@Country", dd_Country.Value);
        cmd.Parameters.AddWithValue("@State", dd_State.Value);
        cmd.Parameters.AddWithValue("@City", dd_City.Value);
        cmd.Parameters.AddWithValue("@Address", txt_Address.Value);
        
        cmd.Parameters.AddWithValue("@CreatedBy", str_Key);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string cust_key = Request.QueryString["cust_key"];
        //  string str_Sql = "UPDATE transactions set transaction_id=@transaction_id, transaction_line=@transaction_line,  where transaction_key=@transaction_key  ";

        string str_Sql = "UPDATE Customers set CustomerId=@CustomerId, CustomerName=@CustomerName, CustomerEmail=@CustomerEmail, CustomerType=@CustomerType, Mobile=@Mobile, Country=@Country, State=@State, City=@City, Address=@Address , ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where CustomerKey=@CustomerKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CustomerName", txt_CustomerName.Value);
        cmd.Parameters.AddWithValue("@CustomerId", txt_CustomerId.Value);
        cmd.Parameters.AddWithValue("@CustomerEmail", txt_CustomerEmail.Value);
        cmd.Parameters.AddWithValue("@CustomerType", txt_CustomerType.Value);
        cmd.Parameters.AddWithValue("@Mobile", txt_Mobile.Value);
        cmd.Parameters.AddWithValue("@Country", dd_Country.Value);
        cmd.Parameters.AddWithValue("@State", dd_State.Value);
        cmd.Parameters.AddWithValue("@City", dd_City.Value);
        cmd.Parameters.AddWithValue("@Address", txt_Address.Value);
        cmd.Parameters.AddWithValue("@ModifiedBy", cust_key);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@CustomerKey", cust_key);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/Customers.aspx?cust_key=" + cust_key + "");
    }
}