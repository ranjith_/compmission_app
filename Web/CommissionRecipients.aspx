﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" AutoEventWireup="true" CodeFile="CommissionRecipients.aspx.cs" Inherits="Web_CommissionRecipients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                         <h5 class="card-title d-flex">  <span class="ml-2">Recipients</span>
                </h5> 
                    </div>
                    <div class="card-body">
                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <h6>Assign Recipients</h6>
                                                <select id="DdAssignRecipient" runat="server" onchange="assignRecipientBy(this)" class="form-control">
                                                    <option value="0">-select type-</option>
                                                    <option value="1">Select Invidiual Recipients</option>
                                                     <option value="2">Select  Recipients By Conditions</option>
                                                </select>
                                             
                                                
                                            </div>
                                             <div runat="server" class="col-md-12 d-none" id="Individual_Recipients">
                                                 <div class="table-responsive">
                                                <table id="table-Individual-Recipient"  class="table datatable-basic text-center" >
                                                    <thead>
                                                        <tr>
                                                            <th>Select Recipients </th>
                                                            <th>Name</th>
                                                            <th>Role</th>
                                                            <th>Manager</th>
                                                            <th>Territory</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:PlaceHolder ID="ph_RecipientListtr" runat="server"></asp:PlaceHolder>
                                                       
                                                    </tbody>
                                                </table>
                                                </div>
                                             </div>
                                            <div runat="server" class="form-group col-md-12 d-none" id="Condition_Recipients">
                                               
                                                <table id="table-Condition-Recipient"  class="table custom-panel">
                                                    <thead class="text-center">
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Operator</th>
                                                            <th>Role</th>
                                                            <th>Condition</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:PlaceHolder ID="ph_RecipientConditionTr" runat="server"></asp:PlaceHolder>
                                                      
                                                    </tbody>
                                                </table>
                                            </div>
                                               <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab1()" class="btn btn-primary">PREVIOUS</button>
                                               <button id="btn_SaveRecipient" runat="server" type="button" onclick="Tab3()" class="btn btn-primary">NEXT</button>
                                                     <button id="btn_UpdateRecipient" runat="server" type="button" onclick="UpdateRecipients()" class="btn btn-primary">Update</button>
                                                </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

