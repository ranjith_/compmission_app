﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Diagnostics;
using iTextSharp.text.html.simpleparser;
public partial class Web_CommissionPlanDocument : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        GetAgreement(Com.GetQueryStringValue("RecipientKey"));
    }

    [Obsolete]
    public void MakePDf(string recipientKey, string str_DocTitle, string doc, bool showSignature, string str_signature)
    {
        Document document = new Document();
        PdfWriter.GetInstance(document, new FileStream(Request.PhysicalApplicationPath + "\\UploadFile\\Agreement\\CommissionPlan-" + recipientKey + ".pdf", FileMode.Create));
        document.Open();
        //Paragraph p = new Paragraph(str_DocTitle);
        //p.Alignment = Element.ALIGN_CENTER;
        //document.Add(p);
        //iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(Request.PhysicalApplicationPath + "\\UploadFile\\Logo\\db082fef-7245-4d2b-a154-87b3e5b6b8bd_Bada_Logo.jpg");
        //img.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
        //img.ScaleToFit(100, 150);
        //document.Add(img);
        iTextSharp.text.html.simpleparser.HTMLWorker hw =
                     new iTextSharp.text.html.simpleparser.HTMLWorker(document);
        hw.Parse(new StringReader(doc));
        //if (showSignature == true)
        //{
        //    Paragraph p1 = new Paragraph("I Agree the  Terms and Conditions gives you the right to terminate the access of abusive users or to terminate the access to users who do not follow your rules and guidelines, as well as other desirable business benefits. ");
        //    p.Alignment = Element.ALIGN_CENTER;
        //    document.Add(p1);
        //}
        iTextSharp.text.html.simpleparser.HTMLWorker hw1 =
                    new iTextSharp.text.html.simpleparser.HTMLWorker(document);
        hw1.Parse(new StringReader(str_signature));
        document.Close();
      //  CommissionPlanPdf.Attributes.Add("src", "../UploadFile/Agreement/CommissionPlan-"+recipientKey+".pdf");
    }

    public void GetAgreement(string recipient_key)
    {
        try
        {

            string str_Sql = "select a.PlanKey, a.RecipientKey, a.SpecificTerms,a.AgreementKey, b.Headers, b.Footer, b.DocTitle, b.IncludeCommission,b.IncludeRecipient, b.showSignature from RecipientPlan a join Agreement b on a.AgreementKey=b.AgreementKey where a.RecipientKey=@RecipientKey;"
           + "select *, b.CommissionRecipientKey,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) Count from Commission a   join CommissionRecipients b on a.CommissionKey=b.CommissionKey where RecipientKey=@RecipientKey;" +
           "select *, FORMAT(B.StartDate, 'MM/dd/yyyy') as sDate, FORMAT(B.EndDate, 'MM/dd/yyyy') as eDate  from v_RecipientDetials a join RecipientPlan b on a.RecipientKey=b.RecipientKey where a.RecipientKey=@RecipientKey";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@RecipientKey", recipient_key);
            DataSet ds = DA.GetDataSet(cmd);
            //this.PHT.LoadGridItem(ds, ph_CommissionRecipientCollapsibleTr, "CollapsibleRecipientCommissionTr.txt", "");
            string str_Template = PHT.ReadFileToString("DivCommissionAgreement.txt"), str_HTML = "", str_Replace = "";
            string str_PlanSignature = PHT.ReadFileToString("DivPlanSignature.txt");
            string str_DocTitle = "", str_signature = "";
            bool showSignature = false;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //str_Replace = str_Template;
                //str_Replace = str_Replace.Replace("%%recipient_name%%", dr["recipient_name"].ToString());
                //str_Replace = str_Replace.Replace("%%RecipientKey%%", dr["RecipientKey"].ToString());
                str_Replace = PHT.ReplaceVariableWithValue(dr, str_Template);

                string str_RecipientKey = dr["RecipientKey"].ToString();
                str_DocTitle = dr["DocTitle"].ToString();
             //   showSignature = Convert.ToBoolean(dr["showSignature"].ToString());
                string str_RecipientCommissionTemplate = PHT.ReadFileToString("DivRecipientsCommissionForAgreement.txt");


                string str_RecipientCommissionList = "";
                foreach (DataRow dr1 in ds.Tables[1].Select("RecipientKey='" + str_RecipientKey + "'"))
                {
                    str_RecipientCommissionList += PHT.ReplaceVariableWithValue(dr1, str_RecipientCommissionTemplate);
                    //str_Replace = this.PHT.ReplaceVariableWithValue(dr1, str_Replace);
                }
                str_Replace = str_Replace.Replace("%%DivRecipientsCommissionForAgreement%%", str_RecipientCommissionList);
                foreach (DataRow dr2 in ds.Tables[2].Select("RecipientKey='" + str_RecipientKey + "'"))
                {
                    // str_RecipientCommissionList += PHT.ReplaceVariableWithValue(dr2, str_RecipientCommissionTemplate);
                    //str_Replace = this.PHT.ReplaceVariableWithValue(dr1, str_Replace);
                    str_Replace = str_Replace.Replace("@#RECIPIENT#@", dr2["RecipientName"].ToString() );
                    str_Replace = str_Replace.Replace("@#STARTDATE#@", dr2["sDate"].ToString());
                    str_Replace = str_Replace.Replace("@#ENDDATE#@", dr2["eDate"].ToString());
                    str_signature = PHT.ReplaceVariableWithValue(dr2, str_PlanSignature);
                    str_signature = str_signature.Replace("@#RECIPIENT#@", dr2["RecipientName"].ToString());
                }
                str_HTML += str_Replace;

            }

            doc.Controls.Add(new LiteralControl(str_HTML + str_signature));

           // MakePDf(recipient_key, str_DocTitle, str_HTML, showSignature, str_signature);
        }
        catch (Exception e)
        {
            throw e;
        }
    }
}