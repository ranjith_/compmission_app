﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
public partial class Web_Commission : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        if ( Com.GetQueryStringValue("commission_key") != "")
        {
           viewCommissionDetails(Com.GetQueryStringValue("commission_key"));
            //div_AdjustmentDetails.Visible = true;
            div_CommissionForm.Visible = false;
            div_WizardCommission.Visible = true;
            //div_InputForm.Visible = false;
        }
    }

    private void viewCommissionDetails(string commission_key)
    {
        string str_Sql_view = "select * from Commission where CommissionKey =@CommissionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", commission_key);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_CommissionView, "DivCommissionView.txt", "");
        div_CardHeader.InnerHtml = "<a href='../Web/Commission.aspx?commission_key="+commission_key+"&isEdit=1' class='btn btn-primary  btn-sm '><i class='icon-plus22 position-left'></i> Edit</a>";
    }

    protected void btn_Save_Commission_ServerClick(object sender, EventArgs e)
    {
        string commissionKey = Guid.NewGuid().ToString();
        //(CommissionKey, CommissionName, EffectiveDate, Description1, Description2, CalculationFeq, CreatedOn, CreatedBy)

        string str_Sql = "insert into Commission (CommissionKey, CommissionName, EffectiveDate, Description1, Description2, CalculationFeq, CreatedOn, CreatedBy)"
                            + "select @CommissionKey, @CommissionName, @EffectiveDate, @Description1, @Description2, @CalculationFeq, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commissionKey);
        cmd.Parameters.AddWithValue("@CommissionName", txt_CommissionName.Value);
        cmd.Parameters.AddWithValue("@EffectiveDate", dp_EffectiveDate.Value);
        cmd.Parameters.AddWithValue("@Description1", txt_Description1.Value);
        cmd.Parameters.AddWithValue("@Description2", txt_Description2.Value);
        cmd.Parameters.AddWithValue("@CalculationFeq", dd_CalFeq.Value);
        

        cmd.Parameters.AddWithValue("@CreatedBy", SC.GetUserKey());

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/Commission.aspx?commission_key="+commissionKey);
    }
}