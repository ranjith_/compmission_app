﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="FunctionSplit.aspx.cs" Inherits="Web_FunctionSplit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <script>

       
        function changeSplitType(val,id) {
            var type = $(val).val();
            
            if (type == 1) {
               
                $('.Div_Field-' + id).addClass('d-none');
                $('.Div_Field-' + id).removeClass('d-flex');
                $('.Div_Variable-' + id).removeClass('d-none');
                
            }
            else if (type == 2) {
              
                $('.Div_Variable-' + id).addClass('d-none');
                $('.Div_Field-' + id).removeClass('d-none');
                $('.Div_Field-' + id).addClass('d-flex');
            }
           
        }
        function addSplitCondition(btn,BeforeTr, id) {

            $.ajax({
                async: false,
                type: "POST",
                url: "../Web/FunctionSplit.aspx/SplitConditionNew",
                data: "{id:'" + (id+1) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(data.d);
                    $('#Split').append(data.d);
                    $(btn).addClass('d-none');
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }

        function saveSplit() {
            var SplitKey = $('#hdn_SplitKey').val();
            var SplitName = $('#txt_SplitName').val();
            var CommissionKey = $('#hdn_CommissionKey').val();
            var RuleType = $('#hdn_RuleType').val();

            if (SplitName == '') {
                alertNotify('Split Error', 'Please fill out the split name ', 'bg-warning border-warning');
                return false;
            }

            InsertSplit(SplitKey, SplitName, CommissionKey, RuleType);
            IsValidate();
            $('tr.split-condition').each(function () {
                var Attribute = $('.attribute', this).val();
                var AttributeType = $('.attribute-type', this).val();
                var Variable = $('.variable-name', this).val();
                var Percentage = $('.percentage', this).val();
                var TransactionField = $('.transaction-field', this).val();

                console.log(Attribute);
                console.log(AttributeType);
                console.log(Variable);
                console.log(Percentage);
                console.log(TransactionField);

                InsertSplitCondition(SplitKey, Attribute, AttributeType, Variable, Percentage, TransactionField);
            });

            $('#hdn_FunctionConditionKey').val(SplitKey);

           // alert('Worked');
            //$('#addSplit').addClass('d-none');
            //$('#successMsg').removeClass('d-none');
           // parent.LoadFunctionStatements('Split', SplitKey, SplitName);

            parent.SplitSaved();
        }
        function InsertSplit(SplitKey, SplitName, CommissionKey, RuleType) {
            $.ajax({
                async: false,
                type: "POST",
                url: "../Web/FunctionSplit.aspx/InsertSplit",
                data: "{SplitKey:'" + SplitKey + "',SplitName:'" + SplitName + "',CommissionKey:'" + CommissionKey + "',RuleType:'" + RuleType + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //console.log(data.d);
                  
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function InsertSplitCondition(SplitKey, Attribute, AttributeType, Variable, Percentage, TransactionField) {
            $.ajax({
                async: false,
                type: "POST",
                url: "../Web/FunctionSplit.aspx/InsertSplitCondition",
                data: "{SplitKey:'" + SplitKey + "',Attribute:'" + Attribute + "',AttributeType:'" + AttributeType + "',Variable:'" + Variable + "',Percentage:'" + Percentage + "',TransactionField:'" + TransactionField + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data.d);
                 
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function UpdateSplitCondition(SplitKey) {
            //var SplitKey = $('#hdn_SplitKey').val();
            var SplitName = $('#txt_SplitName').val();

            if (SplitName == '') {
                alertNotify('Split Error', 'Please fill out the split name ', 'bg-warning border-warning');
                return false;
            }

            var validateSplit = IsValidate();

         //   alert(validateSplit);

            if (validateSplit == false) {
                alertNotify('Split Error', 'Split conditions fields are required', 'bg-warning border-warning');
                return false;
            }
            UpdateSplit(SplitKey, SplitName);
            DeleteSplitCondition(SplitKey);
            $('tr.split-condition').each(function () {
                var Attribute = $('.attribute', this).val();
                var AttributeType = $('.attribute-type', this).val();
                var Variable = $('.variable-name', this).val();
                var Percentage = $('.percentage', this).val();
                var TransactionField = $('.transaction-field', this).val();

                console.log(Attribute);
                console.log(AttributeType);
                console.log(Variable);
                console.log(Percentage);
                console.log(TransactionField);

                InsertSplitCondition(SplitKey, Attribute, AttributeType, Variable, Percentage, TransactionField);
            });
            parent.SplitSaved();
            $('#hdn_FunctionConditionKey').val(SplitKey);
            //$('#addSplit').addClass('d-none');
            //$('#successMsg').removeClass('d-none');
          
        }
        function DeleteSplitCondition(SplitKey) {
            $.ajax({
                async: false,
                type: "POST",
                url: "../Web/FunctionSplit.aspx/DeleteSplitCondition",
                data: "{SplitKey:'" + SplitKey + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data.d);

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function UpdateSplit(SplitKey, SplitName) {
            $.ajax({
                async: false,
                type: "POST",
                url: "../Web/FunctionSplit.aspx/UpdateSplit",
                data: "{SplitKey:'" + SplitKey + "',SplitName:'" + SplitName+"'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data.d);

                },
                error: function () {
                    alertNotify('Split Error', 'Split must have conditions ', 'bg-warning border-warning');
                    alert('Error communicating with server');
                }
            });
        }

        function removeSplitCondition(id) {
           
            var LevelCount = parseInt( $('#Split tr').length);

            if (LevelCount == 1) {
                alert('Split must have one condition')
            }
            else {
                $(id).closest('tr').remove();
            }
        } 

        function IsValidate() {
            var valid = true;
            $('tr.split-condition').each(function () {
                var Attribute = $('.attribute', this).val();
                var AttributeType = $('.attribute-type', this).val();
                var Variable = $('.variable-name', this).val();
                var Percentage = $('.percentage', this).val();
                var TransactionField = $('.transaction-field', this).val();

                if (!$('.attribute, .attribute-type,.variable-name,.percentage ,.transaction-field ', this).val()) {
                    valid= false;
                }

                
            });
            return valid;
        }

    </script>
    <style>
        .btn-xx{
                padding: .1rem .3rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
   <div class="container-fluid">
       <div class="row">
           <div id="successMsg" class="col-md-12 d-none">
                                        <div class="card card-body bg-green-300 border-top-3 border-top-green-800">
                                            <h3>Split Created Successfully</h3>
                                        </div>
                                    </div>    
       </div>
        <div class="row" >
                                        
            
            <div id="addSplit" class="form-group col-md-12" >
                    <label>Split Name  <span class="text-danger">*</span></label>
                 <%--   <input id="txt_SplitName" runat="server" type="text" class="form-control typeahead-basic-type" >--%>
                <input type="text" id="txt_SplitName" required runat="server" class="form-control" />
            </div>
           
            <div style="background-color: rgba(31, 230, 31, 0.1);" class="form-group col-md-12 table-responsive">
                                                   
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th colspan="2">Attribute</th>
                                    <th>Attribute Type</th>
                                    <th>Variable Name</th>
                                   
                                    <th><span class="ml-5 mr-5">%</span> Field</th>
                                    <th></th>

                                </tr>
                            </thead>
                            <tbody id="Split">
                                   <asp:PlaceHolder ID="ph_SplitCondition" runat="server"></asp:PlaceHolder>                             
                              <%--  <tr id="split-1">
                                   <td>1</td>                 
                                    <td colspan="2">
                                        <select class="form-control">
                                            <option value="">-select one-</option>
                                            <option>Sales manager</option>
                                            <option>Sales Rep</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control">
                                            <option>-select type-</option>
                                            <option>From Variable</option>
                                            <option>From Fileds</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" />
                                    </td>
                                    <td>
                                        <input type="text" placeholder="%" class="form-control w-50" />
                                    </td>
                                    <td>
                                        <select class="form-control">
                                            <option>-select Field-</option>
                                            <option>Transaction</option>
                                            <option>Territory</option>
                                        </select>
                                    </td>
                                    <td class="tr-btn">
                                        <div class="d-inline-block add-recipient">
                                            <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button>
                                         
                                        </div>
                                    </td>
                                </tr>--%>
                            </tbody>
                        </table>
                </div>
            <div class=" col-md-12 text-right">

                <button type="button" id="btn_SaveSplit" runat="server" onclick="saveSplit()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
            </div>
              
                                             
            </div>
   </div>
    <asp:HiddenField ID="hdn_SplitKey" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdn_CommissionKey" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdn_RuleType" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField ID="hdn_FunctionConditionKey" ClientIDMode="Static" runat="server" />
</asp:Content>

