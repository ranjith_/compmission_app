﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Text;
using System.IO;

public partial class Web_CommissionView : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        if(Com.GetQueryStringValue("CommissionKey") != null & Com.GetQueryStringValue("CommissionKey") != "")
        {
           
            string FunKey = Guid.NewGuid().ToString();
            loadCommissionInitDetails(Com.GetQueryStringValue("CommissionKey"));
            hpLink_EditCommission.HRef = "../Web/CommissionInitDetails.aspx?CommissionKey="+ Com.GetQueryStringValue("CommissionKey");
            hpLink_EditCommissionRecipients.HRef = "../Web/CommissionRecipient.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey");
            loadRecipientListEdit(Com.GetQueryStringValue("CommissionKey"));

            loadCommissionRuleCrediting(Com.GetQueryStringValue("CommissionKey"));
            loadCommissionRuleCalculating(Com.GetQueryStringValue("CommissionKey"));
            loadCommissionRuleSummarization(Com.GetQueryStringValue("CommissionKey"));

            loadCommissionSplit(Com.GetQueryStringValue("CommissionKey"), "Summarization", btn_PopSummarizationSplit);
            loadCommissionSplit(Com.GetQueryStringValue("CommissionKey"), "Calculation",  btn_PopCalculationSplit);
            loadCommissionSplit(Com.GetQueryStringValue("CommissionKey"), "Crediting",  btn_PopCreditingSplit);

            loadTnxGroupByFields();
            hdn_CommissionKey.Value = Com.GetQueryStringValue("CommissionKey");

            loadCalculationProcessSetup(Com.GetQueryStringValue("CommissionKey"));
        }
    }

    private void loadCalculationProcessSetup( string CommissionKey)
    {
        string str_Select = "select * from CalculationRuleProcessSetup where CommissionKey=@CommissionKey";
        SqlCommand cmd = new SqlCommand(str_Select);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataTable dt = this.DA.GetDataTable(cmd);

        if (dt.Rows.Count > 0)
        {
            foreach(DataRow dr in dt.Rows)
            {
                hdn_IsGroupBy.Value = dr["IsGroupBY"].ToString();
                ddl_CmProcessTransactionGroupBy.SelectedValue= dr["GroupBy"].ToString();
            }
        }
    }

    private void loadTnxGroupByFields()
    {
        string str_Select = "select Name as Text, Value as Value from v_TransactionGroupByFields order by Seq";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_CmProcessTransactionGroupBy, ds.Tables[0], "Text", "Value", true, "-- Select Field --");
    }

    private void loadCommissionSplit(string CommissionKey, string CalculationType, System.Web.UI.HtmlControls.HtmlButton btn_PopSummarizationSplit)
    {
        string str_Sql = "select * from Split where CommissionKey = @CommissionKey and RuleType=@FunctionType ";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        cmd.Parameters.AddWithValue("@FunctionType", CalculationType);
        DataSet ds = this.DA.GetDataSet(cmd);
        int Count = ds.Tables[0].Rows.Count;
        if (Count >= 1)
        {
            // this.PHT.LoadGridItem(ds, ph_SummarizationListTr, "DivCommissionRuleListTr.txt", "");
            DataRow dr = ds.Tables[0].Rows[0];
            btn_PopSummarizationSplit.Attributes.Add("onclick", "openSplitPopUp('" + CommissionKey + "','" + CalculationType + "','isEdit=1','"+ dr["SplitKey"].ToString() + "')");
            btn_PopSummarizationSplit.InnerHtml = "<i class='icon-file-spreadsheet mr-1'></i>" + dr["SplitName"].ToString() ;
        }
        else
        {
            btn_PopSummarizationSplit.Attributes.Add("onclick", "openSplitPopUp('"+ CommissionKey + "','"+ CalculationType + "','isNew=1')");
        }
    }

    private void loadCommissionRuleSummarization(string CommissionKey)
    {
        string RuleKey = Guid.NewGuid().ToString();
        string str_Sql = "select * from CommissionRuleFunction where CommissionKey = @CommissionKey and RuleType='Summarization' order by RuleSequence ASC";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        int sequence = (ds.Tables[0].Rows.Count) + 1;
        hpLink_Summarization.HRef = "../Web/CommissionRule.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey") + "&RuleType=Summarization&Sequence=" + sequence + "&RuleKey=" + RuleKey;
        this.PHT.LoadGridItem(ds, ph_SummarizationListTr, "DivCommissionRuleListTr.txt", "");
    }

    private void loadCommissionRuleCalculating(string CommissionKey)
    {
        string RuleKey = Guid.NewGuid().ToString();
        string str_Sql = "select *,(case when RuleSequence+1 > (select count(*) as MoveRuleDow from CommissionRuleFunction where CommissionKey=@CommissionKey) then 0 else RuleSequence+1 end ) as MoveRuleDown ,(case when RuleSequence-1 <= 0 then 0 else RuleSequence-1 end ) as MoveRuleUp from CommissionRuleFunction where CommissionKey = @CommissionKey and RuleType='Calculation' order by RuleSequence ASC";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        int sequence = (ds.Tables[0].Rows.Count)+1;
        hpLink_Calculation.HRef = "../Web/CommissionRule.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey") + "&RuleType=Calculation&Sequence=" + sequence+ "&RuleKey=" + RuleKey;

        this.PHT.LoadGridItem(ds, ph_CalculationListTr, "DivCommissionRuleListTr.txt", "");
    }

    private void loadCommissionRuleCrediting(string CommissionKey)
    {
        string RuleKey = Guid.NewGuid().ToString();
        string str_Sql = "select *,(case when RuleSequence+1 > (select count(*) as MoveRuleDow from CommissionRuleFunction where CommissionKey=@CommissionKey) then 0 else RuleSequence+1 end ) as MoveRuleDown ,(case when RuleSequence-1 <= 0 then 0 else RuleSequence-1 end ) as MoveRuleUp from CommissionRuleFunction a  where CommissionKey = @CommissionKey  and RuleType='Crediting' order by RuleSequence ASC";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        int sequence = (ds.Tables[0].Rows.Count) + 1;
        hpLink_Crediting.HRef = "../Web/CommissionRule.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey") + "&RuleType=Crediting&Sequence=" + sequence + "&RuleKey=" + RuleKey;

        this.PHT.LoadGridItem(ds, ph_CreditinglistTr, "DivCommissionRuleListTr.txt", "");
    }

    private void loadCommissionInitDetails(string CommissionKey)
    {
        string str_Sql_view = " select a.*,b.ScheduleCalculationKey,c.Text as FrequencyMaster, format(StartDate, 'MM/dd/yyyy') as sDate,format(EndDate, 'MM/dd/yyyy') as  eDate from Commission a left join SchedulePlanCalculation b on a.CommissionKey=b.CommissionKey join v_MasterFrequency c on a.Frequency=c.Value where a.CommissionKey =@CommissionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_DivCommissionInitDetails, "DivCommissionInitDetails.txt", "");
    }

    //for recipient list

    private void loadRecipientListEdit(string commissionkey)
    {
        //recipients (RecipientKey, RecipientId, RecipientType, RecipientTerritory, StartDate, UserKey, Employee, RecipientName, JobCategory, RecipientManager, EndDate, RecipientEligible, Notes, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)
        string str_Sql = "select e.Text as Territory,   c.text as Role, concat (d.FirstName, ' ' ,d.LastName) as Manager, a.RecipientKey,concat (a.FirstName, ' ' ,a.LastName) as RecipientName, a.RecipientType,a.ReportingManager, a.RecipientTerritory,case when  a.RecipientKey=b.RecipientKey then 'checked' else '' end as checked from recipients a  join (select * from CommissionRecipients where CommissionKey=@CommissionKey) b on a.RecipientKey=b.RecipientKey left join v_RecipientCategory c on a.RecipientType=c.Value left join recipients d on convert(uniqueidentifier,a.ReportingManager)=d.RecipientKey  join v_Territory e on a.RecipientTerritory=e.Value order by  checked DESC, b.CreatedOn ASC";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_SelectedCommissionRecipients, "DivSelectedCOmmisssionRecipientListTr.txt", "");
    }

    [WebMethod]
    public static string moveRule(string CommissionKey, string RuleSequence, string MoveRuleSeq, string RuleType)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "p_MoveRule";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
            cmd.Parameters.AddWithValue("@RuleSeq", RuleSequence);
            cmd.Parameters.AddWithValue("@RuleMoveSeq", MoveRuleSeq);
            cmd.Parameters.AddWithValue("@RuleType", RuleType);
            DA.ExecuteNonQuery(cmd);
            return "RuleMoved";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }

    }

    [WebMethod]

    public static string CalculationRuleSetupGroup(string CommissionKey, string GroupBy)
    {
        try
        {
            DataAccess DA = new DataAccess();
            SessionCustom Sc = new SessionCustom();
            String str_sql = "update CalculationRuleProcessSetup set IsGroupBy=1, GroupBy=@GroupBy, ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where CommissionKey=@CommissionKey";
            SqlCommand cmd = new SqlCommand(str_sql);
           
            cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
            cmd.Parameters.AddWithValue("@GroupBy", GroupBy);
            cmd.Parameters.AddWithValue("@ModifiedBy", Sc.UserKey);
            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());
            DA.ExecuteNonQuery(cmd);
            return "Saved";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    [WebMethod]

    public static string CalculationRuleSetupEach(string CommissionKey)
    {
        try
        {
            DataAccess DA = new DataAccess();
            SessionCustom Sc = new SessionCustom();
            String str_sql = "update CalculationRuleProcessSetup set IsGroupBy=0, GroupBy=NULL, ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where CommissionKey=@CommissionKey";
            SqlCommand cmd = new SqlCommand(str_sql);
            
            cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
            cmd.Parameters.AddWithValue("@ModifiedBy", Sc.UserKey);
            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);
            return "Saved";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    [WebMethod]
    public static string deleteRule(string RuleKey)
    {
        try
        {
            DataAccess DA = new DataAccess();
            SessionCustom Sc = new SessionCustom();
            String str_sql = "p_DeleteFunctionRule";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@RuleKey", RuleKey);

            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}