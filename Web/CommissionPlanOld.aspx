﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionPlanOld.aspx.cs" Inherits="Web_CommissionList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission</title>
     <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
    <style>

        #RecipientsCommissionTable td, #RecipientsCommissionTable th {
    padding: .3rem .5rem;
    vertical-align: top;
    border-top: 0px; 
     border-bottom: 0px; 
}
        .card-title > a:before {
            margin-top: -.4rem !important;
    font-size: 13px !important;
        }
        .desc-comp{
                line-height: 1.5em;
    height: 2.5em;
        width: 430px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
        }

    </style>
    <script>
        $(document).ready(function () {
            $('#RecipientsCommissionTable tr div.collapse').collapse('hide');
        });
        function saveRecipientPlan(PlanKey) {
            var startdate = $('#dp_StartDate-' + PlanKey).val();
            var enddate = $('#dp_EndDate-' + PlanKey).val();
            var adjustment = $('#dd_Adjustment-' + PlanKey).val();
            var agreement = $('#dd_Agreement-' + PlanKey).val();
            var desc = $('#txt_Desc-' + PlanKey).summernote('code');
            //console.log(startdate, enddate, adjustment, agreement, desc);
          //  return false
            $.ajax({
               // async: false,
                type: "POST",
                url: "../Web/CommissionPlan.aspx/updateRecipientPlan",
                data: "{PlanKey:'" + PlanKey + "', startdate:'" + startdate + "',enddate:'" + enddate + "', adjustment:'" + adjustment + "', agreement:'" + agreement + "', desc:'" + desc + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    //  console.log(data.d);
                   // alert('Plan saved successfully !');
                    AlertPopUp('Saved', 'Plan saved successfully !', 'success');
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function viewAgreement(recipient_key) {
            $.ajax({
                // async: false,
                type: "POST",
                url: "../Web/CommissionPlan.aspx/viewAgreement",
                data: "{recipient_key:'" + recipient_key + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#div_AgreementView').html(data.d);

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function viewPdfAgreement(filename) {
           // console.log(filename);
            //$('#AgreementViewPDF').attr('src', filename);

            var parent = $('embed#AgreementViewPDF').parent();
            var newImage = "<embed id=\"AgreementViewPDF\" src=\"" + filename + "\" width=\"100%\" height=\"500\" alt=\"pdf\" pluginspage='http://www.adobe.com/products/acrobat/readstep2.html'/>";
            var newElement = $(newImage);

            $('embed#AgreementViewPDF').remove();
            parent.append(newElement);
        }
        function setEndDate(startDate, endDate) {
            var sDate = new Date($(startDate).val());
            var syear = sDate.getFullYear();
            var smonth = ("0" + (sDate.getMonth() + 1)).slice(-2) ;
            var sday = ("0" + sDate.getDate()).slice(-2);
            var eDate = new Date(syear + 1, smonth, sday)
            var end = syear + 1 + '-' + smonth + '-' + sday;
            //console.log(end);
            $(endDate).val(end);
            

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
     
                            <div class="row">
                                <!--Data List-->
                                 <div class="col-lg-6 col-md-6">
                                     <div class="crd bg-transparent">
                                         <div class="card-header header-elements-inline  d-none">
                                             <h5 class="card-title">Commission Recipients</h5>
                                         </div>
                                         <div class="card-body">
                                                 <table id="RecipientsCommissionTable" class="table  datatable-basic">
                                           <thead>
                                               <tr class="p-2">
                                                   <th> Recipient List</th>
                                                  
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <asp:PlaceHolder ID="ph_CommissionRecipientCollapsibleTr" runat="server"></asp:PlaceHolder>
                                           </tbody>
                                       </table>
                                         </div>
                                     </div>
                                   
                                 </div>
                             
            
                            <!-- /accordion with left control button -->
                               
                                 <!--Data List-->
                                  
                                <div class="col-lg-6 col-md-6">
                                        <!--Data print-->
                                        <div class="card ">
                                            <div class="card-header header-elements-inline">
                                                <h5 class="card-title">Plan <small></small></h5>
                                                <div class="header-elements d-none">
                                                    <div class="list-icons  d-none">
                                                        <button id="checkPdf" runat="server" onserverclick="checkPdf_ServerClick">check pdf</button>
                                                        <a class="btn btn-primary  btn-sm " href="commission.aspx"> Edit</a>
                                                        <a class="list-icons-item" data-action="collapse"></a>
                                                        <a class="list-icons-item" data-action="reload"></a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div id="div_AgreementView" runat="server" clientIdMode="static" class="card-body">
                                                
                                               <embed id="AgreementViewPDF" src="" width="100%" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
                                            </div>
                                        </div>
                                         <!--Data print-->
                                </div>
            
              
                            </div>
                       
                        </div>
</asp:Content>
