﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionRecipient : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        loadCounts();

        hdn_CommissionKey.Value = Com.GetQueryStringValue("CommissionKey");
        if (!IsPostBack)
        {
            String type = this.getCommissionRecipientType(Com.GetQueryStringValue("CommissionKey"));

            if (type == "")
            {
                loadRecipientList();
                loadRecipientCondition();
                lbl_ErrorSelectRecipients.Text = "Please select assign type to add recipient to the commission";
                btn_UpdateRecipient.Visible = false;
                btn_SaveRecipient.Visible = true;
            }
            else
            {
                loadRecipientConditionEdit(Com.GetQueryStringValue("CommissionKey"));
                loadRecipientListEdit(Com.GetQueryStringValue("CommissionKey"));
                btn_UpdateRecipient.Visible = true;
                btn_SaveRecipient.Visible = false;
            }

            if (type == "1")
            {
                Individual_Recipients.Attributes.Add("class", "col-md-12");
            }
            if (type == "2")
            {

                Condition_Recipients.Attributes.Add("class", "form-group col-md-12");
            }
          
        }
    }

    private void loadCounts()
    {
        string str_Sql = "select (select count(*) from Recipients) as Recipients,(select count(*) from Territory ) as Territory";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataTable dt = this.DA.GetDataTable(cmd);

        lbl_RecipientCount.InnerText = dt.Rows[0]["Recipients"].ToString();
        lbl_TerritoryCount.InnerText = dt.Rows[0]["Territory"].ToString();
    }

    public string getCommissionRecipientType(string commissionkey)
    {
        string str_Sql = "select RecipientsType from Commission where CommissionKey=@CommissionKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DataSet ds = this.DA.GetDataSet(cmd);
        DataTable dt = this.DA.GetDataTable(cmd);
        string RecipientType = "";
        foreach (DataRow dr in dt.Rows)
        {
            RecipientType = dr["RecipientsType"].ToString();
            DdAssignRecipient.Value = dr["RecipientsType"].ToString();
        }
       
        return RecipientType;
    }
   
    private void loadRecipientList()
    {
        string str_Sql = "    select a.RecipientKey, d.Text as Territory, e.text as RepType, concat (b.FirstName, ' ' ,b.LastName) as Manager,  concat (a.FirstName, ' ' ,a.LastName) as RecipientName from Recipients a left join recipients b on convert(uniqueidentifier,a.ReportingManager)=b.RecipientKey join v_Territory d on a.RecipientTerritory=d.Value join v_RecipientCategory e on a.RecipientType=e.Value   order by a.FirstName ";
       
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_RecipientListtr, "CommissionRecipientListTr.txt", "");
    }
    private void loadRecipientCondition()
    {
        PhTemplate PHT = new PhTemplate();

        string recipientCondition = addRecipientCondition("1", "1", PHT);
        ph_RecipientConditionTr.Controls.Add(new LiteralControl(recipientCondition));
    }
    public string addRecipientCondition(string id1, string id2, PhTemplate PHT)
    {
        string str_RecipientCondition = PHT.ReadFileToString("DivCommissionRecipientConditionTr.txt"), str_Replace = "";

        string optionCommissionCondition = this.getHtmlOptions("select * from v_CommissionRecipientCondition", "");
        string optionOperator = this.getHtmlOptions("select * from v_Operators", "");
        string optionCondition = this.getHtmlOptions("select * from v_Condition", "");
        str_Replace = str_RecipientCondition.Replace("%%id1%%", id1);
        str_Replace = str_Replace.Replace("%%id2%%", id2);
        str_Replace = str_Replace.Replace("%%optionRecipientType%%", optionCommissionCondition);
        str_Replace = str_Replace.Replace("%%operator%%", optionOperator);
        str_Replace = str_Replace.Replace("%%condition%%", optionCondition);

        return str_Replace;
    }
    public string getHtmlOptions(string qry, string value)
    {
        DataAccess DA = new DataAccess();
        // string str_Sql = "select a.AgreementName as Text, a.AgreementKey as Value from Agreement a";
        SqlCommand sqlcmd = new SqlCommand(qry);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string option = "";
        foreach (DataRow dr in dt.Rows)
        {
            string selected = "";
            if (dr["Value"].ToString() == value)
            {
                selected = "selected='selected'";
            }
            option += "<option value='" + dr["Value"].ToString() + "' " + selected + ">" + dr["Text"].ToString() + "</option>";
        }
        return option;
    }
    private void loadRecipientListEdit(string commissionkey)
    {
        //recipients (RecipientKey, RecipientId, RecipientType, RecipientTerritory, StartDate, UserKey, Employee, RecipientName, JobCategory, RecipientManager, EndDate, RecipientEligible, Notes, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)

        string str_Sql = "select d.Text as Territory, e.text as RepType, a.RecipientKey,concat (a.FirstName, ' ' ,a.LastName) as RecipientName, a.RecipientType,a.ReportingManager, c.Text as Manager ,a.RecipientTerritory, case when a.RecipientKey=b.RecipientKey then 'checked' else '' end as checked from recipients a left join(select * from CommissionRecipients where CommissionKey=@CommissionKey) b on a.RecipientKey=b.RecipientKey left join v_RecipientManager c on a.ReportingManager=c.Value left join v_Territory d on a.RecipientTerritory=d.Value left join v_RecipientCategory e on a.RecipientType=e.Value order by checked DESC, b.CreatedOn ASC";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_RecipientListtr, "CommissionRecipientListTr.txt", "");
    }
    private void loadRecipientConditionEdit(string commisssionkey)
    {
        string str_Sql = "select * from CommissionRecipientsCondition where CommissionKey=@CommissionKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commisssionkey);
        DataSet ds = this.DA.GetDataSet(cmd);

        string tr = "";
        string str_RecipientConditionTemplate = PHT.ReadFileToString("DivCommissionRecipientConditionEdit.txt"), str_Replace = "";
        int id1 = 1;
        int id2 = 1;
        if (ds.Tables[0].Rows.Count == 0)
        {
            this.loadRecipientCondition();
        }
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            // string trRecipientCondition = this.PHT.ReplaceVariableWithValue(dr, str_RecipientConditionTemplate);
            string trRecipientCondition = str_RecipientConditionTemplate;
            string optionCommissionCondition = this.getHtmlOptions("select * from v_CommissionRecipientCondition", dr["RecipientType"].ToString());
            string optionOperator = this.getHtmlOptions("select * from v_Operators", dr["Operator"].ToString());

            string optionCommissionRole = this.getHtmlOptions("select * from " + dr["RecipientType"].ToString(), dr["Role"].ToString());
            string optionCondition = this.getHtmlOptions("select * from v_Condition", dr["Condition"].ToString());


            trRecipientCondition = trRecipientCondition.Replace("%%optionRecipientType%%", optionCommissionCondition);
            trRecipientCondition = trRecipientCondition.Replace("%%Role%%", optionCommissionRole);
            trRecipientCondition = trRecipientCondition.Replace("%%operator%%", optionOperator);
            trRecipientCondition = trRecipientCondition.Replace("%%condition%%", optionCondition);
            trRecipientCondition = trRecipientCondition.Replace("%%id1%%", id1.ToString());
            trRecipientCondition = trRecipientCondition.Replace("%%id2%%", id2.ToString());
            tr += trRecipientCondition;

            id1 += 1;
        }

        ph_RecipientConditionTr.Controls.Add(new LiteralControl(tr));
    }


    //webmethods

    //for insert the assign recipient

    [WebMethod]
    public static string addCommissionRecipients(string commissionkey, string recipientkey)
    {
        //CommissionRecipients (CommissionRecipientKey, CommissionKey, RecipientKey, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        try
        {
            //   string commissionKey = "CABFB645-3EAE-4CFA-AD22-FBD0EB821F39";
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into CommissionRecipients (CommissionRecipientKey, CommissionKey, RecipientKey, CreatedOn, CreatedBy)"
                                + "select @CommissionRecipientKey, @CommissionKey, @RecipientKey, @CreatedOn, @CreatedBy"; 
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@CommissionRecipientKey", str_Key);
            cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
            cmd.Parameters.AddWithValue("@RecipientKey", recipientkey);

            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    //for add commisssion recipient condition

    [WebMethod]
    public static string addCommissionRecipientCondition(string commissionkey, string RecipientType, string RecipientOperator, string RecipientRole, string RecipientCondition)
    {
        //CommissionRecipientsCondition (RecipientsConditionKey, commissionKey, RecipientType, Operator, Role, Condition, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        try
        {
            //  string commissionKey = "CABFB645-3EAE-4CFA-AD22-FBD0EB821F39";
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into CommissionRecipientsCondition (RecipientsConditionKey, commissionKey, RecipientType, Operator,Role,  Condition,CreatedOn, CreatedBy)"
                                + "select @RecipientsConditionKey, @commissionKey, @RecipientType, @Operator, @Role,  @Condition, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@RecipientsConditionKey", str_Key);
            cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
            cmd.Parameters.AddWithValue("@RecipientType", RecipientType);
            cmd.Parameters.AddWithValue("@Operator", RecipientOperator);
            cmd.Parameters.AddWithValue("@Role", RecipientRole);
            cmd.Parameters.AddWithValue("@Condition", RecipientCondition);

            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string updateCommissionRecipientSelect(string commissionkey, string value)
    {
        DataAccess DA = new DataAccess();
        string str_Sql = "update  commission set RecipientsType=@RecipientsType where commissionKey=@CommissionKey ";


        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("@RecipientsType", value);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DA.ExecuteNonQuery(sqlcmd);
        return "updated";
    }

    [WebMethod]
    public static string deleteExistingRecipients(string commissionkey)
    {
        DataAccess DA = new DataAccess();
        string str_Sql = "delete from commissionRecipientsCondition where commissionKey=@CommissionKey " +
            "delete from CommissionRecipients where commissionKey =@CommissionKey ;" +
            "delete from RecipientPlan where commissionKey =@CommissionKey ";

        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DA.ExecuteNonQuery(sqlcmd);
        return "deleted";
    }

    [WebMethod]
    public static string loadRoleData(string ViewName)
    {
        string value = "";
        DataAccess DA = new DataAccess();
         string str_Sql = "select  Text,  Value from  "+ ViewName +" order by Seq";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string option = "";
        foreach (DataRow dr in dt.Rows)
        {
            string selected = "";
            if (dr["Value"].ToString() == value)
            {
                selected = "selected='selected'";
            }
            option += "<option value='" + dr["Value"].ToString() + "' " + selected + ">" + dr["Text"].ToString() + "</option>";
        }
        return option;
    }

    [WebMethod]
    public static string getCommissionRecipientCondition(string id1, string id2)
    {
        Web_CommissionRecipient commission = new Web_CommissionRecipient();
        PhTemplate PHT1 = new PhTemplate();

        string recipientCOndition = commission.addRecipientCondition(id1, id2, PHT1);
        return recipientCOndition;
    }

    [WebMethod]
    public static string includeConditionedRecipient(string CommissionKey)
    {
        
        DataAccess DA = new DataAccess();
        string str_Sql = "p_IncludeConditionedRecipient";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        sqlcmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataTable dt = DA.GetDataTable(sqlcmd);

        return "Successs";
    }
}