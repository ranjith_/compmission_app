﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" AutoEventWireup="true" CodeFile="CommissionGoalRange.aspx.cs" Inherits="Web_CommissionGoalRange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
      <div class="form-group mt-5">
                                            <!--set goal--->
                                           <div class="row">
                                                <div class="form-group col-md-2">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input">
                                                        Set Goal
                                                    </label>
                                                </div>
                                                    
                                                </div>
                                               
                                                
                                                <div class="form-group col-md-6">
                                                            
                                                            <input placeholder="type to search goal..." type="text" class="form-control required">
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <!--<a data-toggle="collapse" class="btn btn-sm btn-primary" data-parent="" href="#accordion1"><i class="icon-menu"></i></a>-->
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalGoal"> <i class="icon-menu"></i></button>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <a data-toggle="collapse" class="btn btn-default btn-sm bg-light text-dark collapsed" data-parent="" href="#accordion1"><i class="icon-arrow-down22"></i></a>
                                                </div>
                                               
                                                <div class="form-group  col-md-12 mb-3">
                                                     <div id="accordion1" class="panel-collapse collapse ">
                                                    <table class="table bordered custom-panel">
                                                            <thead>
                                                               
                                                               <tr> <th>Period</th><th>Goal Value</th></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                   <td>period 1</td>
                                                                   <td> 10</td>
                                                                </tr>
                                                                <tr>
                                                                   <td>period 2</td>
                                                                   <td> 20</td>
                                                                </tr>
                                                                <tr>
                                                                   <td>period 3</td>
                                                                   <td> 13</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>    
                                                </div>
                                           </div>
                                            <!--/set goal-->
                                            <!--Range matrix--->
                                        <div class="form-group row">
                                                <div class="form-group col-md-2">
                                                    <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input">
                                                       Range Matrix
                                                    </label>
                                                    </div>
                                                    
                                                </div>
                                               
                                                
                                                <div class="form-group col-md-6">
                                                            
                                                           <input placeholder="type to search range matrix..." type="text" class="form-control">
                                                </div>
                                                <div class="form-group col-md-2">
                                                   <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalRange"> <i class="icon-menu"></i></button>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <a data-toggle="collapse" class="btn btn-default btn-sm bg-light text-dark collapsed" data-parent="" href="#accordion2"><i class="icon-arrow-down22"></i></a>
                                                </div>
                                               
                                                <div class="form-group  col-md-12 ">
                                                     <div id="accordion2" class="panel-collapse collapse ">
                                                     <table class="table bordered custom-panel">
                                                            <thead>
                                                               
                                                               <tr> <th>From</th><th>to</th><th> Value</th></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                   <td>12-06-2019</td>
                                                                   <td> 22-06-2019</td>
                                                                   <td>40</td>
                                                                </tr>
                                                                <tr>
                                                                   <td>12-06-2019</td>
                                                                   <td> 22-06-2019</td>
                                                                   <td>40</td>
                                                                </tr>
                                                                <tr>
                                                                   <td>12-06-2019</td>
                                                                   <td> 22-06-2019</td>
                                                                   <td>40</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>    
                                                </div>
                                           </div>
                                          
                                            <!--/ramge matrix-->
                                    </div>


        <!-- Goal modal -->
                <div id="modalGoal" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;Add Goal</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                   <div class="row">
                                           
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Goal Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <table style="background-color: #80808040;"  class="table">
                                                            <thead>
                                                               <tr> <th>Period</th><th>Goal Value</th><th></th></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="period" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="goal" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td></td>
                                                                </tr>
                                                                <tr>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="period" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="goal" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td><button class="btn btn-primary btn-xs"><i class="icon-plus22"></i></button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                            <div class="form-group col-md-12">
                                                    
                                            </div>
                                            <div class=" col-md-12 text-right ">
                                                
                                            </div>
                                            </div> 
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross2 font-size-base mr-1"></i> Close</button>
                                <button class="btn bg-primary"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /goal modal -->
    
                <!-- Range modal -->
                <div id="modalRange" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;Add Range</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                <div class="row">
                                            
                                           
                                            <div class="form-group col-md-12">
                                                    <label>Range Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr> <th>From</th><th>To</th><th>Value</th><th></th></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="from" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="to" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="value" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="from" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="to" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="value" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td><button class="btn btn-primary btn-xs"><i class="icon-plus22"></i></button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>
                                            
                                            </div>
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross2 font-size-base mr-1"></i> Close</button>
                                <button class="btn bg-primary"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Range modal -->
    
</asp:Content>

