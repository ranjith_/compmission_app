﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="Roles.aspx.cs" Inherits="Web_Roles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Roles</title>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
        
            <!--Input Form-->
              <div id="div_InputForm" runat="server" class="col-lg-6 col-md-6">
            <!-- Basic layout-->
                  <div class="card ">
                        <div class="card-header header-elements-inline">
                        <h5 class="card-title">Role Details</h5>
                            <div class="header-elements">
                                <div class="list-icons">
                                <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                <a href="../web/RoleList.aspx" class="btn btn-primary btn-sm">Role List</a>
                                <asp:Button ID="btn_Update" Text="Update" Visible="false"  runat="server" CssClass="btn btn-primary  btn-sm " />
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>          
                                </div>
                            </div>
                        </div>
    
                          <div class="card-body">
                                <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Role Name<span class="text-danger">*</span></label>
                                    <input type="text" id="txt_RoleName" runat="server" required class="form-control" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Role Desc</label>
                                    <input type="text" id="txt_RoleDesc" runat="server"  class="form-control" />
                                </div>
                                <div class="form-group col-md-6">
                                        <label>System Role  </label>
                                       
                                        <div class="form-check">
	                                        <label class="form-check-label">
		                                        <input id="ch_IsSystemRole"  runat="server" type="checkbox" class="form-check-input">
		                                        Is System Role
	                                        </label>
                                        </div>
                                </div>
                                
                                </div>
                         </div>
                  </div>
                               
            <!-- /basic layout -->
              </div>
            <!--Input Form-->
            <!--View Data-->
             <div id="div_MenuAccess" runat="server"  class="col-lg-6 col-md-6">
            <!-- Basic layout-->
                    <div class="card ">
                            
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Select Menu Access</h5>
                            <div class="header-elements">
                                <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>          
                                </div>
                            </div>
                     </div>
                     <div class="card-body">
                         <asp:TreeView ID="tree_Menu" runat="server" ShowCheckBoxes="All"></asp:TreeView>
                     </div>           
				              
                    </div>
                                
            </div>
            <!--view data-->
        </div>
    </div>
</asp:Content>

