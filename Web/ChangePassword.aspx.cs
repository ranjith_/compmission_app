﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Web_ChangePassword : System.Web.UI.Page
{
    string str_CredentialKey = "", OldPassword="";
    DataAccess DA;
    SessionCustom SC;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.SC = new SessionCustom(true);
        this.DA = new DataAccess();
        DataRow dr = this.SC.UserRecord;
        OldPassword = dr["Password"].ToString();

    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
       
        if(OldPassword != pwd_OldPassword.Value)
        {
            lbl_err.Text = "Invalid Old Password";
        }
        else if (pwd_NewPassword.Value.ToString() != pwd_ConfirmPassword.Value.ToString())
        {
            lbl_err.Text = "New password and confirm password must be same";
        }
        else
        {
            DataRow dr = this.SC.UserRecord;
            String str_sql = "update Credential set Password=@Password where CredentialKey=@CredentialKey";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@Password", pwd_NewPassword.Value);
            cmd.Parameters.AddWithValue("@CredentialKey", dr["CredentialKey"].ToString());
            this.DA = new DataAccess();
            this.DA.ExecuteNonQuery(cmd);
            ClientScript.RegisterStartupScript(this.GetType(), "Globex Inc.", "<script>alert('Password Changed Successfully.')</script>");
            Response.Redirect("../Web/Logout.aspx");
        }
       
    }
}