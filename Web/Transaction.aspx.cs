﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Transaction : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        loadTransList();

       

        if (Request.QueryString["trans_key"] != null)
        {
            //viewTransDetails(Request.QueryString["trans_key"]);
            //div_TransactionDetails.Visible = true;
            //div_TransactionDetails.Attributes["class"] = "col-lg-6 col-md-6";
           
        }
        if (!IsPostBack)
        {
           

            if (Request.QueryString["trans_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadTransDetailToEdit(Request.QueryString["trans_key"]);
                div_TransactionDetails.Attributes["class"] = "col-lg-6 col-md-6 d-none";
             
            }
        }

    }

  

    
    [WebMethod]
    public static string viewTransaction(string trans_key)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select TransactionKey,TransactionLine,convert(varchar(10),EffectiveDate, 101) as effectivedate,convert(varchar(10),TransactionDate, 101) as transactiondate,TransactionType,TransactionLineType,Customer,SalesRep1,SalesRep2,Product,SalesAmount,"
                + "TransactionLineLevel,SoNumber,PaymentType,BillNumber,TransactionId,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn from transactions where TransactionKey =@TransactionKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@TransactionKey", trans_key);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivTransactionView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    private void loadTransList()
    {
        string str_Sql = "select a.*, b.ProductName, c.CustomerName, convert(varchar(10),a.EffectiveDate, 101) as effective_Date from Transactions a left join Products b on convert(uniqueidentifier,a.Product)=b.ProductKey left join  Customers c on convert(uniqueidentifier,a.Customer)=c.CustomerKey order by a.CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, tbodyTransaction, "Transactions.txt", "");
    }

    private void viewTransDetails(string trans_key)
    {

        string str_Sql_view = "select TransactionKey,TransactionLine,convert(varchar(10),EffectiveDate, 101) as effectivedate,convert(varchar(10),TransactionDate, 101) as transactiondate,TransactionType,TransactionLineType,Customer,SalesRep1,SalesRep2,Product,SalesAmount,"
               + "TransactionLineLevel,SoNumber,PaymentType,BillNumber,TransactionId,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn from transactions where TransactionKey =@TransactionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@TransactionKey", trans_key);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_Transaction_Details, "DivTransactionView.txt", "");
        
    }

    private void loadTransDetailToEdit(string trans_key)
    {

        string str_Sql_view = "select TransactionKey,TransactionLine,format(EffectiveDate, 'yyyy-MM-dd') as effectivedate,format(TransactionDate, 'yyyy-MM-dd') as transactiondate,TransactionType,TransactionLineType,Customer,SalesRep1,SalesRep2,Product,SalesAmount,"
                + "TransactionLineLevel,SoNumber,PaymentType,BillNumber,TransactionId,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn from transactions where TransactionKey =@TransactionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@TransactionKey", trans_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            //txt_TransactionId.Value = dr["transactionid"].ToString();
            //txt_TransactionLine.Value = dr["transactionline"].ToString();
            //txt_EffectiveDate.Value = dr["effectivedate"].ToString();
            //txt_TransactionDate.Value = dr["transactionDate"].ToString();
            //dd_TransactionType.Value = dr["transactiontype"].ToString();
            //dd_TransactionLineType.Value = dr["transactionlinetype"].ToString();
            //txt_Customer.Value = dr["customer"].ToString();
            //txt_SalesRep1.Value = dr["salesrep1"].ToString();
            //txt_SalesRep2.Value = dr["salesrep2"].ToString();
            //txt_Product.Value = dr["product"].ToString();
            //txt_SalesAmount.Value = dr["salesamount"].ToString();
            //dd_TransactionLineLevel.Value = dr["transactionlinelevel"].ToString();
            //txt_SoNumber.Value = dr["sonumber"].ToString();
            //dd_PaymentType.Value = dr["paymenttype"].ToString();
            //txt_BillNumber.Value = dr["billnumber"].ToString();

        }

    }
    [WebMethod]
    public static string deleteTransaction(string TransKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Transactions where TransactionKey=@TransactionKey;" +
                "Delete from TransactionCustom where TransactionKey=@TransactionKey";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@TransactionKey", TransKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }



    protected void update_Click(object sender, EventArgs e)
    {
       
    }

    protected void btn_Excel_ServerClick(object sender, EventArgs e)
    {
        string str_Sql = "select transaction_id , transaction_line, convert(varchar(10), effective_date, 103) as effectivedate,convert(varchar(10), transaction_date, 103) as transactiondate,transaction_type,transaction_line_type,customer,sales_rep1,sales_rep2,product,sales_amount," +
                         " transaction_line_level,so_number,payment_type,bill_number,convert(varchar(10), CreatedOn, 103) as CreatedOn from transactions ";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);

        DataTable dt = this.DA.GetDataTable(sqlcmd);

        DateTime time = DateTime.Now;
        string CurrentDate_Time = time.ToString("MM/dd/yyyy hh:mm:ss");
        this.OCF.ExporttoExcel(dt, "TransactionExcel" + CurrentDate_Time + "");
    }
}