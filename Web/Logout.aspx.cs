﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Web_Logout : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();

        try
        {
            if (this.SC.UserKey != null)
            {
                string str_DeleteSessionId = "delete from UserActiveSession where UserKey = @UserKey and SessionId=@SessionId";
                SqlCommand cmd = new SqlCommand(str_DeleteSessionId);
                cmd.Parameters.AddWithValue("@UserKey", SC.UserKey);
               // cmd.Parameters.AddWithValue("@SessionId", SC.EmployeeSessionId);
                DA.ExecuteNonQuery(cmd);
            }
        }
        catch (Exception exe)
        {

        }


        Session.Clear();
        Session.Abandon();
        new SessionCustom().NavigateToLoginPage();
    }
}