﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Currency : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        loadCurrencyList();

        

        if (Request.QueryString["currency_key"] != null)
        {
            viewCurrencyDetails(Request.QueryString["currency_key"]);
            div_CurrencyDetails.Visible = true;
            div_CurrencyDetails.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Visible = false;
        }

        if (!IsPostBack)
        {
            if (Request.QueryString["currency_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadCurrencyDetailToEdit(Request.QueryString["currency_key"]);
                div_CurrencyDetails.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }

    private void loadCurrencyDetailToEdit(string CurrencyKey)
    {

        //(CurrencyKey, CurrencyCode, CurrencyName, Rate, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql_view = "select * from Currency where CurrencyKey =@CurrencyKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CurrencyKey", CurrencyKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            txt_CurrencyCode.Value = dr["CurrencyCode"].ToString();
            txt_CurrencyName.Value = dr["CurrencyName"].ToString();
            txt_Rate.Value = dr["Rate"].ToString();


        }
    }

    private void viewCurrencyDetails(string CurrencyKey)
    {
        string str_Sql_view = "select *, convert(varchar(10),CreatedOn, 101) as Created_On from Currency where CurrencyKey =@CurrencyKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CurrencyKey", CurrencyKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_CurrencyDetails, "DivCurrencyView.txt", "");
    }

    private void loadCurrencyList()
    {
        string str_Sql = "select *, convert(varchar(10),CreatedOn, 101) as Created_On from Currency order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_CurrencyListTr, "CurrencyListTr.txt", "");
    }
    [WebMethod]
    public static string viewCurrency(string CurrencyKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select *, convert(varchar(10),CreatedOn, 101) as Created_On from Currency where CurrencyKey =@CurrencyKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@CurrencyKey", CurrencyKey);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivCurrencyView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string deleteCurrency(string CurrencyKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Currency where CurrencyKey=@CurrencyKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@CurrencyKey", CurrencyKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string ValidateCurrencyCode(string CurrencyCode)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "select *  from Currency where CurrencyCode=@CurrencyCode ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@CurrencyCode", CurrencyCode);
            DataTable dt = DA.GetDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                return "0";
            }
            else
            {
                return "1";
            }

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    protected void btn_Save_Click(object sender, EventArgs e)
    {
        //(CurrencyKey, CurrencyCode, CurrencyName, Rate, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";

        
        string str_Key = Guid.NewGuid().ToString();
        
        string str_Sql = "insert into Currency (CurrencyKey, CurrencyCode, CurrencyName, Rate, CreatedOn, CreatedBy)"
                            + "select @CurrencyKey, @CurrencyCode, @CurrencyName, @Rate, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CurrencyKey", str_Key);
        cmd.Parameters.AddWithValue("@CurrencyCode", txt_CurrencyCode.Value);
        cmd.Parameters.AddWithValue("@CurrencyName", txt_CurrencyName.Value);
        cmd.Parameters.AddWithValue("@Rate", txt_Rate.Value);
        cmd.Parameters.AddWithValue("@CreatedBy", userKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);
       
        Response.Redirect(Request.RawUrl);
       
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string CurrencyKey = Request.QueryString["currency_key"];

        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";
        //  string str_Sql = "UPDATE transactions set transaction_id=@transaction_id, transaction_line=@transaction_line,  where transaction_key=@transaction_key  ";

        string str_Sql = "UPDATE Currency set CurrencyCode=@CurrencyCode, CurrencyName=@CurrencyName, Rate=@Rate, ModifiedBy=@ModifiedBy where CurrencyKey=@CurrencyKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);


        cmd.Parameters.AddWithValue("@CurrencyCode", txt_CurrencyCode.Value);
        cmd.Parameters.AddWithValue("@CurrencyName", txt_CurrencyName.Value);
        cmd.Parameters.AddWithValue("@Rate", txt_Rate.Value);
       
        cmd.Parameters.AddWithValue("@ModifiedBy", userKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@CurrencyKey", CurrencyKey);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/Currency.aspx?currency_key=" + CurrencyKey + "");
    }
}