﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_TransactionCustomConfig : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        if (!IsPostBack)
        {
            loadDataType();
            loadDdlOption();
            loadCustomColumnList(this.Com.GetQueryStringValue("PrimaryTableName"));
            hdn_ParentTable.Value = this.Com.GetQueryStringValue("PrimaryTableName");
        }
    }

    private void loadCustomColumnList(string ParentTable)
    {
        string str_Sql = "select a.*, b.Name as DataTypeName, (case When a.FieldType=2 then 'DropDownList' Else 'TextBox' End ) as FieldTypeName from TableCustomConfig a join IconfigDetails b on a.DataType=b.ConfigDetailKey where PrimaryTableName= '"+ ParentTable + "' order by seq";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_CustomColumnList, "CustomColumnListTr.txt", "");
    }

    private void loadDdlOption()
    {
        string str_Select = "select *, ConfigName as Text, ConfigKey as Value from IConfig where ConfigText1='TransactionCustomDropdown'";
        SqlCommand cmm = new SqlCommand(str_Select);

        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_DdlList, ds.Tables[0], "Text", "Value", true, "--Select Dropdown--");

      
    }

    private void loadDataType()
    {
        string str_Select = "select Value,  Name as Text  from v_DataTypes  order by seq ";
        SqlCommand cmm = new SqlCommand(str_Select);

        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_DataType, ds.Tables[0], "Text", "Value", true, "--Select Datatype Scope--");
    }

    protected void btn_Save_ServerClick(object sender, EventArgs e)
    {
        //TransactionCustomConfig (TnxConfigKey,  FieldType,  LableName,  ColumnName,  DataType,  DdlKey,  IsActive,  CompanyKey,  CreatedOn,  CreatedBy,  ModifiedOn,  ModifiedBy)
        string str_Key = Guid.NewGuid().ToString();

        string PrimaryTableName = this.Com.GetQueryStringValue("PrimaryTableName");
        string str_Sql = "insert into TableCustomConfig  (TnxConfigKey,PrimaryTableName,  FieldType,  LableName,  ColumnName,  DataType,  DdlKey, Seq, IsRequired, CreatedBy,CreatedOn)"
                            + "select @TnxConfigKey, @PrimaryTableName, @FieldType,  @LableName,  @ColumnName,  @DataType,  @DdlKey, @Seq, @IsRequired, @CreatedBy,@CreatedOn; " +
                            "alter table "+PrimaryTableName+ "Custom  add " + txt_ColumnName.Value.ToString()+" "+ ddl_DataType.SelectedItem.Text + " null";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@TnxConfigKey", str_Key);
        cmd.Parameters.AddWithValue("@PrimaryTableName", PrimaryTableName);
        cmd.Parameters.AddWithValue("@FieldType", ddl_FieldType.SelectedValue);
        cmd.Parameters.AddWithValue("@LableName", txt_LabelName.Value);
        cmd.Parameters.AddWithValue("@ColumnName", txt_ColumnName.Value.ToString());
        cmd.Parameters.AddWithValue("@DataType", ddl_DataType.SelectedValue);
        cmd.Parameters.AddWithValue("@Seq", txt_Sequence.Value);
        if (chb_IsRequired.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsRequired", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsRequired", "0");
        }

        if (ddl_FieldType.SelectedValue == "1")
        {

            cmd.Parameters.AddWithValue("@DdlKey",DBNull.Value);
        }
        else
        {

            cmd.Parameters.AddWithValue("@DdlKey", ddl_DdlList.SelectedValue);
        }

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        cmd.Parameters.AddWithValue("@createdby", SC.GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }
    [WebMethod]
    public static string deleteField(string ParentTable, string FieldKey, string ColumnName)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from TableCustomConfig where TnxConfigKey=@TnxConfigKey;" +
                "Alter table "+ ParentTable + "Custom drop Column " + ColumnName;
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@TnxConfigKey", FieldKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}