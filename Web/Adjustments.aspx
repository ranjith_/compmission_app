﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="Adjustments.aspx.cs" Inherits="Web_Adjustments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Adjustments</title>
    <script type="text/javascript">

        function viewAdjustment(adjust_key) {
         //   alert(adjust_key);
            $.ajax({
                type: "POST",
                url: "../Web/Adjustments.aspx/viewAdjustment",
                data: "{adjust_key:'" + adjust_key + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    // alert(data.d);
                    $('#card-adjustment').html(data.d);
                    $('#div_InputForm').addClass('d-none');
                    $('#div_AdjustmentDetails').removeClass('d-none');

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }

        function deleteAdjustment(adjust_key, adjust_name) {
            var confirmation = confirm("are you sure want to delete this " + adjust_name + " Adjustment ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/Adjustments.aspx/deleteAdjustment",
                    data: "{AdjustKey:'" + adjust_key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Adjustment " + adjust_name + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-6 col-md-6">
                                    <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Adjustments
                                    
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                        <a href="Adjustments.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                   
                           
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Adjustment Name</th>
                                        <th>Recipient Name</th>
                                        <th> Type</th>
                                        <th>Description </th>
                                        <th>Start Date</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_AdjustmentsListTr" runat="server"></asp:PlaceHolder>
                                        
                                </tbody>
                            </table>
                    </div>
                                 </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                 <!--Input Form-->
                                 <div id="div_InputForm" runat="server"  class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                            <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Add Adjustments</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                         <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Adjustment Name<span class="text-danger">*</span></label>
                                                <input id="txt_AdjustmentName" required runat="server" type="text" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Recipient Name<span class="text-danger">*</span></label>
                                                <input id="txt_RecipientName" required runat="server" type="text" class="form-control" >
                                            </div>
                                            
                                            <div class="form-group col-md-6">
                                                <label>Recipient Type</label>
                                                <select id="dd_RecipientType" runat="server" required class="form-control">
                                                <option value="">-- select type --</option>
                                                    <option value="Add">Add</option>
                                                    <option value="Reduce">Reduce</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Start Date<span class="text-danger">*</span></label>
                                                <input id="dp_StartDate" runat="server" type="date" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>End Date<span class="text-danger">*</span></label>
                                                <input id="dp_EndDate" runat="server" type="date" class="form-control" >
                                            </div>
                                            
                                            <div class="form-group col-md-6">
                                                    <label>Description  <span class="text-danger">*</span></label>
                                                    <textarea id="txt_AdjustmentDesc" runat="server" type="text" class="form-control "  row="2"> </textarea>
                                            </div>
                                            
                            </div>
                                        </div>
                                    </div>
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->

                                 <!--View Data-->
                                 <div id="div_AdjustmentDetails" runat="server"  class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card" id="card-adjustment">
                            
                                <asp:PlaceHolder ID="ph_AdjustmentDetails" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
                            </div>
                        </div>
    
</asp:Content>

