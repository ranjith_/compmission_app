﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="AuditLogConfig.aspx.cs" Inherits="Web_AuditLogConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Audit Log</title>
     <script type="text/javascript">

        function viewAuditLog(audit_key) {
           // alert(audit_key);
            $.ajax({
                type: "POST",
                url: "../Web/AuditLogConfig.aspx/viewAuditLog",
                data: "{audit_key:'" + audit_key + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    // alert(data.d);
                    $('#card-audit-log').html(data.d);
                    $('#div_InputForm').addClass('d-none');
                    $('#div_AduitLog').removeClass('d-none');

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }

         function deleteAuditLog(adjust_key, adjust_name) {
            var confirmation = confirm("are you sure want to delete this " + adjust_name + " Adjustment ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/Adjustments.aspx/deleteAdjustment",
                    data: "{AdjustKey:'" + adjust_key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Adjustment " + adjust_name + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
      <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-6 col-md-6">
                                    <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Audit Log
                                    
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                        <a href="AuditLogConfig.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                   
                           
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Log Name</th>
                                        <th>Table Name</th>
                                        
                                        <th> Created On</th>
                                        <th> Created By</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_AuditLogListTr" runat="server"></asp:PlaceHolder>
                                        
                                </tbody>
                            </table>
                    </div>
                                 </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                 <!--Input Form-->
                                 <div id="div_InputForm" runat="server"  class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                            <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Add Audit Log</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                         <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Log Name<span class="text-danger">*</span></label>
                                                <input id="txt_AuditLogName" runat="server" type="text" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Table Name<span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddl_TableName" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                             <div class="form-group col-md-3">
                                                 Add Log on
                                             </div>
                                            <div class="form-group col-md-3">
                                               <div class="form-check">                               
                                                    <label class="form-check-label">
                                                    <input id="chb_Insert"  runat="server" type="checkbox" class="form-check-input" />
                                                    INSERT
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                               <div class="form-check">                               
                                                    <label class="form-check-label">
                                                    <input id="chb_Update"  runat="server" type="checkbox" class="form-check-input" />
                                                    UPDATE
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                               <div class="form-check">                               
                                                    <label class="form-check-label">
                                                    <input id="chb_Delete"  runat="server" type="checkbox" class="form-check-input" />
                                                    DELETE
                                                    </label>
                                                </div>
                                            </div>
                                            
                                            
                            </div>
                                        </div>
                                    </div>
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->

                                 <!--View Data-->
                                 <div id="div_AduitLog" runat="server"  class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card" id="card-audit-log">
                            
                                  <asp:PlaceHolder ID="ph_AuditLogDetails" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
                            </div>
                        </div>
    
</asp:Content>

