﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Diagnostics;
using iTextSharp.text.html.simpleparser;

public partial class Web_CommissionList : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;

   
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        loadRecipientList();
        setDefaultAgreement();
      
        ///MakePDf("dasfszdf", "<div class='ddd'>dsfsdgvfssd rsgrdgdrthd fg hhfg hfg hgyjzfzdf</div>");
    }

    private void setDefaultAgreement()
    {
        string str_Sql = "update RecipientPlan set AgreementKey=(select AgreementKey from Agreement where IsDefault=1) where AgreementKey is NULL";
        SqlCommand cmd = new SqlCommand(str_Sql);
        this.DA.ExecuteNonQuery(cmd);
    }

    
    private void loadRecipientList()
    {
        string str_Sql = "select distinct(a.RecipientKey),b.recipient_name,b.recipient_territory, b.recipient_id,b.recipient_type from CommissionRecipients a left outer join Recipients b on a.RecipientKey=b.recipient_key;"
            + "select *, b.CommissionRecipientKey from Commission a   join CommissionRecipients b on a.CommissionKey=b.CommissionKey;" +
            "select *, FORMAT (StartDate, 'yyyy-MM-dd') as sDate,FORMAT (EndDate, 'yyyy-MM-dd') as eDate from RecipientPlan";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        //this.PHT.LoadGridItem(ds, ph_CommissionRecipientCollapsibleTr, "CollapsibleRecipientCommissionTr.txt", "");
        string str_Template = this.PHT.ReadFileToString("CollapsibleRecipientCommissionTr.txt"), str_HTML = "", str_Replace = "";
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            //str_Replace = str_Template;
            //str_Replace = str_Replace.Replace("%%recipient_name%%", dr["recipient_name"].ToString());
            //str_Replace = str_Replace.Replace("%%RecipientKey%%", dr["RecipientKey"].ToString());
            str_Replace = this.PHT.ReplaceVariableWithValue(dr, str_Template);
            str_Replace = str_Replace.Replace("%%optionAgreement%%", loadOptionAgreement());
            str_Replace = str_Replace.Replace("%%optionAdjustment%%", loadOptionAdjustment());
            string str_RecipientKey = dr["RecipientKey"].ToString();
            string str_RecipientCommissionTemplate = this.PHT.ReadFileToString("DivRecipientCommissionListTr.txt") ;
            string str_RecipientCommissionList = "";
            foreach (DataRow dr1 in ds.Tables[1].Select("RecipientKey='" + str_RecipientKey + "'"))
            {
                str_RecipientCommissionList += this.PHT.ReplaceVariableWithValue(dr1, str_RecipientCommissionTemplate);
                //str_Replace = this.PHT.ReplaceVariableWithValue(dr1, str_Replace);
            }
            str_Replace = str_Replace.Replace("%%str_RecipientCommissionList%%", str_RecipientCommissionList);

            

            foreach(DataRow dr2 in ds.Tables[2].Select("RecipientKey='" + str_RecipientKey + "'"))
            {

                str_Replace = this.PHT.ReplaceVariableWithValue(dr2, str_Replace);
                string adjustmentSelect = "value='"+dr2["AdjustmentKey"]+"'";
                str_Replace = str_Replace.Replace(adjustmentSelect, adjustmentSelect+" selected='selected'");
                string agreementSelect = "value='" + dr2["AgreementKey"] + "'";
                str_Replace = str_Replace.Replace(agreementSelect, agreementSelect + " selected='selected'");
            }
            str_HTML += str_Replace;
           GetAgreement(str_RecipientKey);

        }
        ph_CommissionRecipientCollapsibleTr.Controls.Add(new LiteralControl(str_HTML));
    }
    [WebMethod]
    public static string viewAgreement(string recipient_key)
    {
        try
        {
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            PhTemplate PHT = new PhTemplate();
            string str_Sql = "select a.PlanKey, a.RecipientKey, a.SpecificTerms,a.AgreementKey, b.Headers, b.Footer, b.DocTitle, b.IncludeCommission,b.IncludeRecipient from RecipientPlan a join Agreement b on a.AgreementKey=b.AgreementKey where a.RecipientKey=@recipient_key;"
           + "select *, b.CommissionRecipientKey,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) Count from Commission a   join CommissionRecipients b on a.CommissionKey=b.CommissionKey where RecipientKey=@recipient_key";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@recipient_key", recipient_key);
            DataSet ds = DA.GetDataSet(cmd);
            //this.PHT.LoadGridItem(ds, ph_CommissionRecipientCollapsibleTr, "CollapsibleRecipientCommissionTr.txt", "");
            string str_Template = PHT.ReadFileToString("DivCommissionAgreement.txt"), str_HTML = "", str_Replace = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //str_Replace = str_Template;
                //str_Replace = str_Replace.Replace("%%recipient_name%%", dr["recipient_name"].ToString());
                //str_Replace = str_Replace.Replace("%%RecipientKey%%", dr["RecipientKey"].ToString());
                str_Replace = PHT.ReplaceVariableWithValue(dr, str_Template);

                string str_RecipientKey = dr["RecipientKey"].ToString();
                string str_RecipientCommissionTemplate = PHT.ReadFileToString("DivRecipientsCommissionForAgreement.txt");
                string str_RecipientCommissionList = "";
                foreach (DataRow dr1 in ds.Tables[1].Select("RecipientKey='" + str_RecipientKey + "'"))
                {
                    str_RecipientCommissionList += PHT.ReplaceVariableWithValue(dr1, str_RecipientCommissionTemplate);
                    //str_Replace = this.PHT.ReplaceVariableWithValue(dr1, str_Replace);
                }
                str_Replace = str_Replace.Replace("%%DivRecipientsCommissionForAgreement%%", str_RecipientCommissionList);
                str_HTML += str_Replace;

            }

            return str_HTML;
        }
        catch (Exception e)
        {
            return e.ToString();
        }
       
    }
    private string loadOptionAgreement()
    {
        string str_Sql = "select a.AgreementName as Text, a.AgreementKey as Value from Agreement a";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string optionRole = "";
        foreach (DataRow dr in dt.Rows)
        {

            optionRole += "<option value='" + dr["Value"].ToString() + "'>" + dr["Text"].ToString() + "</option>";
        }
        return optionRole;
    }
    private string loadOptionAdjustment()
    {
        string str_Sql = "select a.AdjustmentName as Text, a.AdjustmentKey as Value from adjustments a";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string optionRole = "";
        foreach (DataRow dr in dt.Rows)
        {

            optionRole += "<option value='" + dr["Value"].ToString() + "'>" + dr["Text"].ToString() + "</option>";
        }
        return optionRole;
    }

    [WebMethod]
    public static string updateRecipientPlan(string PlanKey, string startdate, string enddate, string adjustment, string agreement, string desc)
    {
       // RecipientPlan(PlanKey, RecipientKey, StartDate, EndDate, AdjustmentKey, AgreementKey, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        try
        {

            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "Update RecipientPlan set   StartDate=@StartDate, EndDate=@EndDate, AdjustmentKey=@AdjustmentKey, AgreementKey=@AgreementKey, SpecificTerms=@SpecificTerms,  ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn   where PlanKey=@PlanKey";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@PlanKey", PlanKey);
            cmd.Parameters.AddWithValue("@StartDate", startdate);
            cmd.Parameters.AddWithValue("@EndDate", enddate);
            cmd.Parameters.AddWithValue("@AdjustmentKey", adjustment);

            cmd.Parameters.AddWithValue("@SpecificTerms", desc);
            cmd.Parameters.AddWithValue("@AgreementKey", agreement);
            cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    [Obsolete]
    public void MakePDf(string recipientKey,string str_DocTitle,string doc, bool showSignature, string str_signature)
    {
        Document document = new Document();
        PdfWriter.GetInstance(document, new FileStream(Request.PhysicalApplicationPath + "\\UploadFile\\Agreement\\CommissionAgreement-" + recipientKey+".pdf", FileMode.Create));
        document.Open();
        Paragraph p = new Paragraph(str_DocTitle);
        p.Alignment = Element.ALIGN_CENTER;
        document.Add(p);
        iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(Request.PhysicalApplicationPath + "\\UploadFile\\Logo\\db082fef-7245-4d2b-a154-87b3e5b6b8bd_Bada_Logo.jpg");
        img.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
        img.ScaleToFit(100, 150);
        document.Add(img);
        iTextSharp.text.html.simpleparser.HTMLWorker hw =
                     new iTextSharp.text.html.simpleparser.HTMLWorker(document);
        hw.Parse(new StringReader(doc));
        if (showSignature == true)
        {
            Paragraph p1 = new Paragraph("I Agree the  Terms and Conditions gives you the right to terminate the access of abusive users or to terminate the access to users who do not follow your rules and guidelines, as well as other desirable business benefits. ");
            p.Alignment = Element.ALIGN_CENTER;
            document.Add(p1);
        }
        iTextSharp.text.html.simpleparser.HTMLWorker hw1 =
                    new iTextSharp.text.html.simpleparser.HTMLWorker(document);
        hw1.Parse(new StringReader(str_signature));
        document.Close();
    }

    public void GetAgreement( string recipient_key)
    {
        try
        {
            
            string str_Sql = "select a.PlanKey, a.RecipientKey, a.SpecificTerms,a.AgreementKey, b.Headers, b.Footer, b.DocTitle, b.IncludeCommission,b.IncludeRecipient, b.showSignature from RecipientPlan a join Agreement b on a.AgreementKey=b.AgreementKey where a.RecipientKey=@recipient_key;"
           + "select *, b.CommissionRecipientKey,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) Count from Commission a   join CommissionRecipients b on a.CommissionKey=b.CommissionKey where RecipientKey=@recipient_key;" +
           "SELECT *, FORMAT(B.StartDate, 'MM/dd/yyyy') as sDate, FORMAT(B.EndDate, 'MM/dd/yyyy') as eDate FROM recipients A JOIN RecipientPlan B ON A.recipient_key= B.RecipientKey";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@recipient_key", recipient_key);
            DataSet ds = DA.GetDataSet(cmd);
            //this.PHT.LoadGridItem(ds, ph_CommissionRecipientCollapsibleTr, "CollapsibleRecipientCommissionTr.txt", "");
            string str_Template = PHT.ReadFileToString("DivCommissionAgreement.txt"), str_HTML = "", str_Replace = "";
            string str_PlanSignature = PHT.ReadFileToString("DivPlanSignature.txt");
            string str_DocTitle = "", str_signature="";
            bool showSignature = false;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //str_Replace = str_Template;
                //str_Replace = str_Replace.Replace("%%recipient_name%%", dr["recipient_name"].ToString());
                //str_Replace = str_Replace.Replace("%%RecipientKey%%", dr["RecipientKey"].ToString());
                str_Replace = PHT.ReplaceVariableWithValue(dr, str_Template);
               
                string str_RecipientKey = dr["RecipientKey"].ToString();
                str_DocTitle= dr["DocTitle"].ToString();
                showSignature = Convert.ToBoolean(dr["showSignature"].ToString());
                string str_RecipientCommissionTemplate = PHT.ReadFileToString("DivRecipientsCommissionForAgreement.txt");

               
                string str_RecipientCommissionList = "";
                foreach (DataRow dr1 in ds.Tables[1].Select("RecipientKey='" + str_RecipientKey + "'"))
                {
                    str_RecipientCommissionList += PHT.ReplaceVariableWithValue(dr1, str_RecipientCommissionTemplate);
                    //str_Replace = this.PHT.ReplaceVariableWithValue(dr1, str_Replace);
                }
                str_Replace = str_Replace.Replace("%%DivRecipientsCommissionForAgreement%%", str_RecipientCommissionList);
                foreach (DataRow dr2 in ds.Tables[2].Select("RecipientKey='" + str_RecipientKey + "'"))
                {
                    // str_RecipientCommissionList += PHT.ReplaceVariableWithValue(dr2, str_RecipientCommissionTemplate);
                    //str_Replace = this.PHT.ReplaceVariableWithValue(dr1, str_Replace);
                    str_Replace = str_Replace.Replace("@#RECIPIENT#@", dr2["recipient_name"].ToString());
                    str_Replace = str_Replace.Replace("@#STARTDATE#@", dr2["sDate"].ToString());
                    str_Replace = str_Replace.Replace("@#ENDDATE#@", dr2["eDate"].ToString());
                    str_signature = PHT.ReplaceVariableWithValue(dr2, str_PlanSignature);
                }
                str_HTML += str_Replace;

            }

            MakePDf(recipient_key, str_DocTitle, str_HTML, showSignature, str_signature) ;
        }
        catch (Exception e)
        {
           // return e.ToString();
        }
    }

    [Obsolete]
    protected void checkPdf_ServerClick(object sender, EventArgs e)
    {
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Panel.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter);
        div_AgreementView.RenderControl(htmlTextWriter);
        StringReader stringReader = new StringReader(stringWriter.ToString());
        Document Doc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
        //  Document document = new Document();
        HTMLWorker htmlparser = new HTMLWorker(Doc);
        PdfWriter.GetInstance(Doc, new FileStream(Request.PhysicalApplicationPath + "\\CommissionAgreement.pdf", FileMode.Create));
        

       
       // PdfWriter.GetInstance(Doc, Response.OutputStream);
        Doc.Open();
        htmlparser.Parse(stringReader);
        Doc.Close();
        Response.Write(Doc);
        Response.End();
    }
}