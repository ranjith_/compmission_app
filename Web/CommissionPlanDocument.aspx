﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionPlanDocument.aspx.cs" Inherits="Web_CommissionPlanDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission Plan Document</title>
      <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
 <script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>

        <script>
        
        function ExportPdf() {
           
            kendo.drawing
                .drawDOM("#div_AgreementView",
                    {
                        paperSize: "A4",
                        margin: { top: "1cm", bottom: "1cm", left: "1cm", right: "1cm" },
                        scale: 0.5,
                        height: 500
                    })
                .then(function (group) {
                    kendo.drawing.pdf.saveAs(group, "CommissionPlanDocument.pdf")
                });
            
        }
</script>
    <style>
        .Document-Shadow{
            box-shadow: 1px 2px 15px 0px rgba(0, 0, 0, 0.12);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="ROW">
            <div class="col-lg-10 col-md-10 mx-auto">
                                        <!--Data print-->
                                        <div class="card Document-Shadow ">
                                            <div class="card-header header-elements-inline" style="background-color: #292222;">
                                                <h5 class="card-title text-white">Plan <small></small></h5>
                                                <div class="header-elements ">
                                                    <div class="list-icons">
                                                       
                                                        <button type="button" onclick="ExportPdf();" class="btn btn-light btn-sm ml-3"><i class="icon-file-pdf mr-2"></i> PDF</button>
                                                   <%--     <a class="list-icons-item" data-action="collapse"></a>
                                                        <a class="list-icons-item" data-action="reload"></a>--%>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                 <div id="div_AgreementView" runat="server" clientIdMode="static" class="">
                                                <asp:PlaceHolder ID="doc" runat="server"></asp:PlaceHolder>
                                          <%--     <embed id="AgreementViewPDF" runat="server" src="" width="100%" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">--%>
                                               <%-- <embed id="CommissionPlanPdf" runat="server" src="" width="100%" height="800" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html" />--%>
                                            </div>
                                            </div>
                                           
                                        </div>
                                         <!--Data print-->
                                </div>
        </div>
    </div>
</asp:Content>

