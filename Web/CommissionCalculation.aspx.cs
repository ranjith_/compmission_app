﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionCalculation : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        if (!IsPostBack)
        {
            loadProcessFrequency();
            loadCommissionDetails(Com.GetQueryStringValue("CommissionKey"));
            loadSchedule(Com.GetQueryStringValue("ScheduleCalculationKey"));
            ProcessPeriodFrequencyList(Com.GetQueryStringValue("ScheduleCalculationKey"));
        }
       
    }

    private void loadSchedule(string ScheduleCalculationKey)
    {
        string str_Select = "select * from SchedulePlanCalculation where ScheduleCalculationKey=@ScheduleCalculationKey";
        SqlCommand cmd = new SqlCommand(str_Select);
        cmd.Parameters.AddWithValue("@ScheduleCalculationKey", ScheduleCalculationKey);
        DataSet ds = this.DA.GetDataSet(cmd);

        foreach(DataRow dr in ds.Tables[0].Rows)
        {
            ddl_ProcessFrequency.SelectedValue = dr["ProcessFrequency"].ToString();
            ddl_DateType.SelectedValue = dr["ProcessDateType"].ToString();
            ddl_AfterType.SelectedValue = dr["ProcessAfterType"].ToString();
            txt_AfterValue.Value = dr["ProcessAfterValue"].ToString();
            txt_AfterHour.Value= dr["ProcessAfterHour"].ToString();
        }
    }

    private void loadCommissionDetails(string CommissionKey)
    {
        string str_Select = "select a.*,(select FrequencyName from CalendarSetup where CalendarKey=a.Frequency) as FrequencyName, format(a.StartDate,'MM/dd/yyyy') as sDate, FORMAT(a.EndDate,'MM/dd/yyyy') as eDate from Commission a  where a.CommissionKey=@CommissionKey";
        SqlCommand cmd = new SqlCommand(str_Select);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataSet ds = this.DA.GetDataSet(cmd);

        foreach(DataRow dr in ds.Tables[0].Rows)
        {
            lbl_CommissionName.InnerText = dr["CommissionName"].ToString();
            lbl_CalcFrqu.InnerText = dr["FrequencyName"].ToString();
            lbl_edate.InnerText = dr["eDate"].ToString();
            lbl_sDate.InnerText = dr["sDate"].ToString();
            hdn_PlanEndDate.Value = dr["EndDate"].ToString();
            hdn_PlanStartDate.Value = dr["StartDate"].ToString();

            MasterCalendarKey.Value= dr["Frequency"].ToString();
        }
    }
    private void loadProcessFrequency()
    {
        string str_Select = "Select Seq as Value, Name as Text  from v_ProcessFrequency order by Seq";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_ProcessFrequency, ds.Tables[0], "Text", "Value", true, "Select process frequency");
    }


    protected void btn_ScheduleSave_ServerClick(object sender, EventArgs e)
    {
        //SchedulePlanCalculation(ScheduleCalculationKey, CommissionKey, ProcessFrequency, ProcessDateType, ProcessAfterValue, ProcessAfterType, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)


        string str_Sql = "Update  SchedulePlanCalculation  set  ProcessFrequency=@ProcessFrequency, ProcessDateType=@ProcessDateType, ProcessAfterHour=@ProcessAfterHour, ProcessAfterValue=@ProcessAfterValue, ProcessAfterType=@ProcessAfterType, ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where ScheduleCalculationKey=@ScheduleCalculationKey;" +
            "delete from ScheduledRunPeriod where ScheduleCalculationKey=@ScheduleCalculationKey";


        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@ScheduleCalculationKey", Com.GetQueryStringValue("ScheduleCalculationKey"));
        cmd.Parameters.AddWithValue("@ProcessFrequency", ddl_ProcessFrequency.SelectedValue);
        cmd.Parameters.AddWithValue("@ProcessDateType", ddl_DateType.SelectedValue);
        cmd.Parameters.AddWithValue("@ProcessAfterValue", txt_AfterValue.Value);
        cmd.Parameters.AddWithValue("@ProcessAfterType", ddl_AfterType.SelectedValue);
        cmd.Parameters.AddWithValue("@ProcessAfterHour", txt_AfterHour.Value);
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());
        this.DA.ExecuteNonQuery(cmd);

      //  Response.Redirect("../Web/CommissionCalculationList.aspx");

        CalcProcessPeriods(Com.GetQueryStringValue("ScheduleCalculationKey"));
        Response.Redirect(Request.RawUrl);

    }

    private void CalcProcessPeriods(string ScheduledCalculationKey)
    {
        DateTime startdate = Convert.ToDateTime(hdn_PlanStartDate.Value);
        DateTime enddate = Convert.ToDateTime(hdn_PlanEndDate.Value);

        float AfterHours = float.Parse(txt_AfterHour.Value);

        int startdateMonth = startdate.Month;
        int enddateMonth = enddate.Month;
      //  int year = int.Parse(hdn_PlanStartDate.Value);
        int year = 2019;

        int Frequence = Int32.Parse(ddl_ProcessFrequency.SelectedValue);

        int RunAfterDate = Int32.Parse(txt_AfterValue.Value);

        int RunType = Int32.Parse(ddl_AfterType.SelectedValue);
        int RunDateType = Int32.Parse(ddl_DateType.SelectedValue);
        DateTime RunDate = new DateTime();


        switch (Frequence)
        {
            case 2:
                //weekly
                int x = 0;

                var Totaldays = (enddate - startdate).TotalDays;

                DateTime firstDayOfMonth = new DateTime();
                DateTime lastDayOfMonth = new DateTime();

                DateTime StartDate = startdate;
                DateTime EndDate = enddate;

                DateTime WeekFirstDay = startdate;
                DateTime WeekLastDay = new DateTime();
               

                int seq = 1;

                for (x = 0; x <= Totaldays; x += 7)
                {
                    if (x == 0)
                    {
                        WeekFirstDay = startdate;
                    }
                    else
                    {
                        WeekFirstDay = WeekLastDay.AddDays(1);
                    }
                    if (x + 6 > Totaldays)
                    {
                        WeekLastDay = enddate;

                    }
                    else
                    {
                        WeekLastDay = WeekFirstDay.AddDays(6);
                    }

                    firstDayOfMonth = new DateTime(WeekFirstDay.Year, WeekFirstDay.Month, 1);

                    lastDayOfMonth = new DateTime(WeekLastDay.Year, WeekLastDay.Month, 1).AddMonths(1).AddDays(-1);

                    //add Days
                    if (RunType == 1)
                    {
                        if (RunDateType == 1)
                        {
                            //end date
                            RunDate = WeekLastDay.AddDays(RunAfterDate);
                        }
                        else if(RunDateType == 2)
                        {
                            //start date
                            RunDate = WeekFirstDay.AddDays(RunAfterDate);
                        }
                       
                    }
                    //add Hours
                    else if (RunType == 2)
                    {
                        if (RunDateType == 1)
                        {
                            //end date
                            RunDate = WeekLastDay.AddHours(RunAfterDate);
                        }
                        else if (RunDateType == 2)
                        {
                            //start date
                            RunDate = WeekFirstDay.AddHours(RunAfterDate);
                        }
                    }

                    //addHours
                    if (AfterHours == 0 || AfterHours==0.0)
                    {
                        RunDate = RunDate.AddHours(0);
                    }
                    else
                    {
                        RunDate = RunDate.AddHours(AfterHours);
                    }

                   InsertCalcPeriod(ScheduledCalculationKey,seq, firstDayOfMonth, lastDayOfMonth,WeekFirstDay, WeekLastDay, RunDate);
                    seq += 1;
                }


                break;
            case 4:
                //monthly
                int seqMonth = 1;
                for (int i = startdateMonth; i <= enddateMonth; i++)
                {
                    DateTime FirstDay = startdate;
                    DateTime LastDay = startdate;
                    if (i == startdateMonth)
                    {
                        FirstDay = startdate;
                    }
                    else
                    {
                        FirstDay = new DateTime(year, i, 1);
                    }

                    if (i > (enddateMonth - 1))
                    {
                        LastDay = enddate;
                    }
                    else
                    {
                        LastDay = new DateTime(FirstDay.Year, FirstDay.Month, DateTime.DaysInMonth(FirstDay.Year, FirstDay.Month));
                    }

                    //add Days
                    if (RunType == 1)
                    {
                        if (RunDateType == 1)
                        {
                            //end date
                            RunDate = LastDay.AddDays(RunAfterDate);
                        }
                        else if (RunDateType == 2)
                        {
                            //start date
                            RunDate = FirstDay.AddDays(RunAfterDate);
                        }

                    }
                    //add Hours
                    else if (RunType == 2)
                    {
                        if (RunDateType == 1)
                        {
                            //end date
                            RunDate = LastDay.AddHours(RunAfterDate);
                        }
                        else if (RunDateType == 2)
                        {
                            //start date
                            RunDate = FirstDay.AddHours(RunAfterDate);
                        }
                    }

                    if (AfterHours == 0 || AfterHours == 0.0)
                    {
                        RunDate = RunDate.AddHours(0);
                    }
                    else
                    {
                        RunDate = RunDate.AddHours(AfterHours);
                    }
                    InsertCalcPeriod(ScheduledCalculationKey, seqMonth, FirstDay, LastDay, FirstDay, LastDay, RunDate);
                    seqMonth += 1;
                }
                break;
            case 3:
                //Quarterly
                for (int i = startdateMonth; i <= enddateMonth; i += 3)
                {
                    DateTime FirstDay = startdate;
                    DateTime LastDay = startdate;
                    if (i == startdateMonth)
                    {
                        FirstDay = startdate;
                    }
                    else
                    {
                        FirstDay = new DateTime(year, i, 1);
                    }

                    if (i > (enddateMonth - 3))
                    {
                        LastDay = enddate;
                    }
                    else
                    {
                        var DD = FirstDay.AddDays(1).AddMonths(2).AddDays(-1);
                        LastDay = new DateTime(FirstDay.Year, DD.Month, DateTime.DaysInMonth(DD.Year, DD.Month));
                    }

                   // insertPeriod(FirstDay, LastDay, str_Key);
                }
                break;
            case 6:
                //Half Yearly
                for (int i = startdateMonth; i <= enddateMonth; i += 6)
                {
                    DateTime FirstDay = startdate;
                    DateTime LastDay = startdate;
                    if (i == startdateMonth)
                    {
                        FirstDay = startdate;
                    }
                    else
                    {
                        FirstDay = new DateTime(year, i, 1);
                    }

                    if (i > (enddateMonth - 6))
                    {
                        LastDay = enddate;
                    }
                    else
                    {
                        var DD = FirstDay.AddDays(1).AddMonths(5).AddDays(-1);
                        LastDay = new DateTime(FirstDay.Year, DD.Month, DateTime.DaysInMonth(DD.Year, DD.Month));
                    }

                  //  insertPeriod(FirstDay, LastDay, str_Key);
                }
                break;
        }
    }

    private void InsertCalcPeriod(string scheduledCalculationKey, int Seq, DateTime firstDayOfMonth, DateTime lastDayOfMonth, DateTime weekFirstDay, DateTime weekLastDay, DateTime runDate)
    {
        //ScheduledRunPeriod(ScheduledPeriodKey, ScheduleCalculationKey, PeriodSeq, PlanFreqStartDate, PlanFreqEndDate, ProcessFreqStartDate, 
        //ProcessFreqEndDate, ScheduleToRunOn, Status, CalcProcessedOn, ActivateManualRun, IsManualCalc, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)


        string str_Key = Guid.NewGuid().ToString();

        string str_Sql = "insert into ScheduledRunPeriod (ScheduledPeriodKey, ScheduleCalculationKey, PeriodSeq, PlanFreqStartDate, PlanFreqEndDate, ProcessFreqStartDate, ProcessFreqEndDate, ScheduleToRunOn, Status, CreatedOn, CreatedBy,MasterPeriodKey)" +
            "select @ScheduledPeriodKey, @ScheduleCalculationKey, @PeriodSeq, @PlanFreqStartDate, @PlanFreqEndDate, @ProcessFreqStartDate, @ProcessFreqEndDate, @ScheduleToRunOn, @Status, @CreatedOn, @CreatedBy," +
            "(SELECT PeriodKey FROM PeriodMaster WHERE CalendarKey=@CalendarKey and @ProcessFreqEndDate between startDate and endDate)";


        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@ScheduledPeriodKey", str_Key);
        cmd.Parameters.AddWithValue("@ScheduleCalculationKey", scheduledCalculationKey);
        cmd.Parameters.AddWithValue("@PeriodSeq", Seq);
        cmd.Parameters.AddWithValue("@PlanFreqStartDate", firstDayOfMonth);
        cmd.Parameters.AddWithValue("@PlanFreqEndDate", lastDayOfMonth);
        cmd.Parameters.AddWithValue("@ProcessFreqStartDate", weekFirstDay);
        cmd.Parameters.AddWithValue("@ProcessFreqEndDate", weekLastDay);
        cmd.Parameters.AddWithValue("@ScheduleToRunOn", runDate);
        cmd.Parameters.AddWithValue("@CalendarKey", MasterCalendarKey.Value);
        cmd.Parameters.AddWithValue("@Status", "0");
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);
    }

    private void ProcessPeriodFrequencyList(string scheduledCalculationKey)
    {
        //DivCommissionProcessCalculationPeriodTr ProcessedOn
        string str_Sql_view = "select *,(case when CalcProcessedOn is NULL then 'd-none' else '' end) as ViewCalc ,(case when ActivateManualRun=1 then 'Checked' else '' end) as ManualRunChecked ,(case when ActivateManualRun=1 then 'line-through' else '' end) as LineThrough ,(case when IsManualCalc=1 then '' else 'd-none' end) as ShowRunButton, (case when Status=0 then 'In Process' when Status=1 then 'Completed' when Status=2 then 'Error'  end) as StatusType,format(PlanFreqStartDate,'MM/dd/yyyy') as plsDate,format(PlanFreqEndDate,'MM/dd/yyyy') as pleDate , format(ProcessFreqEndDate,'MM/dd/yyyy') as peDate,format(ProcessFreqStartDate,'MM/dd/yyyy') as psDate , Status, format( CalcProcessedOn,'MM/dd/yyyy') as ProcessedOn, (select CommissionKey from SchedulePlanCalculation where ScheduleCalculationKey=@ScheduleCalculationKey) as CommissionKey from ScheduledRunPeriod  where ScheduleCalculationKey=@ScheduleCalculationKey order by PeriodSeq";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ScheduleCalculationKey", scheduledCalculationKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_ProcessPeriodList, "DivCommissionProcessCalculationPeriodTr.txt", "");
    }

    [WebMethod]
    public static string ExecuteCommissionCalculation(string PeriodKey, string CommissionKey, string ScheduledRunPeriodKey)
    {
      //  string[] Commissions = CommissionKeys.Split(',');
        string SuccessMessage = "";
        var ErrorMessage = new List<string>();

        try
        {
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Sql = "p_ExecuteManualCalculationProcess";
            SqlCommand sqlcmd = new SqlCommand(str_Sql);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@PeriodKey", PeriodKey);
            sqlcmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
            sqlcmd.Parameters.AddWithValue("@ScheduledRunPeriodKey", ScheduledRunPeriodKey);
            sqlcmd.Parameters.AddWithValue("@UserKey", SC.UserKey);
            DataSet ds = DA.GetDataSet(sqlcmd);
            DataRow dr = ds.Tables[0].Rows[0];

            if (dr["ErrorMessage"].ToString() != "")
            {
                ErrorMessage.Add(dr["ErrorMessage"].ToString());
            }
            else
            {
                SuccessMessage = "Executed";
            }
        }
        catch (Exception e)
        {
            return e.ToString();
        }


        var ErrorArray = ErrorMessage.ToArray();

        if (ErrorArray.Length > 0)
        {
            return string.Join(",", ErrorArray);
        }
        else
        {
            return SuccessMessage;
        }

    }
}