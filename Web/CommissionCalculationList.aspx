﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionCalculationList.aspx.cs" Inherits="Web_CommissionCalculationList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title> Scheduler List</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
    <div class="row">
    <!--Data List-->
    <div class="col-lg-12 col-md-12">
        <!-- Basic datatable -->
        <div class="card ">
            <div class="card-header header-elements-inline">
            <h5 class="card-title">Scheduler List
                
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                                       
                    <a href="CommissionList.aspx" class="btn btn-primary  btn-sm "> Commisions List</a>
                                      
                </div>
            </div>
            </div>
    
            <div class="card-body">
                  <div class="table-responsive">
                      <table class="table datatable-basic">
                          <thead>
                              <tr>
                                  <th>Commission Name</th>
                                  <th>Start Date</th>
                                  <th>End Date</th>
                                  <th>Calculation Frequency</th>
                                  <th>Process Frequency</th>
                              </tr>
                          </thead>
                          <tbody>
                              <asp:PlaceHolder ID="ph_CommissionCalculation" runat="server"></asp:PlaceHolder>
                          </tbody>
                      </table>
                  </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</asp:Content>

