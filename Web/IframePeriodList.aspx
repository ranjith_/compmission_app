﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" AutoEventWireup="true" CodeFile="IframePeriodList.aspx.cs" Inherits="Web_IframePeriodList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Period List</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="row">
        
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table datatable-basic">
                        <thead>
                            <tr>
                                <th>Period No</th>
                                <th>Year</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:PlaceHolder ID="ph_PeriodList" runat="server"></asp:PlaceHolder>
                        </tbody>
                    </table>
                </div>
            </div>
     </div>
</asp:Content>

