﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Adjustments : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        loadAdjustmentList();

        if (Request.QueryString["adjust_key"] != null)
        {
            viewAdjustmentDetails(Request.QueryString["adjust_key"]);
            div_AdjustmentDetails.Visible = true;
            div_AdjustmentDetails.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Visible = false;
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["adjust_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadAdjustmentDetailToEdit(Request.QueryString["adjust_key"]);
                div_AdjustmentDetails.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }
    [WebMethod]
    public static string viewAdjustment(string adjust_key)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select *, convert(varchar, StartDate, 101) as Start_Date,convert(varchar, EndDate, 101) as End_Date from Adjustments where AdjustmentKey =@AdjustmentKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@AdjustmentKey", adjust_key);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivAdjustmentView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string deleteAdjustment(string AdjustKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Adjustments where AdjustmentKey=@AdjustmentKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@AdjustmentKey", AdjustKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    private void loadAdjustmentDetailToEdit(string adjust_key)
    {
        //( AdjustmentKey, AdjustmentName, AdjustmentDesc, RecipientName, RecipientType, replace(convert(varchar, StartDate, 111),'/', '-') as Start_Date, replace(convert(varchar, EndDate, 111),'/', '-') as End_Date, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql_view = "select AdjustmentKey, AdjustmentName, AdjustmentDesc, RecipientName, RecipientType, replace(convert(varchar, StartDate, 111),'/', '-') as Start_Date, replace(convert(varchar, EndDate, 111),'/', '-') as End_Date," +
            " CreatedOn, CreatedBy, ModifiedOn, ModifiedBy" +
            " from Adjustments where AdjustmentKey =@AdjustmentKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@AdjustmentKey", adjust_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            txt_AdjustmentName.Value = dr["AdjustmentName"].ToString();
            txt_AdjustmentDesc.Value = dr["AdjustmentDesc"].ToString();
            txt_RecipientName.Value = dr["RecipientName"].ToString();
            dd_RecipientType.Value = dr["RecipientType"].ToString();
            dp_StartDate.Value = dr["Start_Date"].ToString();
            dp_EndDate.Value = dr["End_Date"].ToString();
            

        }
    }

    private void viewAdjustmentDetails(string adjust_key)
    {
        string str_Sql_view = "select *, convert(varchar, StartDate, 101) as Start_Date,convert(varchar, EndDate, 101) as End_Date from Adjustments where AdjustmentKey =@AdjustmentKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@AdjustmentKey", adjust_key);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_AdjustmentDetails, "DivAdjustmentView.txt", "");
    }

    private void loadAdjustmentList()
    {
        string str_Sql = "select AdjustmentKey ,AdjustmentName ,AdjustmentDesc ,RecipientName ,RecipientType , convert(varchar, StartDate, 101) as Start_Date  ,convert(varchar, EndDate, 101) as End_Date ,CreatedOn ,CreatedBy ,ModifiedOn ,ModifiedBy from Adjustments order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_AdjustmentsListTr, "AdjustmentListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        

        //string dd = txt_TransactionDate.Value.ToString();
        string str_Key = Guid.NewGuid().ToString();
        //string trans_Key = Guid.NewGuid().ToString();
        string str_Sql = "insert into Adjustments (AdjustmentKey, AdjustmentName, AdjustmentDesc, RecipientName, RecipientType, StartDate, EndDate, CreatedOn, CreatedBy)"
                            + "select @AdjustmentKey, @AdjustmentName, @AdjustmentDesc, @RecipientName, @RecipientType, @StartDate, @EndDate, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AdjustmentKey", str_Key);
        cmd.Parameters.AddWithValue("@AdjustmentName", txt_AdjustmentName.Value);
        cmd.Parameters.AddWithValue("@AdjustmentDesc", txt_AdjustmentDesc.Value);
        cmd.Parameters.AddWithValue("@RecipientName", txt_RecipientName.Value);
        cmd.Parameters.AddWithValue("@RecipientType", dd_RecipientType.Value);
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
       
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string adjust_key = Request.QueryString["adjust_key"];

        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";
        //  string str_Sql = "UPDATE transactions set transaction_id=@transaction_id, transaction_line=@transaction_line,  where transaction_key=@transaction_key  ";

        string str_Sql = "UPDATE Adjustments set  AdjustmentName=@AdjustmentName, AdjustmentDesc=@AdjustmentDesc, RecipientName=@RecipientName, RecipientType=@RecipientType, " +
            "StartDate=@StartDate, EndDate=@EndDate, ModifiedOn=@ModifiedOn, ModifiedBy =@ModifiedBy where AdjustmentKey=@AdjustmentKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);

       
        cmd.Parameters.AddWithValue("@AdjustmentName", txt_AdjustmentName.Value);
        cmd.Parameters.AddWithValue("@AdjustmentDesc", txt_AdjustmentDesc.Value);
        cmd.Parameters.AddWithValue("@RecipientName", txt_RecipientName.Value);
        cmd.Parameters.AddWithValue("@RecipientType", dd_RecipientType.Value);
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
        cmd.Parameters.AddWithValue("@ModifiedBy", userKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@AdjustmentKey", adjust_key);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/Adjustments.aspx?adjust_key=" + adjust_key + "");
    }
}