﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class Web_Login : System.Web.UI.Page
{
    DataAccess DA;
    Common CM;
    SessionCustom SC;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.CM = new Common();
        this.SC = new SessionCustom();

    }

    protected void btn_Login_ServerClick(object sender, EventArgs e)
    {
        string str_userName = txt_UserName.Value.Trim();
        string str_Password = txt_Password.Value.Trim();

        if(str_userName=="" && str_Password == "")
        {
            lbl_UserNameError.InnerText = "Enter your username";
            lbl_PasswordError.InnerText = "Enter your password";
            
        }
        else if (str_userName == "")
        {
            lbl_UserNameError.InnerText = "Enter your username";
            lbl_PasswordError.InnerText = "";
        }
        else if (str_Password == "")
        {
            lbl_UserNameError.InnerText = "";
            lbl_PasswordError.InnerText = "Enter your password";
        }
        else
        {
            lbl_UserNameError.InnerText = "";
            lbl_PasswordError.InnerText = "";

            DirectSignIn(str_userName, str_Password);
        }
       
    }

    private void DirectSignIn(string str_UserName, string str_Password)
    {
        Common CM = new Common();
        string str_Sql = "select * from Credential where UserName=@UserName and Password=@Password;";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@UserName", str_UserName);
        cmd.Parameters.AddWithValue("@Password", str_Password);
        DataTable dt = this.DA.GetDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            Session["UserKey"] = dt.Rows[0]["UserKey"].ToString();
            
            CM.LoadSessionVariables(dt.Rows[0]["UserKey"].ToString());
            HttpContext.Current.Session["httproot"] = Request.Url.OriginalString.ToLower().Replace("login.aspx", "").Replace("?session=logout", "");

            this.SC.UserRecord=dt.Rows[0];
            
            string RoleKey = this.SC.RoleKey.ToUpper();

            if (RoleKey == "B78F016A-C96F-4FD3-90C2-42BAB32D96DB")
            {
                //System Admin
                Response.Redirect("~/Dashboard/AdminDashboard.aspx");
            }
            else if (RoleKey == "1EAE78EF-B37B-400E-9452-9F89F31D0456")
            {
                //Comp ADmin
                Response.Redirect("~/Dashboard/CompAdminDashboard.aspx");

            }
            else if (RoleKey == "BE14E866-B717-4C94-A91B-E18BBC26F865")
            {
                //Manager
                Response.Redirect("~/Dashboard/ManagerDashboard.aspx");
            }
            else if(RoleKey== "7AFDFF20-2AAA-4768-8B3F-73F29814F32E")
            {
                //Sales Rep
                Response.Redirect("~/Dashboard/SalesRepDashboard.aspx");

            }


          //  Response.Redirect("~/Web/Dashboard.aspx");
        }
        else
        {
            CM.Alert("Invalid Username or Password ");
        }
    }

    
}