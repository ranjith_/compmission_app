﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master"   validateRequest="false" AutoEventWireup="true" ClientIDMode="Static" CodeFile="Agreement.aspx.cs" Inherits="Web_Agreement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Agreement</title>
      <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
    <script type="text/javascript">
        function deleteAgreement(agreement_key, agreement_name) {
            var confirmation = confirm("are you sure want to delete this " + agreement_name + " Adjustment ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/Agreement.aspx/deleteAgreement",
                    data: "{AgreementKey:'" + agreement_key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Adjustment " + agreement_name + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
     
        function appendWYSWYG() {
            
            var header = $('#txt_Headers').summernote('code');
            var footer = $('#txt_Footer').summernote('code');

            $('#hdn_Header').val(header);
            $('#hdn_Footer').val(footer);

           
        }
        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
            alertNotify('Variable Copied', ' Paste the text in doc.. !', 'bg-success border-success');
        }
        function ValidateAgreement() {
            //alert(Currency_Key);
            let AgreementName = $('#txt_AgreementName').val();
            let hdnAgreementName = $('#hdn_AgreementName').val();

            let valid = true;

            if (AgreementName == hdnAgreementName) {
                valid = true;
                }
                else {
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "../Web/Agreement.aspx/ValidateAgreement",
                        data: "{AgreementName:'" + AgreementName + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            if (data.d == "0") {
                                alertNotify('Invalid Agrrement Name', 'Agreement Name ' + AgreementName + ' is already exist', 'bg-danger border-danger');
                                $('#txt_AgreementName-error').addClass('d-none');
                                valid = false;


                            }
                            else if (data.d == "1") {
                                valid = true;
                            }

                        },
                        error: function () {
                            alert('Error communicating with server');
                            valid = false;
                        }
                    });
                }

            return valid;
        }
        function Validation() {
            appendWYSWYG();
            return ValidateAgreement();
        }
    </script>
    <style>
       .note-toolbar-wrapper{
            height:80px!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="container-fluid">
<div class="row">
     
    <div id="div_InputForm" runat="server" class="col-md-12 mx-auto">
                
        <div class="card ">
<div class="card-header header-elements-inline">
    <h5 class="card-title">Agreement</h5>
    <div class="header-elements">
        <div class="list-icons">
            <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" OnClientClick=" return Validation();" runat="server" CssClass="btn btn-primary  btn-sm " />
            <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClientClick=" return Validation();" OnClick="btn_Update_Click1" runat="server" CssClass="btn btn-primary  btn-sm " />
            <a class="list-icons-item" data-action="collapse"></a>
            <a class="list-icons-item" data-action="reload"></a>
                                        
        </div>
    </div>
</div>
    
<div class="card-body">
    <div class="row m-1">
         <div class="form-group col-md-4">
            <label>Agreement Name</label>
            <input id="txt_AgreementName" onfocusout="ValidateAgreement();" required runat="server" class="form-control" type="text" />
        </div>
    
        
    <div class="form-group col-md-2">
        <div class="form-check">
			<label class="form-check-label">
				<input type="checkbox" id="chb_DefaultAgreement" runat="server" class="form-check-input-styled" >
				Make as Default
			</label>
		</div>
        </div>
        <div class="col-md-6 bg-light">
            <div class="m-2">
                <h5 class="d-inline">Variables</h5> <span class="d-inline">(click to copy the variables)</span>
                <div class="row text-center">
                    <div class="col-md-6">
                        <button type="button" onclick="copyToClipboard(this)" class="btn btn-outline-primary btn-sm">%%IncludeRecipientSpecificTerms%%</button>
                       
                    </div>
                     <div class="col-md-6">
                        <button type="button" onclick="copyToClipboard(this)"  class="btn btn-outline-primary btn-sm">%%IncludeRecipientCommission%%</button>
                       
                    </div>
                     <div class="col-md-6">
                        <button type="button" onclick="copyToClipboard(this)" class="btn btn-outline-primary btn-sm">%%StartDate%%</button>
                       
                    </div>
                     <div class="col-md-6">
                        <button type="button" onclick="copyToClipboard(this)"  class="btn btn-outline-primary btn-sm">%%Recipient%%</button>
                       
                    </div>
                </div>
            </div>
        </div>
         <div class="form-group col-md-6 mx-auto d-none">
        <div class="form-check">
			<label class="form-check-label">
                <input type="checkbox" id="chb_Showlogo" runat="server" class="form-check-input-styled"/>
				
				Show Logo
			</label>
		</div>
        </div>
        <div class="form-group col-md-12 mx-auto d-none">
            <label>Doc Title</label>
            <input id="txt_DocTitle" runat="server" class="form-control" type="text" />
        </div>
        <div class="form-group col-md-12 my-auto ">
            <h5>Doc</h5>
            <%--<textarea id="d" runat="server" class="form-control" rows="5"></textarea>--%>
             <div  id="txt_Headers" runat="server"  class="summernote mt-2">
						
						</div>

        </div>
        
        <div class="form-group col-md-12 mx-auto d-none">
            <label>Include Recipeint specific terms</label>
                    <select id="dd_IncludeRecipient" onchange="changeIncludeRecipient();" runat="server" class="form-control" >
                        <option value="">-- select one --</option>
                        <option value="1">Show First</option>
                        <option value="2">Show Last</option>
                    </select>
        </div>
        <div class="form-group col-md-12 mx-auto d-none">
            <label>Include Commissions</label>
                    <select runat="server" id="dd_IncludeCommission" onchange="changeIncludeCommission()" class="form-control" >
                         <option value="">-- select one --</option>
                        <option value="1">Show First</option>
                        <option value="2">Show Last</option>
                    </select>
        </div>
           
        <div class="form-group col-md-12 mx-auto d-none">
            <label>Footers</label>
           <%-- <textarea id="dd" runat="server" class="form-control" rows="5"></textarea>--%>
             <div  id="txt_Footer" runat="server"  class="summernote mt-2">
							
						</div>
        </div>

          <div class="form-group col-md-12 mx-auto d-none">
            <div class="form-check">
				    <label class="form-check-label">
                    <input type="checkbox" class="form-check-input-styled" id="chb_ShowSignature" runat="server"  />
                        Show Signature
                        </label>
		    </div>
        </div>
   </div>
   
   
       
    </div>
        </div>
    </div>


                             
</div>
</div>
    <asp:HiddenField ID="hdn_Header" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdn_Footer" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdn_AgreementName" runat="server" ClientIDMode="Static" />
</asp:Content>

