﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Web_TransactionView : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;

    protected void Page_Load(object sender, EventArgs e)
    {
        string transaction_key = Request.QueryString["trans_key"];
        btn_EditTransaction.Attributes.Add("onclick", "OpenPopUp('Edit Transaction','transactions','" + transaction_key + "','','');");
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        //string transaction_key = this.Com.GetQueryStringValue("trans_key");

        string str_Sql = "select transaction_key,transaction_line,replace(convert(varchar, effective_date, 111),'/', '-') as effectivedate,transaction_date,transaction_type,transaction_line_type,customer,sales_rep1,sales_rep2,product,sales_amount,"
                +"transaction_line_level,so_number,payment_type,bill_number,transaction_id,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn from transactions where transaction_key =@transaction_key";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@transaction_key", transaction_key);
        DataTable dt = this.DA.GetDataTable(cmd);
        //this.PHT.LoadGridItem(ds, tbodyTransaction, "Transactions.txt", "");
        string str_Template = this.PHT.ReadFileToString("TransactionDetials.txt"), str_HTML = "", str_ReplaceStr = "";
        foreach (DataRow dr in dt.Rows)
        {
            str_ReplaceStr = str_Template;
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ReplaceStr);
        }
        ph_TransactionDetail.Controls.Add(new LiteralControl(str_HTML));

        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_CustomerDetails, "TransactionCustomerDetails.txt", "");
    }
}