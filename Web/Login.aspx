﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Web_Login" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
   <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Template/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/main/jquery.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/validation/validate.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	

	<script src="../Limitless_Bootstrap_4/Template/layout_1/LTR/default/full/assets/js/app.js"></script>
	<%--<script src="../Limitless_Bootstrap_4/Template/global_assets/js/demo_pages/login_validation.js"></script>--%>
	<!-- /theme JS files -->

    <title> Bada CM | Login</title>
</head>
<body>
    
       
	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login card -->
				<form id="formLogin" runat="server" class="login-form form-validate" >
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="mb-0">Login to your account</h5>
								<span class="d-block text-muted">Your credentials</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input id="txt_UserName" runat="server" type="text" class="form-control" name="username" placeholder="Username" required="required"/>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
                                <label id="lbl_UserNameError" runat="server"  class="validation-invalid-label" for="txt_UserName"></label>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input id="txt_Password" runat="server" type="password" class="form-control" name="password" placeholder="Password" required="required"/>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
                                <label id="lbl_PasswordError" runat="server" class="validation-invalid-label" for="txt_Password"></label>
							</div>

							<div class="form-group d-flex align-items-center">
								<div class="form-check mb-0">
									<label class="form-check-label">
										<input type="checkbox" name="remember" class="form-input-styled" checked="checked"/>
										Remember
									</label>
								</div>

								<a href="ForgotPassword.aspx" class="ml-auto">Forgot password?</a>
							</div>

							<div class="form-group">
								<button id="btn_Login" name="btn_Login" runat="server" onserverclick="btn_Login_ServerClick" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
							</div>

						</div>
					</div>
				</form>
				<!-- /login card -->

			</div>
			<!-- /content area -->


			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light fixed-bottom">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2019. <a href="#">Bada CM</a> by <a href="http://rakatech.com" target="_blank">RaKa Technology</a>
					</span>

					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
						<li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Contact</a></li>
						<li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
					</ul>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
    
</body>
</html>
