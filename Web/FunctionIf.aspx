﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="FunctionIf.aspx.cs" Inherits="Web_FunctionIf" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            setValueType();
        });
        
        
        function SaveCondition() {
            parent.SaveCondition('if', $('#hdn_IfKey').val());
        }
        function setValueType() {
            let val = $('#ddl_ValueType').val();
         //   alert(val);
            if (val == 1) {
                $('.field-value').removeClass('d-none');
                $('.custom-value').addClass('d-none');
            }
            else if (val == 2) {
                $('.field-value').addClass('d-none');
                $('.custom-value').removeClass('d-none');
            }
            else if(val==0 ){
                $('.field-value').addClass('d-none');
                $('.custom-value').addClass('d-none');
            }
        }
      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label>Source Table</label>
                        <asp:DropDownList ID="ddl_SourceTable"  runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    
                     <div class="col-md-3 form-group">
                        <label>Operator</label>
                        <asp:DropDownList ID="ddl_Operator" CssClass="form-control" runat="server"></asp:DropDownList>
                     </div>
                    <div class="col-md-3 form-group">
                        <label>Select Value Type</label>
                      <asp:DropDownList ID="ddl_ValueType" onchange="setValueType()" runat="server" CssClass="form-control">
                          <asp:ListItem Text="--select value type--" Value="0"></asp:ListItem>
                          <asp:ListItem Text="From source field" Value="1"></asp:ListItem>
                          <asp:ListItem Text="From Custom value" Value="2"></asp:ListItem>
                      </asp:DropDownList>
                    </div>
                    <div class="col-md-3 form-group field-value d-none">
                        <label>Field</label>
                      <asp:DropDownList ID="ddl_SourceField"  CssClass="form-control"  runat="server"></asp:DropDownList>
                    </div>
                    <div class="col-md-3 form-group custom-value d-none">
                        <label>Value</label>
                        <input type="text" class="form-control" id="txt_value" runat="server" />    
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Then</label>
                        <textarea id="txt_ThenCondition" runat="server" class="form-control" rows="2"></textarea>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Else</label>
                        <textarea id="txt_ElseCondition"  runat="server" class="form-control" rows="2">0</textarea>
                    </div>
                    <div class="col-md-12 text-right form-group">
                        <button type="button" id="Btn_SaveIfCondition" onclick="SaveCondition();" runat="server" onserverclick="Btn_SaveIfCondition_ServerClick" class="btn btn-primary">Save</button>
                          <button type="button" id="btn_UpdateIfCOndition" visible="false" onclick="SaveCondition();" runat="server" onserverclick="btn_UpdateIfCOndition_ServerClick" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <asp:HiddenField ID="hdn_IfKey" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdn_RuleKey" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdn_FunctionConditionKey" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdn_TableFileValue" runat="server" ClientIDMode="Static" />
</asp:Content>

