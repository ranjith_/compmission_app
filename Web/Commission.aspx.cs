﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Text;
using System.IO;

public partial class Web_Commission : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
       // string ss = txt_DescWYSWYG.InnerHtml;

        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        createCommission();

       
        loadOptions();
       

        
        if (Com.GetQueryStringValue("CommissionKey")!=""&& Com.GetQueryStringValue("edit")!="")
        {
            
            LoadCommissionDetails(Com.GetQueryStringValue("CommissionKey"));
            LoadCommissionRule(Com.GetQueryStringValue("CommissionKey"));
            loadRecipientListEdit(Com.GetQueryStringValue("CommissionKey"));
            loadRecipientConditionEdit(Com.GetQueryStringValue("CommissionKey"));
            hdn_CommissionKey.Value = Com.GetQueryStringValue("CommissionKey");
            div_CommissionTabs.Visible = true;
            btn_SaveCommissionDetails.Visible = false;
            btn_UpdateRule.Visible = true;
            btn_SaveRule.Visible = false;
            btn_UpdateRecipient.Visible = true;
            btn_SaveRecipient.Visible = false;
            btn_UpdateCommissionDetails.Visible = true;
            btn_UpdateOptions.Visible = true;
            btn_SumbitOverAllCommission.Visible = false;
            //  TableCommissionRule.Rows.Add( addCommissionRule("1"));
        }
        else if(Com.GetQueryStringValue("new") == "1")
        {
            loadRecipientList();
            loadRecipientCondition();
            LoadCommissionDetails(Com.GetQueryStringValue("CommissionKey"));
            string tbody = addCommissionRule("1", "1", PHT);
            div_Tbody.InnerHtml = "<table id='TableCommissionRule' class='table table-actions'>" + tbody + "</table>";
            div_CommissionTabs.Visible = true;
            btn_SaveCommissionDetails.Visible = false;
            btn_UpdateRule.Visible = false;
            btn_UpdateRecipient.Visible = false;
            btn_SaveRecipient.Visible = true;
            hdn_CommissionKey.Value = Com.GetQueryStringValue("CommissionKey");
            btn_UpdateCommissionDetails.Visible = false;
            btn_UpdateOptions.Visible = false;
            btn_SumbitOverAllCommission.Visible = true;

        }
        
    }

    private void loadRecipientConditionEdit(string commisssionkey)
    {
        string str_Sql = "select * from CommissionRecipientsCondition where CommissionKey=@CommissionKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commisssionkey);
        DataSet ds = this.DA.GetDataSet(cmd);

        string tr = "";
        string str_RecipientConditionTemplate = PHT.ReadFileToString("DivCommissionRecipientConditionEdit.txt"), str_Replace = "";
        int id1 = 1;
        int id2 = 1;
        if (ds.Tables[0].Rows.Count == 0)
        {
            this.loadRecipientCondition();
        }
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            // string trRecipientCondition = this.PHT.ReplaceVariableWithValue(dr, str_RecipientConditionTemplate);
            string trRecipientCondition = str_RecipientConditionTemplate;
            string optionCommissionCondition = this.getHtmlOptions("select * from v_CommissionRecipientCondition", dr["RecipientType"].ToString());
            string optionOperator = this.getHtmlOptions("select * from v_Operators", dr["Operator"].ToString());

            string optionCommissionRole = this.getHtmlOptions("select * from " + dr["RecipientType"].ToString(), dr["Role"].ToString());
            string optionCondition = this.getHtmlOptions("select * from v_Condition", dr["Condition"].ToString());


            trRecipientCondition = trRecipientCondition.Replace("%%optionRecipientType%%", optionCommissionCondition);
            trRecipientCondition = trRecipientCondition.Replace("%%Role%%", optionCommissionRole);
            trRecipientCondition = trRecipientCondition.Replace("%%operator%%", optionOperator);
            trRecipientCondition = trRecipientCondition.Replace("%%condition%%", optionCondition);
            trRecipientCondition = trRecipientCondition.Replace("%%id1%%", id1.ToString());
            trRecipientCondition = trRecipientCondition.Replace("%%id2%%", id2.ToString());
            tr += trRecipientCondition;

            id1 += 1;
        }

        ph_RecipientConditionTr.Controls.Add(new LiteralControl(tr));
    }

    private void loadRecipientCondition()
    {
        PhTemplate PHT = new PhTemplate();

     string recipientCondition=   addRecipientCondition("1", "1",  PHT);
        ph_RecipientConditionTr.Controls.Add(new LiteralControl(recipientCondition));
    }
    public string addRecipientCondition(string id1, string id2, PhTemplate PHT)
    {
        string str_RecipientCondition = PHT.ReadFileToString("DivCommissionRecipientConditionTr.txt"), str_Replace = "";

        string optionCommissionCondition = this.getHtmlOptions("select * from v_CommissionRecipientCondition", "");
        string optionOperator = this.getHtmlOptions("select * from v_Operators", "");
        string optionCondition = this.getHtmlOptions("select * from v_Condition", "");
        str_Replace = str_RecipientCondition.Replace("%%id1%%", id1);
        str_Replace = str_Replace.Replace("%%id2%%", id2);
        str_Replace = str_Replace.Replace("%%optionRecipientType%%", optionCommissionCondition);
        str_Replace = str_Replace.Replace("%%operator%%", optionOperator);
        str_Replace = str_Replace.Replace("%%condition%%", optionCondition);

        return str_Replace;
    }

    private void loadRecipientListEdit(string commissionkey)
    {
        string str_Sql = "select a.recipient_key,a.recipient_name,a.recipient_type,a.recipient_manager,a.recipient_territory, case when  a.recipient_key=b.RecipientKey then 'checked' else '' end as checked from recipients a left join (select * from CommissionRecipients where CommissionKey=@CommissionKey) b on a.recipient_key=b.RecipientKey order by  checked DESC, b.CreatedOn ASC";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_RecipientListtr, "CommissionRecipientListTr.txt", "");
    }

    public string addCommissionRule(string id1, string id2, PhTemplate PHT)
    {
        string str_CommissionRule = "<tbody id='CommissionRule-"+id1+"'>";

        str_CommissionRule += this.CommissionRuleName(id1,id2,PHT);
        str_CommissionRule += this.CommissionRuleCondition(id1,id2, PHT);
        str_CommissionRule += this.CommissionRuleDo(id1,id2, PHT);
        str_CommissionRule += "</tbody>";
        return str_CommissionRule;


    }

    private void LoadCommissionDetails(string CommissionKey)
    {
        string str_Sql_view = "select *,FORMAT (StartDate, 'yyyy-MM-dd') as sDate,FORMAT (EndDate, 'yyyy-MM-dd') as eDate from Commission where CommissionKey =@CommissionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {
            //commissionkey, CommissionName, CalFeq, transaction, CreditOf, Percentage, StartDate, EndDate, DescWYSWYG, isField, onAgree,

            txt_CommissionName.Value = dr["CommissionName"].ToString();
            dd_CalFeq.Value = dr["Frequency"].ToString();
            txt_Percentage.Value = dr["CreditPercentage"].ToString();
            dd_CreditOf.Value = dr["CreditOf"].ToString();
            dp_StartDate.Value= dr["sDate"].ToString();
            dp_EndDate.Value = dr["eDate"].ToString();
            txt_DescWYSWYG.InnerHtml= dr["Description"].ToString();
            if (dr["IsCreditField"].ToString() == "True")
            {
                chb_IsCreditField.Checked = true;
            }
            else
            {
                chb_IsCreditField.Checked = false;
            }
            if (dr["IsActive"].ToString() == "True")
            {
                chb_ActiveCommission.Checked = true;
            }
            else
            {
                chb_ActiveCommission.Checked = false;
            }
            if (dr["IsOnAggr"].ToString() == "True")
            {
                chb_OnAgree.Checked = true;
            }
            else
            {
                chb_OnAgree.Checked = false;
            }

            DdAssignRecipient.Value = dr["RecipientsType"].ToString();

            if (dr["RecipientsType"].ToString() == "1")
            {
                Individual_Recipients.Attributes.Add("class", "col-md-12");
            }
            if(dr["RecipientsType"].ToString() == "2")
            {
                Condition_Recipients.Attributes.Add("class", "form-group col-md-12");
            }

        }


       
    }

    private void LoadCommissionRule(string commissionKey)
    {
        string str_Sql = "select * ,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) ruleId from CommissionRule where CommissionKey=@CommissionKey order by RuleOrder;" +
           "select *,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) conditionId  from RuleCondition a join CommissionRule b on a.RuleKey = b.RuleKey where b.CommissionKey =@CommissionKey order by a.CreatedOn " +
           "select*,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) doId from RuleAction a join CommissionRule b on a.RuleKey = b.RuleKey where b.CommissionKey =@CommissionKey order by a.CreatedOn";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commissionKey);
        DataSet ds = this.DA.GetDataSet(cmd);

        string tbody = "";

        string str_RuleNameTemplate = PHT.ReadFileToString("DivCommissionRuleNameEdit.txt"), str_Replace = "";
        string str_RuleConditionTemplate = PHT.ReadFileToString("DivCommissionRuleConditionEdit.txt");
        string str_RuleDoTemplate = PHT.ReadFileToString("DivCommissionRuleDoEdit.txt");
        //str_Replace = str_RuleNameTemplate.Replace("%%id1%%", id1.ToString());
        //str_Replace = str_Replace.Replace("%%id2%%", id2.ToString());

        int id1 = 1;
        int id2 = 1;

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            tbody += "<tbody id="+id1+"-"+id2+">";


            string trRuleName = this.PHT.ReplaceVariableWithValue(dr, str_RuleNameTemplate);
           
            tbody += trRuleName;
            string RuleKey = dr["RuleKey"].ToString();
            
            foreach (DataRow dr1 in ds.Tables[1].Select("RuleKey='" + RuleKey + "'"))
            {
                string trRuleCondition = this.PHT.ReplaceVariableWithValue(dr1, str_RuleConditionTemplate);
                trRuleCondition = trRuleCondition.Replace("%%TableNames%%",this.getHtmlOptions("select * from v_CommissionRuleTableName", dr1["TableName"].ToString()));
                trRuleCondition = trRuleCondition.Replace("%%TableValues%%", this.getHtmlOptions("select * from "+ dr1["TableName"].ToString(), dr1["TableField"].ToString()));
                trRuleCondition = trRuleCondition.Replace("%%operatorSelect%%", this.getHtmlOptions("select * from v_Operators", dr1["Operator"].ToString()));
                trRuleCondition = trRuleCondition.Replace("%%condition%%", this.getHtmlOptions("select * from v_Condition", dr1["AndOr"].ToString()));
                trRuleCondition = trRuleCondition.Replace("%%id1%%", id1.ToString());
                trRuleCondition = trRuleCondition.Replace("%%id2%%",dr1["conditionId"].ToString());
                tbody += trRuleCondition;

                
            }

            foreach (DataRow dr2 in ds.Tables[2].Select("RuleKey='" + RuleKey + "'"))
            {
                string trRuleDo = this.PHT.ReplaceVariableWithValue(dr2, str_RuleDoTemplate);
                trRuleDo = trRuleDo.Replace("%%id1%%", id1.ToString());
                trRuleDo = trRuleDo.Replace("%%id2%%", dr2["doId"].ToString());
                tbody += trRuleDo;
            }

            tbody += "</tbody>";

            id1 += 1;
        }
       
       

        div_Tbody.InnerHtml = "<table id='TableCommissionRule' class='table table-actions'>"+tbody+"</table>";
    }


    private void loadOptions()
    {
        string str_Sql = "select * from role";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string optionRole = "";
        foreach (DataRow dr in dt.Rows)
        {

            optionRole += "<option value='" + dr["RoleKey"].ToString() + "'>" + dr["RoleName"].ToString() + "</option>";
        }

        string HtmlTr = "";

        HtmlTr = PHT.ReadFileToString("DivCommissionConditionTr.txt");


        string tr = HtmlTr.Replace("%%RoleOption%%", optionRole);
    }

    private void loadRecipientList()
    {
        string str_Sql = "select * from Recipients order by recipient_name";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_RecipientListtr, "CommissionRecipientListTr.txt", "");
    }

    private void createCommission()
    {
        string commission_key = Guid.NewGuid().ToString();
        hdn_CommissionKey.Value = commission_key;
    }

    [WebMethod]
    public static string addRuleDetails(string commissionkey, string rulename, string ruleorder)
    {

        try
        {
            //string commissionkey = "CABFB645-3EAE-4CFA-AD22-FBD0EB821F39";
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into CommissionRule (RuleKey, CommissionKey, RuleName, RuleOrder, CreatedOn, CreatedBy)"
                                + "select @RuleKey, @CommissionKey, @RuleName, @RuleOrder, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@RuleKey", str_Key);
            cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
            cmd.Parameters.AddWithValue("@RuleName",rulename);
            cmd.Parameters.AddWithValue("@RuleOrder", ruleorder);
            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
        

    }

    [WebMethod]
    public static string addConditionDetails(string rulekey, string tablename, string tablevalue, string operatorvalue, string conditionvalue, string condition)
    {
        //RuleCondition (ConditionKey, RuleKey, RuleOrder, TableName, tableField, Operator, Value, AndOr, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        try
        {
            
         
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into RuleCondition (ConditionKey, RuleKey,RuleOrder,  TableName, tableField, Operator, Value, AndOr, CreatedOn, CreatedBy)"
                                + "select @ConditionKey, @RuleKey, @RuleOrder,  @TableName, @tableField, @Operator, @Value, @AndOr, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@ConditionKey", str_Key);
            cmd.Parameters.AddWithValue("@RuleKey", rulekey);
            cmd.Parameters.AddWithValue("@RuleOrder", "1");
            cmd.Parameters.AddWithValue("@TableName", tablename);
            cmd.Parameters.AddWithValue("@tableField", tablevalue);
            cmd.Parameters.AddWithValue("@Operator", operatorvalue);
            cmd.Parameters.AddWithValue("@Value", conditionvalue);
            cmd.Parameters.AddWithValue("@AndOr", condition);
            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    [WebMethod]
    public static string addCommissionDetails(string commissionkey, string CommissionName, string CalFeq, string transaction, string CreditOf, string Percentage, string StartDate, string EndDate, string DescWYSWYG, string isField, string onAgree, string assignRecipient, string ActiveCommission)
    {

        //(commissionkey, CommissionName, CalFeq, transaction, CreditOf, Percentage, StartDate, EndDate, DescWYSWYG, isField, onAgree, assignRecipient, ActiveCommission)

        //Commission (CommissionKey, CommissionName, StartDate, EndDate, Description, IsOnAggr, Frequency, CreditPercentage, IsCreditField, CreditField, RecipientsType, IsActive, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        try
        {


            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into Commission (CommissionKey, CommissionName, StartDate, EndDate, Description, IsOnAggr, Frequency, CreditPercentage, CreditOf, IsCreditField, CreditField, RecipientsType, IsActive, CreatedOn, CreatedBy)"
                                + "select @CommissionKey, @CommissionName, @StartDate, @EndDate, @Description, @IsOnAggr, @Frequency, @CreditPercentage, @CreditOf, @IsCreditField, @CreditField, @RecipientsType, @IsActive, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
            cmd.Parameters.AddWithValue("@CommissionName", CommissionName);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Parameters.AddWithValue("@Description", DescWYSWYG);
            cmd.Parameters.AddWithValue("@IsOnAggr", onAgree);
            cmd.Parameters.AddWithValue("@Frequency", CalFeq);
            cmd.Parameters.AddWithValue("@CreditPercentage", Percentage);
            cmd.Parameters.AddWithValue("@CreditOf", CreditOf);
            cmd.Parameters.AddWithValue("@IsCreditField", isField);
            cmd.Parameters.AddWithValue("@CreditField", transaction);
            cmd.Parameters.AddWithValue("@RecipientsType", assignRecipient);
            cmd.Parameters.AddWithValue("@IsActive", ActiveCommission);
            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }


    [WebMethod]
    public static string addDoDetails(string rulekey, string dovalue)
    {
        //RuleAction (ActionKey, RuleKey, ActionOrder, Action, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        try
        {
           
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into RuleAction (ActionKey, RuleKey, ActionOrder, Action, CreatedOn, CreatedBy)"
                                + "select @ActionKey, @RuleKey, @ActionOrder, @Action, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@ActionKey", str_Key);
            cmd.Parameters.AddWithValue("@RuleKey", rulekey);
            cmd.Parameters.AddWithValue("@ActionOrder", "0");
            cmd.Parameters.AddWithValue("@Action", dovalue);

            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string addCommissionRecipients(string commissionkey, string recipientkey)
    {
        //CommissionRecipients (CommissionRecipientKey, CommissionKey, RecipientKey, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        try
        {
         //   string commissionKey = "CABFB645-3EAE-4CFA-AD22-FBD0EB821F39";
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into CommissionRecipients (CommissionRecipientKey, CommissionKey, RecipientKey, CreatedOn, CreatedBy)"
                                + "select @CommissionRecipientKey, @CommissionKey, @RecipientKey, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@CommissionRecipientKey", str_Key);
            cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
            cmd.Parameters.AddWithValue("@RecipientKey", recipientkey);
           
            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    [WebMethod]
    public static string addCommissionRecipientCondition(string commissionkey, string RecipientType, string RecipientOperator, string RecipientRole, string RecipientCondition)
    {
        //CommissionRecipientsCondition (RecipientsConditionKey, commissionKey, RecipientType, Operator, Role, Condition, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        try
        {
          //  string commissionKey = "CABFB645-3EAE-4CFA-AD22-FBD0EB821F39";
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into CommissionRecipientsCondition (RecipientsConditionKey, commissionKey, RecipientType, Operator,Role,  Condition,CreatedOn, CreatedBy)"
                                + "select @RecipientsConditionKey, @commissionKey, @RecipientType, @Operator, @Role,  @Condition, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@RecipientsConditionKey", str_Key);
            cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
            cmd.Parameters.AddWithValue("@RecipientType", RecipientType);
            cmd.Parameters.AddWithValue("@Operator", RecipientOperator);
           cmd.Parameters.AddWithValue("@Role", RecipientRole);
            cmd.Parameters.AddWithValue("@Condition", RecipientCondition);

            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return str_Key;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    [WebMethod]
    public static string getRecipientConditionTr(string condition)
    {

        try
        {
           
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
            PhTemplate PHT = new PhTemplate();

            string str_Sql = "select * from role";
            SqlCommand sqlcmd = new SqlCommand(str_Sql);
           
            DataTable dt = DA.GetDataTable(sqlcmd);
            string optionRole = "";
            foreach (DataRow dr in dt.Rows)
            {
                optionRole += "<option value='"+dr["RoleKey"].ToString()+"'>"+ dr["RoleName"].ToString() + "</option>";
            }

            string HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivCommissionConditionTr.txt");
           

            string tr = HtmlTr.Replace("%%RoleOption%%", optionRole);

            return tr;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string getCommissionRulesTbody(string id1, string id2)
    {

        Web_Commission commission = new Web_Commission();
        PhTemplate PHT1 = new PhTemplate();

       string rule= commission.addCommissionRule(id1,id2,PHT1);
        return rule;
    }
    [WebMethod]
    public static string getTableValueOptions(string table)
    {
        DataAccess DA = new DataAccess();
         string str_Sql = "select * from "+table;
        SqlCommand sqlcmd = new SqlCommand(str_Sql);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string option = "";
        foreach (DataRow dr in dt.Rows)
        {

            option += "<option value='" + dr["Value"].ToString() + "'>" + dr["Text"].ToString() + "</option>";
        }
        return option;
       
    }
    
    [WebMethod]
    public static string getCommissionCondition(string id1, string id2)
    {
        Web_Commission commission = new Web_Commission();
        PhTemplate PHT1 = new PhTemplate();

        string condition = commission.CommissionRuleCondition(id1,id2, PHT1);
        return condition;
    }
    [WebMethod]
    public static string getCommissionDo(string id1, string id2)
    {
        Web_Commission commission = new Web_Commission();
        PhTemplate PHT1 = new PhTemplate();

        string DO = commission.CommissionRuleDo(id1, id2, PHT1);
        return DO;
    }
    [WebMethod]
    public static string getCommissionRecipientCondition(string id1, string id2)
    {
        Web_Commission commission = new Web_Commission();
        PhTemplate PHT1 = new PhTemplate();

        string recipientCOndition = commission.addRecipientCondition(id1, id2, PHT1);
        return recipientCOndition;
    }
    protected void btn_SaveCommissionDetails_ServerClick(object sender, EventArgs e)
    {
        string commission_key = Guid.NewGuid().ToString();

        string str_Sql = "insert into Commission (CommissionKey, CommissionName, Frequency, CreditPercentage, CreditOf, IsCreditField, CreditField,IsActive, CreatedOn, CreatedBy)"
                            + "select @CommissionKey, @CommissionName,  @Frequency, @CreditPercentage, @CreditOf, @IsCreditField, @CreditField, @IsActive, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commission_key);
        cmd.Parameters.AddWithValue("@CommissionName", txt_CommissionName.Value);
        //cmd.Parameters.AddWithValue("@StartDate", StartDate);
        //cmd.Parameters.AddWithValue("@EndDate", EndDate);
        //cmd.Parameters.AddWithValue("@Description", DescWYSWYG);
        //cmd.Parameters.AddWithValue("@IsOnAggr", onAgree);
        cmd.Parameters.AddWithValue("@Frequency", dd_CalFeq.Value);
        cmd.Parameters.AddWithValue("@CreditPercentage", txt_Percentage.Value);
        cmd.Parameters.AddWithValue("@CreditOf", dd_CreditOf.Value);
        if (chb_IsCreditField.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsCreditField", "1");
            cmd.Parameters.AddWithValue("@CreditField", dd_transaction.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsCreditField", "0");
            cmd.Parameters.AddWithValue("@CreditField", "");
        }
        if (chb_ActiveCommission.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsActive", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsActive", "0");
        }
        //cmd.Parameters.AddWithValue("@IsCreditField", isField);
        //cmd.Parameters.AddWithValue("@CreditField", transaction);
        //cmd.Parameters.AddWithValue("@RecipientsType", assignRecipient);
        //cmd.Parameters.AddWithValue("@IsActive", ActiveCommission);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);
        Response.Redirect("../Web/Commission.aspx?CommissionKey=" + commission_key+"&new=1");
    }

    public string getHtmlOptions( string qry,string value)
    {
        DataAccess DA = new DataAccess();
       // string str_Sql = "select a.AgreementName as Text, a.AgreementKey as Value from Agreement a";
        SqlCommand sqlcmd = new SqlCommand(qry);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string option = "";
        foreach (DataRow dr in dt.Rows)
        {
            string selected = "";
            if (dr["Value"].ToString() == value)
            {
                selected = "selected='selected'";
            }
            option += "<option value='" + dr["Value"].ToString() + "' "+selected+">" + dr["Text"].ToString() + "</option>";
        }
        return option;
    }
    public string CommissionRuleName( string id1,string id2, PhTemplate PHT1)
    {
        string str_RuleNameTemplate = PHT1.ReadFileToString("DivCommissionRuleNameTr.txt"),  str_Replace = "";

        str_Replace = str_RuleNameTemplate.Replace("%%id1%%", id1);
        str_Replace = str_Replace.Replace("%%id2%%", id2);
        return str_Replace;
    }

    public string CommissionRuleCondition(string id1, string id2, PhTemplate PHT1)
    {
        string str_RuleConditionTemplate = PHT1.ReadFileToString("DivCommissionRuleConditionTr.txt"), str_Replace = "";

        string optionTableName = this.getHtmlOptions("select * from v_CommissionRuleTableName","");
        string optionOperator= this.getHtmlOptions("select * from v_Operators", "");
        string optionCondition = this.getHtmlOptions("select * from v_Condition", "");
        str_Replace = str_RuleConditionTemplate.Replace("%%id1%%", id1);
        str_Replace = str_Replace.Replace("%%id2%%", id2);
        str_Replace = str_Replace.Replace("%%TableNames%%", optionTableName);
        str_Replace = str_Replace.Replace("%%operator%%", optionOperator);
        str_Replace = str_Replace.Replace("%%condition%%", optionCondition);
        return str_Replace;
    }
    public string CommissionRuleDo(string id1, string id2 ,PhTemplate PHT1)
    {
        string str_RuleConditionTemplate = PHT1.ReadFileToString("DivCommissionRuleDo.txt"), str_Replace = "";

       
        str_Replace = str_RuleConditionTemplate.Replace("%%id1%%", id1);
        str_Replace = str_Replace.Replace("%%id2%%", id2);
        return str_Replace;
    }

    //delete rules

        [WebMethod]
    public static string deleteExistingRules(string commissionkey)
    {
        DataAccess DA = new DataAccess();
        string str_Sql = "delete a   from RuleCondition a join CommissionRule b on a.RuleKey = b.RuleKey where b.CommissionKey =@CommissionKey ;" +
            "delete a from RuleAction a join CommissionRule b on a.RuleKey = b.RuleKey where b.CommissionKey =@CommissionKey "+
        "delete from CommissionRule where CommissionKey=@CommissionKey ;";
          
        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DA.ExecuteNonQuery(sqlcmd);
        return "deleted";
    }
    [WebMethod]
    public static string deleteExistingRecipients(string commissionkey)
    {
        DataAccess DA = new DataAccess();
        string str_Sql = "delete from commissionRecipientsCondition where commissionKey=@CommissionKey " +
            "delete from CommissionRecipients where commissionKey =@CommissionKey ";

        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DA.ExecuteNonQuery(sqlcmd);
        return "deleted";
    }
    [WebMethod]
    public static string updateCommissionRecipientSelect(string commissionkey, string value)
    {
        DataAccess DA = new DataAccess();
        string str_Sql = "update  commission set RecipientsType=@RecipientsType where commissionKey=@CommissionKey ";
           

        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("@RecipientsType", value);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
        DA.ExecuteNonQuery(sqlcmd);
        return "updated";
    }

    [WebMethod]
    public static string updateCommissionDetails(string commissionkey, string CommissionName, string CalFeq, string transaction, string CreditOf, string Percentage, string StartDate, string EndDate, string DescWYSWYG, string isField, string onAgree, string ActiveCommission)
    {
        try
        {


            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
           

            string str_Sql = "Update  Commission set  CommissionName=@CommissionName, StartDate=@StartDate, EndDate=@EndDate, Description=@Description, " +
                "IsOnAggr=@IsOnAggr, Frequency=@Frequency, CreditPercentage=@CreditPercentage, CreditOf=@CreditOf," +
                " IsCreditField=@IsCreditField, CreditField=@CreditField,  IsActive=@IsActive," +
                " ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where CommissionKey=@CommissionKey";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@CommissionKey", commissionkey);
            cmd.Parameters.AddWithValue("@CommissionName", CommissionName);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Parameters.AddWithValue("@Description", DescWYSWYG);
            cmd.Parameters.AddWithValue("@IsOnAggr", onAgree);
            cmd.Parameters.AddWithValue("@Frequency", CalFeq);
            cmd.Parameters.AddWithValue("@CreditPercentage", Percentage);
            cmd.Parameters.AddWithValue("@CreditOf", CreditOf);
            cmd.Parameters.AddWithValue("@IsCreditField", isField);
            cmd.Parameters.AddWithValue("@CreditField", transaction);
            cmd.Parameters.AddWithValue("@IsActive", ActiveCommission);
            cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return "Updated";

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}