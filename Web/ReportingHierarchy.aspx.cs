﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_ReportingHierarchy : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    public string str_view;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        loadReportingList();

        if (Request.QueryString["reporting_key"] != null)
        {
            viewReportingDetails(Request.QueryString["reporting_key"]);
            div_ReportingDetails.Visible = true;
            div_ReportingDetails.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Visible = false;
        }
        if (!IsPostBack)
        {
            //(ReportingKey, JobTitle, ReportingTitle, Description, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)
            string str_Select = "select ReportingKey as Value, JobTitle as Text from ReportingHierarchy";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.Com.LoadDropDown(ddl_ReportingTerritory, ds.Tables[0], "Text", "Value", true, "--select reporting title--");

        }
        if (!IsPostBack)
        {
            if (Request.QueryString["reporting_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadReportingDetailToEdit(Request.QueryString["reporting_key"]);
                // div_ReportingDetails.Visible = false;
                div_ReportingDetails.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }

        if (!IsPostBack)
        {
            HierarchicalTreeView();
            string ViewUl = "<ul id='ul-data'  style='display: none; '>" + str_view + "</ul>";
            ph_TreeView.Controls.Add(new LiteralControl(ViewUl));

        }
    }

    private void loadReportingDetailToEdit(string ReportingKey)
    {
        //(ReportingKey, JobTitle, ReportingTitle, Description, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn) ReportingHierarchy

        string str_Sql_view = "select  * from ReportingHierarchy where  ReportingKey=@ReportingKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ReportingKey", ReportingKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            txt_JobTitle.Value = dr["JobTitle"].ToString();
            txt_Description.Value = dr["Description"].ToString();
            
            ddl_ReportingTerritory.SelectedValue = dr["ReportingTitle"].ToString();

        }
    }
    [WebMethod]
    public static string deleteReporting(string ReportingKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from ReportingHierarchy where  ReportingKey=@ReportingKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@ReportingKey", ReportingKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    private void viewReportingDetails(string ReportingKey)
    {
        string str_Sql_view = "select *,(select JobTitle from ReportingHierarchy t2 where t1.ReportingTitle=t2.ReportingKey) as ReportingTo from ReportingHierarchy t1 where  ReportingKey=@ReportingKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ReportingKey", ReportingKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_ReportingDetails, "DivReportingView.txt", "");
    }
    [WebMethod]
    public static string viewReporting(string ReportingKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select *,(select JobTitle from ReportingHierarchy t2 where t1.ReportingTitle=t2.ReportingKey) as ReportingTo from ReportingHierarchy t1 where  ReportingKey=@ReportingKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@ReportingKey", ReportingKey);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivReportingView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    private void loadReportingList()
    {
        //(ReportingKey, JobTitle, ReportingTitle, Description, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn) ReportingHierarchy

        string str_Sql = "select *, (select JobTitle from ReportingHierarchy t2 where t1.ReportingTitle=t2.ReportingKey) as ReportingTo ,convert(varchar, CreatedOn, 103)  as Created_On from ReportingHierarchy t1 order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_ReportingList, "ReportingListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
       

        string str_Key = Guid.NewGuid().ToString();

        string str_Sql = "insert into ReportingHierarchy (ReportingKey, JobTitle, ReportingTitle, Description, CreatedBy, CreatedOn)"
                            + "select @ReportingKey, @JobTitle, @ReportingTitle, @Description, @CreatedBy, @CreatedOn";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@ReportingKey", str_Key);
        cmd.Parameters.AddWithValue("@JobTitle", txt_JobTitle.Value);
        cmd.Parameters.AddWithValue("@Description", txt_Description.Value);
        if (ddl_ReportingTerritory.SelectedValue == "")
            cmd.Parameters.AddWithValue("@ReportingTitle", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ReportingTitle", ddl_ReportingTerritory.SelectedValue);
        // cmd.Parameters.AddWithValue("@ReportToTerritory", ddl_ReportingTerritory.SelectedValue);
        cmd.Parameters.AddWithValue("@CreatedBy", this.SC.GetUserKey());

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string ReportingKey = Request.QueryString["reporting_key"];

        //(ReportingKey, JobTitle, ReportingTitle, Description, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn) ReportingHierarchy

       

        string str_Sql = "UPDATE ReportingHierarchy set  JobTitle=@JobTitle, ReportingTitle=@ReportingTitle, Description=@Description," +
            " ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where ReportingKey=@ReportingKey ";

        SqlCommand cmd = new SqlCommand(str_Sql);

        cmd.Parameters.AddWithValue("@JobTitle", txt_JobTitle.Value);
        cmd.Parameters.AddWithValue("@Description", txt_Description.Value);
        if (ddl_ReportingTerritory.SelectedValue == "")
            cmd.Parameters.AddWithValue("@ReportingTitle", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ReportingTitle", ddl_ReportingTerritory.SelectedValue);
        cmd.Parameters.AddWithValue("@ModifiedBy", this.SC.GetUserKey());

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@ReportingKey", ReportingKey);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/ReportingHierarchy.aspx?reporting_key=" + ReportingKey + "");
    }

    private void HierarchicalTreeView()
    {

        string view = "";
        DataSet Ds = new DataSet();

        string sql_qry = "select ReportingKey, JobTitle, ReportingTitle from ReportingHierarchy";

        SqlCommand cmd = new SqlCommand(sql_qry);
        DataTable dt = this.DA.GetDataTable(cmd);



        foreach (DataRow Dr in dt.Select("ReportingTitle is NULL"))
        {
            if (Dr["ReportingTitle"] == DBNull.Value)
            {
                string parentNodeId = Dr["ReportingKey"].ToString();
                string ParentNodeTitle = Dr["JobTitle"].ToString();
               // string ParentNodeKey = Dr["TerritoryKey"].ToString();

                str_view += "<li data-id='" + parentNodeId + "'>" + ParentNodeTitle;
                str_view += loadChildNode(parentNodeId, ParentNodeTitle, dt);

                str_view += "</li>";
            }

        }
    }

    public string loadChildNode(string parentNodeId, string parentNodeTitle, DataTable dt)
    {
        string childnode = "";
        childnode += "<ul>";
        DataRow[] dr_cchild = dt.Select("ReportingTitle ='" + parentNodeId + "'");

        foreach (DataRow dr_child in dt.Select("ReportingTitle ='" + parentNodeId + "'"))
        {

            if (dr_child["ReportingTitle"] != DBNull.Value)
            {
                string childNodeId = dr_child["ReportingKey"].ToString();
                string childNodeTitle = dr_child["JobTitle"].ToString();
                //string childNodeKey = dr_child["TerritoryKey"].ToString();


                DataRow[] dr_grant_child = dt.Select("ReportingTitle ='" + childNodeId + "'");
                if (dr_grant_child.Length > 0)
                {
                    childnode += "<li data-id='" + childNodeId + "'>" + childNodeTitle;
                    childnode += this.loadChildNode(childNodeId, childNodeTitle, dt);
                    childnode += "</li>";
                }
                else
                {
                    childnode += "<li data-id='" + childNodeId + "'>" + childNodeTitle + "</li>";
                }
            }

        }
        childnode += "</ul>";
        return childnode;
    }
}