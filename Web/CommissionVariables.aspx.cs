﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionVariables : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.Com = new Common();

        if (!IsPostBack)
        {
            string str_Select = "select Name as Text, Value from v_ListDataTypes order by value ";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.Com.LoadDropDown(ddl_DataType, ds.Tables[0], "Text", "Text", true, "--Select Data type--");

        }

        loadVariableList();
        if (!IsPostBack)
        {
            if (Request.QueryString["variable_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadVariableDetailToEdit(Request.QueryString["variable_key"]);
               // div_AduitLog.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }

    private void loadVariableDetailToEdit(string variable_key)
    {
        string str_Sql_view = "select * " +
           " from CommissionVariables where  VariableKey =@VariableKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@VariableKey", variable_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {
            txt_VariableName.Value = dr["VariableName"].ToString();
            ddl_DataType.SelectedValue = dr["DataType"].ToString();

            txt_DefaultValue.Value = dr["DefaultValue"].ToString();
            txt_Description.Value= dr["Description"].ToString();
        }
    }

    private void loadVariableList()
    {
        string str_Sql = "select *, convert(varchar, a.CreatedOn, 101) as Created_On, (select FirstName from users where UserKey=a.CreatedBy) as CreatedByName from CommissionVariables a   order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_CommissionVariables, "CommissionVariableListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string str_Key = Guid.NewGuid().ToString();
        //  CommissionVariables  (VariableKey, VariableName, DataType, DefaultValue, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "insert into CommissionVariables  (VariableKey, VariableName, DataType, DefaultValue,Description, CreatedOn, CreatedBy)"
                            + "select @VariableKey, @VariableName, @DataType, @DefaultValue, @Description, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@VariableKey", str_Key);
        cmd.Parameters.AddWithValue("@VariableName", txt_VariableName.Value);
        cmd.Parameters.AddWithValue("@DataType", ddl_DataType.SelectedValue);

        cmd.Parameters.AddWithValue("@DefaultValue", txt_DefaultValue.Value);


        cmd.Parameters.AddWithValue("@Description", txt_Description.Value);


        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string VariableKey = Request.QueryString["variable_key"];

        //  CommissionVariables  (VariableKey, VariableName, DataType, DefaultValue, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "UPDATE CommissionVariables set   VariableName=@VariableName, DataType=@DataType, DefaultValue=@DefaultValue, Description=@Description,  " +
            "ModifiedOn=@ModifiedOn, ModifiedBy =@ModifiedBy where  VariableKey =@VariableKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);

        cmd.Parameters.AddWithValue("@VariableName", txt_VariableName.Value);
        cmd.Parameters.AddWithValue("@DataType", ddl_DataType.SelectedValue);

        cmd.Parameters.AddWithValue("@DefaultValue", txt_DefaultValue.Value);


        cmd.Parameters.AddWithValue("@Description", txt_Description.Value);

        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@VariableKey", VariableKey);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/CommissionVariables.aspx?CommissionVariables=" + VariableKey + "");
    }
}