﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionCalculationStatementReport : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        if (Com.GetQueryStringValue("CommissionKey")!="")
        {
           
            loadCommissionDetails(Com.GetQueryStringValue("CommissionKey"), Com.GetQueryStringValue("PeriodKey"));
           
           CommissionCalculationList(Com.GetQueryStringValue("PeriodKey"), Com.GetQueryStringValue("CommissionKey"));
        }
    }

    private void CommissionCalculationList(string ScheduleCalculationKey,string CommissionKey)
    {
       string str_Sql_view = "select a.CommissionRate, b.TransactionId,a.PeriodKey, c.Text as Product, a.TransactionKey,a.SalesRepId,a.PayOutAmount, a.CreatedOn, b.SalesAmount, (select RecipientName from v_RecipientDetials where RecipientId=a.SalesRepId) as RecipientName from CalculationPayOut a join Transactions b on a.TransactionKey=b.TransactionKey join v_ProductList c on b.Product=c.Value  where a.Commissionkey=@Commissionkey and a.PeriodKey=@PeriodKey ";
     //   string str_Sql_view = "select a.PeriodKey, c.Text as Product, a.TransactionKey,a.SalesRepId,a.PayOutAmount,a.CreatedOn,b.TransactionId, b.SalesAmount from CalculationPayOut a  left join  Transactions b on a.TransactionKey=b.TransactionKey join v_ProductList c on b.Product=c.Value  where a.Commissionkey=@Commissionkey and a.PeriodKey=@PeriodKey ";
       // string str_Sql_view = "select * from CalculationPayOut a  where a.Commissionkey=CommissionKey and a.PeriodKey=@PeriodKey ";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@PeriodKey", ScheduleCalculationKey);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_CalcReport, "DivCommissionCalculationListTr.txt", "");
    }

    private void loadCommissionDetails(string CommissionKey, string ScheduleCalculationKey)
    {
        string str_Select = "select *, format(a.StartDate,'MM/dd/yyyy') as sDate, FORMAT(a.EndDate,'MM/dd/yyyy') as eDate,(select FrequencyName from CalendarSetup where CalendarKey=a.Frequency) as FrequencyName from Commission a where CommissionKey=@CommissionKey;" +
            "select concat( format(PlanFreqStartDate,'MM/dd/yyyy'), ' - ', format(PlanFreqEndDate,'MM/dd/yyyy')) as PlanFrequency, concat (format(ProcessFreqStartDate,'MM/dd/yyyy'),' - ',format(ProcessFreqEndDate,'MM/dd/yyyy') )as ProcessFrequency from ScheduledRunPeriod where ScheduledPeriodKey=@PeriodKey";
        SqlCommand cmd = new SqlCommand(str_Select);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        cmd.Parameters.AddWithValue("@PeriodKey", ScheduleCalculationKey);
        DataSet ds = this.DA.GetDataSet(cmd);

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            lbl_CommissionName.InnerText = dr["CommissionName"].ToString();
            lbl_CalcFrqu.InnerText = dr["FrequencyName"].ToString();
            lbl_edate.InnerText = dr["eDate"].ToString();
            lbl_sDate.InnerText = dr["sDate"].ToString();
           
        }

        foreach(DataRow dr in ds.Tables[1].Rows)
        {
            lbl_PlanFrequency.InnerText= dr["PlanFrequency"].ToString();
            lbl_ProcessFrequency.InnerText= dr["ProcessFrequency"].ToString();
        }
    }
}