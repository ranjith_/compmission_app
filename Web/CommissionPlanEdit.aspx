﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" ValidateRequest="false" CodeFile="CommissionPlanEdit.aspx.cs" Inherits="Web_CommissionPlanEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission Plan | Edit</title>
     <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
     <style>
        .note-editing-area{
            margin-top:20px!important;
        }
    </style>
    <script>
        function appendDescToHdn() {
            var desc = $('.note-editable').html();
            console.log(desc);
            $('#hdn_desc').val(desc);
           
        }
        function setEndDate(startDate, endDate) {
            var sDate = new Date($(startDate).val());
            var syear = sDate.getFullYear();
            var smonth = ("0" + (sDate.getMonth() + 1)).slice(-2);
            var sday = ("0" + sDate.getDate()).slice(-2);
            var eDate = new Date(syear + 1, smonth, sday)
            var end = syear + 1 + '-' + smonth + '-' + sday;
            //console.log(end);
            $(endDate).val(end);


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card bordert-top-2 border-top-warning-400">
                    <div class="card-body">
                        <h5 class="text-muted text-center">Recipient Info</h5>
                        <asp:PlaceHolder ID="ph_DivRecipientDetail" runat="server"></asp:PlaceHolder>
                        
                    </div>
                </div>
                  <div class="card bordert-top-2 border-top-success-600">
                    <div class="card-body">
                        <h5 class="text-muted text-center">Commissions </h5>
                        <asp:PlaceHolder ID="ph_RecipientsCommission" runat="server"></asp:PlaceHolder>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                  <div class="card border-top-2 border-top-blue-600">
                    <div class="card-header header-elements-inline  ">
                        <h5 class="card-title">Commission Plan </h5>
                        <div class="header-elements">
                            <asp:Button ID="btn_SavePlan" CssClass="btn btn-primary btn-sm" OnClientClick="appendDescToHdn()" OnClick="btn_SavePlan_Click" runat="server" text="Save"/>
                        </div>
                    </div>
                    <div class="card-body">
                           <div class="row">

                                 <div class="form-group col-md-6">
                                    <label>Start Date <span class="text-danger">*</span></label>
                                    <input onchange="setEndDate(this,'#dp_EndDate')" id="dp_StartDate" runat="server" type="date" class="form-control" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>End Date <span class="text-danger">*</span></label>
                                    <input readonly id="dp_EndDate"   type="date" runat="server" class="form-control" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Adjustments </label>
	                                <asp:DropDownList runat="server" CssClass="form-control" ID="dd_Adjustments">
	                                </asp:DropDownList>
                                </div>                                           
                                 <div class="form-group col-md-6">
                                    <label>Agreement </label>
	                                <asp:DropDownList ID="dd_Agreement" CssClass="form-control" runat="server">

	                                </asp:DropDownList>
                                </div>
                                 <div class="form-group col-md-12">
                                 <div  id="txt_Desc" runat="server"  class="summernote mt-2">
							                              
						         </div>
                                </div>
                                <div class="text-right col-md-12">

                                
                                </div>

                              <asp:HiddenField ID="hdn_desc" runat="server"/>
                           </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

