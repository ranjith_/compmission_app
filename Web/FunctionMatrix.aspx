﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="FunctionMatrix.aspx.cs" Inherits="Web_FunctionMatrix" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Matrix</title>
    <script>
        function CreateMatrix() {
            $('#MatrixTbody').html('');
            var columnField = $('#ColumnParameter').val();
            var RowField = $('#RowParameter').val();
            var RowCount = parseInt( $('#MatrixRow').val());
            var ColumnCount = parseInt($('#MatrixColumn').val());

            
            for (var r = (RowCount + 3); r >= 1; r--) {
                var RowText = "<tr>";
                if (r == (RowCount + 3)) {
                    //first Row
                    for (var c = (ColumnCount + 3); c >= 1; c--) {
                        if ((c - ColumnCount) > 0) {

                            RowText += "<td></td>";

                        } else {
                            RowText += "<td>" + columnField + "</td>";
                        }
                    }
                }
                else {
                    for (var c = (ColumnCount + 3); c >= 1; c--) {
                        if ((c - ColumnCount) > 0) {

                            if ((r - RowCount) > 0) {
                                RowText += "<td></td>";
                            }
                            else {
                                if ((c - (ColumnCount + 3)) == 0) {
                                    RowText += "<td>" + RowField + "</td>";
                                }
                                else {
                                    RowText += "<td><input id='Matrix-"+r+"-"+c+"' type='Text' class='form-control w-50'/></td>";
                                }
                                
                            }



                        } else {
                            RowText += "<td><input id='Matrix-" + r + "-" + c +"' type='Text' class='form-control w-50'/></td>";
                        }
                    }
                }
               
                
                RowText += "</tr>";
                console.log(RowText);
                $('#MatrixTbody').append(RowText);
            }
            
        }
    </script>
    <script type="text/javascript" src="../JScript/FunctionMatrix.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">

    <div class="container-fluid">
        <div class="row">
            <div class="form-group col-md-6" >
                    <label>Matrix Name  <span class="text-danger">*</span></label>
              
                <input type="text" id="txt_MatrixName" required runat="server" class="form-control required" />
            </div>
            <div class="form-group col-md-3">
                <label>Row Paramenter</label>
                <input type="text" id="txt_RowParameter" required runat="server" class="form-control" />
            </div>
            <div class="form-group col-md-3">
                <label>Column Parameter</label>
                <input type="text" id="txt_ColumnParameter" required runat="server" class="form-control" />
            </div>
            <div class="form-group col-md-1">
                <label>Rows</label>
                <input id="txt_MatrixRow" style="border: 2px solid #ea3d3d;" runat="server" type="text" onkeyup="" class="form-control number" required />
            </div>
             <div class="form-group col-md-2 my-auto">
                <div class="form-check">
					<label class="form-check-label">
					
                        <input type="checkbox" id="chb_EnableRowRange" runat="server" class="form-check-input-styled" />
						Enable Row Range 
					</label>
				</div>
            </div>
            <div class="form-group col-md-1">
                <label>Columns </label>
                <input id="txt_MatrixColumn"  style="border: 2px solid #a3cc00;" runat="server" type="text" class="form-control number" required />
            </div>
               <div class="form-group col-md-2 my-auto">
                <div class="form-check">
					<label class="form-check-label">
						<%--<input type="checkbox" runat="server" class="form-check-input-styled" id="chb_EnableColumnRange ">--%>
                        <input  type="checkbox" runat="server"  class="form-check-input-styled" id="chb_EnableColumnRange"/>
						Enable Column Range 
					</label>
				</div>
            </div>
            <div class="form-group col-md-3 my-auto">
                 <asp:Button ID="btn_CreateMatrix" Text="Generate Commission Table" OnClick="btn_CreateMatrix_ServerClick"   runat="server" CssClass="btn btn-primary  btn-sm " />
                <asp:Button ID="btn_Updatematrix" Text="Update Commission Table" Visible="false"  OnClick="btn_Updatematrix_ServerClick" runat="server" CssClass="btn btn-primary  btn-sm " />

                
            </div>
            <div class="from-group col-md-3 my-auto">
                <asp:Label CssClass="text-danger-600" ID="lbl_UpdateMsg" Visible="false" runat="server">If you update the Commission table, it may affect your existing commission values</asp:Label>
            </div>
        </div>
        <table class="table">
            <tbody id="MatrixTbody">
                <asp:PlaceHolder ID="ph_MatrixTr" runat="server"></asp:PlaceHolder>
            </tbody>
        </table>
         <div id="MatrixSave" visible="false" runat="server" class="col-md-12 text-right">
      <%--  <button class="btn btn-primary mr-3" id="btn_CancelMatrix" runat="server" onserverclick="btn_CancelMatrix_ServerClick" onclick="CancelMatrix();" >Cancel</button>--%>
         <button class="btn btn-primary" id="btn_SaveMatrix" onclick="SaveMatrix();">Save</button>
    </div>
    </div>
    <asp:HiddenField ID="hdn_MatrixKey" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdn_RuleKey" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdn_FunctionConditionKey" ClientIDMode="Static" runat="server" />
   
</asp:Content>

