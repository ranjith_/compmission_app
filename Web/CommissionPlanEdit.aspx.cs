﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionPlanEdit : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        if (!IsPostBack)
        {
            loadAdjustment();
            loadAgreement();
            loadRecipientPlan(Com.GetQueryStringValue("PlanKey"));
            loadRecipientDetails(Com.GetQueryStringValue("RecipientKey"));
            loadRecipientCommission(Com.GetQueryStringValue("RecipientKey"));
        }
       
    }

    private void loadRecipientCommission(string RecipientKey)
    {
        string str_Sql = "select CommissionName from Commission a join CommissionRecipients b on a.CommissionKey=b.CommissionKey where b.RecipientKey=@RecipientKey;";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@RecipientKey", RecipientKey);
        DataSet ds = DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_RecipientsCommission, "DivRecipientCommisssionListInfo.txt", "");
    }

    private void loadRecipientDetails(string RecipientKey)
    {
        string str_Sql = "select concat(b.FirstName ,' ', b.LastName ) as RecipientName, b.RecipientId, c.text as RepType, d.Text as ReportingManager, e.Text as TerritoryId from Recipients b  left join v_RecipientCategory c on b.RecipientType=c.Value left join v_RecipientManager d on b.ReportingManager=d.Value left join v_Territory e on b.RecipientTerritory=e.Value where b.RecipientKey=@RecipientKey;";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@RecipientKey", RecipientKey);
        DataSet ds = DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_DivRecipientDetail, "DivCommissionPlanRecipientInfo.txt", "");
    }

    private void loadRecipientPlan(string plankey)
    {
        string str_Sql_view = "select *,FORMAT (StartDate, 'yyyy-MM-dd') as sDate,FORMAT (EndDate, 'yyyy-MM-dd') as eDate  from RecipientPlan where PlanKey =@PlanKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@PlanKey", plankey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            dp_StartDate.Value = dr["sDate"].ToString();
            dp_EndDate.Value = dr["eDate"].ToString();
            dd_Agreement.SelectedValue = dr["AgreementKey"].ToString();
            dd_Adjustments.SelectedValue = dr["AdjustmentKey"].ToString();

            txt_Desc.InnerHtml = dr["SpecificTerms"].ToString();

           
            //(AgreementKey, AgreementName, DocTitle, Headers,IsDefault, IncludeRecipient, IncludeCommission, Footer, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        }
    }

    public void loadAgreement()
    {
         string str_Select = "select a.AgreementName as Text, a.AgreementKey as Value from Agreement a";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.Com.LoadDropDown(dd_Agreement, ds.Tables[0], "Text", "Value", true, "--Select Agreement--");
    }
    public void loadAdjustment()
    {
        string str_Select = "select a.AdjustmentName as Text, a.AdjustmentKey as Value from adjustments a";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(dd_Adjustments, ds.Tables[0], "Text", "Value", true, "--Select adjustments--");
    }
    public string getHtmlOptions(string qry, string value)
    {
        DataAccess DA = new DataAccess();
        // string str_Sql = "select a.AgreementName as Text, a.AgreementKey as Value from Agreement a";
        SqlCommand sqlcmd = new SqlCommand(qry);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string option = "";
        foreach (DataRow dr in dt.Rows)
        {
            string selected = "";
            if (dr["Value"].ToString() == value)
            {
                selected = "selected='selected'";
            }
            option += "<option value='" + dr["Value"].ToString() + "' " + selected + ">" + dr["Text"].ToString() + "</option>";
        }
        return option;
    }


    protected void btn_SavePlan_Click(object sender, EventArgs e)
    {
        string str_Sql = "Update RecipientPlan set   StartDate=@StartDate, EndDate=@EndDate, AdjustmentKey=@AdjustmentKey, AgreementKey=@AgreementKey, SpecificTerms=@SpecificTerms,  ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn   where PlanKey=@PlanKey;" +
            "update Recipients set PlanStartDate=@StartDate , PlanEndDate=@EndDate, ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn  where RecipientKey=@RecipientKey ";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PlanKey", Com.GetQueryStringValue("PlanKey"));
        cmd.Parameters.AddWithValue("@RecipientKey", Com.GetQueryStringValue("RecipientKey"));
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
        if (dd_Adjustments.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@AdjustmentKey", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@AdjustmentKey", dd_Adjustments.SelectedValue);
        }
        if (dd_Agreement.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@AgreementKey", DBNull.Value);
        }
        else
        {

            cmd.Parameters.AddWithValue("@AgreementKey", dd_Agreement.SelectedValue);
        }

        cmd.Parameters.AddWithValue("@SpecificTerms", hdn_desc.Value);
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);
        Response.Redirect("../Web/CommissionPlan.aspx");
    }
}