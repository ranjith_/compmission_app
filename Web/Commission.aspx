﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="Commission.aspx.cs" Inherits="Web_Commission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission</title>
      <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
    
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        //$(function () {
        //    var availabletags = [
        //        "actionscript",
        //        "applescript",
        //        "asp",
        //        "basic",
        //        "c",
        //        "c++",
        //        "clojure",
        //        "cobol",
        //        "coldfusion",
        //        "erlang",
        //        "fortran",
        //        "groovy",
        //        "haskell",
        //        "java",
        //        "javascript",
        //        "lisp",
        //        "perl",
        //        "php",
        //        "python",
        //        "ruby",
        //        "scala",
        //        "scheme"
        //    ];
        //    function split(val) {
        //        return val.split(/ \s*/);
        //    }
        //    function extractlast(term) {
        //        return split(term).pop();
        //    }

        //    $("#Variables")
        //        // don't navigate away from the field on tab when selecting an item
        //        .on("keydown", function (event) {
        //            if (event.keycode === $.ui.keycode.tab &&
        //                $(this).autocomplete("instance").menu.active) {
        //                alert('ddd');
        //                event.preventdefault();
        //            }
        //        })
        //        .autocomplete({
        //            minlength: 0,
        //            source: function (request, response) {
        //                // delegate back to autocomplete, but extract the last term
        //                response($.ui.autocomplete.filter(
        //                    availabletags, extractlast(request.term)));
        //            },
        //            focus: function () {
        //                // prevent value inserted on focus
        //                return false;
        //            },
        //            select: function (event, ui) {
        //                var terms = split(this.value);
        //                // remove the current input
        //                terms.pop();
        //                // add the selected item
        //                terms.push(ui.item.value);
        //                // add placeholder to get the comma-and-space at the end
        //                terms.push("");
        //                this.value = terms.join(" ");
        //                return false;
        //            }
        //        });
        //});

      
	</script>
     <style>
        .custom-panel{
    background-color: #80808008;
    padding: 15px;
    border-radius: 5px;
}
        .pl-36{
            padding-left: 36px!important;
        }
        .btn-xx{
            padding: .3em 0.4em;

        }
        .btn-xxx{
                padding: .1em 0.1em;
                height: 22px;
        }
        .table-actions td{
            border:none;
                padding: .2rem 1.25rem;
        }
        .table tbody{
            border-bottom:10px solid transparent;
        }
        .table-actions  tbody:after {
          content: '';
          display: block;
          height: 20px;
          border:2px solid double;
        }
        .rule-box{
            width:50px;
        }
        .tokenfield .token>.close {
             display:none;
         }
/*@media (min-width: 576px){
        .modal-dialog {
            max-width: 630px !important;
        }
}*/

#table-Individual-Recipient tr >td:nth-child(4){
    white-space:nowrap;
}

        #table-Condition-Recipient td, #table-Condition-Recipient thead th{
             border:none;
                padding: .2rem 1.25rem;
        }

.card-custom-commission{
    border: 2px solid #80808057;
    border-top: 8px solid #80808057;
    /* box-shadow: 2px 2px 10px 0px #d8d8d8; */
    border-radius: 20px;
    background-color: #f5f5f5;
    padding: 13px;
    /* border: 2px solid blue; */
}

.note-toolbar-wrapper{
    height:auto!important;
}
    </style>
    

	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>

    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/tags/tagsinput.min.js"></script>
	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/tags/tokenfield.min.js"></script>
	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>

    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/demo_pages/form_tags_input.js"></script>

 
    <script src="../JScript/Commission.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
<div class="container-fluid">
    <div class="row">
                               
    <!--Input Form-->
            <div class="col-lg-12 col-md-12">
            <!-- Basic setup -->
                <div class="card ">
                <div class="card-header header-elements-inline">
                <h5 class="card-title d-flex"><i class="icon-tree6"></i>  <span class="ml-2">Commission</span>
                        
                       
                <div class="form-check form-check-left form-check-switchery form-check-switchery-sm ml-3">
                        <label class="form-check-label"> ON / OFF
                            <!--Active -->
                            <input id="chb_ActiveCommission" type="checkbox" runat="server" class="form-input-switchery"  >
                        </label>
                        </div>
                </h5> 
                <div class="header-elements">
                                 
                </div>
                </div>
                <div class="card-body">
                    <div class="row">

                    
                    <div class="col-md-12">

                   
                        <div class="row m-1 card-cusom-commission">
                                <div class="form-group col-md-6">
                                    <label>Commission Name<span class="text-danger">*</span></label>
                                    <input   type="text" id="txt_CommissionName" runat="server" class="form-control required" >
                                   <asp:HiddenField id="hdn_CommissionKey" ClientIDMode="Static"  runat="server"/>
                                </div>
                                <div class="form-group col-md-6">
                                                <label>Calculation Feq<span class="text-danger">*</span></label>
                                                <select id="dd_CalFeq" runat="server" class="form-control ">
                                                    <option value="">-- select one --</option>
                                                    <option value="Weekly">Weekly</option>
                                                    <option value="Monthly">Monthly</option>
                                                    <option value="Quarterly">Quarterly</option>
                                                    <option value="Yearly">Yearly</option>
                                                </select>
                                             </div>
                            
                            
                             <div class="form-group col-md-6">
                                 
                                      <div class="d-flex">           
                                    <label>Source of Credit</label>
                                         
                                    <input id="chb_IsCreditField" runat="server" type="checkbox" onchange="changeSourceOfCredit(this)" class="mr-1 ml-4">
                                     <p>( Select from Transactions )</p>
                                          </div>
                                    <div class="d-flex">
                                        <select id="dd_transaction" runat="server" class="form-control  d-none">
                                    <option value="">-- select one --</option>
                                    <option value="1">Transaction 1</option>
                                    <option value="2">Transaction 2</option>
                                    <option value="3">Transaction 3</option>
                                    <option value="4">Transaction 4</option>
                                </select>
                                        <input id="txt_Percentage" runat="server" placeholder="Enter Percentage" type="text" class="form-control required" style="width:40%;">
                                        <span class="mr-3 ml-3 my-auto" style="white-space:nowrap;"> % of </span>
                                        <select id="dd_CreditOf" runat="server" class="form-control">
                                            <option value="">-- select one --</option>
                                            <option value="Sale Amount">Sale Amount</option>
                                            <option value="Advance">Advance</option>
                                        </select>
                                    </div>
                             </div>
                            <div class="form-group col-md-12 text-right">
                                <button id="btn_UpdateCommissionDetails" runat="server" visible="false" type="button" onclick="UpdateCommissionDetails()" class="btn btn-primary">UPDATE</button>
                            </div>
                            <div class="form-group col-md-12 text-right">
                                <button type="button" runat="server" class="btn btn-primary " id="btn_SaveCommissionDetails" onserverclick="btn_SaveCommissionDetails_ServerClick">Next</button>
                            </div>
                        </div>
                    </div>
                  
                    <div runat="server" id="div_CommissionTabs" visible="false" class="col-md-12">
                         <!--Nav Tabs for commission-->
                            <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0 mr-2 ml-2">
									<li class="nav-item"><a href="#Qualifying-Actions" class="nav-link active" data-toggle="tab"><span class="text-success"><i class="icon-checkmark2"></i></span> Qualifying Criteria & Actions  </a></li>
									
                                    <li class="nav-item"><a href="#Qualifying-Recipients" class="nav-link" data-toggle="tab">Qualifying Recipients</a></li>
                                 <li class="nav-item"><a href="#Options" class="nav-link" data-toggle="tab"> Options</a></li>
                                
								</ul>


                            <div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0 mr-2 ml-2">
									<div class="tab-pane fade show active" id="Qualifying-Actions">
                                        <div class="  text-right p-1 mb-2 border-bottom-3 border-dark-alpha">
                                             <div class="list-icons">
                                                <div class="dropdown">
                                                    <a href="#" class=" btn btn-primary  btn-xx" data-toggle="dropdown">
                                                        <i class="icon-menu7"></i>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-right">
                                                            
                                                        <button  type="button" onclick="addRule(this)" class="dropdown-item"> Add Rule</button>
                                                                        
                                                       <%-- <button type="button" onclick="addControlStatement(this)" class="dropdown-item"> Condition</button>--%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-icons"> 
                                                <div class="dropdown"> 
                                                    <a href="#" class=" btn btn-light btn-xx ml-2" data-toggle="dropdown"> <i class="icon-menu"></i> </a> 
                                                    <div class="dropdown-menu dropdown-menu-right"> 
                                                        <button type="button" data-toggle="modal" data-target="#modalSplit"  class="dropdown-item"> Split()</button> 
                                                        <button type="button" data-toggle="modal" data-target="#modalOverride"  class="dropdown-item"> Override()</button>
                                                        <button type="button" data-toggle="modal" data-target="#modalRange"  class="dropdown-item"> Matrix()</button> 
                                                        <button type="button" data-toggle="modal" data-target="#modalGoal"  class="dropdown-item"> Goal()</button>
                                                        <button type="button" data-toggle="modal" data-target="#"  class="dropdown-item"> Summarize()</button>
                                                        <button type="button" data-toggle="modal" data-target="#"  class="dropdown-item"> Crediting()</button> 

                                                    </div>

                                                </div> 

                                            </div>
                                             <button  type="button" onclick="" class="btn btn-outline-primary btn-xx ml-2">Help </button>
                                        </div>
                                           <%--  <button  type="button" onclick="" class="btn btn-light btn-xx ml-2"><i class="icon-menu "></i></button>--%>
                                             
                                           
                                        
                                        
                                        <!-- Actions -->
                                        <div class="table-responsiv ">
                                             <div id="div_Tbody" runat="server">
                                          <%--  <table id="TableCommissionRule" runat="server"  class="table table-actions">
                                               

                                                
                                             <%--   <tbody>
                                                    <tr class="CommissionRule">
                                                        <td class="d-flex text-right float-right" >
                                                            <span class="my-auto mr-2">Rule </span>
                                                            <input value="1" type="text" class="form-control rule-box RuleOrder" / >
                                                        </td>
                                                        
                                                       <td  colspan="3" class="Name"  >
                                                           <div class="d-flex">
                                                                 <input type="text" class="form-control RuleName" placeholder="Commission Template RuleName" />
                                                           <button type="button" onclick="deleteTbody(this)" class="btn btn-outline-danger btn-xxx my-auto ml-2"><i class="icon-cross"></i></button>
                                                           </div>
                                                          
                                                        </td>
                                                    </tr>
                                                    <tr class="RuleCondition">
                                                        <td class="text-right">
                                                            
                                                        </td>
                                                         <td>
                                                             <div class="d-flex">
                                                                 <b class="my-auto mr-1">IF</b>
                                                                 <select  class="form-control TableNames">
                                                                <option>-select one-</option>
                                                                <option>Customer</option>
                                                                <option>Product </option>
                                                                <option>Transaction</option>
                                                                <option>Recipient</option>
                                                                <option>Variables</option>
                                                                <option>Territory</option>
                                                            </select>
                                                             </div>
                                                            
                                                        </td>
                                                        <td>
                                                            <select class="form-control TableValues">
                                                                <option>-select one-</option>
                                                                <option>Product ID 1</option>
                                                                <option>Product ID 2</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select  class="form-control Operator">
                                                                <option>-select one-</option>
                                                                <option>equals</option>
                                                                <option>not equals</option>
                                                                <option>less than</option>
                                                                <option>greater than</option>
                                                                <option>less than or equal to</option>
                                                                <option>greater than or equal to</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input  type="text" class="form-control ConditionValue" />
                                                        </td>
                                                        <td>
                                                            <select onchange="addCondition(this)" class="form-control Condition">
                                                                <option value="0">-select one-</option>
                                                                <option value="1">AND</option>
                                                                <option value="2"> OR</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                          
                                                        </td>
                                                    </tr>
                                                   <%-- <tr class="RuleAction">
                                                        <td class="text-right" colspan="2"> </td>
                                                        <td colspan="3">
                                                            <div class="d-flex"> 
                                                                <b class="my-auto mr-2"> DO</b>
                                                                <input type="text" class="form-control Do tokenfield-typeahead" />
                                                                  
                                                                  <button type="button" onclick="deleteRow(this)" class="btn btn-outline-danger btn-xxx my-auto ml-2"><i class="icon-cross"></i></button>
                                                                <button type="button" onclick="addDo(this)" class="btn btn-outline-primary btn-xxx my-auto ml-2"><i class="icon-plus2"></i></button>
                                                            </div>

                                                        </td> 
                                                        <td></td>
                                                        <td>  </td>

                                                    </tr>
                                                  
                                                </tbody>
                                            </table>--%>
                                                 </div>
                                            <%--<button type="button" onclick="insertRules()">Check</button>--%>
                                        </div>
                                        <div class="form-group col-md-12 d-flex justify-content-end">
                                               <button type="button" onclick="Tab1()" class="btn btn-primary d-none">PREVIOUS</button>
                                               <button id="btn_SaveRule" runat="server" type="button" onclick="Tab2(0)" class="btn btn-primary">NEXT</button>
                                            <button id="btn_UpdateRule" runat="server" type="button" onclick="UpdateRules()" class="btn btn-primary">Update</button>
                                                </div>
                                        <!-- /Actions -->
                                     
                                    </div>
                                    <div class="tab-pane fade  " id="Qualifying-Recipients">
                                         <div class="row">
                                            <div class="form-group col-md-6">
                                                <h6>Assign Recipients</h6>
                                                <select id="DdAssignRecipient" runat="server" onchange="assignRecipientBy(this)" class="form-control">
                                                    <option value="0">-select type-</option>
                                                    <option value="1">Select Invidiual Recipients</option>
                                                     <option value="2">Select  Recipients By Conditions</option>
                                                </select>
                                             
                                                
                                            </div>
                                             <div runat="server" class="col-md-12 d-none" id="Individual_Recipients">
                                                 <div class="table-responsive">
                                                <table id="table-Individual-Recipient"  class="table datatable-basic text-center" >
                                                    <thead>
                                                        <tr>
                                                            <th>Select Recipients </th>
                                                            <th>Name</th>
                                                            <th>Role</th>
                                                            <th>Manager</th>
                                                            <th>Territory</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:PlaceHolder ID="ph_RecipientListtr" runat="server"></asp:PlaceHolder>
                                                       
                                                    </tbody>
                                                </table>
                                                </div>
                                             </div>
                                            <div runat="server" class="form-group col-md-12 d-none" id="Condition_Recipients">
                                               
                                                <table id="table-Condition-Recipient"  class="table custom-panel">
                                                    <thead class="text-center">
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Operator</th>
                                                            <th>Role</th>
                                                            <th>Condition</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:PlaceHolder ID="ph_RecipientConditionTr" runat="server"></asp:PlaceHolder>
                                                       <%-- <tr class="RecipientCondition" >
                                                            <td>
                                                                <select class="form-control DdRecipientType">
                                                                    <option>-- select type --</option>
                                                                    <option>Recipient Type 1</option>
                                                                    <option>Recipient Type 2  -</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="form-control DdRecipientOperator">
                                                                    <option>-- select operator --</option>
                                                                    <option>equal</option>
                                                                    <option>not equal</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                
                                                                <select class="form-control DdRecipientRole">
                                                                    <option>-- select type --</option>
                                                                    <option>Sales Rep</option>
                                                                    <option>Manager</option>
                                                                    <option>Sales Person</option>
                                                                </select>
                                                            </td>
                                                             <td>
                                                                <select class="form-control DdRecipientCondition">
                                                                    <option value="0">-- select condition --</option>
                                                                    <option value="1">AND</option>
                                                                    <option value="2">OR</option>
                                                                </select>
                                                            </td>
                                                            <td><button type="button" onclick="addRecipientCondition(this)" class="btn btn-primary btn-xx"><i class="icon-plus22"></i></button></td>
                                                        </tr>--%>
                                                    </tbody>
                                                </table>
                                            </div>
                                               <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab1()" class="btn btn-primary">PREVIOUS</button>
                                               <button id="btn_SaveRecipient" runat="server" type="button" onclick="Tab3()" class="btn btn-primary">NEXT</button>
                                                     <button id="btn_UpdateRecipient" runat="server" type="button" onclick="UpdateRecipients()" class="btn btn-primary">Update</button>
                                                </div>
                                </div>
                                    </div>
                                    <div class="tab-pane fade  " id="Options">
                                        <div class="row">
                                             <div class="form-group  col-md-6" >
                                                <label style="white-space: nowrap; " class="">Effective Date<span class="text-danger">*</span></label>
                                                 <input type="date" id="dp_StartDate" runat="server" class="form-control"/>
                                               <%-- <input type="date" id="dp_EffectiveDate" runat="server" required="required" class="form-control required " >--%>
                                             </div>
                                            <div class="form-group  col-md-6" >
                                                <label style="white-space: nowrap; " class="">End Date<span class="text-danger">*</span></label>
                                                 <input type="date" id="dp_EndDate" runat="server" class="form-control"/>
                                              <%--  <input type="date" id="dp_EndDate" runat="server" required="required" class="form-control required " >--%>
                                             </div>
                                            
                                        </div>
                                        <!--WYSIWYG-->
                                        <div class="row">
                                            <div class=" col-md-12">
                                                <div class="d-flex ">
                                                    <label>Description</label> <input id="chb_OnAgree" runat="server" class="ml-2 mr-2" type="checkbox" /> Show on Agreement
                                                    <div class="d-none"><button id="save" type="button" class="btn btn-primary btn-xx"><i class="icon-eye"></i></button></div>
                                                </div>
                                                <div class="d-none">
                                                    <button type="button" id="edit" class="btn btn-primary"><i class="icon-pencil3 mr-2"></i> Edit</button>
							                        <button type="button" id="save1" class="btn btn-success"><i class="icon-checkmark3 mr-2"></i> View Document Format</button>
                                                </div>
							
                                                <script type="text/javascript">$(document).ready(function () { $('#edit').click(); });</script>
						</div>
                        <div class="col-md-12">
                            <div runat="server" clientIdMode="static" id="txt_DescWYSWYG"  class="click2edit mt-2">
							<p>Type what you want to display</p>
						</div>
                        </div>
                                                <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab2(1)" class="btn btn-primary">PREVIOUS</button>
                                                    <button id="btn_UpdateOptions" runat="server" visible="false" type="button" onclick="UpdateCommissionDetails()" class="btn btn-primary">UPDATE</button>
                                               <button id="btn_SumbitOverAllCommission" runat="server" type="button" onclick="UpdateCommissionDetails()" class="btn btn-primary">SUBMIT</button>
                                                </div>
                            
						
                                        </div>
                                    </div>
                            </div>
                          <!--/Nav Tabs for commission-->
                    </div>
                    </div>
                </div>
                
                    
                </div>
    </div>
        </div>
</div>

                <!-- Goal modal -->
                <div id="modalGoal" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Goal</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                 <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Goal-list" class="nav-link active" data-toggle="tab">Goal List</a></li>
									<li class="nav-item"><a href="#Goal-new" class="nav-link" data-toggle="tab">New Goal</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Goal-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Goal</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Goal 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Goal 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Goal 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Goal-new">
										 <div class="row">
                                           
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Goal Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <table style="background-color: #80808040;"  class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>Period</th><th>Goal Value</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="set-goal">
                                                                <tr>
                                                                    <td class="tr-count">1</td>
                                                                   <td>
                                                                        <div class="">
                                                                         <input name="period" placeholder="period" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="">
                                                                         <input name="goal" placeholder="goal" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td class="td-btn">
                                                                        <div class="d-inline-block add-goal">
                                                                            <button type="button" class="btn btn-primary btn-sm add-goal-btn btn-xx mr-2" onclick="addGoal()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="addGoal(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>  
                                                                   </td>
                                                                </tr>
                                                               
                                                            </tbody>
                                                        </table>
                                             <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveRange()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div> 
									</div>

									
								</div>
                                   
                            </div>

                           
                        </div>
                    </div>
                </div>
                <!-- /goal modal -->
    
                <!-- Range modal -->
                <div id="modalRange" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Range</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                 <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Range-list" class="nav-link active" data-toggle="tab">Range List</a></li>
									<li class="nav-item"><a href="#Range-new" class="nav-link" data-toggle="tab">New Range</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Range-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Range</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Range 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Range 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Range 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Range-new">
										 <div class="row">
                                            
                                           
                                            <div class="form-group col-md-12">
                                                    <label>Range Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>From</th><th>To</th><th>Value</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="set-range">
                                                                <tr>
                                                                    <td class="td-count">1</td>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input name="from" placeholder="from" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input name="to" placeholder="to" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input name="value" placeholder="value" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td class="td-btn">
                                                                        <div class="d-inline-block add-range">
                                                                            <button type="button" class="btn btn-primary btn-sm add-range-btn btn-xx mr-2" onclick="addRange()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="addGoal(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>  
                                                                   </td>
                                                                </tr>
                                                              
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveRange()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                           
                        </div>
                    </div>
                </div>
                <!-- /Range modal -->
    
                <!-- override modal -->
                <div id="modalOverride" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Override</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                  <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Override-list" class="nav-link active" data-toggle="tab">Override List</a></li>
									<li class="nav-item"><a href="#Override-new" class="nav-link" data-toggle="tab">New Override</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Override-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Levels</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Levels 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Levels 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Levels 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Override-new">
										 <div class="row">
                                            
                                            
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Level Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr> <th>Level</th><th>%</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="Override-Levels">
                                                               
                                                                <tr>
                                                                    <td class="td-level">
                                                                        <div class="form-group">
                                                                         <label>Level 1</label>
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="%" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    
                                                                   <td class="tr-btn">
                                                                     <div class="d-inline-block add-override-level">
                                                                            <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>    
                                                                   </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveOverride()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                            
                        </div>
                    </div>
                </div>
                <!-- /override modal -->
    
                <!-- split modal -->
                <div id="modalSplit" class="modal fade" tabindex="-1">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;Split</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#split-list" class="nav-link active" data-toggle="tab">Split List</a></li>
									<li class="nav-item"><a href="#split-new" class="nav-link" data-toggle="tab">New Split</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="split-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total recipients</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Split 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Split 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Split 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="split-new">
										 <div class="row">
                                            
                                            
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Split Name  <span class="text-danger">*</span></label>
                                                    <input id="txt_SplitName" type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea id="txt_SplitDesc" class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>Recipient</th><th>%</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="Split-Recipients">
                                                                
                                                                <tr id="split-1">
                                                                    <td class="tr-count">1</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         
                                                                          <select class="form-control recipient-dd" >
                                                                             <option value="">-select recipient-</option>
                                                                             <option value="Raj">Raj</option>
                                                                             <option value="John">John</option>
                                                                             <option value="Bob">Bob</option>
                                                                         </select>
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="%"  name="RecipientPercentage[]"  type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    
                                                                   <td class="tr-btn">
                                                                       <div class="d-inline-block add-recipient">
                                                                            <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>
                                                                  </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveSplit()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>

                                             
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                            <%--<div class="modal-footer">
                              
                               
                            </div>--%>
                        </div>
                    </div>
                </div>
                <!-- /split modal -->

    <script>
        function openSetGoal() {
            $('#openSetgoal').click();
        }
        function openRangeMatrix() {
            $('#openRangeMatrix').click();
        }
        function advProcessRegister() {
            $('#advProcessRegister').click();
        }


    </script>   
</asp:Content>

