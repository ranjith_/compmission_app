﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CompanySetup.aspx.cs" Inherits="Web_CompanySetup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Company Setup</title>
    <style>
        .company-logo{
            height:100px;
            width:200px; 
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
<div class="container-fluid">
<div  id="div_CompanySetupDetails" runat="server" class="row">
     <div class="col-md-12">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Company Setup</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a href="CompanySetup.aspx?isedit=1" class="btn btn-primary btn-sm">Edit</a>
                        <%--<asp:Button ID="btn_Edit" Text="Edit" OnClick="btn_Edit_Click" runat="server" CssClass="btn btn-primary  btn-sm " />--%>
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <asp:PlaceHolder ID="ph_CompanyDetails" runat="server"></asp:PlaceHolder>
            </div>
            </div>
    </div>
</div>
<div id="div_CompanySetupInputForm" visible="false" runat="server" class="row">
   
    <div class="col-md-6">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Company Setup</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                    </div>
                </div>
            </div>

                <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Company Name<span class="text-danger">*</span></label>
                            <input id="txt_CompanyName" runat="server" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Email<span class="text-danger">*</span></label>
                            <input id="txt_Email" runat="server" type="email" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Mobile<span class="text-danger">*</span></label>
                            <input id="txt_Mobile" runat="server"  type="text" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Country</label>
                                        <select id="dd_Country" runat="server" class="form-control">
                                        <option value="0">-- select country --</option>
                                            <option value="1">America</option>
                                            <option value="2">South Africa</option>
                                            <option value="3">India</option>
                                            <option >Pondichery</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>State</label>
                                        <select id="dd_State" runat="server" class="form-control">
                                            <option value="0">-- select state --</option>
                                            <option value="1">Tamilnadu</option>
                                            <option value="2">Kerala</option>
                                            <option value="3">Andhra</option>
                                            <option value="4">Srilanka</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>City</label>
                                        <select id="dd_City" runat="server" class="form-control">
                                            <option value="0">-- select city --</option>
                                            <option value="12">Chennai</option>
                                            <option value="13">Madurai</option>
                                            <option value="14">Cuddalore</option>
                                            <option value="15">Villupuram</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group ">
                                        <label>Currency</label>
                                        <select id="dd_Currency" runat="server" class="form-control">
                                            <option value="">-- select Currency --</option>
                                            <option>U.S. Dollar (USD)</option>
                                            <option>European Euro (EUR)</option>
                                            <option>Canadian Dollar (CAD)</option>
                                            <option>Japanese Yen (JPY)</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Time Zone</label>
                                        <select id="dd_TimeZone" runat="server" class="form-control">
                                            <option value="">-- select time Zone --</option>
                                            <option>Alpha Time Zome UTC+1</option>
                                            <option>ACT UTC-5</option>
                                            <option>ACST UTC+9.30</option>
                                                    
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                            <label>Address 1<span class="text-danger">*</span></label>
                                            <textarea id="txt_Address1" runat="server" rows="2" cols="3" class="form-control" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                            <label>Address 2 <small>(optional)</small></label>
                                            <textarea id="txt_Address2" runat="server"  rows="2" cols="3" class="form-control" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Website</label>
                                        <input id="txt_Website" runat="server" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class=" col-md-12">
                                
                        <div><h5>Apear in Reports</h5></div>
                        <hr>
                    </div>
                      <div class="col-md-6">
                        <div class="form-group ">
                            <label>Company Name<span class="text-danger">*</span></label>
                            <input id="txt_AprCompanyName" runat="server" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Email<span class="text-danger">*</span></label>
                            <input id="txt_AprEmail" runat="server" type="email" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Mobile<span class="text-danger">*</span></label>
                            <input id="txt_AprMobile" runat="server"  type="text" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Country</label>
                                        <select id="dd_AprCountry" runat="server" class="form-control">
                                         <option value="1">America</option>
                                            <option value="2">South Africa</option>
                                            <option value="3">India</option>
                                            <option >Pondichery</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>State</label>
                                        <select id="dd_AprState" runat="server" class="form-control">
                                             <option value="0">-- select state --</option>
                                            <option value="1">Tamilnadu</option>
                                            <option value="2">Kerala</option>
                                            <option value="3">Andhra</option>
                                            <option value="4">Srilanka</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>City</label>
                                        <select id="dd_AprCity" runat="server" class="form-control">
                                            <option value="0">-- select city --</option>
                                            <option value="12">Chennai</option>
                                            <option value="13">Madurai</option>
                                            <option value="14">Cuddalore</option>
                                            <option value="15">Villupuram</option>
                                        </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group ">
                                            <label>Address 1<span class="text-danger">*</span></label>
                                            <textarea id="txt_AprAddress1" runat="server" rows="2" cols="3" class="form-control" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                            <label>Address 2 <small>(optional)</small></label>
                                            <textarea id="txt_AprAddress2" runat="server"  rows="2" cols="3" class="form-control" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Website</label>
                                        <input id="txt_AprWebsite" runat="server" type="text" class="form-control" >
                        </div>
                    </div>
                </div>                    
                                
                </div>
        </div>    
    </div>
    <!--file--->
        <div class="col-md-6">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Company Setup</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                    </div>
                </div>
            </div>

                <div class="card-body">

                    <div class="form-group col-md-4">
                        <asp:HiddenField ID="hdn_LogoName" runat="server" />
                        <img id="img_Logo" runat="server" class="img-fluid  border border-dark p-2 company-logo" />
                        <span id="err_NoLogo" runat="server" visible="false" class="text-danger">Please Upload a Logo</span>
                    </div>
                <div class="form-group col-md-12">
                                        <label>Upload Logo</label>
                                        <asp:FileUpload ID="fu_CompanyLogo" runat="server" CssClass="form-control" />
                </div>
                </div>
        </div>    
    </div>
</div>
</div>   
                       
    
</asp:Content>

