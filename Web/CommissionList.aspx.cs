﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionPlan : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        loadCommissionList();
    }
    private void loadCommissionList()
    {
        string str_Sql = "select *,format(a.Startdate,'MM//dd/yyyy') as sDate,format(a.EndDate,'MM//dd/yyyy') as eDate,(select FrequencyName from CalendarSetup where CalendarKey=a.Frequency) as FrequencyName   from Commission a order by CreatedOn Desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_CommisssionListTr, "DivCommissionListTr.txt", "");
    }
    [WebMethod]
    public static string deleteCommission(string CommissionKey)
    {
        try
        {
            DataAccess DA = new DataAccess();
            SessionCustom Sc = new SessionCustom();
            String str_sql = "p_DeleteCommission";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);

            DataSet ds = DA.GetDataSet(cmd);

            string returnValue = ds.Tables[0].Rows[0]["Valid"].ToString();
            return returnValue;
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}