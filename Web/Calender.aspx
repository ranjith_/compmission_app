﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="Calender.aspx.cs" Inherits="Web_Calender" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Calender</title>
 
    <style>
        /*#Period >thead>tr>th:nth-child(4),#Period >tbody>tr>td:nth-child(4){
        background-color:#4FC3F7;
        }
        #Period >thead>tr>th:nth-child(5),#Period >tbody>tr>td:nth-child(5){
        background-color:#AED581;
        }
        #Period >thead>tr>th:nth-child(6),#Period >tbody>tr>td:nth-child(6){
        background-color:#FFB74D;
        }
        #Period >thead>tr>th:nth-child(7),#Period >tbody>tr>td:nth-child(7){
        background-color:#4DB6AC;
        }*/

          .select-custom{
               width:40%;
          border-radius: 4px;
    border: 1px solid #bbb7b7;
    padding: 5px;
    margin: 8px;
    font-size: 16px;
    color: #777777;
        }
        .input-custom{
          
    width:11%;
         border-radius: 4px;
    border: 1px solid #bbb7b7;
    padding: 3px;
    margin: 8px;
    font-size: 16px;
    color: #777777;
        }
        .f-1-5em{
            font-size:1.5em;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $('button.close').click(function () {
                window.location.href = window.location.href;
            });


        });

        function showCount() {
            let type = $("#ddl_Frequency option:selected").text();
            if (type != '') {
                if (type == 'BiMonthly') {
                    $('#div_Freq').addClass('d-none');
                }
                else {
                    $('#div_Freq').removeClass('d-none');

                    $('#FrequencyName').text($('#txt_FreqName').val());
                    $('#FrequencyType').text(type);
                }
            }
           
        }
        
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
             <div class="container-fluid">
                            <div class="row">
                              
                                 <!--Input Form-->
                                 <div id="div_InputForm"  class="col-lg-12 col-md-12 " runat="server">
                                        <!-- Basic layout-->
                            
                                    <div class="card ">
                                        <div class="card-header header-elements-inline">
                                            <h5 class="card-title">Calendar Setup</h5>
                                            <div class="header-elements">
                                                <div class="list-icons">
                                                      <button type="button" id="btn_Save" runat="server" onserverclick="btn_Save_ServerClick" class="btn btn-primary">Generate</button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-3">
                                                <label>Frequency Name <span class="text-danger">*</span></label>
                                                <input type="text" required id="txt_FreqName" runat="server" class="form-control" />
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Start Date<span class="text-danger">*</span></label>
                                                <input runat="server"   id="dp_StartDate" type="date" name="" required class="form-control pickadate-limits-start " > 
                                              
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>End Date<span class="text-danger">*</span></label>
                                               <%-- <input onchange="checkFascalYear(this)" id="dp_EndDate" type="date" class="form-control" >--%>
                                                 <input runat="server"  id="dp_EndDate" type="date" name="" required class="form-control pickadate-limits-end " > 
                                            </div>
                                                 <div class="form-group col-md-3">
                                                      <label>  Frequency type <span class="text-danger">*</span></label>
                                                         <asp:DropDownList onchange="showCount(this);"   runat="server" ID="ddl_Frequency" required CssClass=" form-control">
                                                        </asp:DropDownList>
                                                     </div>
                                            
                                            <div class="form-group col-md-6 d-none" id="div_Freq">
                                               
                                             <h6 class="text-muted pl-3 ">
                                                Custom Configuration :
                                                 <span id="FrequencyName">Frequency name  </span>
                                                calendar from start date plus
                                                     
                                                   <input  type="number" id="txt_TypeCount" runat="server" min="0" max="24" value="1" class="input-custom ignore">
                                                     <span id="FrequencyType">Week</span>
                                                </h6>

                                            </div>
                                          
    
                                            
                                        </div>
                                        </div>
                                    </div>
                               
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->

                                <div  class="col-lg-12 col-md-12" >
                                     <div class="card ">
                                            <div class="card-header header-elements-inline">
                                                <h5 class="card-title">Period</h5>
                                                <div class="header-elements">
                                                    <div class="list-icons">
                                                      
                                                      
                                                        <a class="list-icons-item" data-action="collapse"></a>
                                                        <a class="list-icons-item" data-action="reload"></a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="card-body">
                                            
                                           <div class="table-responsive">
                            <table class="table datatable-basic " id="Period">
                                <thead>
                                    <tr>
                                        <th>Frequency Name</th>
                                        <th> Start Date</th>
                                        <th> End Date</th>
                                        <th> Frequency type</th>
                                        <th> Created On</th>
                                       
                                       
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_PeriodListTr" runat="server"></asp:PlaceHolder>
                                       <%-- <tr>
                                            <td> 1</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>43</td>
                                            <td>86</td>
                                            <td>83</td>
                                            <td>36</td>
                                        </tr>
                                        <tr>
                                            <td> 2</td>
                                            <td>03-06-2019</td>
                                            <td>08-06-2019</td>
                                            <td>43</td>
                                            <td>76</td>
                                            <td>93</td>
                                            <td>34</td>
                                        </tr>
                                        <tr>
                                            <td> 3</td>
                                            <td>11-06-2019</td>
                                            <td>16-06-2019</td>
                                            <td>48</td>
                                            <td>76</td>
                                            <td>89</td>
                                            <td>46</td>
                                        </tr>
                                        <tr>
                                            <td> 4</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>43</td>
                                            <td>86</td>
                                            <td>83</td>
                                            <td>36</td>
                                        </tr>
                                        <tr>
                                            <td> 5</td>
                                            <td>01-06-2019</td>
                                            <td>09-06-2019</td>
                                            <td>83</td>
                                            <td>66</td>
                                            <td>33</td>
                                            <td>76</td>
                                        </tr>--%>
                                       
                                </tbody>
                            </table>


                                   </div>
                            

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    <asp:HiddenField ID="hdnPeriodValues" runat="server" />
</asp:Content>

