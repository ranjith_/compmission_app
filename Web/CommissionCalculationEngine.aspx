﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="CommissionCalculationEngine.aspx.cs" Inherits="Web_CommissionCalculationEngine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission Calculation Engine</title>
   
    <style>
        .svg-cont{
            height: 100%;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
  -webkit-justify-content: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center; 
        }
        .machine {
  width: 60vmin;
  fill: #3eb049; }

.small-shadow, .medium-shadow, .large-shadow {
  fill: rgba(0, 0, 0, 0.05); }

.small {
  -webkit-animation: counter-rotation 2.5s infinite linear;
	   -moz-animation: counter-rotation 2.5s infinite linear;
	     -o-animation: counter-rotation 2.5s infinite linear;
	        animation: counter-rotation 2.5s infinite linear;
  -webkit-transform-origin: 100.136px 225.345px;
      -ms-transform-origin: 100.136px 225.345px;
          transform-origin: 100.136px 225.345px; }

.small-shadow {
  -webkit-animation: counter-rotation 2.5s infinite linear;
	   -moz-animation: counter-rotation 2.5s infinite linear;
	     -o-animation: counter-rotation 2.5s infinite linear;
	        animation: counter-rotation 2.5s infinite linear;
  -webkit-transform-origin: 110.136px 235.345px;
      -ms-transform-origin: 110.136px 235.345px;
          transform-origin: 110.136px 235.345px; }

.medium {
  -webkit-animation: rotation 3.75s infinite linear;
	   -moz-animation: rotation 3.75s infinite linear;
	     -o-animation: rotation 3.75s infinite linear;
	        animation: rotation 3.75s infinite linear;
  -webkit-transform-origin: 254.675px 379.447px;
      -ms-transform-origin: 254.675px 379.447px;
          transform-origin: 254.675px 379.447px; }

.medium-shadow {
  -webkit-animation: rotation 3.75s infinite linear;
	   -moz-animation: rotation 3.75s infinite linear;
	     -o-animation: rotation 3.75s infinite linear;
	        animation: rotation 3.75s infinite linear;
  -webkit-transform-origin: 264.675px 389.447px;
      -ms-transform-origin: 264.675px 389.447px;
          transform-origin: 264.675px 389.447px; }

.large {
  -webkit-animation: counter-rotation 5s infinite linear;
     -moz-animation: counter-rotation 5s infinite linear;
	     -o-animation: counter-rotation 5s infinite linear;
        	animation: counter-rotation 5s infinite linear;
  -webkit-transform-origin: 461.37px 173.694px;
      -ms-transform-origin: 461.37px 173.694px;
          transform-origin: 461.37px 173.694px; }

.large-shadow {  
  -webkit-animation: counter-rotation 5s infinite linear;
	   -moz-animation: counter-rotation 5s infinite linear;
	     -o-animation: counter-rotation 5s infinite linear;
	        animation: counter-rotation 5s infinite linear;
  -webkit-transform-origin: 471.37px 183.694px;
      -ms-transform-origin: 471.37px 183.694px;
          transform-origin: 471.37px 183.694px; }

@-webkit-keyframes rotation {
    from {-webkit-transform: rotate(0deg);}
    to   {-webkit-transform: rotate(359deg);}
}
@-moz-keyframes rotation {
    from {-moz-transform: rotate(0deg);}
    to   {-moz-transform: rotate(359deg);}
}
@-o-keyframes rotation {
    from {-o-transform: rotate(0deg);}
    to   {-o-transform: rotate(359deg);}
}
@keyframes rotation {
    from {transform: rotate(0deg);}
    to   {transform: rotate(359deg);}
}

@-webkit-keyframes counter-rotation {
    from {-webkit-transform: rotate(359deg);}
    to   {-webkit-transform: rotate(0deg);}
}
@-moz-keyframes counter-rotation {
    from {-moz-transform: rotate(359deg);}
    to   {-moz-transform: rotate(0deg);}
}
@-o-keyframes counter-rotation {
    from {-o-transform: rotate(359deg);}
    to   {-o-transform: rotate(0deg);}
}
@keyframes counter-rotation {
    from {transform: rotate(359deg);}
    to   {transform: rotate(0deg);}
}
    </style>
    <script>
        function loadOptionPeriod(val) {
            let FiscalYear = $('#ddl_FiscalYear').val();
            $.ajax({
                type: "POST",
                url: "../Web/CommissionReportStatement.aspx/loadOptionPeriod",
                data: "{CalendarKey:'" + FiscalYear + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data.d);
                    $('#dd_FiscalPeriod').append(data.d);
                    if (val == 1) {
                        let PeriodKeySelected = $('#hdn_PeriodKey').val();
                        $('#dd_FiscalPeriod').val(PeriodKeySelected);
                        $('#dd_FiscalPeriod').select2().trigger('change');
                    }
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function ExecuteCommissionCalculation() {
            let PeriodKey = $('#dd_FiscalPeriod').val();
            let CommissionKey = $('#ddl_Commission').val();


            $.ajax({
                type: "POST",
                url: "../Web/CommissionCalculationEngine.aspx/ExecuteCommissionCalculation",
                data: "{PeriodKey:'" + PeriodKey + "',CommissionKeys:'" + CommissionKey + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    
                    if (data.d == "Executed") {
                       // alert("Calculation Ended");
                        var d = new Date();
                        $('#ProcessEndTime').text(d.toLocaleString());
                        $('#CalcEnd').removeClass('d-none');
                        $('#btn_Calc').html('Calculate');
                        $('#btn_Calc').removeClass('disabled');
                       //$('.large,.small-shadow,.small, .medium-shadow,.medium, .large-shadow').css("animation", "none");
                        $(".large,.small-shadow,.small, .medium-shadow,.medium, .large-shadow").css("animation-play-state", "paused");
                        $('#SVG-Gear-Engine').addClass('d-none');
                    }
                    else {
                      //  alert(data.d);
                        console.log(data.d);
                        alertNotify('Calculation Failed', 'Please contact admin ', 'bg-danger border-danger');
                        $('#CalcError').text('Error : ' + data.d );
                        $('#CalcError').removeClass('d-none');
                        $(".large,.small-shadow,.small, .medium-shadow,.medium, .large-shadow").css("animation-play-state", "paused");
                      
                        $('#btn_Calc').html('Calculate');
                        $('#btn_Calc').removeClass('disabled');
                    }
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function CalculateCommission(element) {
            let PeriodKey = $('#dd_FiscalPeriod').val();
            let CommissionKey = $('#ddl_Commission').val();

            if (PeriodKey == "" && CommissionKey == "") {
                alertNotify('Error', 'Please select the period and Commission !', 'bg-danger border-danger');
                // return false;
            }
            else {
                $('#SVG-Gear-Engine').removeClass('d-none');
                $('#CalcError').addClass('d-none');
                $('#CalcDetail').removeClass('d-none');
                $('#CalcEnd').addClass('d-none');
                $(".large,.small-shadow,.small, .medium-shadow,.medium, .large-shadow").css("animation-play-state", "running");
                $(element).addClass('disabled');
                $(element).html('<i class="icon-spinner2 spinner mr-1"></i>  Calculating');
                var d = new Date();
                $('#ProcessStartTime').text(d.toLocaleString());
                ExecuteCommissionCalculation();
            }
           
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <!--Data print-->
                <div class="card border-left-2 border-left-success">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Calculation Engine <small></small></h5>
                        <div class="header-elements d-none">
                            <div class="list-icons  d-none">
                                                       
                                <a class="btn btn-primary  btn-sm " href="commission.aspx"> Edit</a>
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                                        
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row m-1">
                                     <div class="col-md-4 form-group">
                                       
                                            <label>Fiscal Year</label>
									        <asp:DropDownList CssClass="form-control-select2" onchange="loadOptionPeriod(2);" ClientIDMode="Static" ID="ddl_FiscalYear" runat="server"></asp:DropDownList>
								        
                                    </div>
                                    <div class="col-md-4 form-group">
                                       <label>Period </label>
									        <select  class="form-control-select2" id="dd_FiscalPeriod" >
										        <option value="">select period</option>
										
									        </select>
                                    </div>
                                    <div class="col-md-8 form-group">
                                       
									        <label><span class="font-weight-semibold">Select Commissions</span></label>
                                            <asp:DropDownList CssClass="form-control multiselect-select-all-filtering" multiple="multiple" ID="ddl_Commission" runat="server"></asp:DropDownList>
									
                                    </div>
                                    <div class="col-md-8  text-right">
                                        <button id="btn_Calc" class="btn btn-success btn-sm" onclick="CalculateCommission(this)" type="button">Calculate</button>
                                    </div>
                                      <div class="col-md-8 text-center d-none" id="CalcDetail">
                                        <h6 class="text-success"><i class="icon-cogs"></i> Calculation Process Start at <span id="ProcessStartTime"></span></h6>
                                        <h6 class="text-success d-none" id="CalcEnd"> <i class="icon-cogs"></i> Calculation Process End at <span id="ProcessEndTime"></span></h6>
                                        <h5 class="text-danger d-none mt-1" id="CalcError"></h5>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-3 ">
                              
                                    <div class="svg-cont d-none" id="SVG-Gear-Engine">
                                         
                                     <svg class="machine"xmlns="http://www.w3.org/2000/svg" x="0px" y="50px" viewBox="0 0 645 526">
      <defs/>
      <g>
        <path  x="-173,694" y="-173,694" class="large-shadow" d="M645 194v-21l-29-4c-1-10-3-19-6-28l25-14 -8-19 -28 7c-5-8-10-16-16-24L602 68l-15-15 -23 17c-7-6-15-11-24-16l7-28 -19-8 -14 25c-9-3-18-5-28-6L482 10h-21l-4 29c-10 1-19 3-28 6l-14-25 -19 8 7 28c-8 5-16 10-24 16l-23-17L341 68l17 23c-6 7-11 15-16 24l-28-7 -8 19 25 14c-3 9-5 18-6 28l-29 4v21l29 4c1 10 3 19 6 28l-25 14 8 19 28-7c5 8 10 16 16 24l-17 23 15 15 23-17c7 6 15 11 24 16l-7 28 19 8 14-25c9 3 18 5 28 6l4 29h21l4-29c10-1 19-3 28-6l14 25 19-8 -7-28c8-5 16-10 24-16l23 17 15-15 -17-23c6-7 11-15 16-24l28 7 8-19 -25-14c3-9 5-18 6-28L645 194zM471 294c-61 0-110-49-110-110S411 74 471 74s110 49 110 110S532 294 471 294z"/>
      </g>
      <g>
        <path x="-136,996" y="-136,996" class="medium-shadow" d="M402 400v-21l-28-4c-1-10-4-19-7-28l23-17 -11-18L352 323c-6-8-13-14-20-20l11-26 -18-11 -17 23c-9-4-18-6-28-7l-4-28h-21l-4 28c-10 1-19 4-28 7l-17-23 -18 11 11 26c-8 6-14 13-20 20l-26-11 -11 18 23 17c-4 9-6 18-7 28l-28 4v21l28 4c1 10 4 19 7 28l-23 17 11 18 26-11c6 8 13 14 20 20l-11 26 18 11 17-23c9 4 18 6 28 7l4 28h21l4-28c10-1 19-4 28-7l17 23 18-11 -11-26c8-6 14-13 20-20l26 11 11-18 -23-17c4-9 6-18 7-28L402 400zM265 463c-41 0-74-33-74-74 0-41 33-74 74-74 41 0 74 33 74 74C338 430 305 463 265 463z"/>
      </g>
      <g >
        <path x="-100,136" y="-100,136" class="small-shadow" d="M210 246v-21l-29-4c-2-10-6-18-11-26l18-23 -15-15 -23 18c-8-5-17-9-26-11l-4-29H100l-4 29c-10 2-18 6-26 11l-23-18 -15 15 18 23c-5 8-9 17-11 26L10 225v21l29 4c2 10 6 18 11 26l-18 23 15 15 23-18c8 5 17 9 26 11l4 29h21l4-29c10-2 18-6 26-11l23 18 15-15 -18-23c5-8 9-17 11-26L210 246zM110 272c-20 0-37-17-37-37s17-37 37-37c20 0 37 17 37 37S131 272 110 272z"/>
      </g>
      <g>
        <path x="-100,136" y="-100,136" class="small" d="M200 236v-21l-29-4c-2-10-6-18-11-26l18-23 -15-15 -23 18c-8-5-17-9-26-11l-4-29H90l-4 29c-10 2-18 6-26 11l-23-18 -15 15 18 23c-5 8-9 17-11 26L0 215v21l29 4c2 10 6 18 11 26l-18 23 15 15 23-18c8 5 17 9 26 11l4 29h21l4-29c10-2 18-6 26-11l23 18 15-15 -18-23c5-8 9-17 11-26L200 236zM100 262c-20 0-37-17-37-37s17-37 37-37c20 0 37 17 37 37S121 262 100 262z"/>
      </g>
      <g>
        <path x="-173,694" y="-173,694" class="large" d="M635 184v-21l-29-4c-1-10-3-19-6-28l25-14 -8-19 -28 7c-5-8-10-16-16-24L592 58l-15-15 -23 17c-7-6-15-11-24-16l7-28 -19-8 -14 25c-9-3-18-5-28-6L472 0h-21l-4 29c-10 1-19 3-28 6L405 9l-19 8 7 28c-8 5-16 10-24 16l-23-17L331 58l17 23c-6 7-11 15-16 24l-28-7 -8 19 25 14c-3 9-5 18-6 28l-29 4v21l29 4c1 10 3 19 6 28l-25 14 8 19 28-7c5 8 10 16 16 24l-17 23 15 15 23-17c7 6 15 11 24 16l-7 28 19 8 14-25c9 3 18 5 28 6l4 29h21l4-29c10-1 19-3 28-6l14 25 19-8 -7-28c8-5 16-10 24-16l23 17 15-15 -17-23c6-7 11-15 16-24l28 7 8-19 -25-14c3-9 5-18 6-28L635 184zM461 284c-61 0-110-49-110-110S401 64 461 64s110 49 110 110S522 284 461 284z"/>
      </g>
      <g>
        <path x="-136,996" y="-136,996" class="medium" d="M392 390v-21l-28-4c-1-10-4-19-7-28l23-17 -11-18L342 313c-6-8-13-14-20-20l11-26 -18-11 -17 23c-9-4-18-6-28-7l-4-28h-21l-4 28c-10 1-19 4-28 7l-17-23 -18 11 11 26c-8 6-14 13-20 20l-26-11 -11 18 23 17c-4 9-6 18-7 28l-28 4v21l28 4c1 10 4 19 7 28l-23 17 11 18 26-11c6 8 13 14 20 20l-11 26 18 11 17-23c9 4 18 6 28 7l4 28h21l4-28c10-1 19-4 28-7l17 23 18-11 -11-26c8-6 14-13 20-20l26 11 11-18 -23-17c4-9 6-18 7-28L392 390zM255 453c-41 0-74-33-74-74 0-41 33-74 74-74 41 0 74 33 74 74C328 420 295 453 255 453z"/>
      </g>
    </svg>
                                   
                                    
                                    </div>
                                
                                    </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                                            
                </div>
                
            </div>
        </div>
    </div>
    
    
</asp:Content>

