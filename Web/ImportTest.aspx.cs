﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using System.Data.OleDb;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

public partial class Web_ImportTest : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    Common Com;
   
    DataAccessExcel DAE;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        //this.SC = new SessionCustom(true);
        this.Com = new Common();
    }

   
    protected void Upload_Click(object sender, EventArgs e)
    {

      //  WorkFlow WF = new WorkFlow();
        DataTable dtResult = null;
        string str_Sql = "", str_FilePath = "", str_FileName = "", str_ImportSeq = "";
        if (FileUpload1.PostedFile.FileName == "")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please upload the File');", true);
            return;
        }
        try
        {
            str_FileName = Guid.NewGuid() + "_" + FileUpload1.PostedFile.FileName;

            string str_Root = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "UploadFile/BatchFiles/";
            str_FilePath = str_Root + str_FileName;
            string str_FileExt = Path.GetExtension(str_FilePath);
            if (!Directory.Exists(str_Root))
            {
                Directory.CreateDirectory(str_Root);
            }
            FileUpload1.PostedFile.SaveAs(Server.MapPath("~//UploadFile/BatchFiles/" + str_FileName));
            //str_Sql = @"insert into BatchFiles(FileKey,FileName,ActualFileName,CreatedOn,CreatedBy) 
            //            select NEWID(),@FileName,@ActualFileName,GETDATE(),@CreatedBy; select @@identity";
            //SqlCommand sqc = new SqlCommand(str_Sql);
            //sqc.Parameters.AddWithValue("@FileName", str_FileName);
            //sqc.Parameters.AddWithValue("@ActualFileName", FileUpload1.PostedFile.FileName);
            //sqc.Parameters.AddWithValue("@CreatedBy", this.SC.UserKey);
            //DataTable dt = this.DA.GetDataTable(sqc);
            //str_ImportSeq = dt.Rows[0][0].ToString();

            // ScriptManager.RegisterStartupScript(this, GetType(), "CareMB", "alert('File Imported Successfully');", true);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString());
            ScriptManager.RegisterStartupScript(this, GetType(), "CareMB", "alert('" + ex.Message + "');", true);
        }
        this.DAE = new DataAccessExcel(str_FilePath);
        string str_ConnectionString = this.DAE.GetOledbConnectionString();


        using (OleDbConnection objConn = new OleDbConnection(str_ConnectionString))
        {
            //Get Excel Data as DataTable
            objConn.Open();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter oleda = new OleDbDataAdapter();
            DataSet ds = new DataSet();
            DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string sheetName = string.Empty;
            if (dt != null)
            {
                sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
            }
            dtResult = this.DAE.ExcelSheetData(sheetName, "");

            //Insert Record into Batch Table
            str_Sql = "p_InsertBatchFiles";
            SqlCommand sc = new SqlCommand(str_Sql);
            sc.CommandType = CommandType.StoredProcedure;
            sc.Parameters.AddWithValue("@BatchFiles", dtResult);
            sc.Parameters.AddWithValue("@UserKey", this.SC.UserKey);
            sc.Parameters.AddWithValue("@ImportSeq", str_ImportSeq);
            DataSet dt_Res = this.DA.GetDataSet(sc);
            if (dt_Res.Tables[0].Rows.Count > 0)
            {
                string str_Message = dt_Res.Tables[0].Rows[0]["Message"].ToString().Replace("'", "");
                ScriptManager.RegisterStartupScript(this, GetType(), "CareMB", "alert('" + str_Message + "');", true);
                //return;
            }

            //Create WorkFlow for Each Batch
            //if(dt_Res.Tables[1].Rows.Count>0)
            //{
            //    foreach(DataRow dr in dt_Res.Tables[1].Rows)
            //    {
            //        WF.NewRequest("7783EE51-C91F-4D2E-97B6-86405AAB2751", dr["BatchKey"].ToString(), SC.UserKey);
            //    }
            //}


           // Div_loding.Visible = false;
            //objConn.Close();

            ////Upload the file to Azure
            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=careb;AccountKey=L+MzfgFNT+KF1TkzIiVTl2RiLD5+VRyunDEQZ0Xled0B2B4m2W1IS0d27Nffvv2TLGYYPn1ZAbeN4MPzsxd09w==;EndpointSuffix=core.windows.net");
            //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            //CloudBlobContainer container = blobClient.GetContainerReference("caremb/BatchFiles");
            //CloudBlockBlob blockBlob = container.GetBlockBlobReference(str_FileName);
            //using (var fileStream = System.IO.File.OpenRead(str_FilePath))
            //{
            //    blockBlob.UploadFromStream(fileStream);
            //}
        }
    }
}