﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="CompanySettings.aspx.cs" Inherits="Web_CompanySettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Company Settings</title>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/visualization/echarts/echarts.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/demo_pages/user_pages_profile.js"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             changeModalSize('sm');
             $('#ddl_Fascalyear').change(function () {
                 $('#dp_StartDate').val('');
                 $('#dp_EndDate').val('');

                 var year = $('#ddl_Fascalyear').val();
                 changeYear(year);


             });
             $('#a_CompanyLogo').click(function () {
                 changeModalSize('sm');
             });
             $('#ddl_PayFrequency').change(function () {

                 calculatePayperiod();

             });



         });
         function changeModalSize(val) {
             if (val == 'lg') {
                 $('.modal-dialog').removeClass('modal-sm');
                 $('.modal-dialog').addClass('modal-lg');
             }
             else {
               
                 $('.modal-dialog').removeClass('modal-lg');
                 $('.modal-dialog').addClass('modal-sm');
             }
            
         }
         function calculatePayperiod() {
             $('#Period tbody').html('');
             var sDate = $('#dp_StartDate').val();
             var eDate = $('#dp_EndDate').val();
             //console.log(sDate);
             //console.log(eDate);

             var startdate = new Date(sDate);
             console.log(startdate);
             var startdateYear = startdate.getFullYear(), startdateMonth = startdate.getMonth();

             var enddate = new Date(eDate);
             var enddateYear = enddate.getFullYear(), enddateMonth = enddate.getMonth();

             var payFrequency = $('#ddl_PayFrequency').val();
             var PeriodArray = {};

             console.log(startdateMonth + ' ddafd sdf vfd' + enddateMonth);

             if (payFrequency == 2) {
                 var count = 1;
                 for (let i = startdateMonth; i <= enddateMonth; i++) {
                     //var firstDay = new Date(startdateYear, i, 1);
                     //var lastDay = new Date(startdateYear, i + 1, 0);

                     if (i == startdateMonth) {
                         var firstDay = new Date(startdate);
                     } else {
                         var firstDay = new Date(startdateYear, i, 1);
                     }
                     if (i > (enddateMonth - 1)) {
                         var lastDay = new Date(enddate);
                     } else {
                         var lastDay = new Date(startdateYear, i + 1, 0);
                     }

                     var startPeriod = dateToString(firstDay);
                     var endPeriod = dateToString(lastDay);

                     //var trPeriod = '<tr> <td> ' + (i + 1) + '</td> <td>' + startPeriod + '</td> <td>' + endPeriod + '</td> <td>' + (firstDay.getMonth() + 1) + '</td> <td>-</td> <td>-</td> <td>-</td> </tr>';

                     var trPeriod = '<tr> <td> ' + count + '</td> <td>' + startPeriod + '</td> <td>' + endPeriod + '</td> <td>' + count + '</td>  </tr>';
                     count++;
                     $('#Period tbody').append(trPeriod);
                     console.log(i + ' --- ' + startPeriod + ' ---' + endPeriod);

                     PeriodArray[count] = startPeriod + '%%' + enddate;
                     //PeriodArray.push({ startdate: startPeriod, enddate: endPeriod });
                 }

                 $('#Period').removeClass('d-none');
             }
             else if (payFrequency == 3) {

                 var count = 1;
                 for (let i = startdateMonth; i <= enddateMonth; i += 3) {
                     console.log(' loop' + i);
                     //if (i == startdateMonth) {
                     //    var firstDay = new Date(startdateYear, i, 1);
                     //}

                     if (i == startdateMonth) {
                         var firstDay = new Date(startdate);
                     } else {
                         var firstDay = new Date(startdateYear, i, 1);
                     }
                     if (i >= (enddateMonth - 3)) {
                         var lastDay = new Date(enddate);
                     } else {
                         var lastDay = new Date(startdateYear, i + 3, 0);
                     }

                     var startPeriod = dateToString(firstDay);
                     var endPeriod = dateToString(lastDay);

                     //var trPeriod = '<tr> <td> ' + (i + 1) + '</td> <td>' + startPeriod + '</td> <td>' + endPeriod + '</td> <td>' + (firstDay.getMonth() + 1) + '</td> <td>-</td> <td>-</td> <td>-</td> </tr>';

                     var trPeriod = '<tr> <td> ' + count + '</td> <td>' + startPeriod + '</td> <td>' + endPeriod + '</td> <td>' + count + '</td>  </tr>';
                     count++;
                     $('#Period tbody').append(trPeriod);
                     // console.log(i + ' --- ' + startPeriod + ' ---' + endPeriod);

                     // PeriodArray.push({ startdate: startPeriod, enddate: endPeriod });
                 }

                 $('#Period').removeClass('d-none');
             }
             else if (payFrequency == 4) {

                 var count = 1;
                 for (let i = startdateMonth; i <= enddateMonth; i += 6) {
                     console.log(' loop' + i);
                     //if (i == startdateMonth) {
                     //    var firstDay = new Date(startdateYear, i, 1);
                     //}

                     if (i == startdateMonth) {
                         var firstDay = new Date(startdate);
                     } else {
                         var firstDay = new Date(startdateYear, i, 1);
                     }
                     if (i >= (enddateMonth - 6)) {
                         var lastDay = new Date(enddate);
                     } else {
                         var lastDay = new Date(startdateYear, i + 6, 0);
                     }

                     var startPeriod = dateToString(firstDay);
                     var endPeriod = dateToString(lastDay);

                     //var trPeriod = '<tr> <td> ' + (i + 1) + '</td> <td>' + startPeriod + '</td> <td>' + endPeriod + '</td> <td>' + (firstDay.getMonth() + 1) + '</td> <td>-</td> <td>-</td> <td>-</td> </tr>';

                     var trPeriod = '<tr> <td> ' + count + '</td> <td>' + startPeriod + '</td> <td>' + endPeriod + '</td> <td>' + count + '</td>  </tr>';
                     count++;
                     $('#Period tbody').append(trPeriod);
                     // console.log(i + ' --- ' + startPeriod + ' ---' + endPeriod);

                     //  PeriodArray.push({ startdate: startPeriod, enddate: endPeriod });
                 }

                 $('#Period').removeClass('d-none');
             }
             else {
                 alert('select Pay Frequency');
             }

             $('#PayFrequencySpan').html($('#ddl_PayFrequency :selected').text());
             console.log(PeriodArray);
             var stringMap = JSON.stringify(PeriodArray);

             $('#hdnPeriodValues').val(stringMap);
             console.log(stringMap);
         }
         function dateToString(val) {
             var d = new Date(val);

             var datestring = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();

             // console.log(datestring);
             return datestring;
         }
         //function checkFascalYear(val) {
         //   // alert('sss');
         //    var FascalYear = $('#dd_FascalYear').val();

         //    var d = new Date($(val).val());
         //    if (FascalYear != d.getFullYear()) {
         //        alert('Invalid');
         //        $(val).val('');
         //    }

         //}

         function setDateRange(val) {
             //  alert($(val).val());
             $('#dp_StartDate').val('');
             $('#dp_EndDate').val('');

             var year = $(val).val();
             changeYear(year);

         }
         function changeYear(year) {
             $('.pickadate-limits-start').pickadate({
                 min: new Date(year, 0, 1),
                 max: new Date(year, 11, 31),
                 format: '  mm/dd/yyyy'
             });
             $('.pickadate-limits-end').pickadate({
                 min: new Date(year, 0, 1),
                 max: new Date(year, 11, 31),
                 format: '  mm/dd/yyyy'
             });


             //// Use the picker object directly.
             var picker1 = $('.pickadate-limits-start').pickadate('picker');
             var picker2 = $('.pickadate-limits-end').pickadate('picker');

             picker1.set('min', [year, 0, 1]);
             picker1.set('max', [year, 11, 31]);

             picker2.set('min', [year, 0, 1]);
             picker2.set('max', [year, 11, 31]);



         }


         function loadPeriod() {
             var MapArray = {};

             $('.MapColums').each(function () {
                 if (this.value != '') {
                     MapArray[this.id] = this.value;
                 }

             });
             var stringMap = JSON.stringify(MapArray);
         }

    </script>
    <script>
        $(document).ready(function(){
            $('#BasicCompanyDetails input, #BasicCompanyDetails select, #BasicCompanyDetails textarea').each(
                function (index) {
                    $(this).attr('readonly', true);
                }
            );
            $(' #BasicCompanyDetails select').each(
                function (index) {
                    $(this).attr('disabled', true);
                }
            );

            $('#PeriodForm input, #PeriodForm select, #PeriodForm textarea').each(
                function (index) {
                    $(this).attr('readonly', true);
                }
            );
            $(' #PeriodForm select').each(
                function (index) {
                    $(this).attr('disabled', true);
                }
            );
        });
        function removeDisablePeriod(val) {
            $(val).addClass('d-none');
            $('#btn_PeriodSave').removeClass('d-none');
            $('#PeriodForm input, #PeriodForm select, #PeriodForm textarea').each(
                function (index) {
                    $(this).attr('readonly', false);
                }
            );
            $(' #PeriodForm select').each(
                function (index) {
                    $(this).attr('disabled', false);
                }
            );
        }
        function removeDisableBasic(val) {
            $(val).addClass('d-none');
            $('#btn_SaveDetails').removeClass('d-none');
            $('#BasicCompanyDetails input, #BasicCompanyDetails select, #BasicCompanyDetails textarea').each(
                function (index) {
                    $(this).attr('readonly', false);
                }
            );
            $(' #BasicCompanyDetails select').each(
                function (index) {
                    $(this).attr('disabled', false);
                }
            );
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">

    <!-- Page header -->
			<div class="page-header page-header-light border-bottom-0">

				

			
			

				<!-- Profile navigation -->
				<div class="navbar navbar-expand-lg navbar-light bg-light mb-3 p-0">
					<div class="text-center d-lg-none  w-100">
						<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-second">
							<i class="icon-menu7 mr-2"></i>
							Profile navigation
						</button>
					</div>

					<div class="navbar-collapse collapse m-1 m-md-0 p-md-0" id="navbar-second">
						<ul class="nav navbar-nav">
							<li class="nav-item">
								<a href="#activity" class="navbar-nav-link active" data-toggle="tab">
									<i class="icon-menu7 mr-2"></i>
									Account Setting
								</a>
							</li>
							<li class="nav-item">
								<a href="#schedule" class="navbar-nav-link" data-toggle="tab">
									<i class="icon-calendar3 mr-2"></i>
									Pay Schedule
									
								</a>
							</li>
							<li class="nav-item">
								<a href="#settings" class="navbar-nav-link" data-toggle="tab">
									<i class="icon-cog3 mr-2"></i>
									Company Profile
								</a>
							</li>
						</ul>

						<ul class="navbar-nav ml-lg-auto">
							
							<li class="nav-item">
								<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear"></i>
									<span class="d-lg-none ml-2">Settings</span>
								</a>

								<div class="dropdown-menu dropdown-menu-right">
									<%--<a href="#" class="dropdown-item"><i class="icon-image2"></i> Update cover</a>
									<a href="#" class="dropdown-item"><i class="icon-clippy"></i> Update info</a>
									<a href="#" class="dropdown-item"><i class="icon-make-group"></i> Manage sections</a>
									<div class="dropdown-divider"></div>
									<a href="#" class="dropdown-item"><i class="icon-three-bars"></i> Activity log</a>--%>
									<a href="AuditLogConfig.aspx" class="dropdown-item"><i class="icon-cog5"></i> Audit Log</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<!-- /profile navigation -->

			</div>
			<!-- /page header -->

    	<!-- Inner container -->
				<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="tab-content w-100 overflow-auto order-2 order-md-1">

						<div class="tab-pane fade active show" id="activity">

							<!-- Account settings -->
							<div class="card border-left-2 border-left-primary-400">
								<div class="card-header header-elements-inline">
									<h5 class="card-title">Account settings</h5>
									<div class="header-elements">
										<div class="list-icons">
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
					                		
					                	</div>
				                	</div>
								</div>

								<div class="card-body">
									
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Username</label>
													<input type="text" value="Kopyov" readonly="readonly" class="form-control">
												</div>

												<div class="col-md-6">
													<label>Current password</label>
													<input type="password" value="password" readonly="readonly" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>New password</label>
													<input type="password" placeholder="Enter new password" class="form-control">
												</div>

												<div class="col-md-6">
													<label>Repeat password</label>
													<input type="password" placeholder="Repeat new password" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Profile visibility</label>

													<div class="form-check">
														<label class="form-check-label">
															<input type="radio" name="visibility" class="form-input-styled" checked data-fouc>
															Visible to everyone
														</label>
													</div>

													<div class="form-check">
														<label class="form-check-label">
															<input type="radio" name="visibility" class="form-input-styled" data-fouc>
															Visible to friends only
														</label>
													</div>

													<div class="form-check">
														<label class="form-check-label">
															<input type="radio" name="visibility" class="form-input-styled" data-fouc>
															Visible to my connections only
														</label>
													</div>

													<div class="form-check">
														<label class="form-check-label">
															<input type="radio" name="visibility" class="form-input-styled" data-fouc>
															Visible to my colleagues only
														</label>
													</div>
												</div>

												<div class="col-md-6">
													<label>Notifications</label>

													<div class="form-check">
														<label class="form-check-label">
															<input type="checkbox" class="form-input-styled" checked data-fouc>
															Password expiration notification
														</label>
													</div>

													<div class="form-check">
														<label class="form-check-label">
															<input type="checkbox" class="form-input-styled" checked data-fouc>
															New message notification
														</label>
													</div>

													<div class="form-check">
														<label class="form-check-label">
															<input type="checkbox" class="form-input-styled" checked data-fouc>
															New task notification
														</label>
													</div>

													<div class="form-check">
														<label class="form-check-label">
															<input type="checkbox" class="form-input-styled">
															New contact request notification
														</label>
													</div>
												</div>
											</div>
										</div>

				                        <div class="text-right">
				                        	
				                        </div>
			                   
								</div>
							</div>
							<!-- /account settings -->

					    </div>

					    <div class="tab-pane fade" id="schedule">

				    		  <!-- Calender layout-->
                            
                                    <div class="card border-left-2 border-left-primary-400">
                                        <div class="card-header header-elements-inline">
                                            <h5 class="card-title">Add Period</h5>
                                            <div class="header-elements">
                                                <div class="list-icons">
                                                    <button type="button" class="btn btn-primary btn-sm" id="btn_EditPeriod" onclick="removeDisablePeriod(this)">Edit</button>
                                                     <button id="btn_PeriodSave" runat="server" onserverclick="btn_PeriodSave_ServerClick" class="btn btn-primary btn-sm d-none">Save</button>
                                               
                                                    
                                                </div>
                                            </div>
                                        </div>
                
                                        <div class="card-body">
                                            <div class="row" id="PeriodForm">
                                           
                                                <div class="form-group col-md-6">
                                                <label>Fascal Year</label>
                                                    <asp:DropDownList   runat="server" ID="ddl_Fascalyear" CssClass="form-control">
                                                    </asp:DropDownList>
                                           
                                            </div>
                                            
                                             <div class="form-group col-md-6">
                                                <label>Current Period<span class="text-danger">*</span></label>
                                                <input runat="server"   id="txt_CurrentPeriod" type="text" name="" class="form-control " > 
                                              
                                            </div>
                                            
                                            <div class="form-group col-md-6">
                                                <label>Start Date<span class="text-danger">*</span></label>
                                                <input runat="server"   id="dp_StartDate" type="text" name="" class="form-control pickadate-limits-start " > 
                                               
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>End Date<span class="text-danger">*</span></label>
                                               
                                                 <input runat="server"  id="dp_EndDate" type="text" name="" class="form-control pickadate-limits-end " > 
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pay Frequency<span class="text-danger">*</span></label>
                                                 <asp:DropDownList   runat="server" ID="ddl_PayFrequency" CssClass="form-control">
                                                    </asp:DropDownList>
                                              
                                            </div>
                                            <div class="form-group col-md-6 my-auto">
                                              <button type="button" onclick="changeModalSize('lg')" data-toggle="modal" data-target="#ModalPayPeriod"  class="btn btn-link"> View Period</button>
                                            </div>
    
                                            <div class="text-right col-md-12 ">
                                               
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                               
                                <!-- /Calnder layout -->

				    	</div>

					    <div class="tab-pane fade" id="settings">

							<!-- Profile info -->
							<div class="card border-left-2 border-left-primary-400">
								<div class="card-header header-elements-inline">
									<h5 class="card-title">Profile information</h5>
									<div class="header-elements">
										<div class="list-icons">
                                            <button type="button" class="btn btn-primary btn-sm" id="btn_Edit" onclick="removeDisableBasic(this)">Edit</button>
                                            <button type="button" class="btn btn-primary btn-sm d-none" id="btn_SaveDetails" runat="server" onserverclick="btn_SaveDetails_ServerClick">Save</button>
					                		<a class="list-icons-item" data-action="collapse"></a>
					                	</div>
				                	</div>
								</div>

								<div class="card-body">

                                    <!--Basic Company Settup-->
                                         <div id="BasicCompanyDetails" class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Company Name<span class="text-danger">*</span></label>
                            <input id="txt_CompanyName" runat="server" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Email<span class="text-danger">*</span></label>
                            <input id="txt_Email" runat="server" type="email" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Mobile<span class="text-danger">*</span></label>
                            <input id="txt_Mobile" runat="server"  type="text" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Country</label>
                                        <select id="dd_Country" runat="server" class="form-control">
                                        <option value="0">-- select country --</option>
                                            <option value="1">America</option>
                                            <option value="2">South Africa</option>
                                            <option value="3">India</option>
                                            <option >Pondichery</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>State</label>
                                        <select id="dd_State" runat="server" class="form-control">
                                            <option value="0">-- select state --</option>
                                            <option value="1">Tamilnadu</option>
                                            <option value="2">Kerala</option>
                                            <option value="3">Andhra</option>
                                            <option value="4">Srilanka</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>City</label>
                                        <select id="dd_City" runat="server" class="form-control">
                                            <option value="0">-- select city --</option>
                                            <option value="12">Chennai</option>
                                            <option value="13">Madurai</option>
                                            <option value="14">Cuddalore</option>
                                            <option value="15">Villupuram</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group ">
                                        <label>Currency</label>
                                        <select id="dd_Currency" runat="server" class="form-control">
                                            <option value="">-- select Currency --</option>
                                            <option>U.S. Dollar (USD)</option>
                                            <option>European Euro (EUR)</option>
                                            <option>Canadian Dollar (CAD)</option>
                                            <option>Japanese Yen (JPY)</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Time Zone</label>
                                        <select id="dd_TimeZone" runat="server" class="form-control">
                                            <option value="">-- select time Zone --</option>
                                            <option>Alpha Time Zome UTC+1</option>
                                            <option>ACT UTC-5</option>
                                            <option>ACST UTC+9.30</option>
                                                    
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                            <label>Address 1<span class="text-danger">*</span></label>
                                            <textarea id="txt_Address1" runat="server" rows="2" cols="3" class="form-control" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                            <label>Address 2 <small>(optional)</small></label>
                                            <textarea id="txt_Address2" runat="server"  rows="2" cols="3" class="form-control" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Website</label>
                                        <input id="txt_Website" runat="server" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class=" col-md-12">
                                
                        <div><h5>Apear in Reports</h5></div>
                        <hr>
                    </div>
                      <div class="col-md-6">
                        <div class="form-group ">
                            <label>Company Name<span class="text-danger">*</span></label>
                            <input id="txt_AprCompanyName" runat="server" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Email<span class="text-danger">*</span></label>
                            <input id="txt_AprEmail" runat="server" type="email" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label>Mobile<span class="text-danger">*</span></label>
                            <input id="txt_AprMobile" runat="server"  type="text" class="form-control"/>
                        </div>
                    </div>
                                             <div class="col-md-6">
                                                    <label>Language</label>
                                                 <select class="goog-te-combo form-control" aria-label="Language Translate Widget"><option value="en">English</option><option value="af">Afrikaans</option><option value="sq">Albanian</option><option value="am">Amharic</option><option value="ar">Arabic</option><option value="hy">Armenian</option><option value="az">Azerbaijani</option><option value="eu">Basque</option><option value="be">Belarusian</option><option value="bn">Bengali</option><option value="bs">Bosnian</option><option value="bg">Bulgarian</option><option value="ca">Catalan</option><option value="ceb">Cebuano</option><option value="ny">Chichewa</option><option value="zh-CN">Chinese (Simplified)</option><option value="zh-TW">Chinese (Traditional)</option><option value="co">Corsican</option><option value="hr">Croatian</option><option value="cs">Czech</option><option value="da">Danish</option><option value="nl">Dutch</option><option value="eo">Esperanto</option><option value="et">Estonian</option><option value="tl">Filipino</option><option value="fi">Finnish</option><option value="fr">French</option><option value="fy">Frisian</option><option value="gl">Galician</option><option value="ka">Georgian</option><option value="de">German</option><option value="el">Greek</option><option value="gu">Gujarati</option><option value="ht">Haitian Creole</option><option value="ha">Hausa</option><option value="haw">Hawaiian</option><option value="iw">Hebrew</option><option value="hi">Hindi</option><option value="hmn">Hmong</option><option value="hu">Hungarian</option><option value="is">Icelandic</option><option value="ig">Igbo</option><option value="id">Indonesian</option><option value="ga">Irish</option><option value="it">Italian</option><option value="ja">Japanese</option><option value="jw">Javanese</option><option value="kn">Kannada</option><option value="kk">Kazakh</option><option value="km">Khmer</option><option value="ko">Korean</option><option value="ku">Kurdish (Kurmanji)</option><option value="ky">Kyrgyz</option><option value="lo">Lao</option><option value="la">Latin</option><option value="lv">Latvian</option><option value="lt">Lithuanian</option><option value="lb">Luxembourgish</option><option value="mk">Macedonian</option><option value="mg">Malagasy</option><option value="ms">Malay</option><option value="ml">Malayalam</option><option value="mt">Maltese</option><option value="mi">Maori</option><option value="mr">Marathi</option><option value="mn">Mongolian</option><option value="my">Myanmar (Burmese)</option><option value="ne">Nepali</option><option value="no">Norwegian</option><option value="ps">Pashto</option><option value="fa">Persian</option><option value="pl">Polish</option><option value="pt">Portuguese</option><option value="pa">Punjabi</option><option value="ro">Romanian</option><option value="ru">Russian</option><option value="sm">Samoan</option><option value="gd">Scots Gaelic</option><option value="sr">Serbian</option><option value="st">Sesotho</option><option value="sn">Shona</option><option value="sd">Sindhi</option><option value="si">Sinhala</option><option value="sk">Slovak</option><option value="sl">Slovenian</option><option value="so">Somali</option><option value="es">Spanish</option><option value="su">Sundanese</option><option value="sw">Swahili</option><option value="sv">Swedish</option><option value="tg">Tajik</option><option value="ta">Tamil</option><option value="te">Telugu</option><option value="th">Thai</option><option value="tr">Turkish</option><option value="uk">Ukrainian</option><option value="ur">Urdu</option><option value="uz">Uzbek</option><option value="vi">Vietnamese</option><option value="cy">Welsh</option><option value="xh">Xhosa</option><option value="yi">Yiddish</option><option value="yo">Yoruba</option><option value="zu">Zulu</option></select>
                                             </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Country</label>
                                        <select id="dd_AprCountry" runat="server" class="form-control">
                                         <option value="1">America</option>
                                            <option value="2">South Africa</option>
                                            <option value="3">India</option>
                                            <option >Pondichery</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>State</label>
                                        <select id="dd_AprState" runat="server" class="form-control">
                                             <option value="0">-- select state --</option>
                                            <option value="1">Tamilnadu</option>
                                            <option value="2">Kerala</option>
                                            <option value="3">Andhra</option>
                                            <option value="4">Srilanka</option>
                                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>City</label>
                                        <select id="dd_AprCity" runat="server" class="form-control">
                                            <option value="0">-- select city --</option>
                                            <option value="12">Chennai</option>
                                            <option value="13">Madurai</option>
                                            <option value="14">Cuddalore</option>
                                            <option value="15">Villupuram</option>
                                        </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group ">
                                            <label>Address 1<span class="text-danger">*</span></label>
                                            <textarea id="txt_AprAddress1" runat="server" rows="2" cols="3" class="form-control" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                            <label>Address 2 <small>(optional)</small></label>
                                            <textarea id="txt_AprAddress2" runat="server"  rows="2" cols="3" class="form-control" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                                        <label>Website</label>
                                        <input id="txt_AprWebsite" runat="server" type="text" class="form-control" >
                        </div>
                    </div>
                </div>
                                    <!--Basic Company Settup-->


									<%--<form action="#">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Username</label>
													<input type="text" value="Eugene" class="form-control">
												</div>
												<div class="col-md-6">
													<label>Full name</label>
													<input type="text" value="Kopyov" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Address line 1</label>
													<input type="text" value="Ring street 12" class="form-control">
												</div>
												<div class="col-md-6">
													<label>Address line 2</label>
													<input type="text" value="building D, flat #67" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-4">
													<label>City</label>
													<input type="text" value="Munich" class="form-control">
												</div>
												<div class="col-md-4">
													<label>State/Province</label>
													<input type="text" value="Bayern" class="form-control">
												</div>
												<div class="col-md-4">
													<label>ZIP code</label>
													<input type="text" value="1031" class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Email</label>
													<input type="text" readonly="readonly" value="eugene@kopyov.com" class="form-control">
												</div>
												<div class="col-md-6">
						                            <label>Your country</label>
						                            <select class="form-control form-control-select2" data-fouc>
						                                <option value="germany" selected>Germany</option> 
						                                <option value="france">France</option> 
						                                <option value="spain">Spain</option> 
						                                <option value="netherlands">Netherlands</option> 
						                                <option value="other">...</option> 
						                                <option value="uk">United Kingdom</option> 
						                            </select>
												</div>
											</div>
										</div>

				                        <div class="form-group">
				                        	<div class="row">
				                        		<div class="col-md-6">
													<label>Phone #</label>
													<input type="text" value="+99-99-9999-9999" class="form-control">
													<span class="form-text text-muted">+99-99-9999-9999</span>
				                        		</div>

												<div class="col-md-6">
													<label>Upload profile image</label>
				                                    <input type="file" class="form-input-styled" data-fouc>
				                                    <span class="form-text text-muted">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
												</div>
				                        	</div>
				                        </div>

				                        <div class="text-right">
				                        	<button type="submit" class="btn btn-primary">Save changes</button>
				                        </div>
									</form>--%>
								</div>
							</div>
							<!-- /profile info -->
                           

						

				    	</div>
					</div>
					<!-- /left content -->


					<!-- Right sidebar component -->
					<div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-300 border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

						<!-- Sidebar content -->
						<div class="sidebar-content">

							<!-- User card -->
							<div class="card border-left-2 border-left-primary-400">
                                <div class="card-header  header-elements-inline">
                                   <h5 class="card-title "> Logo</h5>    
                                </div>
								<div class="card-body text-center">
									<div class="card-img-actions d-inline-block mb-3">
                                         <img runat="server" src="../Limitless_Bootstrap_4/Template/global_assets/images/placeholders/placeholder.jpg"  id="img_CompanyLogo" class="img-fluid p-3"   width="500" height="170"/>
										<%--<img class="img-fluid rounded-circle" src="../Limitless_Bootstrap_4/Template/global_assets/images/placeholders/placeholder.jpg" width="170" height="170" alt="">--%>
										<div class="card-img-actions-overlay card-img ">
											<a id="a_CompanyLogo"  runat="server" href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
												<i class="icon-plus3"></i>
											</a>
											<%--<a href="user_pages_profile.html" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
												<i class="icon-link"></i>
											</a>--%>
										</div>
									</div>

						    	
						    	</div>
					    	</div>
					    	<!-- /user card -->


							<!-- Navigation -->
							<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="card-title font-weight-semibold">Navigation</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body p-0">
									<ul class="nav nav-sidebar my-2">
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-user"></i>
												 My profile
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-cash3"></i>
												Balance
												<span class="text-muted font-size-sm font-weight-normal ml-auto">$1,430</span>
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-tree7"></i>
												Connections
												<span class="badge bg-danger badge-pill ml-auto">29</span>
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-users"></i>
												Friends
											</a>
										</li>

										<li class="nav-item-divider"></li>

										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-calendar3"></i>
												Events
												<span class="badge bg-teal-400 badge-pill ml-auto">48</span>
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-cog3"></i>
												Account settings
											</a>
										</li>
									</ul>
								</div>
							</div>
							<!-- /navigation -->


							<!-- Share your thoughts -->
							<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="card-title font-weight-semibold">Feedback</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body">
									
				                    	<textarea name="enter-message" class="form-control mb-3" rows="3" cols="1" placeholder="Enter your message..."></textarea>

				                    	<div class="d-flex align-items-center">
				                    		
				                    		<button type="button" class="btn bg-blue btn-labeled btn-labeled-right ml-auto"><b><i class="icon-paperplane"></i></b> Share</button>
				                    	</div>
									
								</div>
							</div>
							<!-- /share your thoughts -->


							


						</div>
						<!-- /sidebar content -->

					</div>
					<!-- /right sidebar component -->

				</div>
				<!-- /inner container -->



     <!-- split modal -->
                <div id="ModalPayPeriod" class="modal fade" tabindex="-1">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;Period</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                 <p class="text-left"><span runat="server" id="PayFrequencySpan"></span> Frequency</p>
                                   <div class="table-responsive">
                            <table class="table table-bordered " id="Period">
                                <thead>
                                    <tr>
                                        <th>Period Number</th>
                                        <th>Period Start Date</th>
                                        <th>Period End Date</th>
                                       
                                       
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_PeriodListTr" runat="server"></asp:PlaceHolder>
                                       <%-- <tr>
                                            <td> 1</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>43</td>
                                            <td>86</td>
                                            <td>83</td>
                                            <td>36</td>
                                        </tr>
                                        <tr>
                                            <td> 2</td>
                                            <td>03-06-2019</td>
                                            <td>08-06-2019</td>
                                            <td>43</td>
                                            <td>76</td>
                                            <td>93</td>
                                            <td>34</td>
                                        </tr>
                                        <tr>
                                            <td> 3</td>
                                            <td>11-06-2019</td>
                                            <td>16-06-2019</td>
                                            <td>48</td>
                                            <td>76</td>
                                            <td>89</td>
                                            <td>46</td>
                                        </tr>
                                        <tr>
                                            <td> 4</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>43</td>
                                            <td>86</td>
                                            <td>83</td>
                                            <td>36</td>
                                        </tr>
                                        <tr>
                                            <td> 5</td>
                                            <td>01-06-2019</td>
                                            <td>09-06-2019</td>
                                            <td>83</td>
                                            <td>66</td>
                                            <td>33</td>
                                            <td>76</td>
                                        </tr>--%>
                                       
                                </tbody>
                            </table>


                                   </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /split modal -->


    <asp:HiddenField ID="hdn_CalendarKey" runat="server" />
</asp:Content>

