﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_FunctionGoal : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        if (!IsPostBack)
        {
            loadGoalScopeDDL();
            loadGoalBy();
            hdn_GoalKey.Value = Guid.NewGuid().ToString();
            if (Com.GetQueryStringValue("isEdit") == "1")
            {
                hdn_GoalKey.Value = Com.GetQueryStringValue("FunctionKey");
                //lbl_UpdateMsg.Visible = true;

                loadGoal(Com.GetQueryStringValue("FunctionKey"));
            }
        }
        
    }

    private void loadGoal(string GoalKey)
    {
        string str_sql = "select * from Goal where GoalKey=@GoalKey";
        SqlCommand sqlcmd = new SqlCommand(str_sql);
        sqlcmd.Parameters.AddWithValue("@GoalKey", GoalKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);


        foreach (DataRow dr in dt.Rows)
        {
            txt_GoalName.Value = dr["GoalName"].ToString();
            ddl_GoalScope.SelectedValue = dr["GoalScope"].ToString();
            ddl_GoalBy.SelectedValue = dr["GoalBy"].ToString();
           ddl_GoalType.SelectedValue = dr["GoalTypeId"].ToString();
           

            //Goal (GoalKey, GoalName, GoalScope, GoalBy, GoalTypeId, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        }
        GoalUI(GoalKey);
    }

    private void loadGoalBy()
    {
        string str_Select = "select ValueKey as Value, Text from v_CommissionGoalBy ";
        SqlCommand cmm = new SqlCommand(str_Select);

        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_GoalBy, ds.Tables[0], "Text", "Value", true, "--Select Goal By--");
    }

    private void loadGoalScopeDDL()
    {
        string str_Select = "select ValueKey as Value, Name as Text from v_CommisssionVariableScope order by seq ";
        SqlCommand cmm = new SqlCommand(str_Select);

        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_GoalScope, ds.Tables[0], "Text", "Value", true, "--Select Goal Scope--");
    }

    protected void btn_GenerateGoal_ServerClick(object sender, EventArgs e)
    {
        //Goal (GoalKey, GoalName, GoalScope, GoalBy, GoalTypeId, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "insert into Goal  (GoalKey, GoalName, GoalScope, GoalBy, GoalTypeId, CreatedOn, CreatedBy)"
                              + "select  @GoalKey, @GoalName, @GoalScope, @GoalBy, @GoalTypeId, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@GoalKey", hdn_GoalKey.Value);
        cmd.Parameters.AddWithValue("@GoalName", txt_GoalName.Value);
        cmd.Parameters.AddWithValue("@GoalScope",ddl_GoalScope.SelectedValue);
        cmd.Parameters.AddWithValue("@GoalBy", ddl_GoalBy.SelectedValue);
        cmd.Parameters.AddWithValue("@GoalTypeId", ddl_GoalType.SelectedValue);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);

        GenearateGoal(hdn_GoalKey.Value);
    }

    private void GenearateGoal(string GoalKey)
    {
        string str_Sql = "p_GenerateGoal";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@GoalKey", GoalKey);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@CommissionKey", Com.GetQueryStringValue("CommissionKey"));
        cmd.CommandType = CommandType.StoredProcedure;
        DA.ExecuteNonQuery(cmd);

        GoalUI(GoalKey);
    }

    private void GoalUI(string goalKey)
    {
        String TrColumnField = ColumnFieldTr(goalKey);

        String TrRowFieldWithValues = RowFieldWithValuesTr(goalKey);

        String FullTBody = TrColumnField + TrRowFieldWithValues;

        ph_GoalUI.Controls.Add(new LiteralControl(FullTBody));
        btn_GenerateGoal.Visible = false;
        btn_UpdateGoal.Visible = true;
        btn_SaveGoal.Visible = true;
    }

    private string RowFieldWithValuesTr(string GoalKey)
    {
        string str_Sql = "p_GenerateGoalUI";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@GoalKey", GoalKey);
        cmd.Parameters.AddWithValue("@CommissionKey", Com.GetQueryStringValue("CommissionKey"));
        cmd.CommandType = CommandType.StoredProcedure;
        DataSet ds = DA.GetDataSet(cmd);

        string Tr = "";

        string RowFiledTemplate = PHT.ReadFileToString("TdGoalRowField.txt");
       
        string RowGoalValuesTemplate = PHT.ReadFileToString("TdGoalValue.txt");


        foreach (DataRow dr1 in ds.Tables[0].Rows)
        {
            string SingleTr = "<tr>";
            string RowId = dr1["Seq"].ToString();
           
            
                SingleTr += PHT.ReplaceVariableWithValue(dr1, RowFiledTemplate);
            

            foreach (DataRow dr2 in ds.Tables[1].Rows)
            {
                string ColumnId = dr2["Seq"].ToString();

                foreach (DataRow dr3 in ds.Tables[2].Select("RowId='" + RowId + "' AND ColumnId='" + ColumnId + "' "))
                {
                    SingleTr += PHT.ReplaceVariableWithValue(dr3, RowGoalValuesTemplate);
                }

            }

            SingleTr += "</td>";
            Tr += SingleTr;
        }
        return Tr;
    }

    private string ColumnFieldTr(string GoalKey)
    {
        //for column range UI 
        string sql_qry = "select Seq from GoalRowColumn where GoalKey=@GoalKey and Type=2 order by Seq ";
        SqlCommand cmd = new SqlCommand(sql_qry);
        cmd.Parameters.AddWithValue("@GoalKey", GoalKey);
        DataTable dt = DA.GetDataTable(cmd);

        string ColumnTr = "<tr><td ></td>";

        string ColumnScopeTemplate = PHT.ReadFileToString("TdColumnScope.txt");
        foreach (DataRow dr in dt.Rows)
        {
            
            ColumnTr += PHT.ReplaceVariableWithValue(dr, ColumnScopeTemplate);
            
        }

        ColumnTr += "</tr>";

        return ColumnTr;
    }

    [WebMethod]
    public static string UpdateGoalValue(string ValueKey, string Value)
    {
        try
        {
            //GoalValue(ValueKey, GoalKey, RowColumnKey, RowId, ColumnId, Value, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();


            string str_Sql = "Update GoalValue set  Value=@Value, ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where ValueKey=@ValueKey ";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@Value", Value);
            cmd.Parameters.AddWithValue("@ValueKey", ValueKey);
            cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return "Range Value Updated";

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    protected void btn_UpdateGoal_ServerClick(object sender, EventArgs e)
    {
        //Goal (GoalKey, GoalName, GoalScope, GoalBy, GoalTypeId, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "Update  Goal set   GoalName=@GoalName, GoalScope=@GoalScope, GoalBy=@GoalBy, GoalTypeId=@GoalTypeId, ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where GoalKey=@GoalKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@GoalKey", hdn_GoalKey.Value);
        cmd.Parameters.AddWithValue("@GoalName", txt_GoalName.Value);
        cmd.Parameters.AddWithValue("@GoalScope", ddl_GoalScope.SelectedValue);
        cmd.Parameters.AddWithValue("@GoalBy", ddl_GoalBy.SelectedValue);
        cmd.Parameters.AddWithValue("@GoalTypeId", ddl_GoalType.SelectedValue);
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);

        GenearateGoal(hdn_GoalKey.Value);
    }
}