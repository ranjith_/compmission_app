﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="Web_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Change Password</title>
    <script type="text/javascript">
        function validatePassword() {
            var password = $('#txt_NewPassword').val();
            var confirmpassword = $('#txt_ConfirmNewPassword').val();
            if (password != confirmpassword) {
                alert("New password and confirm password must be same");
                return false;
            }
            return true;
        }

        function validateOldPassword() {
            var oldpassword = $('#txt_OldPassword').val();
            var existingpassword = $('#hf_oldPassword').val();
            if (oldpassword != existingpassword) {
                alert("Incorrect old password");
                return false;
            }

            return validatePassword();
            
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mx-auto">
                   <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Change Password
                                    
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                <div class="form-group col-md-12 mx-auto">
                                    <label>Old Password</label>
                                    <input id="pwd_OldPassword" runat="server" class="form-control" type="password" />
                                </div> 
                                <div class="form-group col-md-12 mx-auto">
                                    <label>New Password</label>
                                    <input id="pwd_NewPassword" runat="server" class="form-control" type="password" />
                                </div> 
                                <div class="form-group col-md-12 mx-auto">
                                    <label>Confirm New Password</label>
                                    <input id="pwd_ConfirmPassword" runat="server" class="form-control" type="password" />
                                </div> 
                                <div class="text-center">
                                    <asp:Label ID="lbl_err" CssClass="text-danger" runat="server"></asp:Label>
                                </div>
                                <div class="form-group col-md-12 text-right">
                                    <asp:Button ID="btn_save" runat="server" OnClientClick="return validateOldPassword();" OnClick="btn_save_Click" Text="Change Password" class="btn btn-primary btn-sm" />
                                </div> 
                            </div>
                        </div>
            </div>
        </div>
    </div>
    
                  
</asp:Content>

