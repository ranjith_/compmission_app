﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public partial class Web_Calender : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.Com = new Common();

        if (!IsPostBack)
        {
            string str_Select = "select Name as Text, ConfigDetailKey as Value from v_CalendarFrequency order by Seq";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.Com.LoadDropDown(ddl_Frequency, ds.Tables[0], "Text", "Value", true, "-- Select type --");

            loadExistingPeriods();

        }
      //  loadExistingPeriod();
    }

    private void loadExistingPeriod()
    {
        string str_Sql_view = "select c.*,p.Name as Frequency, convert(varchar, c.CreatedOn, 101) as Created_On, convert(varchar, c.StartDate, 101) as SDate,convert(varchar, c.EndDate, 101) as EDate from Calendar c join v_PayFrequency p on c.PayFrequency=p.Value where c.CreatedBy=@CreatedBy";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);

        if (dt.Rows.Count > 0)
        {
           
                // div_PeriodDiv.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Attributes["class"] = "col-lg-6 col-md-6 d-none";
            DataSet ds2 = this.DA.GetDataSet(sqlcmd);
         //   this.PHT.LoadGridItem(ds2, ph_CalendarView, "DivCalendar.txt", "");

           
            string CalendarKey = "";
            foreach (DataRow dr in dt.Rows)
            {

                CalendarKey = dr["CalendarKey"].ToString();

             //   PayFrequencySpan.InnerHtml= dr["Frequency"].ToString();
            }

           // loadExistingPeriods(CalendarKey);
        }
        else
        {
            div_InputForm.Attributes["class"] = "col-lg-6 col-md-6 ";
          //  div_PeriodDiv.Attributes["class"] = "col-lg-6 col-md-6 d-none";
        //    div_Calendar.Attributes["class"] = "col-lg-6 col-md-6 d-none";
        }
        
    }

    private void loadExistingPeriods()
    {
        string str_Sql = "select format(a.StartDate,'MM/d/yyyy') as sDate, format(a.EndDate,'MM/d/yyyy') as eDate,a.FrequencyName, a.CalendarKey,a.CreatedOn,b.Seq,b.Name as FrequencyType from CalendarSetup a join v_CalendarFrequency b on a.Frequency=b.ConfigDetailKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
       
        DataSet ds = this.DA.GetDataSet(cmd);

        this.PHT.LoadGridItem(ds, ph_PeriodListTr, "PeriodListTr.txt", "");
    }

    ////(CalenderKey , FascalYear , StartDate , EndDate , PayFrequency , Description , CreatedOn , CreatedBy , ModifiedOn , ModifiedBy)


    protected void btn_Name_ServerClick(object sender, EventArgs e)
    {

    }

    protected void btn_Save_ServerClick(object sender, EventArgs e)
    {
        string CalendarKey = Guid.NewGuid().ToString();
        //// CalendarSetup (CalendarKey, StartDate, EndDate, Frequency, FrequencyName, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        ///
        string str_Sql = "insert into CalendarSetup(CalendarKey, StartDate, EndDate, Frequency, FrequencyName, CreatedOn, CreatedBy)"
                            + "select @CalendarKey, @StartDate, @EndDate, @Frequency, @FrequencyName, @CreatedOn, @CreatedBy;";
                           
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CalendarKey", CalendarKey);
        cmd.Parameters.AddWithValue("@FrequencyName", txt_FreqName.Value);
        cmd.Parameters.AddWithValue("@Frequency", ddl_Frequency.SelectedValue);
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        this.DA.ExecuteNonQuery(cmd);


        string str_Select = "select Value from v_CalendarFrequency where ConfigDetailKey=@ConfigDetailKey ";
       
        SqlCommand cmm = new SqlCommand(str_Select);
        cmm.Parameters.AddWithValue("@ConfigDetailKey", ddl_Frequency.SelectedValue);
        DataSet ds = this.DA.GetDataSet(cmm);
        int Frequency =int.Parse(ds.Tables[0].Rows[0]["Value"].ToString());

        loadPeriods(CalendarKey, Frequency, txt_FreqName.Value, txt_TypeCount.Value);

        
    }

    private void loadPeriods(string CalendarKey, int Frequency, string Value, string AddTypeCount)
    {
        DateTime startdate = Convert.ToDateTime(dp_StartDate.Value);
        DateTime enddate = Convert.ToDateTime(dp_EndDate.Value);

        int Counter = int.Parse(AddTypeCount);

        int startdateMonth = startdate.Month;
        int enddateMonth = enddate.Month;
        int year = 0;


        switch (Frequency)
        {
            case 1:
                int x = 0;

                int seqday = 1;

                var Totaldays = (enddate - startdate).TotalDays;

                DateTime StartDate = startdate;
                DateTime EndDate = enddate;

                DateTime WeekFirstDay = startdate;
                DateTime WeekLastDay = new DateTime();

                for (x = 0; x <= Totaldays; x += Counter)
                {
                    if (x == 0)
                    {
                        WeekFirstDay = startdate;
                    }
                    else
                    {
                        WeekFirstDay = WeekLastDay.AddDays(1);
                    }
                    if (x + Counter - 1 > Totaldays)
                    {
                        WeekLastDay = enddate;

                    }
                    else
                    {
                        WeekLastDay = WeekFirstDay.AddDays(Counter-1);
                    }
                    int ThisYear = WeekLastDay.Year;
                    insertPeriod(CalendarKey, seqday, ThisYear, WeekFirstDay, WeekLastDay, Value);

                    seqday += 1;
                }
                break;

            case 2:
               x = 0;

                int seqWeekly = 1;

                Totaldays = (enddate - startdate).TotalDays;

                 StartDate = startdate;
                EndDate = enddate;

                 WeekFirstDay = startdate;
                 WeekLastDay = new DateTime();

                for (x = 0; x <= Totaldays; x += Counter*7)
                {
                    if (x == 0)
                    {
                        WeekFirstDay = startdate;
                    }
                    else
                    {
                        WeekFirstDay = WeekLastDay.AddDays(1);
                    }
                    if (x + Counter*7 - 1 > Totaldays)
                    {
                        WeekLastDay = enddate;

                    }
                    else
                    {
                        WeekLastDay = WeekFirstDay.AddDays((Counter *7 )- 1);
                    }
                    int ThisYear = WeekLastDay.Year;
                    insertPeriod(CalendarKey, seqWeekly, ThisYear, WeekFirstDay, WeekLastDay, Value);

                    seqWeekly += 1;
                }
                break;
            case 4:
                int seqMonth = 1;
                int Year = startdate.Year;


                var TotalMonths = GetMonthDifference(startdate, enddate);

                // for (int i=startdateMonth; i <= enddateMonth; i++)

                DateTime MonthDay = startdate;

                for (int i = 0; i <= TotalMonths; i+=Counter)
                {
                    DateTime FirstDay = startdate;
                    DateTime LastDay = startdate;



                    var startOfMonth = new DateTime(MonthDay.Year, MonthDay.Month, 1);
                    var endOfMonth = startOfMonth.AddMonths(Counter).AddDays(-1);

                    if (i == 0)
                    {
                        FirstDay = startdate;
                    }
                    else
                    {
                        FirstDay = startOfMonth;
                    }

                    if (i == TotalMonths)
                    {
                        LastDay = enddate;
                    }
                    else
                    {
                        // LastDay = new DateTime(FirstDay.Year, FirstDay.Month, DateTime.DaysInMonth(FirstDay.Year, FirstDay.Month));
                        LastDay = endOfMonth;
                    }
                    int ThisYear = LastDay.Year;
                    Year = ThisYear;
                    insertPeriod(CalendarKey, seqMonth, ThisYear, FirstDay, LastDay, Value);
                    seqMonth += 1;

                    MonthDay = LastDay.AddMonths(1);
                }
                break;
            case 3:
                 x = 0;

                int seqBiMonth = 1;

                Totaldays = (enddate - startdate).TotalDays;

                StartDate = startdate;
                EndDate = enddate;

                WeekFirstDay = startdate;
                WeekLastDay = new DateTime();

                DateTime FirstCycle = new DateTime();
                DateTime SecondCycle = new DateTime();

                int CycleType = 1;

                for (x = 0; x <= Totaldays; x +=14)
                {
                    
                   
                    if (CycleType == 1)
                    {
                            if (x == 0)
                            {
                                WeekFirstDay = startdate;
                            }
                            else
                            {
                                WeekFirstDay = WeekLastDay.AddDays(1);
                            }
                            if (x + 14 - 1 > Totaldays)
                            {
                                WeekLastDay = enddate;

                            }
                            else
                            {
                                WeekLastDay = WeekFirstDay.AddDays(14);
                            }
                        FirstCycle = WeekFirstDay;
                    CycleType = 2;
                    }
                    else if (CycleType == 2)
                    {
                        if (x == 0)
                        {
                            WeekFirstDay = startdate;
                        }
                        else
                        {
                            WeekFirstDay = WeekLastDay.AddDays(1); 
                        }
                        if (x + 14 - 1 > Totaldays)
                        {
                            WeekLastDay = enddate;

                        }
                        else
                        {
                            WeekLastDay = FirstCycle.AddMonths(1).AddDays(-1);
                        }

                        CycleType = 1;
                    }
                    int ThisYear = WeekLastDay.Year;
                    insertPeriod(CalendarKey, seqBiMonth, ThisYear, WeekFirstDay, WeekLastDay, Value);

                    seqBiMonth += 1;
                }
                break;
        }




        //switch (Frequency)
        //{
        //    case 1:
        //        //daily
        //        break;
        //    case 2:
        //        //weekly
        //        int x=0;

        //        int seqWeekly = 1;

        //        var Totaldays = (enddate - startdate).TotalDays;

        //        DateTime StartDate = startdate;
        //        DateTime EndDate = enddate;

        //        DateTime WeekFirstDay = startdate;
        //        DateTime WeekLastDay = new DateTime();

        //        for (x=0; x<=Totaldays; x += 7)
        //        {
        //            if (x == 0)
        //            {
        //                WeekFirstDay = startdate;
        //            }
        //            else
        //            {
        //                WeekFirstDay = WeekLastDay.AddDays(1);
        //            }
        //            if (x+6 > Totaldays)
        //            {
        //                WeekLastDay = enddate;

        //            }
        //            else
        //            {
        //                WeekLastDay = WeekFirstDay.AddDays(6);
        //            }
        //            int ThisYear = WeekLastDay.Year;
        //            insertPeriod(CalendarKey, seqWeekly, ThisYear,WeekFirstDay, WeekLastDay, Value);

        //            seqWeekly += 1;
        //        }


        //        //while ( x < Totaldays){
        //        //    DateTime WeekFirstDay = startdate;
        //        //    DateTime WeekLastDay = startdate;
        //        //    if (x == 1)
        //        //    {
        //        //        WeekFirstDay = startdate;
        //        //    }
        //        //    else
        //        //    {
        //        //        WeekFirstDay = WeekFirstDay.AddDays(x);
        //        //    }
        //        //    if (x > Totaldays)
        //        //    {
        //        //        WeekLastDay = enddate;

        //        //    }
        //        //    else
        //        //    {
        //        //        WeekLastDay = WeekFirstDay.AddDays(6);
        //        //    }
        //        //    insertPeriod(WeekFirstDay, WeekLastDay, str_Key);
        //        //    x += 6;
        //        //}
        //        break;
        //    case 3:
        //        //monthly
        //        int seqMonth = 1;
        //        int Year = startdate.Year;


        //        var TotalMonths = GetMonthDifference(startdate,enddate);

        //        // for (int i=startdateMonth; i <= enddateMonth; i++)

        //        DateTime MonthDay = startdate;

        //        for (int i=0; i <= TotalMonths; i++)
        //        {
        //            DateTime FirstDay = startdate;
        //            DateTime LastDay = startdate;



        //            var startOfMonth = new DateTime(MonthDay.Year, MonthDay.Month, 1);
        //            var endOfMonth = startOfMonth.AddMonths(1).AddDays(-1);

        //            if (i == 0)
        //            {
        //                 FirstDay = startdate;
        //            }
        //            else
        //            {
        //                 FirstDay = startOfMonth;
        //            }

        //            if (i == TotalMonths)
        //            {
        //                 LastDay = enddate;
        //            }
        //            else
        //            {
        //                // LastDay = new DateTime(FirstDay.Year, FirstDay.Month, DateTime.DaysInMonth(FirstDay.Year, FirstDay.Month));
        //                LastDay = endOfMonth;
        //            }
        //            int ThisYear = LastDay.Year;
        //            Year = ThisYear;
        //            insertPeriod(CalendarKey, seqMonth, ThisYear, FirstDay, LastDay, Value);
        //            seqMonth += 1;

        //            MonthDay= LastDay.AddMonths(1);
        //        }
        //        break;
        //    case 4:
        //        //Quarterly
        //        int seqQt = 1;
        //        int YearQt = startdate.Year;


        //        int TotalMonthsQt =( GetMonthDifference(startdate, enddate))/3;

        //        // for (int i=startdateMonth; i <= enddateMonth; i++)

        //        DateTime MonthDayQt = startdate;

        //        for (int i = 0; i <= TotalMonthsQt; i++)
        //        {
        //            DateTime FirstDay = startdate;
        //            DateTime LastDay = startdate;



        //            var startOfMonth = new DateTime(MonthDayQt.Year, MonthDayQt.Month, 1);
        //            var endOfMonth = startOfMonth.AddMonths(3).AddDays(-1);

        //            if (i == 0)
        //            {
        //                FirstDay = startdate;
        //            }
        //            else
        //            {
        //                FirstDay = startOfMonth;
        //            }

        //            if (i == TotalMonthsQt)
        //            {
        //                LastDay = enddate;
        //            }
        //            else
        //            {
        //                // LastDay = new DateTime(FirstDay.Year, FirstDay.Month, DateTime.DaysInMonth(FirstDay.Year, FirstDay.Month));
        //                LastDay = endOfMonth;
        //            }
        //            int ThisYear = LastDay.Year;
        //            YearQt = ThisYear;
        //            insertPeriod(CalendarKey, seqQt, ThisYear, FirstDay, LastDay, Value);
        //            seqQt += 1;

        //            MonthDayQt = LastDay.AddMonths(1);
        //        }
        //        break;
        //    case 5:
        //        //Half Yearly
        //        int seqHf = 1;

        //        int YearHF = startdate.Year;


        //        int TotalMonthsHf = (GetMonthDifference(startdate, enddate)) / 6;

        //        DateTime MonthDayHf = startdate;

        //        for (int i = 0; i <= TotalMonthsHf; i++)
        //        {
        //            DateTime FirstDay = startdate;
        //            DateTime LastDay = startdate;



        //            var startOfMonth = new DateTime(MonthDayHf.Year, MonthDayHf.Month, 1);
        //            var endOfMonth = startOfMonth.AddMonths(6).AddDays(-1);

        //            if (i == 0)
        //            {
        //                FirstDay = startdate;
        //            }
        //            else
        //            {
        //                FirstDay = startOfMonth;
        //            }

        //            if (i == TotalMonthsHf)
        //            {
        //                LastDay = enddate;
        //            }
        //            else
        //            {
        //                // LastDay = new DateTime(FirstDay.Year, FirstDay.Month, DateTime.DaysInMonth(FirstDay.Year, FirstDay.Month));
        //                LastDay = endOfMonth;
        //            }
        //            int ThisYear = LastDay.Year;
        //            YearQt = ThisYear;
        //            insertPeriod(CalendarKey, seqHf, ThisYear, FirstDay, LastDay, Value);
        //            seqHf += 1;

        //            MonthDayHf = LastDay.AddMonths(1);
        //        }
        //        break;
        //}

        //  Response.Redirect("../Web/Calender.aspx");

        ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:OpenIframePopUp('"+txt_FreqName.Value+"','../Web/IframePeriodList.aspx?CalendarKey="+CalendarKey+"'); ", true);
    }

    private void insertPeriod(string CalendarKey, int seqWeekly, int Year, DateTime startdate, DateTime enddate,string value)
    {
        string Key = Guid.NewGuid().ToString();
        //// PeriodMaster (PeriodKey, CalendarKey, PeriodNumber, PeriodYear, StartDate, EndDate, PeriodName, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        ///
        string str_Sql = "insert into PeriodMaster  (PeriodKey, CalendarKey, PeriodNumber, PeriodYear, StartDate, EndDate, PeriodName, CreatedOn, CreatedBy)"
                            + "select @PeriodKey, @CalendarKey, @PeriodNumber, @PeriodYear, @StartDate, @EndDate, @PeriodName, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PeriodKey", Key);
        cmd.Parameters.AddWithValue("@CalendarKey", CalendarKey);
        cmd.Parameters.AddWithValue("@PeriodNumber", seqWeekly);
        cmd.Parameters.AddWithValue("@PeriodYear", Year);
        cmd.Parameters.AddWithValue("@StartDate", startdate);
        cmd.Parameters.AddWithValue("@EndDate", enddate);
        cmd.Parameters.AddWithValue("@PeriodName", value);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        this.DA.ExecuteNonQuery(cmd);
    }

    public static int GetMonthDifference(DateTime startDate, DateTime endDate)
    {
        int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
        return Math.Abs(monthsApart);
    }
}