﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="DrawList.aspx.cs" Inherits="Web_DrawList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Draw</title>
    <script>
        function deleteDraw(DrawKey, key) {
             var confirmation = confirm("are you sure want to delete this Draw ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/DrawList.aspx/deleteDraw",
                    data: "{DrawKey:'" + DrawKey + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Draw deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
<div class="row">
                              
<div class="col-lg-12 col-md-12">
                                   
<div class="card border-top-2 border-top-primary-400 ">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Draw 
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                                                 
                   <a  class="btn btn-primary btn-sm" href="Draw.aspx">New</a>
                    <a class="list-icons-item" data-action="collapse"></a>
                  
                </div>
            </div>
        </div>
    
        <div class="card-body">
            <div class="table-responsive">
                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>Rep Id</th>
                            <th>Rep Name</th>
                            <th>Type</th>
                            <th>Amount</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="ph_DrawListTr" runat="server"></asp:PlaceHolder>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
    
</div>
</div>
</div>
  
</asp:Content>

