﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Web_CreateUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Users</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
       <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-6 col-md-6">
                                    <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Users <small> <a data-toggle="collapse" data-parent="#accordion1" href="#accordion-group1">( More )</a></small></h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                         <button id="btn_Excel" runat="server" onserverclick="btn_Excel_ServerClick"  type="button" class="btn border-default btn-sm"><i class="icon-file-excel position-left"></i>  .csv</button>
                                      
                                        <a href="Customers.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                    
                            
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Email</th>
                                        <th>Created By</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_UsersTr" runat="server"></asp:PlaceHolder>
                                        
                                </tbody>
                            </table>
                    </div>
                                </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                 <!--Input Form-->
                                 <div id="div_InputForm" runat="server" class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                            <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"> User</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                          <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>First Name<span class="text-danger">*</span></label>
                                                <input type="text" id="txt_FirstName" runat="server" required class="form-control" >
                                            </div>
                                                 <div class="form-group col-md-6">
                                                <label>Last Name<span class="text-danger">*</span></label>
                                                <input type="text" id="txt_LastName" runat="server" required class="form-control" >
                                            </div>
                                                 <div class="form-group col-md-6">
                                                <label>Email<span class="text-danger">*</span> <small>(User Name)</small></label>
                                                <input type="email" id="txt_Email" runat="server" required class="form-control" >
                                            </div>
                                                 <div class="form-group col-md-6">
                                                <label>Phone Number <span class="text-danger">*</span></label>
                                                <input type="text" id="txt_Phone" runat="server" required class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Role <span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddl_UserRole" CssClass="form-control" runat="server"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Password <span class="text-danger">*</span></label>
                                                <input type="password" id="txt_Password" runat="server"  class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="form-check">                               
                                                    <label class="form-check-label">
                                                    <input id="chb_IsActive"  runat="server" type="checkbox" class="form-check-input" />
                                                    Is Active
                                                    </label>
                                                </div>
                                            </div>

                                           
                            </div>
                                        </div>
                                    </div>
                               
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->
                                <!--View Data-->
                                 <div id="div_UserrDetails" runat="server" visible="false" class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                                    <div class="card ">
                            
                                <asp:PlaceHolder ID="ph_UserDetails" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
                            </div>
                        </div>
</asp:Content>

