﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Products : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();

        loadProductList();
        if (Request.QueryString["product_key"] != null)
        {
            viewProductDetails(Request.QueryString["product_key"]);
            
            div_ProductDetails.Visible = true;
            div_ProductDetails.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Visible = false;
        }

        if (!IsPostBack)
        {
            if (Request.QueryString["product_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadProductDetailToEdit(Request.QueryString["product_key"]);
                div_ProductDetails.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }
    [WebMethod]
    public static string viewProduct(string product_key)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select * from Products where ProductKey =@ProductKey ";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@ProductKey ", product_key);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivProductView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    [WebMethod]
    public static string deleteProduct(string ProductKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Products where ProductKey=@ProductKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@ProductKey", ProductKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    private void loadProductDetailToEdit(string product_key)
    {
        string str_Sql_view = "select * from Products where ProductKey =@ProductKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ProductKey", product_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {
            txt_ProductId.Value = dr["ProductId"].ToString();
            txt_ProductName.Value = dr["ProductName"].ToString();
            txt_ProductType.Value = dr["ProductType"].ToString();
            dd_Category.Value = dr["Category"].ToString();
            txt_Prize.Value = dr["Prize"].ToString();
            txt_Units.Value = dr["Units"].ToString();
           

        }
    }

    private void viewProductDetails(string product_key)
    {
        string str_Sql_view = "select * from Products where ProductKey =@ProductKey ";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ProductKey ", product_key);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_ProductDetails, "DivProductView.txt", "");
    }

    private void loadProductList()
    {
        string str_Sql = "select * from Products order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_ProductTr, "ProductListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string str_Key = Guid.NewGuid().ToString();
        //string trans_Key = Guid.NewGuid().ToString();
        string str_Sql = "insert into Products (ProductKey , ProductName ,ProductId, ProductType , Category , Prize , Units , CreatedBy , CreatedOn)"
                            + "select @ProductKey , @ProductName ,@ProductId, @ProductType , @Category , @Prize , @Units , @CreatedBy , @CreatedOn ";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@ProductKey", str_Key);
       
        cmd.Parameters.AddWithValue("@ProductName", txt_ProductName.Value); 
        cmd.Parameters.AddWithValue("@ProductId", txt_ProductId.Value); 
        cmd.Parameters.AddWithValue("@ProductType", txt_ProductType.Value);
        cmd.Parameters.AddWithValue("@Category", dd_Category.Value);
        cmd.Parameters.AddWithValue("@Prize", txt_Prize.Value);
        cmd.Parameters.AddWithValue("@Units", txt_Units.Value);
        
        cmd.Parameters.AddWithValue("@CreatedBy", str_Key);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string product_key = Request.QueryString["product_key"];

        string str_Sql = "update Products set ProductKey=@ProductKey , ProductId=@ProductId, ProductName= @ProductName , ProductType=@ProductType , Category=@Category , Prize=@Prize , Units=@Units , ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where ProductKey=@ProductKey";
                           
        SqlCommand cmd = new SqlCommand(str_Sql);
       

        cmd.Parameters.AddWithValue("@ProductName", txt_ProductName.Value);
        cmd.Parameters.AddWithValue("@ProductType", txt_ProductType.Value);
        cmd.Parameters.AddWithValue("@ProductId", txt_ProductId.Value);
        cmd.Parameters.AddWithValue("@Category", dd_Category.Value);
        cmd.Parameters.AddWithValue("@Prize", txt_Prize.Value);
        cmd.Parameters.AddWithValue("@Units", txt_Units.Value);

        cmd.Parameters.AddWithValue("@ModifiedBy", product_key);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@ProductKey", product_key);


        this.DA.ExecuteNonQuery(cmd);
        Response.Redirect("../Web/Products.aspx?product_key=" + product_key + "");
    }

    [WebMethod]
    public static string ValidateProductId(string ProductId)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "select *  from Products where ProductId=@ProductId ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@ProductId", ProductId);
            DataTable dt = DA.GetDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                return "0";
            }
            else
            {
                return "1";
            }

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}