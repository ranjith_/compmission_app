﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="WorkFlow.aspx.cs" Inherits="Web_WorkFlow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Work Flow</title>
     <script src="https://www.jqueryscript.net/demo/Simple-SVG-Flow-Chart-Plugin-with-jQuery-flowSVG/jquery.scrollTo.min.js"></script>
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/1.0.1/svg.min.js"></script>
<script src="https://www.jqueryscript.net/demo/Simple-SVG-Flow-Chart-Plugin-with-jQuery-flowSVG/dist/flowsvg.min.js"></script>

 <script src="Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/tags/tagsinput.min.js"></script>
    <script src="Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/tags/tokenfield.min.js"></script>
    <script src="Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
    <script src="Limitless_Bootstrap_4/Template/global_assets/js/plugins/ui/prism.min.js"></script>
<script src="Limitless_Bootstrap_4/Template/global_assets/js/demo_pages/form_tags_input.js"></script>
         
<style>
<!--a:hover {
    text-decoration:underline;
}-->
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="container-fluid">
                            <div class="row">
                                <!--List--->
                                <div class="col-md-6">
                                    
                                     <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="panel-title">Approval List
                                   
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                      
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                                              
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>stages</th>
                                        <th>Comments</th>
                                        <th>Due Days</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   
                                        <tr>
                                            <td>Type 1</td>
                                            <td>Ranjith</td>
                                            <td>+310 3434 543</td>
                                            <td>S5, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 2</td>
                                            <td>Rajesh</td>
                                            <td>+310 3434 543</td>
                                            <td>S7, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 3</td>
                                            <td>Balaji</td>
                                            <td>+240 3934 513</td>
                                            <td>S8, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 4</td>
                                            <td>Ram</td>
                                            <td>+210 3434 543</td>
                                            <td>S5, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 5</td>
                                            <td>Sandy</td>
                                            <td>+210 3434 943</td>
                                            <td>S2, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 1</td>
                                            <td>Ranjith</td>
                                            <td>+310 3434 543</td>
                                            <td>S5, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 2</td>
                                            <td>Rajesh</td>
                                            <td>+310 3434 543</td>
                                            <td>S7, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 3</td>
                                            <td>Balaji</td>
                                            <td>+240 3934 513</td>
                                            <td>S8, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 4</td>
                                            <td>Ram</td>
                                            <td>+210 3434 543</td>
                                            <td>S5, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 5</td>
                                            <td>Sandy</td>
                                            <td>+210 3434 943</td>
                                            <td>S2, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 1</td>
                                            <td>Ranjith</td>
                                            <td>+310 3434 543</td>
                                            <td>S5, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 2</td>
                                            <td>Rajesh</td>
                                            <td>+310 3434 543</td>
                                            <td>S7, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 3</td>
                                            <td>Balaji</td>
                                            <td>+240 3934 513</td>
                                            <td>S8, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 4</td>
                                            <td>Ram</td>
                                            <td>+210 3434 543</td>
                                            <td>S5, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                        <tr>
                                            <td>Type 5</td>
                                            <td>Sandy</td>
                                            <td>+210 3434 943</td>
                                            <td>S2, 2nd Street, LosVagas</td>
                                            <td>New york</td>
                                        </tr>
                                </tbody>
                            </table>
                    </div>
                                 </div>
                        </div>
                        <!-- /basic datatable -->
                
                                <div class="">
                                <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"> Flow Chart</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body" style="overflow: scroll;">
                                    
                                            
                                            <div id="drawing" style=" width:100%; height:100%"></div>
                                        </div>
                                    </div>
        
                                </div>
                                
                                </div>
                                <!--List--->
            
            
                                <div class="col-md-6">
                                 <!-- Basic layout-->
                            <form action="#">
                                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Approval Work Flow</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            
                                            <div class="form-group ">
                                                <label>NAME <span>(enter unique name for your workflow)</span><span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" >
                                            </div>
                                            <div class="form-group ">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3"></textarea>
                                            </div>
                                           
                                            <div class="form-group">
                                                <label>Workflow Template</label>
                                                <select class="form-control">
                                                <option>-- select workflow template --</option>
                                                    <option>Template 1</option>
                                                    <option>Template 2</option>
                                                    <option>Template 3</option>
                                                    <option>Template 4</option>
                                                </select>
                                            </div>
                                            <h6>Add Approvers</h6>
                                            <div class="form-group form-group-material">
                                                <label class=""> STAGE 1</label>
                                               <input type="text" class="form-control tokenfield" value="These,are,tokens">
                                            </div>
                                            <div class="form-group form-group-material">
                                                <label class=""> STAGE 2</label>
                                                <input type="text" class="tags-input" placeholder="add multiple approvers">
                                            </div>
                                            <div class="form-group ">
                                                <label>Request Comments <span class="text-danger">*</span></label>
                                                <textarea class="form-control" placeholder="write comments..." rows="3"></textarea>
                                            </div>
                                            <div class="form-group ">
                                                <label>Due Days for the Task <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" >
                                            </div>
                                            <h6>On Aproval</h6>
                                            
                                            <div class="form-group col-md-6  ">
                                                  <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox">
                                                            Send Email
                                                        </label>
                                                </div>    
                                            </div>
                                            <div class="form-group col-md-6 ">
                                                  <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" >
                                                            Alert MEssage
                                                        </label>
                                                </div>    
                                            </div>
                                            <h6>On Reject</h6>
                                            
                                            <div class="form-group  col-md-6">
                                                  <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox">
                                                            Send Email
                                                        </label>
                                                </div>    
                                            </div>
                                            <div class="form-group col-md-6">
                                                  <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" >
                                                            Alert MEssage
                                                        </label>
                                                </div>    
                                            </div>
                                            <div class="form-group form-group-material">
                                                <label class=""> CC to <small>(Notify Following People)</small></label>
                                                <input type="text" class="tags-input" placeholder="add peoples">
                                            </div>
                                            <div class="text-right ">
                                                <button type="submit" class="btn btn-primary">ADD</button>
                                            </div>
                            
                                        </div>
                                    </div>
                                </form>
                                <!-- /basic layout -->
                                </div>
            
                            </div>
                        </div>
    
                  <script>
///////////////////// start flow chart ////////////////////////////////////////////////////////////
    flowSVG.draw(SVG('drawing').size(900, 1100));
    flowSVG.config({
        interactive: false,
        showButtons: false,
        connectorLength: 60,
        scrollto: true
    });
    flowSVG.shapes(
        [
            {
            label: 'knowPolicy',
            type: 'decision',
            text: [
                'Do you know the ',
                'Open Access policy',
                'of the journal?'
            ],
            next: 'df',
            no: 'checkPolicy'
        }, 
      {
            label: 'hasOAPolicy',
            type: 'decision',
            text: [
                'Does it have Open',
                'Access paid option or is it an',
                ' Open Access journal?'
            ],
            yes: 'CCOffered',
            no: 'canWrap',
            
        }, 
        {
            label: 'df',
            type: 'decision',
            text: [
                'Doef h Open',
                'Access paid option or is it an',
                ' Open Access journal?'
            ],
            yes: 'CCOffered',
            no: 'canWrap'
        }, 
        {
            label: 'CCOffered',
            type: 'decision',
            text: [
                'Creative Commons licence',
                'CC-BY offered?'
            ],
            yes: 'canComply',
            no:'checkGreen'
        },
        {
            label: 'canComply',
            type: 'finish',
            text: [
                'Great - can comply. ',
                'Please complete'
            ],
          links: [
              {
                  text: 'application form', 
                  url: 'http://www.jqueryscript.net/chart-graph/Simple-SVG-Flow-Chart-Plugin-with-jQuery-flowSVG.html', 
                  target: '_blank'
              }
          ],
          tip: {title: 'HEFCE Note',
          text:
          [
              'You must put your',
              'accepted version into',
              'WRAP and/or subject',
              'repository within 3 months',
              'of acceptance.'
          ]}
        },
        {
            label: 'canWrap',
            type: 'decision',
            text: [
                'Can you archive in ',
                'WRAP and/or Subject',
                'repository?'
            ],
            yes: 'checkTimeLimits',
            no: 'doNotComply'
        }, 
        {
            label: 'doNotComply',
            type: 'finish',
            text: [
                'You do not comply at all. ',
                'Is this really the only journal',
                ' you want to use? ',
                'Choose another or make ',
                'representations to journal'
            ],
            tip: {title: 'HEFCE Note',
            text:
            [
                'If you really have to go',
                'this route you must log',
                'the exception in WRAP on',
                'acceptance in order',
                'to comply.'
            ]}
        },       
        {
            label: 'checkGreen',
            type: 'process',
            text: [
                'Check the journal\'s policy',
                'on the green route'
            ],
            next: 'journalAllows',
        }, 
        {
            label: 'journalAllows',
            type: 'decision',
            text: ['Does the journal allow this?'],
            yes: 'checkTimeLimits',
            no: 'cannotComply',
            orient: {
                yes:'r',
                no: 'b'
            }
            
        },
        {
            label: 'checkTimeLimits',
            type: 'process',
            text: [
                'Make sure the time limits',
                'acceptable',
                '6 month Stem',
                '12 month AHSS'
            ],
            next: 'depositInWrap'
        },
        {
            label: 'cannotComply',
            type: 'finish',
            text: [
                'You cannot comply with',
                'RCUK policy. Contact ',
                'journal to discuss or',
                'choose another'
            ],
            tip: {title: 'HEFCE Note',
            text:
            [
                'Deposit in WRAP if',
                'time limits acceptable. If',
                'journal does not allow at all',
                'an exception record will',
                'have to be entered',
                'in WRAP, if you feel this is',
                'most appropriate journal.'
            ]}
        },
        {
            label: 'depositInWrap',
            type: 'finish',
            text: [
                'Deposit in WRAP here or ',
                'contact team'
            ],
            tip: {title: 'HEFCE Note',
            text:
            [
                'You must put your',
                'accepted version into',
                'WRAP and/or subject',
                'repository within 3 months',
                'of acceptance.',
                'Note also time limits:',
                'HEFCE 12 months',
                'STEM ? months',
                'AHSS ? months',
                'So you comply here too.'
            ]}
        },
        {
            label: 'checkPolicy',
            type: 'process',
            text: [
                'Check journal website',
                'or go to '
            ],
            links: [
                {
                    text: 'SHERPA FACT/ROMEO ', 
                    url: 'http://www.jqueryscript.net/chart-graph/Simple-SVG-Flow-Chart-Plugin-with-jQuery-flowSVG.html', 
                    target: '_blank'
                }
            ],
            next: 'hasOAPolicy'
        }
    ]);
</script>
</asp:Content>

