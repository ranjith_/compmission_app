﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Web_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Products</title>
    <script>
        function viewProduct(product_key) {
            $.ajax({
                type: "POST",
                url: "../Web/Products.aspx/viewProduct",
                data: "{product_key:'" + product_key + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    // alert(data.d);
                    $('#card-product').html(data.d);
                    $('#div_InputForm').addClass('d-none');
                    $('#div_ProductDetails').removeClass('d-none');

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }

        function deleteProduct(ProductKey, ProductName) {
            var confirmation = confirm("are you sure want to delete this product" + ProductName + " ?");
            if (confirmation == true) {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/Products.aspx/deleteProduct",
                    data: "{ProductKey:'" + ProductKey + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data.d = "DeleteSuccess") {
                            alert("Recipients " + ProductName + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }

                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }

        function ValidateProductId(element) {
            //alert(Currency_Key);
            let ProductId = $(element).val();
            if (ProductId != "") {
                $.ajax({
                    type: "POST",
                    url: "../Web/Products.aspx/ValidateProductId",
                    data: "{ProductId:'" + ProductId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data.d == "0") {
                            $('#btn_Save').addClass('disabled');
                            $('#btn_Update').addClass('disabled');
                            $('#txt_ProductId-error').addClass(' validation-invalid-label');
                            $('#txt_ProductId-error').removeClass(' validation-valid-label');
                            $('#txt_ProductId-error').text('Product Id Already Exists');

                        }
                        else if (data.d == "1") {
                            $('#btn_Save').removeClass('disabled');
                            $('#btn_Update').removeClass('disabled');
                            $('#txt_ProductId-error').removeClass(' validation-invalid-label');
                            $('#txt_ProductId-error').addClass('d-none');
                        }

                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
<div class="container-fluid">
        <div class="row">
            <!--Data List-->
            <div class="col-lg-6 col-md-6">
                <!-- Basic datatable -->
        <div class="card ">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Products </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a href="Products.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                                        
                </div>
            </div>
        </div>
    
        <div class="card-body">
                  
        
        <div class="table-responsive">
        <table class="table datatable-basic">
            <thead>
                <tr>
                     <th>Product Id </th>
                    <th>Product Name</th>
                  
                    <th>Product Category</th>
                    <th>Unit prize</th>
                    <th>Action </th>
                </tr>
                                    
            </thead>
            <tbody>
                   <asp:PlaceHolder ID="ph_ProductTr" runat="server"></asp:PlaceHolder>                
                   
            </tbody>
        </table>
</div>
            </div>
    </div>
    <!-- /basic datatable -->
            </div>
                <!--Data List-->
                <!--Input Form-->
                <div id="div_InputForm" runat="server" class="col-lg-6 col-md-6">
                    <!-- Basic layout-->
        <div class="card ">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Product</h5>
            <div class="header-elements">
                <div class="list-icons">
                        <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                                        
                </div>
            </div>
        </div>
    
        <div class="card-body">
                <div class="row">
                     <div class="form-group col-md-6">
                                <label>Product Id <span class="text-danger">*</span></label>
                                <%--<input type="text" id="txt_ProductType" runat="server" class="form-control typeahead-basic-type" >--%>
                            <input onfocusOut="ValidateProductId(this);" type="text" id="txt_ProductId" required runat="server" class="form-control " >
                        </div>         
                        <div class="form-group col-md-6">
                            <label>Product Name<span class="text-danger">*</span></label>
                          
                            <input type="text" id="txt_ProductName" required runat="server" class="form-control" />
                        </div>
                                  
                        <div class="form-group col-md-6">
                                <label>Product Type <span class="text-danger">*</span></label>
                                <%--<input type="text" id="txt_ProductType" runat="server" class="form-control typeahead-basic-type" >--%>
                            <input type="text" id="txt_ProductType"  runat="server" class="form-control typeahead-basic-type" >
                        </div>
                                            
                        <div class="form-group col-md-6">
                            <label>Category</label>
                            <select id="dd_Category" runat="server"  class="form-control">
                            <option value="">-- select category --</option>
                                <option value="Category 1">Category 1</option>
                                <option value="Category 2">Category 2</option>
                                <option value="Category 3">Category 3</option>
                                <option value="Category 4">Category 4</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                                <label>Prize <small>(per unit in INR)</small> <span class="text-danger">*</span></label>
                                <%--<input id="txt_Prize" runat="server" class="form-control" type="text">--%>
                                <input type="text" id="txt_Prize" required runat="server" class="form-control number"/>
                        </div>
                                            
                        <div class="form-group col-md-6">
                                <label>Units <span class="text-danger">*</span></label>
                                <input id="txt_Units" runat="server" class="form-control number" type="text">
                        </div>
    
                    </div>
                    </div>
                </div>
                               
            <!-- /basic layout -->
            </div>
            <!--Input Form-->
            <!--View Data-->
                                 <div id="div_ProductDetails" runat="server"  class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card " id="card-product">
                            
                                        <asp:PlaceHolder ID="ph_ProductDetails" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
        </div>
    </div>
</asp:Content>

