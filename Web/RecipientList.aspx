﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="RecipientList.aspx.cs" Inherits="Web_RecipientList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Recipients</title>
     <script type="text/javascript">

         function viewRecipient(recip_key) {
             $.ajax({
                 type: "POST",
                 url: "../Web/Recipients.aspx/viewRecipient",
                 data: "{RecipKey:'" + recip_key + "'}",
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function (data) {

                    // alert(data.d);
                     $('#card-recipient').html(data.d);
                     $('#div_InputForm').addClass('d-none');
                     $('#div_RecipientDetails').removeClass('d-none');
                     
                 },
                 error: function () {
                     alert('Error communicating with server');
                 }
             });
         }
         function deleteRecipient(recip_key, recip_id) {
             var confirmation = confirm("are you sure want to delete this " + recip_id + " recipients ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/RecipientList.aspx/deleteRecipient",
                    data: "{RecipKey:'" + recip_key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Recipients " + recip_id + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-12 col-md-12">
                                    <!-- Basic datatable -->
                                    <div class="card ">
                                            <div class="card-header header-elements-inline">
                                                <h5 class="card-title">Recipient</h5>
                                                <div class="header-elements">
                                                    <div class="list-icons">
                                                     <%--   <button id="btn_Excel" runat="server" onserverclick="btn_Excel_ServerClick" onclick="" type="button" class="btn border-default btn-sm"><i class="icon-file-excel position-left"></i>  .csv</button>--%>
                                                         <a href="Recipient.aspx?IsNew=1" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                                        <a class="list-icons-item" data-action="collapse"></a>
                                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                                    </div>
                                                </div>
                                            </div>
    
                                            <div class="card-body">
                                   
                                
                            
                                            <div class="table-responsive">
                                            <table class="table datatable-basic " id="tt">
                                                <thead>
                                                    <tr>
                                                        <th>Rep Id</th>
                                                        <th>Name</th>
                                                        <th>User Id</th>
                                                        <th>Recipient Type</th>
                                                        <th>Reporting</th>
                                                        <th>Recipient Territory</th>
                                                        <th>Join Date</th>
                                                        <th>Salary Amount</th>
                                                        <th>Action </th>
                                                    </tr>
                                    
                                                </thead>
                                                <tbody>
                                                   <asp:PlaceHolder ID="ph_RecipientTableRows" runat="server"></asp:PlaceHolder>
                                    
                                                </tbody>
                                            </table>
                                    </div>
                                                </div>
                                        </div>
                                    <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                
                            
                            </div>
         </div>
</asp:Content>

