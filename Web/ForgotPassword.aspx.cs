﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Web_ForgotPassword : System.Web.UI.Page
{
    OtherCommonFunction OCF;
    DataAccess DA;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.OCF = new OtherCommonFunction();
        this.DA = new DataAccess();

    }

    protected void btn_RecoverPwd_ServerClick(object sender, EventArgs e)
    {
        string usermail = txt_Email.Value;

        string str_Sql = "select * from users u join Credential c on u.UserKey = c.UserKey where u.Email =@Email";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@Email", usermail);
        
        DataTable dt = this.DA.GetDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
           
            string password= dt.Rows[0]["Password"].ToString();
            string username = dt.Rows[0]["FirstName"].ToString();

            string subject = "Password Recovery | IMS";
            
            string message = "Hi "+username +". Your Password is "+password;


            OCF.SendMail(subject,message,usermail, "");


            string div = "<div class='text-center mb-3'>" +
                "<i class='icon-mail-read icon-2x text-success border-success border-3 rounded-round p-3 mb-3 mt-1'></i>" +
                "<h5 class='mb-0'>Password recovery</h5>" +
                "<span class='d-block text-muted'>Check your email, we sent your password</span>" +
                "<a href='../Web/Login.aspx' class='btn btn-primary btn-sm mt-4'>Login</a>" +
                "</div>";
            div_CardBody.InnerHtml = div;
        }
        else
        {
            lbl_Err.Text = "Invalid User Mail Id";
        }
        
    }
}