﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="RoleList.aspx.cs" Inherits="Web_RoleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Role List</title>
    <script type="text/javascript">
        function deleteRole(role_key, role_name) {
            var confirmation = confirm("are you sure want to delete this " + role_name + " Role ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/RoleList.aspx/deleteRole",
                    data: "{RoleKey:'" + role_key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Role " + role_name + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
<div class="container-fluid">
    <div class="row">
        <!--Data List-->
        <div class="col-lg-12 col-md-12">
                <div class="card ">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Role List </h5>
                    <div class="header-elements">
                        <div class="list-icons">
                               
                            <a href="Roles.aspx?Id=" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                                        
                        </div>
                    </div>
                </div>
    
                <div class="card-body">
                          
                
                <div class="table-responsive">
                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>Role Name</th>
                            <th>Role Desc</th>
                            <th>Is SystemRole</th>
                            <th>Created On</th>
                            <th>Action</th>
                        </tr>
                                    
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="ph_RoleListTr" runat="server"></asp:PlaceHolder>
                                        
                    </tbody>
                </table>
                </div>
                    </div>
                </div>
        </div>                  
    </div>
</div>
</asp:Content>

