﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="ImportTest.aspx.cs" Inherits="Web_ImportTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Import test</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container">
        <div class="card card-body">
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <asp:Button ID="Upload"  Text="Upload" CssClass="btn btn-success" OnClick="Upload_Click" runat="server" />
        </div>
    </div>
</asp:Content>

