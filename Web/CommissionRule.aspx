﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="CommissionRule.aspx.cs" Inherits="Web_CommissionRule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission Rule</title>
    <style>
        .table-rules td{
            border:none;
                padding: .2rem 1.25rem;
        }
        .IFrameStyle{
            

            border: none;
            box-shadow: -1px 1px 9px 0px #1908081c;
            border-top: 3px solid #1fe61f;

        }
    </style>
    <script>
        
    </script>
    <script src="../JScript/CommissionRule.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="card border-top-success border-top-2">
                    <div class="card-header header-elements-inline">
                         <h5 class="card-title d-flex text-success">  <span class="ml-2"><i class="icon-clipboard5"></i> #Rule <span id="span_Sequence" runat="server" class="h1">2</span> </span> 
                </h5> 
                        <div class="header-elements">
                             <asp:Button ID="btn_SaveRule"  Text="Save" OnClick="btn_SaveRule_ServerClick" OnClientClick="javascript:return ValidateRule()"  runat="server" CssClass="btn btn-primary  btn-sm " />
                                                    <asp:Button ID="btn_UpdateRule" Text="Update" Visible="false"  OnClientClick=""  OnClick="btn_UpdateRule_ServerClick" runat="server" CssClass="btn btn-primary  btn-sm " />


                        </div>
                    </div>
                    <div class="card-body">
                           <div class="row">

                    
                    <div class="col-md-12">

                   
                        <div class="row m-1 card-cusom-commission">
                                <div class="form-group col-md-6">
                                    <label>Rule Name<span class="text-danger">*</span></label>
                                    <input   type="text" id="txt_RuleName" runat="server"  class="form-control required " required>
                                  
                                </div>
                             <div class="form-group col-md-6">
                                    <label>Variable Name<span class="text-danger">*</span></label>
                                    <input onfocusout="checkVariableName(this);"   type="text" id="txt_VariableName" required runat="server" class="form-control required" >
                                 
                                </div>
                                <div class="form-group col-md-6">
                                                <label>Function<span class="text-danger">*</span></label>
                                                <asp:DropDownList onchange="CallFunction(this)" CssClass="form-control" ID="ddl_Functions" required runat="server"></asp:DropDownList>
                                             </div>
                            
                                <div class="form-group col-md-6">
                                     <label>Variable Scope</label>
                                    <asp:HiddenField ID="hdn_FunctionKey" runat="server" ClientIDMode="Static" />
                                    <asp:DropDownList CssClass="form-control" ID="ddl_VariableType" runat="server"></asp:DropDownList>
                                </div>
                            
                               
                           
                               <div id="IfFunction" class="form-group  col-md-12 d-none ">
                           
                                   <label>Custom Rule / Evaluate <span class="text-danger">*</span></label>
                                    <textarea rows="2"  id="txt_FunctionDetails"  runat="server" class="form-control readonly" readonly></textarea>                                 
                      
                                </div>
                            <div class="col-md-2 my-auto" id="IFrameFunc">
                                <h5 class="text-muted" id="FunctionName" runat="server" ></h5>
                                <button type="button" id="btn_EditFunction" class="btn btn-primary btn-sm d-none"  onclick="EditSplit();" runat="server" >Edit</button>
                            </div>
                        </div>
                       
                         <div class="form-group col-md-12 text-right">
                             
                            </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>

                <div class="col-md-3 ">
               
                
                     <div class="card border-top-3 border-top-success-600">
							<div class="card-body">
                                <div class="text-center">
								<h6 class="mb-2 font-weight-semibold">Functions </h6>
								
							</div>
                           <div class="d-flex justify-content-between ">
                                <button type="button" data-toggle="modal" data-target="#modalSplit"  class="btn btn-success bg-green-600 btn-sm"> Split()</button> 
                                <button type="button" data-toggle="modal" data-target="#modalOverride"  class="btn btn-success bg-green-600 btn-sm"> Override()</button>
                                <button type="button" data-toggle="modal" data-target="#modalRange"  class="btn btn-success bg-green-600 btn-sm"> Matrix()</button> 
                                
                           </div>
                                <div class="d-flex justify-content-between mt-2">
                                    <button type="button" data-toggle="modal" data-target="#modalGoal" class="btn btn-success bg-green-600 btn-sm"> Goal()</button>
                                    <button type="button" data-toggle="modal" data-target="#" class="btn btn-success bg-green-600 btn-sm"> Summarize()</button>
                                    <button type="button" data-toggle="modal" data-target="#"  class="btn btn-success bg-green-600 btn-sm"> Crediting()</button> 
                                </div>
                                 <div class="d-flex justify-content-between mt-2">
                                    <button type="button" data-toggle="modal" data-target="#modalGoal" class="btn btn-success bg-green-600 btn-sm"> Custom()</button>
                                    
                                </div>
                              
							</div>

					</div>
                     <div class="card border-top-3 border-top-success-600">
							<div class="card-body">
                                <div class="text-center">
								<h6 class="mb-2 font-weight-semibold">Existing System Variables</h6>
								
							</div>
                            <h6 class="text-muted "> #SalesAmount </h6>
							<h6 class="text-muted h6"> #CompRate </h6>
                                <h6 class="text-muted h6"> #CalculationPayOut </h6>
                                
							</div>

						</div>
                </div>
        </div>
    </div>

   <!-- for popup functions-->

       <!-- Goal modal -->
                <div id="modalGoal" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Goal</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                 <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Goal-list" class="nav-link active" data-toggle="tab">Goal List</a></li>
									<li class="nav-item"><a href="#Goal-new" class="nav-link" data-toggle="tab">New Goal</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Goal-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Goal</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Goal 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Goal 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Goal 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Goal-new">
										 <div class="row">
                                           
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Goal Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <table style="background-color: #80808040;"  class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>Period</th><th>Goal Value</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="set-goal">
                                                                <tr>
                                                                    <td class="tr-count">1</td>
                                                                   <td>
                                                                        <div class="">
                                                                         <input name="period" placeholder="period" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="">
                                                                         <input name="goal" placeholder="goal" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td class="td-btn">
                                                                        <div class="d-inline-block add-goal">
                                                                            <button type="button" class="btn btn-primary btn-sm add-goal-btn btn-xx mr-2" onclick="addGoal()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="addGoal(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>  
                                                                   </td>
                                                                </tr>
                                                               
                                                            </tbody>
                                                        </table>
                                             <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveRange()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div> 
									</div>

									
								</div>
                                   
                            </div>

                           
                        </div>
                    </div>
                </div>
                <!-- /goal modal -->
    
                <!-- Range modal -->
                <div id="modalRange" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Range</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                 <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Range-list" class="nav-link active" data-toggle="tab">Range List</a></li>
									<li class="nav-item"><a href="#Range-new" class="nav-link" data-toggle="tab">New Range</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Range-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Range</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Range 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Range 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Range 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Range-new">
										 <div class="row">
                                            
                                           
                                            <div class="form-group col-md-12">
                                                    <label>Range Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>From</th><th>To</th><th>Value</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="set-range">
                                                                <tr>
                                                                    <td class="td-count">1</td>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input name="from" placeholder="from" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input name="to" placeholder="to" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input name="value" placeholder="value" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td class="td-btn">
                                                                        <div class="d-inline-block add-range">
                                                                            <button type="button" class="btn btn-primary btn-sm add-range-btn btn-xx mr-2" onclick="addRange()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="addGoal(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>  
                                                                   </td>
                                                                </tr>
                                                              
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveRange()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                           
                        </div>
                    </div>
                </div>
                <!-- /Range modal -->
    
                <!-- override modal -->
                <div id="modalOverride" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Override</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                  <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Override-list" class="nav-link active" data-toggle="tab">Override List</a></li>
									<li class="nav-item"><a href="#Override-new" class="nav-link" data-toggle="tab">New Override</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Override-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Levels</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Levels 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Levels 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Levels 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Override-new">
										 <div class="row">
                                            
                                            
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Level Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr> <th>Level</th><th>%</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="Override-Levels">
                                                               
                                                                <tr>
                                                                    <td class="td-level">
                                                                        <div class="form-group">
                                                                         <label>Level 1</label>
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="%" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    
                                                                   <td class="tr-btn">
                                                                     <div class="d-inline-block add-override-level">
                                                                            <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>    
                                                                   </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveOverride()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                            
                        </div>
                    </div>
                </div>
                <!-- /override modal -->
    
                <!-- split modal -->
                <div id="modalSplit" class="modal fade" tabindex="-1">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;Split</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#split-list" class="nav-link active" data-toggle="tab">Split List</a></li>
									<li class="nav-item"><a href="#split-new" class="nav-link" data-toggle="tab">New Split</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="split-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total recipients</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Split 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Split 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Split 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="split-new">
										 <div class="row">
                                            
                                            
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Split Name  <span class="text-danger">*</span></label>
                                                    <input id="txt_SplitName" type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea id="txt_SplitDesc" class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>Recipient</th><th>%</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="Split-Recipients">
                                                                
                                                                <tr id="split-1">
                                                                    <td class="tr-count">1</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         
                                                                          <select class="form-control recipient-dd" >
                                                                             <option value="">-select recipient-</option>
                                                                             <option value="Raj">Raj</option>
                                                                             <option value="John">John</option>
                                                                             <option value="Bob">Bob</option>
                                                                         </select>
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="%"  name="RecipientPercentage[]"  type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    
                                                                   <td class="tr-btn">
                                                                       <div class="d-inline-block add-recipient">
                                                                            <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>
                                                                  </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveSplit()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>

                                             
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                            <%--<div class="modal-footer">
                              
                               
                            </div>--%>
                        </div>
                    </div>
                </div>
                <!-- /split modal -->


    <!--Iframe pop up-->
      
                <div id="PopUpIframe" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-full h-100 ">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;Function</h5>
                                <button type="button" class="close " data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                              <iframe id="iFramePopUp" class="IFrameStyle" height="100%" width="100%" src=""></iframe>
                               
                            </div>

                           <%-- <div class="modal-footer text-center">
                              <button type="button" class="btn btn-primary" onclick="LoadFunctionStatements('split');">Close</button>
                            </div>--%>
                        </div>
                    </div>
                </div>
                <!-- /Iframe -->

    <asp:HiddenField ID="hdn_RuleKey" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdn_CommissionKey" runat="server" ClientIDMode="Static" />
</asp:Content>

