﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="MenuList.aspx.cs" Inherits="Web_MenuList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Menu List</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <!--Data List-->
            <div class="col-lg-6 col-md-6">
                <!-- Basic datatable -->
        <div class="card ">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Menu List </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a href="MenuList.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                                        
                </div>
            </div>
        </div>
    
        <div class="card-body">
                  
                                
        
        <div class="table-responsive">
        <table class="table datatable-basic">
            <thead>
                <tr>
                        <th><b>Name</b></th>
                        <th><b>Parent Menu </b></th>
                      <%--  <th><b>Menu Type</b></th>--%>
                        
                        <th><b>Is Active</b></th>
                        <th><b> Action</b></th>
                        <%--<th><i class="icon-calendar2 position-left"></i><b>Last Modified On</b></th>--%>
                </tr>
                                    
            </thead>
            <tbody>
                   <asp:PlaceHolder ID="ph_MenuListTr" runat="server"></asp:PlaceHolder> 
            </tbody>
        </table>
</div>
            </div>
    </div>
    <!-- /basic datatable -->
            </div>
                <!--Data List-->
                <!--Input Form-->
                <div id="div_InputForm" runat="server" class="col-lg-6 col-md-6">
                    <!-- Basic layout-->
        <div class="card ">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Add Menu</h5>
            <div class="header-elements">
                <div class="list-icons">
                        <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                                        
                </div>
            </div>
        </div>
    
        <div class="card-body">
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Menu Name<span class="text-danger">*</span></label>
                            <input type="text" id="txt_MenuName" runat="server" class="form-control" />
                        </div>
                                            
                        <div class="form-group col-md-6">
                                <label>Menu Description<span class="text-danger">*</span></label>
                              
                            <input type="text" id="txt_MenuDescription" runat="server" class="form-control typeahead-basic-type" >
                        </div>
                                            
                        <div class="form-group col-md-6">
                            <label>Parent Menu Id</label>
                            <asp:DropDownList ID="dd_ParentMenuId" CssClass="form-control" runat="server"></asp:DropDownList>
                            <%--<select id="" runat="server" class="form-control">
                            <option value="">-- select parent menu --</option>
                                <option value="Category 1">Category 1</option>
                                <option value="Category 2">Category 2</option>
                                <option value="Category 3">Category 3</option>
                                <option value="Category 4">Category 4</option>
                            </select>--%>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Menu Type</label>
                             <asp:DropDownList ID="dd_MenuType" CssClass="form-control" runat="server"></asp:DropDownList>
                           <%-- <select id="" runat="server" class="form-control">
                            <option value="">-- select menu type --</option>
                                <option value="Category 1">Category 1</option>
                                <option value="Category 2">Category 2</option>
                                <option value="Category 3">Category 3</option>
                                <option value="Category 4">Category 4</option>
                            </select>--%>
                        </div>
                        <div class="form-group col-md-6">
                                <label>Target Url</label>
                                <input type="text" id="txt_TargetUrl" runat="server" class="form-control"/>
                        </div>
                        <div class="form-group col-md-6">
                                <label>Menu Order <span class="text-danger">*</span></label>
                                <input id="txt_MenuOrder" runat="server" class="form-control" type="text">
                        </div>
                        <div class="form-group col-md-6">
                                <label>Menu Icon <span class="text-danger">*</span></label>
                                <input id="txt_MenuIcon" runat="server" class="form-control" type="text"/>
                        </div>
                        <div class="form-group col-md-3">      
                            <div class="form-check">
                                
	                            <label class="form-check-label">
		                            <input id="chb_IsActive"  runat="server" type="checkbox" class="form-check-input" />
		                            Is Active
	                            </label>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="form-check">
	                            <label class="form-check-label">
		                            <input id="chb_IsNewWindow"  runat="server" type="checkbox" class="form-check-input" />
		                            Is New Window
	                            </label>
                            </div>
                        </div>
                        
    
                    </div>
                    </div>
                </div>
                               
            <!-- /basic layout -->
            </div>
            <!--Input Form-->
            <!--View Data-->
                                 <div id="div_MenuDetails" runat="server" visible="false" class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                                    <div class="card ">
                            
                                        <asp:PlaceHolder ID="ph_MenuDetails" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
        </div>
    </div>
</asp:Content>

