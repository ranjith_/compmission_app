﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"   AutoEventWireup="true" CodeFile="Transaction.aspx.cs" Inherits="Web_Transaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Transactions</title>
    <script type="text/javascript">

        function viewTransaction(trans_key) {
            $.ajax({
                type: "POST",
                url: "../Web/Transaction.aspx/viewTransaction",
                data: "{trans_key:'" + trans_key + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    // alert(data.d);
                    $('#card-transaction').html(data.d);
                    $('#div_InputForm').addClass('d-none');
                    $('#div_TransactionDetails').removeClass('d-none');

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }

        function deleteTransaction(trans_key, trans_id) {
            var confirmation = confirm("are you sure want to delete this " + trans_id + " transaction ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/Transaction.aspx/deleteTransaction",
                    data: "{TransKey:'" + trans_key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Transaction " + trans_id + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-12 col-md-12">
                                    <!-- Basic datatable -->
                    
                    <div class="card border-top-2 border-top-primary-600">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Transactions 
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                        <button id="btn_Excel" runat="server" onserverclick="btn_Excel_ServerClick" onclick="" type="button" class="btn border-default btn-sm"><i class="icon-file-excel position-left"></i>  .csv</button>
                                       <%-- <button type="button" class="btn border-default text-default-500 btn-xs btn-flat"><i class="icon-file-pdf position-left"></i>.pdf</button>
                                        <button type="button" class="btn border-default text-default-500 btn-xs btn-flat"><i class="icon-file-excel position-left"></i>  .csv</button>
                                        <button type="button" class="btn border-default text-default-500 btn-xs btn-flat"><i class="icon-file-word position-left"></i>  .doc</button>--%>
                                        <a href="TransactionEdit.aspx?New=1" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                            
                             
                            <div class="table-responsive">
                           
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Tranasction ID</th>
                                        <th>Transaction Line</th>
                                        <th>Effective Date</th>
                                        <th>Product</th>
                                        <th>Customer</th>
                                        <th>Sales Amount</th>
                                        <th>Transaction Type</th>
                                        <th>Action</th>
                                        
                                    
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   
                                       
                                        <asp:PlaceHolder ID="tbodyTransaction" runat="server"></asp:PlaceHolder>
                                </tbody>
                            </table>
                    </div>
                        </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                

                                 <!--View Data-->
                                 <div id="div_TransactionDetails" runat="server"  class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card" id="card-transaction">
                            
                                <asp:PlaceHolder ID="ph_Transaction_Details" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
                            </div>
                        </div>
</asp:Content>

