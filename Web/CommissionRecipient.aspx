﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" ClientIDMode="Static" CodeFile="CommissionRecipient.aspx.cs" Inherits="Web_CommissionRecipient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <script src="../JScript/CommissionRecipients.js"></script>
    <script>

function assignRecipientBy(val) {
    let assign = $(val).val();
    if (assign == 1) {
        $('#Individual_Recipients').removeClass('d-none');
        $('#Condition_Recipients').addClass('d-none');
        $('#lbl_ErrorSelectRecipients').addClass('d-none');
    }
    else if (assign == 2) {
        $('#Individual_Recipients').addClass('d-none');
        $('#Condition_Recipients').removeClass('d-none');
        $('#lbl_ErrorSelectRecipients').addClass('d-none');
    }
    else {
        $('#Individual_Recipients').addClass('d-none');
        $('#Condition_Recipients').addClass('d-none');
        $('#lbl_ErrorSelectRecipients').removeClass('d-none');
    }
}
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="card border-top-2 border-top-warning">
                    <div class="card-header header-elements-inline">
                         <h5 class="card-title d-flex">  <span class="ml-2"><i class="icon-user-check"></i> Assign Recipients</span>
                            </h5> 
                        <div class="header-elements">
                              <button id="btn_SaveRecipient" onclick="assignRecipeientsInsert();" runat="server" type="button" class="btn btn-primary btn-sm">Save</button>
                              <button id="btn_UpdateRecipient" visible="false" onclick="UpdateRecipients();" runat="server" type="button" class="btn btn-primary btn-sm">Update</button>
                        </div>
                    </div>
                    <div class="card-body">
                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <h6>Assign Recipients</h6>

                                                <select id="DdAssignRecipient" runat="server" onchange="assignRecipientBy(this)" class="form-control">
                                                    <option value="0">-select type-</option>
                                                    <option value="1">Select Invidiual Recipients</option>
                                                     <option value="2">Select  Recipients By Conditions</option>
                                                </select>
                                             
                                                
                                            </div>
                                            <div class="form-group col-md-6 my-auto text-center">
                                                <asp:Label CssClass="text-warning text-center" ID="lbl_ErrorSelectRecipients" runat="server"></asp:Label>
                                               
                                            </div>
                                             <div runat="server" class="col-md-12 d-none" id="Individual_Recipients">
                                                 <div class="table-responsive">
                                                <table id="table-Individual-Recipient"  class="table datatable-basic text-center" >
                                                    <thead>
                                                        <tr>
                                                            <th>Select Recipients </th>
                                                            <th>Name</th>
                                                            <th>Role</th>
                                                            <th>Reporting to </th>
                                                            <th>Territory</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:PlaceHolder ID="ph_RecipientListtr" runat="server"></asp:PlaceHolder>
                                                      
                                                    </tbody>
                                                </table>
                                                </div>
                                             </div>
                                            <div runat="server" class="form-group col-md-12 d-none" id="Condition_Recipients">
                                               
                                                <table id="table-Condition-Recipient"  class="table custom-panel">
                                                    <thead class="text-center">
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Operator</th>
                                                            <th>Role</th>
                                                            <th>Condition</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:PlaceHolder ID="ph_RecipientConditionTr" runat="server"></asp:PlaceHolder>
                                                      
                                                    </tbody>
                                                </table>
                                            </div>
                                               <div class="form-group col-md-12 text-right">
                                             
                                                   
                                                </div>
                                </div>
                    </div>
                </div>
            </div>

               <div class="col-md-3 ">
               
                
                     <div class="card border-top-1 border-top-warning-600">
							<div class="card-body">
                                <div class="text-center">
								<h6 class="mb-2 font-weight-semibold">Quick Links </h6>
								
							</div>
                           <div class="d-flex justify-content-between">
                               <a target="_blank" href="../Web/Recipient.aspx?IsNew=1" class="btn btn-warning bg-warning-300 btn-sm">Add Recipient</a>
                                <a target="_blank"  href="../Web/RecipientList.aspx" class="btn btn-warning bg-warning-300 btn-sm">View Recipient List</a>
                           </div>
                              
							</div>

					</div>
                     <div class="card border-top-1 border-top-warning-600">
							<div class="card-body">
                                <div class="text-center">
								<h6 class="mb-2 font-weight-semibold">Info</h6>
								
							</div>
                            <h6 class="text-muted ">- Total Recipients <span class="badge bg-warning-300 ml-4" id="lbl_RecipientCount" runat="server">157</span></h6>
							<h6 class="text-muted ">- Total Territory <span class="badge bg-warning-400 ml-4" id="lbl_TerritoryCount" runat="server">16</span></h6>
							</div>

						</div>
                </div>
        </div>
    </div>
       <asp:HiddenField id="hdn_CommissionKey" ClientIDMode="Static"  runat="server"/>
</asp:Content>

