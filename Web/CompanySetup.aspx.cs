﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class Web_CompanySetup : System.Web.UI.Page
{
    Common CC;
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    public string str_UserKey = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        
        this.CC = new Common();

        str_UserKey = "15702C1E-1447-4C08-9773-7BD324256180";

        if (CC.GetQueryStringValue("isedit") == "1")
        {
            div_CompanySetupDetails.Visible = false;
            div_CompanySetupInputForm.Visible = true;
        }
        loadCompanySetupView(str_UserKey);
        if (!IsPostBack)
        {
            loadCompanySetupToForm(str_UserKey);
        }
        
    }

    private void loadCompanySetupView(string str_UserKey)
    {
        string str_Sql_view = "select * from CompanySetup where UserKey =@UserKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@UserKey", str_UserKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_CompanyDetails, "DivCompanySetup.txt", "");
    }

    private void loadCompanySetupToForm(string str_UserKey)
    {
        string str_Sql_view = "select * from CompanySetup where UserKey =@UserKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@UserKey", str_UserKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {
            //       StateReport    WebsiteReport)

            txt_CompanyName.Value = dr["CompanyName"].ToString();
            txt_Email.Value = dr["Email"].ToString();
            txt_Mobile.Value = dr["Mobile"].ToString();
            dd_Country.Value = dr["Country"].ToString();
            dd_State.Value = dr["State"].ToString();
            dd_City.Value = dr["City"].ToString();
            dd_Currency.Value = dr["Currency"].ToString();
            dd_TimeZone.Value = dr["TimeZone"].ToString();
            txt_Address1.Value = dr["Address1"].ToString();
            txt_Address2.Value = dr["Address2"].ToString();
            txt_Website.Value = dr["Website"].ToString();
            if (dr["Logo"].ToString() != "")
            {
                hdn_LogoName.Value = dr["Logo"].ToString();
                img_Logo.Src = "../UploadFile/Logo/" + dr["Logo"].ToString();
            }
            else
            {
                err_NoLogo.Visible = true;
            }
           
            txt_AprCompanyName.Value = dr["CompanyNameReport"].ToString();
            txt_AprEmail.Value = dr["EmailReport"].ToString();
            txt_AprMobile.Value = dr["MobileReport"].ToString();
            dd_AprCountry.Value = dr["CountryReport"].ToString();
            dd_AprState.Value = dr["StateReport"].ToString();
            dd_AprCity.Value = dr["CityReport"].ToString();
            txt_AprAddress1.Value = dr["Address1Report"].ToString();
            txt_AprAddress2.Value = dr["Address2Report"].ToString();
            txt_AprWebsite.Value = dr["WebsiteReport"].ToString();

        }
    }

    protected void btn_Edit_Click(object sender, EventArgs e)
    {

    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        
        string fileName = hdn_LogoName.Value;

        if (fu_CompanyLogo.HasFile)
        {
            if (fileName != "")
            {
                //string ExistingFilePath = Server.MapPath("~/UploadFile/Logo");
                //string ExistingFilePath
            }
            string str_FileKey = Guid.NewGuid().ToString();

            string OrginalfileName = Path.GetFileName(fu_CompanyLogo.PostedFile.FileName);

            fileName = str_FileKey + "_" + OrginalfileName;
            //Set the Image File Path.
            string filePath = "~/UploadFile/Logo/" + fileName;
            //Save the Image File in Folder.
            fu_CompanyLogo.PostedFile.SaveAs(Server.MapPath(filePath));
        }

        string str_Sql = "UPDATE CompanySetup set CompanyName=@CompanyName, Email=@Email, Mobile=@Mobile, Country=@Country, State=@State, City=@City, Currency=@Currency, TimeZone=@TimeZone, " +
            "Address1=@Address1, Address2=@Address2, Website=@Website, Logo=@Logo, CompanyNameReport=@CompanyNameReport, EmailReport=@EmailReport, MobileReport=@MobileReport, " +
            "CountryReport=@CountryReport, StateReport=@StateReport, CityReport=@CityReport, Address1Report=@Address1Report, Address2Report=@Address2Report," +
            " WebsiteReport=@WebsiteReport, ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where UserKey=@UserKey";

        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CompanyName", txt_CompanyName.Value);
        cmd.Parameters.AddWithValue("@Email", txt_Email.Value);
        cmd.Parameters.AddWithValue("@Mobile", txt_Mobile.Value);
        cmd.Parameters.AddWithValue("@Country", dd_Country.Value);
        cmd.Parameters.AddWithValue("@State", dd_State.Value);
        cmd.Parameters.AddWithValue("@City", dd_City.Value);
        cmd.Parameters.AddWithValue("@Currency", dd_Currency.Value);
        cmd.Parameters.AddWithValue("@TimeZone", dd_TimeZone.Value);
        cmd.Parameters.AddWithValue("@Address1", txt_Address1.Value);
        cmd.Parameters.AddWithValue("@Address2", txt_Address2.Value);
        cmd.Parameters.AddWithValue("@Website", txt_Website.Value);

        cmd.Parameters.AddWithValue("@Logo", fileName);
        cmd.Parameters.AddWithValue("@CompanyNameReport", txt_AprCompanyName.Value);
        cmd.Parameters.AddWithValue("@EmailReport", txt_AprEmail.Value);
        cmd.Parameters.AddWithValue("@MobileReport", txt_AprMobile.Value);
        cmd.Parameters.AddWithValue("@CountryReport", dd_AprCountry.Value);
        cmd.Parameters.AddWithValue("@StateReport", dd_AprState.Value);
        cmd.Parameters.AddWithValue("@CityReport", dd_AprCity.Value);
        cmd.Parameters.AddWithValue("@Address1Report", txt_AprAddress1.Value);
        cmd.Parameters.AddWithValue("@Address2Report", txt_AprAddress2.Value);
        cmd.Parameters.AddWithValue("@WebsiteReport", txt_AprWebsite.Value);

        cmd.Parameters.AddWithValue("@ModifiedBy", str_UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@UserKey", str_UserKey);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/CompanySetup.aspx");
    }
}