﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="TransactionView.aspx.cs" Inherits="Web_TransactionView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Transaction View</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header header-elements-inline  border-bottom mb-3">
                                <h5 class="panel-title">Transactions Details </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        
                                        <button id="btn_EditTransaction" runat="server" type="button" class="btn btn-primary btn-sm">Edit</button>
                                        
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                     </div>
                    <div class="card-body">
                        <asp:PlaceHolder ID="ph_TransactionDetail" runat="server"></asp:PlaceHolder>                                               
                    </div>
                </div>
            </div>
               <div class="col-12">
                <div class="card">
                    <div class="card-header header-elements-inline border-bottom mb-3">
                                <h5 class="panel-title">Transaction Customer & Sales Details </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        
                                       
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                     </div>
                    <div class="card-body">
                        <asp:PlaceHolder ID="ph_CustomerDetails" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

