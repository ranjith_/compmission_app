﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CreateUser : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.Com = new Common();
         loadUserList();

        if (!IsPostBack)
        {
            string str_Select = "select RoleKey as Value,RoleName as Text from Role";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.Com.LoadDropDown(ddl_UserRole, ds.Tables[0], "Text", "Value", true, "--Select Role--");
           
        }

    }

    private void loadUserList()
    {
        string str_Sql = "select *,(select FirstName from users where UserKey=u.CreatedBy) as CreadedByUser from Users u";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_UsersTr, "UsersListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string str_UserKey = Guid.NewGuid().ToString();
        string str_CredentialKey = Guid.NewGuid().ToString();
        string str_UserRoleKey = Guid.NewGuid().ToString();

        //(UserKey, FirstName, LastName, Email, Phone, status, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn)

        string str_Sql = "insert into Users (UserKey, FirstName, LastName, Email, Phone, status, CreatedBy, CreatedOn) " +
            "select @UserKey, @FirstName, @LastName, @Email, @Phone, @status , @CreatedBy , @CreatedOn "
            + "insert into Credential (CredentialKey ,UserName ,Password ,UserKey ,CreatedBy ,CreatedOn) select @CredentialKey , @UserName , @Password , @UserKey , @CreatedBy , @CreatedOn "
            + " insert into UserRole(UserRoleKey,UserKey,RoleKey,CreatedBy ,CreatedOn) select @UserRoleKey, @UserKey, @RoleKey, @CreatedBy , @CreatedOn ";

       // "insert into Credential (CredentialKey ,UserName ,Password ,UserKey ,CreatedBy ,CreatedOn) select "
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@UserKey", str_UserKey);
        cmd.Parameters.AddWithValue("@UserName", txt_Email.Value);
        cmd.Parameters.AddWithValue("@Password", txt_Password.Value);
        cmd.Parameters.AddWithValue("@FirstName", txt_FirstName.Value);
        cmd.Parameters.AddWithValue("@LastName", txt_LastName.Value);
        cmd.Parameters.AddWithValue("@Email", txt_Email.Value);
        cmd.Parameters.AddWithValue("@Phone", txt_Phone.Value);
        if(chb_IsActive.Checked==true)
            cmd.Parameters.AddWithValue("@status", "1");
        else
            cmd.Parameters.AddWithValue("@status", "0");
        cmd.Parameters.AddWithValue("@CredentialKey", str_CredentialKey);
        cmd.Parameters.AddWithValue("@RoleKey", ddl_UserRole.SelectedValue);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.GetUserKey());
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        cmd.Parameters.AddWithValue("@UserRoleKey", str_UserRoleKey);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {

    }

    protected void btn_Excel_ServerClick(object sender, EventArgs e)
    {

    }
}