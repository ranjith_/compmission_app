﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="CommissionCalculation.aspx.cs" Inherits="Web_CommissionCalculation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission Calculation</title>
    <style>
        .line-through{
            text-decoration: line-through;
        }
        .select-custom{
          border-radius: 4px;
    border: 1px solid #bbb7b7;
    padding: 3px;
    margin: 8px;
    font-size: 16px;
    color: #777777;
        }
        .input-custom{
          
    width: 6%;
         border-radius: 4px;
    border: 1px solid #bbb7b7;
    padding: 3px;
    margin: 8px;
    font-size: 16px;
    color: #777777;
        }
        .f-1-5em{
            font-size:1.5em;
        }
        .btn-xx{
            padding: .2125rem .55rem;
        }
    </style>
    <script>
        function ShowDescription() {
            let ProcessFrequency = $('#ddl_ProcessFrequency').val();
            if (ProcessFrequency != "") {
                $('#div_ScheduleDescription').removeClass('d-none');

                let frequency = $("#ddl_ProcessFrequency option:selected").text();
                $('#span_ProcessFrequency').text(frequency);
            }
            else {
                $('#div_ScheduleDescription').addClass('d-none');
            }
        }

        $(document).ready(function () {
            ShowDescription();
            CheckAfter();
        });


        function CheckAfter() {
            let value = $('#ddl_AfterType').val();

            if (value == '1') {
                $('#aftercheck').removeClass('d-none');
            }
            else if (value == '2') {
                $('#aftercheck').addClass('d-none');
            }
        }
    </script>

    <script>

        function IsRunManual(element, btnid) {
            //  alert('work');
           // console.log($(element).is(':Checked'));

            if ($(element).is(':Checked')) {
               // alert('zdfsd');
                $(btnid).removeClass('d-none');
            }
            else {
                $(btnid).addClass('d-none');
              //  alert('sdf');
            }
        }
        function ExecuteCommissionCalculation(PeriodKey, CommissionKey, ScheduledRunPeriodKey) {
            var d = new Date();
            alertNotify('Calculation Started', 'Manual Calculation Start at  ' + d.toLocaleString(), 'bg-warning border-Warning');

            $.ajax({
                type: "POST",
                url: "../Web/CommissionCalculation.aspx/ExecuteCommissionCalculation",
                data: "{PeriodKey:'" + PeriodKey + "',CommissionKey:'" + CommissionKey + "',ScheduledRunPeriodKey:'" + ScheduledRunPeriodKey + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (data.d == "Executed") {
                        //  var d = new Date();
                       // alertNotify('Calculation Completed', 'Manual Calculation End at  ' + d.toLocaleString(), 'bg-success border-success');
                        
                        location.reload();
                        // alert("Calculation Ended");
                        //var d = new Date();
                        //$('#ProcessEndTime').text(d.toLocaleString());
                        //$('#CalcEnd').removeClass('d-none');
                        //$('#btn_Calc').html('Calculate');
                        //$('#btn_Calc').removeClass('disabled');
                        //$('.large,.small-shadow,.small, .medium-shadow,.medium, .large-shadow').css("animation", "none");
                        //$(".large,.small-shadow,.small, .medium-shadow,.medium, .large-shadow").css("animation-play-state", "paused");
                        //$('#SVG-Gear-Engine').addClass('d-none');
                    }
                    else {
                         // alert(data.d);
                        console.log(data.d);
                        alertNotify('Calculation Failed', 'Please contact admin ', 'bg-danger border-danger');
                        //$('#CalcError').text('Error : ' + data.d);
                        //$('#CalcError').removeClass('d-none');
                        //$(".large,.small-shadow,.small, .medium-shadow,.medium, .large-shadow").css("animation-play-state", "paused");

                        //$('#btn_Calc').html('Calculate');
                        //$('#btn_Calc').removeClass('disabled');
                    }
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function CalculateCommission(element, PeriodKey, CommissionKey, SpinnerId, ScheduledRunPeriodKey) {
           

            if (PeriodKey == "" && CommissionKey == "") {
                alertNotify('Error', 'Please select the period and Commission !', 'bg-danger border-danger');
                // return false;
            }
            else {
                //$('#SVG-Gear-Engine').removeClass('d-none');
                //$('#CalcError').addClass('d-none');
                //$('#CalcDetail').removeClass('d-none');
                //$('#CalcEnd').addClass('d-none');
                //$(".large,.small-shadow,.small, .medium-shadow,.medium, .large-shadow").css("animation-play-state", "running");
                //$(element).addClass('disabled');
                //$(element).html('<i class="icon-spinner2 spinner mr-1"></i>  Calculating');
                //var d = new Date();
                //$('#ProcessStartTime').text(d.toLocaleString());
                $(element).addClass('d-none');
                $(SpinnerId).removeClass('d-none');
                ExecuteCommissionCalculation(PeriodKey, CommissionKey, ScheduledRunPeriodKey);

            }

        }
        function Validate() {
            let ProcessFrequency = $('#ddl_ProcessFrequency').val();
            let DateType = $('#ddl_DateType').val();
            let AfterValue = $('#txt_AfterValue').val();
            let AfterType = $('#ddl_AfterType').val();

            if (ProcessFrequency == '' || DateType == '' || AfterValue == '' || AfterType=='') {
                // alert('false');
                alertNotify(' Schedular Error', 'Invalid Schedule configuratrion.', 'bg-warning border-warning');
                return false;
            }
            else {
                //   alert('true');
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
      <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12 col-md-12">
        
        <div class="card border-top-3 border-top-success-400">
            <div class="card-header header-elements-inline">
            <h5 class="card-title">Commission Scheduler
                
            </h5>
           
            </div>
    
            <div class="card-body">
                 <div class="row">
                     <div class="col-md-5">
                          <span class="text-muted">Commission Name </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_CommissionName" runat="server">Commission Calcualtion Frequence Monthly </h5>
                     </div>
                     <div class="col-md-3">
                         <span class="text-muted">Calcualtion Frequence </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_CalcFrqu" runat="server">Monthly </h5>
                     </div>
                     <div class="col-md-2">
                         <span class="text-muted">Start Date  </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_sDate" runat="server">1/1/2019</h5>
                     </div>
                     <div class="col-md-2">
                         <span class="text-muted">End Date  </span>
			               <h5 class="font-weight-semibold mb-1" id="lbl_edate" runat="server">12/31/2019</h5>
                     </div>
                    

                 </div>
                 <hr />
                      <div class="row">
                          <div class="col-md-12">
                              <h5 class="text-muted">Schedule Process Period</h5>
                          </div>
                         
                        <div class="col-md-2">
                            <label>Process Frequency</label>
                            <asp:DropDownList onchange="ShowDescription();" required ID="ddl_ProcessFrequency" runat="server" CssClass="form-control-select2">
                               
                                </asp:DropDownList>
                        </div>
                      
                     <div class="col-md-9 d-none my-auto" id="div_ScheduleDescription">
                         
			               <h5 class="text-muted pl-3 mt-3">

                               Schedule to run plan every 

                               <span class="font-weight-bold" id="span_ProcessFrequency">Week</span>
                               
                               based on process frequency 
                               
                               <asp:DropDownList  ID="ddl_DateType" runat="server" CssClass="select-custom">
                                <asp:ListItem Text="End" Value="1"></asp:ListItem>
                                <asp:ListItem Selected="True" Text="Start" Value="2"></asp:ListItem>
                                </asp:DropDownList>

                               Date after 
                               <input type="number" min="0" max="24" value="0" class="input-custom ignore" id="txt_AfterValue" runat="server"/>
                              
                               <asp:DropDownList  onchange="CheckAfter();" ID="ddl_AfterType" runat="server" CssClass="select-custom">
                               
                                <asp:ListItem Selected="True" Text="day" Value="1"></asp:ListItem>
                                <asp:ListItem Text="hour" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                               <span id="aftercheck">
                               <input  type="number" min="0" max="24" value="0" class="input-custom  ignore" id="txt_AfterHour" runat="server" />
                               
                               hours
                                   </span>
			               </h5>
                     </div>
                           <div class="col-md-1 my-auto">
                               <asp:Button class="btn btn-sm btn-success" id="btn_ScheduleSave" runat="server" OnClick="btn_ScheduleSave_ServerClick" Text="Schedule" OnClientClick="return Validate();" />
                              <%-- <button class="btn btn-sm btn-success" type="button" id="btn_ScheduleSave" runat="server" onserverclick="btn_ScheduleSave_ServerClick">Schedule</button>--%>
                          </div>
                    
                 </div>
               
            </div>
        </div>
    </div>
          
    </div>
    <div class="row">
        <div class="col-md-12">
             <div class="card border-top-2 border-top-success-400">
            <div class="card-header header-elements-inline">
            <h5 class="card-title">Scheduled Periods
                
            </h5>
           
            </div>
    
            <div class="card-body">
                
               <div class="table-responsive">
                   <table class="table datatable-basic">
                       <thead>
                           <tr>
                               <th>Period </th>
                               <th>Plan Frequency </th>
                               <th>Process Frequency </th>
                               <th>Scheculed On</th>
                               <th>Status</th>
                               <th>Last Processed On</th>
                               <th>Manual Run</th>
                           </tr>
                       </thead>
                       <tbody>
                           <asp:PlaceHolder ID="ph_ProcessPeriodList" runat="server"></asp:PlaceHolder>
                       </tbody>
                   </table>
               </div>

            </div>
        </div>
        </div>
    </div>
    </div>
    <asp:HiddenField ID="hdn_PlanStartDate" runat="server" />
    <asp:HiddenField ID="hdn_PlanEndDate"  runat="server" />

    <asp:HiddenField ID="MasterCalendarKey" runat="server" />
</asp:Content>

