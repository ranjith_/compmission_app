﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_FunctionSplit : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        string RuleType = Com.GetQueryStringValue("RuleType");
        string CommissionKey = Com.GetQueryStringValue("CommissionKey");
        if (!IsPostBack)
        {
           
           // string CommissionKey = Com.GetQueryStringValue("CommissionKey");
            hdn_CommissionKey.Value = CommissionKey;
            hdn_RuleType.Value = RuleType;
          //  string RuleType= Com.GetQueryStringValue("RuleType");


        }
        if (Com.GetQueryStringValue("isEdit") == "1")
        {
            loadSplitConditionsToEdit(CommissionKey, RuleType);
            btn_SaveSplit.Attributes.Add("onclick", "UpdateSplitCondition('"+Com.GetQueryStringValue("SplitKey") +"')");
            btn_SaveSplit.InnerText = "Update";

            loadSplitName(CommissionKey, RuleType);

        }
        else
        {
            loadNewCondition("1");
            string SplitKey = Guid.NewGuid().ToString();
            hdn_SplitKey.Value = SplitKey;
        }

       
    }
    private void loadSplitName(string CommissionKey, string RuleType)
    {
        string sql = "select * from Split where CommissionKey=@CommissionKey and RuleType=@RuleType ";
        SqlCommand cmd = new SqlCommand(sql);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        cmd.Parameters.AddWithValue("@RuleType", RuleType);
        DataTable dt = this.DA.GetDataTable(cmd);
        foreach(DataRow dr in dt.Rows)
        {
            txt_SplitName.Value = dr["SplitName"].ToString();
        }
    }
    private void loadSplitConditionsToEdit(string CommissionKey, string RuleType)
    {
      
        string str_Sql = "select *,ROW_NUMBER() OVER(ORDER BY b.SplitKey ASC) AS ID from SplitCondition a join Split b on a.SplitKey=b.SplitKey where b.CommissionKey=@CommissionKey and RuleType=@RuleType order by a.CreatedOn";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        cmd.Parameters.AddWithValue("@RuleType", RuleType);
        DataTable dt = this.DA.GetDataTable(cmd);
        string Conditions = this.ConditionList(dt);

        ph_SplitCondition.Controls.Add(new LiteralControl(this.ConditionList(dt)));
    }
    public string ConditionList(DataTable dt)
    {
        string ConditionList = "";
        foreach (DataRow dr in dt.Rows)
        {
            string ConditionTemplate = this.PHT.ReadFileToString("DivSplitRuleEdit.txt");
            string Row = "";
            string selected = "selected", unselected = "";

            Row = ConditionTemplate.Replace("%%ID%%", dr["ID"].ToString());
            if (dr["AttributeType"].ToString() == "1")
            {
                Row = Row.Replace("%%optiona%%", selected);
                Row = Row.Replace("%%optionb%%", unselected);
            }
            if (dr["AttributeType"].ToString() == "2")
            {
                Row = Row.Replace("%%optiona%%", unselected);
                Row = Row.Replace("%%optionb%%", selected);
            }
            if(dt.Rows.Count.ToString() == dr["ID"].ToString())
            {
                Row = Row.Replace("%%add%%","");
                Row = Row.Replace("%%remove%%", "");
            }
            else
            {
                Row = Row.Replace("%%remove%%", "");
                Row = Row.Replace("%%add%%", "d-none");

            }
            Row = Row.Replace("%%Percentage%%", dr["Percentage"].ToString());
            Row = Row.Replace("%%VariableName%%", dr["VariableName"].ToString());
           
            string AttributeOption = this.getHtmlOptions("select * from v_CommissionTransactionColumns", dr["Attribute"].ToString());
            string TransactionFieldOption = this.getHtmlOptions("select * from v_CommissionTransactionColumns", dr["TransactionField"].ToString());
            Row = Row.Replace("%%AttributeOption%%", AttributeOption);
            Row = Row.Replace("%%TransactionFieldOption%%", TransactionFieldOption);
           
            ConditionList += Row;
        }
        return ConditionList;
    }

    private void loadNewCondition(string ID)
    {
        
        ph_SplitCondition.Controls.Add(new LiteralControl(SplitConditionTr(ID)));

    }

    public string SplitConditionTr(string ID)
    {
        PhTemplate PHT1 = new PhTemplate();
        string str_ConditionTemplate = PHT1.ReadFileToString("DivSplitRule.txt"), str_Replace = "";

        string TransactionColumns = this.getHtmlOptions("select * from v_CommissionTransactionColumns", "");
        str_Replace = str_ConditionTemplate.Replace("%%TransactionOption%%", TransactionColumns);
        str_Replace = str_Replace.Replace("%%ID%%", ID);
        return str_Replace;
    }
    public string getHtmlOptions(string qry, string value)
    {
        DataAccess DA = new DataAccess();
        // string str_Sql = "select a.AgreementName as Text, a.AgreementKey as Value from Agreement a";
        SqlCommand sqlcmd = new SqlCommand(qry);

        DataTable dt = DA.GetDataTable(sqlcmd);
        string option = "";
        foreach (DataRow dr in dt.Rows)
        {
            string selected = "";
            if (dr["Value"].ToString() == value)
            {
                selected = "selected='selected'";
            }
            option += "<option value='" + dr["Value"].ToString() + "' " + selected + ">" + dr["Text"].ToString() + "</option>";
        }
        return option;
    }
    [WebMethod]
    public static string SplitConditionNew(string id)
    {
        DataAccess DAA = new DataAccess();
        Web_FunctionSplit FS = new Web_FunctionSplit();
        string ConditionTr = "";
        ConditionTr = FS.SplitConditionTr(id);
        return ConditionTr;
    }
    [WebMethod]
    public static string InsertSplit(string SplitKey, string SplitName, string CommissionKey, string RuleType)
    {
        try
        {
            //  Split (SplitKey, SplitName, RuleKey, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();
        

            string str_Sql = "insert into Split (SplitKey, SplitName, CommissionKey,RuleType, CreatedOn, CreatedBy)"
                                + "select @SplitKey, @SplitName, @CommissionKey,@RuleType, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@SplitKey", SplitKey);
            cmd.Parameters.AddWithValue("@SplitName", SplitName);
            cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
            cmd.Parameters.AddWithValue("@RuleType", RuleType);

            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return "Split Inserted";

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }

       
    }
    [WebMethod]
    public static string InsertSplitCondition( string SplitKey, string Attribute, string AttributeType, string Variable, string Percentage, string TransactionField)
    {
        try
        {
            // SplitCondition (SplitConditionKey, SplitKey, Attribute, AttributeType, VariableName, Percentage, TransactionField, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy, RuleKey)
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();

            string SplitConditionKey = Guid.NewGuid().ToString();

            string str_Sql = "insert into SplitCondition (SplitConditionKey, SplitKey, Attribute, AttributeType, VariableName, Percentage, TransactionField, CreatedOn, CreatedBy)"
                                + "select @SplitConditionKey, @SplitKey, @Attribute, @AttributeType, @VariableName, @Percentage, @TransactionField, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@SplitConditionKey", SplitConditionKey);
            cmd.Parameters.AddWithValue("@SplitKey", SplitKey);
            cmd.Parameters.AddWithValue("@Attribute", Attribute);
            cmd.Parameters.AddWithValue("@AttributeType", AttributeType);
            cmd.Parameters.AddWithValue("@VariableName", Variable);
            cmd.Parameters.AddWithValue("@Percentage", Percentage);
            cmd.Parameters.AddWithValue("@TransactionField", TransactionField);

            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return "Split Condition Inserted";

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    [WebMethod]
    public static string DeleteSplitCondition(string SplitKey)
    {
        try
        {
            //  Split (SplitKey, SplitName, RuleKey, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();


            string str_Sql = "Delete from SplitCondition where SplitKey=@SplitKey";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@SplitKey", SplitKey);
            

            DA.ExecuteNonQuery(cmd);

            return "Condition Deleted";

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    [WebMethod]
    public static string UpdateSplit(string SplitKey, string SplitName)
    {
        try
        {
            //  Split (SplitKey, SplitName, RuleKey, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();


            string str_Sql = "Update Split set  SplitName=@SplitName,  ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where SplitKey=@SplitKey ";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@SplitKey", SplitKey);
            cmd.Parameters.AddWithValue("@SplitName", SplitName);
            cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return "Split Updatted";

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }

    }
}