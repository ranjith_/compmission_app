﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="Summarization.aspx.cs" Inherits="Web_Summarization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Summarization</title>
    	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/demo_pages/form_select2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                 <div class="card  border-top-2 border-top-primary-600">
                    <div class="card-header  header-elements-inline">
                         <div class="card-title"> <span class="text-muted">  </span>
                             <h4 class="font-weight-semibold mb-1"><i class="icon-cog52"></i>  Summarization</h4>
                         </div>
							
                        <div class="header-elements">
                            <a class="list-icons-item mr-3" data-action="collapse"></a>
                            <a href="CommissionInitDetails.aspx" id="hpLink_EditCommission" runat="server" class="btn btn-primary btn-sm">Edit</a>

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Name</label>
                                <input required type="text" class="form-control" />
                            </div>
                            <div class="form-group col-md-3">
                                <label>Effective Date</label>
                                <input type="date" class="form-control" />
                            </div>
                            <div class="form-group col-md-3">
                                <label>Recipient</label>
                                 <select class="form-control select-search">
                                     <option value="">select recipient</option>
                                     <option value="1">REP 2</option>
                                     <option value="2">REP 3</option>
                                     <option value="3">REP 4</option>
                                     <option value="4">REP 5</option>
                                 </select>
                            </div>
                             <div class="col-md-3 my-auto form-group  pl-5" id="IsActive">
                               <div class="form-check ml-md-5">
				                        <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input-styled" id="chb_IsActive" runat="server"  />
                                            Is Active
                                            </label>
		                        </div>
                             </div>
                            <div class="form-group col-md-6">
                                <label>Description</label>
                                <input type="text" class="form-control" />
                            </div>
                           
                        </div>
                     
                    </div>
                    	
                </div>
            </div>
           
        </div>
         
     </div>
</asp:Content>

