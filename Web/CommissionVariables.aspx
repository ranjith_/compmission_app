﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionVariables.aspx.cs" Inherits="Web_CommissionVariables" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title> variables</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
      <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-8 col-md-8">
                                    <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"> Variables
                                    
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                        <a href="CommissionVariables.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                   
                           
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Variable Name</th>
                                        <th>Data Type</th>
                                        <th>Default Value</th>
                                        <th> Created On</th>
                                        <th> Created By</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_CommissionVariables" runat="server"></asp:PlaceHolder>
                                        
                                </tbody>
                            </table>
                    </div>
                                 </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                 <!--Input Form-->
                                 <div id="div_InputForm" runat="server"  class="col-lg-4 col-md-4">
                                        <!-- Basic layout-->
                            <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Add  Variables</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                         <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-12">
                                                <label>Variable Name<span class="text-danger">*</span></label>
                                                <input id="txt_VariableName" runat="server" type="text" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Data Type<span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddl_DataType" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Default Value</label>
                                                <input id="txt_DefaultValue" runat="server" type="text" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description</label>
                                                <textarea id="txt_Description" runat="server" class="form-control" rows="2"></textarea>
                                                
                                            </div>
                                            
                            </div>
                                        </div>
                                    </div>
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->

                                 <!--View Data-->
                                 <div id="div_AduitLog" runat="server"  class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card" id="card-audit-log">
                            
                                  <asp:PlaceHolder ID="ph_AuditLogDetails" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
                            </div>
                        </div>
</asp:Content>

