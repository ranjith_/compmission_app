﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Territory : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    public string str_view;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        loadTerritoryList();

        if (Request.QueryString["territory_key"] != null)
        {
            viewTerritoryDetails(Request.QueryString["territory_key"]);
            div_TerritoryDetails.Visible = true;
            div_TerritoryDetails.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Visible = false;
        }
        if (!IsPostBack)
        {
            string str_Select = "select TerritoryKey as Value, TerritoryName as Text from Territory";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.Com.LoadDropDown(ddl_ReportingTerritory, ds.Tables[0], "Text", "Value", true, "--Select Parent Territory--");

        }
        if (!IsPostBack)
        {
            if (Request.QueryString["territory_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadTerritoryDetailToEdit(Request.QueryString["territory_key"]);
                // div_TerritoryDetails.Visible = false;
                div_TerritoryDetails.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
             //   txt_TerritoryId.Attributes["Readonly"] = "true";
            }
        }

        if (!IsPostBack)
        {
            HierarchicalTreeView();
            string ViewUl= "<ul id='ul-data'  style='display: none; '>"+str_view+"</ul>";
            ph_TreeView.Controls.Add(new LiteralControl(ViewUl));
        
        }
        

    }

    private void loadTerritoryDetailToEdit(string TerritoryKey)
    {

        try
        {
            string str_Sql_view = "select  * from Territory where  TerritoryKey=@TerritoryKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@TerritoryKey", TerritoryKey);
            DataTable dt = this.DA.GetDataTable(sqlcmd);
            foreach (DataRow dr in dt.Rows)
            {

                txt_TerritoryId.Value = dr["TerritoryId"].ToString();
                hdn_TerritoryId.Value = dr["TerritoryId"].ToString();
                txt_TerritoryName.Value = dr["TerritoryName"].ToString();
                ddl_ReportingTerritory.SelectedValue = dr["ReportToTerritory"].ToString();

            }
        }
        catch(Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }
    }

    private void viewTerritoryDetails(string TerritoryKey)
    {
        try
        {
            string str_Sql_view = "select a.TerritoryId,a.TerritoryKey,a.TerritoryName, b.Text as ReportingTerritory from Territory a left join v_Territory b on a.ReportToTerritory=b.Value where  a.TerritoryKey=@TerritoryKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@TerritoryKey", TerritoryKey);
            DataSet ds2 = this.DA.GetDataSet(sqlcmd);
            this.PHT.LoadGridItem(ds2, ph_TerritoryDetails, "DivTerritoryView.txt", "");
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

        
    }
    [WebMethod]
    public static string viewTerritory(string TerritoryKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select a.TerritoryId,a.TerritoryKey,a.TerritoryName, b.Text as ReportingTerritory from Territory a left join v_Territory b on a.ReportToTerritory=b.Value where  a.TerritoryKey=@TerritoryKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@TerritoryKey", TerritoryKey);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivTerritoryView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    private void loadTerritoryList()
    {
        try
        {
            string str_Sql = "select *, (select TerritoryName from Territory t2 where t1.ReportToTerritory=t2.TerritoryKey) as ReportToTerritoryName ,convert(varchar, CreatedOn, 103)  as Created_On from Territory t1 order by CreatedOn desc";
            SqlCommand cmd = new SqlCommand(str_Sql);
            DataSet ds = this.DA.GetDataSet(cmd);
            this.PHT.LoadGridItem(ds, ph_TerritoryList, "TerritoryListTr.txt", "");
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

       
    }
    [WebMethod]
    public static string deleteTerritory(string TerritoryKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Territory where TerritoryKey=@TerritoryKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@TerritoryKey", TerritoryKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string ValidateTerritoryId(string TerritoryId)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "select *  from Territory where TerritoryId=@TerritoryId ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@TerritoryId", TerritoryId);
            DataTable dt = DA.GetDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                return "0";
            }
            else
            {
                return "1";
            }
          
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        try
        {
            string str_Key = Guid.NewGuid().ToString();

            string str_Sql = "insert into Territory (TerritoryKey, TerritoryId, TerritoryName, ReportToTerritory, CreatedOn, CreatedBy)"
                                + "select @TerritoryKey, @TerritoryId, @TerritoryName, @ReportToTerritory, @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@TerritoryKey", str_Key);
            cmd.Parameters.AddWithValue("@TerritoryId", txt_TerritoryId.Value);
            cmd.Parameters.AddWithValue("@TerritoryName", txt_TerritoryName.Value);
            if (ddl_ReportingTerritory.SelectedValue == "")
                cmd.Parameters.AddWithValue("@ReportToTerritory", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@ReportToTerritory", ddl_ReportingTerritory.SelectedValue);
            // cmd.Parameters.AddWithValue("@ReportToTerritory", ddl_ReportingTerritory.SelectedValue);
            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

            this.DA.ExecuteNonQuery(cmd);

            Response.Redirect(Request.RawUrl,false);
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

        //(TerritoryKey, TerritoryId, TerritoryName, ReportToTerritory, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

       
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {

        try
        {
            string TerritoryKey = Request.QueryString["territory_key"];

            string userKey = "15702C1E-1447-4C08-9773-7BD324256180";
            //  RateConversionKey, Name, Description, FromCurrency, ToCurrency, EffectiveDate, ToDate, ConversionRate, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy

            string str_Sql = "UPDATE Territory set TerritoryId=@TerritoryId, TerritoryName=@TerritoryName, ReportToTerritory=@ReportToTerritory," +
                " ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where TerritoryKey=@TerritoryKey ";

            SqlCommand cmd = new SqlCommand(str_Sql);

            cmd.Parameters.AddWithValue("@TerritoryId", txt_TerritoryId.Value);
            cmd.Parameters.AddWithValue("@TerritoryName", txt_TerritoryName.Value);
            if (ddl_ReportingTerritory.SelectedValue == "")
                cmd.Parameters.AddWithValue("@ReportToTerritory", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@ReportToTerritory", ddl_ReportingTerritory.SelectedValue);
            cmd.Parameters.AddWithValue("@ModifiedBy", userKey);

            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            cmd.Parameters.AddWithValue("@TerritoryKey", TerritoryKey);
            this.DA.ExecuteNonQuery(cmd);

            Response.Redirect("../Web/Territory.aspx?territory_key=" + TerritoryKey + "", false);
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

        
    }
   

        private void HierarchicalTreeView()
        {

        try
        {
            string view = "";
            DataSet Ds = new DataSet();

            string sql_qry = "select TerritoryKey,TerritoryId,TerritoryName,(select TerritoryId from Territory t2 where t1.ReportToTerritory=t2.TerritoryKey ) as ReportToTerritoryID from Territory t1";

            SqlCommand cmd = new SqlCommand(sql_qry);
            DataTable dt = this.DA.GetDataTable(cmd);



            foreach (DataRow Dr in dt.Select("ReportToTerritoryID is NULL"))
            {
                if (Dr["ReportToTerritoryID"] == DBNull.Value)
                {
                    string parentNodeId = Dr["TerritoryId"].ToString();
                    string ParentNodeTitle = Dr["TerritoryName"].ToString();
                    string ParentNodeKey = Dr["TerritoryKey"].ToString();

                    str_view += "<li data-id='" + parentNodeId + "'>" + ParentNodeTitle;
                    str_view += loadChildNode(parentNodeId, ParentNodeTitle, dt);

                    str_view += "</li>";
                }

            }
        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }

        
        }

    public string loadChildNode(string parentNodeId, string parentNodeTitle, DataTable dt)
    {
        
        string childnode = "";
        childnode += "<ul>";
        DataRow[] dr_cchild = dt.Select("ReportToTerritoryID ='" + parentNodeId + "'");

        foreach (DataRow dr_child in dt.Select("ReportToTerritoryID ='" + parentNodeId + "'"))
        {
           
            if (dr_child["ReportToTerritoryID"] != DBNull.Value)
            {
                 string childNodeId = dr_child["TerritoryId"].ToString();
                string childNodeTitle = dr_child["TerritoryName"].ToString();
                string childNodeKey = dr_child["TerritoryKey"].ToString();

               
                DataRow[] dr_grant_child = dt.Select("ReportToTerritoryID ='" + childNodeId + "'");
                if (dr_grant_child.Length > 0)
                {
                     childnode += "<li data-id='" + childNodeId + "'>" + childNodeTitle ;
                    childnode += this.loadChildNode(childNodeId, childNodeTitle, dt);
                    childnode += "</li>";
                }
                else
                {
                    childnode += "<li data-id='" + childNodeId + "'>" + childNodeTitle + "</li>";
                }
            }

        }
        childnode += "</ul>";
        return childnode;
    }
}