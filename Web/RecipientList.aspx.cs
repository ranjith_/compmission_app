﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_RecipientList : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();

        loadRecipientList();
    }
    private void loadRecipientList()
    {
        string str_Sql = "select *, concat (a.FirstName, ' ' ,a.LastName) as RecipientName  , convert(varchar(10),a.JoinDAte, 103) as jDate , b.Text as Territory, c.Text as ReportingTo, d.text as RepType from Recipients a left join v_Territory b on a.RecipientTerritory=b.Value left join  v_RecipientManager c on a.ReportingManager=c.Value left join v_RecipientCategory d on a.RecipientType=d.Value";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_RecipientTableRows, "RecipientListTr.txt", "");
    }
    [WebMethod]
    public static string deleteRecipient(string RecipKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Recipients where RecipientKey=@RecipientKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@RecipientKey", RecipKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
}