﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
public partial class Web_TransactionEdit : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        if (Request.QueryString["New"] == "1")
        {
            CheckForCustomTransaction("");

        }
        else
        {
            if (panel_CustomTransaction.Controls.Count == 0)
            {
                CheckForCustomTransaction(Request.QueryString["TransactionKey"]);
            }

        }

        if (!IsPostBack)
        {

            loadProductList();
            loadCustomerList();
            loadRecipientList();
            loadTransactionType();
            if (Request.QueryString["New"] == "1")
            {
               // CheckForCustomTransaction("");

            }
            if (Request.QueryString["TransactionKey"] != null && Request.QueryString["IsEdit"] == "1")
            {
                loadTransDetailToEdit(Request.QueryString["TransactionKey"]);
               // CheckForCustomTransaction(Request.QueryString["TransactionKey"]);
                btn_Update.Visible = true;
                btn_Save.Visible = false;
            }
        }

    }

    private void loadTransactionType()
    {
        string str_Select = "select * from v_TransactionType ";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(dd_TransactionType, ds.Tables[0], "Text", "Value", true, "--Select Type --");
    }

    private void loadRecipientList()
    {
        string str_Select = "select * from v_RecipientList ";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_Recipient1, ds.Tables[0], "Text", "Value", true, "--Select Rep --");
        this.Com.LoadDropDown(ddl_Recipient2, ds.Tables[0], "Text", "Value", true, "--Select Rep--");
        this.Com.LoadDropDown(ddl_Recipient3, ds.Tables[0], "Text", "Value", true, "--Select Rep--");
    }

    private void loadCustomerList()
    {
        string str_Select = "select * from v_CustomerList";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_Customer, ds.Tables[0], "Text", "Value", true, "--Select Customer --");
    }

    private void loadProductList()
    {
        string str_Select = "select * from v_ProductList ";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_Product, ds.Tables[0], "Text", "Value", true, "--Select Product --");
    }

    private void loadTransDetailToEdit(string trans_key)
    {

        string str_Sql_view = "select TransactionKey,TransactionLine,format(EffectiveDate, 'yyyy-MM-dd') as effectivedate,format(TransactionDate, 'yyyy-MM-dd') as transactiondate,TransactionType,TransactionLineType,Customer,SalesRep1,SalesRep2,SalesRep3,Product,SalesAmount,"
                + "TransactionLineLevel,SoNumber,PaymentType,BillNumber,TransactionId,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn from transactions where TransactionKey =@TransactionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@TransactionKey", trans_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            txt_TransactionId.Value = dr["transactionid"].ToString();
            txt_TransactionLine.Value = dr["transactionline"].ToString();
            txt_EffectiveDate.Value = dr["effectivedate"].ToString();
            txt_TransactionDate.Value = dr["transactionDate"].ToString();
            dd_TransactionType.SelectedValue = dr["transactiontype"].ToString();
            dd_TransactionLineType.Value = dr["transactionlinetype"].ToString();
            ddl_Customer.SelectedValue = dr["customer"].ToString();
           ddl_Recipient1.SelectedValue = dr["salesrep1"].ToString();
            ddl_Recipient2.SelectedValue = dr["salesrep2"].ToString();

            ddl_Recipient3.SelectedValue = dr["salesrep3"].ToString();
            ddl_Product.SelectedValue= dr["product"].ToString();
            txt_SalesAmount.Value = dr["salesamount"].ToString();
            dd_TransactionLineLevel.Value = dr["transactionlinelevel"].ToString();
            txt_SoNumber.Value = dr["sonumber"].ToString();
            dd_PaymentType.Value = dr["paymenttype"].ToString();
            txt_BillNumber.Value = dr["billnumber"].ToString();

        }

    }
    private void CheckForCustomTransaction(string TransactionKey)
    {
        string str_Sql = "select a.*, b.Name from TableCustomConfig a join IConfigDetails b on a.DataType = b.ConfigDetailKey where a.PrimaryTableName='Transaction' order by Seq;" +
            "select * from TransactionCustom where TransactionKey=@TransactionKey ";
        SqlCommand cmd = new SqlCommand(str_Sql);
        if (TransactionKey == "")
        {
            cmd.Parameters.AddWithValue("@TransactionKey", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@TransactionKey", TransactionKey);
        }
       
        DataSet ds = this.DA.GetDataSet(cmd);

        if (ds.Tables[0].Rows.Count > 0)
        {
            LoadCustomTransaction(ds);
        }
    }

    private void LoadCustomTransaction(DataSet ds)
    {
      
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            string Value = "";
            foreach(DataRow dr1 in ds.Tables[1].Rows)
            {
                foreach (DataColumn cc in ds.Tables[1].Columns)
                {
                    if (dr["ColumnName"].ToString() == cc.ColumnName.ToString())
                    {
                        Value = dr1[cc.ColumnName.ToString()].ToString();
                    }
            }
            }
           
            
            if (dr["FieldType"].ToString() == "1")
            {
                AddTextBox(dr["ColumnName"].ToString(), dr["LableName"].ToString(),Value,dr["IsRequired"].ToString());
            }
            else if (dr["FieldType"].ToString() == "2")
            {
                AddDropDown(dr["ColumnName"].ToString(), dr["DdlKey"].ToString(), dr["LableName"].ToString(), Value, dr["IsRequired"].ToString());
            }

        }
    }

    private void AddDropDown(string ColumnName, string DdlKey, string LabelName, string value, string IsRequired)
    {
        string FormGroupDivStart = "<div class='form-group col-md-3'>";
        string FormGroupDivEnd = "</div>";
        string RequiredField = "<span class='text-danger'>*</span>";
        Label lbl = new Label();
        lbl.Text = LabelName;
        DropDownList ddl = new DropDownList();
        ddl.ID = "ddl_" + ColumnName;
        ddl.CssClass = "form-control ";
        loadDropDown(ddl, DdlKey);
        panel_CustomTransaction.Controls.Add(new LiteralControl(FormGroupDivStart));
        panel_CustomTransaction.Controls.Add(lbl);
        if (IsRequired == "True")
        {
            panel_CustomTransaction.Controls.Add(new LiteralControl(RequiredField));
        }
        panel_CustomTransaction.Controls.Add(ddl);
        ddl.SelectedValue = value;
        panel_CustomTransaction.Controls.Add(new LiteralControl(FormGroupDivEnd));
    }

    private void loadDropDown(DropDownList ddl, string ddlKey)
    {
        string str_Select = "select Name as Text,configDetailKey as Value  from IconfigDetails where ConfigKey=@ConfigKey order by seq ";
        SqlCommand cmm = new SqlCommand(str_Select);
        cmm.Parameters.AddWithValue("@ConfigKey", ddlKey);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl, ds.Tables[0], "Text", "Value", true, "--Select --");
    }

    private void AddTextBox(string ColumnName, string LabelName, string Value, string IsRequired)
    {
        string FormGroupDivStart = "<div class='form-group col-md-3'>";
        string FormGroupDivEnd = "</div>";
        string RequiredField = "<span class='text-danger'>*</span>";
        Label lbl = new Label();
        lbl.Text = LabelName;
        TextBox tb = new TextBox();
        tb.ID = "txt_" + ColumnName;
        tb.Text = Value;
        tb.CssClass = "form-control ";
        panel_CustomTransaction.Controls.Add(new LiteralControl(FormGroupDivStart));
        panel_CustomTransaction.Controls.Add(lbl);
        if (IsRequired == "True")
        {
            panel_CustomTransaction.Controls.Add(new LiteralControl(RequiredField));
        }
        panel_CustomTransaction.Controls.Add(tb);
        panel_CustomTransaction.Controls.Add(new LiteralControl(FormGroupDivEnd));
    }
    protected void btn_Save_Click(object sender, EventArgs e)
    {
        //string dd = txt_TransactionDate.Value.ToString();
        string str_Key = Guid.NewGuid().ToString();
        string str_txn_Key = Guid.NewGuid().ToString();
        //string trans_Key = Guid.NewGuid().ToString();
        string str_Sql = "insert into transactions (transactionkey,transactionid, transactionline,effectivedate,transactiondate," +
                            "transactiontype,transactionlinetype,customer,salesrep1,salesrep2,salesrep3,product,salesamount,transactionlinelevel," +
                            "sonumber,paymenttype,billnumber,CreatedBy,CreatedOn)"
                            + "select @TransactionKey,@transaction_id,@transaction_line,@effective_date,@transaction_date,@transaction_type,@transaction_line_type," +
                            "@customer,@sales_rep1,@sales_rep2,@sales_rep3,@product,@sales_amount,@transaction_line_level,@so_number,@payment_type,@bill_number,@CreatedBy,@CreatedOn;" +
                            "insert into TransactionCustom (TxnCustomKey,TransactionKey,CreatedBy,CreatedOn)" +
                            "select @TxnCustomKey,@TransactionKey,@CreatedBy,@CreatedOn";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@TransactionKey", str_Key);

        cmd.Parameters.AddWithValue("@TxnCustomKey", str_txn_Key);
        cmd.Parameters.AddWithValue("@transaction_id", txt_TransactionId.Value);
        cmd.Parameters.AddWithValue("@transaction_line", txt_TransactionLine.Value);
      
        cmd.Parameters.AddWithValue("@transaction_date", txt_TransactionDate.Value);
        if (txt_EffectiveDate.Value == "")
        {
            cmd.Parameters.AddWithValue("@effective_date", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@effective_date", txt_EffectiveDate.Value);
        }

        cmd.Parameters.AddWithValue("@transaction_type", dd_TransactionType.SelectedValue);
        cmd.Parameters.AddWithValue("@transaction_line_type", dd_TransactionLineType.Value);
       
        if (ddl_Recipient1.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@sales_rep1", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@sales_rep1", ddl_Recipient1.SelectedValue);
        }
        if (ddl_Recipient2.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@sales_rep2", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@sales_rep2", ddl_Recipient2.SelectedValue);
        }
        if (ddl_Recipient3.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@sales_rep3", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@sales_rep3", ddl_Recipient3.SelectedValue);
        }

        if (ddl_Product.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@product", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@product", ddl_Product.SelectedValue);
        }

        if (ddl_Customer.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@customer", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@customer", ddl_Customer.SelectedValue);
        }

        cmd.Parameters.AddWithValue("@sales_amount", txt_SalesAmount.Value);
        cmd.Parameters.AddWithValue("@transaction_line_level", dd_TransactionLineLevel.Value);
        cmd.Parameters.AddWithValue("@so_number", txt_SoNumber.Value);
        cmd.Parameters.AddWithValue("@payment_type", dd_PaymentType.Value);
        cmd.Parameters.AddWithValue("@bill_number", txt_BillNumber.Value);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        InsertCustomTxn(str_Key);
        Response.Redirect("../Web/Transaction.aspx");
    }
    private void InsertCustomTxn(string TransactionKey)
    {
        foreach (Control cc in panel_CustomTransaction.Controls)

        {
            if (cc is TextBox)
            {
                string ColumnName = cc.ID.ToString();
                TextBox txt = (TextBox)panel_CustomTransaction.FindControl(ColumnName);
                string Column = ColumnName.Replace("txt_", "");
                UpdateCustomTransactionFields(TransactionKey, Column, txt.Text.ToString());
            }
            else if (cc is DropDownList)
            {
                string ColumnName = cc.ID.ToString();
                DropDownList ddl = (DropDownList)panel_CustomTransaction.FindControl(ColumnName);
                string value = ddl.SelectedItem.Value.ToString();
                string Column = ColumnName.Replace("ddl_", "");
                UpdateCustomTransactionFields(TransactionKey, Column, value);
            }

        }
    }

    private void UpdateCustomTransactionFields(string TransactionKey, string ColumnName, string Value)
    {
        string str_sql = "update TransactionCustom set " + ColumnName + " =@" + ColumnName + " where TransactionKey=@TransactionKey";
        SqlCommand cmd = new SqlCommand(str_sql);
        if (Value == "")
        {
            cmd.Parameters.AddWithValue("@" + ColumnName, DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@" + ColumnName, Value);
        }
       
        cmd.Parameters.AddWithValue("@TransactionKey", TransactionKey);
        this.DA.ExecuteNonQuery(cmd);
    }
    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string trans_key = Request.QueryString["TransactionKey"];
        //  string str_Sql = "UPDATE transactions set transaction_id=@transaction_id, transaction_line=@transaction_line,  where transaction_key=@transaction_key  ";

        string str_Sql = "UPDATE transactions set transactionid=@transaction_id, transactionline=@transaction_line, effectivedate=@effective_date, transactiondate=@transaction_date, " +
           "transactiontype=@transaction_type, transactionlinetype=@transaction_line_type, customer=@customer, salesrep1=@sales_rep1, salesrep2=@sales_rep2,salesrep3=@sales_rep3, product=@product, salesamount=@sales_amount, transactionlinelevel=@transaction_line_level, " +
           " sonumber=@so_number, paymenttype=@payment_type, billnumber=@bill_number, ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where TransactionKey=@TransactionKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);

        cmd.Parameters.AddWithValue("@transaction_id", txt_TransactionId.Value);
        cmd.Parameters.AddWithValue("@transaction_line", txt_TransactionLine.Value);

      //  cmd.Parameters.AddWithValue("@effective_date", txt_EffectiveDate.Value.ToString());
        cmd.Parameters.AddWithValue("@transaction_date", txt_TransactionDate.Value.ToString());
        cmd.Parameters.AddWithValue("@transaction_type", dd_TransactionType.SelectedValue);
        cmd.Parameters.AddWithValue("@transaction_line_type", dd_TransactionLineType.Value);

        if (txt_EffectiveDate.Value == "")
        {
            cmd.Parameters.AddWithValue("@effective_date", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@effective_date", txt_EffectiveDate.Value);
        }

        if (ddl_Recipient1.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@sales_rep1", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@sales_rep1", ddl_Recipient1.SelectedValue);
        }
        if (ddl_Recipient2.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@sales_rep2", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@sales_rep2", ddl_Recipient2.SelectedValue);
        }
        if (ddl_Recipient3.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@sales_rep3", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@sales_rep3", ddl_Recipient3.SelectedValue);
        }

        if (ddl_Product.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@product", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@product", ddl_Product.SelectedValue);
        }

        if (ddl_Customer.SelectedValue == "")
        {
            cmd.Parameters.AddWithValue("@customer", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@customer", ddl_Customer.SelectedValue);
        }
        cmd.Parameters.AddWithValue("@sales_amount", txt_SalesAmount.Value);
        cmd.Parameters.AddWithValue("@transaction_line_level", dd_TransactionLineLevel.Value);
        cmd.Parameters.AddWithValue("@so_number", txt_SoNumber.Value);
        cmd.Parameters.AddWithValue("@payment_type", dd_PaymentType.Value);
        cmd.Parameters.AddWithValue("@bill_number", txt_BillNumber.Value);
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@TransactionKey", trans_key);
        this.DA.ExecuteNonQuery(cmd);
        InsertCustomTxn(trans_key);
        Response.Redirect("../Web/Transaction.aspx?trans_key=" + trans_key + "");
    }

    [WebMethod]
    public static string ValidateTransaction(string TransId,string TransType)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select * from Transactions where TransactionId=@TransactionId and TransactionType=@TransactionType";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@TransactionId", TransId);
            sqlcmd.Parameters.AddWithValue("@TransactionType", TransType);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string result = "1";

            if (dt.Rows.Count > 0)
            {
                result = "0";
            }


            return result;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

}