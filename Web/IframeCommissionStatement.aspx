﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" AutoEventWireup="true" CodeFile="IframeCommissionStatement.aspx.cs" Inherits="Web_IframeCommissionStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Ifrmae for Commission Report Statement</title>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
 <script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
    <script>
        function ExportPdf() {
            $('.CommissionTransaction').addClass('table-bordered');
            kendo.drawing
                .drawDOM("#RecipientCommissionStatementTable",
                    {
                        paperSize: "A4",
                        margin: { top: "1cm", bottom: "1cm", left: "1cm", right: "1cm" },
                        scale: 0.5,
                        height: 500
                    })
                .then(function (group) {
                    kendo.drawing.pdf.saveAs(group, "CommissionStatement.pdf")
                    $('.CommissionTransaction').removeClass('table-bordered');
                });
           
        }
</script>
     <script type="text/javascript">


         var tableToExcel = (function () {
             var uri = 'data:application/vnd.ms-excel;base64,'
                 , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                 , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                 , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
             return function (table, name) {
                 if (!table.nodeType) table = document.getElementById(table)
                 var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                 window.location.href = uri + base64(format(template, ctx))
             }
         })()
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="car">
					<div class="card-header bg-white header-elements-inline fixed-top">
						<h6 class="card-title">Commission  Statement</h6>
						<div class="header-elements">
                            <button type="button" onclick=" tableToExcel('RecipientCommissionStatementTable','Commission Statement ');;" class="btn btn-light btn-sm ml-3"><i class="icon-file-excel mr-2"></i> Excel</button>
							<button type="button" onclick="ExportPdf();" class="btn btn-light btn-sm ml-3"><i class="icon-file-pdf mr-2"></i> PDF</button>
							
	                	</div>
					</div>
                    <div class="card-body">
                        	<div class="row">
                            <div class="col-md-12 table-responsive" id="RecipientCommissionStatementTable" >
                                <table class="table" >
                                  
                                    
                                    <tbody>
                                        <tr>
                                            <th colspan="3">Commission Statement :  <asp:Label ID="lbl_CommisionStatementTitle" runat="server"></asp:Label> </th>
                                            <td><img id="img_CompanyLogo" runat="server"  class="mr-4"  style="height:3rem;" src="../UploadFile/Logo/thumb_225e6be6-0fd4-0ab7-8bd2-0313ac90166f_Bada_Logo.jpg"/></td>
                                        </tr>
                                    </tbody>
                                    </table>
                              
                                <table class="table bg-white">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table class="table bg-white">
                                                    
                                                    <tbody>
                                                        <tr>
                                                            
                                                            <td>Recipient Id</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_RecipientId" runat="server"></asp:Label></th>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Recipient Name</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_RecipientName" runat="server"></asp:Label></th>
                                                        </tr>
                                                            <tr>
                                                           
                                                            <td>Territory Id</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_TerritoryId" runat="server"></asp:Label></th>
                                                        </tr>
                                                            <tr>
                                                            
                                                            <td>Currency </td>
                                                            <td>:</td>
                                                            <th>USD</th>
                                                        </tr>
                                                            <tr>
                                                            
                                                            <td>Period</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_Period" runat="server"></asp:Label></th>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Position</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_Position" runat="server"></asp:Label></th>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Fiscal year</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_FiscalYear" runat="server"></asp:Label></th>
                                                        </tr>
                                                    </tbody>
                                                   <asp:PlaceHolder ID="ph_RecipientInfo" runat="server"></asp:PlaceHolder>
                                                </table>
                                            </td>
                                              <td>
                                               <table class="table bg-white">
                                                    <tbody>
                                                        <tr>
                                                            
                                                            <td>Gross Payout Amount/td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Draw / Adv Adjustment</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                         <tr>
                                                           
                                                            <td>Cap Adjustment</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                         <tr>
                                                            
                                                            <td>Minimum Pay Adjustment </td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                         <tr>
                                                            
                                                            <td>Other Adjustment</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Adjustment to Payout</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Recovery From Payout</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                         <tr>
                                                            
                                                            <td>Net Payout Amount</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_TotalPayOut" runat="server"></asp:Label></th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                 <asp:PlaceHolder ID="ph_ReportCommissions" runat="server"></asp:PlaceHolder>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Total Payout</th>
                                        </tr>
                                    </thead>
										<tbody>
                                            
                                            <asp:PlaceHolder ID="ph_PayOutTr" runat="server"></asp:PlaceHolder>
                                            <asp:PlaceHolder ID="ph_TotalPayOutTr" runat="server"></asp:PlaceHolder>
											
											
										</tbody>
									</table>
                                  
                            </div>
                            <!--- end print div -->
                           
                         
						</div>
                    </div>
                    </div>
            </div>
        </div>
        
    </div>
</asp:Content>

