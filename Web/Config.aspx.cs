﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Config : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();

        loadConfigList();

        if (Request.QueryString["config_key"] != null)
        {
            viewConfigDetails(Request.QueryString["config_key"]);
            div_ConfigDetails.Visible = true;
            div_InputForm.Visible = false;
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["config_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadConfigToEdit(Request.QueryString["config_key"]);
                div_ConfigDetails.Visible = false;
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }

    private void loadConfigToEdit(string ConfigKey)
    {
        //(ConfigKey, ConfigName, ConfigText1, ConfigText2, ConfigText3, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        string str_Sql_view = "select  ConfigName, ConfigText1 from IConfig where ConfigKey =@ConfigKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ConfigKey", ConfigKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            txt_ConfigName.Value = dr["ConfigName"].ToString();
            txt_ConfigText1.Value = dr["ConfigText1"].ToString();
            


        }
    }

    private void viewConfigDetails(string ConfigKey)
    {
        string str_Sql_view = "select *, convert(varchar(10),CreatedOn, 101) as Created_On from IConfig where ConfigKey =@ConfigKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ConfigKey", ConfigKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_ConfigDetails, "DivConfigView.txt", "");

        LoadConfigDetailsList(ConfigKey);
    }

    private void LoadConfigDetailsList(string configKey)
    {
        string str_Sql_view = "select *, convert(varchar(10),CreatedOn, 101) as Created_On from ICOnfigDetails where ConfigKey =@ConfigKey order by Seq";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@ConfigKey", configKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_ConfigDetailsList, "ConfigDetailListTr.txt", "");
    }

    private void loadConfigList()
    {
        string str_Sql = "select *, convert(varchar(10),CreatedOn, 101) as Created_On from IConfig order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_ConfigTr, "ConfigListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";

        //( ConfigKey, ConfigName, ConfigText1, ConfigText2, ConfigText3, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        string str_Key = Guid.NewGuid().ToString();

        string str_Sql = "insert into IConfig (ConfigKey, ConfigName, ConfigText1, CreatedOn, CreatedBy)"
                            + "select @ConfigKey, @ConfigName, @ConfigText1, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@ConfigKey", str_Key);
        cmd.Parameters.AddWithValue("@ConfigName", txt_ConfigName.Value);
        cmd.Parameters.AddWithValue("@ConfigText1", txt_ConfigText1.Value);
        
        cmd.Parameters.AddWithValue("@CreatedBy", userKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
       

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string config_key = Request.QueryString["config_key"];

        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";
        //  string str_Sql = "UPDATE transactions set transaction_id=@transaction_id, transaction_line=@transaction_line,  where transaction_key=@transaction_key  ";

        string str_Sql = "UPDATE IConfig set ConfigName=@ConfigName, ConfigText1=@ConfigText1 ," +
            " ModifiedOn=@ModifiedOn, ModifiedBy =@ModifiedBy where ConfigKey=@ConfigKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);

        cmd.Parameters.AddWithValue("@ConfigName", txt_ConfigName.Value);
        cmd.Parameters.AddWithValue("@ConfigText1", txt_ConfigText1.Value);

        cmd.Parameters.AddWithValue("@ModifiedBy", userKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@ConfigKey", config_key);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/Config.aspx?config_key=" + config_key + "");
    }
}