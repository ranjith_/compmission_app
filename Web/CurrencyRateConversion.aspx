﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="CurrencyRateConversion.aspx.cs" Inherits="Web_CurrencyRateConversion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Currency Rate Conversion</title>
    <script type="text/javascript">

        function viewRateConversion(Conversion_Key) {
            //alert(Currency_Key);
            $.ajax({
                type: "POST",
                url: "../Web/CurrencyRateConversion.aspx/viewRateConversion",
                data: "{Conversion_Key:'" + Conversion_Key + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    //alert(data.d);
                    $('#card-currency-conversion').html(data.d);
                    $('#div_InputForm').addClass('d-none');
                    $('#div_CurrencyConversionDetails').removeClass('d-none');

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }

        function deleteCurrencyConversion(Conversion_Key, Conversion_Name) {
            var confirmation = confirm("are you sure want to delete this " + Conversion_Name + " rate conversion ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/CurrencyRateConversion.aspx/deleteCurrencyRate",
                    data: "{CurrencyConversionKey:'" + Conversion_Key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert(" " + Conversion_Name + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
      <div class="container-fluid">
        <div class="row">
            <!--Data List-->
            <div class="col-lg-6 col-md-6">
                <!-- Basic datatable -->
<div class="card ">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Currency conversion  Rate 
                
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a href="CurrencyRateConversion.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>                                           
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                                        
                </div>
            </div>
        </div>
    
        <div class="card-body">
                   
       
        <div class="table-responsive">
        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>From Currency</th>
                    <th>To Currency</th>
                    <th>Effective Date</th>
                    <th>conversion Rate</th>
                    <th>Action</th>
                </tr>
                                    
            </thead>
            <tbody>
                                   
                    <asp:PlaceHolder ID="ph_RateConversionList" runat="server"></asp:PlaceHolder>
            </tbody>
        </table>
</div>
             </div>
    </div>
    <!-- /basic datatable -->
            </div>
                <!--Data List-->
                <!--Input Form-->
                <div id="div_InputForm" runat="server" class="col-lg-6 col-md-6">
                    <!-- Basic layout-->
        <div class="card ">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Rate conversion </h5>
            <div class="header-elements">
                <div class="list-icons">
                     <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                    <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                                        
                </div>
            </div>
        </div>
    
        <div class="card-body">
                        <div class="row">
                        <div class="form-group col-md-6">
                            <label>Name<span class="text-danger">*</span></label>
                            <input id="txt_Name" required runat="server" type="text" class="form-control" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Description <span class="text-danger">*</span></label>
                            <textarea id="txt_Description" runat="server" class="form-control" rows="2"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label>From Currency</label>
                            <select required id="dd_FromCurrency" runat="server" class="form-control">
                            <option value="">-- select one --</option>
                                <option value="USD">USD</option>
                                <option value="EUR">EUR</option>
                                <option value="BWD">BWD</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>To Currency</label>
                            <select required id="dd_ToCurrency" runat="server"  class="form-control">
                                <option value="">-- select one --</option>
                                    <option value="USD">USD</option>
                                    <option value="EUR">EUR</option>
                                    <option value="BWD">BWD</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Effective Date<span class="text-danger">*</span></label>
                            <input  required id="dp_EffectiveDate" runat="server" type="date" class="form-control" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>To Date<span class="text-danger">*</span></label>
                            <input required id="dp_ToDate" runat="server" type="date" class="form-control" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>Conversion Rate<span class="text-danger">*</span></label>
                            <input required id="txt_ConversionRate" runat="server" type="text" class="form-control number" />
                        </div>
    
                        </div>
                    </div>
                </div>
            <!-- /basic layout -->
            </div>
            <!--Input Form-->

             <!--View Data-->
                                 <div id="div_CurrencyConversionDetails" runat="server" class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card" id="card-currency-conversion">
                            
                                      <asp:PlaceHolder ID="ph_CurrencyConversionDetails" runat="server"></asp:PlaceHolder>
                                
                                    </div>
                                
                                </div>
                                <!--view data-->
        </div>
    </div>
    
</asp:Content>

