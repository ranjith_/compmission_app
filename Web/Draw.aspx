﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="Draw.aspx.cs" Inherits="Web_Draw" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Draw</title>
    <script>
        $(document).ready(function () {
            CompensationDrawType();
            //$('#btn_Save').addClass('disabled');
            //$('#btn_Update').addClass('disabled');
          //  AmountValidate();
        });

        function CompensationDrawType() {
            let type = $('#ddl_CompType').val();
            if (type == 1) {
                $('#AmountType').text('Threshold');
                $('#CompTypeCard').removeClass('d-none');
                $('#IsRecoverable').addClass('d-none');
                $('#DrawIsRecoverable').addClass('d-none');
             
                return true;
            }
            else if (type == 2) {
                $('#AmountType').text('Draw');
                $('#CompTypeCard').removeClass('d-none');
                $('#IsRecoverable').removeClass('d-none');
                IsRecoveredCheck();
                return true;
            }
            else {
                $('#CompTypeCard').addClass('d-none');
                return false;
            }
         
        }
        function IsRecoveredCheck() {
            if ($('#chb_IsRecoverable ').prop("checked") == true) {
                $('#DrawIsRecoverable').removeClass('d-none');
            }
            else {
                $('#DrawIsRecoverable').addClass('d-none');
            }
        }
        function CheckValidate() {
           return CompensationDrawType();
        }
        function ValidateFields() {
          
            
            
            if ($('#ddl_SalesRep').val() == "" && $('#ddl_CompType').val() == "" && $('#txt_ThresholdAmount').val() == "") {
                return false
            }
            
        }
        //function AmountValidate(element) {
            
        //    if ($('#txt_ThresholdAmount').val() != '') {
        //        $('#btn_Save').removeClass('disabled');
        //        $('#btn_Update').removeClass('disabled');
        //    } else {
        //        $('#btn_Save').addClass('disabled');
        //        $('#btn_Update').addClass('disabled');
        //    }
        //}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
<div class="container-fluid">
<div class="row">
                              
<div class="col-lg-12 col-md-12">
                                   
<div class="card border-top-2 border-top-primary-400 ">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Draw 
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                  <%-- <button type="button" class="btn btn-primary btn-sm" id="btn_Save" onclick="javascript:return ValidateFields()" runat="server" onserverclick="btn_Save_ServerClick"  >Save</button>--%>
                    <asp:Button id="btn_Save" CssClass="btn btn-primary btn-sm" OnClientClick="javascript:return ValidateFields()" runat="server" OnClick="btn_Save_ServerClick" Text="Save"/>
                    <asp:Button id="btn_Update" CssClass="btn btn-primary btn-sm" visible="false"   OnClientClick="javascript:return ValidateFields()" runat="server" OnClick="btn_Update_ServerClick" Text="Update"/>
                 <%--  <button type="button" class="btn btn-primary btn-sm" id="btn_Update" visible="false" runat="server" onserverclick="btn_Update_ServerClick"  >Update</button>--%>
                    <a class="list-icons-item" data-action="collapse"></a>
                  
                </div>
            </div>
        </div>
    
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                   
                    <div class="col-md-12 form-group">
                    <label>Sales Rep</label>
                    <asp:DropDownList ID="ddl_SalesRep"  ClientIDMode="Static" CssClass="form-control" runat="server" required="true"></asp:DropDownList>
                    </div>
                
                </div>
                <div class="col-md-3 form-group">
                    <label>Select Type</label>
                    <asp:DropDownList ID="ddl_CompType" ClientIDMode="Static" onchange="CompensationDrawType()" required="true"  CssClass="form-control" runat="server">
                        <asp:ListItem Text="Select Type" Value=""></asp:ListItem>
                        <asp:ListItem Text="Threshold" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Draw" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-12 d-none" id="CompTypeCard">
                    <div class=" card card-body border-left-2 border-left-primary-700 mx-2  ">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="row m-0">
                            <div class="col-md-3 form-group">
                            <label><span id="AmountType"></span> Amount</label>
                            <input type="text" class="form-control number" id="txt_ThresholdAmount" onfocusOut="AmountValidate(this);" required runat="server" />
                        </div>
                         <div class="col-md-3 form-group">
                            <label>Start Date</label>
                            <input type="date" class="form-control" id="dp_StartDate" required runat="server" />
                        </div>
                        <div class="col-md-3 form-group">
                            <label>End Date</label>
                            <input type="date" class="form-control" id="dp_EndDate" required runat="server" />
                        </div>

                            <div class="col-md-3 my-auto form-group d-none" id="IsRecoverable">
                               <div class="form-check">
				                        <label class="form-check-label">
                                        <input onclick="IsRecoveredCheck(this)" type="checkbox" class="form-check-input-styled" id="chb_IsRecoverable" runat="server"  />
                                            Is Recoverable
                                            </label>
		                        </div>
                             </div>
                                
                            </div>
                        </div>
                        <div class="col-md-12 d-none " id="DrawIsRecoverable">
                            <div class="row m-0">
                             
                        <div class="col-md-3 form-group">
                            <label>Recoverable Start Date</label>
                            <input type="date" class="form-control" id="dp_RecStartDate" runat="server" />
                        </div>
                        <div class="col-md-3 form-group">
                            <label>Recoverable End Date</label>
                            <input type="date" class="form-control" id="dp_RecEndDate" runat="server" />
                        </div>
                                </div>
                        </div>
                            </div>
                    </div>
                </div>
               </div>
            </div>

            
        </div>
                    
</div>
                       
</div>

</div>
</div>
</asp:Content>

