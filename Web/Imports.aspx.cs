﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using System.Data.OleDb;

public partial class Web_Imports : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    Common Com;
    PhTemplate PHT;

    DataAccessExcel DAE;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom(true);
        this.Com = new Common();
        this.PHT = new PhTemplate();
        loadImportList();
    }

    private void loadImportList()
    {
        string str_Sql = "select * from Imports order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_ImportListTr, "DivImportListTr.txt", "");
    }

    protected void btn_ConnectFile_ServerClick(object sender, EventArgs e)
    {
        
        DataTable dtResult = null;
        string str_Sql = "", str_FilePath = "", str_FileName = "", str_ImportSeq = "";
        if (fu_ImportFile.PostedFile.FileName == "")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please upload the File');", true);
            return;
        }
        else
        {
            str_FileName = Guid.NewGuid() + "_" + fu_ImportFile.PostedFile.FileName;

            string str_Root = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "UploadFile/BatchFiles/";
            str_FilePath = str_Root + str_FileName;
            string str_FileExt = Path.GetExtension(str_FilePath);
            if (!Directory.Exists(str_Root))
            {
                Directory.CreateDirectory(str_Root);
            }
            fu_ImportFile.PostedFile.SaveAs(Server.MapPath("~//UploadFile/BatchFiles/" + str_FileName));
            
            //(ImportKey, ImportName, ImportDesc, SourceType, FileName, ActualFileName, CreatedBy, CreatedOn)
            string str_Key = Guid.NewGuid().ToString();

            str_Sql = @"insert into Imports(ImportKey, ImportName, ImportDesc, SourceType, FileName, ActualFileName, CreatedBy, CreatedOn)
                        select @ImportKey, @ImportName, @ImportDesc, @SourceType, @FileName, @ActualFileName, @CreatedBy, @CreatedOn";

            SqlCommand sqc = new SqlCommand(str_Sql);
            sqc.Parameters.AddWithValue("@ImportKey", str_Key);
            sqc.Parameters.AddWithValue("@ImportName", txt_Name.Value);
            sqc.Parameters.AddWithValue("@ImportDesc", txt_Desc.Value);
            sqc.Parameters.AddWithValue("@SourceType", dd_SourceType.Value);
            sqc.Parameters.AddWithValue("@FileName", str_FileName);
            sqc.Parameters.AddWithValue("@ActualFileName", fu_ImportFile.PostedFile.FileName);
            sqc.Parameters.AddWithValue("@CreatedBy", this.SC.UserKey);

            sqc.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
            DA.ExecuteNonQuery(sqc);
            

            Response.Redirect("../Web/ImportData.aspx?ImportKey=" + str_Key + "&file_path="+str_FilePath);
           
        }
        
        
    }
}