﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionPlanEdit : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        setDefaultAgreement();
        loadRecipientList();
    }

    private void loadRecipientList()
    {
        string str_Sql = "select a.PlanKey, a.RecipientKey,concat(b.FirstName ,' ', b.LastName ) as RecipientName, b.RecipientId, c.text as RepType, d.Text as ReportingManager, e.Text as TerritoryId, format( a.StartDate,'MM/d/yyy') as sDate,format( a.EndDate,'MM/d/yyy') as eDate from RecipientPlan a left join Recipients b on a.RecipientKey=b.RecipientKey left join v_RecipientCategory c on b.RecipientType=c.Value left join v_RecipientManager d on b.ReportingManager=d.Value left join v_Territory e on b.RecipientTerritory=e.Value";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_CommissionRecipientPlanTr, "DivCommissionPlanListTr.txt","");
    }
    private void setDefaultAgreement()
    {
        string str_Sql = "update RecipientPlan set AgreementKey=(select AgreementKey from Agreement where IsDefault=1) where AgreementKey is NULL";
        SqlCommand cmd = new SqlCommand(str_Sql);
        this.DA.ExecuteNonQuery(cmd);
    }
}