﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionList.aspx.cs" Inherits="Web_CommissionPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commissions</title>
    <style>
        .desc-comp{
                line-height: 1.5em;
    height: 2.5em;
        width: 430px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
        }
    </style>
    <script>
        
    function deleteCommission(CommissionKey, CommissionName) {
        var confirmation = confirm("are you sure want to delete this " + CommissionName + "  Commission ?");
    if (confirmation == true) {
        //to delete
        $.ajax({
            type: "POST",
            url: "../Web/CommissionList.aspx/deleteCommission",
            data: "{CommissionKey:'" + CommissionKey + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                if (data.d == "1") {
                    alert("Commission " + CommissionName + " deleted successfully")
                    location.reload();
                }
                else if (data.d == "0") {
                    alertNotify('Commission  Error', 'Delete is ignored because Commission is already started. For more Info : please contact admin.  ', 'bg-warning border-warning');
                }

            },
            error: function () {
                alert('Error communicating with server');
            }
        });
    }
    else {
        return false
    }
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                                    <!-- Basic datatable -->
                     <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Commission List
                                   
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="btn btn-primary  btn-sm " href="CommissionInitDetails.aspx">New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                   
                           
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Commission Name</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Calculation Frequency</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_CommisssionListTr" runat="server"></asp:PlaceHolder>
                                </tbody>
                            </table>
                        </div>
                            </div>
                        </div>
                        <!-- /basic datatable -->
               
                        
                
                         
                               
                               </div>
        </div>
    </div>
</asp:Content>

