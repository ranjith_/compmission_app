﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="Customers.aspx.cs" Inherits="Web_Customers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Customers</title>
     <script type="text/javascript">
         function viewCustomer(cust_key) {
             $.ajax({
                 type: "POST",
                 url: "../Web/Customers.aspx/viewCustomer",
                 data: "{cust_key:'" + cust_key + "'}",
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function (data) {

                     // alert(data.d);
                     $('#card-customer').html(data.d);
                     $('#div_InputForm').addClass('d-none');
                     $('#div_CustomerDetails').removeClass('d-none');

                 },
                 error: function () {
                     alert('Error communicating with server');
                 }
             });
         }
         function deleteCustomer(cust_key, cust_name) {
             var confirmation = confirm("are you sure want to delete the customer " + cust_name + "  ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/Customers.aspx/deleteCustomer",
                    data: "{CustKey:'" + cust_key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Customer " + cust_name + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
         }
         function ValidateCustomerId(element) {
             let CustomerId = $(element).val();
             if (CustomerId != "") {

                 let CustomerIdUpdate = $('#hdn_CustomerId').val();
                 $('#btn_Update').removeAttr('disabled');
                 if (CustomerId != CustomerIdUpdate) {
                     $.ajax({
                         type: "POST",
                         url: "../Web/Customers.aspx/ValidateCustomerId",
                         data: "{CustomerId:'" + CustomerId + "'}",
                         contentType: "application/json; charset=utf-8",
                         dataType: "json",
                         success: function (data) {
                             if (data.d == "0") {
                                 $('#btn_Save').prop('disabled', 'true');
                                 $('#btn_Update').prop('disabled', 'true');

                                 $('#txt_CustomerId-error').addClass(' validation-invalid-label');
                                 $('#txt_CustomerId-error').removeClass(' validation-valid-label');
                                 $('#txt_CustomerId-error').text('Customer Id Already Exists');

                             }
                             else if (data.d == "1") {
                                 $('#btn_Save').removeAttr('disabled');
                                 $('#btn_Update').removeAttr('disabled');
                                 $('#txt_CustomerId-error').removeClass('validation-invalid-label');
                                 $('#txt_CustomerId-error').addClass(' validation-valid-label');
                                 //   $('#txt_RecipientId-error').text('Valid Recipient Id');
                                 $('#txt_CustomerId-error').addClass('d-none');
                             }

                            
                         },
                         error: function () {
                             alert('Error communicating with server');
                         }
                     });
                 }



             }
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
          <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-6 col-md-6">
                                    <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Customers </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                         <button id="btn_Excel" runat="server" onserverclick="btn_Excel_ServerClick"  type="button" class="btn border-default btn-sm"><i class="icon-file-excel position-left"></i>  .csv</button>
                                      
                                        <a href="Customers.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                    
                            
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Customer Id</th>
                                        <th>Customer Name</th>
                                        <th>Customer Type</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_CustomersTr" runat="server"></asp:PlaceHolder>
                                        
                                </tbody>
                            </table>
                    </div>
                                </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                 <!--Input Form-->
                                 <div id="div_InputForm" runat="server" class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                            <div class="card ">
                            <div class="card-header header-elements-inline ">
                                <h5 class="card-title">Customers</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                          <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Customer Id<span class="text-danger">*</span></label>
                                                <input type="text" onfocusOut="ValidateCustomerId(this);" id="txt_CustomerId" runat="server" required class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Customer Name<span class="text-danger">*</span></label>
                                                <input type="text" id="txt_CustomerName" runat="server" required class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Customer Email<span class="text-danger">*</span></label>
                                                <input type="email" id="txt_CustomerEmail" runat="server"  class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                    <label>Customer Type <span class="text-danger">*</span></label>
                                                    <input type="text" id="txt_CustomerType" runat="server" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Mobile<span class="text-danger">*</span></label>
                                                
                                                <input type="text" id="txt_Mobile" runat="server" class="form-control"/>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Country</label>
                                                <select class="form-control" id="dd_Country" runat="server">
                                                <option value="">-- select country --</option>
                                                    <option value="1">America</option>
                                                    <option  value="2">South Africa</option>
                                                    <option  value="3">India</option>
                                                    <option  value="4">Pondichery</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>State</label>
                                                <select class="form-control" id="dd_State" runat="server">
                                                    <option value="">-- select state --</option>
                                                    <option  value="11">Tamilnadu</option>
                                                    <option value="12">Kerala</option>
                                                    <option  value="13">Andhra</option>
                                                    <option  value="14">Srilanka</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>City</label>
                                                <select class="form-control"  id="dd_City" runat="server">
                                                    <option value="">-- select city --</option>
                                                    <option  value="21">Chennai</option>
                                                    <option  value="22">Madurai</option>
                                                    <option value="23">Cuddalore</option>
                                                    <option value="24">Villupuram</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                    <label>Address <span class="text-danger">*</span></label>
                                                    <textarea id="txt_Address" runat="server" rows="2" cols="3" class="form-control" ></textarea>
                                            </div>
                                            
                            </div>
                                        </div>
                                    </div>
                               
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->
                                <!--View Data-->
                                 <div id="div_CustomerDetails" runat="server"  class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card " id="card-customer">
                            
                                <asp:PlaceHolder ID="ph_CustomerDetails" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
                            </div>
                        </div>
    <asp:HiddenField ID="hdn_CustomerId" runat="server" />
</asp:Content>

