﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CurrencyRateConversion : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        loadCurrencyConversionList();

        if (Request.QueryString["conversion_key"] != null)
        {
            viewCurrencyConversionDetails(Request.QueryString["conversion_key"]);
            div_CurrencyConversionDetails.Visible = true;
            div_CurrencyConversionDetails.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Visible = false;
        }

        if (!IsPostBack)
        {
            if (Request.QueryString["conversion_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadCurrencyConversionDetailToEdit(Request.QueryString["conversion_key"]);
                div_CurrencyConversionDetails.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }

    private void loadCurrencyConversionDetailToEdit(string conversion_key)
    {


        // (RateConversionKey, Name, Description, FromCurrency, ToCurrency, replace(convert(varchar, EffectiveDate, 111),'/', '-') as Effective_Date ,replace(convert(varchar, ToDate, 111),'/', '-') as ToDate, ConversionRate, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)


        string str_Sql_view = "select  Name, Description, FromCurrency, ToCurrency, replace(convert(varchar, EffectiveDate, 111),'/', '-') as Effective_Date ,replace(convert(varchar, ToDate, 111),'/', '-') as To_Date, ConversionRate" +
            " from CurrencyRateConversion where  RateConversionKey =@RateConversionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@RateConversionKey", conversion_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            txt_Name.Value = dr["Name"].ToString();
            txt_Description.Value = dr["Description"].ToString();
            dd_FromCurrency.Value = dr["FromCurrency"].ToString();
            dd_ToCurrency.Value = dr["ToCurrency"].ToString();
            dp_EffectiveDate.Value = dr["Effective_Date"].ToString();
            dp_ToDate.Value = dr["To_Date"].ToString();
            txt_ConversionRate.Value = dr["ConversionRate"].ToString();


        }
    }

    private void viewCurrencyConversionDetails(string RateConversionKey)
    {
        string str_Sql_view = "select * from CurrencyRateConversion where RateConversionKey =@RateConversionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@RateConversionKey", RateConversionKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_CurrencyConversionDetails, "DivCurrencyConversionView.txt", "");
    }
    [WebMethod]
    public static string viewRateConversion(string Conversion_Key)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select * from CurrencyRateConversion where RateConversionKey =@RateConversionKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@RateConversionKey", Conversion_Key);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivCurrencyConversionView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    private void loadCurrencyConversionList()
    {
        string str_Sql = "select * from CurrencyRateConversion order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_RateConversionList, "CurrencyConversionListTr.txt", "");
    }
    [WebMethod]
    public static string deleteCurrencyRate(string CurrencyConversionKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from CurrencyRateConversion where RateConversionKey=@RateConversionKey";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@RateConversionKey", CurrencyConversionKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    protected void btn_Save_Click(object sender, EventArgs e)
    {
        //(RateConversionKey, Name, Description, FromCurrency, ToCurrency, EffectiveDate, ToDate, ConversionRate, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";


        string str_Key = Guid.NewGuid().ToString();

        string str_Sql = "insert into CurrencyRateConversion (RateConversionKey, Name, Description, FromCurrency, ToCurrency, EffectiveDate, ToDate, ConversionRate, CreatedOn, CreatedBy)"
                            + "select @RateConversionKey, @Name, @Description, @FromCurrency, @ToCurrency, @EffectiveDate, @ToDate, @ConversionRate, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@RateConversionKey", str_Key);
        cmd.Parameters.AddWithValue("@Name", txt_Name.Value);
        cmd.Parameters.AddWithValue("@Description", txt_Description.Value);
        cmd.Parameters.AddWithValue("@FromCurrency", dd_FromCurrency.Value);
        cmd.Parameters.AddWithValue("@ToCurrency", dd_ToCurrency.Value);

        cmd.Parameters.AddWithValue("@EffectiveDate", dp_EffectiveDate.Value.ToString());
        cmd.Parameters.AddWithValue("@ToDate", dp_ToDate.Value.ToString());
        cmd.Parameters.AddWithValue("@ConversionRate", txt_ConversionRate.Value);

        cmd.Parameters.AddWithValue("@CreatedBy", userKey);
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
      

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string CurrencyConversionKey = Request.QueryString["conversion_key"];

        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";
        //  RateConversionKey, Name, Description, FromCurrency, ToCurrency, EffectiveDate, ToDate, ConversionRate, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy

        string str_Sql = "UPDATE CurrencyRateConversion set Name=@Name, Description=@Description, FromCurrency=@FromCurrency," +
            "ToCurrency=@ToCurrency, EffectiveDate=@EffectiveDate, ToDate=@ToDate, ConversionRate=@ConversionRate, ModifiedBy=@ModifiedBy where RateConversionKey=@RateConversionKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);

        cmd.Parameters.AddWithValue("@Name", txt_Name.Value);
        cmd.Parameters.AddWithValue("@Description", txt_Description.Value);
        cmd.Parameters.AddWithValue("@FromCurrency", dd_FromCurrency.Value);
        cmd.Parameters.AddWithValue("@ToCurrency", dd_ToCurrency.Value);

        cmd.Parameters.AddWithValue("@EffectiveDate", dp_EffectiveDate.Value.ToString());
        cmd.Parameters.AddWithValue("@ToDate", dp_ToDate.Value.ToString());
        cmd.Parameters.AddWithValue("@ConversionRate", txt_ConversionRate.Value);

        cmd.Parameters.AddWithValue("@ModifiedBy", userKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@RateConversionKey", CurrencyConversionKey);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/CurrencyRateConversion.aspx?conversion_key=" + CurrencyConversionKey + "");
    }
}