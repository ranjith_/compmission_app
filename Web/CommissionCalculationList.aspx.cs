﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_CommissionCalculationList : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        loadCommissionCalculationList();
            
    }

    private void loadCommissionCalculationList()
    {
        string str_Sql_view = "select a.ScheduleCalculationKey, b.Frequency, a.ProcessFrequency, c.Name as PFrequencyName, d.Text as FrequencyName, a.CommissionKey,b.CommissionName,format(b.StartDate,'MM/dd/yyyy') as sDate, format(b.EndDate,'MM/dd/yyyy') as eDate from  SchedulePlanCalculation a join Commission b on a.CommissionKey=b.CommissionKey left join v_ProcessFrequency c on a.ProcessFrequency=c.Seq left join v_MasterFrequency d on b.Frequency=d.Value";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
       
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_CommissionCalculation, "DivSchedulePlanCalculationlistTr.txt", "");
    }
}