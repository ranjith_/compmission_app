﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionReportStatement.aspx.cs" Inherits="Web_CommissionReportStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission | Report & Statement</title>
   <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
 <script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
    <style>
        .dataTables_length{
            display:none !important;
        }
        .dataTables_filter label, .dataTables_filter{
            width:100% !important;
        }
        .dataTables_filter span{
            display:none;
        }
        .dataTables_filter input{
            width:100% !important;
        }
        .content {
            padding: .8rem 1.25rem;
        }
         .IFrameStyle{
            

            border: none;
            box-shadow: -1px 1px 9px 0px #1908081c;
            border-top: 3px solid #2196f3;

        }
    </style>
  
    <script>
        
        function ExportPdf() {
            $('.CommissionTransaction').addClass('table-bordered');
            kendo.drawing
                .drawDOM("#RecipientCommissionStatementTable",
                    {
                        paperSize: "A4",
                        margin: { top: "1cm", bottom: "1cm", left: "1cm", right: "1cm" },
                        scale: 0.5,
                        height: 500
                    })
                .then(function (group) {
                    kendo.drawing.pdf.saveAs(group, "CommissionPlan.pdf")
                });
            
        }
</script>
   
    <script>
     

        function loadOptionPeriod(val) {
            let FiscalYear = $('#ddl_FiscalYear').val();
            $.ajax({
                type: "POST",
                url: "../Web/CommissionReportStatement.aspx/loadOptionPeriod",
                data: "{Year:'" + FiscalYear + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data.d);
                    $('#dd_FiscalPeriod').append(data.d);
                    if (val == 1) {
                        let PeriodKeySelected = $('#hdn_PeriodKey').val();
                        $('#dd_FiscalPeriod').val(PeriodKeySelected);
                        $('#dd_FiscalPeriod').select2().trigger('change');
                    }
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function loadRecipientListTr() {
           // let FiscalYear = $("#ddl_FiscalYear option:selected").html();
            let FiscalYear = $('#ddl_FiscalYear').val();    
            let PeriodKey = $('#dd_FiscalPeriod').val();
            $.ajax({
                type: "POST",
                url: "../Web/CommissionReportStatement.aspx/LoadRecipients",
                data: "{FiscalYear:'" + FiscalYear + "',PeriodKey:'" + PeriodKey + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data.d);
                    $('#CommissionRecipientTBody').empty();
                    var table = $('#DataTables_Table_0').DataTable();

                    //clear datatable
                    table.clear().draw();
                    //destroy datatable
                    table.destroy();

                    $('#CommissionRecipientTBody').append(data.d);
                    $('.datatable-basic-custom').dataTable();
                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function ViewReport(val) {
            $('#CompReportIframe').addClass('d-none');
            $('#CompReportIframe').attr('src', 'IframeCommissionStatement.aspx?' + val);
            AutoIframeHeight();

            $('#CompReportIframe').removeClass('d-none');
        }
        function AutoIframeHeight() {
            //   console.log('XXXX');
            var iFrameID = document.getElementById('CompReportIframe');
            if (iFrameID) {
                var height = iFrameID.height;
                var height2 = iFrameID.contentWindow.document.body.scrollHeight + "px";
                // console.log(height + "----" + height2);
                if (height != height2) {
                    iFrameID.height = (iFrameID.contentWindow.document.body.scrollHeight) + 3 + "px";
                }
                // here you can make the height, I delete it first, then I make it again
                // iFrameID.height = "";
                //  iFrameID.height = (iFrameID.contentWindow.document.body.scrollHeight + 3) + "px";

            }
        }
        $(document).ready(function () {
            $('#CompReportIframe').addClass('d-none');
            setInterval(AutoIframeHeight, 1000);
            $('.datatable-basic-custom').dataTable();

            let FiscalYear = $('#ddl_FiscalYear').val();
            if (FiscalYear != "") {
                //hdn_PeriodKey
                loadOptionPeriod(1);
                let PeriodKeySelected = $('#hdn_PeriodKey').val();
               // alert(PeriodKeySelected);
               // $('#dd_FiscalPeriod').val('2a7c8bbb-655a-423b-9912-1a9cc75fefa8');
                //$('#dd_FiscalPeriod').select2().trigger('change');
            }
        });
    </script>
    <script type="text/javascript">
      

        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                 <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Reports
                                    <div id="bypassme"></div>
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                   
                                <div class="form-group col-md-12 mx-auto">
                                    <label>Fiscal Year</label>
									<asp:DropDownList CssClass="form-control-select2" onchange="loadOptionPeriod(2);" ClientIDMode="Static" ID="ddl_FiscalYear" runat="server"></asp:DropDownList>
								</div>
                                <div class="form-group col-md-12 mx-auto">
                                    <label>Period </label>
									<select onchange="loadRecipientListTr();" class="form-control-select2" id="dd_FiscalPeriod" >
										<option value="">select period</option>
										
									</select>
								</div>
                                
                            <!-- Tabs -->
                            <ul class="nav nav-tabs nav-tabs-solid nav-justified   border-bottom-0 border-blue mb-0">
                                <li class="nav-item">
                                    <a href="#tab-Statements" class="nav-link font-size-sm text-uppercase active" data-toggle="tab">
                                        Statements
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="#tab-Reports" class="nav-link font-size-sm text-uppercase" data-toggle="tab">
                                        Reports
                                    </a>
                                </li>

                               
                            </ul>
                            <!-- /tabs -->


                            <!-- Tabs content -->
                            <div class="tab-content card-body" style=" border: 1px solid #03a9f4db; ">
                                <div class="tab-pane active fade show" id="tab-Statements">
                                   <div class="table-responsive">
                                       <table class="table  datatable-basic-custom table-hover">
                                           <thead>
                                               <tr>
                                                   <th>Id</th>
                                                   <th>Name</th>
                                                   <th>Role</th>
                                               </tr>
                                           </thead>
                                           <tbody id="CommissionRecipientTBody">
                                              
                                           </tbody>
                                       </table>
                                   </div>
                                </div>

                                <div class="tab-pane fade" id="tab-Reports">
                                     <div class="form-group col-md-12 mx-auto">
                                    <label>Report Section</label>
									<select data-placeholder="select Report " class="form-control-select2" >
										<option value="">select report section</option>
										<option value="discounts">   Admin Reports </option>
										<option value="catalog"> Credit Sales Reports</option>
										<option value="prints"> Insurance Reports</option>
										<option value="promo">Payout Reports</option>
									</select>
								</div>
                                <div class="form-group col-md-12 mx-auto">
                                    <label>Report Name </label>
									<select data-placeholder="select period " class="form-control-select2" >
										<option value="">select period</option>
										<option value="discounts">Buckets data by Description-Dimension-Periods</option>
										<option value="catalog"> Credit for Transaction ID</option>
										<option value="prints">Credit for Transaction Line</option>
										<option value="promo"> Credit Statement for Payee</option>
									</select>
								</div>
                                <div class="form-group col-md-12 text-right">
                                    <button type="button" class="btn  btn-primary bg-blue btn-sm">View Report </button>
								</div>
                                </div>

                               
                            </div>
                            <!-- /tabs content -->
                            </div>
                        </div>
            </div>
            <div class="col-md-8" id="div_Invoice" runat="server">
                <iframe id="CompReportIframe" class="IFrameStyle" src="" width="100%" height="100%"></iframe>
                 <!-- Invoice tem plate -->
				<div class="card d-none">
					<div class="card-header bg-transparent header-elements-inline">
						<h6 class="card-title">Commission  Statement</h6>
						<div class="header-elements">
                            <button type="button" onclick=" tableToExcel('RecipientCommissionStatementTable','Commission Statement ');;" class="btn btn-light btn-sm ml-3"><i class="icon-file-excel mr-2"></i> Excel</button>
							<button type="button" onclick="ExportPdf();" class="btn btn-light btn-sm ml-3"><i class="icon-file-pdf mr-2"></i> PDF</button>
							
	                	</div>
					</div>

					<div class="card-body">
						<div class="row">
                            <div class="col-md-12 table-responsive" id="RecipientCommissionStatementTable" >
                                <table class="table" >
                                  
                                    
                                    <tbody>
                                        <tr>
                                            <th colspan="3">Commission Statement :  <asp:Label ID="lbl_CommisionStatementTitle" runat="server"></asp:Label> </th>
                                            <td><img id="img_CompanyLogo" runat="server"  class="mr-4"  style="height:3rem;" src="../UploadFile/Logo/thumb_225e6be6-0fd4-0ab7-8bd2-0313ac90166f_Bada_Logo.jpg"/></td>
                                        </tr>
                                    </tbody>
                                    </table>
                              
                                <table class="table bg-white">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table class="table bg-white">
                                                    
                                                    <tbody>
                                                        <tr>
                                                            
                                                            <td>Recipient Id</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_RecipientId" runat="server"></asp:Label></th>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Recipient Name</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_RecipientName" runat="server"></asp:Label></th>
                                                        </tr>
                                                            <tr>
                                                           
                                                            <td>Territory Id</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_TerritoryId" runat="server"></asp:Label></th>
                                                        </tr>
                                                            <tr>
                                                            
                                                            <td>Currency </td>
                                                            <td>:</td>
                                                            <th>USD</th>
                                                        </tr>
                                                            <tr>
                                                            
                                                            <td>Period</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_Period" runat="server"></asp:Label></th>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Position</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_Position" runat="server"></asp:Label></th>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Fiscal year</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_FiscalYear" runat="server"></asp:Label></th>
                                                        </tr>
                                                    </tbody>
                                                   <asp:PlaceHolder ID="ph_RecipientInfo" runat="server"></asp:PlaceHolder>
                                                </table>
                                            </td>
                                              <td>
                                               <table class="table bg-white">
                                                    <tbody>
                                                        <tr>
                                                            
                                                            <td>Gross Payout Amount/td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Draw / Adv Adjustment</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                         <tr>
                                                           
                                                            <td>Cap Adjustment</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                         <tr>
                                                            
                                                            <td>Minimum Pay Adjustment </td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                         <tr>
                                                            
                                                            <td>Other Adjustment</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Adjustment to Payout</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                        <tr>
                                                            
                                                            <td>Recovery From Payout</td>
                                                            <td>:</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                         <tr>
                                                            
                                                            <td>Net Payout Amount</td>
                                                            <td>:</td>
                                                            <th><asp:Label ID="lbl_TotalPayOut" runat="server"></asp:Label></th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                 <asp:PlaceHolder ID="ph_ReportCommissions" runat="server"></asp:PlaceHolder>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Total Payout</th>
                                        </tr>
                                    </thead>
										<tbody>
                                            
                                            <asp:PlaceHolder ID="ph_PayOutTr" runat="server"></asp:PlaceHolder>
                                            <asp:PlaceHolder ID="ph_TotalPayOutTr" runat="server"></asp:PlaceHolder>
											
											
										</tbody>
									</table>
                                  
                            </div>
                            <!--- end print div -->
                           
                         
						</div>

						
					</div>
               
					<div class="card-body">
						<div class="d-md-flex flex-md-wrap">
							<div class="pt-2 mb-3">
								
								
							</div>

							<div class="pt-2 mb-3 wmin-md-400 ml-auto">
								

								<div class="text-right mt-3">
									<button type="button" class="btn btn-primary btn-labeled btn-labeled-left"><b><i class="icon-paperplane"></i></b> Send Statememnt</button>
								</div>
							</div>
						</div>
					</div>

					
				</div>
				<!-- /invoice template -->
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdn_PeriodKey" runat="server" ClientIDMode="Static" />
</asp:Content>

