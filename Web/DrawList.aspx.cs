﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_DrawList : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        loadDrawList();
    }
    private void loadDrawList()
    {
        string str_Sql = "select *, b.Value as RepId, b.Text as RepName, (case when a.Type=1 then 'Threshold' when a.Type=2 then 'Draw' end  )as DrawType, format(a.StartDate, 'MM/dd/yyyy') as sDate, format(a.EndDate, 'MM/dd/yyyy') as eDate from Draw a join v_RecipientList b on a.RecipientKey=b.RecipientKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_DrawListTr, "DrawListTr.txt", "");
    }
    [WebMethod]
    public static string deleteDraw(string DrawKey)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Draw where DrawKey=@DrawKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@DrawKey", DrawKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}