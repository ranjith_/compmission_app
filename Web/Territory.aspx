﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="Territory.aspx.cs" Inherits="Web_Territory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Territory</title>
     <link rel="stylesheet" href="https://rawgit.com/dabeng/OrgChart/master/dist/css/jquery.orgchart.css" />
    
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>-->
  <script src="https://rawgit.com/dabeng/OrgChart/master/dist/js/jquery.orgchart.js"></script>  <!-- this is only for the GoJS Samples framework -->
  
    <script type="text/javascript">

        function viewTerritory(territory_key) {
            //alert(Currency_Key);
            $.ajax({
                type: "POST",
                url: "../Web/Territory.aspx/viewTerritory",
                data: "{TerritoryKey:'" + territory_key + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    //alert(data.d);
                    $('#card-territory').html(data.d);
                    $('#div_InputForm').addClass('d-none');
                    $('#div_TerritoryDetails').removeClass('d-none');

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }


        function deleteTerritory(territory_key, territory_name) {
            var confirmation = confirm("are you sure want to delete this " + territory_name + "  territory ?");
            if (confirmation == true) {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/Territory.aspx/deleteTerritory",
                    data: "{TerritoryKey:'" + territory_key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data.d = "DeleteSuccess") {
                            alert("Territory " + territory_name + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }

                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }

        function ValidateTerritoryId() {
            //alert(Currency_Key);
            let TerritoryId = $('#txt_TerritoryId').val();
            let hdnTerritoryId = $('#hdn_TerritoryId').val();
           
            if (TerritoryId != "") {

                if (TerritoryId == hdnTerritoryId) {
                    $('#btn_Save').removeAttr('disabled')
                    $('#btn_Update').removeAttr('disabled')

                    $('#txt_TerritoryId-error').removeClass(' validation-invalid-label');
                    $('#txt_TerritoryId-error').addClass('d-none');
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "../Web/Territory.aspx/ValidateTerritoryId",
                        data: "{TerritoryId:'" + TerritoryId + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            if (data.d == "0") {
                                $('#btn_Save').prop('disabled', 'true');
                                $('#btn_Update').prop('disabled', 'true');
                                // $('#btn_Update').addClass('disabled');
                                $('#txt_TerritoryId-error').addClass(' validation-invalid-label');
                                $('#txt_TerritoryId-error').removeClass(' validation-valid-label');
                                $('#txt_TerritoryId-error').text('Territory Id Already Exists');



                            }
                            else if (data.d == "1") {
                                //$('#btn_Save').removeClass('disabled');
                                //$('#btn_Update').removeClass('disabled');
                                $('#btn_Save').removeAttr('disabled')
                                $('#btn_Update').removeAttr('disabled')

                                $('#txt_TerritoryId-error').removeClass(' validation-invalid-label');
                                $('#txt_TerritoryId-error').addClass('d-none');


                            }

                        },
                        error: function () {
                            alert('Error communicating with server');
                            return false;
                        }
                    });
                }

               
            }

            
        }
    </script>
    
  <style>
#headline {
  background-color: rgba(0, 0, 0, 0.5);
  text-align: center;
}

.demo-heading {
  padding: 40px 10px 0px 10px;
  margin: 0px;
  font-size: 3em;
  color: #fff;
}

.demo-container {
  position: relative;
  display: inline-block;
  top: 10px;
  left: 10px;
  height: 420px;
  width: calc(100% - 24px);
  border: 2px dashed #eee;
  border-radius: 5px;
  overflow: auto;
  text-align: center;
}

.orgchart {
  background: #ffffff;
  width:100%;
  overflow-y: scroll;
}

.orgchart>.spinner {
  color: rgba(255, 255, 0, 0.75);
}

.orgchart .node .title {
  background-color: #6b6b6b;
  color: #fff;
        padding: 4px;
    text-align: center;
    font-size: 1em;
    font-weight: normal;
    height: auto;
    line-height: normal;
    overflow: inherit;
    text-overflow: unset;
    white-space: normal;
    border-radius: 4px;
}
.orgchart .node{
        padding:0px;
        border:none;
        border-radius: 4px;
            margin: 0px 5px 0px 5px;
        }
.orgchart .node .content {
  border-color: transparent;
  border-top-color: #333;
}

.orgchart .node>.spinner {
  color: rgba(184, 0, 54, 0.75);
}

.orgchart .node:hover {
 box-shadow: 0px 8px 7px 1px #88858578;
        background:#6b6b6b;
        cursor:pointer;
}

.orgchart .node.focused {
  border:3px soild red;
}

.orgchart .node .edge {
  color: rgba(0, 0, 0, 0.6);
}

.orgchart .edge:hover {
  color: #000;
}

.orgchart td.left,
.orgchart td.top,
.orgchart td.right {
  border-color: #fff;
}

.orgchart td>.down {
  background-color: #fff;
}
        
        .orgchart .lines .rightLine {
    border-right: 1px solid rgb(202, 202, 202);
    float: none;
    border-radius: 0;
}
       .orgchart .lines .topLine {
    border-top: 2px solid rgb(202, 202, 202);
}
        .orgchart .lines .downLine {
    background-color: rgba(202,202,202);
    margin: 0 auto;
    height: 20px;
    width: 2px;
    float: none;
}
        .orgchart .lines .leftLine {
    border-left: 1px solid rgba(202,202,202);
    float: none;
    border-radius: 0;
}
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
           <div class="container-fluid">
                            <div class="row">
             <!--List--->
                                <div class="col-md-6">
                                    
                                     <!-- Basic datatable -->
                            <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Territory</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                         <a href="Territory.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                   
                           
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Territory Id</th>
                                        <th>Territory Name</th>
                                        <th> Report to Territory</th>
                                        <th>Created On</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_TerritoryList" runat="server"></asp:PlaceHolder>
                                        
                                </tbody>
                            </table>
                    </div>
                                 </div>
                        </div>
                        <!-- /basic datatable -->
                  <div class="">
    <%--<ul id="ul-data" style="display: none;">
    <li data-id="n1">Territory 1
        <ul>
        <li data-id="n2">Territory 2</li>
        <li data-id="n3">Territory 3
            <ul>
            <li data-id="n4">Territory 4</li>
                <li data-id="n5">Territory 5
                    <ul>
                    <li data-id="n6">Territory 6</li>
                    <li data-id="n7">Territory 7</li>
                    </ul>
                </li>
            </ul>
        </li>
        </ul>
    </li>
    </ul>--%>
                     <asp:PlaceHolder ID="ph_TreeView" runat="server"></asp:PlaceHolder>



                                        <div class="card ">
                                            <div class="card-header header-elements-inline">
                                                <h5 class="card-title">Territory Tree view</h5>
                                                <div class="header-elements">
                                                    <div class="list-icons">
                                                        <a class="list-icons-item" data-action="collapse"></a>
                                                        <a class="list-icons-item" data-action="reload"></a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="card-body">
                                                <asp:TreeView ID="tv_Territory" runat="server"></asp:TreeView>
                                            <div id="chart-container"></div>
                                        </div>
                    </div>
        
        </div>
            
                                </div>
                                <!--List--->
            
                              
                                <div id="div_InputForm" runat="server"  class="col-md-6">
                                 <!-- Basic layout-->
                                        <div class="card ">
                                        <div class="card-header header-elements-inline">
                                            <h5 class="card-title">Add Territory</h5>
                                            <div class="header-elements">
                                                <div class="list-icons">
                                                

                                                    <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click"  OnClientClick="javascript:return ValidateTerritoryId()"  runat="server" CssClass="btn btn-primary  btn-sm " />
                                                    <asp:Button ID="btn_Update" Text="Update" Visible="false"  OnClientClick="javascript:return ValidateTerritoryId()"  OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                                    <a class="list-icons-item" data-action="collapse"></a>
                                                    <a class="list-icons-item" data-action="reload"></a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                
                                        <div class="card-body">
                                            
                                            <div class="form-group ">
                                                <label>Territory ID<span class="text-danger">*</span></label>
                                                <input onfocusOut="ValidateTerritoryId()" id="txt_TerritoryId" required runat="server" type="text" class="form-control" >
                                            </div>
                                            <div class="form-group">
                                                <label>Territory Name<span class="text-danger">*</span></label>
                                                <input id="txt_TerritoryName" required runat="server" type="text" class="form-control" >
                                            </div>
                                           
                                            <div class="form-group">
                                                <label>Reporting Territory</label>
                                                <asp:DropDownList CssClass="form-control" ID="ddl_ReportingTerritory" runat="server"></asp:DropDownList>
                                               <%-- <select id="dd_ReportingTerritory" runat="server" class="form-control">
                                                    <option value="">-- select one --</option>
                                                    <option value="E23CC35D-92D4-45DB-9455-04608B6EB21E">Balaji</option>
                                                    <option value="A34CC55D-93D4-45DB-9455-04608A6E421E"> Ranjith</option>
                                                    <option value="B34CC55D-93D4-45DB-9456-04608A7E421A">Harry</option>
                                                    <option value="C34CC95D-32D4-45DB-9455-04608A6E421E">Hari</option>
                                                </select>--%>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                               
                                <!-- /basic layout -->
                                </div>


                                 <!--View Data-->
                                 <div id="div_TerritoryDetails" runat="server"  class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card" id="card-territory">
                            
                                      <asp:PlaceHolder ID="ph_TerritoryDetails" runat="server"></asp:PlaceHolder>
                                
                                    </div>
                                
                                </div>
                                <!--view data-->
            
                            </div>
                        </div>
    
                       <script type="text/javascript">
            $(function() {
            var datascource = {
              'name': 'Lao Lao',
              'title': 'general manager',
              'children': [
                { 'name': 'Bo Miao', 'title': 'department manager',
                  'children': [{ 'name': 'Li Xin', 'title': 'senior engineer' }]
                },
                { 'name': 'Su Miao', 'title': 'department manager',
                  'children': [
                    { 'name': 'Tie Hua', 'title': 'senior engineer' },
                    { 'name': 'Hei Hei', 'title': 'senior engineer',
                      'children': [
                        { 'name': 'Pang Pang', 'title': 'engineer' },
                        { 'name': 'Dan Dan', 'title': 'UE engineer' }
                      ]
                    }
                  ]
                },
                { 'name': 'Hong Miao', 'title': 'department manager' }
              ]
            };
            var oc = $('#chart-container').orgchart({
                'data' : $('#ul-data'),
      
      'draggable': true
            });
            oc.$chart.on('nodedrop.orgchart', function(event, extraParams) {
              console.log('draggedNode:' + extraParams.draggedNode.children('.title').text()
                + ', dragZone:' + extraParams.dragZone.children('.title').text()
                + ', dropZone:' + extraParams.dropZone.children('.title').text()
              );

              var hierarchy = oc.getHierarchy();
    console.log(hierarchy);
    
            });
          });

          </script>
    <asp:HiddenField ID="hdn_TerritoryId" runat="server" />
        
</asp:Content>

