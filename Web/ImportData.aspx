﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="ImportData.aspx.cs" ClientIDMode="Static" Inherits="Web_ImportData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Import Data</title>
     <style>
        .b-l-1{
            border-right: 1px solid rgb(218, 216, 216);
        }
        
       #addimport>tbody>tr>th:nth-child(1),#addimport>tbody>tr>td:nth-child(1),#addimport>tbody>tr>td:nth-child(2){
background-color:#039BE5;
        color:white;
}

#addimport>tbody>tr>th:nth-child(2),#addimport>tbody>tr>td:nth-child(3),#addimport>tbody>tr>td:nth-child(4),#addimport>tbody>tr>td:nth-child(5){
background-color:#4FC3F7;
        color:white;
}
    </style>
    <script type="text/javascript">

        function loadTableSchema() {
            var ExcelColumns = $('#ddl_ExcelSheets').val();
            var DestinationTable = $('#ddl_DestinationTable').val();
            getTableSchemaFields(DestinationTable, ExcelColumns);
        }

        function getTableSchemaFields(Destination, source) {
            
            //to get the fields
                $.ajax({
                    type: "POST",
                    url: "../Web/ImportData.aspx/getTableSchema",
                    data: "{Destination:'" + Destination + "',source:'" + source + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d != "") {
                           // alert(data.d);
                            $('#ImportDataSchemaSelect').after(data.d);
                        }
                        else {
                            alert("Error : Please try again");
                        }

                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            
          
        }
        function appendMappingArray() {
            var MapArray = {};

            $('.MapColums').each(function () {
                if (this.value != '') {
                    MapArray[this.id] = this.value;
                }
               
            });
            var stringMap = JSON.stringify(MapArray);
            $('#hdn_MappingArray').val(stringMap);
            //console.log(stringMap);
        }

        function getSheetData(source) {
            //to get the excel sheet data
            $.ajax({
                type: "POST",
                url: "../Web/ImportData.aspx/getTableSchema",
                data: "{Destination:'" + Destination + "',source:'" + source + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (data.d != "") {
                        // alert(data.d);
                        $('#ImportDataSchemaSelect').after(data.d);
                    }
                    else {
                        alert("Error : Please try again");
                    }

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });

        }
    </script>
  </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                 <!--Basic form panel--->
                                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"><asp:Label ID="lbl_ImportName" runat="server"></asp:Label></h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <asp:Button ID="btn_UploadData" runat="server" Text="Upload" CssClass="btn btn-primary" OnClick="btn_UploadData_ServerClick" OnClientClick="appendMappingArray()" />
                                       <%-- <button id="btn_UploadData" onclick="appendMappingArray()" runat="server" onserverclick="btn_UploadData_ServerClick">Upload</button>--%>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                             <div class="table-responsive" style="border-radius: 10px;">
                                                    <table class="table" id="addimport">
                                                        
                                                     <tr>
                                                        <th colspan="2">
                                                            <div class="form-group ">
                                                            <label>Destination</label>
                                                                <asp:DropDownList  ID="ddl_DestinationTable" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="" Text="-Select Destination-"></asp:ListItem>
                                                                    <asp:ListItem Value="Recipients" Text="Recipients"></asp:ListItem>
                                                                    <asp:ListItem Value="v_ImportTranaction" Text="Transactions"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            
                                                            </div>
                                                        </th>
                                                        <th colspan="1">
                                                            <div class="form-group">
                                                                <label>Source</label>
                                                                <asp:DropDownList onchange="loadTableSchema()" ID="ddl_ExcelSheets" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="" Text="-Select Destination-"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                
                                                            </div>
                                                        </th>
                                                      </tr>
                                                        <!--Heading tr-->
                                                      <tr id="ImportDataSchemaSelect">
                                                        <td>Field Name </td>
                                                        <td>Data Type</td>
                                                        <td>
                                                            Filed Name
                                                        </td>
                                                      </tr>
                                                        <!--/Heading tr-->
                                                      <!-- /field id-->
                                                        <!--Dynamic tr-->
                                                        
                                                     <%-- <tr>
                                                        <td>Field 1</td>
                                                        <td>int</td>
                                                        <td>
                                                            
                                                                <div class="form-group">
                                                            
                                                            <select class="form-control">
                                                                <option>-- select name --</option>
                                                                <option>Balaji</option>
                                                                <option>Rajesh</option>
                                                                <option>Ranjith</option>
                                                                <option>Sandy</option>
                                                                <option>John</option>
                                                            </select>
                                                    
                                                              
                                                            </div>
                                                        </td>
                                                       
                                                       
                                                      </tr>
                                                        <tr>
                                                        <td>Field 2</td>
                                                        <td>Varchar</td>
                                                        <td>
                                                            <div class="form-group">
                                                               
                                                                <select class="form-control">
                                                                    <option>-- select address --</option>
                                                                    <option>Flat No 5, Tharamani Road, Chennai</option>
                                                                    <option>Flat No 3, Velachery Road, Chennai</option>
                                                                    <option>Block No 2, Guindy Road, Chennai</option>
                                                                    <option>Flat No 3, CMBT Road, Chennai</option>
                                                                    <option>Flat No 5, OMR Road, Chennai</option>
                                                                </select>
                                                                
                                                            </div>
                                                        </td>
                                                       
                                                      </tr>
                                                        <tr>
                                                        <td>Field 3</td>
                                                        <td>DateTime</td>
                                                        <td>
                                                            <div class="form-group">
                                                                
                                                                <select class="form-control">
                                                                    <option>-- select zipcode --</option>
                                                                    <option>600057</option>
                                                                    <option>600042</option>
                                                                    <option>600025</option>
                                                                    <option>600012</option>
                                                                    <option>600020</option>
                                                                </select>
                                                                
                                                            </div>
                                                        </td>
                                                        
                                                      </tr>--%>
                                                        <!--/Dynamic tr-->
                                                    </table>
                                            </div>
                                        </div>
                                    </div>
                                <!-- /basic layout -->
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdn_MappingArray" runat="server"  />
</asp:Content>

