﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_FunctionMatrix : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        if (!IsPostBack)
        {
            string RuleKey = Com.GetQueryStringValue("RuleKey");
            hdn_RuleKey.Value = RuleKey;

            string MatrixKey = Guid.NewGuid().ToString();
            hdn_MatrixKey.Value = MatrixKey;
            if (Com.GetQueryStringValue("isEdit") == "1")
            {
                hdn_MatrixKey.Value = Com.GetQueryStringValue("FunctionKey");
                lbl_UpdateMsg.Visible = true;

                loadMatrix(Com.GetQueryStringValue("FunctionKey"));
            }
        }
        
    }

    private void loadMatrix(string MatrixKey)
    {
        string str_sql = "select * from Matrix where MatrixKey=@MatrixKey";
        SqlCommand sqlcmd = new SqlCommand(str_sql);
        sqlcmd.Parameters.AddWithValue("@MatrixKey", MatrixKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
       

        foreach (DataRow dr in dt.Rows)
        {
            txt_MatrixName.Value = dr["MatrixName"].ToString();
            txt_RowParameter.Value = dr["RowHeaderText"].ToString();
            txt_ColumnParameter.Value = dr["ColumnHeaderText"].ToString();
            txt_MatrixRow.Value = dr["RowCountNo"].ToString();
            txt_MatrixColumn.Value = dr["ColumnCountNo"].ToString();

            //MatrixName, MatrixTypeId, RowHeaderText, RowCountNo, EnableRowRange, ColumnHeaderText, ColumnCountNo, EnableColumnRange,

            if (dr["EnableRowRange"].ToString()=="True")
            {
                chb_EnableRowRange.Checked = true;
            }
            else
            {
                chb_EnableRowRange.Checked = false;
            }
            if (dr["EnableColumnRange"].ToString() == "True")
            {
                chb_EnableColumnRange.Checked = true;
            }
            else
            {
                chb_EnableColumnRange.Checked = false;
            }

        }
        MatrixUI(MatrixKey);
    }

    protected void btn_CreateMatrix_ServerClick(object sender, EventArgs e)
    {
        string MatrixKey = hdn_MatrixKey.Value;
        string MatrixName = txt_MatrixName.Value;
        string RowHeaderText = txt_RowParameter.Value;
        string ColumnHeaderText = txt_ColumnParameter.Value;
        string RowCountNo = txt_MatrixRow.Value;
        string ColumnCountNo = txt_MatrixColumn.Value;
        bool EnableRowRange = false;
        if (chb_EnableRowRange.Checked == true)
        {
            EnableRowRange = true;
        }
        bool EnableColumnRange = false;
        if (chb_EnableColumnRange.Checked == true)
        {
            EnableColumnRange = true;
        }
      
        InsertMatrixDetails(MatrixKey, MatrixName, RowHeaderText, ColumnHeaderText, RowCountNo, ColumnCountNo, EnableRowRange, EnableColumnRange);

       
    }

    private void MatrixUI(string matrixKey)
    {
       

        //for column ranges
        String TrColumnRange = ColumnRangeTr(matrixKey);

        String TrRowRangeWithValues = RowRangeWithValuesTr(matrixKey);

        String FullTBody = TrColumnRange + TrRowRangeWithValues;
        ph_MatrixTr.Controls.Add(new LiteralControl(FullTBody));
          btn_CreateMatrix.Visible = false;
        btn_Updatematrix.Visible = true;
        MatrixSave.Visible = true;
    }

    public string RowRangeWithValuesTr(string MatrixKey)
    {
        string BulkTr = "";

        string sql_qry = "select a.*, b.ColumnHeaderText, b.RowHeaderText,b.EnableColumnRange, b.EnableRowRange from MatrixRange a join Matrix b on a.MatrixKey=b.MatrixKey where a.MatrixKey=@MatrixKey and a.RangeType=1 order by rangetypeid;" +
            "select a.*, b.ColumnHeaderText, b.RowHeaderText,b.EnableColumnRange, b.EnableRowRange from MatrixRange a join Matrix b on a.MatrixKey=b.MatrixKey where a.MatrixKey=@MatrixKey and a.RangeType=2 order by rangetypeid;" +
            "select * from MatrixValues where MatrixKey=@MatrixKey  ";
        SqlCommand cmd = new SqlCommand(sql_qry);
        cmd.Parameters.AddWithValue("MatrixKey", MatrixKey);
        DataSet ds = DA.GetDataSet(cmd);

        string Tr = "";

        string RowWithRangeTemplate= PHT.ReadFileToString("TdMatrixRowWithRange.txt");
        string RowWithoutRangeTemplate = PHT.ReadFileToString("TdMatrixRowWithoutRange.txt");
        string RangeValuesTemplate= PHT.ReadFileToString("TdMatrixValue.txt");


        foreach(DataRow dr1 in ds.Tables[0].Rows)
        {
            string SingleTr = "<tr><td></td>";
            string RowRangeKey = dr1["RangeKey"].ToString();
            if(dr1["EnableRowRange"].ToString() == "True")
            {
                SingleTr += PHT.ReplaceVariableWithValue(dr1, RowWithRangeTemplate);
            }
            else
            {
                SingleTr += PHT.ReplaceVariableWithValue(dr1, RowWithoutRangeTemplate);
            }

            foreach(DataRow dr2 in ds.Tables[1].Rows)
            {
                string ColumnRangeKey = dr2["RangeKey"].ToString();
                
                foreach(DataRow dr3 in ds.Tables[2].Select("RangeRowKey='" + RowRangeKey + "' AND RangeColumnKey='"+ ColumnRangeKey + "' "))
                {
                    SingleTr += PHT.ReplaceVariableWithValue(dr3, RangeValuesTemplate);
                }
               
            }

            SingleTr += "</td>";
            Tr += SingleTr;
        }
        return Tr;
    }
    public  string ColumnRangeTr(string MatrixKey)
    {
       //for column range UI 
        string sql_qry = "select a.*, b.ColumnHeaderText, b.RowHeaderText,b.EnableColumnRange, b.EnableRowRange from MatrixRange a join Matrix b on a.MatrixKey=b.MatrixKey where a.MatrixKey=@MatrixKey and a.RangeType=2 order by rangetypeid ";
        SqlCommand cmd = new SqlCommand(sql_qry);
        cmd.Parameters.AddWithValue("MatrixKey", MatrixKey);
        DataTable dt = DA.GetDataTable(cmd);

        string ColumnTr = "<tr><td colspan='2'></td>";

        string ColumnWithRangeTemplate =PHT.ReadFileToString("TdMatrixColumnWithRange.txt");
        string ColumnWithoutRangeTemplate = PHT.ReadFileToString("TdMatrixColumnWithoutRange.txt");
        foreach (DataRow dr in dt.Rows)
        {
            if ( dr["EnableColumnRange"].ToString() == "True")
            {
                ColumnTr += PHT.ReplaceVariableWithValue(dr, ColumnWithRangeTemplate);
            }
            else
            {
                ColumnTr += PHT.ReplaceVariableWithValue(dr, ColumnWithoutRangeTemplate);
            }
            
        }

        ColumnTr += "</tr>";

        return ColumnTr;
    }
   
    private void InsertMatrixDetails(string matrixKey, string matrixName, string rowHeaderText, string columnHeaderText, string rowCountNo, string columnCountNo, bool enableRowRange, bool enableColumnRange)
    {
        //Matrix (MatrixKey, MatrixName, MatrixTypeId, RowHeaderText, RowCountNo, EnableRowRange, ColumnHeaderText, ColumnCountNo, EnableColumnRange, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)


        string str_Sql = "insert into Matrix (MatrixKey, MatrixName, RowHeaderText, RowCountNo, EnableRowRange, ColumnHeaderText, ColumnCountNo, EnableColumnRange,CreatedOn, CreatedBy)"
                               + "select @MatrixKey, @MatrixName, @RowHeaderText, @RowCountNo, @EnableRowRange, @ColumnHeaderText, @ColumnCountNo, @EnableColumnRange, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@MatrixKey", matrixKey);
        cmd.Parameters.AddWithValue("@MatrixName", matrixName);
        cmd.Parameters.AddWithValue("@RowHeaderText", rowHeaderText);
        cmd.Parameters.AddWithValue("@RowCountNo", rowCountNo);
        cmd.Parameters.AddWithValue("@EnableRowRange", enableRowRange);
        cmd.Parameters.AddWithValue("@ColumnHeaderText", columnHeaderText);
        cmd.Parameters.AddWithValue("@ColumnCountNo", columnCountNo);
        cmd.Parameters.AddWithValue("@EnableColumnRange", enableColumnRange);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);

        GenearateMatrix(matrixKey);
    }

    private void GenearateMatrix(string matrixKey)
    {
        string str_Sql = "p_GenerateMatrix";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@MatrixKey", matrixKey);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        cmd.CommandType = CommandType.StoredProcedure;
        DA.ExecuteNonQuery(cmd);
        MatrixUI(matrixKey);
    }

    [WebMethod]
    public static string UpdateRange(string RangeKey,string ColumnName,string Value)
    {
        try
        {
            //MatrixRange (RangeKey, MatrixKey, RangeType, RangeFrom, RangeTo, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();


            string str_Sql = "Update MatrixRange set  "+ColumnName+"=@"+ColumnName+ ", ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where RangeKey=@RangeKey ";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@RangeKey", RangeKey);
            cmd.Parameters.AddWithValue("@"+ ColumnName +"",Value);
            cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return "Range Updated";

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
        
    }
    [WebMethod]
    public static string UpdateRangeValue(string ValueKey, string Value)
    {
        try
        {
            //MatrixValues (ValueKey, MatrixKey, RangeRowId, RangeColumnId, RangeRowKey, RangeColumnKey, RangeValue, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
            DataAccess DA = new DataAccess();
            SessionCustom SC = new SessionCustom();


            string str_Sql = "Update MatrixValues set  RangeValue=@RangeValue, ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where ValueKey=@ValueKey ";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@RangeValue", Value);
            cmd.Parameters.AddWithValue("@ValueKey", ValueKey);
            cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

            cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

            DA.ExecuteNonQuery(cmd);

            return "Range Value Updated";

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    protected void btn_CancelMatrix_ServerClick(object sender, EventArgs e)
    {
          

            string MatrixKey = hdn_MatrixKey.Value;

            string str_Sql = "delete from Matrix where MatrixKey=@MatrixKey; delete from MatrixRange where MatrixKey=@MatrixKey; Delete from MatrixValues where MatrixKey=@MatrixKey";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@MatrixKey", MatrixKey);
            

            DA.ExecuteNonQuery(cmd);

    }

    protected void btn_Updatematrix_ServerClick(object sender, EventArgs e)
    {
       
        //Matrix (MatrixKey, MatrixName, MatrixTypeId, RowHeaderText, RowCountNo, EnableRowRange, ColumnHeaderText, ColumnCountNo, EnableColumnRange, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        
        bool EnableRowRange = false;
        if (chb_EnableRowRange.Checked == true)
        {
            EnableRowRange = true;
        }
        bool EnableColumnRange = false;
        if (chb_EnableColumnRange.Checked == true)
        {
            EnableColumnRange = true;
        }

        string str_Sql = "Update  Matrix  set  MatrixName=@MatrixName, RowHeaderText=@RowHeaderText, RowCountNo=@RowCountNo, EnableRowRange=@EnableRowRange, ColumnHeaderText=@ColumnHeaderText, ColumnCountNo=@ColumnCountNo, EnableColumnRange=@EnableColumnRange,ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where MatrixKey=@MatrixKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@MatrixKey", hdn_MatrixKey.Value);
        cmd.Parameters.AddWithValue("@MatrixName", txt_MatrixName.Value);
        cmd.Parameters.AddWithValue("@RowHeaderText", txt_RowParameter.Value);
        cmd.Parameters.AddWithValue("@RowCountNo", txt_MatrixRow.Value);
        cmd.Parameters.AddWithValue("@EnableRowRange", EnableRowRange);
        cmd.Parameters.AddWithValue("@ColumnHeaderText", txt_ColumnParameter.Value);
        cmd.Parameters.AddWithValue("@ColumnCountNo", txt_MatrixColumn.Value);
        cmd.Parameters.AddWithValue("@EnableColumnRange", EnableColumnRange);
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);
        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);

        GenearateMatrix(hdn_MatrixKey.Value);
    }
}