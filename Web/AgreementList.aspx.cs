﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;


public partial class Web_AgreementList : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        loadAgreementList();
        if (Request.QueryString["agreement_key"] != null)
        {
            viewAgreementDetails(Request.QueryString["agreement_key"]);
            div_AgreementDetails.Visible = true;
            
        }
    }
    private void viewAgreementDetails(string AgreementKey)
    {
        string str_Sql_view = "select * from Agreement where AgreementKey =@AgreementKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@AgreementKey", AgreementKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_AgreementDetails, "DivAgreementView.txt", "");
    }
    private void loadAgreementList()
    {
        string str_Sql = "select AgreementKey,AgreementName,IsDefault, FORMAT(CreatedOn,'MM/dd/yyyy') as CDate, b.UserName from Agreement a left join v_UserDetails b on a.CreatedBy=b.UserKey order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_AgreementListTr, "AgreementListTr.txt", "");
    }

    [WebMethod]
    public static string deleteAgreement(string AgreementKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Agreement where AgreementKey=@AgreementKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@AgreementKey", AgreementKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
}