﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master"  ClientIDMode="Static" AutoEventWireup="true" CodeFile="TransactionCustomConfig.aspx.cs" Inherits="Web_TransactionCustomConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Transaction Custom Config</title>
    <script type="text/javascript">
        function DdlVisible(val) {
            let type = parseInt($(val).val());
            if (type == 1) {
                $('#OptionDdl').addClass('d-none');
            }
            else {
                $('#OptionDdl').removeClass('d-none');
            }
        }

        function deleteField(FieldKey, FieldName, ColumnName) {
            let ParentTable = $('#hdn_ParentTable').val();
            var confirmation = confirm("are you sure want to delete this " + FieldName + " Column ?");
                if (confirmation == true) {
                    //to delete
                    $.ajax({
                        type: "POST",
                        url: "../Web/TransactionCustomConfig.aspx/deleteField",
                        data: "{ParentTable:'" + ParentTable + "', FieldKey:'" + FieldKey + "',ColumnName:'" + ColumnName + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            if (data.d = "DeleteSuccess") {
                                alert("Column " + FieldName + " in Custom Table deleted successfully")
                                location.reload();
                            }
                            else {
                                alert("Error : Please try again");
                            }

                        },
                        error: function () {
                            alert('Error communicating with server');
                        }
                    });
                }
                else {
                    return false
                }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="container-fluid">
                            <div class="row">
                               
                                <div class="col-lg-6 col-md-6">
                                   
                    
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Transactions  Custom
                                    
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                       
                                        <a href="Transaction.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                            
                             
                            <div class="table-responsive">
                           
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Label Name</th>
                                        <th>Column Name</th>
                                        <th>Data Type</th>
                                       
                                        <th>Field Type</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_CustomColumnList" runat="server"></asp:PlaceHolder>
                                </tbody>
                            </table>
                    </div>
                        </div>
                        </div>
                     
                                </div>
                               
                                 <div id="div_InputForm" runat="server" class="col-lg-6 col-md-6">
                                      
                                    <div class="card ">
                            <div class="card-header header-elements-inline ">
                                <h5 class="card-title">Transaction Custom</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       <button type="button" class="btn btn-primary btn-sm" id="btn_Save" runat="server" onserverclick="btn_Save_ServerClick">Save</button>
                                      <%--  <button type="button" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> Save</button>--%>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a  class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Field type</label>
                                                    <asp:DropDownList CssClass="form-control" onchange="DdlVisible(this);" ID="ddl_FieldType" runat="server">
                                                        <asp:ListItem Text="-select field type-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Text Box" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Drop Down" Value="2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                 <div class="form-group col-md-3">
                                                     <label>Required Field</label>
                                                     <input type="checkbox" class="form-control" id="chb_IsRequired" runat="server" />
                                                 </div>
                                                <div class="form-group col-md-3">
                                                     <label>Sequence </label>
                                                     <input type="text" class="form-control" id="txt_Sequence" runat="server" />
                                                 </div>
                                                <div class="form-group col-md-6">
                                                    <label>Label Name</label>   
                                                    <input type="text" id="txt_LabelName" class="form-control" runat="server" />
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>ColumnName</label>
                                                    <input type="text" id="txt_ColumnName" class="form-control" runat="server" />
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Data Type</label>
                                                     <asp:DropDownList CssClass="form-control" ID="ddl_DataType" runat="server">
                                                        
                                                    </asp:DropDownList>
                                                </div>
                                                 <div class="form-group col-md-6" id="OptionDdl">
                                                    <label>Select  DropDown Options from Config</label>
                                                     <asp:DropDownList CssClass="form-control" ID="ddl_DdlList"  runat="server">
                                                       
                                                    </asp:DropDownList>
                                                 </div>
                                                
                                             </div>
                                        </div>
                                    </div>
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->

                                 <!--View Data-->
                                 <div id="div_TransactionDetails" runat="server"  class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card" id="card-transaction">
                            
                                <asp:PlaceHolder ID="ph_Transaction_Details" runat="server"></asp:PlaceHolder>
                                
				              
                                    </div>
                                
                                </div>
                                <!--view data-->
                            </div>
                        </div>
    <asp:HiddenField ID="hdn_ParentTable" runat="server" ClientIDMode="Static" />
</asp:Content>

