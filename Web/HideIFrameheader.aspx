﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="HideIFrameheader.aspx.cs" Inherits="Web_HideIFrameheader" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Iframe Header</title>
    <style>
        .Custom-Iframe{
            height:75vh;
        }
        
        .iframe-container{
            position:relative;
        }
        .div-header{
             position:absolute;
            top:0;
            left:0;
            width:100%;
            height:80px;
            background: Gray;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <a class="btn btn-primary btn-sm">Link 1</a>
                <a class="btn btn-primary btn-sm">Link 2</a>
                <a class="btn btn-primary btn-sm">Link 3</a>
                <a class="btn btn-primary btn-sm">Link 4</a>
            </div>
            <div class="col-md-10 " >
                <div class="iframe-container">

                
                <iframe id="Iframe" src="https://mobile.spectrum.com/products?tab=phones&page=1&sortBy=price-asc" width="100%" class="Custom-Iframe" ></iframe>
                <div class="div-header "></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

