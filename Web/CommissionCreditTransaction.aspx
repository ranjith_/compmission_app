﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" AutoEventWireup="true" CodeFile="CommissionCreditTransaction.aspx.cs" Inherits="Web_CommissionCreditTransaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
        <style>
        .custom-panel{
    background-color: #80808040;
    padding: 15px;
    border-radius: 5px;
        }
        .btn-xx{
            padding: .3em 0.4em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
   	<div class="custom-panel">
                                 <h6>Credit the transaction based on following criteria</h6>
                                   <div  class="row">
                                              
                                                <div class="form-group col-md-6">
                                                    <label>Name<span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control required" >
                                                </div>
                                                <div class="form-group col-md-6 text-right">
                                                    <label> </label>
                                                    <input type="button" value="More .." class="btn btn-primary btn-sm pull-right" >
                                                </div>
                                        </div>
                                    <div class="row">
                                                 <div class="col-md-12 custom-panel">
                                                  <div class="source">
                                                  <p>Source (Configured table names)</p>
                                                    <table class="table">
                                                        <tbody>
                                                <!--  source added --->
                                                            <tr>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>Combo 1</option>
                                                                        <option>Combo 2</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>Combo 1</option>
                                                                        <option>Combo 2</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>equal </option>
                                                                        <option>not equal</option>
                                                                        <option>Greater </option>
                                                                        <option>Lesser</option>
                                                                    </select>
                                                                </td>
                                                                <td> <input type="text" class="form-control"></td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>OR</option>
                                                                        <option>AND</option>
                                                                    </select>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                <!-- / source added --->
                                                <!--  source added --->
                                                            <tr>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>Combo 1</option>
                                                                        <option>Combo 2</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>Combo 1</option>
                                                                        <option>Combo 2</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>equal </option>
                                                                        <option>not equal</option>
                                                                        <option>Greater </option>
                                                                        <option>Lesser</option>
                                                                    </select>
                                                                </td>
                                                                <td> <input type="text" class="form-control"></td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>OR</option>
                                                                        <option>AND</option>
                                                                    </select>
                                                                </td>
                                                                <td><button class="btn btn-primary btn-xs">More ..</button></td>
                                                            </tr>
                                                <!-- / source added --->
                                                        </tbody>
                                                    </table>
                                               </div>
                                            </div>
                                            </div>
                                          
                                    </div>
</asp:Content>

