﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="PayPeriod.aspx.cs" Inherits="Web_PayPeriod" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Pay Period</title>
    <style>
        #Period >thead>tr>th:nth-child(4),#Period >tbody>tr>td:nth-child(4){
        background-color:#4FC3F7;
        }
        #Period >thead>tr>th:nth-child(5),#Period >tbody>tr>td:nth-child(5){
        background-color:#AED581;
        }
        #Period >thead>tr>th:nth-child(6),#Period >tbody>tr>td:nth-child(6){
        background-color:#FFB74D;
        }
        #Period >thead>tr>th:nth-child(7),#Period >tbody>tr>td:nth-child(7){
        background-color:#4DB6AC;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
             <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-6 col-md-6">
                                    <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Calender</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                   
                                        <div id="accordion-group1" class="panel-collapse collapse ">
                                                <div class="panel-body">
                                                        The <code>DataTables</code> is a highly flexible tool, based upon the foundations of progressive enhancement, 
                                                        and will add advanced interaction controls to any HTML table.  <strong>Datatables support all available table styling.</strong>
                                                </div>
                                            </div>
                                
                            </div>
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Calendar Name</th>
                                        <th>Description</th>
                                        <th>Fiscal Year</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Pay Frequency</th>
                                        
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   
                                        <tr>
                                            <td>Name 1</td>
                                            <td>Descriptionn 1</td>
                                            <td>2019</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>86</td>
                                        </tr>
                                        <tr>
                                            <td>Name 2</td>
                                            <td>Descriptionn 2</td>
                                            <td>2017</td>
                                            <td>03-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>66</td>
                                        </tr>
                                        <tr>
                                            <td>Name 2</td>
                                            <td>Descriptionn 2</td>
                                            <td>2018</td>
                                            <td>03-06-2019</td>
                                            <td>08-06-2019</td>
                                            <td>105</td>
                                        </tr>
                                        <tr>
                                            <td>Name 1</td>
                                            <td>Descriptionn 1</td>
                                            <td>2014</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>86</td>
                                        </tr>
                                        <tr>
                                            <td>Name 2</td>
                                            <td>Descriptionn 2</td>
                                            <td>2015</td>
                                            <td>03-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>66</td>
                                        </tr>
                                <tr>
                                            <td>Name 1</td>
                                            <td>Descriptionn 1</td>
                                            <td>2019</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>86</td>
                                        </tr>
                                        <tr>
                                            <td>Name 2</td>
                                            <td>Descriptionn 2</td>
                                            <td>2017</td>
                                            <td>03-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>66</td>
                                        </tr>
                                        <tr>
                                            <td>Name 2</td>
                                            <td>Descriptionn 2</td>
                                            <td>2018</td>
                                            <td>03-06-2019</td>
                                            <td>08-06-2019</td>
                                            <td>105</td>
                                        </tr>
                                        <tr>
                                            <td>Name 1</td>
                                            <td>Descriptionn 1</td>
                                            <td>2014</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>86</td>
                                        </tr>
                                        <tr>
                                            <td>Name 2</td>
                                            <td>Descriptionn 2</td>
                                            <td>2015</td>
                                            <td>03-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>66</td>
                                        </tr>
                                        
                                       
                                </tbody>
                            </table>
                    </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                 <!--Input Form-->
                                 <div class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                            
                                    <div class="card ">
                                        <div class="card-header header-elements-inline">
                                            <h5 class="card-title">Add Period</h5>
                                            <div class="header-elements">
                                                <div class="list-icons">
                                                    <a class="list-icons-item" data-action="collapse"></a>
                                                    <a class="list-icons-item" data-action="reload"></a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Calendar Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="2"></textarea>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Fascal Year</label>
                                                <select class="form-control">
                                                <option>-- select one --</option>
                                                    <option>2018</option>
                                                    <option>2016</option>
                                                    <option>2019</option>
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-md-6">
                                                <label>Start Date<span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>End Date<span class="text-danger">*</span></label>
                                                <input type="date" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pay Frequency<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" >
                                            </div>
                                            
    
                                            <div class="text-right col-md-12 ">
                                                <button type="submit" class="btn btn-primary"> ADD</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                               
                                        <div class="card ">
                                            <div class="card-header header-elements-inline">
                                                <h5 class="card-title">Calender</h5>
                                                <div class="header-elements">
                                                    <div class="list-icons">
                                                        <a class="list-icons-item" data-action="collapse"></a>
                                                        <a class="list-icons-item" data-action="reload"></a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="card-body">
                                            
                                           <div class="table-responsive">
                            <table class="table table-bordered " id="Period">
                                <thead>
                                    <tr>
                                        <th>Period Number</th>
                                        <th>Period Start Date</th>
                                        <th>Period End Date</th>
                                        <th>Monthly Frequency</th>
                                        <th>Quarterly Frequency</th>
                                        <th>Half Yearly Frequency</th>
                                        <th>Annual Frequency</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   
                                        <tr>
                                            <td> 1</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>43</td>
                                            <td>86</td>
                                            <td>83</td>
                                            <td>36</td>
                                        </tr>
                                        <tr>
                                            <td> 2</td>
                                            <td>03-06-2019</td>
                                            <td>08-06-2019</td>
                                            <td>43</td>
                                            <td>76</td>
                                            <td>93</td>
                                            <td>34</td>
                                        </tr>
                                        <tr>
                                            <td> 3</td>
                                            <td>11-06-2019</td>
                                            <td>16-06-2019</td>
                                            <td>48</td>
                                            <td>76</td>
                                            <td>89</td>
                                            <td>46</td>
                                        </tr>
                                        <tr>
                                            <td> 4</td>
                                            <td>01-06-2019</td>
                                            <td>06-06-2019</td>
                                            <td>43</td>
                                            <td>86</td>
                                            <td>83</td>
                                            <td>36</td>
                                        </tr>
                                        <tr>
                                            <td> 5</td>
                                            <td>01-06-2019</td>
                                            <td>09-06-2019</td>
                                            <td>83</td>
                                            <td>66</td>
                                            <td>33</td>
                                            <td>76</td>
                                        </tr>
                                       
                                </tbody>
                            </table>
                    </div>
                                        </div>
                                    </div>
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->
                            </div>
                        </div>
</asp:Content>

