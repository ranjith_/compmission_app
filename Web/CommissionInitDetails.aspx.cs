﻿        using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Text;
using System.IO;

public partial class Web_CommissionInitDetails : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        if (!IsPostBack)
        {
            loadMasterFrequency();
            if (Com.GetQueryStringValue("CommissionKey") != null & Com.GetQueryStringValue("CommissionKey") != "")
            {
                loadCommissionInitDetailsToEdit(Com.GetQueryStringValue("CommissionKey"));
                btn_SaveCommissionInitDetails.Visible = false;
                btn_UpdateCommissionInitDetails.Visible = true;
            }
        }
        

    }

    private void loadMasterFrequency()
    {
        string str_Select = "select * from v_MasterFrequency";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(dd_CalFeq, ds.Tables[0], "Text", "Value", true, "-- Select Frequency --");
       
    }

    private void loadCommissionInitDetailsToEdit(string CommissionKey)
    {
        string str_Sql_view = "select *,FORMAT (StartDate, 'yyyy-MM-dd') as sDate,FORMAT (EndDate, 'yyyy-MM-dd') as eDate from Commission where CommissionKey =@CommissionKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {
            //commissionkey, CommissionName, CalFeq, transaction, CreditOf, Percentage, StartDate, EndDate, DescWYSWYG, isField, onAgree,

            txt_CommissionName.Value = dr["CommissionName"].ToString();
            hdn_CommissionName.Value = dr["CommissionName"].ToString();
            dd_CalFeq.SelectedValue = dr["Frequency"].ToString();
            txt_Percentage.Value = dr["CreditPercentage"].ToString();
            dd_CreditOf.Value = dr["CreditOf"].ToString();
            dp_StartDate.Value = dr["sDate"].ToString();
            dp_EndDate.Value = dr["eDate"].ToString();
            txt_DescWYSWYG.InnerHtml = dr["Description"].ToString();
            dd_transaction.Value = dr["CreditField"].ToString();
            if (dr["IsCreditField"].ToString() == "True")
            {
                chb_IsCreditField.Checked = true;
                div_Transaction.Attributes.Add("class", " w-100");
                txt_Percentage.Attributes.Add("class", "form-control required d-none");
            }
            else
            {
                chb_IsCreditField.Checked = false;
                txt_Percentage.Attributes.Add("class", "form-control required");
                div_Transaction.Attributes.Add("class", "d-none w-100");
            }
            if (dr["IsActive"].ToString() == "True")
            {
                chb_CommisssionActive.Checked = true;
            }
            else
            {
                chb_CommisssionActive.Checked = false;
            }
            if (dr["IsOnAggr"].ToString() == "True")
            {
                chb_IsOnAgree.Checked = true;
            }
            else
            {
                chb_IsOnAgree.Checked = false;
            }

        }

     }

    protected void btn_SaveCommissionInitDetails_ServerClick(object sender, EventArgs e)
    {
        try { 
        string commission_key = Guid.NewGuid().ToString();

        string str_Sql = "insert into Commission (CommissionKey, CommissionName, Frequency, CreditPercentage, StartDate, EndDate,Description,IsOnAggr, CreditOf, IsCreditField, CreditField,IsActive, CreatedOn, CreatedBy)"
                            + "select @CommissionKey, @CommissionName,  @Frequency, @CreditPercentage, @StartDate, @EndDate, @Description, @IsOnAggr, @CreditOf, @IsCreditField, @CreditField, @IsActive, @CreatedOn, @CreatedBy;" +
                            "insert into CalculationRuleProcessSetup(ProcessSetupKey,CommissionKey,IsGroupBy,CreatedOn, CreatedBy) values(NEWID(), @CommissionKey, 0,@CreatedOn, @CreatedBy);" +
                            "insert into SchedulePlanCalculation (ScheduleCalculationKey,CommissionKey,CreatedOn, CreatedBy) values(NEWID(),@CommissionKey,@CreatedOn, @CreatedBy)";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", commission_key);
        cmd.Parameters.AddWithValue("@CommissionName", txt_CommissionName.Value);
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
        cmd.Parameters.AddWithValue("@Description", hdn_DescWYSWYG.Value);
      
        cmd.Parameters.AddWithValue("@Frequency", dd_CalFeq.SelectedValue);
        cmd.Parameters.AddWithValue("@CreditPercentage", txt_Percentage.Value);
        cmd.Parameters.AddWithValue("@CreditOf", dd_CreditOf.Value);
        if (chb_IsCreditField.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsCreditField", "1");
            cmd.Parameters.AddWithValue("@", dd_transaction.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsCreditField", "0");
            cmd.Parameters.AddWithValue("@CreditField", "");
        }
        if (chb_CommisssionActive.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsActive", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsActive", "0");
        }
        if (chb_IsOnAgree.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsOnAggr", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsOnAggr", "0");
        }

        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);
       // Response.Redirect("../Web/Commission.aspx?CommissionKey=" + commission_key + "&new=1");
        Response.Redirect("../Web/CommissionView.aspx?CommissionKey=" + commission_key+ "&location=#div_CommissionView&page=Commission-Init-Details&update=success",false);

        }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }
    }

    protected void btn_UpdateCommissionInitDetails_Click(object sender, EventArgs e)
    {
        try
        {
            string str_Sql = "Update  Commission set  CommissionName=@CommissionName, StartDate=@StartDate, EndDate=@EndDate, Description=@Description, " +
               "IsOnAggr=@IsOnAggr, Frequency=@Frequency, CreditPercentage=@CreditPercentage, CreditOf=@CreditOf," +
               " IsCreditField=@IsCreditField, CreditField=@CreditField,  IsActive=@IsActive," +
               " ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where CommissionKey=@CommissionKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CommissionKey", Com.GetQueryStringValue("CommissionKey"));
        cmd.Parameters.AddWithValue("@CommissionName", txt_CommissionName.Value);
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
        cmd.Parameters.AddWithValue("@Description", hdn_DescWYSWYG.Value);

        cmd.Parameters.AddWithValue("@Frequency", dd_CalFeq.SelectedValue);
        cmd.Parameters.AddWithValue("@CreditPercentage", txt_Percentage.Value);
        cmd.Parameters.AddWithValue("@CreditOf", dd_CreditOf.Value);
        if (chb_IsCreditField.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsCreditField", "1");
            cmd.Parameters.AddWithValue("@CreditField", dd_transaction.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsCreditField", "0");
            cmd.Parameters.AddWithValue("@CreditField", "");
        }
        if (chb_CommisssionActive.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsActive", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsActive", "0");
        }
        if (chb_IsOnAgree.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsOnAggr", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsOnAggr", "0");
        }

        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);
        //string script = "window.onload = function() { AlertPopUp(' updated '); };";
        //ClientScript.RegisterStartupScript(this.GetType(), "UpdateTime", script, true);

        Response.Redirect("../Web/CommissionView.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey")+ "&location=#div_CommissionView&page=Commission-Init-Details&update=success",false);

    }
        catch (Exception ex)
        {
            Com.GlobalErrorHandler(ex);
        }
    }


    [WebMethod]
    public static string ValidateCommissionName(string CommissionName)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "select * FROM Commission WHERE CommissionName=@CommissionName";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@CommissionName", CommissionName);
            DataTable dt = DA.GetDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                return "0";
            }
            else
            {
                return "1";
            }

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}