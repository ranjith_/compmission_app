﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="Commission_oldtype.aspx.cs" Inherits="Web_Commission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission</title>
    <script type="text/javascript" src="../JScript/Commission.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>
    <style>
        .custom-panel{
    background-color: #80808040;
    padding: 15px;
    border-radius: 5px;
}
        .btn-xx{
            padding: .3em 0.4em;
        }

/*@media (min-width: 576px){
        .modal-dialog {
            max-width: 630px !important;
        }
}*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    
                        <div class="container-fluid">
                            <div class="row">
                               
                                
                             <!--Input Form-->
                                 <div class="col-lg-12 col-md-12">
                <!-- Basic setup -->
                   <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title d-flex"><i class="icon-tree6"></i>  <span class="ml-2">Commission</span>
                        
                       
                         <div class="form-check form-check-left form-check-switchery form-check-switchery-sm ml-3">
                                        <label class="form-check-label"> ON / OFF
                                            <!--Active -->
                                            <input type="checkbox" class="form-input-switchery"  >
                                        </label>
                                        </div>
                        </h5> 
                                <div class="header-elements">
                                   
                                </div>
                            </div>
                            <div class="card-body">
                               
                            <!--Nav Tabs for commission-->
                            <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0 mr-2 ml-2">
									<li class="nav-item"><a href="#bordered-justified-tab1" class="nav-link active" data-toggle="tab"><span class="text-success"><i class="icon-checkmark2"></i></span> Commission Setup  </a></li>
									<li class="nav-item"><a href="#bordered-justified-tab2" class="nav-link" data-toggle="tab"><span class="text-success"><i class="icon-checkmark2"></i></span> Commission Type</a></li>
                                    <li class="nav-item"><a href="#bordered-justified-tab3" class="nav-link" data-toggle="tab"><span class="text-success"><i class="icon-checkmark2"></i></span> Credit the transaction</a></li>
                                    <li class="nav-item"><a href="#bordered-justified-tab4" class="nav-link" data-toggle="tab">Goal / Range</a></li>
                                    <li class="nav-item"><a href="#bordered-justified-tab5" class="nav-link" data-toggle="tab">Credit Rule</a></li>
                                    <li class="nav-item"><a href="#bordered-justified-tab6" class="nav-link" data-toggle="tab">Assign Recipients</a></li>
                                
									<%--<li class="nav-item dropdown">
										<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Dropdown</a>
										<div class="dropdown-menu dropdown-menu-right">
											<a href="#bordered-justified-tab3" class="dropdown-item" data-toggle="tab">Dropdown tab</a>
											<a href="#bordered-justified-tab4" class="dropdown-item" data-toggle="tab">Another tab</a>
										</div>
									</li>--%>
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0 mr-2 ml-2">
									<div class="tab-pane fade show active" id="bordered-justified-tab1">
                                         <div class="row ">
                                    <div class="form-group col-md-6">
                                        <label>Commission Name<span class="text-danger">*</span></label>
                                        <input  type="text" id="txt_CommissionName" runat="server" class="form-control required" >
                                    </div>
                                    <div class="form-group  col-md-3" >
                                                    <label style="white-space: nowrap; " class="">Effective Date<span class="text-danger">*</span></label>
                                                    <input type="date" id="dp_EffectiveDate" runat="server" required="required" class="form-control required " >
                                    </div>
                                    <div class="form-group col-md-3">
                                            <label>Calculation Feq<span class="text-danger">*</span></label>
                                            <select id="dd_CalFeq" runat="server" class="form-control ">
                                                <option value="">-- select one --</option>
                                                <option value="Weekly">Weekly</option>
                                                <option value="Monthly">Monthly</option>
                                                <option value="Quarterly">Quarterly</option>
                                                <option value="Yearly">Yearly</option>
                                            </select>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                                    <label>Agreement Description</label>
                                                    <textarea id="txt_Description1" runat="server" class="form-control required mb-20" rows="3" ></textarea>
                                    
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                                    
                                        <label >Description</label>
                                        <textarea id="txt_Description2" runat="server" class="form-control required " rows="3" ></textarea>
                                    </div>
                                </div>
										 <div class="row">
                                            
                                            <div class="form-group col-md-6">
                                                
                                                <label>Source of Credit</label>
                                                <div class="d-flex">
                                                    <input id="Text1" runat="server" type="text" class="form-control required">
                                                    <span class="mr-3 ml-3"> % of </span>
                                                    <select id="Select1" runat="server" class="form-control">
                                                        <option value="">-- select one --</option>
                                                        <option value="Sale Amount">Sale Amount</option>
                                                        <option value="Advance">Advance</option>
                                                    </select>
                                                </div>
                                              <%--  <div class="credit row">
                                               
                                                <div class="form-group col-md-4">
                                                    <input id="txt_CreditPercentage" runat="server" type="text" class="form-control required">
                                                </div>
                                                <div class="form-group col-md-1 text-center">
                                                   <label> % of</label>
                                                </div>
                                                <div class="form-group col-md-5">
                                                     <select id="dd_CreditField" runat="server" class="form-control">
                                                        <option value="">-- select one --</option>
                                                        <option value="Sale Amount">Sale Amount</option>
                                                        <option value="Advance">Advance</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-2 d-none">
                                                   <button class="btn btn-primary btn-xs"><i class="icon-plus22"></i></button>
                                                </div>
                                               </div>--%>
                                            </div>    
                                           
                                            
                                            <div class="form-group col-md-6">
                                                    <label>Set Split</label>
                                                    <div class="d-flex">
                                                        <input id="txt_Split" type="text" class="form-control typeahead-split" placeholder="Search Split ">
                                                   <%-- <input placeholder="type to earch split..." type="text" class="form-control required">--%>
                                                    <button type="button" class="btn btn-primary btn-sm ml-2" data-toggle="modal" data-target="#modalSplit"> <i class="icon-menu"></i></button>
                                                    <a data-toggle="collapse" class="btn btn-default btn-sm ml-2 bg-light text-dark collapsed" data-parent="" href="#accordio3"><i class="icon-arrow-down22"></i></a>
                                                    </div>
                                                    
                                    
                                            </div>
                                             <div class="form-group col-md-12 text-right">
                                               <button type="button" onclick="Tab2()" class="btn btn-primary">NEXT</button>
                                             </div>
                                        </div>
									</div>

									<div class="tab-pane fade" id="bordered-justified-tab2">
										  <div class=" panel-body row">
                                 <h6>Select Commision Type</h6>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            
                                            <div class="col-md-2">
                                                <img style="height:150px;" class="img-thumbnail" src="../UploadFile/Image/commission_users.png">
                                            </div>
                                        <div class="col-md-8">
                                                <div class="form-check">
                                                    <h4>
                                                    <label class="form-check-label">
                                                        <input class="mr-2" id="rd_direct" name="commission_type"  type="radio" >Direct
                                                    </label>
                                                    </h4>
                                                </div>
                                                
                                                <p class="ml-4">There are important differences between commissions and fees, at least in the way these words are used to describe  </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <img style="height:150px;" class="img-thumbnail" src="../UploadFile/Image/commission_users.png">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="d-flex">
                                                <div class="form-check ">
                                                    <h4>
                                                    <label class="form-check-label">
                                                      <input class="mr-2" id="rd_ManagerOverride" name="commission_type"  type="radio" >Manager Override
                                                    </label>
                                                    </h4>
                                                </div>
                                                <div class="form-group ml-3 col-md-6  ">
                                                    <input placeholder="Search override" type="text" class="form-control">
                                                </div>
                                                    <div class=" ">
                                                         <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalOverride"> <i class="icon-menu"></i></button>
                                                    </div>
                                                    </div>
                                                <p class="ml-4">There are important differences between commissions and fees, at least in the way these words are used to describe professional advisors in the financial services industry. </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <img style="height:150px;" class="img-thumbnail" src="../UploadFile/Image/commission_users.png">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-check">
                                                    <h4>
                                                    <label class="form-check-label">
                                                      <input class="mr-2" id="rd_Team" name="commission_type"  type="radio" >Team
                                                    </label>
                                                    </h4>
                                                </div>
                                                
                                                <p class="ml-4">There are important differences between commissions and fees, at least in the way these words are used to describe professional advisors in the financial services industry. A commission-based advisor or broker  </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            
                                            <div class="col-md-2">
                                                <img style="height:150px;" class="img-thumbnail" src="../UploadFile/Image/commission_users.png">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-check">
                                                    <h4>
                                                    <label class="form-check-label">
                                                      <input class="mr-2" id="rd_DealBased" name="commission_type"  type="radio" >Deal Based
                                                    </label>
                                                    </h4>
                                                </div>
                                                
                                                <p class="ml-4">There are important differences between commissions and fees, at least in the way these words are used to describe professional advisors in the financial services industry. </p>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab1()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="Tab3()" class="btn btn-primary">NEXT</button>
                                     </div>
                                </div>
									</div>

									<div class="tab-pane fade" id="bordered-justified-tab3">
										<div class="custom-panel credit-transaction-card mb-3">
                                        <%--<h6>Credit the transaction based on following criteria</h6>--%>
                                        <div  class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Name<span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control required" >
                                                </div>
                                                <div class="form-group col-md-6 text-right">
                                                    <label> </label>
                                                    <button type="button" onclick="addCreditTransaction()" class="btn btn-primary btn-xx pull-right" ><i class="icon-plus22"></i></button>
                                                </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 custom-panel">
                                                  <div class="source">
                                                  <p>Source (Configured table names)</p>
                                                    <table class="table">
                                                        <tbody>
                                                <!--  source added --->
                                                            <tr>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>Combo 1</option>
                                                                        <option>Combo 2</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>Combo 1</option>
                                                                        <option>Combo 2</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>equal </option>
                                                                        <option>not equal</option>
                                                                        <option>Greater </option>
                                                                        <option>Lesser</option>
                                                                    </select>
                                                                </td>
                                                                <td> <input type="text" class="form-control"></td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>OR</option>
                                                                        <option>AND</option>
                                                                    </select>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                <!-- / source added --->
                                                <!--  source added --->
                                                            <tr>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>Combo 1</option>
                                                                        <option>Combo 2</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>Combo 1</option>
                                                                        <option>Combo 2</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>equal </option>
                                                                        <option>not equal</option>
                                                                        <option>Greater </option>
                                                                        <option>Lesser</option>
                                                                    </select>
                                                                </td>
                                                                <td> <input type="text" class="form-control"></td>
                                                                <td>
                                                                    <select class="form-control">
                                                                        <option>-- select one --</option>
                                                                        <option>OR</option>
                                                                        <option>AND</option>
                                                                    </select>
                                                                </td>
                                                                <td><button class="btn btn-primary btn-xx"><i class="icon-plus22"></i></button></td>
                                                            </tr>
                                                <!-- / source added --->
                                                        </tbody>
                                                    </table>
                                               </div>
                                            </div>
                                            </div>
                                           
                                    </div>
                                         <div class="form-group col-md-12 mt-4 d-flex justify-content-between">
                                               <button type="button" onclick="Tab2()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="Tab4()" class="btn btn-primary">NEXT</button>
                                            </div>
									</div>

									<div class="tab-pane fade" id="bordered-justified-tab4">
										     
                                <div>
                                    <div class="form-group mt-5">
                                            <!--set goal--->
                                           <div class="row">
                                                <div class="form-group col-md-2">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input">
                                                        Set Goal
                                                    </label>
                                                </div>
                                                    
                                                </div>
                                               
                                                
                                                <div class="form-group col-md-6">
                                                            
                                                            <input placeholder="type to search goal..." type="text" class="form-control required">
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <!--<a data-toggle="collapse" class="btn btn-sm btn-primary" data-parent="" href="#accordion1"><i class="icon-menu"></i></a>-->
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalGoal"> <i class="icon-menu"></i></button>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <a data-toggle="collapse" class="btn btn-default btn-sm bg-light text-dark collapsed" data-parent="" href="#accordion1"><i class="icon-arrow-down22"></i></a>
                                                </div>
                                               
                                                <div class="form-group  col-md-12 mb-3">
                                                     <div id="accordion1" class="panel-collapse collapse ">
                                                    <table class="table bordered custom-panel">
                                                            <thead>
                                                               
                                                               <tr> <th>Period</th><th>Goal Value</th></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                   <td>period 1</td>
                                                                   <td> 10</td>
                                                                </tr>
                                                                <tr>
                                                                   <td>period 2</td>
                                                                   <td> 20</td>
                                                                </tr>
                                                                <tr>
                                                                   <td>period 3</td>
                                                                   <td> 13</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>    
                                                </div>
                                           </div>
                                            <!--/set goal-->
                                            <!--Range matrix--->
                                        <div class="form-group row">
                                                <div class="form-group col-md-2">
                                                    <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input">
                                                       Range Matrix
                                                    </label>
                                                    </div>
                                                    
                                                </div>
                                               
                                                
                                                <div class="form-group col-md-6">
                                                            
                                                           <input placeholder="type to search range matrix..." type="text" class="form-control">
                                                </div>
                                                <div class="form-group col-md-2">
                                                   <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalRange"> <i class="icon-menu"></i></button>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <a data-toggle="collapse" class="btn btn-default btn-sm bg-light text-dark collapsed" data-parent="" href="#accordion2"><i class="icon-arrow-down22"></i></a>
                                                </div>
                                               
                                                <div class="form-group  col-md-12 ">
                                                     <div id="accordion2" class="panel-collapse collapse ">
                                                     <table class="table bordered custom-panel">
                                                            <thead>
                                                               
                                                               <tr> <th>From</th><th>to</th><th> Value</th></tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                   <td>12-06-2019</td>
                                                                   <td> 22-06-2019</td>
                                                                   <td>40</td>
                                                                </tr>
                                                                <tr>
                                                                   <td>12-06-2019</td>
                                                                   <td> 22-06-2019</td>
                                                                   <td>40</td>
                                                                </tr>
                                                                <tr>
                                                                   <td>12-06-2019</td>
                                                                   <td> 22-06-2019</td>
                                                                   <td>40</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>    
                                                </div>
                                           </div>
                                          
                                            <!--/ramge matrix-->
                                    </div>
                                    <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab3()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="Tab5()" class="btn btn-primary">NEXT</button>
                                     </div>
                                </div>
									</div>

                                    <div class="tab-pane fade" id="bordered-justified-tab5">
                                    <h6>Based on Credit Rule</h6>
                                <div class="row">
                                
                                            <div class="form-group col-md-6">
                                                
                                                <label>Commission rate</label>
                                                <input placeholder="commission rate" type="text" class="form-control required">
                                                
                                            </div>
                                            
                                                <div class="form-group col-md-6">
                                                    <label>Credit Rule 1</label>
                                                    <input type="text" class="form-control required">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Credit Rule 2</label>
                                                    <input type="text" class="form-control required">
                                                </div>
                                    </div>
                                    <div class="row">
                                            <div class="form-group col-md-12">
                                                <h6>Payout</h6>
                                                <table class="table">
                                                            
                                                            <tbody>
                                                                <tr>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td class="text-center">=</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td></td>
                                                                </tr>
                                                                <tr>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td class="text-center">=</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td><button class="btn btn-primary btn-xs"><i class="icon-plus22"></i></button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Round</label>
                                                <select class="form-control">
                                                        <option>-- select one --</option>
                                                        <option>round off +</option>
                                                        <option>round off -</option>
                                                    </select>
                                            </div>
                                            <!--process register--->
                                           <div class="form-group col-md-6">
                                                <label></label>
                                                <a class="d-none" id="advProcessRegister" data-toggle="collapse" data-parent="#advProcessRegister" href="#advprocessRegister">Set Goal</a>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input onclick="advProcessRegister();"  type="checkbox" class="form-check-input" >
                                                        Advance Process Register
                                                    </label>
                                                </div>
                                               
                                                <div id="advprocessRegister" class="panel-collapse collapse ">
                                                <div class="">
                                                       <div class="form-group col-md-12">
                                                        
                                                            <input placeholder="" type="file" class="form-control">
                                                        </div>
                                                        
                                                </div>
                                            </div>
                                           </div>
                                        <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab4()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="Tab6()" class="btn btn-primary">NEXT</button>
                                     </div>
                                </div>
                                    </div>

                                    <div class="tab-pane fade" id="bordered-justified-tab6">
                                           <div class="row">
                                   <div class="form-group col-md-12">
                                                <h6>Assign Recipients</h6>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input-styled" name="stacked-radio-left" checked >
                                                        Selected Individual recipients
                                                    </label>
                                                </div>
                                                <table  class="table custom-panel">
                                                    <thead>
                                                        <tr>
                                                            <th><input type="checkbox" class="form-control"></th>
                                                            <th>Recipient ID</th>
                                                            <th>Name</th>
                                                            <th>Role</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><input type="checkbox" class="form-control"></td>
                                                            <td>CM001</td>
                                                            <td>Ranjith</td>
                                                            <td>Sales Person</td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="checkbox" class="form-control"></td>
                                                            <td>CM002</td>
                                                            <td>Rajesh</td>
                                                            <td>Manager</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label></label>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input-styled" name="stacked-radio-left" checked >
                                                        Set Assignment Condition
                                                    </label>
                                                </div>
                                                <table  class="table custom-panel">
                                                    
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Field
                                                                <select class="form-control">
                                                                    <option>-- select type --</option>
                                                                    <option>Recipient Type 1</option>
                                                                    <option>Recipient Type 2  -</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <label>Operator</label>
                                                                <select class="form-control">
                                                                    <option>-- select operator --</option>
                                                                    <option>equal</option>
                                                                    <option>not equal</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <label>Value</label>
                                                                <select class="form-control">
                                                                    <option>-- select type --</option>
                                                                    <option>Sales Rep</option>
                                                                    <option>Manager</option>
                                                                    <option>Sales Person</option>
                                                                </select>
                                                            </td>
                                                            <td><label></label><button class="btn btn-primary btn-xs"><i class="icon-plus22"></i></button></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                               <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab5()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="" class="btn btn-primary">SUBMIT</button>
                                     </div>
                                </div>
                                    </div>
								</div>
                            <!--/Nav Tabs for commission-->
                     
                    </div>
                    </div>
                    <!-- /basic setup -->

            </div>
            
                                <!--Input Form-->
                            </div>
                       
    </div>
   <%-- <button type="button" class="btn btn-primary" id="pnotify-solid-primary">Launch <i class="icon-play3 ml-2"></i></button>--%>
                <!-- Goal modal -->
                <div id="modalGoal" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Goal</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                 <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Goal-list" class="nav-link active" data-toggle="tab">Goal List</a></li>
									<li class="nav-item"><a href="#Goal-new" class="nav-link" data-toggle="tab">New Goal</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Goal-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Goal</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Goal 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Goal 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Goal 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Goal-new">
										 <div class="row">
                                           
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Goal Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <table style="background-color: #80808040;"  class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>Period</th><th>Goal Value</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="set-goal">
                                                                <tr>
                                                                    <td class="tr-count">1</td>
                                                                   <td>
                                                                        <div class="">
                                                                         <input name="period" placeholder="period" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="">
                                                                         <input name="goal" placeholder="goal" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td class="td-btn">
                                                                        <div class="d-inline-block add-goal">
                                                                            <button type="button" class="btn btn-primary btn-sm add-goal-btn btn-xx mr-2" onclick="addGoal()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="addGoal(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>  
                                                                   </td>
                                                                </tr>
                                                               
                                                            </tbody>
                                                        </table>
                                             <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveRange()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div> 
									</div>

									
								</div>
                                   
                            </div>

                           
                        </div>
                    </div>
                </div>
                <!-- /goal modal -->
    
                <!-- Range modal -->
                <div id="modalRange" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Range</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                 <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Range-list" class="nav-link active" data-toggle="tab">Range List</a></li>
									<li class="nav-item"><a href="#Range-new" class="nav-link" data-toggle="tab">New Range</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Range-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Range</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Range 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Range 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Range 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Range-new">
										 <div class="row">
                                            
                                           
                                            <div class="form-group col-md-12">
                                                    <label>Range Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>From</th><th>To</th><th>Value</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="set-range">
                                                                <tr>
                                                                    <td class="td-count">1</td>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input name="from" placeholder="from" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input name="to" placeholder="to" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input name="value" placeholder="value" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td class="td-btn">
                                                                        <div class="d-inline-block add-range">
                                                                            <button type="button" class="btn btn-primary btn-sm add-range-btn btn-xx mr-2" onclick="addRange()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="addGoal(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>  
                                                                   </td>
                                                                </tr>
                                                              
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveRange()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                           
                        </div>
                    </div>
                </div>
                <!-- /Range modal -->
    
                <!-- override modal -->
                <div id="modalOverride" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp; Override</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                  <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#Override-list" class="nav-link active" data-toggle="tab">Override List</a></li>
									<li class="nav-item"><a href="#Override-new" class="nav-link" data-toggle="tab">New Override</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="Override-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total Levels</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Levels 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Levels 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Levels 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="Override-new">
										 <div class="row">
                                            
                                            
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Level Name  <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr> <th>Level</th><th>%</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="Override-Levels">
                                                               
                                                                <tr>
                                                                    <td class="td-level">
                                                                        <div class="form-group">
                                                                         <label>Level 1</label>
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="%" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    
                                                                   <td class="tr-btn">
                                                                     <div class="d-inline-block add-override-level">
                                                                            <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>    
                                                                   </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveOverride()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                            
                        </div>
                    </div>
                </div>
                <!-- /override modal -->
    
                <!-- split modal -->
                <div id="modalSplit" class="modal fade" tabindex="-1">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;Split</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                                <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
									<li class="nav-item"><a href="#split-list" class="nav-link active" data-toggle="tab">Split List</a></li>
									<li class="nav-item"><a href="#split-new" class="nav-link" data-toggle="tab">New Split</a></li>
									
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
									<div class="tab-pane fade show active" id="split-list">
										<div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Total recipients</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-primary">Split 1</td>
                                                        <td>5</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Split 2</td>
                                                        <td>3</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary">Split 3</td>
                                                        <td>8</td>
                                                        <td><button type="button" class="btn btn-primary btn-xx">Edit</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
										</div>
									</div>

									<div class="tab-pane fade" id="split-new">
										 <div class="row">
                                            
                                            
                                            
                                            <div class="form-group col-md-12">
                                                    <label>Split Name  <span class="text-danger">*</span></label>
                                                    <input id="txt_SplitName" type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea id="txt_SplitDesc" class="form-control" rows="3" ></textarea>
                                            </div>
                                            <div style="background-color: #80808040;" class="form-group col-md-12">
                                                   
                                                        <table class="table">
                                                            <thead>
                                                               <tr><th>No</th> <th>Recipient</th><th>%</th><th></th></tr>
                                                            </thead>
                                                            <tbody id="Split-Recipients">
                                                                
                                                                <tr id="split-1">
                                                                    <td class="tr-count">1</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         
                                                                          <select class="form-control recipient-dd" >
                                                                             <option value="">-select recipient-</option>
                                                                             <option value="Raj">Raj</option>
                                                                             <option value="John">John</option>
                                                                             <option value="Bob">Bob</option>
                                                                         </select>
                                                                        </div>
                                                                   </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="%"  name="RecipientPercentage[]"  type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    
                                                                   <td class="tr-btn">
                                                                       <div class="d-inline-block add-recipient">
                                                                            <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button>
                                                                           <%-- <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(0)"><i class="icon-bin"></i></button>--%>
                                                                       </div>
                                                                  </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                </div>
                                            <div class=" col-md-12 text-right">
                                                <button type="button" onclick="saveSplit()" class="btn bg-primary btn-sm"><i class="icon-checkmark3 font-size-base mr-1"></i> Save</button>
                                            </div>

                                             
                                            </div>
									</div>

									
								</div>
                               
                            </div>

                            <%--<div class="modal-footer">
                              
                               
                            </div>--%>
                        </div>
                    </div>
                </div>
                <!-- /split modal -->
    <script>
        function openSetGoal() {
            $('#openSetgoal').click();
        }
        function openRangeMatrix() {
            $('#openRangeMatrix').click();
        }
        function advProcessRegister() {
            $('#advProcessRegister').click();
        }

        
    </script>     


</asp:Content>

