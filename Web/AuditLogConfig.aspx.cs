﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_AuditLogConfig : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.Com = new Common();
        loadAuditList();

        if (!IsPostBack)
        {
            string str_Select = "SELECT TABLE_NAME AS Value,  TABLE_NAME AS Text FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG='CM'";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.Com.LoadDropDown(ddl_TableName, ds.Tables[0], "Text", "Value", true, "--Select Table--");

        }
        if (Request.QueryString["audit_key"] != null)
        {
           viewAuditDetails(Request.QueryString["audit_key"]);
            div_AduitLog.Visible = true;
            div_AduitLog.Attributes["class"] = "col-lg-6 col-md-6";
            div_InputForm.Visible = false;
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["audit_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadAuditDetailToEdit(Request.QueryString["audit_key"]);
                div_AduitLog.Attributes["class"] = "col-lg-6 col-md-6 d-none";
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }

    }
    [WebMethod]
    public static string viewAuditLog(string audit_key)
    {

        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();

            string str_Sql_view = "select * from AuditLogConfig where AuditKey =@AuditKey";
            SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
            sqlcmd.Parameters.AddWithValue("@AuditKey", audit_key);
            DataTable dt = DA.GetDataTable(sqlcmd);
            string HtmlData = "", HtmlTr = "";

            HtmlTr = PHT.ReadFileToString("DivAuditView.txt");
            foreach (DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }
            return HtmlData;

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }
    [WebMethod]
    public static string deleteAdjustment(string AdjustKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Adjustments where AdjustmentKey=@AdjustmentKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@AdjustmentKey", AdjustKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    private void loadAuditDetailToEdit(string audit_key)
    {
        string str_Sql_view = "select * "+
           " from AuditLogConfig where  AuditKey =@AuditKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@AuditKey", audit_key);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            txt_AuditLogName.Value = dr["AuditName"].ToString();
            ddl_TableName.SelectedValue = dr["TableName"].ToString();

            

            if (dr["LogInsert"].ToString() == "True")
            {
                chb_Insert.Checked = true;
            }
            if (dr["LogUpdate"].ToString() == "True")
            {
                chb_Update.Checked = true;
            }
            if (dr["LogDelete"].ToString() == "True")
            {
                chb_Delete.Checked = true;
            }
        }
    }

    private void viewAuditDetails(string audit_key)
    {
        string str_Sql_view = "select * from AuditLogConfig where AuditKey =@AuditKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@AuditKey", audit_key);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_AuditLogDetails, "DivAuditView.txt", "");
    }

    private void loadAuditList()
    {
        string str_Sql = "select *, convert(varchar, a.CreatedOn, 101) as Created_On, (select FirstName from users where UserKey=a.CreatedBy) as CreatedByName from AuditLogConfig a   order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_AuditLogListTr, "AuditLogListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string str_Key = Guid.NewGuid().ToString();
        //AuditLogConfig AuditKey, AuditName, TableName, LogInsert, LogUpdate, LogDelete, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string str_Sql = "insert into AuditLogConfig (AuditKey, AuditName, TableName, LogInsert, LogUpdate, LogDelete, CreatedOn, CreatedBy)"
                            + "select @AuditKey, @AuditName, @TableName, @LogInsert, @LogUpdate, @LogDelete, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AuditKey", str_Key);
        cmd.Parameters.AddWithValue("@AuditName", txt_AuditLogName.Value);
        cmd.Parameters.AddWithValue("@TableName", ddl_TableName.SelectedValue);

        if (chb_Insert.Checked == true)
            cmd.Parameters.AddWithValue("@LogInsert", "1");
        else
            cmd.Parameters.AddWithValue("@LogInsert", "0");
        if (chb_Update.Checked == true)
            cmd.Parameters.AddWithValue("@LogUpdate", "1");
        else
            cmd.Parameters.AddWithValue("@LogUpdate", "0");
        if (chb_Delete.Checked == true)
            cmd.Parameters.AddWithValue("@LogDelete", "1");
        else
            cmd.Parameters.AddWithValue("@LogDelete", "0");
        

        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string audit_key = Request.QueryString["audit_key"];

       

        string str_Sql = "UPDATE AuditLogConfig set   AuditName=@AuditName, TableName=@TableName, LogInsert=@LogInsert, LogUpdate=@LogUpdate, LogDelete=@LogDelete,  " +
            "ModifiedOn=@ModifiedOn, ModifiedBy =@ModifiedBy where AuditKey=@AuditKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);


        cmd.Parameters.AddWithValue("@AuditName", txt_AuditLogName.Value);
        cmd.Parameters.AddWithValue("@TableName", ddl_TableName.SelectedValue);

        if (chb_Insert.Checked == true)
            cmd.Parameters.AddWithValue("@LogInsert", "1");
        else
            cmd.Parameters.AddWithValue("@LogInsert", "0");
        if (chb_Update.Checked == true)
            cmd.Parameters.AddWithValue("@LogUpdate", "1");
        else
            cmd.Parameters.AddWithValue("@LogUpdate", "0");
        if (chb_Delete.Checked == true)
            cmd.Parameters.AddWithValue("@LogDelete", "1");
        else
            cmd.Parameters.AddWithValue("@LogDelete", "0");

        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@AuditKey", audit_key);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/AuditLogConfig.aspx?audit_key=" + audit_key + "");
    }
}