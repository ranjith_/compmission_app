﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="TransactionEdit.aspx.cs" Inherits="Web_TransactionEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Transaction</title>
    <script>
        function ValidateTransaction() {
            let TransId = $('#txt_TransactionId').val();
            let type = $('#dd_TransactionType').val();
            let valid = true;
            if (type != "") {

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "../Web/TransactionEdit.aspx/ValidateTransaction",
                        data: "{TransId:'" + TransId + "',TransType:'" + type + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == "0") {
                                alertNotify('Transaction Error', 'Transaction Id with same Transaction Type is Already taken', 'bg-danger border-danger');
                                valid = false;
                            }
                            else if (data.d == "1") {

                                valid = true;
                            }


                        },
                        error: function () {
                            alert('Error communicating with server');
                            valid = false;
                        }
                    });
            }

            return valid;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
             <!--Input Form-->
                                 <div id="div_InputForm" runat="server" class="col-lg-12 col-md-12">
                                        <!-- Basic layout-->
                                    <div class="card border-top-3 border-top-primary-600">
                            <div class="card-header header-elements-inline ">
                                <h5 class="card-title">Transaction</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a href="Transaction.aspx" class="btn btn-primary btn-sm">Transaction List</a>
                                        <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" OnClientClick="return ValidateTransaction(); " CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" OnClientClick="return ValidateTransaction(); " runat="server" CssClass="btn btn-primary  btn-sm " />
                                      <%--  <button type="button" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> Save</button>--%>
                                       <%-- <a class="list-icons-item" data-action="collapse"></a>
                                        <a  class="list-icons-item" data-action="reload"></a>--%>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-3">
                                                <label>Transaction ID<span class="text-danger">*</span></label>
                                                <input ID="txt_TransactionId"  runat="server" required  type="text" class="form-control" >
                                                
                                            </div>
                                            
                                            <div class="form-group col-md-3">
                                                    <label>Transaction Line <span class="text-danger">*</span></label>
                                                    <input id="txt_TransactionLine"  runat="server" required type="text" class="form-control typeahead-basic-type" >
                                            </div>
                                           
                                            <div class="form-group col-md-3">
                                                <label>Transaction Date<span class="text-danger">*</span></label>
                                                <input id="txt_TransactionDate"  runat="server" required   type="date" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Sales Rep 1<span class="text-danger">*</span></label>
                                                <%--<input id="txt_SalesRep1"  runat="server" required  type="text" class="form-control" >--%>
                                                <asp:DropDownList CssClass="form-control" required ID="ddl_Recipient1" runat="server"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                     <label>Transaction Type</label>
                                               <asp:DropDownList onchange="ValidateTransaction();" ID="dd_TransactionType" required runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                     <label>Transaction Line Type</label>
                                                <select id="dd_TransactionLineType"  runat="server"   class="form-control">
                                                <option value="">-- select one --</option>
                                                    <option  value="Type 1">Type 1</option>
                                                    <option>Type 2</option>
                                                    <option>Type 3</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Customer<span class="text-danger">*</span></label>
                                               <%-- <input id="txt_Customer"   runat="server" required  class="form-control" >--%>
                                                <asp:DropDownList CssClass="form-control" ID="ddl_Customer" runat="server"></asp:DropDownList>
                                            </div>
                                            
                                            <div class="form-group col-md-3">
                                                <label>Sales Rep 2<span class="text-danger">*</span></label>
                                                <%--<input  id="txt_SalesRep2"  runat="server" required   type="text" class="form-control" >--%>
                                                <asp:DropDownList CssClass="form-control" ID="ddl_Recipient2" runat="server"></asp:DropDownList>
                                            </div>
                                                 <div class="form-group col-md-3">
                                                <label>Sales Rep 3<span class="text-danger">*</span></label>
                                                <%--<input  id="txt_SalesRep2"  runat="server" required   type="text" class="form-control" >--%>
                                                <asp:DropDownList CssClass="form-control" ID="ddl_Recipient3" runat="server"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Product<span class="text-danger">*</span></label>
                                                <%--<input id="txt_Product"  runat="server" required  type="text" class="form-control" >--%>
                                                <asp:DropDownList CssClass="form-control" ID="ddl_Product" runat="server"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Sales Amount<span class="text-danger">*</span></label>
                                                <input id="txt_SalesAmount"  runat="server"   type="text" class="form-control number" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                     <label>Transaction Line Level</label>
                                                <select id="dd_TransactionLineLevel"  runat="server"  class="form-control">
                                                <option value="">-- select one --</option>
                                                    <option>Level 1</option>
                                                    <option>Level 2</option>
                                                    <option>Level 3</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>SO Number<span class="text-danger">*</span></label>
                                                <input id="txt_SoNumber"  runat="server"   type="text" class="form-control number" >
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Payment Type <span class="text-danger">*</span></label>
                                                <select id="dd_PaymentType"  runat="server"   class="form-control">
                                                <option value="">-- select one --</option>
                                                    <option>Cash</option>
                                                    <option>Credit / Debit Card</option>
                                                    <option>Net Banking</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label>Bill Number<span class="text-danger">*</span></label>
                                                <input id="txt_BillNumber" runat="server"   type="text" class="form-control number">
                                            </div>
                                                 <div class="form-group col-md-3">
                                                <label>Effective Date<span class="text-danger">*</span></label>
                                                <input id="txt_EffectiveDate"  runat="server"   type="date" class="form-control" >
                                            </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                         <hr />
                                    <h5 class="d-inline">Custom Fields</h5>
                                    <a class="btn btn-link font-weight-bold float-right" href="TransactionCustomConfig.aspx?PrimaryTableName=Transaction">Add a Field</a>
                                    </div>
                                   
                                </div>
                                 <asp:Panel ID="panel_CustomTransaction" CssClass="row" runat="server"></asp:Panel>
                                        </div>
                                    </div>
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->
        </div>
    </div>
</asp:Content>

