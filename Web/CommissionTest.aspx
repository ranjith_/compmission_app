﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionTest.aspx.cs" Inherits="Web_Commission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission</title>
    <script type="text/javascript" src="../JScript/Commission.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>
    <style>
        .custom-panel{
    background-color: #80808040;
    padding: 15px;
    border-radius: 5px;
        }
        .btn-xx{
            padding: .3em 0.4em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title d-flex"><i class="icon-tree6"></i>  <span class="ml-2">Commission</span>
                                <div class="form-check form-check-left form-check-switchery form-check-switchery-sm ml-3">
                                <label class="form-check-label"> ON / OFF
                                    <!--Active -->
                                    <input type="checkbox" class="form-input-switchery"  >
                                </label>
                                </div>
                                </h5> 
                                <div id="div_CardHeader" runat="server" class="header-elements">
                                   
                                </div>
                            </div>
                            <div class="card-body">
                                <div runat="server" id="div_CommissionForm" class="row">
                                    <div class="col-md-12">
                                          <div class="row m-1">
                                    <div class="form-group col-md-6">
                                        <label>Commission Name<span class="text-danger">*</span></label>
                                        <input  type="text" id="txt_CommissionName" runat="server" class="form-control required" >
                                    </div>
                                    <div class="form-group  col-md-3" >
                                                    <label style="white-space: nowrap; " class="">Effective Date<span class="text-danger">*</span></label>
                                                    <input type="date" id="dp_EffectiveDate" runat="server" required="required" class="form-control required " >
                                    </div>
                                    <div class="form-group col-md-3">
                                            <label>Calculation Feq<span class="text-danger">*</span></label>
                                            <select id="dd_CalFeq" runat="server" class="form-control ">
                                                <option value="">-- select one --</option>
                                                <option value="Weekly">Weekly</option>
                                                <option value="Monthly">Monthly</option>
                                                <option value="Quarterly">Quarterly</option>
                                                <option value="Yearly">Yearly</option>
                                            </select>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                                    <label>Agreement Description</label>
                                                    <textarea id="txt_Description1" runat="server" class="form-control required mb-20" rows="3" ></textarea>
                                    
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                                    
                                        <label >Description</label>
                                        <textarea id="txt_Description2" runat="server" class="form-control required " rows="3" ></textarea>
                                    </div>
                                    <div class="col-md-12 form-group float-right text-right">
                                        <button class="btn btn-primary btn-sm" id="btn_Save_Commission" runat="server" onserverclick="btn_Save_Commission_ServerClick">SAVE</button>
                                    </div>
                                </div>
                                    </div>
                                </div>
                                <asp:PlaceHolder runat="server" ID="ph_CommissionView" ></asp:PlaceHolder>
                                <div id="div_WizardCommission" visible="false" runat="server">
                                     <!--Nav Tabs for commission-->
                            <ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0 mr-2 ml-2">
									<li class="nav-item"><a href="#bordered-justified-tab1 " class="nav-link active " data-toggle="tab">Source of Credit</a></li>
									<li class="nav-item"><a href="#bordered-justified-tab2" class="nav-link" data-toggle="tab">Commission Type</a></li>
                                    <li class="nav-item"><a href="#bordered-justified-tab3" class="nav-link" data-toggle="tab">Credit the transaction</a></li>
                                    <li class="nav-item"><a href="#bordered-justified-tab4" class="nav-link" data-toggle="tab">Goal / Range</a></li>
                                    <li class="nav-item"><a href="#bordered-justified-tab5" class="nav-link" data-toggle="tab">Credit Rule</a></li>
                                    <li class="nav-item"><a href="#bordered-justified-tab6" class="nav-link" data-toggle="tab">Assign Recipients</a></li>
                                
									<%--<li class="nav-item dropdown">
										<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Dropdown</a>
										<div class="dropdown-menu dropdown-menu-right">
											<a href="#bordered-justified-tab3" class="dropdown-item" data-toggle="tab">Dropdown tab</a>
											<a href="#bordered-justified-tab4" class="dropdown-item" data-toggle="tab">Another tab</a>
										</div>
									</li>--%>
								</ul>

								<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0 mr-2 ml-2">
									<div class="tab-pane fade show active" id="bordered-justified-tab1">
                                        <iframe src="CommissionCredit.aspx" frameborder="0" scrolling="no" height="100%" width="100%"></iframe>

										 <div class="row d-none">
                                             <div class="form-group col-md-12 text-right">
                                               <button type="button" onclick="Tab2()" class="btn btn-primary">NEXT</button>
                                             </div>
                                         </div>
                                     </div>

									<div class="tab-pane fade" id="bordered-justified-tab2">
                                        <iframe src="CommissionType.aspx" frameborder="0" scrolling="no" height="800px" width="100%"></iframe>

										  <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab1()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="Tab3()" class="btn btn-primary">NEXT</button>
                                     </div>
									</div>

									<div class="tab-pane fade" id="bordered-justified-tab3">
                                        <iframe src="CommissionCreditTransaction.aspx" width="100%" height="350px"  scrolling="no" frameborder="0"></iframe>
									<div class="row">
                                          <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab2()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="Tab4()" class="btn btn-primary">NEXT</button>
                                            </div>
									</div>
									</div>

									<div class="tab-pane fade" id="bordered-justified-tab4">
									<iframe src="CommissionGoalRange.aspx" width="100%" height="350px"  scrolling="no" frameborder="0"></iframe>
                                  
                                    <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab3()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="Tab5()" class="btn btn-primary">NEXT</button>
                                     </div>
                                
									</div>

                                    <div class="tab-pane fade" id="bordered-justified-tab5">
                                        <iframe src="CommissionCreditRule.aspx" width="100%" height="550px"  scrolling="no" frameborder="0"></iframe>
                                     <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab4()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="Tab6()" class="btn btn-primary">NEXT</button>
                                     </div>
                                    </div>

                                    <div class="tab-pane fade" id="bordered-justified-tab6">
                                         <iframe src="CommissionRecipients.aspx" width="100%" height="450px"  scrolling="no" frameborder="0"></iframe>
                                          <div class="form-group col-md-12 d-flex justify-content-between">
                                               <button type="button" onclick="Tab5()" class="btn btn-primary">PREVIOUS</button>
                                               <button type="button" onclick="" class="btn btn-primary">SUBMIT</button>
                                         </div>
                                    </div>
								</div>
                            <!--/Nav Tabs for commission-->
                                </div>
                                 
                            </div>
                    
                 </div>
            </div>
        </div>
    </div>
</asp:Content>

