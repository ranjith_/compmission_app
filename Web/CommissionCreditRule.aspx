﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" AutoEventWireup="true" CodeFile="CommissionCreditRule.aspx.cs" Inherits="Web_CommissionCreditRule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <h6>Based on Credit Rule</h6>
                                <div class="row">
                                
                                            <div class="form-group col-md-6">
                                                
                                                <label>Commission rate</label>
                                                <input placeholder="commission rate" type="text" class="form-control required">
                                                
                                            </div>
                                            
                                                <div class="form-group col-md-6">
                                                    <label>Credit Rule 1</label>
                                                    <input type="text" class="form-control required">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Credit Rule 2</label>
                                                    <input type="text" class="form-control required">
                                                </div>
                                    </div>
                                    <div class="row">
                                            <div class="form-group col-md-12">
                                                <h6>Payout</h6>
                                                <table class="table">
                                                            
                                                            <tbody>
                                                                <tr>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td class="text-center">=</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td></td>
                                                                </tr>
                                                                <tr>
                                                                   <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                    <td class="text-center">=</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                         <input placeholder="" type="text" class="form-control">
                                                                        </div>
                                                                   </td>
                                                                   <td><button class="btn btn-primary btn-xs"><i class="icon-plus22"></i></button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Round</label>
                                                <select class="form-control">
                                                        <option>-- select one --</option>
                                                        <option>round off +</option>
                                                        <option>round off -</option>
                                                    </select>
                                            </div>
                                            <!--process register--->
                                           <div class="form-group col-md-6">
                                                <label></label>
                                                <a class="d-none" id="advProcessRegister" data-toggle="collapse" data-parent="#advProcessRegister" href="#advprocessRegister">Set Goal</a>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input onclick="advProcessRegister();"  type="checkbox" class="form-check-input" >
                                                        Advance Process Register
                                                    </label>
                                                </div>
                                               
                                                <div id="advprocessRegister" class="panel-collapse collapse ">
                                                <div class="">
                                                       <div class="form-group col-md-12">
                                                        
                                                            <input placeholder="" type="file" class="form-control">
                                                        </div>
                                                        
                                                </div>
                                            </div>
                                           </div>
                                       
                                </div>
</asp:Content>

