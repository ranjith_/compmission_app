﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json;

public partial class Web_ImportData : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;

    DataAccessExcel DAE;
    DataTable DtExcel;
    string importKey = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        string str_FilePath = Com.GetQueryStringValue("file_path");

       importKey = Com.GetQueryStringValue("ImportKey");
        loadFileDetails(importKey);
        ExtractExcelData(str_FilePath);

        //string str10 = ((HtmlInputText)panel.FindControl("txtFlgUpdatedOn")).Value;


    }

    private void loadFileDetails(string importKey)
    {
        string str_Sql = "select * from Imports where ImportKey =@ImportKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("@ImportKey", importKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        string filepath = "";
        foreach (DataRow dr in dt.Rows)
        {

            lbl_ImportName.Text = dr["ImportName"].ToString();
            filepath= dr["FileName"].ToString();
            
        }
       

    }

    private void addListItem(DropDownList dropDownList, string text, string value)
    {
        ListItem l = new ListItem(text, value, true); 
        dropDownList.Items.Add(l);
    }

    

    private void ExtractExcelData(string str_FilePath)
    {
        DataTable dtResult = null;

        this.DAE = new DataAccessExcel(str_FilePath);
        string str_ConnectionString = this.DAE.GetOledbConnectionString();


        using (OleDbConnection objConn = new OleDbConnection(str_ConnectionString))
        {
            //Get Excel Data as DataTable
            objConn.Open();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter oleda = new OleDbDataAdapter();
            DataSet ds = new DataSet();
            DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            this.DtExcel = dt;
            string sheetName = string.Empty;
            if (dt != null)
            {
                sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
            }
            dtResult = this.DAE.ExcelSheetData(sheetName, "");

            String[] excelSheets = this.DAE.ExcelSheetNames();
            
            int i = 0;

            // Add the sheet name to the string array.
            foreach (DataRow row in dt.Rows)
            {
                DataTable dtSheet = this.DAE.ExcelSheetData(row["TABLE_NAME"].ToString(), "");
                string[] sheetColumns = new String[dtSheet.Rows.Count];
                string SheetName = row["TABLE_NAME"].ToString();
                if (dtSheet.Rows.Count > 0)
                {
                    int j = 0;
                    foreach (DataColumn dc in dtSheet.Columns)
                    {
                        sheetColumns[j] = dc.ColumnName;
                        j++;
                    }
                    addListItem(ddl_ExcelSheets, SheetName, string.Join(",", sheetColumns));
                }
                
                
            }

            //Insert Record into Batch Table
            //str_Sql = "p_InsertBatchFiles";
            //SqlCommand sc = new SqlCommand(str_Sql);
            //sc.CommandType = CommandType.StoredProcedure;
            //sc.Parameters.AddWithValue("@BatchFiles", dtResult);
            //sc.Parameters.AddWithValue("@UserKey", this.SC.UserKey);
            //sc.Parameters.AddWithValue("@ImportSeq", str_ImportSeq);
            //DataSet dt_Res = this.DA.GetDataSet(sc);
            //if (dt_Res.Tables[0].Rows.Count > 0)
            //{
            //    string str_Message = dt_Res.Tables[0].Rows[0]["Message"].ToString().Replace("'", "");
            //    ScriptManager.RegisterStartupScript(this, GetType(), "CareMB", "alert('" + str_Message + "');", true);
            //    //return;
            //}

        }
    }
    [WebMethod]
    public static string getTableSchema(string Destination, string source)
    {
       
        try
        {
            DataAccess DA = new DataAccess();
            PhTemplate PHT = new PhTemplate();
            String str_sql ="select * from "+Destination+" order by ColumnName Desc";
            SqlCommand cmd = new SqlCommand(str_sql);
           // cmd.Parameters.AddWithValue("@TableName", Destination);
            
            DataSet ds = DA.GetDataSet(cmd);
            DataTable dt = DA.GetDataTable(cmd);

            string[] options = source.Split(',');

            string Options = "";
            foreach(string optn in options)
            {
                if (optn != "")
                {
                    Options += "<option value='" + optn + "'>" + optn + "</option>";
                }
                
            }

            string HtmlData = "", HtmlTr = "";
            
            HtmlTr = PHT.ReadFileToString("DivImportDataHtml.txt");
            foreach(DataRow dr in dt.Select())
            {
                HtmlData += PHT.ReplaceVariableWithValue(dr, HtmlTr);

            }

          string ss=  HtmlData.Replace("%%OPTIONS%%", Options);

            return ss;
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }


    protected void btn_UploadData_ServerClick(object sender, EventArgs e)
    {
        string cc = hdn_MappingArray.Value;
        string SheetName = ddl_ExcelSheets.Items[ddl_ExcelSheets.SelectedIndex].Text;
        string TableName = ddl_DestinationTable.Items[ddl_DestinationTable.SelectedIndex].Text;
        dynamic result = JsonConvert.DeserializeObject(cc);
        var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(cc);
        // DataTable dtEx = this.DtExcel;
        DataTable dtEx = this.DAE.ExcelSheetData(SheetName, "");
        foreach (var item in values)
        {
            string key = item.Key.ToString();
            string value = item.Value.ToString();
            dtEx.Columns[value].ColumnName = key.Replace("dd_","");
        }
        uploadDataToTable(TableName, dtEx);
    }

    private void uploadDataToTable(string tableName, DataTable dt)
    {
        int count = 0;
       string str_Sql = "p_InsertTransactions";
        SqlCommand sc = new SqlCommand(str_Sql);
        sc.CommandType = CommandType.StoredProcedure;
        sc.Parameters.AddWithValue("@Transaction", dt);
        sc.Parameters.AddWithValue("@userkey", this.SC.UserKey);

        SqlParameter RuturnValue = new SqlParameter("@RowCount", SqlDbType.Int);
        RuturnValue.Direction = ParameterDirection.Output;
        sc.Parameters.Add(RuturnValue);

        this.DA.ExecuteNonQuery(sc);

        count = (int)sc.Parameters["@RowCount"].Value;

        updateImportDetails(count, importKey);
    }

    private void updateImportDetails(int count, string importKey)
    {
      string str_Sql = "Update Imports set RowsInsert=@count where ImportKey=@ImportKey ";
        SqlCommand cmd = new SqlCommand(str_Sql);


        cmd.Parameters.AddWithValue("@count", count);
        cmd.Parameters.AddWithValue("@ImportKey", importKey);
        this.DA.ExecuteNonQuery(cmd);
        Response.Redirect("../Web/Imports.aspx");
    }
}