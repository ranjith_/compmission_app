﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="Web_ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
   <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Template/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/main/jquery.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/validation/validate.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	

	<script src="../Limitless_Bootstrap_4/Template/layout_1/LTR/default/full/assets/js/app.js"></script>
	<%--<script src="../Limitless_Bootstrap_4/Template/global_assets/js/demo_pages/login_validation.js"></script>--%>
	<!-- /theme JS files -->
    <title>Forgot Password</title>
</head>
<body>
            
	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login card -->
				<form id="formLogin" runat="server" class="login-form form-validate" >
					<div class="card mb-0" id="">
						<div class="card-body" id="div_CardBody" runat="server">
							<div class="text-center mb-3">
								<i class="icon-spinner11 icon-2x text-warning border-warning border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="mb-0">Password recovery</h5>
								<span class="d-block text-muted">We'll send your password in email</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-right">
								<input id="txt_Email" runat="server" type="email" class="form-control" placeholder="Your email">
								<div class="form-control-feedback">
									<i class="icon-mail5 text-muted"></i>
								</div>
							</div>
                            <div class="text-center">
                                <asp:Label ID="lbl_Err" runat="server" CssClass="text-danger"></asp:Label>
                            </div>
							<button type="button" runat="server" id="btn_RecoverPwd" onserverclick="btn_RecoverPwd_ServerClick" class="btn bg-blue btn-block"><i class="icon-spinner11 mr-2"></i> Recovery password</button>
						</div>
					</div>
				</form>
				<!-- /login card -->

			</div>
			<!-- /content area -->


			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light fixed-bottom">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2019. <a href="#">Bada CM</a> by <a href="http://rakatech.com" target="_blank">RaKa Technology</a>
					</span>

					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
						<li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Contact</a></li>
						<li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
					</ul>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
    
</body>
</html>
