﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

public partial class Web_Agreement : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
      
       
        if (!IsPostBack)
        {
            if (Request.QueryString["agreement_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadAgreementDetailToEdit(Request.QueryString["agreement_key"]);
             //   div_AgreementDetails.Visible = false;
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }

    private void loadAgreementDetailToEdit(string AgreementKey)
    {
        string str_Sql_view = "select * from Agreement where AgreementKey =@AgreementKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@AgreementKey", AgreementKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            txt_AgreementName.Value = dr["AgreementName"].ToString();
            hdn_AgreementName.Value = dr["AgreementName"].ToString();
            txt_DocTitle.Value = dr["DocTitle"].ToString();
            txt_Headers.InnerHtml = dr["Headers"].ToString();
            dd_IncludeCommission.Value = dr["IncludeCommission"].ToString();
            dd_IncludeRecipient.Value = dr["IncludeRecipient"].ToString();
            txt_Footer.InnerHtml = dr["Footer"].ToString();

            if (dr["showSignature"].ToString()=="True")
            {
                chb_ShowSignature.Checked = true;
            }
            else
            {
                chb_ShowSignature.Checked = false;
            }
            if (dr["IsDefault"].ToString() == "True")
            {
                chb_DefaultAgreement.Checked = true;
            }
            else
            {
                chb_DefaultAgreement.Checked = false;
            }
            if (dr["ShowLogo"].ToString() == "True")
            {
                chb_Showlogo.Checked = true;
            }
            else
            {
                chb_Showlogo.Checked = false;
            }
            //(AgreementKey, AgreementName, DocTitle, Headers,IsDefault, IncludeRecipient, IncludeCommission, Footer, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        }
    }

   

    
    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";

        //string dd = txt_TransactionDate.Value.ToString();
        string str_Key = Guid.NewGuid().ToString();
        //string trans_Key = Guid.NewGuid().ToString();
        //string str_Sql = "insert into Agreement ( AgreementKey, AgreementName, DocTitle, Headers, IncludeRecipient, IncludeCommission, Footer,showSignature,IsDefault,ShowLogo, CreatedOn, CreatedBy)"
        //                    + "select @AgreementKey, @AgreementName, @DocTitle, @Headers, @IncludeRecipient, @IncludeCommission, @Footer,@showSignature,@IsDefault,@ShowLogo, @CreatedOn, @CreatedBy";

        string str_Sql = "insert into Agreement ( AgreementKey, AgreementName, Headers,  IsDefault,CreatedOn, CreatedBy)"
                           + "select @AgreementKey, @AgreementName, @Headers,  @IsDefault, @CreatedOn, @CreatedBy";

        if (chb_DefaultAgreement.Checked == true)
        {
            //str_Sql = "update Agreement set IsDefault=0 ;" +
            //    "insert into Agreement ( AgreementKey, AgreementName, DocTitle, Headers, IncludeRecipient, IncludeCommission, Footer,showSignature,IsDefault,ShowLogo, CreatedOn, CreatedBy)"
            //                + "select @AgreementKey, @AgreementName, @DocTitle, @Headers, @IncludeRecipient, @IncludeCommission, @Footer,@showSignature,@IsDefault,@ShowLogo, @CreatedOn, @CreatedBy";

            str_Sql = "update Agreement set IsDefault=0 ;" +
                   "insert into Agreement ( AgreementKey, AgreementName, Headers,  IsDefault,CreatedOn, CreatedBy)"
                           + "select @AgreementKey, @AgreementName, @Headers,  @IsDefault, @CreatedOn, @CreatedBy";
        }

        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AgreementKey", str_Key);
        cmd.Parameters.AddWithValue("@AgreementName", txt_AgreementName.Value);
      //  cmd.Parameters.AddWithValue("@DocTitle", txt_DocTitle.Value);
        cmd.Parameters.AddWithValue("@Headers", hdn_Header.Value);
       // cmd.Parameters.AddWithValue("@IncludeRecipient", dd_IncludeRecipient.Value);
      //  cmd.Parameters.AddWithValue("@IncludeCommission", dd_IncludeCommission.Value);
      //  cmd.Parameters.AddWithValue("@Footer", hdn_Footer.Value);
        //if (chb_ShowSignature.Checked == true)
        //{
        //    cmd.Parameters.AddWithValue("@showSignature", "1");
        //}
        //else
        //{
        //    cmd.Parameters.AddWithValue("@showSignature", "0");
        //}
        if (chb_DefaultAgreement.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsDefault", "1");


        }
        else
        {
            cmd.Parameters.AddWithValue("@IsDefault", "0");
        }
        //if (chb_Showlogo.Checked == true)
        //{
        //    cmd.Parameters.AddWithValue("@ShowLogo", "1");
        //}
        //else
        //{
        //    cmd.Parameters.AddWithValue("@ShowLogo", "0");
        //}
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/AgreementList.aspx");
    }

    [WebMethod]
    public static string deleteAgreement(string AgreementKey)
    {

        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "DELETE from Agreement where AgreementKey=@AgreementKey ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@AgreementKey", AgreementKey);
            DA.ExecuteNonQuery(cmd);
            return "DeleteSuccess";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }


    }

    protected void btn_Update_Click1(object sender, EventArgs e)
    {
        string agreement_key = Request.QueryString["agreement_key"];

        //  string userKey = "15702C1E-1447-4C08-9773-7BD324256180";
        //  string str_Sql = "UPDATE transactions set transaction_id=@transaction_id, transaction_line=@transaction_line,  where transaction_key=@transaction_key  ";

        //string str_Sql = "UPDATE Agreement set   AgreementName=@AgreementName, DocTitle=@DocTitle, Headers=@Headers, IncludeRecipient=@IncludeRecipient, " +
        //    "IncludeCommission=@IncludeCommission, Footer=@Footer,showSignature=@showSignature, IsDefault=@IsDefault,ShowLogo=@ShowLogo, ModifiedOn=@ModifiedOn, ModifiedBy =@ModifiedBy where AgreementKey=@AgreementKey  ";

        string str_Sql = "UPDATE Agreement set   AgreementName=@AgreementName, Headers=@Headers,  " +
          "IsDefault=@IsDefault, ModifiedOn=@ModifiedOn, ModifiedBy =@ModifiedBy where AgreementKey=@AgreementKey  ";


        if (chb_DefaultAgreement.Checked == true)
        {
            str_Sql = "update Agreement set IsDefault=0 ;" +
               "UPDATE Agreement set   AgreementName=@AgreementName, Headers=@Headers,  " +
          "IsDefault=@IsDefault, ModifiedOn=@ModifiedOn, ModifiedBy =@ModifiedBy where AgreementKey=@AgreementKey ";
        }
        SqlCommand cmd = new SqlCommand(str_Sql);


        cmd.Parameters.AddWithValue("@AgreementKey", agreement_key);
        cmd.Parameters.AddWithValue("@AgreementName", txt_AgreementName.Value);
    //    cmd.Parameters.AddWithValue("@DocTitle", txt_DocTitle.Value);
        cmd.Parameters.AddWithValue("@Headers", hdn_Header.Value);
     //   cmd.Parameters.AddWithValue("@IncludeRecipient", dd_IncludeRecipient.Value);
     //   cmd.Parameters.AddWithValue("@IncludeCommission", dd_IncludeCommission.Value);
     //   cmd.Parameters.AddWithValue("@Footer", hdn_Footer.Value);
        //if (chb_ShowSignature.Checked == true)
        //{
        //    cmd.Parameters.AddWithValue("@showSignature", "1");
        //}
        //else
        //{
        //    cmd.Parameters.AddWithValue("@showSignature", "0");
        //}

        if (chb_DefaultAgreement.Checked == true)
        {
            cmd.Parameters.AddWithValue("@IsDefault", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@IsDefault", "0");
        }
        //if (chb_Showlogo.Checked == true)
        //{
        //    cmd.Parameters.AddWithValue("@ShowLogo", "1");
        //}
        //else
        //{
        //    cmd.Parameters.AddWithValue("@ShowLogo", "0");
        //}
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

       
        this.DA.ExecuteNonQuery(cmd);

        //Response.Redirect("../Web/Agreement.aspx?agreement_key=" + agreement_key + "&IsEdit=1");
        Response.Redirect("../Web/AgreementList.aspx");
    }


    [WebMethod]
    public static string ValidateAgreement(string AgreementName)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "select * from Agreement where AgreementName=@AgreementName ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@AgreementName", AgreementName);
            DataTable dt = DA.GetDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                return "0";
            }
            else
            {
                return "1";
            }

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}