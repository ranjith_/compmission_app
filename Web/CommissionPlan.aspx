﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="CommissionPlan.aspx.cs" Inherits="Web_CommissionPlanEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class="container-fluid">
     
    <div class="row">
        <!--Data List-->
            <div class="col-lg-12 col-md-12">
                <div class="card border-top-2 border-top-blue-600">
                    <div class="card-header header-elements-inline  ">
                        <h5 class="card-title">Commission Plan </h5>
                    </div>
                    <div class="card-body">
                            <table id="RecipientsCommissionPlan" class="table  datatable-basic">
                    <thead>
                        <tr class="p-2">
                            <th>Rep ID</th>
                            <th> Rep Name</th>
                            <th>Job Title</th>
                            <th>Reporting To</th>
                            <th>Territory</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="ph_CommissionRecipientPlanTr" runat="server"></asp:PlaceHolder>
                    </tbody>
                </table>
                    </div>
                </div>
                                   
            </div>


        </div>
         </div>
</asp:Content>

