﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="Config.aspx.cs" Inherits="Web_Config" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Config</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-6 col-md-6">
                                    <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Config </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                        <a href="Config.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                    
                                
                            
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Configuration Name</th>
                                        <th>Configuration Text</th>
                                        <th>Created On</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_ConfigTr" runat="server"></asp:PlaceHolder>
                                        
                                </tbody>
                            </table>
                    </div>
                                </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                 <!--Input Form-->
                                 <div id="div_InputForm" runat="server" class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                            <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"> Create Configuration</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                          <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Configuration Name<span class="text-danger">*</span></label>
                                                <input type="text" id="txt_ConfigName" runat="server" required class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Config Text <span class="text-danger">*</span></label>
                                                <input type="text" id="txt_ConfigText1" runat="server"  class="form-control" >
                                            </div>
                                           
                            </div>
                                        </div>
                                    </div>
                               
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->
                                <!--View Data-->
                                 <div id="div_ConfigDetails" runat="server" visible="false" class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                                    <div class="card ">
                            
                                <asp:PlaceHolder ID="ph_ConfigDetails" runat="server"></asp:PlaceHolder>
                                
                               <hr />
                                <div class="table-responsive border-2 border-blue-300"  style=" border-radius: 15px ">
                                    <table class="table ">
                                        <thead class="bg-blue-300">
                                        <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                        <th>Sequence</th>
                                        <th>Created On</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <asp:PlaceHolder ID="ph_ConfigDetailsList" runat="server">

                                            </asp:PlaceHolder>
                                           
                                        </tbody>
                                    </table>
                                </div>
				              </div>
                                    </div>
                                
                                </div>
                                <!--view data-->
                            </div>
                        </div>

   
</asp:Content>

