﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class Web_IframePeriodList : System.Web.UI.Page
{
    Common Com;
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();

        this.Com = new Common();

        loadPeriods(Com.GetQueryStringValue("CalendarKey"));
    }

    private void loadPeriods(string calendarKey)
    {
        string str_Sql = "select  convert(varchar, StartDate, 101) as sDate,convert(varchar, EndDate, 101) as eDate,*" +
            "from PeriodMaster where CalendarKey = @CalendarKey order by PeriodNumber";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CalendarKey", calendarKey);

        DataSet ds = this.DA.GetDataSet(cmd);

        this.PHT.LoadGridItem(ds, ph_PeriodList, "DivPeriodListTr.txt", "");
    }
}