﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" AutoEventWireup="true" CodeFile="CommissionType.aspx.cs" Inherits="Web_CommissionType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
     <div class=" panel-body row">
                                 <h6>Select Commision Type</h6>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            
                                            <div class="col-md-2">
                                                <img style="height:150px;" class="img-thumbnail" src="../UploadFile/Image/commission_users.png">
                                            </div>
                                        <div class="col-md-8">
                                                <div class="form-check">
                                                    <h4>
                                                    <label class="form-check-label">
                                                        <input class="mr-2" id="rd_direct" name="commission_type"  type="radio" >Direct
                                                    </label>
                                                    </h4>
                                                </div>
                                                
                                                <p class="ml-4">There are important differences between commissions and fees, at least in the way these words are used to describe  </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <img style="height:150px;" class="img-thumbnail" src="../UploadFile/Image/commission_users.png">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="d-flex">
                                                <div class="form-check ">
                                                    <h4>
                                                    <label class="form-check-label">
                                                      <input class="mr-2" id="rd_ManagerOverride" name="commission_type"  type="radio" >Manager Override
                                                    </label>
                                                    </h4>
                                                </div>
                                                <div class="form-group ml-3 col-md-6  ">
                                                    <input placeholder="Search override" type="text" class="form-control">
                                                </div>
                                                    <div class=" ">
                                                         <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalOverride"> <i class="icon-menu"></i></button>
                                                    </div>
                                                    </div>
                                                <p class="ml-4">There are important differences between commissions and fees, at least in the way these words are used to describe professional advisors in the financial services industry. </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <img style="height:150px;" class="img-thumbnail" src="../UploadFile/Image/commission_users.png">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-check">
                                                    <h4>
                                                    <label class="form-check-label">
                                                      <input class="mr-2" id="rd_Team" name="commission_type"  type="radio" >Team
                                                    </label>
                                                    </h4>
                                                </div>
                                                
                                                <p class="ml-4">There are important differences between commissions and fees, at least in the way these words are used to describe professional advisors in the financial services industry. A commission-based advisor or broker  </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            
                                            <div class="col-md-2">
                                                <img style="height:150px;" class="img-thumbnail" src="../UploadFile/Image/commission_users.png">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-check">
                                                    <h4>
                                                    <label class="form-check-label">
                                                      <input class="mr-2" id="rd_DealBased" name="commission_type"  type="radio" >Deal Based
                                                    </label>
                                                    </h4>
                                                </div>
                                                
                                                <p class="ml-4">There are important differences between commissions and fees, at least in the way these words are used to describe professional advisors in the financial services industry. </p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
</asp:Content>

