﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GlobalError.aspx.cs" Inherits="Web_GlobalError" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
   <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Template/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="../Limitless_Bootstrap_4/Documentation/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/main/jquery.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
	<script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/validation/validate.min.js"></script>
    <script src="../Limitless_Bootstrap_4/Template/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	

	<script src="../Limitless_Bootstrap_4/Template/layout_1/LTR/default/full/assets/js/app.js"></script>
	<%--<script src="../Limitless_Bootstrap_4/Template/global_assets/js/demo_pages/login_validation.js"></script>--%>
	<!-- /theme JS files -->

    <title>Oops Error | Compmission</title>
    <style>
        .error{
           background-image: url(../UploadFile/Logo/error1.svg);
           background-size:cover;

        }
        .full{
            height:100vh;
        }
        .img-logo{
            height:5em;
        }
        .h1-error{
            /*font-family: monospace;*/
    font-size: 6vh;
 
        }
    </style>
</head>
<body>
  <div class="container-fluid">
      <div class="row full">
          <div class="col-md-7">
            <div class="row mt-md-5  ml-md-5">
                <div class="col-md-12">
                    <img class="img-logo" src="../UploadFile/Image/CommissionLogoDark.png" />
                </div>
                <div class="col-md-12 mt-md-5">
                    <h1 class="h1-error text-dark">Oops ! Someting went Wrong</h1>
                    <h3 class="text-muted mt-3">Sorry, We had some technical problems during your last operation. </h3>
                </div>
            </div>
          </div>
           <div class="col-md-5 my-auto">
            <img class="img-fluid p-md-5" src="../UploadFile/Image/error.png" />
          </div>
      </div>
         
          
  </div>
</body>
</html>
