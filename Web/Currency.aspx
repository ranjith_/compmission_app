﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static"  AutoEventWireup="true" CodeFile="Currency.aspx.cs" Inherits="Web_Currency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Currency</title>

    <script type="text/javascript">

        function viewCurrency(Currency_Key) {
            //alert(Currency_Key);
            $.ajax({
                type: "POST",
                url: "../Web/Currency.aspx/viewCurrency",
                data: "{CurrencyKey:'" + Currency_Key + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    //alert(data.d);
                    $('#card-currency').html(data.d);
                    $('#div_InputForm').addClass('d-none');
                    $('#div_CurrencyDetails').removeClass('d-none');

                },
                error: function () {
                    alert('Error communicating with server');
                }
            });
        }
        function deleteCurrency(Currency_Key, Currency_Name) {
            var confirmation = confirm("are you sure want to delete this " + Currency_Name + " currency ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/Currency.aspx/deleteCurrency",
                    data: "{CurrencyKey:'" + Currency_Key + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Currency " + Currency_Name + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }

        function ValidateCurrencyCode() {
            //alert(Currency_Key);
            let CurrencyCode = $('#txt_CurrencyCode').val();

            if (CurrencyCode != "") {
                $.ajax({
                    type: "POST",
                    url: "../Web/Currency.aspx/ValidateCurrencyCode",
                    data: "{CurrencyCode:'" + CurrencyCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data.d == "0") {
                            $('#btn_Save').prop('disabled', 'true');
                            $('#btn_Update').prop('disabled', 'true');
                            // $('#btn_Update').addClass('disabled');
                            $('#txt_CurrencyCode-error').addClass(' validation-invalid-label');
                            $('#txt_CurrencyCode-error').removeClass(' validation-valid-label');
                            $('#txt_CurrencyCode-error').text('Currency Code  Already Exists');



                        }
                        else if (data.d == "1") {
                            //$('#btn_Save').removeClass('disabled');
                            //$('#btn_Update').removeClass('disabled');
                            $('#btn_Save').removeAttr('disabled')
                            $('#btn_Update').removeAttr('disabled')

                            $('#txt_CurrencyCode-error').removeClass(' validation-invalid-label');
                            $('#txt_CurrencyCode-error').addClass('d-none');


                        }

                    },
                    error: function () {
                        alert('Error communicating with server');
                        return false;
                    }
                });
            }


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
   
    <div class="container-fluid">
<div class="row">
    <!--Data List-->
    <div class="col-lg-6 col-md-6">
        <!-- Basic datatable -->
<div class="card ">
<div class="card-header header-elements-inline">
    <h5 class="card-title">Currency 
       
    </h5>
    <div class="header-elements">
        <div class="list-icons">
             <a href="Currency.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>                       
            <a class="list-icons-item" data-action="collapse"></a>
            <a class="list-icons-item" data-action="reload"></a>
                                        
        </div>
    </div>
</div>
    
<div class="card-body">
              

<div class="table-responsive">
<table class="table datatable-basic">
    <thead>
        <tr>
            <th>Currency Code</th>
            <th>Currency Name</th>
            <th>Rate</th>
            <th>Created On</th>
            <th>Action</th>             
        </tr>
                                    
    </thead>
    <tbody>
                                   
            
           <asp:PlaceHolder ID="ph_CurrencyListTr" runat="server"></asp:PlaceHolder>
    </tbody>
</table>
</div>
    </div>
</div>
<!-- /basic datatable -->
    </div>
        <!--Data List-->
        <!--Input Form-->
        <div id="div_InputForm" runat="server"  class="col-lg-6 col-md-6">
            <!-- Basic layout-->
<div class="card ">
<div class="card-header header-elements-inline">
    <h5 class="card-title">Add Currency</h5>
    <div class="header-elements">
        <div class="list-icons">
            <asp:Button ID="btn_Save" Text="Save" OnClick="btn_Save_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
            <asp:Button ID="btn_Update" Text="Update" Visible="false" OnClick="btn_Update_Click" runat="server" CssClass="btn btn-primary  btn-sm " />
            <a class="list-icons-item" data-action="collapse"></a>
            <a class="list-icons-item" data-action="reload"></a>
                                        
        </div>
    </div>
</div>
    
<div class="card-body">
                <div class="row">
                <div class="form-group col-md-6">
                    <label>Currency Code<span class="text-danger">*</span></label>
                    <input id="txt_CurrencyCode" onfocusout="ValidateCurrencyCode();" required runat="server" type="text" class="form-control" />
                </div>
                <div class="form-group col-md-6">
                    <label>Currency Name<span class="text-danger">*</span></label>
                    <input id="txt_CurrencyName" required runat="server" type="text" class="form-control" />
                </div>
                <div class="form-group col-md-6">
                    <label>Rate<span class="text-danger">*</span></label>
                    <input id="txt_Rate" required runat="server" type="text" class="form-control number" />
                </div>
               
                </div>
            </div>
        </div>
    <!-- /basic layout -->
    </div>
    <!--Input Form-->

                                 <!--View Data-->
                                 <div id="div_CurrencyDetails" runat="server" class="col-lg-6 col-md-6 d-none">
                                        <!-- Basic layout-->
                                    <div class="card" id="card-currency">
                            
                                      <asp:PlaceHolder ID="ph_CurrencyDetails" runat="server"></asp:PlaceHolder>
                                
                                    </div>
                                
                                </div>
                                <!--view data-->
</div>
</div>
    
</asp:Content>

