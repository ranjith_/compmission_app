﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EmptyMaster.master" AutoEventWireup="true" CodeFile="CommissionCredit.aspx.cs" Inherits="Web_CommissionCredit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="row">
         <div class="form-group col-md-6">
                                                
                                                <p>Source of Credit</p>
                                                <div class="credit row">
                                               
                                                <div class="form-group col-md-4">
                                                    <input id="txt_CreditPercentage" runat="server" type="text" class="form-control required">
                                                </div>
                                                <div class="form-group col-md-1 text-center">
                                                   <label> % of</label>
                                                </div>
                                                <div class="form-group col-md-5">
                                                     <select id="dd_CreditField" runat="server" class="form-control">
                                                        <option value="">-- select one --</option>
                                                        <option value="Sale Amount">Sale Amount</option>
                                                        <option value="Advance">Advance</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-2 d-none">
                                                   <button class="btn btn-primary btn-xs"><i class="icon-plus22"></i></button>
                                                </div>
                                               </div>
                                            </div>    
                                           
                                            
                                            <div class="form-group col-md-6">
                                                    <label>Set Split</label>
                                                    <div class="d-flex">
                                                        <input id="txt_Split" type="text" class="form-control typeahead-split" placeholder="Search Split ">
                                                   <%-- <input placeholder="type to earch split..." type="text" class="form-control required">--%>
                                                    <button type="button" class="btn btn-primary btn-sm ml-2" data-toggle="modal" data-target="#modalSplit"> <i class="icon-menu"></i></button>
                                                    <a data-toggle="collapse" class="btn btn-default btn-sm ml-2 bg-light text-dark collapsed" data-parent="" href="#accordio3"><i class="icon-arrow-down22"></i></a>
                                                    </div>
                                                    
                                    
                                            </div>
    </div>
</asp:Content>

