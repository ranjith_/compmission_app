﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="Imports.aspx.cs" Inherits="Web_Imports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Imports</title>
    <style>
        .b-l-1{
            border-right: 1px solid rgb(218, 216, 216);
        }
        
       #addimport>tbody>tr>th:nth-child(1),#addimport>tbody>tr>td:nth-child(1),#addimport>tbody>tr>td:nth-child(2){
background-color:#039BE5;
        color:white;
}

#addimport>tbody>tr>th:nth-child(2),#addimport>tbody>tr>td:nth-child(3),#addimport>tbody>tr>td:nth-child(4),#addimport>tbody>tr>td:nth-child(5){
background-color:#4FC3F7;
        color:white;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
       <div class="container-fluid">
                            <div class="row">
                                <!--Data List-->
                                <div class="col-lg-6 col-md-6">
                                    <!-- Basic datatable -->
                   <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="panel-title">Imports
                                   
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                        
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                   
                            
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        <th>Import</th>
                                        <th>Description</th>
                                        <th>File Name</th>
                                        <th>Record Inserted</th>
                                        <th>Created On</th>
                                        
                                    
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                      <asp:PlaceHolder ID="ph_ImportListTr" runat="server"></asp:PlaceHolder>
                                       
                                        
                                </tbody>
                            </table>
                    </div>
                                </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->
                                 <!--Input Form-->
                                 <div class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                            <div class="card ">
                                    <div class="card-header header-elements-inline">
                                        <h5 class="card-title">Import</h5>
                                        <div class="header-elements">
                                            <div class="list-icons">
                                                <a class="list-icons-item" data-action="collapse"></a>
                                                <a class="list-icons-item" data-action="reload"></a>
                                                
                                            </div>
                                        </div>
                                    </div>
            
                                    <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Name<span class="text-danger">*</span></label>
                                                <input id="txt_Name" runat="server" type="text" class="form-control" >
                                            </div>
                                            
                                            <div class="form-group col-md-6">
                                                    <label>Description <span class="text-danger">*</span></label>
                                                    <input id="txt_Desc" runat="server" type="text" class="form-control" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Source Type<span class="text-danger">*</span></label>
                                                <select id="dd_SourceType" runat="server" class="form-control">
                                                <option value="">-- select one --</option>
                                                    <option value="Excel">Excel .csv</option>
                                                    <option value="QuicBooks">QuickBooks</option>
                                                    
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Upload Source -File /DAP<span class="text-danger">*</span></label>
                                                <asp:FileUpload ID="fu_ImportFile" runat="server" />
                                            </div>
                                            
                                            
                                            <div class=" col-md-12 text-right ">
                                                <button id="btn_ConnectFile" name="btn_ConnectFile" type="button" runat="server" onserverclick="btn_ConnectFile_ServerClick" class="btn btn-primary"> CONNECT</button>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Basic form panel--->
                                    <div class="card d-none ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title"></h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                             <div class="table-responsive" style="border-radius: 10px;">
                                                    <table class="table" id="addimport">
                                                        
                                                     <tr>
                                                        <th colspan="2">
                                                            <div class="form-group ">
                                                            <label>Destination</label>
                                                            <select class="form-control">
                                                            <option>-- select destination --</option>
                                                                <option>Destination 1</option>
                                                                <option>Destination 2</option>
                                                                <option>Destination 3</option>
                                                                <option>Destination 4</option>
                                                            </select>
                                                            </div>
                                                        </th>
                                                        <th colspan="3">
                                                            <div class="form-group">
                                                                <label>Source</label>
                                                                <select class="form-control">
                                                                <option>-- select source --</option>
                                                                    <option>Source 1</option>
                                                                    <option>Source 2</option>
                                                                    <option>Source 3</option>
                                                                    <option>Source 4</option>
                                                                </select>
                                                            </div>
                                                        </th>
                                                      </tr>
                                                      <tr>
                                                        <td>Field Name </td>
                                                        <td>Data Type</td>
                                                        <td>
                                                            Filed Name
                                                        </td>
                                                        <td>
                                                            Datatype
                                                        </td>
                                                        <td>
                                                            <label>action</label>
                                                            
                                                        </td>
                                                      </tr>
                                                      <!-- /field id-->
                                                      <tr>
                                                        <td>Field 1</td>
                                                        <td>int</td>
                                                        <td>
                                                            
                                                                <div class="form-group">
                                                            
                                                            <select class="form-control">
                                                                <option>-- select name --</option>
                                                                <option>Balaji</option>
                                                                <option>Rajesh</option>
                                                                <option>Ranjith</option>
                                                                <option>Sandy</option>
                                                                <option>John</option>
                                                            </select>
                                                    
                                                              
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                
                                                                <select class="form-control">
                                                                    <option>-- select datatype --</option>
                                                                    <option>Int</option>
                                                                    <option>Varchar</option>
                                                                    <option>DateTime</option>
                                                                    <option>Char</option>
                                                                    
                                                                </select>
                                                                
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="list-icons">
                                                                <div class="dropdown">
                                                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                        <i class="icon-menu9"></i>
                                                                    </a>

                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        
                                                                        <a href="#" class="dropdown-item"><i class="icon-pencil3"></i> Convert</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                      </tr>
                                                      <!-- /filed name--->
                                                        <tr>
                                                        <td>Field 2</td>
                                                        <td>Varchar</td>
                                                        <td>
                                                            <div class="form-group">
                                                               
                                                                <select class="form-control">
                                                                    <option>-- select address --</option>
                                                                    <option>Flat No 5, Tharamani Road, Chennai</option>
                                                                    <option>Flat No 3, Velachery Road, Chennai</option>
                                                                    <option>Block No 2, Guindy Road, Chennai</option>
                                                                    <option>Flat No 3, CMBT Road, Chennai</option>
                                                                    <option>Flat No 5, OMR Road, Chennai</option>
                                                                </select>
                                                                
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                               
                                                                <select class="form-control">
                                                                    <option>-- select datatype --</option>
                                                                    <option>Int</option>
                                                                    <option>Varchar</option>
                                                                    <option>DateTime</option>
                                                                    <option>Char</option>
                                                                    
                                                                </select>
                                                                
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="list-icons">
                                                                <div class="dropdown">
                                                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                        <i class="icon-menu9"></i>
                                                                    </a>

                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        
                                                                        <a href="#" class="dropdown-item"><i class="icon-pencil3"></i> Convert</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                      </tr>
                                                      <!-- /filed address--->
                                                        <tr>
                                                        <td>Field 3</td>
                                                        <td>DateTime</td>
                                                        <td>
                                                            <div class="form-group">
                                                                
                                                                <select class="form-control">
                                                                    <option>-- select zipcode --</option>
                                                                    <option>600057</option>
                                                                    <option>600042</option>
                                                                    <option>600025</option>
                                                                    <option>600012</option>
                                                                    <option>600020</option>
                                                                </select>
                                                                
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                
                                                                <select class="form-control">
                                                                    <option>-- select datatype --</option>
                                                                    <option>Int</option>
                                                                    <option>Varchar</option>
                                                                    <option>DateTime</option>
                                                                    <option>Char</option>
                                                                    
                                                                </select>
                                                                
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="list-icons">
                                                                <div class="dropdown">
                                                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                        <i class="icon-menu9"></i>
                                                                    </a>

                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        
                                                                        <a href="#" class="dropdown-item"><i class="icon-pencil3"></i> Convert</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                      </tr>
                                                      <!-- /field zipcode--->
                                                    </table>
                                            </div>
                                        </div>
                                    </div>
                                <!-- /basic layout -->
                                </div>
                                <!--Input Form-->
                            </div>
                        </div>
</asp:Content>

