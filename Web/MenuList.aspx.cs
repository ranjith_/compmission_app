﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Web_MenuList : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.Com = new Common();

        loadMenuList();
        if (Request.QueryString["menu_key"] != null)
        {
            viewMenuDetails(Request.QueryString["menu_key"]);
            div_MenuDetails.Visible = true;
            div_InputForm.Visible = false;
        }

        if (!IsPostBack)
        {
            string str_Select = "select MenuKey as Value,MenuName as Text from Menu where MenuType=2;select Name as Text,Value from v_ListMenuType order by value";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.Com.LoadDropDown(dd_ParentMenuId, ds.Tables[0], "Text", "Value", true, "--Parent Menu--");
            this.Com.LoadDropDown(dd_MenuType, ds.Tables[1], "Text", "Value", true, "--Menu Type--");
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["menu_key"] != null && Request.QueryString["isEdit"] == "1")
            {
                loadMenuDetailToEdit(Request.QueryString["menu_key"]);
                div_MenuDetails.Visible = false;
                div_InputForm.Visible = true;
                btn_Save.Visible = false;
                btn_Update.Visible = true;
            }
        }
    }

    private void loadMenuDetailToEdit(string MenuKey)
    {
        string str_Sql_view = "select * from Menu where  MenuKey =@MenuKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@MenuKey", MenuKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {

            //( MenuKey, MenuName, MenuDescription, ParentMenuId, MenuType, TargetUrl, IsActive, IsNewWindow, MenuOrder, MenuIcon, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy, DeveloperNotes, )


            txt_MenuName.Value = dr["MenuName"].ToString();
            txt_MenuDescription.Value = dr["MenuDescription"].ToString();
            dd_ParentMenuId.SelectedValue= dr["ParentMenuId"].ToString();
            dd_MenuType.SelectedValue = dr["MenuType"].ToString();
            txt_TargetUrl.Value = dr["TargetUrl"].ToString();
            txt_MenuOrder.Value = dr["MenuOrder"].ToString();
            txt_MenuIcon.Value = dr["MenuIcon"].ToString();

            string cb = dr["IsActive"].ToString();

            if (dr["IsActive"].ToString() == "True")
            {
                chb_IsActive.Checked = true;
            } 
            if (dr["IsNewWindow"].ToString() == "True")
            {
                chb_IsNewWindow.Checked = true;
            }


        }
    }

    private void viewMenuDetails(string MenuKey)
    {
        string str_Sql_view = "select *,(select MenuName  from Menu m2 where m1.ParentMenuId=m2.MenuKey) as ParentMenuName  from menu m1 where MenuKey =@MenuKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@MenuKey", MenuKey);
        DataSet ds2 = this.DA.GetDataSet(sqlcmd);
        this.PHT.LoadGridItem(ds2, ph_MenuDetails, "DivMenuView.txt", "");
    }

    private void loadMenuList()
    {
        string str_Sql = "select *, (select MenuName from Menu m2 where m1.ParentMenuId=m2.MenuKey) as ParentMenuName from Menu m1 order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        DataSet ds = this.DA.GetDataSet(cmd);
        this.PHT.LoadGridItem(ds, ph_MenuListTr, "MenuListTr.txt", "");
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";

        string isActive = "0", isNewWindow = "0";

        if (chb_IsActive.Checked == true) { isActive = "1"; }
        if (chb_IsNewWindow.Checked == true) { isNewWindow = "1"; }


        string str_Key = Guid.NewGuid().ToString();

        //string trans_Key = Guid.NewGuid().ToString();
        string str_Sql = "insert into Menu (MenuKey, MenuName, MenuDescription, ParentMenuId, MenuType, TargetUrl, IsActive, IsNewWindow, MenuOrder, MenuIcon, CreatedOn, CreatedBy)"
                            + "select @MenuKey, @MenuName, @MenuDescription, @ParentMenuId, @MenuType, @TargetUrl, @IsActive, @IsNewWindow, @MenuOrder, @MenuIcon, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@MenuKey", str_Key);

        cmd.Parameters.AddWithValue("@MenuName", txt_MenuName.Value);
        cmd.Parameters.AddWithValue("@MenuDescription", txt_MenuDescription.Value);
        if(dd_ParentMenuId.SelectedValue=="")
        cmd.Parameters.AddWithValue("@ParentMenuId", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ParentMenuId", dd_ParentMenuId.SelectedValue);
        cmd.Parameters.AddWithValue("@MenuType", dd_MenuType.SelectedValue);
        cmd.Parameters.AddWithValue("@TargetUrl", txt_TargetUrl.Value);

        cmd.Parameters.AddWithValue("@IsActive", isActive);
        cmd.Parameters.AddWithValue("@IsNewWindow", isNewWindow);
        cmd.Parameters.AddWithValue("@MenuOrder", txt_MenuOrder.Value);
        cmd.Parameters.AddWithValue("@MenuIcon", txt_MenuIcon.Value);
        cmd.Parameters.AddWithValue("@CreatedBy", userKey);
        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect(Request.RawUrl);
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string MenuKey = Request.QueryString["menu_key"].ToUpper();
        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";

        //( MenuKey, MenuName, MenuDescription, ParentMenuId, MenuType, TargetUrl, IsActive, IsNewWindow, MenuOrder, MenuIcon, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy, DeveloperNotes, )


        string str_Sql = "UPDATE Menu set MenuName=@MenuName, MenuDescription=@MenuDescription, ParentMenuId=@ParentMenuId, MenuType=@MenuType, TargetUrl=@TargetUrl," +
            " IsActive=@IsActive, IsNewWindow=@IsNewWindow, MenuOrder=@MenuOrder, MenuIcon=@MenuIcon," +
           "  ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where MenuKey=@MenuKey  ";

        SqlCommand cmd = new SqlCommand(str_Sql);

        string isActive = "0", isNewWindow = "0";

        if (chb_IsActive.Checked == true) { isActive = "1"; }
        if (chb_IsNewWindow.Checked == true) { isNewWindow = "1"; }

        cmd.Parameters.AddWithValue("@MenuName", txt_MenuName.Value);
        cmd.Parameters.AddWithValue("@MenuDescription", txt_MenuDescription.Value);
        if (dd_ParentMenuId.SelectedValue == "")
            cmd.Parameters.AddWithValue("@ParentMenuId", DBNull.Value);
        else
            cmd.Parameters.AddWithValue("@ParentMenuId", dd_ParentMenuId.SelectedValue);
        cmd.Parameters.AddWithValue("@MenuType", dd_MenuType.SelectedValue);
        cmd.Parameters.AddWithValue("@TargetUrl", txt_TargetUrl.Value);

        cmd.Parameters.AddWithValue("@IsActive", isActive);
        cmd.Parameters.AddWithValue("@IsNewWindow", isNewWindow);
        cmd.Parameters.AddWithValue("@MenuOrder", txt_MenuOrder.Value);
        cmd.Parameters.AddWithValue("@MenuIcon", txt_MenuIcon.Value);

        cmd.Parameters.AddWithValue("@ModifiedBy", userKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@MenuKey", MenuKey);
        this.DA.ExecuteNonQuery(cmd);

        Response.Redirect("../Web/MenuList.aspx?menu_key=" + MenuKey + "");
    }
}