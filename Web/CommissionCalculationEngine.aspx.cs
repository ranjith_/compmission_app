﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class Web_CommissionCalculationEngine : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        loadFiscalYear();
        loadCommissions();
    }

    private void loadCommissions()
    {
        string str_Select = "Select CommissionName as Text, CommissionKey as Value from Commission order by CreatedOn Desc";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_Commission, ds.Tables[0], "Text", "Value", false, "");
    }
    private void loadFiscalYear()
    {
        string str_Select = "Select * from v_CalendarFiscalYear order by Text";
        SqlCommand cmm = new SqlCommand(str_Select);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_FiscalYear, ds.Tables[0], "Text", "Value", true, "--Select Year --");
    }
    [WebMethod]
    public static string loadOptionPeriod(string CalendarKey)
    {
        DataAccess DA = new DataAccess();
        string str_Sql = "select * from v_Periods where CalendarKey=@CalendarKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("CalendarKey", CalendarKey);
        DataTable dt = DA.GetDataTable(sqlcmd);
        string optionRole = "";
        foreach (DataRow dr in dt.Rows)
        {

            optionRole += "<option value='" + dr["Value"].ToString() + "'>" + dr["Text"].ToString() + "</option>";
        }
        return optionRole;
    }

    [WebMethod]
    public static string loadOptionCommission(string CalendarKey)
    {
        DataAccess DA = new DataAccess();
        string str_Sql = "select * from v_Periods where CalendarKey=@CalendarKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql);
        sqlcmd.Parameters.AddWithValue("CalendarKey", CalendarKey);
        DataTable dt = DA.GetDataTable(sqlcmd);
        string optionRole = "";
        foreach (DataRow dr in dt.Rows)
        {

            optionRole += "<option value='" + dr["Value"].ToString() + "'>" + dr["Text"].ToString() + "</option>";
        }
        return optionRole;
    }
    [WebMethod]
    public static string ExecuteCommissionCalculation(string PeriodKey,string CommissionKeys)
    {
        string[] Commissions = CommissionKeys.Split(',');
        string SuccessMessage="";
        var ErrorMessage = new List<string>();

        foreach (string com in Commissions)
        {
            try
            {
                DataAccess DA = new DataAccess();
                SessionCustom SC = new SessionCustom();
                string str_Sql = "p_ExecuteCalculationProcess";
                SqlCommand sqlcmd = new SqlCommand(str_Sql);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@PeriodKey", PeriodKey);
                sqlcmd.Parameters.AddWithValue("@CommissionKey", com);
                sqlcmd.Parameters.AddWithValue("@UserKey", SC.UserKey);
                DataSet ds = DA.GetDataSet(sqlcmd);
                DataRow dr = ds.Tables[0].Rows[0];

                if (dr["ErrorMessage"].ToString() != "")
                {
                    ErrorMessage.Add(dr["ErrorMessage"].ToString());
                }
                else
                {
                    SuccessMessage= "Executed";
                }
            }
            catch(Exception e)
            {
                return e.ToString();
            }
        }

        var ErrorArray = ErrorMessage.ToArray();

        if (ErrorArray.Length > 0)
        {
            return string.Join(",", ErrorArray);
        }
        else
        {
            return SuccessMessage;
        }
       
    }
}