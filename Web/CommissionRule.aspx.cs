﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
public partial class Web_CommissionRule : System.Web.UI.Page
{
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    Common Com;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        this.OCF = new OtherCommonFunction();
        this.Com = new Common();
        if (!IsPostBack)
        {
            loadVariableTypeDropDown();
            loadFunctionDropDown(Com.GetQueryStringValue("RuleType"));
            hdn_RuleKey.Value = Com.GetQueryStringValue("RuleKey");
            hdn_CommissionKey.Value = Com.GetQueryStringValue("CommissionKey");
            if (Com.GetQueryStringValue("isEdit") == "1" && Com.GetQueryStringValue("RuleKey") != "")
            {
                btn_UpdateRule.Visible = true;
                btn_SaveRule.Visible = false;
                LoadFunctionToEdit(Com.GetQueryStringValue("RuleKey"));


            }
        }
       
        span_Sequence.InnerText = Com.GetQueryStringValue("Sequence");
        //select * from v_RuleFunctions where TextOne='Calculation' order by seq
    }

    private void LoadFunctionToEdit(string RuleKey)
    {
        string str_sql = "select * from CommissionRuleFunction where RuleKey=@RuleKey";
        SqlCommand sqlcmd = new SqlCommand(str_sql);
        sqlcmd.Parameters.AddWithValue("@RuleKey", RuleKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);

        foreach (DataRow dr in dt.Rows)
        {
            span_Sequence.InnerText = dr["RuleSequence"].ToString(); ;
            txt_RuleName.Value = dr["RuleName"].ToString();
            txt_VariableName.Value = dr["VariableName"].ToString();
            ddl_Functions.SelectedValue = dr["FunctionType"].ToString();
            ddl_VariableType.SelectedValue = dr["VariableScope"].ToString();
            txt_FunctionDetails.Value = dr["FunctionDetails"].ToString();
            hdn_FunctionKey.Value = dr["FunctionKey"].ToString();
            btn_EditFunction.Attributes.Add("onclick", "EditFunction('" + Com.GetQueryStringValue("RuleKey") + "','" + Com.GetQueryStringValue("FunctionKey") + "','" + dr["FunctionType"].ToString() + "')");
            btn_EditFunction.Attributes.Add("class", "btn btn-primary btn-sm");
            FunctionName.InnerText= dr["VariableName"].ToString();

        }
    }

    protected void btn_SaveRule_ServerClick(object sender, EventArgs e)
    {
        //CommissionRuleFunction (RuleKey, CommissionKey, RuleType, RuleSequence, RuleName, VariableName, FunctionType, FunctionKey, FunctionDetails, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)

        string RuleKey = Com.GetQueryStringValue("RuleKey");

        string str_Sql = "insert into CommissionRuleFunction(RuleKey, CommissionKey, RuleType,VariableScope, RuleSequence, RuleName, VariableName, FunctionType,FunctionKey, FunctionDetails, CreatedOn, CreatedBy)"
                            + "select @RuleKey, @CommissionKey, @RuleType, @VariableScope,@RuleSequence, @RuleName, @VariableName, @FunctionType,@FunctionKey, @FunctionDetails, @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@RuleKey", RuleKey);
        cmd.Parameters.AddWithValue("@CommissionKey", Com.GetQueryStringValue("CommissionKey"));
        cmd.Parameters.AddWithValue("@RuleType", Com.GetQueryStringValue("RuleType"));
        cmd.Parameters.AddWithValue("@RuleSequence", Com.GetQueryStringValue("Sequence"));
        cmd.Parameters.AddWithValue("@RuleName", txt_RuleName.Value);
        cmd.Parameters.AddWithValue("@VariableName", txt_VariableName.Value);
        cmd.Parameters.AddWithValue("@FunctionType", ddl_Functions.SelectedValue);
        if (hdn_FunctionKey.Value == "")
        {
            cmd.Parameters.AddWithValue("@FunctionKey", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@FunctionKey", hdn_FunctionKey.Value);
        }
       
        if (ddl_VariableType.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@VariableScope", ddl_VariableType.SelectedValue);
        }
        else
        {
            cmd.Parameters.AddWithValue("@VariableScope", DBNull.Value);
        }
        cmd.Parameters.AddWithValue("@FunctionDetails", txt_FunctionDetails.Value);
        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);
        // Response.Redirect("../Web/Commission.aspx?CommissionKey=" + commission_key + "&new=1");
        ///  Response.Redirect("../Web/CommissionView.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey") + "&location=#div_CommissionRule&page=Commission-Init-Details&update=success");
        ///  
        Response.Redirect("../Web/CommissionView.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey"));
    }
    public void loadFunctionDropDown(string RuleType)
    {
        string str_Select = "select Name as Text, Name as Value from v_RuleFunctions order by seq";
        SqlCommand cmm = new SqlCommand(str_Select);
       // cmm.Parameters.AddWithValue("@RuleType", RuleType);
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_Functions, ds.Tables[0], "Text", "Value", true, "--Select Function--");
    }
    public void loadVariableTypeDropDown()
    {
        string str_Select = "select Name as Value , Name as Text from v_CommisssionVariableType order by seq ";
        SqlCommand cmm = new SqlCommand(str_Select);
      
        DataSet ds = this.DA.GetDataSet(cmm);
        this.Com.LoadDropDown(ddl_VariableType, ds.Tables[0], "Text", "Value", true, "--Select Variable--");
    }
    [WebMethod]
    public static string getFunctionDescription(string FunctionType, string FunctionKey)
    {
        DataAccess DA = new DataAccess();
        PhTemplate PHT = new PhTemplate();
        string str_Select = "Select * from SplitCondition where SplitKey=@SplitKey";
        SqlCommand cmd = new SqlCommand(str_Select);
        cmd.Parameters.AddWithValue("@SplitKey", FunctionKey);
        DataTable Dt = DA.GetDataTable(cmd);
        string Description = "";
        foreach (DataRow dr in Dt.Rows)
        {
            if (dr["AttributeType"].ToString() == "1")
            {
                string Condition1 = "for " + dr["Attribute"].ToString() + " = " + dr["VariableName"].ToString() + " ";
                Description += Condition1;
            }
            if (dr["AttributeType"].ToString() == "2")
            {
                string Condition2 = "for " + dr["Attribute"].ToString() + " = " + dr["Percentage"].ToString() + " % of " + dr["TransactionField"].ToString() + " ";
                Description += Condition2;
            }

        }

        return Description;
    }

    protected void btn_UpdateRule_ServerClick(object sender, EventArgs e)
    {
        string RuleKey = Com.GetQueryStringValue("RuleKey");
        string str_Sql = "Update CommissionRuleFunction set  RuleType=@RuleType, VariableScope=@VariableScope, RuleSequence=@RuleSequence, RuleName=@RuleName, VariableName=@VariableName, FunctionType=@FunctionType,FunctionKey=@FunctionKey, FunctionDetails=@FunctionDetails, ModifiedOn=@ModifiedOn, ModifiedBy=@ModifiedBy where RuleKey=@RuleKey";

        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@RuleKey", RuleKey);
        cmd.Parameters.AddWithValue("@RuleType", Com.GetQueryStringValue("RuleType"));
        cmd.Parameters.AddWithValue("@RuleSequence", Com.GetQueryStringValue("Sequence"));
        cmd.Parameters.AddWithValue("@RuleName", txt_RuleName.Value);
        cmd.Parameters.AddWithValue("@VariableName", txt_VariableName.Value);
        if (ddl_VariableType.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@VariableScope", ddl_VariableType.SelectedValue);
        }
        else
        {
            cmd.Parameters.AddWithValue("@VariableScope", DBNull.Value);
        }
        cmd.Parameters.AddWithValue("@FunctionType", ddl_Functions.SelectedValue);
        if (hdn_FunctionKey.Value == "")
        {
            cmd.Parameters.AddWithValue("@FunctionKey", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@FunctionKey", hdn_FunctionKey.Value);
        }

        cmd.Parameters.AddWithValue("@FunctionDetails", txt_FunctionDetails.Value);
        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        DA.ExecuteNonQuery(cmd);
        Response.Redirect("../Web/CommissionView.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey") );
       // Response.Redirect("../Web/CommissionView.aspx?CommissionKey=" + Com.GetQueryStringValue("CommissionKey") + "&location=#div_CommissionRule&page=Commission-Init-Details&update=success");
    }

    [WebMethod]

    public static string validateVariableName(string CommissionKey, string VariableName)
    {
        try
        {
            DataAccess DA = new DataAccess();
            String str_sql = "select * from CommissionRuleFunction where CommissionKey=@CommissionKey and VariableName=@VariableName ";
            SqlCommand cmd = new SqlCommand(str_sql);
            cmd.Parameters.AddWithValue("@CommissionKey", CommissionKey);
            cmd.Parameters.AddWithValue("@VariableName", VariableName);
            DataTable dt = DA.GetDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                return "0";
            }
            else
            {
                return "1";
            }

        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}