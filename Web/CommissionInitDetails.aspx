﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" ClientIDMode="Static" ValidateRequest="false" CodeFile="CommissionInitDetails.aspx.cs" Inherits="Web_CommissionInitDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
     <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
     <style>
        .note-editing-area{
            margin-top:20px!important;
        }
    </style>
    <script>
        //source of credit
        function changeSourceOfCredit() {
            if ($('#chb_IsCreditField').is(':checked') == true) {
                $('#div_Transaction').removeClass('d-none');
                $('#txt_Percentage').addClass('d-none');
            }
            else {
                $('#div_Transaction').addClass('d-none');
                $('#txt_Percentage').removeClass('d-none');
            }
        } 
        function appendDescToHdn() {
            var desc = $('.note-editable').html();
            console.log(desc);
            $('#hdn_DescWYSWYG').val(desc);
           
        }
        function Validate() {
            appendDescToHdn();
            let valid = true;
            if (ValidateCommissionName() == false) {
                valid = false;
            }
            if (checkValidCommissionDate() == false) {
                valid = false;
            }
            return valid;
        }
        function ValidateCommissionName() {
            //alert(Currency_Key);
            let CommissionName = $('#txt_CommissionName').val();
            let hdnCommissionName = $('#hdn_CommissionName').val();

            let valid = true;

            if (CommissionName == hdnCommissionName) {
                valid = true;
            }
            else {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "../Web/CommissionInitDetails.aspx/ValidateCommissionName",
                    data: "{CommissionName:'" + CommissionName + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data.d == "0") {
                            alertNotify('Invalid Commission Name', 'Commission Name ' + CommissionName + ' is already exist', 'bg-danger border-danger');
                            $('#txt_AgreementName-error').addClass('d-none');
                            valid = false;


                        }
                        else if (data.d == "1") {
                            valid = true;
                        }

                    },
                    error: function () {
                        alert('Error communicating with server');
                        valid = false;
                    }
                });
            }

            return valid;
        }

        function checkValidCommissionDate() {
            let sDate = $('#dp_StartDate').val();
            let eDate = $('#dp_EndDate').val();

            var startDate = new Date(sDate);
            var endDate = new Date(eDate);

            var valid = true;
                if (startDate > endDate) {
                    alertNotify('Invalid  Date', 'Commission End Date is greater than start date', 'bg-danger border-danger');
                    $('#dp_EndDate-error').addClass('d-none');
                    $('#dp_StartDate-error').addClass('d-none');
                    valid = false;
                }
                else {
                    valid = true;
                }

            return valid;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-top-2 border-top-primary-400">
                    <div class="card-header header-elements-inline">
                         <h5 class="card-title d-flex">  <span class="ml-2"> <i class="icon-cog52"></i> Commission</span>
                              <div class="form-check form-check-left form-check-switchery form-check-switchery-sm ml-3">
                            <label class="form-check-label"> ON / OFF
                                <input id="chb_CommisssionActive" type="checkbox" runat="server" class="form-input-switchery" />
                            </label>
                            </div>
                        </h5> 
                        <div class="header-elements">
                              <asp:Button  ID="btn_UpdateCommissionInitDetails" runat="server" Visible="false" CssClass="btn btn-primary btn-sm " Text="Update"  onclick="btn_UpdateCommissionInitDetails_Click" OnClientClick="return Validate()" />
                            <asp:Button  ID="btn_SaveCommissionInitDetails" runat="server" CssClass="btn btn-primary btn-sm " Text="Next"  onclick="btn_SaveCommissionInitDetails_ServerClick" OnClientClick="return Validate()" />
                               <%--<button type="button" runat="server" class="btn btn-primary btn-sm " onclick="appendDescToHdn()" id="" onserverclick="btn_SaveCommissionInitDetails_ServerClick">Save</button>--%>
                        </div>
                    </div>
                    <div class="card-body">
                           <div class="row">

                    
                    <div class="col-md-12">

                   
                        <div class="row m-1 card-cusom-commission">
                                <div class="form-group col-md-6">
                                    <label>Commission Name<span class="text-danger">*</span></label>
                                    <input required   type="text" id="txt_CommissionName" runat="server" class="form-control required" >
                                   <asp:HiddenField id="hdn_CommissionKey" ClientIDMode="Static"  runat="server"/>
                                </div>
                                <div class="form-group col-md-6">
                                                <label>Calculation Feq<span class="text-danger">*</span></label>
                                    <asp:DropDownList CssClass="form-control-select2"  id="dd_CalFeq" runat="server"></asp:DropDownList>
                                                
                                             </div>
                            
                            
                            
                               <div class="form-group  col-md-6" >
                                <label style="white-space: nowrap; " class="">Effective Date<span class="text-danger">*</span></label>
                                    <input type="date" id="dp_StartDate" required runat="server" class="form-control"/>
                              
                                </div>
                                <div class="form-group  col-md-6" >
                                <label style="white-space: nowrap; " class="">End Date<span class="text-danger">*</span></label>
                                    <input type="date" id="dp_EndDate" required runat="server" class="form-control"/>
                              
                                </div>
                             <div class="form-group col-md-6">
                                 
                                      <div class="d-flex">           
                                    <label class=" mr-2">Source of Credit</label>
                                         
                                    <div class="form-check">
			                            <label class="form-check-label">
				                            <input onchange="changeSourceOfCredit()" type="checkbox" id="chb_IsCreditField" runat="server" class="form-check-input-styled" >
				                            (Select percentage value from transactions)
			                            </label>
		                            </div>
                                          </div>
                                    <div class="d-flex">
                                        <div id="div_Transaction" runat="server" class="d-none w-100">
                                        <select id="dd_transaction" runat="server" class="d-none form-control-select2 ">
                                    <option value="">-- select one --</option>
                                    <option value="1">Transaction 1</option>
                                    <option value="2">Transaction 2</option>
                                    <option value="3">Transaction 3</option>
                                    <option value="4">Transaction 4</option>
                                </select>
                                            </div>
                                        <input value="100" id="txt_Percentage" runat="server" placeholder="Enter Percentage" type="number" max="100" class="number form-control " style="width:40%;">
                                        <span class="mr-3 ml-3 my-auto" style="white-space:nowrap;"> % of </span>
                                        <select id="dd_CreditOf" runat="server" class="form-control-select2">
                                            <option value="">-- select one --</option>
                                            <option value="Sale Amount">Sale Amount</option>
                                            <option value="Advance">Advance</option>
                                        </select>
                                    </div>
                             </div>

                         
                           
                        </div>
                         <!--WYSIWYG-->
                                        <div class="">
                                            <div class=" col-md-12">
                                                <div class="d-flex ">
                                                    <label class="mr-2">Description</label>
                                                    <div class="form-check">
			                            <label class="form-check-label">
				                            <input onchange="changeSourceOfCredit()" type="checkbox" id="chb_IsOnAgree" runat="server" class="form-check-input-styled" >
				                           Show on Agreement
			                            </label>
		                            </div>
                                                
                                                </div>
                                               
							
                                                <script type="text/javascript">$(document).ready(function () { $('#edit').click(); });</script>
						                </div>
                                        <div class="col-md-12">
                                           <div  id="txt_DescWYSWYG" runat="server"  class="summernote mt-2">
							Write your commission description Here
						                    </div>
                                            <asp:HiddenField runat="server" ID="hdn_DescWYSWYG" />
						                </div>
                                        </div>
                                              
						
                                        </div>
                         <div class="form-group col-md-12 text-right">
                             
                            </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdn_CommissionName" runat="server" />
</asp:Content>

