﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="CommissionView.aspx.cs" Inherits="Web_CommissionView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Commission View</title>
    <style>
        .CommissionDesc{
            height:45px!important;
            transition:all 5s l12near;
            overflow: hidden;
        }
        .CommissionDescActive{
             height:auto !important;
              transition:all 5s linear;
        }
         .IFrameStyle{
            

            border: none;
            box-shadow: -1px 1px 9px 0px #1908081c;
            border-top: 3px solid #1fe61f;

        }
         .Hide-0{
             display:none;
         }

         /*.card-header .header-elements  {
             padding-top: 0px; 
        }*/
    </style>
   <script>
      
   </script>
    <script type="text/javascript" src="../JScript/CommissionView.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10">

            </div>
           
        </div>
        <div class="row" id="div_CommissionView">
            <div class="col-md-12">
                <div class="card  border-top-2 border-top-primary-600">
                    <div class="card-header  header-elements-inline">
                         <div class="card-title"> <span class="text-muted">  </span>
                             <h4 class="font-weight-semibold mb-1"><i class="icon-cog52"></i>  Commission</h4>
                         </div>
							
                        <div class="header-elements">
                            <a class="list-icons-item mr-3" data-action="collapse"></a>
                            <a href="CommissionInitDetails.aspx" id="hpLink_EditCommission" runat="server" class="btn btn-primary btn-sm">Edit</a>

                        </div>
                    </div>
                    <div class="card-body">
                        <asp:PlaceHolder ID="ph_DivCommissionInitDetails" runat="server"></asp:PlaceHolder>
                     
                    </div>
                    	
                </div>
            </div>
           
            <div class="col-md-3 d-none">
                <div class="card border-top-2 border-top-danger">
							<div class="card-body">
                                <div class="text-center">
								<h6 class="mb-2 font-weight-semibold">Commission Status</h6>
								
							</div>
                            <span class="text-muted">Commisssion Details </span>
							<div class="progress mb-3">  

                                <div class="progress-bar progress-bar-striped bg-primary"  style="width: 75%">
									<span class="sr-only">15% Complete</span>
								</div>
                                
							</div>
                              <span class="text-muted">Rules</span>
							<div class="progress mb-3">
								<div class="progress-bar progress-bar-striped bg-success" style="width: 100%">
									<span class="sr-only">30% Complete</span>
								</div>
							</div>
                              <span class="text-muted">Recipients</span>
							<div class="progress mb-3">
								<div class="progress-bar progress-bar-striped bg-warning" style="width: 0%" >
									<span class="sr-only">45% Complete</span>
								</div>
							</div>
							</div>

						</div>
            </div>
        </div>
         <!--Recipients -->
         <div id="div_RecipientView" class="row">
            <div class="col-md-12">
                <div class="card  border-top-2 border-top-warning-600">
                    <div class="card-header  header-elements-inline">
                         <div class="card-title"> <span class="text-muted">  </span>
                             <h4 class="font-weight-semibold mb-1"><i class="icon-user-check"></i>  Assign Recipients</h4>
                         </div>
							
                        <div class="header-elements">
                            <a class="list-icons-item mr-3" data-action="collapse"></a>
                           <a href="CommissionRecipient.aspx" id="hpLink_EditCommissionRecipients" runat="server" class="btn btn-primary btn-sm">Edit</a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12 text-center">
                                    <h4 class="text-muted">Assigned Recipient   </h4>
							       <%-- <h4 class="font-weight-semibold mb-1">Based on Condition </h4><span >Select Individual Recipients</span> --%>
                                
                            </div>
                            
                            
                        </div>

                       <div class="table-responsive">
                           <table class="table datatable-basic ">
                               <thead>
                                   <tr>
                                       <th>Name</th>
                                       <th>Role</th>
                                       <th>Manager</th>
                                       <th>Territory</th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <asp:PlaceHolder ID="ph_SelectedCommissionRecipients" runat="server"></asp:PlaceHolder>
                                   

                               </tbody>
                           </table>
                       </div>
                       
                    </div>
                    	
                </div>
            </div>
        </div>
        <!--Crediting--->
         <div class="row" >
            <div class="col-md-12">
                <div class="card  border-top-2 border-top-success-300">
                    <div class="card-header  header-elements-inline">
                         <div class="card-title"> <span class="text-muted">  </span>
                             <div class="d-flex my-auto">
                                 <h4 class="font-weight-semibold  my-auto "><i class="icon-clipboard5"></i>  Crediting Rule</h4>
                                 <div class="ml-3" id="CreditingSplit"> 
                                       <span class="text-muted">Split :  <Button id="btn_PopCreditingSplit" runat="server" type="button" onclick="openSplitPopUp('CommissionKey','RuleType','CallType')" class="btn btn-primary btn-link btn-sm"><i class="icon-add"></i> Config Split</Button></span>
                                 </div>
                             
                             </div>
                            
                         </div>
							
                        <div class="header-elements">
                            <a class="list-icons-item mr-3" data-action="collapse"></a>
                            <a id="hpLink_Crediting" runat="server" href="CommissionRule.aspx?RuleType=Crediting" class="btn btn-primary btn-sm"><i class="icon-plus2"></i> add</a>
                        </div>
                    </div>
                    <div class="card-body">

                       <div class="table-responsive">
                           <table class="table ">
                               <thead class="">
                                   <tr >
                                       <th>Action</th>
                                       <th>Sequence</th>
                                       <th>Rule Name </th>
                                       <th>Variable Name</th>
                                       <th>Variable Scope</th>
                                       <th>Function </th>
                                       <th>Function Details</th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <asp:PlaceHolder ID="ph_CreditinglistTr" runat="server"></asp:PlaceHolder>
                                  
                               </tbody>
                           </table>
                       </div>
                       
                    </div>
                    	
                </div>
            </div>
        </div>
        <!--Calculation--->
         <div class="row" >
            <div class="col-md-12">
                <div class="card  border-top-2 border-top-success-600">
                    <div class="card-header  header-elements-inline">
                         <div class="card-title"> <span class="text-muted">  </span>
                             <div class="d-flex my-auto">
                                 <h4 class="font-weight-semibold  my-auto "><i class="icon-clipboard5"></i>  Calculation Rule</h4>
                                 <div class="ml-3" id="AddSplit"> 
                                       <span class="text-muted">Split :  <Button id="btn_PopCalculationSplit" runat="server" type="button" onclick="openSplitPopUp('CommissionKey','RuleType','CallType')" class="btn btn-primary btn-link btn-sm"><i class="icon-add"></i> Config Split</Button></span>
                                 </div>
                            
                             </div>

                         </div>
							
                        <div class="header-elements">
                            <a class="list-icons-item mr-3" data-action="collapse"></a>
                            <a id="hpLink_Calculation" runat="server" href="CommissionRule.aspx?RuleType=Calculation" class="btn btn-primary btn-sm"><i class="icon-plus2"></i> Add</a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="card border-left-2 border-left-success-300"  id="ProcessTxnCollapseCard"  >
                             <div class="card-header  header-elements-inl">
                                 
                                 <div class="header-elements p-1">
                                     <div class="list-icons-item mr-3" >Process Calculation Rule By</div>
                                     <div class="list-icons-item mr-3" >
                                         <div class="custom-control custom-radio custom-control-inline">
										<input  onclick="ProcessType()"  type="radio" class="custom-control-input" value="EachTransaction"  name="rdb_ProcessTransactionType" id="rdb_EachTxn" >
										<label class="custom-control-label my-auto" for="rdb_EachTxn">Each Transactions</label>
									</div>

									<div class="custom-control custom-radio custom-control-inline" >
										<input onclick="ProcessType()" type="radio" class="custom-control-input" value="GroupTransaction" name="rdb_ProcessTransactionType" id="rdb_GroupTxn" >
										<label class="custom-control-label my-auto" for="rdb_GroupTxn">Group Transaction </label>
									</div>
                                     </div>
                                   <div class="list-icons-item mr-3 float-right badge badge-dark" >use variable #CalculationPayOut </div>
                                 </div>
                             </div>
                            <div class="card-body collapse " id="ProcessTxnCollapseBody">
                                <div class="row">
                                    <div class="col-md-3">
                                       
                                        <asp:DropDownList ID="ddl_CmProcessTransactionGroupBy" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                      <div class="col-md-1">
                                          <button onclick="CalculationRuleSetup();" type="button" class="btn btn-primary btn-sm">Save</button>
                                      </div>
                                    <div class="col-md-8 bg-light">
                                        <h6>Variables</h6>
                                        <span>SalesAmount</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                           
                       <div class="table-responsive">
                           <table class="table ">
                               <thead class="">
                                   <tr >
                                       <th>Action</th>
                                       <th>Sequence</th>
                                       <th>Rule Name </th>
                                       <th>Variable Name</th>
                                        <th>Variable Type</th>
                                       <th>Function </th>
                                       <th>Function Details</th>
                                   </tr>
                               </thead>
                               <tbody>
                                  <asp:PlaceHolder ID="ph_CalculationListTr" runat="server"></asp:PlaceHolder>
                               </tbody>
                           </table>
                       </div>
                       
                    </div>
                    	
                </div>
            </div>
        </div>
        <!--Summarization--->
         <div class="row d-none" >
            <div class="col-md-12">
                <div class="card  border-top-2 border-top-success-800">
                    <div class="card-header  header-elements-inline">
                         <div class="card-title"> <span class="text-muted">  </span>
                                <div class="d-flex my-auto">
                                 <h4 class="font-weight-semibold  my-auto "><i class="icon-clipboard5"></i>  Summarization Rule</h4>
                                 <div class="ml-3  my-auto " id="SplitSummarization "> 
                                       <span class="text-muted">Split : <Button id="btn_PopSummarizationSplit" runat="server" type="button" onclick="openSplitPopUp('CommissionKey','RuleType','CallType')" class="btn btn-primary btn-link btn-sm"><i class="icon-add"></i> Config Split</Button></span>
                                      
                                 </div>
                               
                             </div>
                            
                         </div>
							
                        <div class="header-elements">
                           <a class="list-icons-item mr-3" data-action="collapse"></a>
                            <a id="hpLink_Summarization" runat="server" href="CommissionRule.aspx?RuleType=Summarization" class="btn btn-primary btn-sm"><i class="icon-plus2"></i> Add</a>
                        </div>
                    </div>
                    <div class="card-body">

                       <div class="table-responsive">
                           <table class="table ">
                               <thead class="">
                                   <tr >
                                       <th>Action</th>
                                       <th>Sequence</th>
                                       <th>Rule Name </th>
                                       <th>Variable Name</th>
                                        <th>Variable Type</th>
                                       <th>Function </th>
                                       <th>Function Details</th>
                                   </tr>
                               </thead>
                               <tbody>
                                  <asp:PlaceHolder ID="ph_SummarizationListTr" runat="server"></asp:PlaceHolder>
                               </tbody>
                           </table>
                       </div>
                       
                    </div>
                    	
                </div>
            </div>
        </div>
        
    </div>
     <!--Iframe pop up-->
      
                <div id="PopUpIframe" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-full h-100 ">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;<span id="FunctionType"></span></h5>
                                <button type="button" class="close " data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body">
                              <iframe id="iFramePopUp" class="IFrameStyle" height="100%" width="100%" src=""></iframe>
                               
                            </div>

                           <%-- <div class="modal-footer text-center">
                              <button type="button" class="btn btn-primary" onclick="LoadFunctionStatements('split');">Close</button>
                            </div>--%>
                        </div>
                    </div>
                </div>
                <!-- /Iframe -->
    <asp:HiddenField ID="hdn_CommissionKey" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdn_IsGroupBy" Value="False" runat="server" ClientIDMode="Static" />
</asp:Content>

