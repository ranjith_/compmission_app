﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Web_Roles : System.Web.UI.Page
{
    ApplicationCommonFunction acf;
    TreeNode tnProject;
    DataAccess DA;
    PhTemplate PHT;
    SessionCustom SC;
    TreeView tv_Menu;
    DataTable dt_menu = new DataTable();
    string str_RoleId = "";
    public string str_Job = "", str_EmpCount = "", str_SystemRole = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();
        if (Request.QueryString["Id"] != null || Request.QueryString["Id"].ToString() != "")
        {
            str_RoleId = Request.QueryString["Id"].ToString();
        }
        else return;

        string sqlstring = "select a.*, b.MenuKey as RoleMenu from Menu a left outer join (select MenuKey from MenuRole where RoleKey=@RoleKey) b on a.MenuKey=b.MenuKey where IsActive=1 order by MenuOrder";
        SqlCommand scm = new SqlCommand(sqlstring);
        if (str_RoleId == "") scm.Parameters.AddWithValue("@RoleKey", DBNull.Value);
        else scm.Parameters.AddWithValue("@RoleKey", str_RoleId);
        dt_menu = DA.GetDataTable(scm);
        if (!IsPostBack)
        {
            AssignValueToControls();
            //LoadEmployeesInTheRole(str_RoleId);
            load_TreeMenu(tree_Menu, dt_menu);
        }

    }
    public void load_TreeMenu(TreeView mn_main, DataTable dt_menu)
    {
        acf = new ApplicationCommonFunction();
        foreach (DataRow dr in dt_menu.Select("isnull(ParentMenuId,'')=''", "MenuOrder"))
        {
            MenuItem mi = new MenuItem();
            mi.Text = dr["MenuName"].ToString(); ;
            mi.Value = dr["MenuKey"].ToString();
            tnProject = new TreeNode(mi.Text, mi.Value, null, mi.NavigateUrl, "_blank");
            if (dr["RoleMenu"].ToString() != "") tnProject.Checked = true;
            mn_main.Nodes.Add(tnProject);
            load_child_Treemenu(mi, dr["MenuKey"].ToString(), dt_menu);
        }

    }

    private void load_child_Treemenu(MenuItem mi, string ParentMenuId, DataTable dt_menu)
    {

        foreach (DataRow dr_child in dt_menu.Select("isnull(ParentMenuId,'')='" + ParentMenuId + "'", "MenuOrder"))
        {
            MenuItem mi_child = new MenuItem();
            mi_child.Text = dr_child["MenuName"].ToString();

            mi_child.Value = dr_child["MenuKey"].ToString();

            TreeNode tn_submenu = new TreeNode(mi_child.Text, mi_child.Value, null, mi_child.NavigateUrl, "_blank");

            if (dr_child["RoleMenu"].ToString() != "") tn_submenu.Checked = true;

            tnProject.ChildNodes.Add(tn_submenu);
            if (dr_child["TargetUrl"].ToString() != "")
            {
                if (acf.convert_to_boolean(dr_child["IsNewWindow"].ToString()))
                    mi_child.Target = "_blank";
            }
            mi.ChildItems.Add(mi_child);
            DataRow[] dr_grant_child = dt_menu.Select("isnull(ParentMenuId,'')='" + dr_child["MenuKey"].ToString() + "'", "MenuOrder");
            if (dr_grant_child.Length > 0)
            {
                this.load_child_Treemenu(mi_child, dr_child["MenuKey"].ToString(), dt_menu);
            }
        }

    }
    
    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string userKey = "15702C1E-1447-4C08-9773-7BD324256180";
        try
        {
            ArrayList ar_cmd = new ArrayList();
            string str_UniqueKey = Guid.NewGuid().ToString();
            if (str_RoleId != "") str_UniqueKey = str_RoleId;
            string str_insertRole = "delete from Role where RoleKey=@RoleId; delete from MenuRole where RoleKey=@RoleId; "
                + "insert into Role(RoleKey,RoleName,RoleDesc,IsSystemRole,createdon, createdby, modifiedon, modifiedby) values(@RoleId,@RoleName,@RoleDesc,@IsSystemRole,getdate(), @createdby, getdate(), @createdby)";
            SqlCommand cmd = new SqlCommand(str_insertRole);
            cmd.Parameters.AddWithValue("@RoleId", str_UniqueKey);
            cmd.Parameters.AddWithValue("@RoleName", txt_RoleName.Value);
            cmd.Parameters.AddWithValue("@RoleDesc", txt_RoleDesc.Value);
            cmd.Parameters.AddWithValue("@IsSystemRole", ch_IsSystemRole.Checked ? "1" : "0");
            cmd.Parameters.AddWithValue("@CreatedBy", userKey);
            ar_cmd.Add(cmd);
            foreach (TreeNode tn in tree_Menu.CheckedNodes)
            {
                string str_insertMenuRole = "insert into MenuRole(RoleKey,MenuKey,MenuName) values (@RoleId,@MenuKey,@MenuName)";
                SqlCommand sc = new SqlCommand(str_insertMenuRole);
                sc.Parameters.AddWithValue("@RoleId", str_UniqueKey);
                sc.Parameters.AddWithValue("@MenuKey", tn.Value);
                sc.Parameters.AddWithValue("@MenuName", tn.Text);
                ar_cmd.Add(sc);
            }
            this.DA.ExecuteNonQuery(ar_cmd);
            //ClientScript.RegisterStartupScript(GetType(), "ICustomer", "SaveAlertAndRedirect();", true);

            Response.Redirect("../Web/RoleList.aspx");
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void AssignValueToControls()
    {
        string str_Query = "select * from Role where RoleKey=@RoleKey ; select * from MenuRole where RoleKey=@RoleKey";
        SqlCommand cmd = new SqlCommand(str_Query);
        if (str_RoleId != "")
        {
            cmd.Parameters.AddWithValue("@RoleKey", str_RoleId);
            DataSet ds2 = this.DA.GetDataSet(str_Query);
            DataRow dr = ds2.Tables[0].Rows[0];
            DataSet ds = this.DA.GetDataSet(cmd);
            txt_RoleName.Value = ds.Tables[0].Rows[0]["RoleName"].ToString();
            txt_RoleDesc.Value = ds.Tables[0].Rows[0]["RoleDesc"].ToString();
            ch_IsSystemRole.Checked = !dr.IsNull("IsSystemRole") && Convert.ToBoolean(dr["IsSystemRole"]);
            str_Job = txt_RoleName.Value;
        }
        //load_TreeMenuForEdit(tree_Menu, ds.Tables[1]);
    }

    public void load_TreeMenuForEdit(TreeView mn_main, DataTable dt)
    {
        foreach (DataRow dr in dt_menu.Rows)
        {
            string str_MenuKey = dr["MenuKey"].ToString();

            foreach (DataRow dr1 in dt.Rows)
            {
                if (str_MenuKey == dr1["MenuKey"].ToString())
                {
                    MenuItem mi = new MenuItem();
                    mi.Text = dr1["MenuName"].ToString(); ;
                    mi.Value = dr1["MenuKey"].ToString();
                    tnProject = new TreeNode(mi.Text, mi.Value, null, mi.NavigateUrl, "_blank");
                    tnProject.Checked = true;
                    mn_main.Nodes.Add(tnProject);
                }
            }
        }
    }

    //protected void LoadEmployeesInTheRole(string str_RoleId)
    //{
    //    string str_TemplateFile = "", str_HTML = "";
    //    str_TemplateFile = this.PHT.ReadFileToString("li_EmployeeInRole.txt");
    //    string str_Sql = "Select a.EmpName,a.EmpOriginalName,a.EmpCode,c.RoleName from IEmployee a "
    //        + "left outer join IEmployeerole b on a.EmpKey = b.EmpKey "
    //        + "left Outer join Role c on b.RoleId = c.RoleKey "
    //        + "where c.RoleKey = @RoleKey order by EmpOriginalName asc";
    //    SqlCommand cmd = new SqlCommand(str_Sql);
    //    cmd.Parameters.AddWithValue("@RoleKey", str_RoleId);
    //    DataTable dt_EmpRole = this.DA.GetDataTable(cmd);
    //    str_EmpCount = dt_EmpRole.Rows.Count.ToString();
    //    foreach (DataRow dr in dt_EmpRole.Rows)
    //    {
    //        str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_TemplateFile);
    //    }
    //    Ph_EmpRole.Controls.Add(new LiteralControl(str_HTML));
    //}

}