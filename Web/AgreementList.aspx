﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CMMaster.master" AutoEventWireup="true" CodeFile="AgreementList.aspx.cs" Inherits="Web_AgreementList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    <title>Agreement List</title>
    <script>
        function deleteAgreement(AgreementKey, AgreementName) {
            var confirmation = confirm("are you sure want to delete  " + AgreementName + " Agreement ?");
            if (confirmation == true)
            {
                //to delete
                $.ajax({
                    type: "POST",
                    url: "../Web/AgreementList.aspx/deleteAgreement",
                    data: "{AgreementKey:'" + AgreementKey + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d = "DeleteSuccess") {
                            alert("Agreement " + AgreementName + " deleted successfully")
                            location.reload();
                        }
                        else {
                            alert("Error : Please try again");
                        }
                        
                    },
                    error: function () {
                        alert('Error communicating with server');
                    }
                });
            }
            else {
                return false
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <div class="container-fluid">
<div class="row">
     <!--Data List-->
                                <div class="col-lg-12 col-md-12">
                                    <!-- Basic datatable -->
                    <div class="card ">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Agreement
                                    
                                </h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                       
                                        <a href="Agreement.aspx" class="btn btn-primary  btn-sm "><i class="icon-plus22 position-left"></i> New</a>
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        
                                    </div>
                                </div>
                            </div>
    
                            <div class="card-body">
                                  
                            <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                    <tr>
                                        
                                        <th>Agreement Name</th>
                                        <th> Is Default</th>
                                        <th> Created On</th>
                                        <th> Created By</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                   <asp:PlaceHolder ID="ph_AgreementListTr" runat="server"></asp:PlaceHolder>
                                        
                                </tbody>
                            </table>
                    </div>
                                </div>
                        </div>
                        <!-- /basic datatable -->
                                </div>
                                 <!--Data List-->


        <!--View Data-->
                                 <div id="div_AgreementDetails" runat="server" visible="false" class="col-lg-6 col-md-6">
                                        <!-- Basic layout-->
                                    <div class="card ">
                            
                                <asp:PlaceHolder ID="ph_AgreementDetails" runat="server"></asp:PlaceHolder>
                                
                                    </div>
                                
                                </div>
                                <!--view data-->
    </div>
        </div>
    
</asp:Content>

