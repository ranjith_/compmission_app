﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class Web_CompanySettings : System.Web.UI.Page
{
    Common CC;
    DataAccess DA;
    SessionCustom SC;
    PhTemplate PHT;
    OtherCommonFunction OCF;
    public string str_UserKey = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.SC = new SessionCustom();
        this.PHT = new PhTemplate();

        this.CC = new Common();

       
        if (!IsPostBack)
        {
            string str_Select = "select Name as Text, Value as Value from v_FascalYear order by Seq";
            SqlCommand cmm = new SqlCommand(str_Select);
            DataSet ds = this.DA.GetDataSet(cmm);
            this.CC.LoadDropDown(ddl_Fascalyear, ds.Tables[0], "Text", "Value", true, "-- Select Year --");

            string str_Select2 = "select Name as Text, Value as Value from v_PayFrequency order by Seq";
            SqlCommand cmd = new SqlCommand(str_Select2);
            DataSet ds1 = this.DA.GetDataSet(cmd);
            this.CC.LoadDropDown(ddl_PayFrequency, ds1.Tables[0], "Text", "Value", true, "-- Select Year --");

        }
        if (!IsPostBack)
        {
            loadCompanyDetails(SC.UserKey);
            loadExistingPeriod();

        }

    }

    private void loadCompanyDetails(string userKey)
    {
        string str_Sql_view = "select *,cl.LogoImageKey,cl.LogoImageName from CompanySetup a left outer join CompanyLogo cl on a.CompanyKey=cl.Companykey where UserKey =@UserKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@UserKey", userKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {
            //       StateReport    WebsiteReport)

            txt_CompanyName.Value = dr["CompanyName"].ToString();
            txt_Email.Value = dr["Email"].ToString();
            txt_Mobile.Value = dr["Mobile"].ToString();
            dd_Country.Value = dr["Country"].ToString();
            dd_State.Value = dr["State"].ToString();
            dd_City.Value = dr["City"].ToString();
            dd_Currency.Value = dr["Currency"].ToString();
            dd_TimeZone.Value = dr["TimeZone"].ToString();
            txt_Address1.Value = dr["Address1"].ToString();
            txt_Address2.Value = dr["Address2"].ToString();
            txt_Website.Value = dr["Website"].ToString();
            //
            //if (dr["Logo"].ToString() != "")
            //{
            //    img_CompanyLogo.Src = "../UploadFile/Logo/" + dr["Logo"].ToString();
            //}
           
            txt_AprCompanyName.Value = dr["CompanyNameReport"].ToString();
            txt_AprEmail.Value = dr["EmailReport"].ToString();
            txt_AprMobile.Value = dr["MobileReport"].ToString();
            dd_AprCountry.Value = dr["CountryReport"].ToString();
            dd_AprState.Value = dr["StateReport"].ToString();
            dd_AprCity.Value = dr["CityReport"].ToString();
            txt_AprAddress1.Value = dr["Address1Report"].ToString();
            txt_AprAddress2.Value = dr["Address2Report"].ToString();
            txt_AprWebsite.Value = dr["WebsiteReport"].ToString();

            OneTableDml OTD = new OneTableDml("CompanyLogo");

            if (dr["LogoImageName"].ToString() != "")
                img_CompanyLogo.Attributes.Add("src", OTD.FileFolder + dr["LogoImageName"].ToString());
            else
                img_CompanyLogo.Attributes.Add("src", "../Limitless_Bootstrap_4/Template/global_assets/images/placeholders/placeholder.jpg");

            if (Convert.ToString(dr["LogoImageKey"]) == "")
            {
                a_CompanyLogo.Attributes.Add("onclick", "OpenPopUp('Add Logo to this Company','CompanyLogo','','CompanyKey:" + dr["CompanyKEy"].ToString() + "','');");
            }
            else
                a_CompanyLogo.Attributes.Add("onclick", "OpenPopUp('Add Logo to this Company','CompanyLogo','" + Convert.ToString(dr["LogoImageKey"]) + "','CompanyKey:" + dr["CompanyKEy"].ToString() + "','');");

        }
    }

    protected void btn_SaveDetails_ServerClick(object sender, EventArgs e)
    {
        string str_Sql = "UPDATE CompanySetup set CompanyName=@CompanyName, Email=@Email, Mobile=@Mobile, Country=@Country, State=@State, City=@City, Currency=@Currency, TimeZone=@TimeZone, " +
           "Address1=@Address1, Address2=@Address2, Website=@Website, CompanyNameReport=@CompanyNameReport, EmailReport=@EmailReport, MobileReport=@MobileReport, " +
           "CountryReport=@CountryReport, StateReport=@StateReport, CityReport=@CityReport, Address1Report=@Address1Report, Address2Report=@Address2Report," +
           " WebsiteReport=@WebsiteReport, ModifiedBy=@ModifiedBy, ModifiedOn=@ModifiedOn where UserKey=@UserKey";

        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CompanyName", txt_CompanyName.Value);
        cmd.Parameters.AddWithValue("@Email", txt_Email.Value);
        cmd.Parameters.AddWithValue("@Mobile", txt_Mobile.Value);
        cmd.Parameters.AddWithValue("@Country", dd_Country.Value);
        cmd.Parameters.AddWithValue("@State", dd_State.Value);
        cmd.Parameters.AddWithValue("@City", dd_City.Value);
        cmd.Parameters.AddWithValue("@Currency", dd_Currency.Value);
        cmd.Parameters.AddWithValue("@TimeZone", dd_TimeZone.Value);
        cmd.Parameters.AddWithValue("@Address1", txt_Address1.Value);
        cmd.Parameters.AddWithValue("@Address2", txt_Address2.Value);
        cmd.Parameters.AddWithValue("@Website", txt_Website.Value);

        
        cmd.Parameters.AddWithValue("@CompanyNameReport", txt_AprCompanyName.Value);
        cmd.Parameters.AddWithValue("@EmailReport", txt_AprEmail.Value);
        cmd.Parameters.AddWithValue("@MobileReport", txt_AprMobile.Value);
        cmd.Parameters.AddWithValue("@CountryReport", dd_AprCountry.Value);
        cmd.Parameters.AddWithValue("@StateReport", dd_AprState.Value);
        cmd.Parameters.AddWithValue("@CityReport", dd_AprCity.Value);
        cmd.Parameters.AddWithValue("@Address1Report", txt_AprAddress1.Value);
        cmd.Parameters.AddWithValue("@Address2Report", txt_AprAddress2.Value);
        cmd.Parameters.AddWithValue("@WebsiteReport", txt_AprWebsite.Value);

        cmd.Parameters.AddWithValue("@ModifiedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@ModifiedOn", DateTime.Now.ToString());

        cmd.Parameters.AddWithValue("@UserKey", SC.UserKey);
        this.DA.ExecuteNonQuery(cmd);
    }

    protected void btn_PeriodSave_ServerClick(object sender, EventArgs e)
    {
        string str_Key = Guid.NewGuid().ToString();
        //// Calender (CalendarKey, FascalYear, StartDate, EndDate, PayFrequency, CurrentPeriod, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        ///
        //string str_Sql = "insert into Calendar (CalendarKey, FascalYear, StartDate, EndDate, PayFrequency, CurrentPeriod, CreatedOn, CreatedBy)"
        //                    + "select @CalendarKey ,  @FascalYear , @StartDate , @EndDate , @PayFrequency , @CurrentPeriod, @CreatedOn, @CreatedBy";
        string str_Sql = "Update Calendar set  FascalYear=@FascalYear, StartDate=@StartDate, EndDate=@EndDate, PayFrequency=@PayFrequency, CurrentPeriod=@CurrentPeriod where CalendarKey=@CalendarKey;" +
            "Delete  from Period where CalendarKey=@CalendarKey";
                           
        SqlCommand cmd = new SqlCommand(str_Sql);
       
        cmd.Parameters.AddWithValue("@FascalYear", ddl_Fascalyear.SelectedValue);
        cmd.Parameters.AddWithValue("@StartDate", dp_StartDate.Value);
        cmd.Parameters.AddWithValue("@EndDate", dp_EndDate.Value);
        cmd.Parameters.AddWithValue("@PayFrequency", ddl_PayFrequency.SelectedValue);
        cmd.Parameters.AddWithValue("@CurrentPeriod", txt_CurrentPeriod.Value);



        cmd.Parameters.AddWithValue("@CalendarKey", hdn_CalendarKey.Value);

      
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);

        loadPeriods(hdn_CalendarKey.Value);
        Response.Redirect(Request.RawUrl);
    }
    private void loadPeriods(string str_Key)
    {
        DateTime startdate = Convert.ToDateTime(dp_StartDate.Value);
        DateTime enddate = Convert.ToDateTime(dp_EndDate.Value);

        int startdateMonth = startdate.Month;
        int enddateMonth = enddate.Month;
        int year = int.Parse(ddl_Fascalyear.SelectedValue);

        int Frequence = Int32.Parse(ddl_PayFrequency.SelectedValue);


        switch (Frequence)
        {
            case 1:
                //weekly
                int x = 0;

                var Totaldays = (enddate - startdate).TotalDays;

                DateTime StartDate = startdate;
                DateTime EndDate = enddate;

                DateTime WeekFirstDay = startdate;
                DateTime WeekLastDay = new DateTime();

                for (x = 0; x <= Totaldays; x += 7)
                {
                    if (x == 0)
                    {
                        WeekFirstDay = startdate;
                    }
                    else
                    {
                        WeekFirstDay = WeekLastDay.AddDays(1);
                    }
                    if (x + 6 > Totaldays)
                    {
                        WeekLastDay = enddate;

                    }
                    else
                    {
                        WeekLastDay = WeekFirstDay.AddDays(6);
                    }
                    insertPeriod(WeekFirstDay, WeekLastDay, str_Key);
                }


                //while ( x < Totaldays){
                //    DateTime WeekFirstDay = startdate;
                //    DateTime WeekLastDay = startdate;
                //    if (x == 1)
                //    {
                //        WeekFirstDay = startdate;
                //    }
                //    else
                //    {
                //        WeekFirstDay = WeekFirstDay.AddDays(x);
                //    }
                //    if (x > Totaldays)
                //    {
                //        WeekLastDay = enddate;

                //    }
                //    else
                //    {
                //        WeekLastDay = WeekFirstDay.AddDays(6);
                //    }
                //    insertPeriod(WeekFirstDay, WeekLastDay, str_Key);
                //    x += 6;
                //}
                break;
            case 2:
                //monthly
                for (int i = startdateMonth; i <= enddateMonth; i++)
                {
                    DateTime FirstDay = startdate;
                    DateTime LastDay = startdate;
                    if (i == startdateMonth)
                    {
                        FirstDay = startdate;
                    }
                    else
                    {
                        FirstDay = new DateTime(year, i, 1);
                    }

                    if (i > (enddateMonth - 1))
                    {
                        LastDay = enddate;
                    }
                    else
                    {
                        LastDay = new DateTime(FirstDay.Year, FirstDay.Month, DateTime.DaysInMonth(FirstDay.Year, FirstDay.Month));
                    }

                    insertPeriod(FirstDay, LastDay, str_Key);
                }
                break;
            case 3:
                //Quarterly
                for (int i = startdateMonth; i <= enddateMonth; i += 3)
                {
                    DateTime FirstDay = startdate;
                    DateTime LastDay = startdate;
                    if (i == startdateMonth)
                    {
                        FirstDay = startdate;
                    }
                    else
                    {
                        FirstDay = new DateTime(year, i, 1);
                    }

                    if (i > (enddateMonth - 3))
                    {
                        LastDay = enddate;
                    }
                    else
                    {
                        var DD = FirstDay.AddDays(1).AddMonths(2).AddDays(-1);
                        LastDay = new DateTime(FirstDay.Year, DD.Month, DateTime.DaysInMonth(DD.Year, DD.Month));
                    }

                    insertPeriod(FirstDay, LastDay, str_Key);
                }
                break;
            case 4:
                //Half Yearly
                for (int i = startdateMonth; i <= enddateMonth; i += 6)
                {
                    DateTime FirstDay = startdate;
                    DateTime LastDay = startdate;
                    if (i == startdateMonth)
                    {
                        FirstDay = startdate;
                    }
                    else
                    {
                        FirstDay = new DateTime(year, i, 1);
                    }

                    if (i > (enddateMonth - 6))
                    {
                        LastDay = enddate;
                    }
                    else
                    {
                        var DD = FirstDay.AddDays(1).AddMonths(5).AddDays(-1);
                        LastDay = new DateTime(FirstDay.Year, DD.Month, DateTime.DaysInMonth(DD.Year, DD.Month));
                    }

                    insertPeriod(FirstDay, LastDay, str_Key);
                }
                break;
        }
    }

    private void loadExistingPeriod()
    {
        string str_Sql_view = "select c.*,p.Name as Frequency, convert(varchar, c.CreatedOn, 101) as Created_On, convert(varchar, c.StartDate, 101) as SDate,convert(varchar, c.EndDate, 101) as EDate from Calendar c join v_PayFrequency p on c.PayFrequency=p.Value where c.CreatedBy=@CreatedBy";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);

        if (dt.Rows.Count > 0)
        {
           // Calender(CalendarKey, FascalYear, StartDate, EndDate, PayFrequency, CurrentPeriod, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
            string CalendarKey = "";
            foreach (DataRow dr in dt.Rows)
            {
                ddl_Fascalyear.SelectedValue = dr["FascalYear"].ToString();
                txt_CurrentPeriod.Value= dr["CurrentPeriod"].ToString();
                dp_StartDate.Value= dr["SDate"].ToString();
                dp_EndDate.Value = dr["EDate"].ToString();
                ddl_PayFrequency.SelectedValue= dr["PayFrequency"].ToString();
                CalendarKey = dr["CalendarKey"].ToString();
                hdn_CalendarKey.Value = CalendarKey;
                PayFrequencySpan.InnerHtml = dr["Frequency"].ToString();
            }

            loadExistingPeriods(CalendarKey);
        }
        else
        {
            
        }

    }

    private void loadExistingPeriods(string calendarKey)
    {
        string str_Sql = "select  convert(varchar, StartDate, 101) as SDate,convert(varchar, EndDate, 101) as EDate,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) SiNo " +
            "from Period where CalendarKey = @CalendarKey order by StartDate";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@CalendarKey", calendarKey);

        DataSet ds = this.DA.GetDataSet(cmd);

        this.PHT.LoadGridItem(ds, ph_PeriodListTr, "PeriodListTr.txt", "");
    }

    private void insertPeriod(DateTime startdate, DateTime enddate, string CalendarKey)
    {
        string Key = Guid.NewGuid().ToString();
        //// Period  (PeriodKey, CalendarKey, StartDate, EndDate, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
        ///
        string str_Sql = "insert into Period  (PeriodKey, CalendarKey, StartDate, EndDate, CreatedOn, CreatedBy)"
                            + "select @PeriodKey ,@CalendarKey , @StartDate , @EndDate , @CreatedOn, @CreatedBy";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PeriodKey", Key);
        cmd.Parameters.AddWithValue("@CalendarKey", CalendarKey);
        cmd.Parameters.AddWithValue("@StartDate", startdate);
        cmd.Parameters.AddWithValue("@EndDate", enddate);

        cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);

        cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());
        // cmd.Parameters.AddWithValue("@CreatedOn", getDate());
        // cmd.Parameters.AddWithValue("@createdby", new SessionCustom().GetUserKey());

        this.DA.ExecuteNonQuery(cmd);
    }
}