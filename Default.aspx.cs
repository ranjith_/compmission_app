﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    DataAccess DA;
    Common CM;
    public string str_UserRoles = "", Str_TypeId = "", str_Image = "",str_UserKey="";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.CM = new Common();
        //if (this.CM.GetQueryStringValue("msg") != "")
        //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + this.CM.GetQueryStringValue("msg") + "')", true);
        str_UserKey = this.CM.GetQueryStringValue("UserKey");
        if (str_UserKey != "")
        {
            string str_Sql = "select UserName,UserPassword from Credential where UserKey=@UserKey;";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@UserKey", str_UserKey);
            DataTable dt = this.DA.GetDataTable(cmd);
            if(dt.Rows.Count>0)
            {
                DirectSignIn(dt.Rows[0]["UserName"].ToString(), dt.Rows[0]["UserPassword"].ToString());
            }
        }
    }

    protected void btn_Login_Click(object sender, EventArgs e)
    {
        DirectSignIn(txt_UserName.Text.Trim(), txt_Password.Text.Trim());
    }

    public void DirectSignIn(string str_UserName,string str_Password)
    {
        Common CM = new Common();
        string str_Sql = "select * from Credential where UserName=@UserName and UserPassword=@UserPassword;";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@UserName", str_UserName);
        cmd.Parameters.AddWithValue("@UserPassword", str_Password);
        DataTable dt = this.DA.GetDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            Session["UserKey"] = dt.Rows[0]["UserKey"].ToString();
            Session["DoctorKey"] = dt.Rows[0]["UserKey"].ToString();
            CM.LoadSessionVariables(dt.Rows[0]["UserKey"].ToString());
            HttpContext.Current.Session["httproot"] = Request.Url.OriginalString.ToLower().Replace("default.aspx", "").Replace("?session=logout", "");

            Response.Redirect("~/Masters/TestGrid.aspx");
        }
        //lbl_invalid.Visible = true;
    }
}