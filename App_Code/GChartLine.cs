﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GChart
/// </summary>
/// 
public class GChartLine : GChart
{
    PhTemplate PHT = new PhTemplate();

    string str_DivName = "";
    int int_PositionLeft = 50;
    int int_PositionTop = 20;
    static string[] ColourValues = new string[] {
                "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    };

    public GChartLine(string str_DivName)
    {
        this.str_DivName = str_DivName;
        this.LineColor = LineColorOptions.blue;
    }
    public enum LineColorOptions
    {
        red, green, yellow, blue, gray, purple, orange, silver, gold
    };
    
    public int PositionLeft { get { return this.int_PositionLeft; } set { this.int_PositionLeft = value; } }

    public int PositionTop { get { return this.int_PositionTop; } set { this.int_PositionTop = value; } }

    public LineColorOptions LineColor { set; get; }

    public string DrawLineChart(DataTable dt, string[] str_DisplayColumn, string[] str_ValueColumn)
    {
        //string str_ChartTemplate = PHT.ReadFileToString("ChartLine.txt");
        string str_ChartTemplate = this.GetChartTemplate("ChartLine.txt", this.str_DivName);
        string str_ChartContent = "", str_ValueContent = "";

        int displayCount = str_DisplayColumn.Length;
        for (int i = 0; i < displayCount; i++)
        {
            str_ChartContent += "'" + str_DisplayColumn.GetValue(i).ToString() + "',";
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);
        str_ChartContent = "[" + str_ChartContent + "],";

        foreach (DataRow dr in dt.Rows)
        {
            str_ValueContent = "";
            for (int i = 0; i < displayCount; i++)
            {
                if (dr[str_ValueColumn.GetValue(i).ToString()].ToString() != "")
                {
                    if (i == 0)
                        str_ValueContent += "'" + dr[str_ValueColumn.GetValue(i).ToString()].ToString() + "',";
                    else
                        str_ValueContent += "" + dr[str_ValueColumn.GetValue(i).ToString()].ToString() + ",";
                }
                else
                    str_ValueContent += "0,";
            }
            str_ValueContent = str_ValueContent.Substring(0, str_ValueContent.Length - 1);
            str_ValueContent = "[" + str_ValueContent + "],";
            str_ChartContent += str_ValueContent;
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);

        str_ChartTemplate = str_ChartTemplate.Replace("%%ChartJsonValues%%", str_ChartContent);
        str_ChartTemplate = str_ChartTemplate.Replace("%%Color%%", LineColor.ToString().ToLower());
        str_ChartTemplate = str_ChartTemplate.Replace("%%PositionLeft%%", PositionLeft.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%PositionTop%%", PositionTop.ToString());

        return str_ChartTemplate;
    }


    public string DrawLineChartWithLink(DataTable dt, string[] str_DisplayColumn, string[] str_ValueColumn)
    {
        //string str_ChartTemplate = PHT.ReadFileToString("ChartLineWithLink.txt");
        string str_ChartTemplate = this.GetChartTemplate("ChartLineWithLink.txt", this.str_DivName);
        string str_ChartContent = "", str_ValueContent = "";

        int displayCount = str_DisplayColumn.Length;
        //['Dates', 'Encounters', 'Claims', 'Resolved Claims', 'UnResolved Claims']
        for (int i = 0; i < displayCount; i++)
        {
            str_ChartContent += "'" + str_DisplayColumn.GetValue(i).ToString() + "',";
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);
        str_ChartContent = "[" + str_ChartContent + "],";

        foreach (DataRow dr in dt.Rows)
        {
            str_ValueContent = "";
            for (int i = 0; i < displayCount; i++)
            {
                if (dr[str_ValueColumn.GetValue(i).ToString()].ToString() != "")
                {
                    if (i == 0)
                        str_ValueContent += "'" + dr[str_ValueColumn.GetValue(i).ToString()].ToString() + "',";
                    else
                        str_ValueContent += "" + dr[str_ValueColumn.GetValue(i).ToString()].ToString() + ",";
                }
                else
                    str_ValueContent += "0,";
            }
            str_ValueContent = str_ValueContent.Substring(0, str_ValueContent.Length - 1);
            str_ValueContent = "[" + str_ValueContent + "],";
            str_ChartContent += str_ValueContent;
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);

        str_ChartTemplate = str_ChartTemplate.Replace("%%ChartJsonValues%%", str_ChartContent);
        str_ChartTemplate = str_ChartTemplate.Replace("%%Color%%", LineColor.ToString().ToLower());
        str_ChartTemplate = str_ChartTemplate.Replace("%%PositionLeft%%", PositionLeft.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%PositionTop%%", PositionTop.ToString());
        return str_ChartTemplate;
    }



}