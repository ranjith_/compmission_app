﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for DataAccessWeb
/// </summary>
public class DataAccessWeb
{
    string str_UserKey = "";
    DataAccess DA;
    DataTable dt_Sys;

    public DataAccessWeb(string str_UserKey)
    {
        this.str_UserKey = str_UserKey;
        this.DA = new DataAccess();

    }

    public void WebSaveAddress(string str_TableName, string str_PrimaryKey, string str_PrimaryKeyValue, string str_OtherColumns)
    {
        //validation
    }

    public SqlCommand CreateLogKey()
    {
        string str_LogKey = Guid.NewGuid().ToString();
        string str_Sql = "insert into log(LogKey, CreatedOn, ModifiedOn, CreatedBy, ModifiedBy) values (@LogKey, getdate(), getdate(), @CreatedBy, @ModifiedBy)";
        SqlCommand sc = new SqlCommand(str_Sql);
        sc.Parameters.AddWithValue("@LogKey", str_LogKey);
        sc.Parameters.AddWithValue("@CreatedBy", this.str_UserKey);
        sc.Parameters.AddWithValue("@ModifiedBy", this.str_UserKey);
        return sc;
    }

    public SqlCommand ModifyLogKey(string str_LogKey)
    {
        string str_Sql = "Update log set ModifiedOn=getdate(), ModifiedBy=@ModifiedBy where LogKey=@LogKey";
        SqlCommand sc = new SqlCommand(str_Sql);
        sc.Parameters.AddWithValue("@LogKey", str_LogKey);
        sc.Parameters.AddWithValue("@ModifiedBy", this.str_UserKey);
        return sc;
    }

    public void WebSave(OneTableDml OTD)
    {
        this.dt_Sys = this.DA.GetDataTable("select tablename, columnname, datatype from v_Columns where tablename='" + OTD.TableName + "'");
        string str_TableName = OTD.TableName;
        string str_PrimaryKey = OTD.PrimaryKeyColumnName;
        string str_PrimaryKeyValue = OTD.KeyValue;
        string str_OtherColumns = OTD.PostData;

        bool bln_AddMode = false;
        if (str_PrimaryKeyValue == "") bln_AddMode = true;

        string str_InsertQuery = " insert into " + str_TableName + " %%ColumnName%% values %%ColumnValue%%";
        string str_UpdateQuery = " update " + str_TableName + " set %%UpdateColumnAndValues%% where " + str_PrimaryKey + "=%%WhereColumn%%";
        //string[] str_ValueString = str_OtherColumns.Split('&');
        //int int_length = str_ValueString.Length;
        string str_Query = "";
        string str_ColumnName = "", str_ColumnValue = "", str_UpdateQueryValues = "", str_DbColName = "", str_DbColValue = "";

        ArrayList al_SC = new ArrayList();
        SqlCommand sc = new SqlCommand();
        DataTable dt_PostData = this.PutPostParameterToDataTable(str_OtherColumns, OTD, bln_AddMode);

        //prepare SQL for timestamp LOG  - DO NOT Use, 
        string str_LogKey = "";
        if (OTD.LogKeyRequired)
        {
            SqlCommand sc_Log = new SqlCommand();
            if (bln_AddMode)
            {
                sc_Log = this.CreateLogKey();
                str_LogKey = sc_Log.Parameters[0].Value.ToString();
            }
            else  //retrieve log key from postdata
            {
                str_LogKey = dt_PostData.Select("CName='LogKey'")[0]["CValue"].ToString();
                sc_Log = this.ModifyLogKey(str_LogKey);
            }
            al_SC.Add(sc_Log);
        }


        // prepare sql for insert/update
        //for (int i = 0; i < int_length; i++)
        foreach (DataRow dr in dt_PostData.Rows)
        {
            str_DbColName = dr["CName"].ToString();
            str_DbColValue = dr["CValue"].ToString();

            if (bln_AddMode)
            {
                str_ColumnName += str_DbColName + ",";
                str_ColumnValue += "@" + str_DbColName + ",";
            }
            else
            {
                str_UpdateQueryValues += "[" + str_DbColName + "]=@" + str_DbColName.Replace(" ", "_") + ",";
            }

            if (bln_AddMode && str_DbColName.ToLower() == str_PrimaryKey.ToLower())  // for new record, need to have guid
                sc.Parameters.AddWithValue("@" + str_DbColName, Guid.NewGuid().ToString());


            else if (dt_Sys.Select("columnname='" + str_DbColName + "' and datatype in ('date','datetime','uniqueidentifier','decimal','float')").Length > 0)
            {
                if (str_DbColValue == "")
                    sc.Parameters.AddWithValue("@" + str_DbColName, DBNull.Value);
                else
                    sc.Parameters.AddWithValue("@" + str_DbColName, str_DbColValue);
            }

            else if (dt_Sys.Select("columnname='" + str_DbColName + "' and datatype ='uniqueidentifier'").Length > 0)
            {
                if (str_DbColValue == "")
                    sc.Parameters.AddWithValue("@" + str_DbColName, DBNull.Value);
                else
                    sc.Parameters.AddWithValue("@" + str_DbColName, str_DbColValue);
            }

            else
                sc.Parameters.AddWithValue("@" + str_DbColName, str_DbColValue);
        }


        //prepare final SQL
        if (bln_AddMode)
        {
            str_ColumnName = str_ColumnName.Substring(0, str_ColumnName.Length - 1);
            str_ColumnValue = str_ColumnValue.Substring(0, str_ColumnValue.Length - 1);
            str_Query = str_InsertQuery.Replace("%%ColumnName%%", "(" + str_ColumnName + ")").Replace("%%ColumnValue%%", "(" + str_ColumnValue + ")");
        }
        else
        {
            str_UpdateQueryValues = str_UpdateQueryValues.Substring(0, str_UpdateQueryValues.Length - 1);
            str_Query = str_UpdateQuery.Replace("%%UpdateColumnAndValues%%", str_UpdateQueryValues).Replace("%%WhereColumn%%", "'" + str_PrimaryKeyValue + "'");
        }
        sc.CommandText = str_Query;
        al_SC.Add(sc);
        this.DA.ExecuteNonQuery(al_SC);

    }

    protected DataTable PutPostParameterToDataTable(string str_PostData, OneTableDml OTD, bool bln_AddMode)
    {
        str_PostData = HttpUtility.UrlDecode(str_PostData);

        DataTable dt = new DataTable();
        dt.Columns.Add("Seq");
        dt.Columns.Add("CName");
        dt.Columns.Add("CValue");

        string[] str_ValueString = str_PostData.Split('&');
        int int_length = str_ValueString.Length;
        string str_CName = "", str_CValue = "";

        int i = 0;
        for (i = 0; i < int_length; i++)
        {
            string[] str_value = str_ValueString[i].Split('=');
            if (str_value[0].StartsWith("txt_"))
            {
                str_CName = str_value[0].Replace("txt_", "");
                str_CValue = HttpUtility.UrlDecode(str_value[1]);

                if (dt_Sys.Select("columnname='" + str_CName + "' and datatype='bit'").Length > 0)
                {
                    if (str_CValue == "on") str_CValue = "1";
                    else str_CValue = "0";
                }
                this.AddRecord(dt, i.ToString(), str_CName, str_CValue);

                //dt.Rows.Add(dr);
            }
        }

        //created by
        if (bln_AddMode && OTD.TimestampCreate)
        {
            i++;
            this.AddRecord(dt, i.ToString(), "CreatedOn", DateTime.UtcNow.ToString());

            i++;
            this.AddRecord(dt, i.ToString(), "CreatedBy", this.str_UserKey);

        }

        //modified by
        if (!bln_AddMode && OTD.TimestampModify)
        {
            i++;
            this.AddRecord(dt, i.ToString(), "ModifiedOn", DateTime.UtcNow.ToString());

            i++;
            this.AddRecord(dt, i.ToString(), "ModifiedBy", this.str_UserKey);
        }


        //for checkbox, if value is false, then post parameter will be post the parameter name,
        // hence need to add it manually
        if (OTD.CheckBox != null)
        {
            foreach (string str_Check in OTD.CheckBox)
            {
                if (dt.Select("CName='" + str_Check + "'").Length == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr["Seq"] = 111;
                    dr["CName"] = str_Check;
                    dr["CValue"] = "0";
                    dt.Rows.Add(dr);
                }
            }
        }

        //for radiobox, if value is false, then post parameter will be post the parameter name,
        // hence need to add it manually
        if (OTD.RadioBox != null)
        {
            foreach (DataRow dr_Radio in OTD.RadioBox.Rows)
            {
                string str_Radio = dr_Radio["id"].ToString();
                if (dt.Select("CName='" + str_Radio + "'").Length == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr["Seq"] = 111;
                    dr["CName"] = str_Radio;
                    dr["CValue"] = "0";
                    dt.Rows.Add(dr);
                }
            }
        }

        return dt;
    }

    protected void AddRecord(DataTable dt, string str_Seq, string str_CName, string str_CValue)
    {
        DataRow dr = dt.NewRow();
        dr["Seq"] = str_Seq;
        dr["CName"] = str_CName;
        dr["CValue"] = str_CValue;
        dt.Rows.Add(dr);
    }

    public void WebDelete(OneTableDml OTD)
    {
        string str_TableName = OTD.TableName;
        string str_PrimaryKey = OTD.PrimaryKeyColumnName;
        string str_PrimaryKeyValue = OTD.KeyValue;
        string str_OtherColumns = OTD.PostData;

        string str_DeleteQuery = "";
        SqlCommand sc = new SqlCommand();
        if (OTD.LogKeyRequired)
        {
            str_DeleteQuery += " delete from log where logkey in (select logkey from " + str_TableName + " where " + str_PrimaryKey + "=@" + str_PrimaryKey + ");";
        }

        str_DeleteQuery += "delete from " + str_TableName + " where " + str_PrimaryKey + "=@" + str_PrimaryKey;
        sc.Parameters.AddWithValue("@" + str_PrimaryKey, str_PrimaryKeyValue);


        sc.CommandText = str_DeleteQuery;

        this.DA.ExecuteNonQuery(sc);

    }

}