﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CalendarProp
/// </summary>
public class CalendarProp
{
    public string CalendarName { get; set; }
    public string SQL { get; set; }

	public CalendarProp(string str_CalendarType)
	{

        switch (str_CalendarType.ToLower())
        {
            case "training":
                SQL = "select articlekey id, articletitle title,convert(varchar(11),createdon,111) as start, "
                    + "'false' allday,'/Web/SearchResultView.aspx?arid=' + convert(varchar(255),articlekey) as url from v_article where IsPublish=1 ";
                CalendarName = "Training Calendar";
                break;
            case "tqm":
                SQL = "select articlekey id, articletitle title,convert(varchar(11),createdon,111) as start, "
                    + "'false' allday,'/Web/SearchResultView.aspx?arid=' + convert(varchar(255),articlekey) as url from v_article where IsPublish=1 ";
                CalendarName = "TQM Calendar";
                break;
        }
	}
}