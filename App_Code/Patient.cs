﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for Patient
/// </summary>
public class Patient
{
    DataAccess DA = null;
    string str_PatientKey = "";
    PhTemplate PHT;

    public Patient(DataAccess DA, string str_PatientKey)
    {
        this.DA = DA;
        this.str_PatientKey = str_PatientKey;
        this.PHT = new PhTemplate();
    }

    //LoadPatientDetailsPanel
    public void LoadPatientDetailsPanel(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select PatientKey,PatientName,AddressLine1,City,State,Country,FormattedDOB,Gender,LEFT (HealthDetails,70)+'...'Details,HealthDetails,CreatedOn,CreatedBy,Age from v_patient where PatientKey=@PatientKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("PatientDetailsPanel.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        ph.Controls.Add(new LiteralControl(str_HTML));
    }


    //Weight
    public void LoadPatientWeightWidget(PlaceHolder ph, bool btn_Access)
    {
        string Str_Sql = "select Top 1 * from v_PatientWeight where PatientKey=@PatientKey order by WeightDate desc,WeightTime desc";
        SqlCommand cmd = new SqlCommand(Str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientWeight.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Weight%%", "").Replace("%%FormatWeightDate%%", "").Replace("%%WeightTime%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //PatientBodyTemperature
    public void LoadPatientBodyTemperature(PlaceHolder ph, bool btn_Access)
    {
        string Str_Sql = "select Top 1 * from v_PatientBodyTemperature where PatientKey=@PatientKey order by Date desc, Time desc";
        SqlCommand cmd = new SqlCommand(Str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientBodyTemperature.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%OralDisplayName%%", "").Replace("%%AxillaryDisplayName%%", "").Replace("%%Date%%", "").Replace("%%Time%%", "");

        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //PatientPulseRate
    public void LoadPatientPulseRate(PlaceHolder ph, bool btn_Access)
    {
        string Str_Sql = "select Top 1 * from v_PatientPulserate where PatientKey=@PatientKey order by Date desc, Time desc";
        SqlCommand cmd = new SqlCommand(Str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientPulserate.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%BPMDisplayName%%", "").Replace("%%Date%%", "").Replace("%%Time%%", "").
                       Replace("%%MinDisplayName%%", "").Replace("%%MaxDisplayName%%", "");

        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //PatientRespirationRate
    public void LoadPatientRespirationRate(PlaceHolder ph, bool btn_Access)
    {
        string Str_Sql = "select Top 1 * from v_PatientRespirationRate where PatientKey=@PatientKey order by Date desc, Time desc";
        SqlCommand cmd = new SqlCommand(Str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientRespirationRate.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%BPMDisplayName%%", "").Replace("%%Date%%", "").Replace("%%Time%%", "");

        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //PatientBMI
    public void LoadPatientBMI(PlaceHolder ph, bool btn_Access)
    {
        string Str_Sql = "select Top 1 * from v_PatientBMI where PatientKey=@PatientKey order by Date desc, Time desc";
        SqlCommand cmd = new SqlCommand(Str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientBMI.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%BMIDisplayName%%", "").Replace("%%Date%%", "").Replace("%%Time%%", "");

        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("AddViewAccess", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //PatientOxygenSaturation
    public void LoadPatientOxygenSaturation(PlaceHolder ph, bool btn_Access)
    {
        string Str_Sql = "select Top 1 * from v_PatientOxygenSaturation where PatientKey=@PatientKey order by Date desc, Time desc";
        SqlCommand cmd = new SqlCommand(Str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientOxygenSaturation.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%SpO2DisplayName%%", "").Replace("%%PRbpmDisplayName%%", "").Replace("%%Date%%", "").Replace("%%Time%%", "");

        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Height
    public void LoadPatientHeightWidget(PlaceHolder ph, bool btn_Access)
    {
        string Str_Sql = "select Top 1 * from v_PatientHeight where PatientKey=@PatientKey order by HeightDate desc,HeightTime desc";
        SqlCommand cmd = new SqlCommand(Str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientHeight.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Height%%", "").Replace("%%FormatHeightDate%%", "").Replace("%%HeightTime%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Blood glucose
    public void LoadPatientBloodGlucose(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientBloodGlucose where PatientKey=@PatientKey order by MeasurementDate desc,MeasurementTime desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientBloodGlucose.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Measurement%%", "").Replace("%%FormatMeasurementDate%%", "").Replace("%%MeasurementTime%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Blood Pressure
    public void LoadpatientBloodPressure(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientBloodPressure where PatientKey=@PatientKey order by BloodPressureDate desc,BloodPressureTime desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientBloodPressure.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Systolic%%", "").Replace("%%FormatBloodPressuretDate%%", "").Replace("%%BloodPressureTime%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Body Dimension
    public void LoadPatientBodyDimension(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientBodyDimension where PatientKey=@PatientKey order by DimensionDate desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientBodyDimension.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Bodyarea%%", "").Replace("%%FormatDimensionDate%%", "").Replace("%%MeasurementTime%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Excercise
    public void LoadPatientExercise(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientExercise where PatientKey=@PatientKey order by Exercisedate desc,ExerciseTime desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientExercise.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%ExerciseName%%", "").Replace("%%FormatExerciseDate%%", "").Replace("%%ExerciseTime%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Food & Drink
    public void LoadPatientFoodDrink(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select TOP 1 * from v_PatientFoodDrink where PatientKey=@PatientKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientFoodDrink.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Meal%%", "").Replace("%%FormatFoodDrinkDate%%", "").Replace("%%FoodDrinkTime%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Condition
    public void LoadPatientCondition(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientCondition where PatientKey=@PatientKey order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientCondition.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Name%%", "").Replace("%%FormatStartDate%%", "").Replace("%%FormatEndDate%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Medication
    public void LoadPatientMedication(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientMedication where PatientKey=@PatientKey order by CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientMedication.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Name%%", "").Replace("%%FormatStartDate%%", "").Replace("%%FormatEndDate%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Allergy
    public void LoadPatientAllergy(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientAllergy where PatientKey=@PatientKey order by ObservedDate desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientAllergy.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Name%%", "").Replace("%%FormatObservedDate%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Immunization
    public void LoadPatientImmunization(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientImmunization where PatientKey=@PatientKey order by ReceivedDate desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientImmunization.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%VaccineName%%", "").Replace("%%FormatReceivedDate%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Procedure
    public void LoadPatientProcedure(PlaceHolder ph, bool btn_Access)
    {
        string str_Sql = "select Top 1 * from v_PatientProcedure where PatientKey=@PatientKey order by Date desc,ProcedureTime desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", this.str_PatientKey);
        DataTable dt = this.DA.GetDataTable(cmd);
        string str_Template = this.PHT.ReadFileToString("WidgetPatientProcedure.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in dt.Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        if (str_HTML == "")
            str_HTML = str_Template.Replace("%%Name%%", "").Replace("%%FormatDate%%", "");
        if (btn_Access) str_HTML = str_HTML.Replace("%%AddViewAccess%%", "block");
        else str_HTML = str_HTML.Replace("%%AddViewAccess%%", "none");
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    //Patient Details
    public void LoadPatientDetails(PlaceHolder Ph, bool btn_Access)
    {
        string str_Sql = " select i.ImageKey,ISNULL(i.FilePath,'../Robust/app-assets/images/portrait/small/avatar-s-1.png')FilePath,PatientKey,ISNULL (FirstName,'')+' '+ISNULL (LastName,'') as PatientName,AddressLine1,AddressLine2,City,State,Country,CONVERT(varchar(11),DOB,101) DOB,CASE Gender WHEN 1 THEN 'Male' WHEN 2 THEN 'Female' WHEN 3 THEN 'Other' END Gender,"
                       + " CASE MaritalStatus WHEN 1 THEN 'Married' WHEN 2 THEN 'Single' WHEN 3 THEN 'Divorced' WHEN 4 THEN 'Widowed' END MaritalStatus, HealthDetails, p.CreatedOn, p.ModifiedOn, p.CreatedBy,"
                       + " p.ModifiedBy,CONCAT(DATEDIFF(yy,DOB,getdate()),'Years') AS [AgeYears],CONCAT(DATEDIFF(MM,DOB,getdate()),'Months') AS [AgeMonths],"
                       + " CONCAT(DATEDIFF(dd, DOB, getdate()), 'Days') AS[AgeDays] from patient p left outer join Image i on p.PatientKey=i.UserKey where PatientKey=@PatientKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@PatientKey", str_PatientKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        string str_Template = this.PHT.ReadFileToString("PatientView.txt"), str_HTML = "";
        str_Template = str_Template.Replace("%%PatientKey%%", this.str_PatientKey);
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_HTML = this.PHT.ReplaceVariableWithValue(dr, str_Template);
        }
        Ph.Controls.Add(new LiteralControl(str_HTML));
    }
}
