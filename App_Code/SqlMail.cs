﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for SqlMail
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class SqlMail : System.Web.Services.WebService {

    public SqlMail () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public void  SendTestMail(string str_Recipients)
    {
        this.Push(str_Recipients, "Test Mail from iCGUWEB via SQL", "Body Content");
    }

    [WebMethod]
    public void Push(string str_Recipients,string str_Subject,string str_Body)
    {
        string str_Sql = "exec CGU_sendemail @p_recipients, @p_subject, @p_body";
        System.Data.SqlClient.SqlCommand sc = new System.Data.SqlClient.SqlCommand(str_Sql);
        sc.Parameters.AddWithValue("@p_recipients", str_Recipients);
        sc.Parameters.AddWithValue("@p_subject", str_Subject);
        sc.Parameters.AddWithValue("@p_body", str_Body);
        new DataAccess().ExecuteNonQuery(sc);
    }
    
}
