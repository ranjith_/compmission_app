﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Collections;

/// <summary>
/// Summary description for JQDataTables
/// </summary>
public class JQDataTables
{
    string str_EditLinkName = "Modify";
    string str_RemoveLinkName = "Remove";

    ArrayList al_Links = new ArrayList();
    ArrayList al_LinkRedirect = new ArrayList();
    

	public JQDataTables()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    protected void CheckAndAddLinks(DataTable dt)
    {
        string str_ProjectLink = "Glorian/ProjectSnapshot.aspx?fid=project_form&md=e&mid=menu_ProjectSnapshot&ProjectId=%1%";
        string str_TaskLink = "Glorian/TaskSnapshot.aspx?pageid=aTasks_page&taskid=%1%";
        string str_RevisionLInk = "OnePageGen.aspx?pageid=aRevision_page&revisionid=%1%";
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            if (dt.Columns[i].ColumnName.ToLower().Contains("projectid"))
            {
                al_Links.Add(i);
                al_LinkRedirect.Add(str_ProjectLink);
            }
            else if (dt.Columns[i].ColumnName.ToLower().Contains("taskid"))
            {
                al_Links.Add(i);
                al_LinkRedirect.Add(str_TaskLink);
            }
            else if (dt.Columns[i].ColumnName.ToLower().Contains("revisionid"))
            {
                al_Links.Add(i);
                al_LinkRedirect.Add(str_RevisionLInk);
            }

        }
    }

    public StringBuilder BuildTableDataForGrid(DataTable dt, string str_TableName, string str_Edit, string str_Remove)
    {
        this.CheckAndAddLinks(dt);

        StringBuilder SB = new StringBuilder();
        SB.Append("<table id=\"" + str_TableName + "\" class=\"table table-striped table-bordered table-hover \" width=\"100%\" cellspacing=\"0\">");

        //header data
        SB.Append("<thead><tr>");
        foreach (DataColumn dc in dt.Columns)
        {
            SB.Append("<th>" + dc.Caption + "</th>");
        }
        if (str_Edit != "") SB.Append("<th>View</th>");
        if (str_Remove != "") SB.Append("<th>Remove</th>");
        SB.Append("</tr></thead>");

        //body data
        string str_EditLink = str_Edit;
        string str_RemoveLink = str_Remove;
        string str_TD = "", str_TDLink = "";
        SB.Append("<tbody>");
        foreach (DataRow dr in dt.Rows)
        {
            str_EditLink = str_Edit;
            str_RemoveLink = str_Remove;
           
            SB.Append(" <tr>");
            for (int i = 0; i < dt.Columns.Count; i++)
            { 
                str_TDLink="";
                if (al_Links.Contains(i))
                {
                    str_TDLink = al_LinkRedirect[al_Links.IndexOf(i)].ToString();
                    str_TDLink = str_TDLink.Replace("%1%", dr[i].ToString());
                    str_TD = "<td> <a href=###>" + dr[i].ToString() + "</a></td>";
                    str_TD = str_TD.Replace("###", str_TDLink);
                }
                else
                    str_TD = "<td>" + dr[i].ToString() + "</td>";

                SB.Append(str_TD);
                if (str_EditLink.Contains("{" + dt.Columns[i].ColumnName + "}"))
                {
                    str_EditLink = str_EditLink.Replace("{" + dt.Columns[i].ColumnName + "}", dr[i].ToString());
                    str_RemoveLink = str_RemoveLink.Replace("{" + dt.Columns[i].ColumnName + "}", dr[i].ToString());
                }
            }
            //replace edit and modify link if provided
            if (str_Edit != "") SB.Append("<td><a class='btn btn-primary btn-circle' href='" + str_EditLink + "'> <i class='fa fa-list'></i></a></td>");
            if (str_Remove != "") SB.Append("<td><a class='btn btn-warning btn-circle' onclick=\"return confirm('Are you sure you want to delete this record?')\"; href='" + str_RemoveLink + "'><i class='fa fa-times'></i></a></td>");
    
            SB.Append(" </tr>");
        }
        SB.Append("</tbody>");
        SB.Append("</table>");

        return SB;
        //hl.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this record?');");
    }

}