﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for RMaster
/// </summary>
public class RMaster
{
    MasterPage master;
    DataAccess DA;
    SessionCustom SC;
    Common CM;
    SqlCommand cmd;
    PhTemplate PHT;
    DataTable DT;
    DataSet ds = null;
    public enum LinkType { HRef, OnClick };


    public RMaster(MasterPage master)
    {
        this.SC = new SessionCustom(true);
        this.CM = new Common();
        this.DA = new DataAccess();
        this.PHT = new PhTemplate();
        this.DA = new DataAccess();
        this.master = master;
        master.FindControl("div_MasterHeader").Visible = true;
        master.FindControl("div_Footer").Visible = true;
        //((Label)master.FindControl("lbl_MasterPageTitle")).Text = "";
        //((Label)master.FindControl("lbl_Message")).Text = "";
        //master.FindControl("div_Message").Visible = false;
        //master.FindControl("div_TechnicianHeader").Visible = false;
        //master.FindControl("div_MasterHeader").Visible = true;

        ((HtmlAnchor)master.FindControl("a_MasterAdd")).Visible = false;
        //((HtmlAnchor)master.FindControl("a_MasterList")).Visible = false;
        //((HtmlAnchor)master.FindControl("a_MasterDelete")).Visible = false;
        //((HtmlAnchor)master.FindControl("a_MasterCopy")).Visible = false;
        //((HtmlAnchor)master.FindControl("a_MasterApprove")).Visible = false;
        //((HtmlAnchor)master.FindControl("a_MasterApply")).Visible = false;
        //((HtmlAnchor)master.FindControl("a_MasterReject")).Visible = false;
        //((HtmlAnchor)master.FindControl("a_RequestEdit")).Visible = false;

    }

    public void SetPageTitle(string str_Caption)
    {
        ((Label)master.FindControl("lbl_MasterPageTitle")).Text = str_Caption;
    }

    public void SetPageMessage(string str_Caption, bool bln_Success)
    {
        ((Label)master.FindControl("lbl_Message")).Text = str_Caption;
        master.FindControl("div_Message").Visible = true;

        if (bln_Success == false) ((HtmlGenericControl)master.FindControl("div_Message")).Attributes.Add("class", "alert alert-danger no-border");

    }

    public void SetAddLink(string str_Caption, string str_Link)
    {
        this.SetAddLink(str_Caption, str_Link, LinkType.HRef);
    }

    public void SetAddLink(string str_Caption, string str_Link, LinkType str_Type)
    {
        HtmlAnchor HA = ((HtmlAnchor)master.FindControl("a_MasterAdd"));
        if (str_Type == LinkType.HRef) HA.HRef = str_Link;
        else if (str_Type == LinkType.OnClick) HA.Attributes.Add("onclick", str_Link);

        ((Label)master.FindControl("lbl_MasterAdd")).Text = str_Caption;
        HA.Visible = true;

    }

    public void SetDeleteLink(string str_Caption, string str_Link)
    {
        this.SetDeleteLink(str_Caption, str_Link, LinkType.HRef);
    }

    public void SetDeleteLink(string str_Caption, string str_Link, LinkType str_Type)
    {
        HtmlAnchor HA = ((HtmlAnchor)master.FindControl("a_MasterDelete"));
        if (str_Type == LinkType.HRef) HA.HRef = str_Link;
        else if (str_Type == LinkType.OnClick) HA.Attributes.Add("onclick", str_Link);

        ((Label)master.FindControl("lbl_MasterDelete")).Text = str_Caption;
        HA.Visible = true;

    }

    public void SetCopyLink(string str_Caption, string str_Link, LinkType str_Type)
    {
        HtmlAnchor HA = ((HtmlAnchor)master.FindControl("a_MasterCopy"));
        if (str_Type == LinkType.HRef) HA.HRef = str_Link;
        else if (str_Type == LinkType.OnClick) HA.Attributes.Add("onclick", str_Link);

        ((Label)master.FindControl("lbl_MasterCopy")).Text = str_Caption;
        HA.Visible = true;

    }

    public void SetListLink(string str_Caption, string str_Link)
    {
        this.SetListLink(str_Caption, str_Link, LinkType.HRef);
    }

    public void SetListLink(string str_Caption, string str_Link, LinkType str_Type)
    {
        HtmlAnchor HA = ((HtmlAnchor)master.FindControl("a_MasterList"));
        if (str_Type == LinkType.HRef) HA.HRef = str_Link;
        else if (str_Type == LinkType.OnClick) HA.Attributes.Add("onclick", str_Link);

        ((Label)master.FindControl("lbl_MasterList")).Text = str_Caption;
        HA.Visible = true;
    }

    public void SetApproveLink(string str_Caption, string str_Link)
    {
        this.SetApproveLink(str_Caption, str_Link, LinkType.HRef);
    }

    public void SetApproveLink(string str_Caption, string str_Link, LinkType str_Type)
    {
        HtmlAnchor HA = ((HtmlAnchor)master.FindControl("a_MasterApprove"));
        if (str_Type == LinkType.HRef) HA.HRef = str_Link;
        else if (str_Type == LinkType.OnClick) HA.Attributes.Add("onclick", str_Link);

        ((Label)master.FindControl("lbl_MasterApprove")).Text = str_Caption;
        HA.Visible = true;
    }

    public void SetApplyLink(string str_Caption, string str_Link)
    {
        this.SetApplyLink(str_Caption, str_Link, LinkType.HRef);
    }

    public void SetApplyLink(string str_Caption, string str_Link, LinkType str_Type)
    {
        HtmlAnchor HA = ((HtmlAnchor)master.FindControl("a_MasterApply"));
        if (str_Type == LinkType.HRef) HA.HRef = str_Link;
        else if (str_Type == LinkType.OnClick) HA.Attributes.Add("onclick", str_Link);

        ((Label)master.FindControl("lbl_MasterApply")).Text = str_Caption;
        HA.Visible = true;
    }

    public void SetRejectLink(string str_Caption, string str_Link)
    {
        this.SetRejectLink(str_Caption, str_Link, LinkType.HRef);
    }

    public void SetRejectLink(string str_Caption, string str_Link, LinkType str_Type)
    {
        HtmlAnchor HA = ((HtmlAnchor)master.FindControl("a_MasterReject"));
        if (str_Type == LinkType.HRef) HA.HRef = str_Link;
        else if (str_Type == LinkType.OnClick) HA.Attributes.Add("onclick", str_Link);

        ((Label)master.FindControl("lbl_MasterReject")).Text = str_Caption;
        HA.Visible = true;
    }

    public void SetEditLink(string str_Caption, string str_Link)
    {
        this.SetEditLink(str_Caption, str_Link, LinkType.OnClick);
    }

    public void SetEditLink(string str_Caption, string str_Link, LinkType str_Type)
    {
        HtmlAnchor HA = ((HtmlAnchor)master.FindControl("a_RequestEdit"));
        if (str_Type == LinkType.HRef) HA.HRef = str_Link;
        else if (str_Type == LinkType.OnClick) HA.Attributes.Add("onclick", str_Link);

        ((Label)master.FindControl("lbl_Edit")).Text = str_Caption;
        HA.Visible = true;

    }

    public void HideMasterHeader()
    {
        master.FindControl("div_MasterHeader").Visible = false;
    }
    public void HideFooter()
    {
        master.FindControl("div_Footer").Visible = false;
    }

}