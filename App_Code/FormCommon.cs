﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for FormCommon
/// </summary>
public class FormCommon
{
    AppVar AV;
	public FormCommon()
	{
        this.AV = new AppVar();
	}

    public void ConvertAndUpdateDateToTimeZone(DataSet ds_TableRecords)
    {
        ArrayList al_Columns;
        foreach (DataTable dt in ds_TableRecords.Tables)  //go through each table
        {
            al_Columns = new ArrayList();
            if (dt.Rows.Count > 0)  // go inside if table has records
            {
                foreach (DataColumn dc in dt.Columns)  //loop column and add date column to array
                {
                    if (dc.DataType == typeof(System.DateTime))
                    {
                        al_Columns.Add(dc);
                    }
                }
                if (al_Columns.Count > 0) // if date field exists then loop each row;
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        foreach (DataColumn dc in al_Columns)
                        {
                            if (dr[dc].ToString() != "") dr[dc] = Convert.ToDateTime(dr[dc]).AddHours(this.AV.TimeZoneHours).AddMinutes(this.AV.TimeZoneMinutes);
                        }
                    }
                }
            }
        }
    }
}