﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ILog
/// </summary>
public class ILog
{
    public ILog()
    {
        this.ErrorMessage = "";
        this.RecordCount = 0;
        this.RecordInsertedCount = 0;
    }
    public string ErrorMessage { get; set; }
    public int RecordCount { get; set; }
    public int RecordInsertedCount { get; set; }
    public int CustomerId { get; set; }
    public int PracticeId { get; set; }
}
