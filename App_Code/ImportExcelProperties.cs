﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for ImportExcelProperties
/// </summary>
public class ImportExcelProperties
{
    string str_ImportType = "";
    string str_FilePath = "";
    string str_ExcelColumnNames = "";
    string str_ExcelWhere = "";
    string str_TableName = "";
    string str_ViewName = "";
    string str_ProcedureName = "";
    string str_ProcedureNameDelete = "";
    string str_Caption = "";

    public string ImportType { get { return this.str_ImportType; } set { this.str_ImportType = value; } }
    public string ImportFilePath { get { return this.str_FilePath; } set { this.str_FilePath = value; } }
    public string ExcelColumnNames { get { return this.str_ExcelColumnNames; } set { this.str_ExcelColumnNames = value; } }
    public string ExcelWhere { get { return this.str_ExcelWhere; } set { this.str_ExcelWhere = value; } }
    public string TableName { get { return this.str_TableName; } set { this.str_TableName = value; } }
    public string ViewName { get { return this.str_ViewName; } set { this.str_ViewName = value; } }
    public string Caption { get { return this.str_Caption; } set { this.str_Caption = value; } }
    public string ProcedureName { get { return this.str_ProcedureName; } set { this.str_ProcedureName = value; } }
    public string ProcedureNameDelete { get { return this.str_ProcedureNameDelete; } set { this.str_ProcedureNameDelete = value; } }

    public ImportExcelProperties(string str_ImportType)
	{
        this.str_ImportType = str_ImportType;
        this.UpdateProperties();
	}

    protected void UpdateProperties()
    {
        switch (ImportType)
        {
            case "1": //Salary Component Import
                Caption = "Salary Component - Import CSV file";
                ImportFilePath = "SalaryComponent";
                ExcelColumnNames = "[SalaryYear],[SalaryMonth],[EmpCode],[CompCode],[CompValue]";
                ExcelWhere = "1=1";
                TableName = "SalaryComponent";
                ViewName = "V_SalaryComponent";
                ProcedureName = "";
                ProcedureNameDelete = "";
                break;
            case "2": //Present Days
                Caption = "Employee Present Days";
                ImportFilePath = "PresentDays";
                ExcelColumnNames = "[SalaryYear],[SalaryMonth],[EmpCode],[PresentDays]";
                ExcelWhere = "1=1";
                TableName = "SalaryComponent";
                ViewName = "V_SalaryComponent";
                ProcedureName = "";
                ProcedureNameDelete = "";
                break;

            case "3": // Import Amendments
                Caption = "Amendments";
                ImportFilePath = "Amendments";
                ExcelColumnNames = "[EMPCODE],[PF],[ESI],[CTC],[REV],[VP],[LO]";
                ExcelWhere = "1=1";
                TableName = "";
                ViewName = "";
                ProcedureName = "";
                ProcedureNameDelete = "";
                break;

            case "4": // Import Amendments
                Caption = "Employee Shift ";
                ImportFilePath = "EmployeeShift";
                ExcelColumnNames = "[EmployeeCode],[Month Dates] ( like 1/1/2017,1/2/2017 ... 31/1/2017)";
                ExcelWhere = "1=1";
                TableName = "";
                ViewName = "";
                ProcedureName = "";
                ProcedureNameDelete = "";
                break;

        }
    }
}
