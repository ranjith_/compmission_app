using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Collections;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for PaySummary
/// </summary>
public class PaySummary
{
    ApplicationCommonFunction acf;
    HDDatabase hddb;
    string str_message;

	public PaySummary()
    {
        str_message = "";
        acf = new ApplicationCommonFunction();
        hddb = new HDDatabase();
	}
    public bool close_period(int in_new_period)
    {
        bool bln_close = false;
        string str_sql = "update asetting set set_value =" + in_new_period.ToString() + "  where user_id=" + acf.get_session_user() + " and set_id='current_period'";
        SqlCommand sc = new SqlCommand (str_sql);
        if (hddb.ExecuteCmdObj(sc) == -1)
        {
            this.str_message = hddb.get_user_message(); 
        }
        else
        {
            HttpContext.Current.Session["current_period"] = in_new_period;
            bln_close = true;
        }

        return bln_close;

    }

    public bool rollback_payroll()
    {
        bool bln_rollback = false;
        string str_user_id = acf.get_session_user();
        string str_current_period = acf.get_current_period();
        string str_fiscal_year = acf.get_fiscal_year();
        SqlCommand sc  ;
        ArrayList al_sql = new ArrayList ();
        string str_sql = "";

        str_sql = "update apayroll set status='T' where user_id=" + str_user_id + " and fiscal_year=" + str_fiscal_year + " and period_number=" + str_current_period + ";";
        sc = new SqlCommand (str_sql );
        al_sql.Add (sc );

        str_sql = "update apayprocess set process_dt=null, processed=0 where user_id=" + str_user_id + " and fiscal_year=" + str_fiscal_year + " and period_number=" + str_current_period + ";";
        sc = new SqlCommand (str_sql );
        al_sql.Add (sc );

        ArrayList al_log =  hddb.ExecuteCmdObj(al_sql);
        if (al_log.Count != al_sql .Count )
        {
            this.str_message = hddb.get_user_message();  
        }
        else
        {
            bln_rollback = true;
        }
        return  bln_rollback ;
    }

    public bool process_payroll()
    {
        bool bln_process = false;

        string str_user_id = acf.get_session_user();
        string str_current_period = acf.get_current_period();
        string str_fiscal_year = acf.get_fiscal_year();
        ArrayList al_log = new ArrayList();
        ArrayList al_sql = new ArrayList();

        string str_sql = "select * from aemployee a, apayroll b where a.user_id=b.user_id and a.emp_id=b.emp_id";
        str_sql += " and a.user_id=" + str_user_id + " and fiscal_year=" + str_fiscal_year + " and period_number=" + str_current_period + " and  is_active=1;";
        str_sql += "select * from apayroll_comp where user_id=" + str_user_id + " and fiscal_year=" + str_fiscal_year + " and period_number=" + str_current_period + ";";
        str_sql += "select * from asetting where user_id=" + str_user_id + ";";
        str_sql += "select * from aemployee where is_active=1 and user_id=" + str_user_id  + ";";
        str_sql += "select * from apayprocess where user_id=" + str_user_id  + " and fiscal_year=" + str_fiscal_year + " and period_number=" + str_current_period + ";";

        DataSet ds_emp = hddb.GetDataSet(str_sql);
        DataTable dt_emppay = ds_emp.Tables[0];
        DataTable dt_emppay_comp = ds_emp.Tables[1];
        DataTable dt_setting = ds_emp.Tables[2];
        DataTable dt_emp = ds_emp.Tables[3];
        DataTable dt_paylog =  ds_emp.Tables[4];

        SqlCommand sc;

        al_log .Add("Started Processing....");
        if (acf.convert_to_boolean(dt_paylog.Rows[0]["processed"].ToString()))
        {
            this.str_message = "Payroll Already processed, to re-process, please rollback & try ";
        }
        else
        {
            sc = new SqlCommand();
            sc.CommandText = "Update apayprocess set processed=1, process_dt=getdate() where user_id=" + str_user_id + " and fiscal_year=" + str_fiscal_year + " and period_number=" + str_current_period + ";";
            al_sql.Add(sc);

            foreach (DataRow dr_emppay in dt_emppay.Select())
            {
                al_log.Add("Employee ID: " + dr_emppay["emp_id"].ToString());

                //update status update
                sc = new SqlCommand();
                sc.CommandText = "update apayroll set status='C' where user_id=" + str_user_id + " and fiscal_year=" + str_fiscal_year + " and period_number=" + str_current_period + " and emp_id='" + dr_emppay["emp_id"].ToString() + "'";
                al_sql.Add(sc);

                //comp value update
                sc = new SqlCommand();
                sc.CommandText = "update apayroll_comp set comp_calc_value = comp_sys_value, comp_value=comp_sys_value  where user_id=" + str_user_id + " and fiscal_year=" + str_fiscal_year + " and period_number=" + str_current_period + " and emp_id='" + dr_emppay["emp_id"].ToString() + "'";
                al_sql.Add(sc);
            }

            ArrayList al_sql_log = hddb.ExecuteCmdObj(al_sql);
            if (al_sql_log.Count == al_sql.Count)
            {
                bln_process = true;
            }
            else
            {
                this.str_message = hddb.get_user_message();
            }

        }    

        return bln_process;
    }

    public string  get_message()
    {
        return this.str_message;
    }

    public DataTable get_attendance(string str_emp_id)
    {
        DataTable dt_attend = new DataTable();
        return dt_attend;
    }
    public DataTable get_attendance(ArrayList  al_emp_id)
    {
        DataTable dt_attend = new DataTable();
        return dt_attend;
    }

    public DataTable get_pay_summary(string str_emp_id)
    {

        ArrayList al_htmlrow = new ArrayList();

        string str_sql_payroll = "select a.*, period_name from apayroll a, fiscal_year_period b ";
        str_sql_payroll += " where b.frequency='M' and  a.fiscal_year=b.fiscal_year ";
        str_sql_payroll += " and a.period_number=b.period_number and a.user_id=" + acf.get_session_user() + " and "; ;
        str_sql_payroll += " a.fiscal_year=" + acf.get_fiscal_year() + " and a.emp_id='" + str_emp_id + "'";

        string str_sql_payroll_comp = "select distinct a.comp_id, b.comp_name, b.comp_type, b.is_taxable from apayroll_comp a, vsalary_comp_all b where ";
        str_sql_payroll_comp += " a.user_id=" + acf.get_session_user() + " and ";
        str_sql_payroll_comp += " a.fiscal_year=" + acf.get_fiscal_year() + " and a.emp_id='" + str_emp_id + "'";
        str_sql_payroll_comp += " and a.user_id=b.user_id ";
        str_sql_payroll_comp += " and a.fiscal_year=b.fiscal_year and a.comp_id=b.comp_id ";


        string str_sql_payroll_comp_value = "select b.*, a.status, c.comp_type from apayroll a, apayroll_comp b, vsalary_comp_all c  where ";
        str_sql_payroll_comp_value += " a.user_id=b.user_id and a.fiscal_year=b.fiscal_year and a.period_number=b.period_number ";
        str_sql_payroll_comp_value += " and b.user_id=c.user_id and b.fiscal_year = c.fiscal_year and b.comp_id=c.comp_id ";
        str_sql_payroll_comp_value += " and a.emp_id=b.emp_id and a.user_id=" + acf.get_session_user() + " and ";
        str_sql_payroll_comp_value += " a.fiscal_year=" + acf.get_fiscal_year() + " and a.emp_id='" + str_emp_id + "'";


        string str_sql = str_sql_payroll + ";" +
                         str_sql_payroll_comp + ";" +
                         str_sql_payroll_comp_value + ";" ;

        DataSet ds_gross = hddb.GetDataSet(str_sql);

        DataTable dt_payroll = ds_gross.Tables[0];
        DataTable dt_salcomp = ds_gross.Tables[1];
        DataTable dt_salcompvalue = ds_gross.Tables[2];


        DataTable dt_comptotal = new DataTable();
        DataTable dt_return = new DataTable();

        DataColumn dc_period = new DataColumn("period", Type.GetType("System.Int16"));
        dc_period.Caption = "Period";
        dt_comptotal.Columns.Add(dc_period);
        
        DataColumn dc_period_name = new DataColumn("period_name");
        dc_period_name.MaxLength = 25;
        dc_period_name.Caption = "Period Name";
        dt_comptotal.Columns.Add(dc_period_name);

        DataColumn dc_status = new DataColumn("status");
        dc_status.Caption = "Status";
        dt_comptotal.Columns.Add(dc_status);

        foreach (DataRow dr_comp_id in dt_salcomp.Select())
        {
            DataColumn dc_comp = new DataColumn(dr_comp_id["comp_id"].ToString(), Type.GetType("System.Double"));
            dc_comp.Caption = dr_comp_id["comp_name"].ToString();
            dt_comptotal.Columns.Add(dc_comp);
        }

        DataColumn dc_gross_salary = new DataColumn("gross_salary", Type.GetType("System.Double"));
        dc_gross_salary.Caption = "Gross Salary";
        dt_comptotal.Columns.Add(dc_gross_salary);

        //clone a dt_comptotal
        DataTable dt_paysummary = dt_comptotal.Clone(); 
        
        // add newrow and set values to "0"
        dt_comptotal.Rows.Add(dt_comptotal.NewRow());
        foreach (DataColumn dc in dt_comptotal.Columns)
        {
            dt_comptotal.Rows[0][dc] = 0;
        }

        string str_comp_id = "";
        string str_comp_value = "";
        string str_comp_type = "";
        string str_period_number = "";
        double dbl_gross_salary = 0;

        foreach (DataRow dr_payroll in dt_payroll.Select("", "period_number"))
        //for (int i = 1; i <= 12; i++)
        {

            DataRow dr_paysummary = dt_paysummary.NewRow();

            str_period_number = dr_payroll["period_number"].ToString();

            dr_paysummary["period"] = str_period_number;
            dr_paysummary["period_name"] = dr_payroll["period_name"].ToString();
            if (dr_payroll["Status"].ToString() == "C")
                dr_paysummary["status"] = "Processed";
            else
                dr_paysummary["status"] = "Tentative";

            dbl_gross_salary = 0;

            foreach (DataRow dr_salcomp in dt_salcomp.Select())
            {
                str_comp_id = dr_salcomp["comp_id"].ToString();

                DataRow [] dr_salcompvalue = dt_salcompvalue.Select("period_number=" + str_period_number + " and comp_id='" + str_comp_id + "'") ;
                if (dr_salcompvalue.Length > 0)
                    str_comp_value = dr_salcompvalue[0]["comp_value"].ToString();
                else
                    str_comp_value = "0.00";

                dr_paysummary[str_comp_id] = str_comp_value;

                //DataRow [] dr_salcomp = dt_salcomp.Select("comp_id='" + str_comp_id + "'");
                str_comp_type = dr_salcomp["comp_type"].ToString();
                if ((str_comp_type == "EAR" || str_comp_type == "REI") && (acf.convert_to_boolean(dr_salcomp["is_taxable"].ToString()) == true))
                {
                    dbl_gross_salary += Convert.ToDouble(str_comp_value);
                }
                dt_comptotal.Rows[0][str_comp_id] = Convert.ToDouble(dt_comptotal.Rows[0][str_comp_id]) + Convert.ToDouble(str_comp_value);
            }

            dr_paysummary["gross_salary"] = dbl_gross_salary;

            dt_paysummary.Rows.Add(dr_paysummary);

            dt_comptotal.Rows[0]["gross_salary"] = Convert.ToDouble(dt_comptotal.Rows[0]["gross_salary"]) + dbl_gross_salary;

        }

        DataRow dr_paytotal = dt_paysummary.NewRow(); 
        foreach (DataColumn dc in dt_comptotal.Columns)
        {
            dr_paytotal[dc.ColumnName] = dt_comptotal.Rows[0][dc.ColumnName]; 
        }

        dt_paysummary.Rows.Add(dr_paytotal); 
        
        return dt_paysummary ;

    }

    public  void get_paysummary_loaded_control(DataTable dt_paysummary, Control ph_gross_salary)
    {
        
        HtmlTableRow htr_caption = new HtmlTableRow();
        htr_caption.Align = "center";
        htr_caption.BgColor = "#84A38E";

        foreach (DataColumn dc in dt_paysummary.Columns)
        {
            HtmlTableCell htc_caption = new HtmlTableCell();
            htc_caption.Controls.Add(new LiteralControl(dc.Caption));
            htr_caption.Controls.Add(htc_caption);
        }
        ph_gross_salary.Controls.Add(htr_caption);

        string str_value = "";
        int i = 0;
        foreach (DataRow dr in dt_paysummary.Select("Period<>0", "Period"))
        {
            HtmlTableRow htr_period = new HtmlTableRow();
            htr_period.Align = "right";
            htr_period.Height = "20"; 
            
            i++;
            if (i % 2 == 0) htr_period.BgColor = "#F1F5F3";

            foreach (DataColumn dc in dt_paysummary.Columns)
            {
                HtmlTableCell htc_period = new HtmlTableCell();
                
                if (dc.DataType.FullName == "System.Double")
                {
                    str_value = acf.num_format(  dr[dc].ToString());
                    htc_period.Align = "right";
                    htc_period.Controls.Add(new LiteralControl(str_value));
                }
                else
                {
                    htc_period.Align = "left";
                    htc_period.Controls.Add(new LiteralControl(dr[dc].ToString()));
                }
                
                htr_period.Controls.Add(htc_period);
            }
            ph_gross_salary.Controls.Add(htr_period);
        }

        
        foreach (DataRow dr in dt_paysummary.Select("Period=0", "Period"))
        {
            HtmlTableRow htr_period = new HtmlTableRow();
            htr_period.Align = "right";
            foreach (DataColumn dc in dt_paysummary.Columns)
            {
                HtmlTableCell htc_period = new HtmlTableCell();
                htc_period.BgColor = "#D9E2DC";   

                if (dc.ColumnName == "period" || dc.ColumnName == "period_name" || dc.ColumnName == "status")
                {
                    htc_period.Controls.Add(new LiteralControl(""));
                }
                else
                {
                    str_value = acf.num_format(dr[dc].ToString());
                    htc_period.Controls.Add(new LiteralControl(str_value));
                }
                
                if (dc.DataType.FullName == "System.Double")
                    htc_period.Align = "right";
                else
                    htc_period.Align = "left";
                //htc_period.Width = "100";
                htr_period.Controls.Add(htc_period);
            }
            ph_gross_salary.Controls.Add(htr_period);
        }
    }

}
