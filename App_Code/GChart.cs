﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GChart
/// </summary>
/// 
public class GChart
{
    string str_TemplateFile = "";
    int int_ChartHeight = 0;
    PhTemplate PHT = new PhTemplate();
    bool bln_CreateDivControl = true;
    string str_Title = "";
    static string[] ColourValues = new string[] {
                "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    };
    public GChart()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string ChartTemplateFile { get { return this.str_TemplateFile; } set { this.str_TemplateFile = value; } }

    public int ChartHeight { get { return this.int_ChartHeight; } set { this.int_ChartHeight = value; } }

    public bool CreateDivControl { get { return this.bln_CreateDivControl; } set { this.bln_CreateDivControl = value; } }

    public string ChartTitle{ get { return this.str_Title; } set { this.str_Title = value; } }

    public string GetChartTemplate(string str_TemplateFileName, string str_DivName)
    {
        string str_ChartTemplate = "";

        if (this.ChartTemplateFile!="")
        {
            str_ChartTemplate = this.ChartTemplateFile;
        }
        else
        {
            str_ChartTemplate = PHT.ReadFileToString(str_TemplateFileName);
        }


        str_ChartTemplate = str_ChartTemplate.Replace("%%ChartDivName%%", str_DivName);

        //set chart height
        if (this.ChartHeight > 0)
            str_ChartTemplate = str_ChartTemplate.Replace("%%Height%%", "height: " + this.ChartHeight.ToString() + ",");
        else
            str_ChartTemplate = str_ChartTemplate.Replace("%%Height%%", "");

        //create html div control
        if (this.CreateDivControl == true)
            str_ChartTemplate = str_ChartTemplate.Replace("%%HtmlDivTag%%", "<div id='" + str_DivName + "'></div>");
        else
            str_ChartTemplate = str_ChartTemplate.Replace("%%HtmlDivTag%%", "");

        str_ChartTemplate = str_ChartTemplate.Replace("%%Title%%", ChartTitle);

        return str_ChartTemplate;
    }
   
    public string DrawGougeChart(DataTable dt, string[] str_DisplayColumn, string[] str_ValueColumn, string str_DivName)
    {
        string str_ChartTemplate = PHT.ReadFileToString("GaugeChart.txt");
        string str_ChartContent = "", str_ValueContent = "";

        int displayCount = str_DisplayColumn.Length;
        //for (int i = 0; i < displayCount; i++)
        //{
        //    str_ChartContent += "'" + str_DisplayColumn.GetValue(i).ToString() + "',";
        //}
        //str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);
        //str_ChartContent = "[" + str_ChartContent + "],";

        foreach (DataRow dr in dt.Rows)
        {
            str_ValueContent = "";
            for (int i = 0; i < displayCount; i++)
            {
                    str_ValueContent += "['"+ str_DisplayColumn.GetValue(i).ToString() + "'," + dr[str_ValueColumn.GetValue(i).ToString()].ToString() + "],";
               
            }
            str_ValueContent = str_ValueContent.Substring(0, str_ValueContent.Length - 1);
            str_ValueContent =  str_ValueContent + ",";
            str_ChartContent += str_ValueContent;
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);

        str_ChartTemplate = str_ChartTemplate.Replace("%%GoughChartValues%%", str_ChartContent);
        str_ChartTemplate = str_ChartTemplate.Replace("%%ChartDivName%%", str_DivName);

        return str_ChartTemplate;
    }
    
}