﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GChart
/// </summary>
/// 
public class GChartPie : GChart
{
    PhTemplate PHT = new PhTemplate();
    bool bln_Is3d = true;
    public string str_RedirectUrl = "";
    static string[] ColourValues = new string[] {
                "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    };

    public enum pieSliceOption
    {
        percentage,
        value,
        label,
        none
    };

string str_DivName = "";

    public pieSliceOption pieSliceText { set; get; }

    public bool is3D { get { return this.bln_Is3d; } set { this.bln_Is3d = value; } }

    public string RedirectUrl { get { return this.str_RedirectUrl; } set { this.str_RedirectUrl = value; } }

    public GChartPie(string str_DivName)
    {
        this.str_DivName = str_DivName;
        this.pieSliceText = pieSliceOption.percentage;
    }
    public string DrawChart(DataTable dt,string str_DisplayColumn,string str_ValueColumn)
    {
        
        string str_ChartTemplate = this.GetChartTemplate("ChartPie.txt", this.str_DivName);

        string str_ChartContent = "['%DisplayColumn%', '%ValueColumn%'],";
        str_ChartContent = str_ChartContent.Replace("%DisplayColumn%", str_DisplayColumn);
        str_ChartContent = str_ChartContent.Replace("%ValueColumn%", str_ValueColumn);

        foreach (DataRow dr in dt.Rows)
        {
            str_ChartContent += "['" + dr[str_DisplayColumn].ToString() + "'," + dr[str_ValueColumn].ToString() + "],";
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);

        str_ChartTemplate = str_ChartTemplate.Replace("%%ChartJsonValues%%", str_ChartContent);

        str_ChartTemplate = str_ChartTemplate.Replace("%%pieSliceText%%", this.pieSliceText.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%is3d%%", bln_Is3d.ToString().ToLower());

        return str_ChartTemplate;
    }

    public string DrawPieChartWithLink(DataTable dt, string str_DisplayColumn, string str_ValueColumn)
    {
        //string str_ChartTemplate = PHT.ReadFileToString("ChartPieWithLink.txt");
        string str_ChartTemplate = this.GetChartTemplate("ChartPieWithLink.txt", this.str_DivName);
        string str_ChartContent = "['%DisplayColumn%', '%ValueColumn%'],";
        str_ChartContent = str_ChartContent.Replace("%DisplayColumn%", str_DisplayColumn);
        str_ChartContent = str_ChartContent.Replace("%ValueColumn%", str_ValueColumn);

        foreach (DataRow dr in dt.Rows)
        {
            str_ChartContent += "['" + dr[str_DisplayColumn].ToString() + "'," + dr[str_ValueColumn].ToString() + "],";
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);

        str_ChartTemplate = str_ChartTemplate.Replace("%%ChartJsonValues%%", str_ChartContent);
        //str_ChartTemplate = str_ChartTemplate.Replace("%%ChartDivName%%", str_DivName);

        //if (this.ChartHeight > 0) str_ChartTemplate = str_ChartTemplate.Replace("%%Height%%", this.ChartHeight.ToString());
        //else str_ChartTemplate = str_ChartTemplate.Replace("%%Height%%", "320");
        str_ChartTemplate = str_ChartTemplate.Replace("%%pieSliceText%%", this.pieSliceText.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%is3d%%", bln_Is3d.ToString().ToLower());
        str_ChartTemplate = str_ChartTemplate.Replace("%%RedirectUrl%%", str_RedirectUrl.ToString());

        return str_ChartTemplate;
    }

}