﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for GridDisplay
/// </summary>
public class GridDisplay
{
    DataAccess DA;
    string str_Sql = "";
    SqlCommand SC;

    public GridDisplay(string str_Object)
    {
        this.DA = new DataAccess();
        GridObject = str_Object;


        switch (GridObject.ToLower())
        {
            case "newjoinees":
                GridSQL = "Select * from employee where year(joiningdate)=2017 and year(joiningdate)=2017";
                break;
        }

        }

    public string GridObject { get; set; }
    public string GridSQL { get; set; }

    protected string UrlParameter(string str_ParameterName)
    {
        HttpRequest Request = HttpContext.Current.Request;
        string str_Value = "";
        if (Request.QueryString[str_ParameterName] != null)
            str_Value = Request.QueryString[str_ParameterName];

        return HttpUtility.UrlDecode(str_Value);
    }
}