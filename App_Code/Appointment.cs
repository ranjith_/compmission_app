﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

/// <summary>
/// Summary description for Appointment
/// </summary>
public class Appointment
{
    DataAccess DA = null;
    string str_AppointmentKey = "";
    PhTemplate PHT;
    SessionCustom SC;
    public Appointment(DataAccess DA, string str_AppointmentKey)
    {
        this.DA = DA;
        this.SC = new SessionCustom();
        this.str_AppointmentKey = str_AppointmentKey;
        this.PHT = new PhTemplate();
    }

    public void LoadDoctorAppointmentWidget(PlaceHolder ph)
    {
        string str_Sql = "select * from v_appointment where AppointmentKey=@AppointmentKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AppointmentKey", str_AppointmentKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        ds.Tables[0].TableName = "appointment";
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentDetail.txt"), str_HTML = "";

        if(SC.IsOperationAdmin())
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%displaystyle%%", "block");
        else
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%displaystyle%%", "none");
        

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_WidgetTemplate);
        }

        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    public void LoadDoctorCurrentStatus(PlaceHolder ph, bool bln_Access)
    {
        string str_Sql = "select * from v_appointment where AppointmentKey=@AppointmentKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AppointmentKey", str_AppointmentKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        ds.Tables[0].TableName = "status";
        string str_DoctorKey = ds.Tables[0].Rows[0]["DoctorKey"].ToString();
        string str_RequesterKey = ds.Tables[0].Rows[0]["RequestedBy"].ToString();
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentStatus.txt"), str_HTML = "";
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_WidgetTemplate);
        }
        string str_StatusKey = ds.Tables[0].Rows[0]["StatusKey"].ToString();

        if (str_StatusKey.ToUpper() == "5697A00F-C0FB-457B-9193-721CD21FB1AB")
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayDoctor%%", "none");
            str_HTML = str_HTML.Replace("%%ControlDisplay%%", "block");
        }
        if (!bln_Access)
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayDoctor%%", "none");
            str_HTML = str_HTML.Replace("%%ControlDisplay%%", "block");
        }
        else if (bln_Access)
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayDoctor%%", "block");
            str_HTML = str_HTML.Replace("%%ControlDisplay%%", "none");
        }
        ph.Controls.Add(new LiteralControl(str_HTML));
    }

    public void LoadDoctorAppointmentCommentsWidget(PlaceHolder ph)
    {
        string str_Sql = "select r.RequesterName,b.Prescription,b.JoinedDate,b.EndDate,datediff(minute,b.JoinedDate,b.EndDate) TotalMiniutes,c.FollowUpDate from RoomArchive a "
                            + " join RoomParticipantsArchive b on a.RoomKey = b.RoomKey"
                            + " join v_Appointment c on a.AppointmentKey = c.AppointmentKey"
                            + " join v_Requester r on r.RequesterKey = b.SubscriberKey"
                            + " where a.AppointmentKey = @AppointmentKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AppointmentKey", str_AppointmentKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentComments.txt"), str_HTML = "";
        string str_ChildTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentCommentsTr.txt");
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ChildTemplate);
        }
        //str_WidgetTemplate = str_WidgetTemplate.Replace("%%DoctorKey%%", this.str_AppointmentKey);
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
    }

    public void LoadDoctorAppointmentLog(PlaceHolder ph)
    {
        string str_Sql = "select a.StatusKey,StatusName,r.RequesterName ChangedBy,a.CreatedOn ChangedOn  from AppointmentStatus a "
                            + " join AppStatus b on a.StatusKey = b.StatusKey"
                            + " join v_Requester r on a.CreatedBy = r.RequesterKey"
                            + " where a.AppointmentKey = @AppointmentKey order by a.CreatedOn desc";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AppointmentKey", str_AppointmentKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentLog.txt"), str_HTML = "";
        string str_ChildTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentLogTr.txt");
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ChildTemplate);
        }
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
    }

    public void LoadAppointmentCallJoinWidget(PlaceHolder ph, string str_AppointmentKey)
    {
        string str_WidgetTemplate = "", str_ChildTemplate = "", str_HTML = "", str_Sql = "";
        str_WidgetTemplate = this.PHT.ReadFileToString("WidgetAppointmentCallJoin.txt");
        str_ChildTemplate = this.PHT.ReadFileToString("WidgetAppointmentCallJoinTr.txt");
        str_Sql = "select * from v_Appointment where AppointmentKey=@AppointmentKey and StatusKey <>'5697A00F-C0FB-457B-9193-721CD21FB1AB'";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AppointmentKey", this.str_AppointmentKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ChildTemplate);
        }
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);

        if (ds.Tables[0].Rows.Count > 0)//More Appointments Link 
        {
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%controldisplay%%", "block");
        }
        else
        {
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%controldisplay%%", "none");
        }
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
    }

    //Lab Order
    public void LoadLabOrder(PlaceHolder ph)
    {
        string str_Sql = "select a.LabKey,a.LabName,b.OrderName,b.AppointmentKey from Lab a left outer join LabOrder b on a.LabKey = b.LabKey where AppointmentKey=@AppointmentKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AppointmentKey", str_AppointmentKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetLabOrder.txt"), str_HTML = "";
        string str_ChildTemplate = this.PHT.ReadFileToString("WidgetLabOrderTr.txt");
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ChildTemplate);
            }
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%control%%", "none");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);
        }
        else
        {
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", "");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%header%%", "none");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%control%%", "block");
        }
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
    }

    //Patient Medication
    public void LoadPatientMedication(PlaceHolder ph)
    {
        string str_Sql = "select * from v_PatientMedication where AppointmentKey=@AppointmentKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AppointmentKey", str_AppointmentKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetAppointmentPatientMedication.txt"), str_HTML = "";
        string str_ChildTemplate = this.PHT.ReadFileToString("WidgetAppointmentPatientMedicationTr.txt");
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ChildTemplate);
            }
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%control%%", "none");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);
        }
        else
        {
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", "");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%header%%", "none");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%control%%", "block");
        }
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
    }

    //Patient Assessment
    public void LoadPatientAssessment(PlaceHolder ph)
    {
        string str_Sql = "select * from PatientAssessment where AppointmentKey=@AppointmentKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@AppointmentKey", str_AppointmentKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetAppointmentPatientAssessment.txt"), str_HTML = "";
        string str_ChildTemplate = this.PHT.ReadFileToString("WidgetAppointmentPatientAssessmentTr.txt");
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ChildTemplate);
            }
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%control%%", "none");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);
        }
        else
        {
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", "");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%header%%", "none");
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%control%%", "block");
        }
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
    }
}