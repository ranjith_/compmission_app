﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.IO;
using System.Web;

/// <summary>
/// Summary description for FileFunction
/// </summary>
public class FileFunction
{
    string str_TemplateFilePath = "";
    string str_DictionaryFilePath = "";

	public FileFunction()
	{
        //this.str_TemplateFilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + @"BootTemplate\";
        //this.str_DictionaryFilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + @"Dictionary\";

        this.str_TemplateFilePath = System.AppDomain.CurrentDomain.BaseDirectory + @"BootTemplate\";
        this.str_DictionaryFilePath = System.AppDomain.CurrentDomain.BaseDirectory + @"Dictionary\";

	}

    public string ReadFileToString(string str_TemplateFileName)
    {
        string str_Filepath = this.str_TemplateFilePath + str_TemplateFileName;
        StreamReader streamReader = new StreamReader(str_Filepath);
        string str_Div = streamReader.ReadToEnd();
        streamReader.Close();

        return str_Div;
    }

    public string ReadFileFormDictionary(string str_form_id)
    {
        string str_Filepath = this.str_DictionaryFilePath + "file_" + str_form_id + ".xml";
        StreamReader streamReader = new StreamReader(str_Filepath);
        string str_Div = streamReader.ReadToEnd();
        streamReader.Close();

        return str_Div;
    }

    public void WriteFileFormDictionary(string str_form_id, string str_XmlContent)
    {
 
        string str_TargetFolder = HttpContext.Current.Server.MapPath("~/Dictionary/");
        if (!System.IO.Directory.Exists(str_TargetFolder)) System.IO.Directory.CreateDirectory(str_TargetFolder);

        string str_DictFileName = str_TargetFolder + "file_" + str_form_id + ".xml";

        XmlDocument XDoc = new XmlDocument();
        XDoc.LoadXml(str_XmlContent);

        System.IO.File.Delete(str_DictFileName);
        XDoc.Save(str_DictFileName);
    }

    public string ReadFileFormDictionarySchema(string str_form_id)
    {
        string str_Filepath = this.str_DictionaryFilePath + "schema_" + str_form_id + ".xml";
        StreamReader streamReader = new StreamReader(str_Filepath);
        string str_Div = streamReader.ReadToEnd();
        streamReader.Close();

        return str_Div;
    }

    public void WriteFileFormDictionarySchema(string str_form_id, string str_XmlContent)
    {

        string str_TargetFolder = HttpContext.Current.Server.MapPath("~/Dictionary/");
        if (!System.IO.Directory.Exists(str_TargetFolder)) System.IO.Directory.CreateDirectory(str_TargetFolder);

        string str_DictFileName = str_TargetFolder + "schema_" + str_form_id + ".xml";

        XmlDocument XDoc = new XmlDocument();
        XDoc.LoadXml(str_XmlContent);

        System.IO.File.Delete(str_DictFileName);
        XDoc.Save(str_DictFileName);
    }

}