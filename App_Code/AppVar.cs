﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for AppVar
/// </summary>
public class AppVar
{
	public AppVar()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    //public string DatabaseConnectionString { get { return @"server=172.17.3.203;uid=sa;pwd=ihskareo;database=Signaling;Pooling=True;"; } }
    //public string DatabaseConnectionString { get { return @"server=159.8.72.109;uid=sa;pwd=ihskareo;database=Signaling;Pooling=True;"; } }
    // public string DatabaseConnectionString { get { return @"server=43.255.152.26;uid=telehospital;pwd=Zq&r24e4;database=raka_TeleHospital;Pooling=True;"; } }

    public string DatabaseConnectionString { get { return @"server=DESKTOP-BPPP7AQ\SQLEXPRESS;uid=DESKTOP-BPPP7AQ\123;database=CM;Trusted_Connection = yes;Pooling=True;"; } }
    //public string DatabaseConnectionString { get { return @"server=cureselect.database.windows.net;uid=cureselectdb;pwd=CureSelect_2018;database=CureSelect;Pooling=True;"; } }

    public string Culture { get { return HttpContext.Current.Application["culture"].ToString(); } }

    public string WebRoot { get { return HttpContext.Current.Request.Url.OriginalString.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/"); } }

    //public string FileRoot { get { return @"\\223.100.103.109\mssqlserver\EmployeeDocumentStream\EmployeeFiles\"; } }
    public string FileRoot { get { return @"\\172.17.3.203\mssqlserver\EmployeeDocumentStream\EmployeeFiles\"; } }

    public string RequestStatusListQuery { get { return HttpContext.Current.Application["requeststatuslistquery"].ToString(); } }

    public DateTime getCurrentPstDate { get { return DateTime.UtcNow.AddHours(-8); } }

    public string GuestUserName { get { return HttpContext.Current.Application["guest_username"].ToString(); } }

    public string GuestUserPassword { get { return HttpContext.Current.Application["guest_userpassword"].ToString(); } }

    public string AdminRoleKey { get { return HttpContext.Current.Application["adminrolekey"].ToString(); } }

    public DataSet OTD { get { return (DataSet) HttpContext.Current.Application["OTD"]; } }

    public DataTable LoadAzureCredential(DataAccess DA)
    {
        if (this.AzureCredential != null) return this.AzureCredential;
        string str_Masters = " select * from AzureCredential; ";
        System.Data.DataTable dt = DA.GetDataTable(str_Masters);
        this.AzureCredential = dt;
        return dt;
    }

    public DataTable AzureCredential { get { return (DataTable)HttpContext.Current.Application["AzureCredential"]; } set { HttpContext.Current.Application["AzureCredential"] = value; } }

    public int TimeZoneHours
    {
        get
        {
            if (HttpContext.Current.Application["timezone_hours"].ToString() != "")
                return Convert.ToInt32(HttpContext.Current.Application["timezone_hours"]);
            else
                return 0;
        }
    }

    public int TimeZoneMinutes
    {
        get
        {
            if (HttpContext.Current.Application["timezone_minutes"].ToString() != "")
                return Convert.ToInt32(HttpContext.Current.Application["timezone_minutes"]);
            else
                return 0;
        }
    }

 
}
