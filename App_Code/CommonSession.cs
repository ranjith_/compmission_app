﻿using System;
using System.Collections.Generic;

using System.Web;

/// <summary>
/// Summary description for CommonSession
/// </summary>
public class CommonSession
{
	public CommonSession()
	{
        if (!SessionExists())
        {
            HttpContext.Current.Response.Redirect("~/Signon.aspx");
        }
	
	}

    public bool SessionExists()
    {
        if (HttpContext.Current.Session["user_id"] != null)
            return true;
        else
            return false;
    }

    public string SV_Imei()
    {
        return this.Pull("IMEI").ToString ();
    }

    public string SV_TimeDifference()
    {
        return this.Pull("timediff").ToString();
    }

    public string SV_MsSqlMasterConectionString()
    {
        return HttpContext.Current.Application["dbconnect"].ToString();
    }

    public string SV_MsSqlConectionString()
    {
        return SV_MsSqlMasterConectionString();
    }

    public string SV_SqliteConnectionString()
    {
        //string str_DBpath = @"C:\MobiKlon\MobiKlonDb\" + this.Pull("IMEI").ToString() + ".db";
        //string str_DBConStr = "Data Source=" + str_DBpath + ";Version=3;";
        return HttpContext.Current.Application["DbConnectionString"].ToString().Replace("%IMEI%", this.Pull("IMEI").ToString());
    }

    /// <summary>
    /// gets the value of the Session Variable. 
    /// If session variable does not exits, it will return null 
    /// </summary>
    public object Pull(string str_variable)
    {
        object obj_SessionVariableValue = null;
        if (Contains(str_variable))
        {
            obj_SessionVariableValue = HttpContext.Current.Session[str_variable];
        }
        return obj_SessionVariableValue;
    }

    /// <summary>
    /// Adds the value to the Session Variable. 
    /// If session variable does not exits, it will create a new variable and stores the value 
    /// </summary>
    public void Push(string str_variable, object obj_variable_value)
    {
        HttpContext.Current.Session[str_variable] = obj_variable_value;
    }

    /// <summary>
    /// checks the existence of session variable 
    /// </summary>
    /// <returns>true or false</returns>
    public bool Contains(string str_variable)
    {
        bool bln_SessionExists = false;
        if (Contains())
        {
            if (HttpContext.Current.Session[str_variable] != null) bln_SessionExists = true;
        }
        return bln_SessionExists;
    }

    /// <summary>
    /// checks the existence of session
    /// </summary>
    /// <returns>true or false</returns>
    public bool Contains()
    {
        bool bln_SessionExists = false;
        if (HttpContext.Current.Session != null) bln_SessionExists = true;
        return bln_SessionExists;
    }


}