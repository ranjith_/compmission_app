﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

/// <summary>
/// Summary description for Cjson
/// </summary>
public class Cjson
{
	public Cjson()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string ConvertDataTabletoJsonString(DataRow[] dra)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;
        foreach (DataRow dr in dra)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dr.Table.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
        return serializer.Serialize(rows);

    }

    public string ConvertDataTabletoJsonString(DataTable dt)
    {
        return this.ConvertDataTabletoJsonString(dt.Select());
    }
}