using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for HDDatabase
/// </summary>
public class HDDatabase
{
    private string strConString = "";
    private string strErrMessage = "";
    public string connectionstring;

    public HDDatabase()
    {
        this.strConString = HttpContext.Current.Application["dbconnect"].ToString();
    }

    public HDDatabase(string strConString)
    {
        this.strConString = strConString;
    }

    private SqlConnection GetConnection()
    {
        SqlConnection conObj = new SqlConnection(strConString);
        return conObj;
    }
    public int GetRecordCount(String strSql)
    {
        string strOut = GetColumnValue(strSql);
        if (strOut.Trim() != "")
            return Convert.ToInt32 (strOut);
        else
            return 0;
    }

    public string GetColumnValue(string strTableName, string strColName, string strWhere)
    {
        string strValue = "";
        string strSql = "select " + strColName + " from " + strTableName;
        if (strWhere != "")
        {
            strSql += " where " + strWhere;
        }
        strValue = GetColumnValue(strSql);
        return strValue;
    }

    public string GetColumnValue(string strSql)
    {
        string strValue = "";
        DataTable dtaObj = GetDataTable(strSql);
        if (dtaObj.Rows.Count > 0)
        {
            strValue = dtaObj.Rows[0][0].ToString();
        }
        return strValue;
    }

    public DataRow GetTableRow(string strTableName, string strColName, string strWhere)
    {
        string strSql = "select " + strColName + " from " + strTableName;
        if (strWhere != "")
        {
            strSql += " where " + strWhere;
        }
        return GetTableRow(strSql);
    }

    public DataRow GetTableRow(string strSql)
    {
        DataRow droObj = null;
        DataTable dtaObj = GetDataTable(strSql);
        if (dtaObj.Rows.Count > 0)
        {
            droObj = dtaObj.Rows[0];
        }
        return droObj;
    }

    public DataTable GetTable(string strTableName, string strColName, string strWhere)
    {
        string strSql = "select " + strColName + " from " + strTableName;
        if (strWhere != "")
        {
            strSql += " where " + strWhere;
        }
        return GetDataTable(strSql);
    }


    public DataTable GetDataTable(string strSql)
    {
        return GetDataTable(strSql, "");
    }


    public DataTable GetDataTable(string strSql, string strTargetTableName)
    {
        DataSet ds = new DataSet();
        ds = GetDataSet(strSql);
        DataTable dt = new DataTable();
        
        if (ds.Tables.Count  > 0)
            dt =  ds.Tables[0];
        
        if (strTargetTableName!="")
            dt.TableName = strTargetTableName;

        return dt;
        //DataTable dseObj = new DataTable(strTargetTableName);
        //try
        //{
        //    //LogWriter.WriteLog(strSql);
        //    SqlCommand cmdObj = new SqlCommand(strSql, GetConnection());
        //    SqlDataAdapter sdaObj = new SqlDataAdapter(cmdObj);
        //    sdaObj.Fill(dseObj);
        //}
        //catch (Exception ex)
        //{
        //    strErrMessage = ex.Message;
        //    //LogWriter.WriteLog(strErrMessage);
        //}
        //return dseObj;

    }

    public DataSet GetDataSet(string strSql)
    {
        DataSet dseObj = new DataSet();
        try
        {
            //LogWriter.WriteLog(strSql);
            SqlCommand cmdObj = new SqlCommand(strSql, GetConnection());
            SqlDataAdapter sdaObj = new SqlDataAdapter(cmdObj);
            sdaObj.Fill(dseObj);
        }
        catch (Exception ex)
        {
            strErrMessage = ex.Message;
            throw ex;
            //LogWriter.WriteLog(strErrMessage);
        }
        return dseObj;

    }

    public ArrayList ExecuteCmdObj(ArrayList arlCmdObj)
    {
        ArrayList al_rec_affected = new ArrayList();
        bool blnStatus = true;
        string strSql = "";
        SqlTransaction sqlTrans = null;
        int in_current_query = 0;
        try
        {
            SqlConnection conObj = GetConnection();
            conObj.Open();
            //LogWriter.WriteLog("Transaction Started");
            sqlTrans = conObj.BeginTransaction();
            int intRecsAffect = -1;
            for (int i = 0; i < arlCmdObj.Count; i++)
            {
                SqlCommand cmdObj = (SqlCommand)arlCmdObj[i];
                strSql = cmdObj.CommandText;
                in_current_query = i;
                //LogWriter.WriteLog("Command going to execute: " + strSql);
                WriteParameters(cmdObj);
                cmdObj.Connection = conObj;
                cmdObj.Transaction = sqlTrans;
                intRecsAffect = cmdObj.ExecuteNonQuery();
                al_rec_affected.Add(intRecsAffect);
                //LogWriter.WriteLog("Record affected : " + intRecsAffect.ToString());
            }
            sqlTrans.Commit();
            //LogWriter.WriteLog("Transaction Commit. Success");
            conObj.Close();
        }
        catch (Exception ex)
        {
            blnStatus = false;
            strErrMessage = ex.Message;
            //LogWriter.WriteLog("Error : " + strErrMessage);
            try
            {
                sqlTrans.Rollback();
                //LogWriter.WriteLog("Transaction Rollbacked");
            }
            catch (Exception ex2)
            {
                //LogWriter.WriteLog("Error in Rollback. " + ex2.Message);
            }

            throw ex;
        }
        return al_rec_affected;
    }

    public int ExecuteCmdObj(SqlCommand cmdObj)
    {
        ArrayList al_cmdobj = new ArrayList();
        al_cmdobj.Add(cmdObj);
        ArrayList al_affected_records = ExecuteCmdObj(al_cmdobj);
        if (al_affected_records.Count > 0)
            return Convert.ToInt32(al_affected_records[0]);
        else
            return 0;
    }

    private bool WriteParameters(SqlCommand cmdObj)
    {
        bool blnStatus = true;
        for (int i = 0; i < cmdObj.Parameters.Count; i++)
        {
            //LogWriter.WriteLog("Param Name : " + cmdObj.Parameters[i].ParameterName.ToString() + "; Value : " + cmdObj.Parameters[i].Value.ToString());
        }
        return blnStatus;
    }
    public string ErrorMessage
    {
        get
        {
            return this.strErrMessage;
        }
        set
        {
            this.strErrMessage = value;
        }
    }

    public string get_user_message()
    {
        string user_message = "";
         
        string str_sql = "Select * from User_Message where charindex(message_name,'" + this.strErrMessage.Replace("'","''")  + "')<>0";
        foreach (DataRow dr in GetDataTable(str_sql).Select())
        {
            user_message = dr["user_message"].ToString(); 
        }
        if (user_message != "")
            return user_message;
        else return this.strErrMessage; 
    }

}
