﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types

/// <summary>
/// Summary description for AzureContainer
/// </summary>
public class AzureContainer
{
    string str_AzureConnn = "";

   
    public AzureContainer(DataTable dt_AzureCredential)
    {
        if (dt_AzureCredential == null || dt_AzureCredential.Rows.Count == 0)
        {
            throw new Exception("Azure Credential not found");
        }

        this.AzureBlobClientConnString = dt_AzureCredential.Rows[0]["ConString1"].ToString();

        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(this.AzureBlobClientConnString);
        this.AzureBlobClient = storageAccount.CreateCloudBlobClient();
        this.AzureBlobClientContainer = AzureBlobClient.GetContainerReference(dt_AzureCredential.Rows[0]["ContainerName"].ToString());

    }


    public CloudBlobClient AzureBlobClient { get; set; }
    public CloudBlobContainer AzureBlobClientContainer { get; set; }
    public string AzureBlobClientConnString { get; set; }

    public DataTable AzureContainerList()
    {
        //loop all container
        DataTable dt_Container = new DataTable();
        dt_Container.Columns.Add("ContainerName");
        dt_Container.Columns.Add("LeaseState");
        dt_Container.Columns.Add("LastModified");
        foreach (CloudBlobContainer clientcontainer in AzureBlobClient.ListContainers())
        {
            DataRow dr = dt_Container.NewRow();
            dr["ContainerName"] = clientcontainer.Name;
            dr["LeaseState"] = clientcontainer.Properties.LeaseState.ToString();
            dr["LastModified"] = clientcontainer.Properties.LastModified.ToString();
            dt_Container.Rows.Add(dr);
        }

        return dt_Container;
    }

    public CloudBlobContainer AzureCreateContainer(string str_ContainerName)
    {
        //the container for this is companystyles
        CloudBlobContainer container = AzureBlobClient.GetContainerReference(str_ContainerName);
        //Create a new container, if it does not exist
        container.CreateIfNotExists();
        return container;
    }

    public void AzureUploadFile(CloudBlobContainer container, string str_Folder, HttpPostedFile postedfile)
    {
        string str_BlobName = postedfile.FileName;
        if (str_Folder != "") str_BlobName = str_Folder + @"\" + postedfile.FileName;

        CloudBlockBlob cloudBlockBlob = container.GetBlockBlobReference(str_BlobName);

        using (postedfile.InputStream)
        {
            cloudBlockBlob.UploadFromStream(postedfile.InputStream);
        }
    }

    public void AzureDownloadFile(string str_CLoudFileName, string str_LocalFileName)
    {
        CloudBlockBlob blockBlob = this.AzureBlobClientContainer.GetBlockBlobReference(str_CLoudFileName);
        blockBlob.DownloadToFileAsync(str_LocalFileName, System.IO.FileMode.Create);
    }

    public bool AzureFileExists(string str_CLoudFileName)
    {
        bool bln_Value = false;
        try
        {
            CloudBlockBlob blockBlob = this.AzureBlobClientContainer.GetBlockBlobReference(str_CLoudFileName);
            blockBlob.FetchAttributes();
            bln_Value = true;
        }
        catch (Exception ex)
        {
        }
        return bln_Value;
    }

    public CloudBlockBlob AzureFile(string str_CLoudFileName)
    {
        CloudBlockBlob blockBlob = this.AzureBlobClientContainer.GetBlockBlobReference(str_CLoudFileName);
        return blockBlob;
       
    }

    public string GetAccessUri(CloudBlobContainer container, string blobName, string policyName = null)
    {
        string sasBlobToken;
        // Get a reference to a blob within the container.
        // Note that the blob may not exist yet, but a SAS can still be created for it.
        CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
        string Downloadfilename = blobName.Substring(blobName.IndexOf("_") + 1);

        if (policyName == null)
        {
            // Create a new access policy and define its constraints.
            // Note that the SharedAccessBlobPolicy class is used both to define the parameters of an ad-hoc SAS, and
            // to construct a shared access policy that is saved to the container's shared access policies.
            SharedAccessBlobPolicy adHocSAS = new SharedAccessBlobPolicy()
            {
                // When the start time for the SAS is omitted, the start time is assumed to be the time when the storage service receives the request.
                // Omitting the start time for a SAS that is effective immediately helps to avoid clock skew.
                SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(60),
                Permissions = SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List
            };

            //SharedAccessBlobHeaders SASHeader = new SharedAccessBlobHeaders()
            //{
            //    ContentType = "application/pdf",
            //    ContentDisposition = "inline; filename=" + Downloadfilename
                
            //};
           
            // Generate the shared access signature on the blob, setting the constraints directly on the signature.
           //sasBlobToken = blob.GetSharedAccessSignature(adHocSAS, SASHeader);
            sasBlobToken = blob.GetSharedAccessSignature(adHocSAS);

            //Console.WriteLine("SAS for blob (ad hoc): {0}", sasBlobToken);
            // Console.WriteLine();
        }
        else
        {
            // Generate the shared access signature on the blob. In this case, all of the constraints for the
            // shared access signature are specified on the container's stored access policy.
            sasBlobToken = blob.GetSharedAccessSignature(null, policyName);

            //   Console.WriteLine("SAS for blob (stored access policy): {0}", sasBlobToken);
            //  Console.WriteLine();
        }

        // Return the URI string for the container, including the SAS token.
        return blob.Uri + sasBlobToken;
    }


    public string GetDownloadAccessUri(CloudBlobContainer container, string blobName, string policyName = null)
    {
        string sasBlobToken;
        string Downloadfilename = blobName.Substring(blobName.IndexOf("_") + 1); 

        // Get a reference to a blob within the container.
        // Note that the blob may not exist yet, but a SAS can still be created for it.
        CloudBlockBlob blob = container.GetBlockBlobReference(blobName);

        if (policyName == null)
        {
            SharedAccessBlobPolicy SASPolicy = new SharedAccessBlobPolicy()
            {
                Permissions = SharedAccessBlobPermissions.Read,
                SharedAccessStartTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(-5)),
                SharedAccessExpiryTime = new DateTimeOffset(DateTime.UtcNow.AddMinutes(2))
            };

            SharedAccessBlobHeaders SASHeader = new SharedAccessBlobHeaders()
            {
                ContentDisposition = "attachment; filename=" + Downloadfilename,
                //ContentDisposition = "inline; filename=" + Downloadfilename,
                ContentType = "application/pdf"
            };
            sasBlobToken = blob.GetSharedAccessSignature(SASPolicy, SASHeader);
        }
        else
        {
            // Generate the shared access signature on the blob. In this case, all of the constraints for the
            // shared access signature are specified on the container's stored access policy.
            sasBlobToken = blob.GetSharedAccessSignature(null, policyName);

            //   Console.WriteLine("SAS for blob (stored access policy): {0}", sasBlobToken);
            //  Console.WriteLine();
        }

        

        // Return the URI string for the container, including the SAS token.
        return blob.Uri + sasBlobToken; 
    }


}