using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;


/// <summary>
/// Summary description for PageCommonFunction
/// </summary>
public class FormPage
{
    DataAccess DA;
    DataSet ds_form_tables;
    string str_form_id = "";
    string str_page_id = "";
    string str_master_page = "";

    string str_message = "";

    public FormPage(string str_form_id, string str_page_id)
	{
        this.DA = new DataAccess();
        this.str_form_id = str_form_id;
        this.str_page_id = str_page_id;

        this.ds_form_tables = new DataSet();
        this.load_form_tables();
    }

    public bool IncludeInActivePageAttriutes { get; set; }

    protected DataSet load_form_tables()
    {
    
        FileFunction FF = new FileFunction();
        Cxml CX = new Cxml();
        this.ds_form_tables = CX.XmlStringToDataSet(FF.ReadFileFormDictionary(this.str_form_id), FF.ReadFileFormDictionarySchema(this.str_form_id));
        return this.ds_form_tables;

    }

    public DataSet get_form_tables()
    {
        return this.ds_form_tables;
    }

    public string get_page_dbtable()
    {
        string str_table_name = "";
        foreach (DataRow dr_page in ds_form_tables.Tables["Page"].Select("page_id='" + this.str_page_id + "'"))
        {
            str_table_name = dr_page["db_table"].ToString(); 
        }
        return str_table_name;
    }

    public DataRow [] get_page_attributes()
    {
        return ds_form_tables.Tables["Page_Attribute"].Select("page_id='" + this.str_page_id + "'");
    }

    public DataRow[] get_page()
    {
        return ds_form_tables.Tables["Page"].Select("page_id='" + this.str_page_id + "'");
    }

    public string get_page_name()
    {
        string str_table_name = "";
        foreach (DataRow dr_page in ds_form_tables.Tables["Page"].Select("page_id='" + this.str_page_id + "'"))
        {
            str_table_name = dr_page["page_name"].ToString(); 
        }
        return str_table_name;

    }

    public string get_message()
    {
        return str_message;
    }

    public DataTable get_page_table_data(string str_unikey)
    {
        string str_sql = "Select * from " + this.get_page_dbtable() + " where unikey='" + str_unikey + "'";
        DataTable dt = DA.GetDataTable(str_sql);
        return dt;
    }

    public string get_master_page()
    {
        foreach (DataRow dr_master in this.ds_form_tables.Tables["Page"].Select("is_master=1"))
        {
            this.str_master_page = dr_master["page_id"].ToString();
        }
        return this.str_master_page;


    }

    public ArrayList get_master_keys()
    {
        // determine master page keys
        DataRow[] dra_page_key = ds_form_tables.Tables["Page_Attribute"].Select("page_id='" + get_master_page() + "' and is_key=1", "attribute_seq");
        ArrayList al_keys = new ArrayList();

        foreach (DataRow dr_page_key in dra_page_key)
        {
            al_keys.Add(dr_page_key["db_column"].ToString());
        }

        return al_keys;
    }

    
}
