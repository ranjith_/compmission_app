﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GChart
/// </summary>
/// 
public class GChartBar : GChart
{
    PhTemplate PHT = new PhTemplate();

    string str_DivName = "";
    int int_Barheight = 50;
    int int_PositionLeft = 100;
    int int_PositionTop = 20;
    string str_Title = "", str_XTitle = "", str_YTitle = "";
    bool bln_IsStacked = false;
    //static string[] ColourValues = new string[] {
    //            "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    //    "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    //    "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    //    "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    //    "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    //    "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    //    "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    //    "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    //};
    static string[] ColourValues = new string[] { "Bisque", "SteelBlue", "Aqua", "Aquamarine", "Azure", "Beige",  "Black", "BlanchedAlmond", "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse", "Chocolate", "Coral", "CornflowerBlue",
        "Cornsilk", "Crimson", "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki",
        "DarkMagenta", "DarkOliveGreen", "Darkorange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue",
        "DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", "DimGray", "DimGrey",
        "DodgerBlue", "FireBrick", "FloralWhite", "ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite", "Gold", "GoldenRod",
        "Gray", "Grey", "Green", "GreenYellow", "HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender",
        "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan", "LightGoldenRodYellow", "LightGray",
        "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSlateGrey",
        "LightSteelBlue", "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue",
        "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed",
        "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", "OldLace", "Olive", "OliveDrab", "Orange", "OrangeRed",
        "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum",
        "PowderBlue", "Purple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna",
        "Silver", "SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato",
        "Turquoise", "Violet", "Wheat", "White", "WhiteSmoke", "Yellow", "YellowGreen" };
    public GChartBar(string str_DivName)
    {
        this.str_DivName = str_DivName;
        this.BarType = BarsType.vertical;
    }
    public enum BarsType
    {
        vertical,
        horizontal
    };

    public bool IsStacked { get { return this.bln_IsStacked; } set { this.bln_IsStacked = value; } }

    public int Barheight { get { return this.int_Barheight; } set { this.int_Barheight = value; } }

    public int PositionLeft { get { return this.int_PositionLeft; } set { this.int_PositionLeft = value; } }

    public int PositionTop { get { return this.int_PositionTop; } set { this.int_PositionTop = value; } }

    public string Title { get { return this.str_Title; } set { this.str_Title = value; } }

    public string XTitle { get { return this.str_XTitle; } set { this.str_XTitle = value; } }

    public string YTitle { get { return this.str_YTitle; } set { this.str_YTitle = value; } }

    public BarsType BarType { set; get; }

    public string DrawMultipleBar(DataTable dt, string[] str_DisplayColumn, string[] str_ValueColumn)
    {
        string str_ChartTemplate = this.GetChartTemplate("ChartBar.txt", this.str_DivName);

        string str_ChartContent = "", str_ValueContent = "";

        int displayCount = str_DisplayColumn.Length;
        for (int i = 0; i < displayCount; i++)
        {
            str_ChartContent += "'" + str_DisplayColumn.GetValue(i).ToString() + "',";
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);
        str_ChartContent = "[" + str_ChartContent + "],";

        foreach (DataRow dr in dt.Rows)
        {
            str_ValueContent = "";
            for (int i = 0; i < displayCount; i++)
            {
                if (i == 0)
                    str_ValueContent += "'" + dr[str_ValueColumn.GetValue(i).ToString()].ToString() + "',";
                else
                    str_ValueContent += "" + dr[str_ValueColumn.GetValue(i).ToString()].ToString() + ",";
            }
            str_ValueContent = str_ValueContent.Substring(0, str_ValueContent.Length - 1);
            str_ValueContent = "[" + str_ValueContent + "],";
            str_ChartContent += str_ValueContent;
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);

        str_ChartTemplate = str_ChartTemplate.Replace("%%ChartJsonValues%%", str_ChartContent);
        str_ChartTemplate = str_ChartTemplate.Replace("%%Title%%", str_Title.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%barType%%", BarType.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%isStacked%%", IsStacked.ToString().ToLower());

        return str_ChartTemplate;
    }

    public string DrawSingleBarChart(DataTable dt, string str_DisplayColumn, string str_ValueColumn)
    {
        string str_ChartTemplate = "";
        //if (this.BarType == BarsType.horizontal)
        //    str_ChartTemplate = this.GetChartTemplate("ChartHorizontalBar.txt", this.str_DivName);
        //else
            str_ChartTemplate = this.GetChartTemplate("ChartSingleBar.txt", this.str_DivName);

        string str_ChartContent = "['%DisplayColumn%', '%ValueColumn%',{ role: 'annotation' },{ role: 'style' }],";
        str_ChartContent = str_ChartContent.Replace("%DisplayColumn%", str_DisplayColumn);
        str_ChartContent = str_ChartContent.Replace("%ValueColumn%", str_ValueColumn);
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            str_ChartContent += "['" + dr[str_DisplayColumn].ToString() + "'," + dr[str_ValueColumn].ToString() + ",'" + dr[str_ValueColumn].ToString() + "','" + ColourValues[i] + "'],";
            i++;
        }
        str_ChartContent = str_ChartContent.Substring(0, str_ChartContent.Length - 1);

        str_ChartTemplate = str_ChartTemplate.Replace("%%ChartJsonValues%%", str_ChartContent);
        str_ChartTemplate = str_ChartTemplate.Replace("%%BarHeight%%", Barheight.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%PositionLeft%%", PositionLeft.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%PositionTop%%", PositionTop.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%Title%%", str_Title.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%xtitle%%", str_XTitle.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%ytitle%%", str_YTitle.ToString());
        str_ChartTemplate = str_ChartTemplate.Replace("%%BarType%%", BarType.ToString());

        return str_ChartTemplate;
    }

    public string DrawSingleVerticalColumnChart(DataTable dt, string str_DisplayColumn, string str_ValueColumn)
    {
        return "";
    }
}