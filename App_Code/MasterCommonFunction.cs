using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.IO;

/// <summary>
/// Summary description for MasterCommonFunction
/// </summary>
public class MasterCommonFunction
{
    TreeNode tnProject;
    ApplicationCommonFunction acf;
    string str_MenuId = "", str_FilterdMenu = "", str_ParentMenu = "";
    string str_BaseUrl = "";
    public MasterCommonFunction()
    {
        this.str_BaseUrl = new SessionCustom().BaseUrl();
    }

    public string load_menu_Single(string str_AccountId)
    {
        string str_SingleLink = "<li class=' '><a href='%link%'> <em class='fa fa-file-o'></em><span>%Menu%</span></a></li>";
        string str_Menu = "", str_Menu_load = "", str_load = "";
        acf = new ApplicationCommonFunction();
       

        DataTable dt_menu = (DataTable) HttpContext.Current.Session["menu"];

        foreach (DataRow dr in dt_menu.Select("isnull(parent_menu_id,'')=''", "menu_order"))
        {
            MenuItem mi = new MenuItem();
            mi.Text = dr["menu_name"].ToString();
            mi.Value = dr["menu_id"].ToString();
            mi.PopOutImageUrl = "";
            mi.NavigateUrl = get_navigate_url(dr);
            DataRow[] dr_sub = dt_menu.Select("isnull(parent_menu_id,'')='" + dr["menu_id"].ToString() + "'", "menu_order");
            if (dr_sub.Length == 0)
            {
                str_Menu = str_SingleLink.Replace("%Menu%", mi.Text);
                str_Menu_load = str_Menu.Replace("%link%", mi.NavigateUrl);
                str_load += str_Menu_load;
            }
            str_load += load_child_menu_Second(dr["menu_id"].ToString(),dr["menu_name"].ToString(), dt_menu,"icon-folder-alt");
        }
        return str_load;

    }

    public string load_child_menu_Second(string parent_menu_id, string str_Parent_Menu_name, DataTable dt_menu,string str_Foldericon)
    {
        string str_subMenuUrl = "", str_subMenuName = "";
        string str_Menu = "<li class=' '><a href='#" + parent_menu_id + "' data-toggle='collapse'> <em class='%foldericon%'></em><span>" + str_Parent_Menu_name + "</span></a><ul id='" + parent_menu_id + "' class='nav sidebar-subnav collapse'><li class='sidebar-subnav-header'>" + str_Parent_Menu_name + "</li>";
        //string str_SubMenu = "<li><a href='%Link%' ><em class='fa fa-file-o'></em><span> %Menu% </span></a></li>";
        string str_SubMenu = "<li><a href='%Link%' ><span> %Menu% </span></a></li>";

        DataRow[] dr = dt_menu.Select("isnull(parent_menu_id,'')='" + parent_menu_id + "'", "menu_order");
        if (dr.Length > 0)
        {
            foreach (DataRow dr_child in dt_menu.Select("isnull(parent_menu_id,'')='" + parent_menu_id + "'", "menu_order"))
            {
                MenuItem mi_child = new MenuItem();
                mi_child.Text = dr_child["menu_name"].ToString();
                mi_child.Value = dr_child["menu_id"].ToString();
                mi_child.NavigateUrl = get_navigate_url(dr_child);
                if (dr_child["redirect_url"].ToString() != "")
                {
                    if (acf.convert_to_boolean(dr_child["is_new_window"].ToString()))
                        mi_child.Target = "_blank";
                }
                DataRow[] dr_grant_child = dt_menu.Select("isnull(parent_menu_id,'')='" + dr_child["menu_id"].ToString() + "'", "menu_order");
                if (dr_grant_child.Length > 0)
                {
                    str_Menu += this.load_child_menu_Second(dr_child["menu_id"].ToString(), dr_child["menu_name"].ToString(), dt_menu, "icon-arrow-right-circle");
                }
                else
                {
                    str_subMenuName = str_SubMenu.Replace("%Menu%", mi_child.Text);
                    str_subMenuUrl = str_subMenuName.Replace("%Link%", mi_child.NavigateUrl);
                    str_Menu += str_subMenuUrl;
                }
                str_Menu = str_Menu.Replace("%foldericon%", str_Foldericon);
            }
            return str_Menu + "</ul></li>";
        }
        else
        {
            return "";
        }
    }

   
    protected string get_navigate_url(DataRow dr_menu)
    {

        string str_navigate_url = this.str_BaseUrl;
        if (dr_menu["redirect_url"].ToString() != "")
        {
            str_navigate_url = dr_menu["redirect_url"].ToString();
            str_navigate_url = acf.replace_variable_with_value(str_navigate_url);
        }
        //else if ((dr_menu["grid_sql"].ToString() != "") && dr_menu["form_id"].ToString()=="")
        else if (dr_menu["grid_sql"].ToString() != "")
        {
            str_navigate_url += "www/web/DataTableReport.aspx?mid=" + dr_menu["menu_id"].ToString() + "&fid=" + dr_menu["form_id"].ToString();
        }
        else if (dr_menu["on_menu_click"].ToString() == "" || dr_menu["on_menu_click"].ToString() == "G" || dr_menu["on_menu_click"].ToString() == "S")
        {
            str_navigate_url += "www/web/DataTableView.aspx?fid=" + dr_menu["form_id"].ToString() + "&mid=" + dr_menu["menu_id"].ToString();
        }
        else if (dr_menu["on_menu_click"].ToString() == "T")
        {
            str_navigate_url += "www/web/DataTreeView.aspx?fid=" + dr_menu["form_id"].ToString() + "&mid=" + dr_menu["menu_id"].ToString();
        }
        else
        {
            str_navigate_url = "www/web/";

        }


        return str_navigate_url;

    }

    public void GenerateFormXml()
    {
        DataAccess DA = new DataAccess();
        DataTable dt = DA.GetDataTable("select form_id from form");
        string str_form_id = "";
        string str_sql = "";
        DataSet ds_form_tables;
        FileFunction FF = new FileFunction();
        foreach (DataRow dr in dt.Rows)
        {
            str_form_id = dr["form_id"].ToString();
            str_sql = "Select * from Form where form_id='" + str_form_id + "';";
            str_sql += "Select a.form_id, a.page_seq, a.is_master, a.add_caption, a.disable_add, B.* from Form_Page A, Page B where a.page_id=b.page_id and form_id='" + str_form_id + "';";
            str_sql += "Select a.form_id, b.db_table, C.* from Form_Page A, Page B, Page_Attribute C where a.page_id=b.page_id and b.page_id=c.page_id and form_id='" + str_form_id + "'";

            ds_form_tables = new DataAccess().GetDataSet(str_sql);
            ds_form_tables.Tables[0].TableName = "Form";
            ds_form_tables.Tables[1].TableName = "Page";
            ds_form_tables.Tables[2].TableName = "Page_Attribute";

            DataColumn[] dc_Parent = new DataColumn[1];
            dc_Parent[0] = ds_form_tables.Tables[0].Columns["form_id"];
            DataColumn[] dc_Child = new DataColumn[1];
            dc_Child[0] = ds_form_tables.Tables[1].Columns["form_id"];
            DataRelation drel1 = ds_form_tables.Relations.Add("Relation1", dc_Parent, dc_Child);
            drel1.Nested = true;


            DataColumn[] dc_Parent1 = new DataColumn[2];
            dc_Parent1[0] = ds_form_tables.Tables[1].Columns["form_id"];
            dc_Parent1[1] = ds_form_tables.Tables[1].Columns["page_id"];
            DataColumn[] dc_Child1 = new DataColumn[2];
            dc_Child1[0] = ds_form_tables.Tables[2].Columns["form_id"];
            dc_Child1[1] = ds_form_tables.Tables[2].Columns["page_id"];
            DataRelation drel2 = ds_form_tables.Relations.Add("Relation2", dc_Parent1, dc_Child1);
            drel2.Nested = true;

            FF.WriteFileFormDictionarySchema(str_form_id, ds_form_tables.GetXmlSchema());
            FF.WriteFileFormDictionary(str_form_id, ds_form_tables.GetXml());

        }

    }

    public void load_menu(TreeView mn_main)
    {
        acf = new ApplicationCommonFunction();

        HDDatabase hddb = new HDDatabase();
        string sqlstring = "select * from Menu where is_active=1 order by menu_order";

        DataTable dt_menu = new DataTable();
        dt_menu = hddb.GetDataTable(sqlstring, "Menu");
        foreach (DataRow dr in dt_menu.Select("isnull(parent_menu_id,'')=''", "menu_order"))
        {
            MenuItem mi = new MenuItem();
            mi.Text = "<img src='Images/Main_menu.png' width=20px height=20px alt='' style='padding-right:5px;margin-left: 5px;' />" + dr["menu_name"].ToString();
            //mi.Text = dr["menu_name"].ToString();
            mi.Value = dr["Row_key"].ToString();
            //mi.PopOutImageUrl = "";
            mi.NavigateUrl = get_navigate_url(dr);
            tnProject = new TreeNode(mi.Text, mi.Value, null, mi.NavigateUrl, "_blank");
            mn_main.Nodes.Add(tnProject);
            load_child_menu(mi, dr["menu_id"].ToString(), dt_menu);

        }

    }

    private void load_child_menu(MenuItem mi, string parent_menu_id, DataTable dt_menu)
    {

        foreach (DataRow dr_child in dt_menu.Select("isnull(parent_menu_id,'')='" + parent_menu_id + "'", "menu_order"))
        {
            MenuItem mi_child = new MenuItem();
            mi_child.Text = "<img src='Images/Child_menu.png' width=20px height=20px alt='' style='padding-right:5px;margin-left: 5px;' />" + dr_child["menu_name"].ToString();
            
            mi_child.Value = dr_child["Row_key"].ToString();
            mi_child.NavigateUrl = get_navigate_url(dr_child);

            TreeNode tn_submenu = new TreeNode(mi_child.Text, mi_child.Value, null, mi_child.NavigateUrl, "_blank");

            tnProject.ChildNodes.Add(tn_submenu);
            if (dr_child["redirect_url"].ToString() != "")
            {
                if (acf.convert_to_boolean(dr_child["is_new_window"].ToString()))
                    mi_child.Target = "_blank";
            }
            mi.ChildItems.Add(mi_child);
            DataRow[] dr_grant_child = dt_menu.Select("isnull(parent_menu_id,'')='" + dr_child["menu_id"].ToString() + "'", "menu_order");
            if (dr_grant_child.Length > 0)
            {
                this.load_child_menu(mi_child, dr_child["menu_id"].ToString(), dt_menu);
            }
        }

    }

}
