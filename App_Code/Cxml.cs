﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.IO;
using System.Web;

/// <summary>
/// Summary description for Cxml
/// </summary>
public class Cxml
{
	public Cxml()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet XmlStringToDataSet(string xmlData)
    {
        StringReader stream = new StringReader(xmlData);
        XmlTextReader reader = new XmlTextReader(stream);

        DataSet xmlDS = new DataSet();
        xmlDS.ReadXml(reader);

        if (reader != null) reader.Close();

        return xmlDS;

    }

    public DataSet XmlStringToDataSet(string xmlData, string xmlSchema)
    {
        StringReader streamSchema = new StringReader(xmlSchema);
        XmlTextReader readerSchema = new XmlTextReader(streamSchema);
        DataSet xmlDS = new DataSet();
        xmlDS.ReadXmlSchema(readerSchema);


        StringReader stream = new StringReader(xmlData);
        XmlTextReader reader = new XmlTextReader(stream);
        xmlDS.ReadXml(reader, XmlReadMode.IgnoreSchema);
        if (reader != null) reader.Close();

        return xmlDS;

    }

}