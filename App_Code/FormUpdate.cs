﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.IO;
using System.Xml;
using System.Web;

/// <summary>
/// Summary description for FormUpdate
/// </summary>
public class FormUpdate
{
    string str_FormId = "";
	
    public FormUpdate()
	{
        
	}

    public DataSet ReadSchema(string str_XmlSchemaUrl)
    {
        WebClient client = new WebClient();
        string strResult = client.DownloadString(str_XmlSchemaUrl);
        DataSet ds = new DataSet();
        System.IO.StringReader SR = new System.IO.StringReader(strResult);
        XmlReader xmlReader = XmlReader.Create(SR);
        ds.ReadXmlSchema(xmlReader);
        SR.Close();
        xmlReader.Close();
        return ds;
    }

    public string PostData(string str_PostDataUrl, string str_PostData)
    {
         HttpWebRequest request = (HttpWebRequest)WebRequest.Create(str_PostDataUrl);
         request.Method = "POST";
         byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(str_PostData);
         request.ContentType = "application/x-www-form-urlencoded";
         request.ContentLength = byteArray.Length;
         System.IO.Stream dataStream = request.GetRequestStream();
         dataStream.Write(byteArray, 0, byteArray.Length);
         dataStream.Close();
         WebResponse response = request.GetResponse();
         dataStream = response.GetResponseStream();
         System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);
         string responseFromServer = HttpUtility.UrlDecode(reader.ReadToEnd());
         reader.Close();
         dataStream.Close();
         response.Close();

         return responseFromServer;
    }

}