﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Data;
using System.IO;

public class DataAccessExcel : DataAccessF
{
    protected string str_password = "";
    protected string str_file = "";
    protected int int_version = 2003;

    public DataAccessExcel(string str_file)
    {
        this.str_file = str_file;
        if (!ExcelFileExists()) throw new Exception("Excel File Not Found; File Name:" + this.str_file);
        this.UpdateExcelConnectionString();
        this.Intialize();
    }

    protected void UpdateExcelConnectionString()
    {
        if (this.int_version == 2007)
            this.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.ExcelFile + ";Extended Properties='Excel 12.0;IMEX=1'";
        else if (int_version == 2003)
            this.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this.ExcelFile + ";Extended Properties='Excel 8.0;IMEX=1'";
        else
            throw new Exception("Excel Version not supported:" + this.int_version.ToString() + "; Valid version number should be 2003 or 2007");
    }

    public string GetOledbConnectionString()
    {
        string str_FileExt = Path.GetExtension(this.str_file);
        if (str_FileExt.ToLower() == ".xlsx")
            this.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.ExcelFile + ";Extended Properties='Excel 12.0;IMEX=1'";
        else if (str_FileExt.ToLower() == ".xls")
            this.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.ExcelFile + ";Extended Properties='Excel 8.0;IMEX=1'";
        else
            this.ConnectionString = "";
        return this.ConnectionString;
    }

    public string ExcelPassword { get { return this.str_password; } set { this.str_password = value; } }
    public string ExcelFile { get { return this.str_file; } set { this.str_file = value; } }

    public void SetExcelVersion(int int_version)
    {
        this.int_version = int_version;
        this.UpdateExcelConnectionString();
    }

    public bool ExcelFileExists()
    {
        bool bln_Exists = false;
        System.IO.FileInfo FI = new System.IO.FileInfo(this.ExcelFile);
        bln_Exists = FI.Exists;
        return bln_Exists;
    }

    protected override void Intialize()
    {
        this.DbCon = new OleDbConnection();
        OleDbTransaction oledb_dbtran = null;
        this.DbTran = oledb_dbtran;
        this.DbComm = new OleDbCommand();
        this.DbAdap = new OleDbDataAdapter();
        OleDbDataReader oledb_dbread = null;
        this.DbRead = oledb_dbread;
    }

    public DataTable ExcelSheetColumns(string str_SheetName)
    {
        DataTable dt_data = this.ExcelSheetData(str_SheetName, "1=2");

        DataTable dt_column = new DataTable(str_SheetName);
        dt_column.Columns.Add(new DataColumn("TableName", typeof(System.String)));
        dt_column.Columns.Add(new DataColumn("ColumnOrder", typeof(System.Int32)));
        dt_column.Columns.Add(new DataColumn("ColumnName", typeof(System.String)));
        dt_column.Columns.Add(new DataColumn("ColumnDataType", typeof(System.String)));
        dt_column.Columns.Add(new DataColumn("ColumnLength", typeof(System.Int32)));

        foreach (DataColumn dc in dt_data.Columns)
        {
            DataRow dr_new = dt_column.NewRow();
            dr_new["TableName"] = str_SheetName;
            dr_new["ColumnOrder"] = dc.Ordinal + 1;
            dr_new["ColumnName"] = dc.ColumnName;
            dr_new["ColumnDataType"] = dc.DataType.Name;
            dr_new["ColumnLength"] = dc.MaxLength;
            dt_column.Rows.Add(dr_new);
        }

        return dt_column;
    }

    public DataTable ExcelSheetData(string str_SheetName)
    {
        return this.ExcelSheetData(str_SheetName, "");
    }

    public DataSet ExcelSheetData()
    {
        DataSet ds = new DataSet();
        DataTable dt = null;
        foreach (string str_SheetName in this.ExcelSheetNames())
        {
            dt = this.ExcelSheetData(str_SheetName);
            ds.Tables.Add(dt);
        }

        return ds;
    }

    //public bool ExecuteNonQuery(string str_Sql)
    //{
    //    this.DbComm = new OleDbCommand(str_Sql);
    //    bool bln_Result= this.ExecuteNonQuery(this.DbComm);
    //}

    public int ExcelSheetDataToDbTable(string str_DbTableName, string str_SheetName, string str_where)
    {
        int int_RowCount = 0;
        string str_Query = "select * into " + str_DbTableName + " from [" + str_SheetName + "]";
        if (str_where != "")
            str_Query += " where " + str_where;

        this.DbComm = new OleDbCommand(str_Query);
        this.DbComm.Connection = this.DbCon;
        try
        {
            this.OpenDatabaseConnection();
            int_RowCount = this.DbComm.ExecuteNonQuery();

        }
        catch (OleDbException OE)
        {
            throw OE;
        }
        finally
        {
            this.CloseDatabaseConnection();
            this.DbComm.Dispose();

        }
        return int_RowCount;
    }

    public DataTable ExcelSheetData(string str_SheetName, string str_where)
    {
        string str_Query = "select * from [" + str_SheetName + "]";
        if (str_where != "")
            str_Query += " where " + str_where;

        this.DbComm = new OleDbCommand(str_Query);
        DataTable dt = null;
        try
        {
            dt = this.GetDataTable(this.DbComm);
        }
        catch (OleDbException OE)
        {
            this.LW.WriteLog(OE, "Excel Sheet Name:" + str_SheetName, LogWriter.Severity.Error);

            if (OE.ErrorCode == -2147467259)
            {
                throw new Exception("Excel Sheet not found: " + str_SheetName);
            }
            else if (OE.ErrorCode == -2147217904)
            {
                throw new Exception("Excel Column not found: " + str_SheetName);
            }
            else throw OE;
        }
        catch (Exception EX)
        {
            this.LW.WriteLog(EX, "Excel Sheet Name:" + str_SheetName, LogWriter.Severity.Error);
            throw EX;
        }
        dt.TableName = str_SheetName;
        return dt;
    }

    public String[] ExcelSheetNames()
    {
        String[] str_SheetNames = null;
        OleDbConnection odc = new OleDbConnection(this.ConnectionString);
        try
        {
            odc.Open();
            DataTable dt = odc.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            str_SheetNames = new String[dt.Rows.Count];
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                str_SheetNames[i] = dr["Table_Name"].ToString();
                i++;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (odc != null)
            {
                odc.Close();
                odc.Dispose();
            }

        }
        return str_SheetNames;

    }

}
          