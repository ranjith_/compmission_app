using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Net.Mime;

/// <summary>
/// Summary description for OtherCommonFunction
/// </summary>
public class OtherCommonFunction
{
    DataAccess DA = new DataAccess();
    AppVar AV = new AppVar();
    PhTemplate PHT = new PhTemplate();
    string str_EmailUserName, str_EmailPassword;
    public OtherCommonFunction()
    {
        str_EmailUserName = "";
        str_EmailPassword = "";
        //
        // TODO: Add constructor logic here
        //
    }

    public string get_hidden_page_text(string str_page_id, string str_hidden_text)
    {
        string str_return_page_text = str_hidden_text;
        string str_key_start = "#" + str_page_id + "|";
        int in_start = str_hidden_text.IndexOf(str_key_start) + str_key_start.Length;
        str_hidden_text = str_hidden_text.Substring(in_start);
        if (str_hidden_text.IndexOf("|#") > 0)
        {
            str_hidden_text = str_hidden_text.Substring(0, str_hidden_text.IndexOf("|#"));

            string str_act_page = str_key_start;
            foreach (string str_rownum in str_hidden_text.Split(new char[] { '|' }))
            {
                str_act_page += str_rownum + "|";
            }
            str_act_page += "#";

            return str_act_page;
        }
        else
        {
            return str_return_page_text;
        }
    }

    public ArrayList get_hidden_page_rowkey_array(string str_page_id, string str_hidden_text)
    {
        ArrayList al = new ArrayList();
        string str_return_page_text = str_hidden_text;
        string str_key_start = "#" + str_page_id + "|";

        if (str_hidden_text.IndexOf(str_key_start) >= 0)
        {
            int in_start = str_hidden_text.IndexOf(str_key_start) + str_key_start.Length;
            str_hidden_text = str_hidden_text.Substring(in_start);
            if (str_hidden_text.IndexOf("|#") > 0)
            {
                str_hidden_text = str_hidden_text.Substring(0, str_hidden_text.IndexOf("|#"));

                string str_act_page = str_key_start;
                foreach (string str_rownum in str_hidden_text.Split(new char[] { '|' }))
                {
                    str_act_page += str_rownum + "|";
                    al.Add(str_rownum);
                }
                str_act_page += "#";
            }
        }

        return al;
    }

    public string add_row_to_hidden(string str_page_id, HiddenField hf)
    {
        string str_hidden_value = hf.Value;
        string str_hidden_page_actual_text = this.get_hidden_page_text(str_page_id, str_hidden_value);
        string str_hidden_page_text = str_hidden_page_actual_text.Substring(1);
        str_hidden_page_text = str_hidden_page_text.Substring(0, str_hidden_page_text.IndexOf("#"));

        string str_rows = str_hidden_page_text.Substring(str_hidden_page_text.IndexOf("|") + 1);
        int new_row = 0;
        foreach (string str_rownum in str_rows.Split(new char[] { '|' }))
        {
            if (str_rownum != "")
            {
                new_row = Convert.ToInt32(str_rownum);
            }
            else
            {
                new_row++;
                str_hidden_page_text += new_row.ToString() + "|";
            }
        }

        str_hidden_page_text = "#" + str_hidden_page_text + "#";

        hf.Value = str_hidden_value.Replace(str_hidden_page_actual_text, str_hidden_page_text);
        return new_row.ToString();


    }

    public string remove_row_from_hidden(string str_page_id, string str_hidden_value, string row_num)
    {
        string str_hidden_page_actual_text = this.get_hidden_page_text(str_page_id, str_hidden_value);
        string str_hidden_page_text = str_hidden_page_actual_text.Replace("|" + row_num + "|", "|");

        return str_hidden_value.Replace(str_hidden_page_actual_text, str_hidden_page_text);
    }

    public string get_db_format_current_date()
    {
        string str_date = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
        str_date = "Convert(Varchar,'" + str_date + "',101)";
        return str_date;
    }

    public bool is_number(string str_value)
    {
        try
        {
            double dbl_vallue = Convert.ToDouble(str_value);
            return true;
        }
        catch (Exception e1)
        {
            return false;
        }


    }

    public string ExporttoExcel(DataTable table, string str_FileName)
    {
        str_FileName = str_FileName.Replace(" ", "_");
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        HttpContext.Current.Response.Write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><meta http-equiv='content-type' content='text-html; charset=utf-8'>");
        string style = @"<style> .xlText {mso-number-format: '\@';} </style>";
        HttpContext.Current.Response.Write(style);
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + str_FileName + ".xls");
        HttpContext.Current.Response.Charset = "utf-8";
        HttpContext.Current.Response.ContentEncoding = Encoding.GetEncoding("utf-8");
        HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
        HttpContext.Current.Response.Write("<BR><BR><BR>");
        HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
        int count = table.Columns.Count;
        for (int i = 0; i < count; i++)
        {
            HttpContext.Current.Response.Write("<Td>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write(table.Columns[i].ColumnName);
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");
        }
        HttpContext.Current.Response.Write("</TR>");
        foreach (DataRow row in table.Rows)
        {
            HttpContext.Current.Response.Write("<TR>");
            for (int j = 0; j < table.Columns.Count; j++)
            {
                HttpContext.Current.Response.Write("<TD class=\"xlText\" >");

                HttpContext.Current.Response.Write(row[j].ToString());
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
        }
        HttpContext.Current.Response.Write("</Table>");
        HttpContext.Current.Response.Write("</font>");
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
        return "";
    }

    public SqlCommand GetSQLCommandText(string str_ControlValue, string str_InsertQuery)
    {
        ArrayList al_cmd = new ArrayList();
        string[] str_ValueString = str_ControlValue.Split('&');
        int int_length = str_ValueString.Length;
        string str_Query = "";
        string str_ColumnName = "", str_ColumnValue = "";
        SqlCommand sc = new SqlCommand();
        for (int i = 0; i < int_length; i++)
        {
            string[] str_value = str_ValueString[i].Split('=');
            str_ColumnName += str_value[0].Replace("txt_", "") + ",";
            str_ColumnValue += "@" + str_value[0].Replace("txt_", "") + ",";

            sc.Parameters.AddWithValue("@" + str_value[0].Replace("txt_", ""), str_value[1]);
        }
        str_ColumnName = str_ColumnName.Substring(0, str_ColumnName.Length - 1);
        str_ColumnValue = str_ColumnValue.Substring(0, str_ColumnValue.Length - 1);
        str_Query = str_InsertQuery.Replace("%%ColumnName%%", "(" + str_ColumnName + ")").Replace("%%ColumnValue%%", "(" + str_ColumnValue + ")");
        sc.CommandText = str_Query;
        return sc;

    }

    public string GetLiveCusotmerAccount(string str_EmpKey)
    {
        string str_AccountId = "";
        try
        {
            string str_SQl = "exec [p_EmployeeCustomerAccess] @Empkey";
            SqlCommand cmd = new SqlCommand(str_SQl);
            cmd.Parameters.AddWithValue("@Empkey", str_EmpKey);
            DataTable dt = DA.GetDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    str_AccountId += "'" + dr["KareoID"].ToString() + "',";
                }
                str_AccountId = str_AccountId.Substring(0, str_AccountId.Length - 1);
            }

        }
        catch (Exception ex)
        {
            return "";
        }

        return str_AccountId;
    }

    public void SendMail(string str_Subject, string str_Body, string str_MailTo, string str_MailToCC)
    {
        //string str_EmailUserName = "shanmugam0071@outlook.com";
        //string str_EmailPassword = "shanmugam007";
        
        string str_EmailUserName = "rk792665@gmail.com";
        string str_EmailPassword = "ranji62423";
        //MailMessage msg = new MailMessage();
        //msg.To.Add(str_MailTo);
        
        //msg.Bcc.Add("shreeshanmugam25@gmail.com");
        //msg.Bcc.Add("vanaja@globalhealthcareresource.com");
        //msg.Bcc.Add("ramesh@globalhealthcareresource.com");
        //msg.Bcc.Add("k_davamani@yahoo.com");
        //Configure the address we are sending the mail from
        //MailAddress address = new MailAddress(str_EmailUserName);

        //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
        //msg.From = new MailAddress(str_EmailUserName);
        //msg.Subject = str_Subject;
        //msg.Body = str_Body;
        //msg.IsBodyHtml = true;

        //SmtpServer.Port = 587;
        //SmtpServer.Credentials = new System.Net.NetworkCredential(str_EmailUserName, str_EmailPassword);
        //SmtpServer.EnableSsl = true;
        //SmtpServer.Send(msg);

        SmtpClient client = new SmtpClient();
        client.Port = 587;
        client.Host = "smtp.gmail.com";
        client.EnableSsl = true;
        client.Timeout = 10000;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.UseDefaultCredentials = true;
        client.Credentials = new System.Net.NetworkCredential(str_EmailUserName,str_EmailPassword);

        MailMessage mm = new MailMessage(str_EmailUserName, str_MailTo);
        if (str_MailToCC != "")
        {
            string[] str_CC = str_MailToCC.Substring(0, str_MailToCC.Length - 1).Split(',');
            for (int i = 0; i < str_CC.Length; i++)
            {
                mm.Bcc.Add(str_CC[i]);
            }
        }
        mm.Subject = str_Subject;
        mm.IsBodyHtml = true;
        mm.Body = str_Body;
        
        //mm.BodyEncoding = UTF8Encoding.UTF8;
        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        client.Send(mm);



        ////Configure an SmtpClient to send the mail.            
        //SmtpClient client = new SmtpClient();
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //client.EnableSsl = true;
        //client.Host = "smtp.office365.com";
        //client.Port = 25;

        ////Setup credentials to login to our sender email address ("UserName", "Password")
        //NetworkCredential credentials = new NetworkCredential(str_EmailUserName, str_EmailPassword);
        //client.UseDefaultCredentials = true;
        //client.Credentials = credentials;

        //Send the msg
        //client.Send(msg);

    }

   

    public void sendEMailThroughOUTLOOK(string str_Subject, string str_Body, string str_MailTo)
    {
        string str_EmailUserName = "shanmugam0071@outlook.com";
        string str_EmailPassword = "shanmugam007";
        MailMessage msg = new MailMessage();
        msg.To.Add(str_MailTo);
        //Configure the address we are sending the mail from
        MailAddress address = new MailAddress(str_EmailUserName);
        msg.From = address;
        msg.Subject = str_Subject;
        //msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(str_Body, null, MediaTypeNames.Text.Html));
        msg.IsBodyHtml = true;
        msg.Body = str_Body;
        //Configure an SmtpClient to send the mail.            
        SmtpClient client = new SmtpClient();
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.EnableSsl = true;
        client.Host = "smtp.office365.com";
        client.Port = 25;

        //Setup credentials to login to our sender email address ("UserName", "Password")
        NetworkCredential credentials = new NetworkCredential(str_EmailUserName, str_EmailPassword);
        client.UseDefaultCredentials = true;
        client.Credentials = credentials;

        //Send the msg
        client.Send(msg);
    }


    public string GetAllReportingEmployeeIds(string str_EmployeeKey)
    {
        //DataTable dt = this.SC.ReportingUserKeys;
        DataTable dt = this.DA.GetDataTable("select EmployeeKey,ReportingEmployeeId from v_EmployeeWorkRecent where EmployeestatusId=1");
        string str_Ids = "";
        foreach (DataRow dr in dt.Select("ReportingEmployeeId='" + str_EmployeeKey + "'", ""))
        {
            DataRow[] dr_sub = dt.Select("isnull(ReportingEmployeeId,'')='" + dr["EmployeeKey"].ToString() + "'", "");
            if (dr_sub.Length != 0)
                str_Ids += GetNextLevelReportingEmployeeIds(dt, dr["EmployeeKey"].ToString());
            str_Ids += "'" + dr["EmployeeKey"].ToString() + "',";
        }
        return str_Ids;
    }

    public string GetNextLevelReportingEmployeeIds(DataTable dt, string ReportingEmployeeId)
    {
        string str_Ids = "";
        foreach (DataRow dr_child in dt.Select("ReportingEmployeeId='" + ReportingEmployeeId + "'", ""))
        {
            DataRow[] dr_sub = dt.Select("isnull(ReportingEmployeeId,'')='" + dr_child["EmployeeKey"].ToString() + "'", "");
            if (dr_sub.Length != 0)
                str_Ids += GetNextLevelReportingEmployeeIds(dt, dr_child["EmployeeKey"].ToString());
            str_Ids += "'" + dr_child["EmployeeKey"].ToString() + "',";
        }
        return str_Ids;
    }

    public string LoadParentHierarchy(string str_EmpKey, string str_RedirectPage)
    {
        string str_HTML = PHT.ReadFileToString("EmployeeTreeView_li.txt");
        string str_ReturnHTML = "", str_replaceStr = "";
        string str_Sql = "select case when EmpOriginalName<>'' then EmpOriginalName else EmpName end as EmpdisplayName, "
            + " EmpKey,EmpCode,EmpName,EmpOriginalName,ReportingTo,b.LocationName Location,EmpCount,a.CompanyId from IEmployee a "
            + " left outer join v_ListLocation b on a.LocationId = b.LocationValue where a.CompanyId=2";

        DataTable dt = this.DA.GetDataTable(str_Sql);

        foreach (DataRow dr in dt.Select("ReportingTo='" + str_EmpKey + "'", ""))
        {
            DataRow[] dr_sub = dt.Select("isnull(ReportingTo,'')='" + dr["EmpKey"].ToString() + "'", "");
            if (dr_sub.Length != 0)
            {
                str_ReturnHTML += LoadChildHierarchy(dr["EmpKey"].ToString(), dr["EmpdisplayName"].ToString(), dr["EmpCode"].ToString(),
                    dr["EmpName"].ToString(), dr["Location"].ToString(), dr["EmpCount"].ToString(), str_RedirectPage, dt);
            }
            else
            {
                str_replaceStr = str_HTML;
                str_replaceStr = str_replaceStr.Replace("%%EmployeeName%%", dr["EmpdisplayName"].ToString());
                str_replaceStr = str_replaceStr.Replace("%%EmpCode%%", dr["EmpCode"].ToString());
                str_replaceStr = str_replaceStr.Replace("%%EmpKey%%", dr["EmpKey"].ToString());
                str_replaceStr = str_replaceStr.Replace("%%PseudoName%%", dr["EmpName"].ToString());
                str_replaceStr = str_replaceStr.Replace("%%Location%%", dr["Location"].ToString());
                str_replaceStr = str_replaceStr.Replace("%%EmpCount%%", dr["EmpCount"].ToString());
                str_replaceStr = str_replaceStr.Replace("%%redirectpage%%", str_RedirectPage);
                str_ReturnHTML += str_replaceStr;
            }
        }
        return str_ReturnHTML;
    }

    protected string LoadChildHierarchy(string str_EmpKey, string str_EmpName, string str_EmpCode, string str_EmpPseudoname,
        string str_Location, string str_EmpCount, string str_RedirectPage, DataTable dt)
    {
        string str_SubHTML = PHT.ReadFileToString("EmployeeTreeView_lichild.txt");
        string str_SubHTML_Child = PHT.ReadFileToString("EmployeeTreeView_li.txt");
        string str_ReturnSubHTML = "";
        str_ReturnSubHTML = str_SubHTML;
        str_ReturnSubHTML = str_ReturnSubHTML.Replace("%%EmployeeName%%", str_EmpName);
        str_ReturnSubHTML = str_ReturnSubHTML.Replace("%%EmpCode%%", str_EmpCode);
        str_ReturnSubHTML = str_ReturnSubHTML.Replace("%%EmpKey%%", str_EmpKey);
        str_ReturnSubHTML = str_ReturnSubHTML.Replace("%%PseudoName%%", str_EmpPseudoname);
        str_ReturnSubHTML = str_ReturnSubHTML.Replace("%%Location%%", str_Location);
        str_ReturnSubHTML = str_ReturnSubHTML.Replace("%%EmpCount%%", str_EmpCount);
        str_ReturnSubHTML = str_ReturnSubHTML.Replace("%%redirectpage%%", str_RedirectPage);
        string str_LastLevelEmp = "", str_LastLevelHtml = "";
        foreach (DataRow dr_child in dt.Select("ReportingTo='" + str_EmpKey + "'", ""))
        {
            DataRow[] dr_grant_child = dt.Select("isnull(ReportingTo,'')='" + dr_child["EmpKey"].ToString() + "'", "");
            if (dr_grant_child.Length > 0)
            {
                str_ReturnSubHTML += this.LoadChildHierarchy(dr_child["EmpKey"].ToString(), dr_child["EmpdisplayName"].ToString(),
                    dr_child["EmpCode"].ToString(), dr_child["EmpName"].ToString(), dr_child["Location"].ToString(), dr_child["EmpCount"].ToString(), str_RedirectPage, dt);
            }
            else
            {
                str_LastLevelEmp = str_SubHTML_Child;
                str_LastLevelEmp = str_LastLevelEmp.Replace("%%EmployeeName%%", dr_child["EmpdisplayName"].ToString());
                str_LastLevelEmp = str_LastLevelEmp.Replace("%%EmpCode%%", dr_child["EmpCode"].ToString());
                str_LastLevelEmp = str_LastLevelEmp.Replace("%%EmpKey%%", dr_child["EmpKey"].ToString());
                str_LastLevelEmp = str_LastLevelEmp.Replace("%%PseudoName%%", dr_child["EmpName"].ToString());
                str_LastLevelEmp = str_LastLevelEmp.Replace("%%Location%%", dr_child["Location"].ToString());
                str_LastLevelEmp = str_LastLevelEmp.Replace("%%EmpCount%%", dr_child["EmpCount"].ToString());
                str_LastLevelEmp = str_LastLevelEmp.Replace("%%redirectpage%%", str_RedirectPage);
                str_LastLevelHtml += str_LastLevelEmp;
            }
        }
        return str_ReturnSubHTML + str_LastLevelHtml + "</ul></li>";
    }


    public string PasswordRecover(string str_email, string type, string str_subject, string str_link, string str_firstname)
    {
        string str_msg = "success";
        string str_template = "";

        if (type == "password")
        {

            str_template = "Passwordrecover.txt";
        }

        SmtpClient smtpClient = new SmtpClient();

        var smtp = new System.Net.Mail.SmtpClient();
        {
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential("gamesakmp@gmail.com", "illumo_2016");
            smtp.Timeout = 20000;
        }
        MailMessage mail = new MailMessage();

        //Setting From , To and CC
        mail.From = new MailAddress("gamesakmp@gmail.com", "Gamesa");
        mail.To.Add(new MailAddress(str_email));
        string subject = str_subject;
        string body = this.PHT.ReadFileToString(str_template);
        body = this.PHT.ReplaceVariableWithValueForEmail(str_firstname, str_link, body);
        mail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html));
        mail.IsBodyHtml = true;
        mail.Subject = subject;

        smtp.Send(mail);

        return str_msg;
    }

    //public string GetDashBoardURL(SessionCustom SC)
    //{
    //    if (SC.UserRoleKeys.ToLower().Contains("ed7b6e4f-2155-4fc0-9466-db31d23c3c9d")) return "../Dashboard/DashboardAdmin.aspx";
    //    else if (SC.UserRoleKeys.ToLower().Contains("5fd2ede0-b728-4198-961c-4280bbb2407c")) return "../Web/ManualEntriesList.aspx";
    //    return "../Dashboard/Dashboard.aspx";
    //}

}
