﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
/// <summary>
/// Summary description for SessionCustom
/// </summary>
public class SessionCustom
{
    DataTable dt_Company = null;
    DataTable dt_Menu = null;
    DataTable dt_QuickAccess = null;
    DataTable dt_Provider = null;
    DataTable dt_Empmanager = null;

    public SessionCustom()
    {
    }

    public SessionCustom(bool bln_SessionCheckRequired)
    {
        if (HttpContext.Current.Session["UserKey"] == null)
            this.NavigateToLoginPage();
    }

    public string GetUserKey()
    {
        return UserKey;
    }


    public string BaseUrl()
    {
        return HttpContext.Current.Session["httproot"].ToString();
    }

    public void NavigateToLoginPage()
    {
        HttpContext.Current.Response.Redirect("../Web/Login.aspx");
    }

    public DataRow UserRecord
    {
        get { return (DataRow)HttpContext.Current.Session["userrecord"]; }
        set { HttpContext.Current.Session["userrecord"] = value; }
    }

    public string RoleName
    {
        get { return HttpContext.Current.Session["Role"].ToString(); }
        set { HttpContext.Current.Session["Role"] = value; }
    }

    public string UserKey
    {
        get { return HttpContext.Current.Session["UserKey"].ToString(); }
        set { HttpContext.Current.Session["UserKey"] = value; }
    }
    public string CompanyLogo
    {
        get { return HttpContext.Current.Session["CompanyLogo"].ToString(); }
        set { HttpContext.Current.Session["CompanyLogo"] = value; }
    }
    public string RoleKey
    {
        get { return HttpContext.Current.Session["RoleKey"].ToString(); }
        set { HttpContext.Current.Session["RoleKey"] = value; }
    }
    public string UserName
    {
        get { return HttpContext.Current.Session["UserName"].ToString(); }
        set { HttpContext.Current.Session["UserName"] = value; }
    }
    public string Name
    {
        get { return HttpContext.Current.Session["Name"].ToString(); }
        set { HttpContext.Current.Session["Name"] = value; }
    }
   
    public string EmployeeName
    {
        get { return HttpContext.Current.Session["EmployeeName"].ToString(); }
        set { HttpContext.Current.Session["EmployeeName"] = value; }
    }

    public string EmployeeCode
    {
        get { return HttpContext.Current.Session["EmployeeCode"].ToString(); }
        set { HttpContext.Current.Session["EmployeeCode"] = value; }
    }

    public string LocationKey
    {
        get { return HttpContext.Current.Session["LocationKey"].ToString(); }
        set { HttpContext.Current.Session["LocationKey"] = value; }
    }

    public string HospitalKey
    {
        get { return HttpContext.Current.Session["HospitalKey"].ToString(); }
        set { HttpContext.Current.Session["HospitalKey"] = value; }
    }

    public string HospitalName
    {
        get { return HttpContext.Current.Session["HospitalName"].ToString(); }
        set { HttpContext.Current.Session["HospitalName"] = value; }
    }

    public string LicenseExpiryOn
    {
        get { return HttpContext.Current.Session["LicenseExpiryOn"].ToString(); }
        set { HttpContext.Current.Session["LicenseExpiryOn"] = value; }
    }

    public string ImageKey
    {
        get { return HttpContext.Current.Session["ImageKey"].ToString(); }
        set { HttpContext.Current.Session["ImageKey"] = value; }
    }

    public string FilePath
    {
        get { return HttpContext.Current.Session["FilePath"].ToString(); }
        set { HttpContext.Current.Session["FilePath"] = value; }
    }

    public string DoctorKey
    {
        get { return HttpContext.Current.Session["DoctorKey"].ToString(); }
        set { HttpContext.Current.Session["DoctorKey"] = value; }
    }

    public string TypeId
    {
        get { return HttpContext.Current.Session["TypeId"].ToString(); }
        set { HttpContext.Current.Session["TypeId"] = value; }
    }

    public bool IsDoctor()
    {
        if (HttpContext.Current.Session["TypeId"].ToString() == "1") return true;
        return false;
    }

    public bool IsPatient()
    {
        if (HttpContext.Current.Session["TypeId"].ToString() == "2") return true;
        return false;
    }

    public bool IsHospitalAdmin()
    {
        if (HttpContext.Current.Session["TypeId"].ToString() == "3") return true;
        return false;
    }

    public bool IsOperationAdmin()
    {
        if (HttpContext.Current.Session["TypeId"].ToString() == "4") return true;
        return false;
    }

    public bool IsSuperAdmin()
    {
        if (HttpContext.Current.Session["TypeId"].ToString() == "5") return true;
        return false;
    }

    public bool IsOperationManager()
    {
        if (HttpContext.Current.Session["TypeId"].ToString() == "6") return true;
        return false;
    }

    public bool IsOperationSupport()
    {
        if (HttpContext.Current.Session["TypeId"].ToString() == "7") return true;
        return false;
    }

    public bool IsAdmin()
    {
        if (this.UserKey.ToUpper() == "4DF6199D-070B-416B-9EC2-F6E25A7C48C2") return true;
        return false;
    }

    public bool SessionExists()
    {
        if (HttpContext.Current.Session["UserKey"] == null || HttpContext.Current.Session["UserKey"].ToString() == "")
            return false;
        else
            return true;
    }

}
