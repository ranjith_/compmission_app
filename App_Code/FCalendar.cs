﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GChart
/// </summary>
/// 
public class FCalendar
{
    string str_TemplateFile = "";
    int int_CalendarMaxWidth = 0;
    PhTemplate PHT = new PhTemplate();
    bool bln_CreateDivControl = true;
    string str_Title = "";

    string str_DivName = "";

    string str_EventColumnName = "", str_EventStartDateColumnName = "", str_EventEndDateColumnName = "", str_EventUrlColumnName = "";

    static string[] ColourValues = new string[] {
                "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
        "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A", "F9810A",
    };
    public FCalendar(string str_DivName)
    {
        this.str_DivName = str_DivName;
        this.Editable = true;
        this.EventLimit = true;
    }

    

    public string CalendarTemplateFile { set; get; }
    public int CalendarMaxWidth { set; get; }
    public bool CreateDivControl { set; get; }
    public string CalendarTitle { set; get; }
    public string ColumnNameEndDate { set; get; }
    public string ColumnNameUrl { set; get; }
    public string ColumnNameId { set; get; }
    public string ColumnNameRendering { set; get; }
    public string ColumnNameColor { set; get; }
    public string EventDay { set; get; }
    public string EventMonth { set; get; }
    public string EventYear { set; get; }
    public string EventHour { set; get; }
    public string EventMinutes { set; get; }
    public string CalendarType { set; get; }

    public bool Editable { set; get; }

    public bool EventLimit { set; get; }  // allow "more" link when too many events


    public string BuildCalendar(DataTable dt,string str_DefaultDate, string str_ColumnNameTitle, string str_ColumnNameStartDate)
    {
        string str_CalendarTemplate = this.GetCalendarTemplate("FCalendar.txt", this.str_DivName);
        string str_EventTemplate = this.PHT.ReadFileToString("FCalendarEvents.txt");

        string str_Events = "" , str_TempEventTemplate = "";

        // Bind Events
        foreach (DataRow dr in dt.Select())
        {
            str_TempEventTemplate = str_EventTemplate;

            str_TempEventTemplate = this.ReplaceEventData(str_ColumnNameTitle, "%%EventTitle%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(str_ColumnNameStartDate, "%%StartDate%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.ColumnNameEndDate, "%%EndDate%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.ColumnNameUrl, "%%Url%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.ColumnNameId, "%%id%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.ColumnNameRendering, "%%rendering%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.ColumnNameColor, "%%color%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.EventYear, "%%EventYear%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.EventMonth, "%%EventMonth%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.EventDay, "%%EventDay%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.EventHour, "%%EventHour%%", str_TempEventTemplate, dr);
            str_TempEventTemplate = this.ReplaceEventData(this.EventMinutes, "%%EventMinute%%", str_TempEventTemplate, dr);
            
            str_TempEventTemplate = str_TempEventTemplate.Replace("'%%EventTitle%%'", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("'%%StartDate%%'", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("'%%EndDate%%'", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("'%%Url%%'", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("'%%id%%'", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("'%%rendering%%'", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("'%%color%%'", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("%%EventYear%%", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("%%EventMonth%%", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("%%EventDay%%", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("%%EventHour%%", "null");
            str_TempEventTemplate = str_TempEventTemplate.Replace("%%EventMinute%%", "null");

            if (str_TempEventTemplate.EndsWith(",")) str_TempEventTemplate = str_Events.Remove(str_TempEventTemplate.Length - 1);

            str_Events +=  str_TempEventTemplate +  " ,";
        }

        if (str_Events.EndsWith(",")) str_Events = str_Events.Remove(str_Events.Length - 1);
        

        str_CalendarTemplate = str_CalendarTemplate.Replace("%%maxWidth%%", this.CalendarMaxWidth.ToString());
        str_CalendarTemplate = str_CalendarTemplate.Replace("%%defaultDate%%", str_DefaultDate);
        str_CalendarTemplate = str_CalendarTemplate.Replace("%%editable%%", this.Editable.ToString().ToLower());
        str_CalendarTemplate = str_CalendarTemplate.Replace("%%eventLimit%%", this.EventLimit.ToString().ToLower());
        str_CalendarTemplate = str_CalendarTemplate.Replace("%%events%%", str_Events);
        str_CalendarTemplate = str_CalendarTemplate.Replace("%%CalendarType%%", this.CalendarType);

        return str_CalendarTemplate;
    }

    protected string ReplaceEventData(string str_ColumnName, string str_ReplaceTo ,string str_TempEventTemplate, DataRow dr)
    {
        if (str_ColumnName != null && str_ColumnName != "" && Convert.ToString(dr[str_ColumnName]) != "")
            str_TempEventTemplate = str_TempEventTemplate.Replace(str_ReplaceTo, dr[str_ColumnName].ToString());

        return str_TempEventTemplate;
    }

    public string GetCalendarTemplate(string str_TemplateFileName, string str_DivName)
    {
        string str_ChartTemplate = "";

        //if (this.CalendarTemplateFile != "")
        //{
        //    str_ChartTemplate = this.CalendarTemplateFile;
        //}
        //else
        //{
        //    str_ChartTemplate = PHT.ReadFileToString(str_TemplateFileName);
        //}


        str_ChartTemplate = PHT.ReadFileToString(str_TemplateFileName);
        str_ChartTemplate = str_ChartTemplate.Replace("%%CalendarDivName%%", str_DivName);

        //set chart height
        if (this.CalendarMaxWidth > 0)
            str_ChartTemplate = str_ChartTemplate.Replace("%%max-width%%", "height: " + this.CalendarMaxWidth.ToString() + ",");
        else
            str_ChartTemplate = str_ChartTemplate.Replace("%%max-width%%", "");

        //create html div control
        if (this.CreateDivControl == true)
            str_ChartTemplate = str_ChartTemplate.Replace("%%HtmlDivTag%%", "<div id='" + str_DivName + "'></div>");
        else
            str_ChartTemplate = str_ChartTemplate.Replace("%%HtmlDivTag%%", "");

        str_ChartTemplate = str_ChartTemplate.Replace("%%Title%%", CalendarTitle);

        return str_ChartTemplate;
    }
   
    
    
}