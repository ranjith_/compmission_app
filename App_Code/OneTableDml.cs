﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Summary description for OneTableDml
/// </summary>
public class OneTableDml
{
    DataAccess DA;
    PhTemplate PHT;
    bool bln_AddMode = true;

    public string TableName { get; set; }
    public string KeyValue { get; set; }
    public string PrimaryKeyColumnName { get; set; }
    public string PostData { get; set; }
    public string DivTemplateFileName { get; set; }
    public bool LogKeyRequired { get; set; }
    public bool TimestampCreate { get; set; }
    public bool TimestampModify { get; set; }
    public string[] TableColumns { get; set; }
    public DataTable DropBox { get; set; }
    public string[] CheckBox { get; set; }
    public DataTable RadioBox { get; set; }
    public string RadioBoxOptionTemplate { get; set; }
    public string DropBoxOptionTemplate { get; set; }
    public string[] FileColumns { get; set; }
    public string FileFolder { get; set; }
    public bool AllowDelete { get; set; }


    public OneTableDml(string str_TableName)
    {

        this.DA = new DataAccess();
        this.PHT = new PhTemplate();
        this.TableName = str_TableName;
        this.LogKeyRequired = false;
        this.TimestampCreate = false;
        this.TimestampModify = false;
        this.AllowDelete = false;

        DataTable dt_Drop = new DataTable();
        dt_Drop.Columns.Add("id");
        dt_Drop.Columns.Add("sql");
        this.DropBox = dt_Drop;

        DataTable dt_Radio = new DataTable();
        dt_Radio.Columns.Add("id");
        dt_Radio.Columns.Add("sql");
        this.RadioBox = dt_Radio;
        DataRow dr;

        this.RadioBoxOptionTemplate = " <div class='radio'><label><input name='%%radiogroupname%%' value='%%value%%' %%checked%% type='radio'>%%text%%</label></div>";

        FileFolder = @"/UploadFile/";
        FileColumns = new string[] { };


        switch (str_TableName.ToLower())
        {
            case "transactions":
                DivTemplateFileName = "PopTransactionEdit.txt";
                PrimaryKeyColumnName = "transaction_key";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;
            case "iconfigdetails":
                DivTemplateFileName = "PopConfigDetailsEdit.txt";
                PrimaryKeyColumnName = "ConfigDetailKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                this.AllowDelete = true;
                break;
            case "companylogo":                DivTemplateFileName = "PopCompanyLogo.txt";                PrimaryKeyColumnName = "LogoImageKey";                FileFolder = @"/UploadFile/Logo/";                this.FileColumns = new string[] { "LogoImageName" };                this.TimestampCreate = true;                this.TimestampModify = true;                this.AllowDelete = true;                break;
            case "doctor":
                DivTemplateFileName = "PopAddDoctor.txt";
                PrimaryKeyColumnName = "DoctorKey";
                this.CheckBox = new string[] { "IsActive", "DEAStatus" };
                dr = this.DropBox.NewRow();
                dr["id"] = "Specialist";
                dr["sql"] = "select SpecialityKey value,SpecialityName text from speciality order by SpecialityName";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "GroupKey";
                dr["sql"] = "select GroupKey value,GroupName text from ProviderGroup order by GroupName";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;
            case "doctor_stage":
                DivTemplateFileName = "PopAddDoctor.txt";
                PrimaryKeyColumnName = "DoctorKey";
                this.CheckBox = new string[] { "IsActive", "DEAStatus" };
                dr = this.DropBox.NewRow();
                dr["id"] = "Specialist";
                dr["sql"] = "select SpecialityKey value,SpecialityName text from speciality order by SpecialityName";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "GroupKey";
                dr["sql"] = "select GroupKey value,GroupName text from ProviderGroup order by GroupName";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;
            case "speciality":
                DivTemplateFileName = "PopSpecialityList.txt";
                PrimaryKeyColumnName = "SpecialityKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "subspeciality":
                DivTemplateFileName = "PopSubSpeciality.txt";
                PrimaryKeyColumnName = "SubSpecialityKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "appstatus":
                DivTemplateFileName = "PopAddAppStatus.txt";
                PrimaryKeyColumnName = "StatusKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patient":
                DivTemplateFileName = "PopAddPatient.txt";
                PrimaryKeyColumnName = "PatientKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "MaritalStatus";
                dr["sql"] = "select Value,Text from v_ListMaritalStatus";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "Gender";
                dr["sql"] = "select Value,Text from v_ListGender";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "appointment":
                DivTemplateFileName = "PopAppointment.txt";
                PrimaryKeyColumnName = "AppointmentKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "DoctorKey";
                dr["sql"] = "select DoctorKey as value,DoctorName as text from v_Doctor order by DoctorName";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "HospitalKey";
                dr["sql"] = "select HospitalKey as value,HospitalName as text from Hospital order by HospitalName";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "AppointmentTypeId";
                dr["sql"] = "select Value as value, Text as text from v_ListAppointmentType order by Text";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "SpecialityKey";
                dr["sql"] = "select SpecialityKey as value,SpecialityName as text from Speciality order by SpecialityName";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "PatientKey";
                dr["sql"] = "select PatientKey as value, PatientName as text from v_Patient order by PatientName";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "appointmentstatus":
                DivTemplateFileName = "PopAppointmentStatus.txt";
                PrimaryKeyColumnName = "LogKey";

                dr = this.DropBox.NewRow();
                dr["id"] = "StatusKey";
                //dr["sql"] = "select StatusKey value,StatusName text from AppStatus";
                dr["sql"] = "select ap.StatusKey value,ap.StatusName Text from v_Appointment a join AppStatusAction b on a.StatusKey=b.StatusKey "
                    + " join AppStatus ap on ap.Statuskey=b.ActionKey where AppointmentKey='%%AppointmentKey%%'";
                DropBox.Rows.Add(dr);

                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "doctorservices":
                DivTemplateFileName = "PopDoctorServices.txt";
                PrimaryKeyColumnName = "ServiceKey";

                dr = this.DropBox.NewRow();
                dr["id"] = "ServiceNameKey";
                dr["sql"] = "select ServiceKey value,ServiceName text from Service order by ServiceName";
                DropBox.Rows.Add(dr);

                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "doctoracadamic":
                DivTemplateFileName = "PopDoctorAcadamic.txt";
                PrimaryKeyColumnName = "AcadamicKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "testgrid":
                DivTemplateFileName = "PopTestGrid.txt";
                PrimaryKeyColumnName = "TestGridKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "doctorspeciality":
                DivTemplateFileName = "PopDoctorSpeciality.txt";
                PrimaryKeyColumnName = "DocSpecialityKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "SpecialityKey";
                dr["sql"] = "select SpecialityKey value,SpecialityName text from speciality order by SpecialityName";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "doctortimings":
                DivTemplateFileName = "PopDoctorTimings.txt";
                PrimaryKeyColumnName = "TimingKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "Day";
                dr["sql"] = "select value,Text from v_WeekDays";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "service":
                DivTemplateFileName = "PopService.txt";
                PrimaryKeyColumnName = "ServiceKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "iconfig":
                DivTemplateFileName = "PopIConfig.txt";
                PrimaryKeyColumnName = "ConfigKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            //case "iconfigdetails":
            //    DivTemplateFileName = "PopIConfigView.txt";
            //    PrimaryKeyColumnName = "ConfigDetailKey";
            //    this.TimestampCreate = true;
            //    this.TimestampModify = true;
            //    break;

            case "patientweight":
                DivTemplateFileName = "PopPatientWeight.txt";
                PrimaryKeyColumnName = "WeightKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "MeasureId";
                dr["sql"] = "select * from v_ListWeightMeasurements";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;
            case "patientheight":
                DivTemplateFileName = "PopPatientHeight.txt";
                PrimaryKeyColumnName = "HeightKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "MeasureId";
                dr["sql"] = "select * from v_ListHeightMeasurements";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientbloodglucose":
                DivTemplateFileName = "PopPatientBloodGlucose.txt";
                PrimaryKeyColumnName = "BloodGlocoseKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "MeasureId";
                dr["sql"] = "select * from v_ListBloodGlucoseMeasurement";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "MeasurementTypeId";
                dr["sql"] = "select * from v_ListBloodGlucoseType";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "MeasurementContextId";
                dr["sql"] = "select * from v_ListBloodGlucoseMeasurementcontext";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientbloodpressure":
                DivTemplateFileName = "PopPatientBloodPressure.txt";
                PrimaryKeyColumnName = "BloodPressureKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "HeartbeatTypeId";
                dr["sql"] = "select * from v_ListHeartbeat";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientbodydimension":
                DivTemplateFileName = "PopPatientBodyDimension.txt";
                PrimaryKeyColumnName = "DimensionKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "DimensionTypeId";
                dr["sql"] = "select * from v_ListBodyDimension";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "MeasurementId";
                dr["sql"] = "select * from v_ListHeightMeasurements";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientexercise":
                DivTemplateFileName = "PopPatientExercise.txt";
                PrimaryKeyColumnName = "ExerciseKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "DistanceMeasurement";
                dr["sql"] = "select * from v_ListDistanceMeasurement";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientcondition":
                DivTemplateFileName = "PopPatientCondition.txt";
                PrimaryKeyColumnName = "ConditionKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "StatusId";
                dr["sql"] = "select * from v_ListConditionStatus";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientmedication":
                DivTemplateFileName = "PopPatientMedication.txt";
                PrimaryKeyColumnName = "MedicationKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "StrengthMeasurementId";
                dr["sql"] = "select * from v_ListStrengthMeasurement order by Text";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "DosageMeasurementId";
                dr["sql"] = "select * from v_ListDosageMeasurement order by Text";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "HowTakenId";
                dr["sql"] = "select * from v_ListMedicationHowtaken order by Text";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientallergy":
                DivTemplateFileName = "PopPatientAllergy.txt";
                PrimaryKeyColumnName = "AllergyKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "ReactionId";
                dr["sql"] = "select * from v_ListAllergryReaction order by Text";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "TypeId";
                dr["sql"] = "select * from v_ListAllergryType  order by Text";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "TreatmentId";
                dr["sql"] = "select * from v_ListAllergryTreatmentReaction order by Text";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientimmunization":
                DivTemplateFileName = "PopPatientImmunization.txt";
                PrimaryKeyColumnName = "ImmunizationKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientprocedure":
                DivTemplateFileName = "PopPatientProcedure.txt";
                PrimaryKeyColumnName = "ProcedureKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientfooddrink":
                DivTemplateFileName = "PopPatientFoodDrink.txt";
                PrimaryKeyColumnName = "FoodDrinkKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "MealId";
                dr["sql"] = "select * from v_ListFoodDrink order by Text";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientdocument":
                DivTemplateFileName = "PopPatientDocument.txt";
                PrimaryKeyColumnName = "DocumentKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                this.FileFolder = "/UploadFile/PatientDocuments";
                break;

            case "lab":
                DivTemplateFileName = "PopLab.txt";
                PrimaryKeyColumnName = "LabKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "procedures":
                DivTemplateFileName = "PopProcedure.txt";
                PrimaryKeyColumnName = "ProcedureKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;
            case "laborder":
                DivTemplateFileName = "PopLabOrder.txt";
                PrimaryKeyColumnName = "OrderKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "LabKey";
                dr["sql"] = "select LabKey value,LabName text from Lab order by text ";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;
            case "patientassessment":
                DivTemplateFileName = "PopAssessment.txt";
                PrimaryKeyColumnName = "AssessmentKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "examination":
                DivTemplateFileName = "PopExamination.txt";
                PrimaryKeyColumnName = "ExaminationKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "ExaminationTypeId";
                dr["sql"] = "select *  from v_ListExaminationType order by Text";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "examinationcomp":
                DivTemplateFileName = "PopExaminationComp.txt";
                PrimaryKeyColumnName = "Compkey";
                dr = this.DropBox.NewRow();
                dr["id"] = "CompType";
                dr["sql"] = "select *  from v_ListCompType order by Text";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "examinationcompvalue":
                DivTemplateFileName = "PopExaminationCompValue.txt";
                PrimaryKeyColumnName = "CompValueKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "hospital":
                DivTemplateFileName = "PopHospital.txt";
                PrimaryKeyColumnName = "HospitalKey";
                this.CheckBox = new string[] { "IsActive" };
                dr = this.DropBox.NewRow();
                dr["id"] = "CorporateEntityKey";
                dr["sql"] = "select CorporateEntityKey as Value,LegalBuinessName as Text from Corporate order by Text";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "role":
                DivTemplateFileName = "PopRole.txt";
                PrimaryKeyColumnName = "RoleKey";
                this.CheckBox = new string[] { "IsActive" };
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "menu":
                DivTemplateFileName = "PopMenu.txt";
                PrimaryKeyColumnName = "MenuKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "ParentMenukey";
                dr["sql"] = "select MenuKey Value,MenuName Text from Menu where ParentMenuKey is null";
                DropBox.Rows.Add(dr);
                this.CheckBox = new string[] { "IsActive", "IsNewWindow" };
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "appstatusaction":
                DivTemplateFileName = "PopAppStatusAction.txt";
                PrimaryKeyColumnName = "StatusActionKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "ActionKey";
                dr["sql"] = "select StatusKey value,StatusName text from AppStatus";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "hospitalstaff":
                DivTemplateFileName = "PopStaff.txt";
                PrimaryKeyColumnName = "StaffKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "RoleKey";
                dr["sql"] = "select RoleKey value, RoleName text from Role where RoleKey in('61D21D56-923C-429E-99E0-CA6362220E69')";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "credential":
                DivTemplateFileName = "PopCredential.txt";
                PrimaryKeyColumnName = "CredentialKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientbodytemperature":
                DivTemplateFileName = "PopPatientBodyTemperature.txt";
                PrimaryKeyColumnName = "TemperatureKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "OralMeasurementId";
                dr["sql"] = "select Value as value,Text as text from v_ListTemperatureType";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "AxillaryMeasurementId";
                dr["sql"] = "select Value as value, Text as text from v_ListTemperatureType";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientpulserate":
                DivTemplateFileName = "PopPatientPulserate.txt";
                PrimaryKeyColumnName = "PulseKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientrespirationrate":
                DivTemplateFileName = "PopPatientRespirationRate.txt";
                PrimaryKeyColumnName = "RespirationKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientbmi":
                DivTemplateFileName = "PopPatientBMI.txt";
                PrimaryKeyColumnName = "BMIKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "WeightMeasurementId";
                dr["sql"] = "select * from v_ListWeightMeasurements";
                DropBox.Rows.Add(dr);
                dr = this.DropBox.NewRow();
                dr["id"] = "HeightMeasurementId";
                dr["sql"] = "select * from v_ListHeightMeasurements";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "patientoxygensaturation":
                DivTemplateFileName = "PopPatientOxygenSaturation.txt";
                PrimaryKeyColumnName = "SaturationKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "image":
                DivTemplateFileName = "PopImage.txt";
                PrimaryKeyColumnName = "ImageKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "image_stage":
                DivTemplateFileName = "PopImage.txt";
                PrimaryKeyColumnName = "ImageKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "corporate":
                DivTemplateFileName = "PopCorporateEntity.txt";
                PrimaryKeyColumnName = "CorporateEntityKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "providergroup":
                DivTemplateFileName = "PopProviderGroup.txt";
                PrimaryKeyColumnName = "GroupKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "providerhospital":
                DivTemplateFileName = "PopProviderHospital.txt";
                PrimaryKeyColumnName = "ProviderHospitalKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "HospitalKey";
                dr["sql"] = "select HospitalKey as value,HospitalName as text from Hospital order by HospitalName";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "providertemplate":
                DivTemplateFileName = "PopProviderTemplate.txt";
                PrimaryKeyColumnName = "TemplateKey";
                dr = this.DropBox.NewRow();
                dr["id"] = "TemplateTypeId";
                dr["sql"] = "select * from v_ListTemplateType";
                DropBox.Rows.Add(dr);
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            case "appointmentaddendum":
                DivTemplateFileName = "PopAppointmentAddendum.txt";
                PrimaryKeyColumnName = "AppointmentAddendumKey";
                this.TimestampCreate = true;
                this.TimestampModify = true;
                break;

            default:

                throw new Exception("Table Definition not found in OneTableDml.cs");
        }
    }



}