using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for ApplicationCommonFunction
/// </summary>
public class ApplicationCommonFunction
{
	public ApplicationCommonFunction()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public void set_theme(Page ps)
    {
        ps.Theme =  "Default";
        ps.MasterPageFile = "MasterPage.master";
    }

    public bool convert_to_boolean(string str_value)
    {
        bool bln_value = false;

        if ((str_value.ToLower() == "true") || (str_value.ToLower() == "1"))
        {
            bln_value = true;
        }
        return bln_value; 
    }
    public bool convert_to_control_visible(string str_value)
    {
        bool bln_value = true;

        if ((str_value.ToLower() == "true") || (str_value.ToLower() == "1"))
        {
            bln_value = false ;
        }
        return bln_value;
    }

    public bool convert_to_control_enable(string str_value)
    {
        bool bln_value = true;

        if ((str_value.ToLower() == "true") || (str_value.ToLower() == "1"))
        {
            bln_value = false;
        }
        return bln_value;
    }

    public int convert_to_control_width(string str_value)
    {
        int int_value = 150;

        if (str_value.ToLower() != "" || str_value.ToLower() != "")
        {
            int_value = Convert.ToInt32(str_value);
        }
        return int_value;
    }

    public string get_session_user()
    {
        string str_user_id = "";
        if (HttpContext.Current.Session["user_id"] != null)
        {
            str_user_id = HttpContext.Current.Session["user_id"].ToString();
        }
        return str_user_id;
    }

    public string get_login()
    {
        string str_login_id = "";
        if (HttpContext.Current.Session["user_name"] != null)
        {
            str_login_id = HttpContext.Current.Session["user_name"].ToString();
        }
        return str_login_id;
    }

    public string get_fiscal_year()
    {
        string str_fiscal_year = "";
        if (HttpContext.Current.Session["fiscal_year"] != null)
        {
            str_fiscal_year = HttpContext.Current.Session["fiscal_year"].ToString();
        }
        return str_fiscal_year;
    }

    public string get_current_period()
    {
        string str_current_period = "";
        if (HttpContext.Current.Session["current_period"] != null)
        {
            str_current_period = HttpContext.Current.Session["current_period"].ToString();
        }
        return str_current_period;
    }

    public string get_role()
    {
        string str_role = "";
        if (HttpContext.Current.Session["role"] != null)
        {
            str_role = HttpContext.Current.Session["role"].ToString();
        }
        return str_role;
    }


    public void  valid_session()
    {
        if (get_session_user() == "")
        {
            HttpContext.Current.Session.Clear();  
            HttpContext.Current.Response.Redirect(this.login_url()); 
        }
    }

    public string login_url()
    {
        return "~/Signon.aspx";
    }

    public HtmlTable  get_employee_panel(string str_user_id, string str_emp_id)
    {
        HDDatabase hddb = new HDDatabase ();
        DataTable dt = hddb.GetDataTable("select * from vemployee where emp_id='" + str_emp_id + "'"); 
        DataRow dr = dt.Rows[0];
        HtmlTable ht = new HtmlTable ();
        HtmlTableRow htr;
        HtmlTableCell htc;

        // first row
        htr = new HtmlTableRow();
        htc = new HtmlTableCell();
        htc.Controls.Add(new LiteralControl("<b>Employee ID:</b>"));
        htr.Controls.Add(htc);

        htc = new HtmlTableCell();
        htc.Controls.Add(new LiteralControl(dr["emp_id"].ToString() ));
        htr.Controls.Add(htc);

        htc = new HtmlTableCell();
        htc.Controls.Add(new LiteralControl("<b> Name:</b>"));
        htr.Controls.Add(htc);

        htc = new HtmlTableCell();
        htc.Controls.Add(new LiteralControl(dr["emp_lname"].ToString() + " " + dr["emp_fname"].ToString()));
        htr.Controls.Add(htc);
        ht.Controls.Add(htr);

        // second row
        htr = new HtmlTableRow();
        htc = new HtmlTableCell();
        htc.Controls.Add(new LiteralControl("<b>Sex:</b>"));
        htr.Controls.Add(htc);

        htc = new HtmlTableCell();
        htc.Controls.Add(new LiteralControl(dr["sex"].ToString()));
        htr.Controls.Add(htc);

        htc = new HtmlTableCell();
        htc.Controls.Add(new LiteralControl("<b>Job Code:</b>"));
        htr.Controls.Add(htc);

        htc = new HtmlTableCell();
        htc.Controls.Add(new LiteralControl(dr["job_code_name"].ToString()));
        htr.Controls.Add(htc);
        ht.Controls.Add(htr);

        return ht;
    }

    public string num_format(string str_num)
    {
        string num ="";
        string dec = "";
        int sep = 2;
        if (str_num.IndexOf(".") != -1)
        {
            num = str_num.Substring(0, str_num.IndexOf("."));
            dec = str_num.Substring(str_num.IndexOf(".") + 1);
        }
        else
        {
            num = str_num;
            dec = "00";
        }

        string ret_num ="";
        string anum = num;
        int count =0;
        for (int i = 0; i < str_num.Length; i++)
        {   
            count++;
            if (anum != "")
            {
                ret_num = anum.Substring(anum.Length - 1) + ret_num;
                if (count == 3) { ret_num = "," + ret_num; }
                else if (count > 3)
                {
                    if (count % sep == 1) { ret_num = "," + ret_num; }
                }
                //anum = anum.substr(0,anum.length-1);
                if (anum != "") anum = anum.Substring(0, anum.Length - 1);
            }
        }
        if (ret_num!="" && ret_num.Substring(0,1)  == ",") { ret_num = ret_num.Substring (1, ret_num.Length - 1); }
    
        return  ret_num  + "." + dec;
    }

    public string replace_variable_with_value(string str_var)
    {
        ArrayList al_var = new ArrayList();

        al_var.Add("##user_id##");
        al_var.Add("##current_year##");
        al_var.Add("##current_period##");
        al_var.Add("##current_date##");

        foreach (string id in al_var)
        {
            str_var = str_var.Replace(id, this.get_variable_value(id)); 
        }

        return str_var;
    }


    public string get_variable_value(string var_id)
    {
        string var_value="";
        switch (var_id)
        {
            case "##user_id##":
                var_value = this.get_session_user(); 
                break;
            case "##current_year##":
                var_value = this.get_fiscal_year(); 
                break;
            case "##current_period##":
                var_value = this.get_current_period(); 
                break;
            case "##current_date##":
                var_value = DateTime.Now.ToShortDateString();
                break;
            case "##guid##":
                var_value = Guid.NewGuid().ToString();
                break;
        }

        return var_value;
    }

    //    function num_format(obj)
    //{
    //    var prec=2;
    //    var sep=2;
    //    var sym='Rs ';
    //    var value = obj.value;
    //    var num = 0;
    //    var dec = 0;
    //    value = Number(value).toFixed(2)
    //    if (value.indexOf('.') != -1)
    //    {
    //        num = value.substr(0,value.indexOf('.'));
    //        dec = value.substr(value.indexOf('.')+1);
    //    }

    //    var ret_num ='';        
    //    var anum = num;
    //    var count =0;
    //    for (var i=0; i< num.length ; i++)
    //    {   
    //        count++;
    //        ret_num = anum.substr(anum.length-1,anum.length) + ret_num;
    //        if (count == 3 ) { ret_num = ',' + ret_num; }
    //        else if (count > 3  )
    //        {
    //            if (count%sep == 1 ) { ret_num = ',' + ret_num; }
    //        }
    //        //document.write ('count: ' + count + ' || ' +  anum.substr(anum.length-1,anum.length) + ' </br> ' );
    //        //document.write ( ret_num + ' </br> ' );
    //        anum = anum.substr(0,anum.length-1);
    //    }
    //    if (ret_num.substr(0,1) ==',') { ret_num = ret_num.substr(1,ret_num.length-1); }
    //    obj.value = ret_num + '.' + dec;        

    //}
    //function num_deformat(obj)
    //{
    //    var value = obj.value;
    //    value = value.replace(/,/gi,'');
    //    obj.value = value;
    //}


    public string GenerateWordsinRs(decimal dbl_Amount)
    {
        decimal numberrs = dbl_Amount;
        System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-IN");
        string aaa = String.Format("{0:#,##0.##}", numberrs);
        aaa = aaa + " " + ci.NumberFormat.CurrencySymbol.ToString();


        string input = dbl_Amount.ToString();
        string a = "";
        string b = "";

        // take decimal part of input. convert it to word. add it at the end of method.
        string decimals = "";

        if (input.Contains("."))
        {
            decimals = input.Substring(input.IndexOf(".") + 1);
            // remove decimal part from input
            input = input.Remove(input.IndexOf("."));

        }
        string strWords = this.NumbersToWords(Convert.ToInt32(input));

        if (!dbl_Amount.ToString().Contains("."))
        {
            a = strWords + " Rupees Only";
        }
        else
        {
            a = strWords + " Rupees";
        }

        if (decimals.Length > 0)
        {
            // if there is any decimal part convert it to words and add it to strWords.
            string strwords2 = this.NumbersToWords(Convert.ToInt32(decimals));
            b = " and " + strwords2 + " Paisa Only ";
        }

        return a + b;
    }

    private string NumbersToWords(int inputNumber)
    {
        int inputNo = inputNumber;

        if (inputNo == 0)
            return "Zero";

        int[] numbers = new int[4];
        int first = 0;
        int u, h, t;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        if (inputNo < 0)
        {
            sb.Append("Minus ");
            inputNo = -inputNo;
        }

        string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
        string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
        string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
        string[] words3 = { "Thousand ", "Lakh ", "Crore " };

        numbers[0] = inputNo % 1000; // units
        numbers[1] = inputNo / 1000;
        numbers[2] = inputNo / 100000;
        numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
        numbers[3] = inputNo / 10000000; // crores
        numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

        for (int i = 3; i > 0; i--)
        {
            if (numbers[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (numbers[i] == 0) continue;
            u = numbers[i] % 10; // ones
            t = numbers[i] / 10;
            h = numbers[i] / 100; // hundreds
            t = t - 10 * h; // tens
            if (h > 0) sb.Append(words0[h] + "Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i == 0) sb.Append("");
                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }
            if (i != 0) sb.Append(words3[i - 1]);
        }
        return sb.ToString().TrimEnd();
    }


}
