using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;

    //DataAccess is default to Microsoft SQL Server
public class DataAccess : DataAccessF
    {
        public DataAccess()
        {
            this.Intialize();
        }

        public DataAccess(string connection_string)
        {
            this.ConnectionString = connection_string;
            this.Intialize();
        }

        protected override void Intialize()
        {
            this.DbCon = new SqlConnection();
            SqlTransaction sql_dbtran = null;
            this.DbTran = sql_dbtran;
            this.DbComm = new SqlCommand();
            this.DbAdap = new SqlDataAdapter();

            SqlDataReader sql_dbread = null;
            this.DbRead = sql_dbread;
        }


    public DataTable  ReadFromCsvFile(string str_FilePath, string str_FileName, string str_TempTableName)
    {
        // Note:  Filepath must be SQL Sever accessable file path

        /*
          select [claim id], [Billed To]  into #t1
        from openrowset('MSDASQL','Driver={Microsoft Access Text Driver (*.txt, *.csv)}; DBQ=C:\OWL\C_4946_1\' ,
	    'select * from "owl_export-NoResponse.CSV"') a 
         */

        string str_CsvSql  = "select [claim id], [Billed To]  into #t1 from openrowset('MSDASQL','Driver={Microsoft Access Text Driver (*.txt, *.csv)}; DBQ=C:\\OWL\\C_4946_1\' ,'select * from \"owl_export-NoResponse.CSV\"') a ";

        str_CsvSql = "select * <temptablename> from openrowset('MSDASQL','Driver={Microsoft Access Text Driver (*.txt, *.csv)}; DBQ=<filepath>' ,'select * from \"<filename>\"') a ";
        str_CsvSql = str_CsvSql.Replace("<filepath>", str_FilePath);
        str_CsvSql = str_CsvSql.Replace("<filename>", str_FileName);

        if (str_TempTableName=="")
            str_CsvSql = str_CsvSql.Replace("<temptablename>", "");
        else
            str_CsvSql = str_CsvSql.Replace("<temptablename>", " into [" + str_TempTableName + "]");

        DataTable dt = this.GetDataTable(str_CsvSql);

        return dt;

    }

}
