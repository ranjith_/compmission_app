﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

/// <summary>
/// Summary description for Doctor
/// </summary>
public class Doctor
{
    DataAccess DA = null;
    string str_DoctorKey = "";
    PhTemplate PHT;

    public Doctor(DataAccess DA, string str_DoctorKey)
    {
        this.DA = DA;
        this.str_DoctorKey = str_DoctorKey;
        this.PHT = new PhTemplate();
    }

    public void LoadDoctorProfileSummaryWidget(PlaceHolder ph, bool bln_Access)
    {
        string str_Sql = "select i.ImageKey,case when i.Filepath= '' then '../Images/DoctorAvatar.png' WHEN i.Filepath IS NULL then '../Images/DoctorAvatar.png' else i.FilePath end as FilePath,i.UserKey,DoctorKey,RegistrationId,DoctorName,Gender,DOB,Experience,Qualification,AddressLine1,AddressLine2,City,State,Country,ZipCode,ConsultingFees,d.CreatedOn,"
            + "d.CreatedBy,Awards,DoctorDescription,Specialist,SpecialityName,Facebook,Twitter,Linkedin,IsActive,DispColor,GroupName,LicensureState,DEAStatus,CredentialKey from v_Doctor d left outer join Image I on d.DoctorKey = I.UserKey where doctorkey=@doctorkey;"
            + "select AcadamicKey,Degree,datename(m,CompletionDate)+' '+cast(datepart(yyyy,CompletionDate) as varchar) CompletionDate from DoctorAcadamic where doctorkey=@doctorkey;"
            + "select a.*,b.SpecialityName from DoctorSpeciality a join Speciality b on a.SpecialityKey=b.SpecialityKey where doctorkey=@doctorkey;";

        SqlCommand sc = new SqlCommand(str_Sql);
        sc.Parameters.AddWithValue("@doctorkey", this.str_DoctorKey);
        DataSet ds = this.DA.GetDataSet(sc);
        ds.Tables[0].TableName = "profile";
        ds.Tables[1].TableName = "academic";
        ds.Tables[2].TableName = "speciality";

        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorProfileSummary.txt"), str_HTML = "", str_ReplaceStr = "";

        //Academic / Training
        string str_AcadamicTemplate = this.PHT.ReadFileToString("WidgetDoctorProfileAcadamicTr.txt");
        str_HTML = "";
        string str_EditTrTemplateAcadamic = "<a data-toggle=\"modal\" onclick=\"OpenPopUp('Modify Academic/Training', 'DoctorAcadamic', '%%AcadamicKey%%', 'DoctorKey:%%DoctorKey%%', '', '');\">%%Degree%%</a>";
        string str_TrHTMLAcadamic = "";
        foreach (DataRow dr in ds.Tables[1].Rows)
        {
            str_ReplaceStr = str_AcadamicTemplate;
            if (bln_Access)
            {
                str_TrHTMLAcadamic = this.PHT.ReplaceVariableWithValue(dr, str_EditTrTemplateAcadamic);
                str_ReplaceStr = str_ReplaceStr.Replace("%%Degree%%", str_TrHTMLAcadamic);
            }


            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ReplaceStr);
        }
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%acadamicdetails%%", str_HTML);

        //Specialization
        string str_SpecializationTemplate = this.PHT.ReadFileToString("WidgetDoctorProfileSpecializationTr.txt");
        str_HTML = "";
        string str_EditTrTemplate = "<a data-toggle=\"modal\" onclick=\"OpenPopUp('Modify Specialization', 'DoctorSpeciality', '%%DocSpecialityKey%%', 'DoctorKey:%%DoctorKey%%', '', '');\">%%SpecialityName%%</a> ";
        string str_TrHTML = "";
        foreach (DataRow dr in ds.Tables[2].Rows)
        {
            str_ReplaceStr = str_SpecializationTemplate;
            if (bln_Access)
            {
                str_TrHTML = this.PHT.ReplaceVariableWithValue(dr, str_EditTrTemplate);
                str_ReplaceStr = str_ReplaceStr.Replace("%%SpecialityName%%", str_TrHTML);
            }
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ReplaceStr);
        }
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%DoctorKey%%", this.str_DoctorKey);
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%specializationdetail%%", str_HTML);

        //Doctor Details
        str_HTML = "";
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_WidgetTemplate);
        }
        if (bln_Access == true)
            str_HTML = str_HTML.Replace("%%controldisplay%%", "block");
        else
            str_HTML = str_HTML.Replace("%%controldisplay%%", "none");

        string str_Facebook = ds.Tables[0].Rows[0]["Facebook"].ToString();
        if (str_Facebook != "")
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayFacebook%%", "block");
        }
        else
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayFacebook%%", "none");
        }
        string str_Twitter = ds.Tables[0].Rows[0]["Twitter"].ToString();
        if (str_Twitter != "")
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayTwitter%%", "block");
        }
        else
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayTwitter%%", "none");
        }
        string str_Linkedin = ds.Tables[0].Rows[0]["Linkedin"].ToString();
        if (str_Linkedin != "")
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayLinkedin%%", "block");
        }
        else
        {
            str_HTML = str_HTML.Replace("%%ControlDisplayLinkedin%%", "none");
        }

        //string str_Status = ds.Tables[0].Rows[0]["IsActive"].ToString();
        //if (str_Status == "ACTIVE")
        //{
        //    str_HTML = str_HTML.Replace("%%DispColor%%", "success");
        //}
        //else
        //{
        //    str_HTML = str_HTML.Replace("%%DispColor%%", "danger");
        //}
        ph.Controls.Add(new LiteralControl(str_HTML));

        //this.PHT.LoadGridItem(ds, ph, "WidgetDoctorProfileSummary.txt", "profile");
    }

    //Services
    public void LoadDoctorServicesWidget(PlaceHolder ph, bool bln_Access)
    {

        //string str_Sql = "select * from doctor where doctorkey=@doctorkey;";
        string str_Sql = "select b.ServiceName,a.ServiceKey,a.ServiceNameKey from DoctorServices a join Service b on a.ServiceNameKey=b.ServiceKey where DoctorKey=@doctorkey";
        SqlCommand sc = new SqlCommand(str_Sql);
        sc.Parameters.AddWithValue("@doctorkey", this.str_DoctorKey);
        DataSet ds = this.DA.GetDataSet(sc);
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorProfileService.txt"), str_HTML = "", str_ReplaceStr = "";
        string str_ChildTemplate = this.PHT.ReadFileToString("WidgetDoctorProfileServiceTr.txt");
        string str_EditTrTemplate = "<a data-toggle=\"modal\" onclick=\"OpenPopUp('Modify Services','DoctorServices','%%ServiceKey%%','DoctorKey:%%DoctorKey%%','','');\">%%ServiceName%%</a>";
        string str_TrHTML = "";
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_ReplaceStr = str_ChildTemplate;
            if (bln_Access)
            {
                str_TrHTML = this.PHT.ReplaceVariableWithValue(dr, str_EditTrTemplate);
                str_ReplaceStr = str_ReplaceStr.Replace("%%ServiceName%%", str_TrHTML);
            }
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ReplaceStr);
        }
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%DoctorKey%%", this.str_DoctorKey);
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);
        if (bln_Access == true)
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%controldisplay%%", "block");
        else
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%controldisplay%%", "none");
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
    }

    //Consulting Time
    public void LoadDoctorTimeTableWidget(PlaceHolder ph, bool bln_Access)
    {
        //string str_Sql = "select * from doctor where doctorkey=@doctorkey;";
        string str_Sql = "select TimingKey,DoctorKey,Day,FromTime,ToTime,Text from DoctorTimings a join v_WeekDays b on a.Day=b.value where doctorkey=@doctorkey order by Day;";
        SqlCommand sc = new SqlCommand(str_Sql);
        sc.Parameters.AddWithValue("@doctorkey", this.str_DoctorKey);
        DataSet ds = this.DA.GetDataSet(sc);
        string str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorProfileTimeTable.txt"), str_HTML = "", str_ReplaceStr = "";
        string str_ChildTemplate = this.PHT.ReadFileToString("WidgetDoctorProfileTimeTableTr.txt");
        string str_EditTrTemplate = "<a class=\"pull-left\" data-toggle=\"modal\" onclick=\"OpenPopUp('Modify Consulting Time', 'DoctorTimings', '%%TimingKey%%', 'DoctorKey:%%DoctorKey%%', '', '')\"><i class=\"fa fa-calendar info\"></i>&nbsp;%%Text%%</a>";
        string str_TrHTML = "";
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_ReplaceStr = str_ChildTemplate;
            if (bln_Access)
            {
                str_TrHTML = this.PHT.ReplaceVariableWithValue(dr, str_EditTrTemplate);
                str_ReplaceStr = str_ReplaceStr.Replace("%%Text%%", str_TrHTML);
            }
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ReplaceStr);
        }
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%DoctorKey%%", this.str_DoctorKey);
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);
        if (bln_Access == true)
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%controldisplay%%", "block");
        else
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%controldisplay%%", "none");
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
        //ds.Tables[0].TableName = "time";
        //this.PHT.LoadGridItem(ds, ph, "WidgetDoctorProfileTimeTable.txt", "time");
    }


    public void LoadDoctorAppointmentWidget(PlaceHolder ph, AppointmentType At, int AppointmentCount)
    {
        string str_WidgetTemplate = "", str_ChildTemplate = "", str_HTML = "", str_Sql = "";

        if (AppointmentType.upcoming == At)//Upcoming Schedules
        {
            str_Sql = "select top 5 *,case when StatusKey='FCC5E11E-4844-4577-AA72-C953D4F8E167' then 'enabled' else 'disabled' End DisplayStyle from v_Appointment "
                + "where doctorkey = @DoctorKey and AppDate >= Convert(date, GETUTCDATE()) "
                + " and StatusKey <> 'ECA92B94-5214-44AE-8F60-56DAF180A829' order by Appdate asc";
            str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointment.txt");
            str_ChildTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentTr.txt");
        }
        else if (AppointmentType.completed == At)//Completed Schedules
        {
            str_Sql = "select top 5 * from v_Appointment where doctorkey=@doctorkey and StatusKey='ECA92B94-5214-44AE-8F60-56DAF180A829' and AppDate <=Convert(date,GETUTCDATE()) order by AppDate desc";
            str_WidgetTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentCompleted.txt");
            str_ChildTemplate = this.PHT.ReadFileToString("WidgetDoctorAppointmentCompletedTr.txt");
        }
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@DoctorKey", this.str_DoctorKey);
        DataSet ds = this.DA.GetDataSet(cmd);
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_ChildTemplate);
        }
        str_WidgetTemplate = str_WidgetTemplate.Replace("%%ChildContent%%", str_HTML);

        if (ds.Tables[0].Rows.Count > 0)//More Appointments Link 
        {
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%controldisplay%%", "block");
        }
        else
        {
            str_WidgetTemplate = str_WidgetTemplate.Replace("%%controldisplay%%", "none");
        }
        ph.Controls.Add(new LiteralControl(str_WidgetTemplate));
    }

    public enum AppointmentType
    {
        upcoming,
        completed,
        all,
        cancelled
    };


}