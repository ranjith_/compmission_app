﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.UI;

/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    SessionCustom SC;
    DataAccess DA;
    public Common()
    {
        SC = new SessionCustom();
        DA = new DataAccess();
        //
        // TODO: Add constructor logic here
        //
    }

    public void LoadDropDown(DropDownList ddl, DataTable dt, string str_ColumnText, string str_ColumnValue, bool bln_AddEmptyRow, string str_EmptyRowText)
    {
        ddl.DataSource = dt;
        ddl.DataValueField = str_ColumnValue;
        ddl.DataTextField = str_ColumnText;
        ddl.DataBind();
        if (bln_AddEmptyRow)
        {
            ddl.Items.Add(new ListItem(str_EmptyRowText, ""));
            ddl.SelectedValue = "";
        }
    }

    protected string[] getColorCodes(int inCount)
    {
        string[] stra = new string[inCount];
        return stra;
    }

    public string GetQueryStringValue(string str_Param_Name)
    {
        string str_Param_Value = "";
        if (HttpContext.Current.Request.QueryString[str_Param_Name] != null && HttpContext.Current.Request.QueryString[str_Param_Name] != "")
            str_Param_Value = HttpContext.Current.Request.QueryString[str_Param_Name];

        return str_Param_Value;
    }



    public void LoadYearToDropdown(System.Web.UI.WebControls.DropDownList ddl_year)
    {
        //ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2011", "2011"));
        //ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2012", "2012"));
        // ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2013", "2013"));
        //ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2014", "2014"));
        //ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2015", "2015"));
        //ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2016", "2016"));
        //ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2017", "2017"));
        //ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2018", "2018"));
        //ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem("2019", "2019"));
        int CurrentYear = DateTime.Today.Year;
        ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem(Convert.ToString(CurrentYear - 3), Convert.ToString(CurrentYear - 3)));
        ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem(Convert.ToString(CurrentYear - 2), Convert.ToString(CurrentYear - 2)));
        ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem(Convert.ToString(CurrentYear - 1), Convert.ToString(CurrentYear - 1)));
        ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem(Convert.ToString(CurrentYear), Convert.ToString(CurrentYear)));
        ddl_year.Items.Add(new System.Web.UI.WebControls.ListItem(Convert.ToString(CurrentYear + 1), Convert.ToString(CurrentYear + 1)));

        ddl_year.SelectedValue = CurrentYear.ToString();

    }


    public void LoadMonthToDropdown(System.Web.UI.WebControls.DropDownList ddl_month)
    {
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("January", "1"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("February", "2"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("March", "3"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("April", "4"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("May", "5"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("June", "6"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("July", "7"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("August", "8"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("September", "9"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("October", "10"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("November", "11"));
        ddl_month.Items.Add(new System.Web.UI.WebControls.ListItem("December", "12"));
        ddl_month.SelectedValue = DateTime.Now.Month.ToString();
    }




    public string GetEmployeeCodeFromKey(string str_EmployeeKey)
    {
        string str_Sql = "Select EmployeeCode from Employee where EmployeeKey=@EmployeeKey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@EmployeeKey", str_EmployeeKey);
        DataTable dt = DA.GetDataTable(cmd);
        return dt.Rows[0][0].ToString();
    }

    public string CheckRequestCutOffDate(string str_TypeKey, string str_Date)
    {
        string str_ReturnMsg = "";
        string str_Sql = "select Convert(nvarchar(12),ApplyFrom,106)ApplyFrom from RequestType where RequestTypeKey=@RequestTypeKey and ApplyFrom>@Date";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@RequestTypeKey", str_TypeKey);
        cmd.Parameters.AddWithValue("@Date", str_Date);
        DataTable dt = DA.GetDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            str_ReturnMsg = "you can apply request from : " + dt.Rows[0][0].ToString();
        }
        return str_ReturnMsg;
    }

    public void Alert(string message)
    {
        // Cleans the message to allow single quotation marks 
        string cleanMessage = message.Replace("'", "\\'");
        string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');</script>";

        // Gets the executing web page 
        Page page = HttpContext.Current.CurrentHandler as Page;

        // Checks if the handler is a Page and that the script isn't allready on the Page 
        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
        {
            //ScriptManager.RegisterStartupScript(page, page.GetType(), "Alert", script, false);     
            page.ClientScript.RegisterStartupScript(page.GetType(), "alert", script);

        }
    }

    public void AlertNotify(string title,string msg,string cssClass)
    {
        // Cleans the message to allow single quotation marks 
        string cleanMessage = msg.Replace("'", "\\'");
        string cleanTitle = title.Replace("'", "\\'");
        string script = "<script type=\"text/javascript\">alertNotify('" + cleanTitle + "','" + cleanMessage + "','" + cssClass + "');</script>";

        // Gets the executing web page 
        Page page = HttpContext.Current.CurrentHandler as Page;

        // Checks if the handler is a Page and that the script isn't allready on the Page 
        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
        {
            //ScriptManager.RegisterStartupScript(page, page.GetType(), "Alert", script, false);     
            page.ClientScript.RegisterStartupScript(page.GetType(), "alert", script);

        }
    }
    public string GetDashboardUrl()
    {
        SessionCustom SC = new SessionCustom();
        if (SC.IsDoctor())
            return "~/Web/DoctorDashbord.aspx";
        else if (SC.IsHospitalAdmin())
            return "~/Web/HospitalDashboard.aspx";
        else if (SC.IsOperationAdmin() || SC.IsOperationManager() || SC.IsOperationSupport())
            return "~/Web/OperationDashboard.aspx";
        else if (SC.IsOperationManager())
            return "~/Web/OperationDashboard.aspx";
        else if (SC.IsSuperAdmin())
            return "~/Web/OperationDashboard.aspx";
        else
            return "~/Web/Dashboard.aspx";
    }

    public void LoadSessionVariables(string str_UserKey)
    {
        string Str_TypeId = "", str_UserRoles = "";
        string str_Sql = " exec p_GetUserDetails @Userkey";
        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@Userkey", str_UserKey);
        DataSet ds_doc = this.DA.GetDataSet(cmd);
        if (ds_doc.Tables[0].Rows.Count > 0)
        {
            //HttpContext.Current.Session["TypeId"] = ds_doc.Tables[0].Rows[0]["TypeId"].ToString();
       //     HttpContext.Current.Session["UserName"] = ds_doc.Tables[0].Rows[0]["UserName"].ToString();
           // HttpContext.Current.Session["HospitalKey"] = ds_doc.Tables[0].Rows[0]["HospitalKey"].ToString();
           // HttpContext.Current.Session["HospitalName"] = ds_doc.Tables[0].Rows[0]["HospitalName"].ToString();
           // HttpContext.Current.Session["ImageKey"] = ds_doc.Tables[0].Rows[0]["ImageKey"].ToString();
           // HttpContext.Current.Session["FilePath"] = ds_doc.Tables[0].Rows[0]["FilePath"].ToString();
           // Str_TypeId = ds_doc.Tables[0].Rows[0]["TypeId"].ToString();


            //if (ds_doc.Tables[3].Rows.Count > 0)
            //{
            //    HttpContext.Current.Session["ImageKey"] = ds_doc.Tables[3].Rows[0]["FilePath"].ToString();
            //}

            //License ExpiryOn check for Hospital
            if (Str_TypeId == "3")
            {
                string str_ExpiryOn = ds_doc.Tables[0].Rows[0]["LicenseExpiryOn"].ToString();
                HttpContext.Current.Session["LicenseExpiryOn"] = str_ExpiryOn.ToString();
                DateTime oDate = Convert.ToDateTime(str_ExpiryOn);
                if (oDate < DateTime.Now)
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert Message", "alert('Your License is Expired ,Please Contact Admin')", true);
                    return;
                }
            }
        }
        if (ds_doc.Tables[0].Rows.Count > 0)
        {
            HttpContext.Current.Session["Role"] = ds_doc.Tables[2].Rows[0]["Role"].ToString();
            HttpContext.Current.Session["RoleKey"] = ds_doc.Tables[1].Rows[0]["RoleKey"].ToString();
            HttpContext.Current.Session["UserName"] = ds_doc.Tables[2].Rows[0]["Email"].ToString();

            HttpContext.Current.Session["Name"] = ds_doc.Tables[2].Rows[0]["FirstName"].ToString();

            foreach (DataRow dr in ds_doc.Tables[1].Rows)
            {
                str_UserRoles = str_UserRoles + "'" + dr["RoleKey"].ToString() + "',";
            }
            str_UserRoles = str_UserRoles.TrimEnd(',');
        }

        if (ds_doc.Tables[3].Rows.Count > 0)
        {
            //HttpContext.Current.Session["CompanyName"] = ds_doc.Tables[3].Rows[0]["CompanyName"].ToString();
            //HttpContext.Current.Session["CompanyEmail"] = ds_doc.Tables[3].Rows[0]["Email"].ToString();
            HttpContext.Current.Session["CompanyLogo"] = ds_doc.Tables[3].Rows[0]["LogoImageName"].ToString();
            //HttpContext.Current.Session["CompanyWebsite"] = ds_doc.Tables[3].Rows[0]["Website"].ToString();

        }
        HttpContext.Current.Session["Menu"] = ds_doc.Tables[1];


    }

    public void GlobalErrorHandler(Exception ex)
    {
        string ErrorSource = ex.Source; 
        string ErrorMessage = ex.Message;
        string StackTrace =  ex.StackTrace;

            //GlobalErrorLog (ErrorLogKey ,ErrorSource ,ErrorMessage ,StackTrace ,CreatedOn ,CreatedBy ,ModifiedOn ,ModifiedBy)
        string ErrorLogKey = Guid.NewGuid().ToString();

            string str_Sql = "insert into GlobalErrorLog (ErrorLogKey ,ErrorSource ,ErrorMessage ,StackTrace , CreatedOn, CreatedBy)"
                                + "select  @ErrorLogKey ,@ErrorSource ,@ErrorMessage ,@StackTrace , @CreatedOn, @CreatedBy";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@ErrorLogKey", ErrorLogKey);
            cmd.Parameters.AddWithValue("@ErrorSource", ErrorSource);
            cmd.Parameters.AddWithValue("@ErrorMessage", ErrorMessage);
            cmd.Parameters.AddWithValue("@StackTrace", StackTrace);
            cmd.Parameters.AddWithValue("@CreatedBy", SC.UserKey);
            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now.ToString());


            this.DA.ExecuteNonQuery(cmd);
            HttpContext.Current.Response.Redirect("GlobalError.aspx");
       
       
    }

}
