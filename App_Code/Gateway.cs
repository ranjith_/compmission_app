﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// Summary description for Gateway
/// </summary>
public class Gateway
{
    DataAccess DA;
    public Gateway()
    {
        this.DA = new DataAccess();
    }
    public string GetVisitorIPAddress(bool GetLan = false)
    {
        string stringHostName = "";
        string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (String.IsNullOrEmpty(visitorIPAddress))
            visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        if (string.IsNullOrEmpty(visitorIPAddress))
            visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

        if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
        {
            GetLan = true;
            visitorIPAddress = string.Empty;
        }

        if (GetLan && string.IsNullOrEmpty(visitorIPAddress))
        {
            //This is for Local(LAN) Connected ID Address
            stringHostName = Dns.GetHostName();
            //Get Ip Host Entry
            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //Get Ip Address From The Ip Host Entry Address List
            IPAddress[] arrIpAddress = ipHostEntries.AddressList;

            try
            {
                visitorIPAddress = arrIpAddress[arrIpAddress.Length - 1].ToString();
            }
            catch
            {
                try
                {
                    visitorIPAddress = arrIpAddress[0].ToString();
                }
                catch
                {
                    try
                    {
                        arrIpAddress = Dns.GetHostAddresses(stringHostName);
                        visitorIPAddress = arrIpAddress[0].ToString();
                    }
                    catch
                    {
                        visitorIPAddress = "127.0.0.1";
                    }
                }
            }

        }
        return visitorIPAddress;
    }

    public bool CheckGatewayIp(string str_LoginUser, bool SuccessLogin)
    {
        string str_IPAddress = this.GetVisitorIPAddress();
        bool bln_Return = false;

        string str_Sql = " select * from GatewayIp where HostIp=@HostIp";

        SqlCommand cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@HostIp", str_IPAddress);
        DataTable dt = this.DA.GetDataTable(cmd);
        if (dt.Rows.Count > 0)
            bln_Return = true;

        str_Sql = " insert into Gatewaylog(LogKey,HostIp,UserName,LoginSuccess,GatewaySuccess,CreatedOn) select NEWID(), @HostIp,@UserName,@LoginSuccess,@GatewaySuccess,GETDATE();";
        cmd = new SqlCommand(str_Sql);
        cmd.Parameters.AddWithValue("@HostIp", str_IPAddress);
        cmd.Parameters.AddWithValue("@UserName", str_LoginUser);
        cmd.Parameters.AddWithValue("@LoginSuccess", SuccessLogin);
        cmd.Parameters.AddWithValue("@GatewaySuccess", bln_Return);
        this.DA.ExecuteNonQuery(cmd);

        return bln_Return;
    }

}