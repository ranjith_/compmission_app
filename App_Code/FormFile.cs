﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for FormFile
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class FormFile : System.Web.Services.WebService {

    public FormFile () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    [WebMethod]
    public byte[] GetFile(string filename)
    {
        BinaryReader binReader = new
 BinaryReader(File.Open(Server.MapPath(filename), FileMode.Open,
 FileAccess.Read));
        binReader.BaseStream.Position = 0;
        byte[] binFile =
 binReader.ReadBytes(Convert.ToInt32(binReader.BaseStream.Length));
        binReader.Close();
        return binFile;
    }

    [WebMethod]
    public void PutFile(byte[] buffer, string filename)
    {
        BinaryWriter binWriter = new
 BinaryWriter(File.Open(Server.MapPath(filename), FileMode.CreateNew,
 FileAccess.ReadWrite));
        binWriter.Write(buffer);
        binWriter.Close();
    }
    
}
