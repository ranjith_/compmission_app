﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for FormSecurity
/// </summary>
public class FormSecurity
{
    DataAccess DA;
    public FormSecurity(HttpContext context)
    {
        HttpRequest req = context.Request;
        this.DA = new DataAccess();
    }
    public FormSecurity()
    {
        this.DA = new DataAccess();
    }

    public bool IsValidUser(string str_UserName, string str_UserPassword)
    {
        string str_Sql = "select * from e_Account where UserName=@UserName and Password=@Password and IsActive=1";
        SqlCommand sc = new SqlCommand(str_Sql);
        sc.Parameters.AddWithValue("@UserName", str_UserName);
        sc.Parameters.AddWithValue("@Password", str_UserPassword);
        DataTable dt = DA.GetDataTable(sc);
        if (dt.Rows.Count > 0) return true;
        else return false;
    }

    public bool IsValidToken(string str_Token)
    {
        string str_Sql = "select * from  UserAuth where AuthToken=@AuthToken and expirydate >= getutcdate()";
        SqlCommand sc = new SqlCommand(str_Sql);
        sc.Parameters.AddWithValue("@AuthToken", str_Token);
        DataTable dt = DA.GetDataTable(sc);
        if (dt.Rows.Count > 0) return true;
        else return false;
    }

    public string getTokenXml(string str_UserName, string str_Message, bool bln_Success)
    {
        string str_AuthToken = Guid.NewGuid().ToString();
        DateTime dt_ExpiryDate = DateTime.UtcNow.AddHours(1);
        if (bln_Success)  //insert token to database
        {
            //string str_Sql = "Delete from UserAuth where UserName=@UserName; "
            //+ " Insert into UserAuth (AuthToken, Username, ExpiryDate) Select @AuthToken, @UserName, @ExpiryDate";

            string str_Sql = "Insert into UserAuth (AuthToken, Username, ExpiryDate) Select @AuthToken, @UserName, @ExpiryDate";
            SqlCommand sc = new SqlCommand(str_Sql);
            sc.Parameters.AddWithValue("@UserName", str_UserName);
            sc.Parameters.AddWithValue("@AuthToken", str_AuthToken);
            sc.Parameters.AddWithValue("@ExpiryDate", dt_ExpiryDate);
            this.DA.ExecuteNonQuery(sc);
        }


        DataTable dt = new DataTable();
        dt.Columns.Add("AuthStatus", typeof(bool));
        dt.Columns.Add("AuthMessage");
        dt.Columns.Add("AuthToken");
        dt.Columns.Add("AuthNotes");
        dt.Columns.Add("AuthExpiryDate", typeof(DateTime));
        DataRow dr = dt.NewRow();
        dr["AuthStatus"] = bln_Success;
        dr["AuthMessage"] = str_Message + "; Auth Expiry date would be UTC date";
        dr["AuthExpiryDate"] = DateTime.UtcNow.AddHours(1);
        if (bln_Success)
            dr["AuthToken"] = str_AuthToken;
        else
            dr["AuthToken"] = "";

        dt.Rows.Add(dr);
        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        return ds.GetXml();
    }
}