﻿function assignRecipeientsInsert() {
    var assignRecipient = $('#DdAssignRecipient').val();
    var commisssion_key = $('#hdn_CommissionKey').val();
    updateCommissionRecipientSelect(commisssion_key, assignRecipient);
    if (assignRecipient == 1) {
        insertAssignRecipients(commisssion_key);
        location.replace('../Web/CommissionView.aspx?CommissionKey=' + commisssion_key + '&location=#div_RecipientView&page=Recipients&update=success');
    }
    else if (assignRecipient == 2) {

        let inserted = insertRecipientCondition(commisssion_key);
        if (inserted == true) {
            location.replace('../Web/CommissionView.aspx?CommissionKey=' + commisssion_key + '&location=#div_RecipientView&page=Recipients&update=success');
        }
        else {
            return false;
        }
        
        
    }
    else {
        AlertPopUp('Error', 'Please select the recipients', 'error');

    }
}
//for commission deatils
function updateCommissionRecipientSelect(commissionkey, value) {

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/CommissionRecipient.aspx/updateCommissionRecipientSelect",
        data: "{commissionkey:'" + commissionkey + "', value:'" + value + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}



//for commision recipient checkbox
function insertAssignRecipients(commissionkey) {
    // alert('work');
    $('#table-Individual-Recipient input[type=checkbox]:checked').each(function (index) {

        console.log(index + ' checkbox has value' + $(this).val());

        addCommissionRecipients(commissionkey, $(this).val());
    });


}

function addCommissionRecipients(commissionkey, recipientkey) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/CommissionRecipient.aspx/addCommissionRecipients",
        data: "{commissionkey:'" + commissionkey + "', recipientkey:'" + recipientkey + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);

        },
        error: function () {    
            alert('Error communicating with server');
        }
    });
}
//for condition recipeiet insert
function insertRecipientCondition(commissionkey) {

    let valid = true;
    //for validation
    $('#table-Condition-Recipient tbody tr').each(function (index) {
        var RecipientType = $('.DdRecipientType', this).val();
        var RecipientOperator = $('.DdRecipientOperator', this).val();
        var RecipientRole = $('.DdRecipientRole', this).val();
        var RecipientCondition = $('.DdRecipientCondition', this).val();

        console.log(RecipientType + RecipientOperator + RecipientRole);
        if (RecipientType != '' && RecipientOperator != '' && RecipientRole !='') {
            valid = true;
           // alert('fdaf');
        }
        else {
            valid = false;
            //alert('fdafdfs');
        }



    });

    if (valid == true) {
        $('#table-Condition-Recipient tbody tr').each(function (index) {
            var RecipientType = $('.DdRecipientType', this).val();
            var RecipientOperator = $('.DdRecipientOperator', this).val();
            var RecipientRole = $('.DdRecipientRole', this).val();
            var RecipientCondition = $('.DdRecipientCondition', this).val();

            addCommissionRecipientCondition(commissionkey, RecipientType, RecipientOperator, RecipientRole, RecipientCondition);

        });

        includeConditionedRecipient(commissionkey);
    }
    else {
        alertNotify('Recipeint Condition Error', 'Check the Condition statement', 'bg-warning border-warning');
    }

    return valid;

}

function includeConditionedRecipient(CommissionKey) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/CommissionRecipient.aspx/includeConditionedRecipient",
        data: "{CommissionKey:'" + CommissionKey + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}


function addCommissionRecipientCondition(commissionkey, RecipientType, RecipientOperator, RecipientRole, RecipientCondition) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/CommissionRecipient.aspx/addCommissionRecipientCondition",
        data: "{commissionkey:'" + commissionkey + "', RecipientType:'" + RecipientType + "',RecipientOperator:'" + RecipientOperator + "', RecipientRole:'" + RecipientRole + "', RecipientCondition:'" + RecipientCondition + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}


//for update

function UpdateRecipients() {
    var commisssion_key = $('#hdn_CommissionKey').val();

    deleteConditions(commisssion_key);

    var assignRecipient = $('#DdAssignRecipient').val();
    updateCommissionRecipientSelect(commisssion_key, assignRecipient);
    if (assignRecipient == 1) {
        // alert("im here");
        insertAssignRecipients(commisssion_key);
       // AlertPopUp('Updated', 'Recipients Updated Successfully !', 'success');
        location.replace('../Web/CommissionView.aspx?CommissionKey=' + commisssion_key +'&location=#div_RecipientView&page=Recipients&update=success');
        $(' a[href="#Options"]').tab('show');
    }
    else if (assignRecipient == 2) {
        let inserted = insertRecipientCondition(commisssion_key);
        if (inserted == true) {
            location.replace('../Web/CommissionView.aspx?CommissionKey=' + commisssion_key + '&location=#div_RecipientView&page=Recipients&update=success');
        }
        else {
            return false;
        }
        //location.replace('../Web/CommissionView.aspx?CommissionKey=' + commisssion_key + '&location=#div_RecipientView&update=success');
        //$(' a[href="#Options"]').tab('show');
    }
    else {
        AlertPopUp('Error', 'Please select the recipients', 'error');
    }


}


function deleteConditions(commissionkey) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/CommissionRecipient.aspx/deleteExistingRecipients",
        data: "{commissionkey:'" + commissionkey + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);
            //return Rule Key
            //  rulekey = data.d;
            //   ret_val = data.d;
            //  alert("deleted");

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}

function loadRoleData(element, TypeValue) {
    
    let View = $(element).val();

    if (View != '') {
        $.ajax({
            async: false,
            type: "POST",
            url: "../Web/CommissionRecipient.aspx/loadRoleData",
            data: "{ViewName:'" + View + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
              //  console.log(data.d);
                $(TypeValue).html('');
                $(TypeValue).append('<option value="">Select one</option>');

                $(TypeValue).append(data.d);
                
            },
            error: function () {
                alert('Error communicating with server');
            }
        });
    }
    
}


//for recipient condition

function addRecipientCondition(val, id1, id2) {

    var conditionTr = getCommissionRecipientCondition(id1 + 1, id2);
    // var conditionTr = '<tr class="RecipientCondition" > <td> <select class="form-control DdRecipientType"> <option>-- select type --</option> <option>Recipient Type 1</option> <option>Recipient Type 2 -</option> </select> </td> <td> <select class="form-control DdRecipientOperator"> <option>-- select operator --</option> <option>equal</option> <option>not equal</option> </select> </td> <td> <select class="form-control DdRecipientRole"> <option>-- select type --</option> <option>Sales Rep</option> <option>Manager</option> <option>Sales Person</option> </select> </td> <td> <select class="form-control DdRecipientCondition"> <option value="0">-- select condition --</option> <option value="1">AND</option> <option value="2">OR</option> </select> </td> <td><button type="button" onclick="addRecipientCondition(this)" class="btn btn-primary btn-xx"><i class="icon-plus22"></i></button></td> </tr>';

    $(val).closest('tr').after(conditionTr);

}

function getCommissionRecipientCondition(id1, id2) {
    var ret_val;

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/CommissionRecipient.aspx/getCommissionRecipientCondition",
        data: "{id1:'" + id1 + "',id2:'" + id2 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            ret_val = data.d;

        },
        error: function () {
            alert('Error communicating with server');
        }
    });


    return ret_val;
}


function deleteRecipientCondition(val) {
    var tr = parseInt($('#table-Condition-Recipient tbody tr').length);
    if (tr > 1) {
        var confirmation = confirm("are you sure want to delete ?");
        if (confirmation == true) {
            $(val).closest('tr').remove();
        }
        
    }
    else {
        alertNotify('Recipeint Condition Error', 'Assign recipient by condition must have at least one condition', 'bg-warning border-warning');
    }
}
