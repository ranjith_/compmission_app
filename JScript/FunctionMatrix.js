﻿function UpdateRange(RangeKey, ColumnName, val) {
   
    var Value = $(val).val();
  //  console.log(Value);
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/FunctionMatrix.aspx/UpdateRange",
        data: "{RangeKey:'" + RangeKey + "',ColumnName:'" + ColumnName + "',Value:'" + Value + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //console.log(data.d);
            
        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}
function UpdateRangeValue(ValueKey,val) {
    var Value = $(val).val();
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/FunctionMatrix.aspx/UpdateRangeValue",
        data: "{ValueKey:'" + ValueKey + "',Value:'" + Value + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          //  console.log(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}

function SaveMatrix() {
    var MatrixKey = $('#hdn_MatrixKey').val();
    var MatrixName = $('#txt_MatrixName').val();
    
    parent.SaveMatrix(MatrixKey, MatrixName);
}
function CancelMatrix() {
    parent.CancelMatrix();
}