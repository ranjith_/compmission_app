﻿function checkRule(val) {
    let crediting = 0, calculation = 0, summarization = 0;
    if ($('#chb_CreditingActive').is(':checked') == true) {
        crediting = 1;

    }
    if ($('#chb_CalculationRule').is(':checked') == true) {
        calculation = 1;
    }
    if ($('#chb_SummarizationRule').is(':checked') == true) {
        summarization = 1;
    }
    console.log(crediting, calculation, summarization);
    if ($(val).is(':checked')) {
        // alert('checked');
    } else {
        if (crediting == 0 && calculation == 0 && summarization == 0) {
            alert('Please select any one rule');

            $(val).prop('checked', true);
        }
    }
}
function expandDesc(val) {
    if ($('.CommissionDesc').hasClass('CommissionDescActive')) {
        $('.CommissionDesc').removeClass('CommissionDescActive');
        $(val).html('Show More');
    }
    else {
        $('.CommissionDesc').addClass('CommissionDescActive');
        $(val).html('Show Less');
    }
}
function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
//for scroll
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$(document).ready(function () {
    // Handler for .ready() called.
    var loaction = getUrlVars()["location"];
    var update = getUrlVars()["update"];
    var page = getUrlVars()["page"];

    $('html, body').animate({
        scrollTop: $(loaction).offset().top
    }, 1000);
    if (update == "success") {
        //  AlertPopUp('Updated', page + ' Updated Successfully !', 'success');
        // AlertNotify(title, text, cssClass);
        alertNotify('Updated', page + ' Updated Successfully !', 'bg-success border-success');
    }
    var uri = window.location.toString();
    var removedQString = removeURLParameter(uri, 'update');
    window.history.replaceState({}, document.title, removedQString);
    console.log(removedQString);
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function openSplitPopUp(commissionkey,ruletype,calltype,splitkey) {
    $('#PopUpIframe').modal('show');

    $('#iFramePopUp').attr('src', 'FunctionSplit.aspx?CommissionKey=' + commissionkey + '&RuleType=' + ruletype + '&' + calltype + '&SplitKey=' + splitkey);
    $('#FunctionType').text('Split | ' + ruletype);
}

$(document).ready(function () {
    setInterval(AutoIframeHeight, 1000);
});

function AutoIframeHeight() {
    //   console.log('XXXX');
    var iFrameID = document.getElementById('iFramePopUp');
    if (iFrameID) {
        var height = iFrameID.height;
        var height2 = iFrameID.contentWindow.document.body.scrollHeight + "px";
       // console.log(height + "----" + height2);
        if (height != height2) {
            iFrameID.height = (iFrameID.contentWindow.document.body.scrollHeight) + 3 + "px";
        }
        // here you can make the height, I delete it first, then I make it again
        // iFrameID.height = "";
        //  iFrameID.height = (iFrameID.contentWindow.document.body.scrollHeight + 3) + "px";

    }
}
function SplitSaved() {
    $('#PopUpIframe').modal('hide');
    location.reload(true);
}

function moveRule(CommissionKey, RuleSequence, MoveRuleSeq, RuleType) {
    if (MoveRuleSeq == 0) {
        alert('You cannot move up this rule');
        return false;
    }

    $.ajax({
        type: "POST",
        url: "../Web/CommissionView.aspx/moveRule",
        data: "{CommissionKey:'" + CommissionKey + "',RuleSequence:'" + RuleSequence + "',MoveRuleSeq:'" + MoveRuleSeq + "',RuleType:'" + RuleType + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            if (data.d = "RuleMoved") {
              //  alert('Moved');
                location.reload();
            }
            else {
                alert("Error : Please try again");
            }

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}

//for process calculation rules by each transaction line or group tansactions

function ProcessType() {
    //alert('settt');
    //console.log('workdedd');
    var value = $('input[name=rdb_ProcessTransactionType]:checked').val();

    if (value == 'EachTransaction') {
      //  alert('type 1');

        $('#ProcessTxnCollapseBody').collapse("hide");
        CalculationRuleSetupEach();
       
        return 1;
    }
    else if (value = 'GroupTransaction') {
      //  alert('type 2');
        $('#ProcessTxnCollapseBody').collapse("show");

        return 2;
    }
}

$(document).ready(function () {
    let checked = $('#hdn_IsGroupBy').val();

    if (checked == 'False') {
        $("input[name=rdb_ProcessTransactionType][value='EachTransaction']").prop("checked", true);

        $("input[name=rdb_ProcessTransactionType][value='GroupTransaction']").prop("checked", false);
       // $('#rdb_EachTxn').attr('checked','true');
    }
    else {
        $("input[name=rdb_ProcessTransactionType][value='GroupTransaction']").prop("checked", true);
        $("input[name=rdb_ProcessTransactionType][value='EachTransaction']").prop("checked", false);
    }

    ProcessType();
});

function CalculationRuleSetup() {
    let CommissionKey = $('#hdn_CommissionKey').val();
    let GroupBy = $('#ddl_CmProcessTransactionGroupBy').val();
   // alert(CommissionKey + '  ' + GroupBy);
    $.ajax({
        type: "POST",
        url: "../Web/CommissionView.aspx/CalculationRuleSetupGroup",
        data: "{CommissionKey:'" + CommissionKey + "',GroupBy:'" + GroupBy + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            if (data.d = "Saved") {
                //alert('Saved');
               location.reload();
            }
            else {
                alert("Error : Please try again");
            }

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}
function CalculationRuleSetupEach() {
    let CommissionKey = $('#hdn_CommissionKey').val();
   
   // alert(CommissionKey);
    $.ajax({
        type: "POST",
        url: "../Web/CommissionView.aspx/CalculationRuleSetupEach",
        data: "{CommissionKey:'" + CommissionKey + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            if (data.d = "Saved") {
               // alert('Saved');
                // location.reload();
                $('#ddl_CmProcessTransactionGroupBy').val('');
            }
            else {
                alert("Error : Please try again");
            }

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}

function deleteRule(RuleKey, RuleName, RuleType) {
    var confirmation = confirm("are you sure want to delete this " + RuleName + "  Rule ?");
    if (confirmation == true) {
        //to delete
        $.ajax({
            type: "POST",
            url: "../Web/CommissionView.aspx/deleteRule",
            data: "{RuleKey:'" + RuleKey + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                if (data.d = "DeleteSuccess") {
                    alert("Rule " + RuleName + " deleted successfully")
                    location.reload();
                }
                else {
                    alert("Error : Please try again");
                }

            },
            error: function () {
                alert('Error communicating with server');
            }
        });
    }
    else {
        return false
    }
}