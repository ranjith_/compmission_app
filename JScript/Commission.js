﻿function Tab1() {
    $(' a[href="#Qualifying-Actions"]').tab('show');
}
function Tab2(val) {
    //to save the rules and show the next tab
   // alert('work');
    if (val == 0) {
        console.log('insert rules');
        var commisssion_key = $('#hdn_CommissionKey').val();
        insertRules(commisssion_key);
    }
    

    $(' a[href="#Qualifying-Recipients"]').tab('show');
}
function UpdateRules() {
    var commisssion_key = $('#hdn_CommissionKey').val();
    deleteRules(commisssion_key);
    
    insertRules(commisssion_key);
   // alert("Rules Updated Successfully !");
    AlertPopUp('Updated', 'Rules Updated Successfully !', 'success');
    $(' a[href="#Qualifying-Recipients"]').tab('show');
}
function UpdateRecipients() {
    var commisssion_key = $('#hdn_CommissionKey').val();

    deleteConditions(commisssion_key);
   
    var assignRecipient = $('#DdAssignRecipient').val();
    updateCommissionRecipientSelect(commisssion_key, assignRecipient);
    if (assignRecipient == 1) {
       // alert("im here");
        insertAssignRecipients(commisssion_key);
        AlertPopUp('Updated', 'Recipients Updated Successfully !', 'success');
        location.replace('../Web/CommissionView.aspx?CommissionKey=' + commisssion_key);
        $(' a[href="#Options"]').tab('show');
    }
    else if (assignRecipient == 2) {
        insertRecipientCondition(commisssion_key);
        AlertPopUp('Updated', 'Recipients Updated Successfully !', 'success');
        location.replace('../Web/CommissionView.aspx?CommissionKey=' + commisssion_key);
        $(' a[href="#Options"]').tab('show');
    }
    else {
        AlertPopUp('Error', 'Please select the recipients', 'error');
    }


}
function UpdateCommissionDetails() {

    var commisssion_key = $('#hdn_CommissionKey').val();

    var CommissionName = $('#txt_CommissionName').val();
    var CalFeq = $('#dd_CalFeq').val();
    var transaction = $('#dd_transaction').val();
    var CreditOf = $('#dd_CreditOf').val();

    var Percentage = $('#txt_Percentage').val();
    var StartDate = $('#dp_StartDate').val();
    var EndDate = $('#dp_EndDate').val();
    var DescWYSWYG = $('.note-editable').html();
    
    var ActiveCommission = 0;
    var isField = 0;
    var onAgree = 0;
    if ($('#chb_SourceOfCredit').is(':checked')) {
        isField = 1;
    }

    if ($('#chb_OnAgree').is(':checked')) {
        onAgree = 1;
    }
    if ($('#chb_ActiveCommission').is(':checked')) {
        ActiveCommission = 1;
    }


    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/updateCommissionDetails",
        data: "{commissionkey:'" + commisssion_key + "', CommissionName:'" + CommissionName + "',CalFeq:'" + CalFeq + "', transaction:'" + transaction + "', CreditOf:'" + CreditOf + "', " +
            " isField: '" + isField + "', onAgree: '" + onAgree + "', ActiveCommission: '" + ActiveCommission + "', " +
            "Percentage: '" + Percentage + "', StartDate: '" + StartDate + "', EndDate: '" + EndDate + "', DescWYSWYG: '" + DescWYSWYG + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);
            AlertPopUp('Updated', 'Commission Details Updated Successfully !', 'success');
        },
        error: function () {
            alert('Error communicating with server');
          
        }
    });

}
function Tab3() {
    // to save the commission  recipient and show the nest next tab
    //alert('dddd');
    var assignRecipient = $('#DdAssignRecipient').val();
    var commisssion_key = $('#hdn_CommissionKey').val();
    if (assignRecipient == 1) {
        insertAssignRecipients(commisssion_key);
    }
    else if (assignRecipient == 2) {
        insertRecipientCondition(commisssion_key);
    }
    else {
        AlertPopUp('Error', 'Please select the recipients', 'error');
        
    }

    $(' a[href="#Options"]').tab('show');

}

function assignRecipeientsInsert() {
    var assignRecipient = $('#DdAssignRecipient').val();
    var commisssion_key = $('#hdn_CommissionKey').val();
    updateCommissionRecipientSelect(commisssion_key, assignRecipient);
    if (assignRecipient == 1) {
        insertAssignRecipients(commisssion_key);
    }
    else if (assignRecipient == 2) {
        insertRecipientCondition(commisssion_key);
    }
    else {
        AlertPopUp('Error', 'Please select the recipients', 'error');

    }
}

function assignRecipientBy(val) {
    let assign = $(val).val();
    if (assign == 1) {
        $('#Individual_Recipients').removeClass('d-none');
        $('#Condition_Recipients').addClass('d-none');
    }
    else if (assign == 2) {
        $('#Individual_Recipients').addClass('d-none');
        $('#Condition_Recipients').removeClass('d-none');
    }
    else {
        $('#Individual_Recipients').addClass('d-none');
        $('#Condition_Recipients').addClass('d-none');
    }
}
//source of credit
function changeSourceOfCredit() {
    if ($('#chb_SourceOfCredit').is(':checked') == true) {
        $('#dd_transaction').removeClass('d-none');
        $('#txt_Percentage').addClass('d-none');
    }
    else {
        $('#dd_transaction').addClass('d-none');
        $('#txt_Percentage').removeClass('d-none');
    }
} 
function VariableFunction(val1) {
   
    var availableTags = [
        "#Total",
        "#sum",
        "#comm",
        "#SalesAmount",
        "#CreditAmount",
        "#Profit",
        "#CommAMt",
        "@goal(goal1)",
        "@goal(goal2)",
        "@goal(goal3)",
        "@getValue()",
        "@getAmount()",
        "@split()",
        "@matrix",
        "@Override()"
    ];
    function split(val) {
        return val.split(/ \s*/);
    }
    function extractLast(term) {
        return split(term).pop();
    }

    $(val1)
        // don't navigate away from the field on tab when selecting an item
        .on("keydown", function (event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).autocomplete("instance").menu.active) {

                event.preventDefault();
            }
        })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                // delegate back to autocomplete, but extract the last term
                response($.ui.autocomplete.filter(
                    availableTags, extractLast(request.term)));
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(" ");
                return false;
            }
        });
}

$(document).ready(function () {
   
   // var DO = '<tr class="RuleAction"> <td class="text-right" colspan="2"> </td> <td colspan="3"> <div class="d-flex"> <b class="my-auto mr-2"> DO</b> <input type="text" class="form-control tokenfield-typeahead Do" > <button type="button" onclick="deleteRow(this)" class="btn btn-outline-danger btn-xxx my-auto ml-2"><i class="icon-cross"></i></button> <button type="button" onclick="addDo(this)" class="btn btn-outline-primary btn-xxx my-auto ml-2"><i class="icon-plus2"></i></button> </div> </td> <td></td> <td> </td> </tr>';

   // $('.table-actions tbody').append(DO);
   // var ruleTbody = addCommissionRule(1,1);
   // console.log(ruleTbody);
  //  $('#TableCommissionRule').append(ruleTbody);
   // LoadVariablesAndProcedures();
   // vv('.Variables');
    //  VariableFunction('#Variables-1-1');
   
    setAllVariables();
});
function setAllVariables() {
    $('.table-actions tbody .Do').each(function () {
        var id = $(this).attr('id');
        VariableFunction('#' + id);
    });
}
function addCommissionRule(id1, id2) {
    var ret_val;

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/getCommissionRulesTbody",
        data: "{id1:'" + id1 + "',id2:'" + id2 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          //  console.log(data.d);
            //return Rule Key
            //  rulekey = data.d;
            ret_val = data.d;

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
    //   console.log(ret_val);

    return ret_val;
}
function getCommissionCondition(id1, id2) {
    var ret_val;

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/getCommissionCondition",
        data: "{id1:'" + id1 + "',id2:'" + id2 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
       
            ret_val = data.d;

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
    

    return ret_val;
}
function getCommissionRecipientCondition(id1, id2) {
    var ret_val;

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/getCommissionRecipientCondition",
        data: "{id1:'" + id1 + "',id2:'" + id2 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            ret_val = data.d;

        },
        error: function () {
            alert('Error communicating with server');
        }
    });


    return ret_val;
}
function getCommissionDo(id1, id2) {
    var ret_val;

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/getCommissionDo",
        data: "{id1:'" + id1 + "',id2:'" + id2 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            ret_val = data.d;

        },
        error: function () {
            alert('Error communicating with server');
        }
    });


    return ret_val;
}
function saveCommission() {
    var commissionkey = $('#hdn_CommissionKey').val();
    addCommissionDetails(commissionkey);
    insertRules(commissionkey);
    var assignRecipient = $('#DdAssignRecipient').val();
    if (assignRecipient == 1) {
        insertAssignRecipients(commissionkey);
    }
    else if (assignRecipient == 2) {
        insertRecipientCondition(commissionkey);
    }
    else {
        alert('Please select the recipient type');
    }
    location.reload();
    
}

//load table values 

function loadTableValues(val, select) {
    var tablename = $(val).val();
   // alert(tablename);

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/getTableValueOptions",
        data: "{table:'" + tablename + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
           //    console.log(data.d);
            //return Rule Key
            //  rulekey = data.d;
            // ret_val = data.d;
            $(select).append(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}

function loadRecipientType(val, select) {
    var tablename = $(val).val();
    // alert(tablename);

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/getTableValueOptions",
        data: "{table:'" + tablename + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
                console.log(data.d);
            //return Rule Key
            //  rulekey = data.d;
            // ret_val = data.d;
            $(select).append(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}
  //Conditions AND / OR
function addCondition(val,id1,id2) {
    
   // var ConditionTr = '<tr class="RuleCondition"> <td class="text-right"> </td> <td> <div class="d-flex"> <b class="my-auto mr-1">IF</b> <select class="form-control TableNames"> <option>-select one-</option> <option>Customer</option> <option>Product </option> <option>Transaction</option> <option>Recipient</option> <option>Variables</option> <option>Territory</option> </select> </div> </td> <td> <select class="form-control TableValues"> <option>-select one-</option> <option>Product ID 1</option> <option>Product ID 2</option> </select> </td> <td> <select class="form-control Operator"> <option>-select one-</option> <option>equals</option> <option>not equals</option> <option>less than</option> <option>greater than</option> <option>less than or equal to</option> <option>greater than or equal to</option> </select> </td> <td> <input type="text" class="form-control ConditionValue" /> </td> <td> <select onchange="addCondition(this)" class="form-control Condition"> <option value="0">-select one-</option> <option value="1">AND</option> <option value="2"> OR</option> </select> </td> <td> </td> </tr>';
    var ConditionTr = getCommissionCondition(id1,id2+1);
    $(val).closest('tr').after(ConditionTr);

}

function addDo(val, id1, id2) {
  
    $(val).addClass('d-none');
   // var DO = '<tr class="RuleAction"> <td class="text-right" colspan="2"> </td> <td colspan="3"> <div class="d-flex"> <b class="my-auto mr-2"> DO</b> <input type="text" class="form-control tokenfield-typeahead Do" > <button type="button" onclick="deleteRow(this)" class="btn btn-outline-danger btn-xxx my-auto ml-2"><i class="icon-cross"></i></button> <button type="button" onclick="addDo(this)" class="btn btn-outline-primary btn-xxx my-auto ml-2"><i class="icon-plus2"></i></button> </div> </td> <td></td> <td> </td> </tr>';
    var DO = getCommissionDo(id1, id2 + 1);
    $(val).closest('tbody').append(DO);
   // LoadVariablesAndProcedures();
    var id = "#Variables-" + id1 + "-" + (id2 + 1);
    console.log(id);
    VariableFunction(id);
}

function addControlStatement(val) {
    var tbody = '<tr class="RuleCondition"> <td class="text-right"> </td> <td> <div class="d-flex"> <b class="my-auto mr-1">IF</b> <select class="form-control TableNames"> <option>-select one-</option> <option>Customer</option> <option>Product </option> <option>Transaction</option> <option>Recipient</option> <option>Variables</option> <option>Territory</option> </select> </div> </td> <td> <select class="form-control TableValues"> <option>-select one-</option> <option>Product ID 1</option> <option>Product ID 2</option> </select> </td> <td> <select class="form-control Operator"> <option>-select one-</option> <option>equals</option> <option>not equals</option> <option>less than</option> <option>greater than</option> <option>less than or equal to</option> <option>greater than or equal to</option> </select> </td> <td> <input type="text" class="form-control ConditionValue" /> </td> <td> <select onchange="addCondition(this)" class="form-control Condition"> <option value="0">-select one-</option> <option value="1">AND</option> <option value="2"> OR</option> </select> </td> <td> </td> </tr>';
    $(val).closest('tbody').after(tbody);
  //  $('.table-actions tbody:last').after(tbody);
}

function addRule(val) {
    let tbodyCount = $('.table-actions tbody').length;
    //var tbody = '<tbody > <tr class="CommissionRule"> <td class="d-flex text-right float-right" > <span class="my-auto mr-2">Rule </span> <input value="' + (tbodyCount + 1)+'" type="text" class="form-control rule-box RuleOrder" / > </td> <td colspan="3" class="Name" > <div class="d-flex"> <input type="text" class="form-control RuleName" placeholder="Commission Template RuleName" /> <button type="button" onclick="deleteTbody(this)" class="btn btn-outline-danger btn-xxx my-auto ml-2"><i class="icon-cross"></i></button> </div> </td> </tr> <tr class="RuleCondition"> <td class="text-right"> </td> <td> <div class="d-flex"> <b class="my-auto mr-1">IF</b> <select class="form-control TableNames"> <option>-select one-</option> <option>Customer</option> <option>Product </option> <option>Transaction</option> <option>Recipient</option> <option>Variables</option> <option>Territory</option> </select> </div> </td> <td> <select class="form-control TableValues"> <option>-select one-</option> <option>Product ID 1</option> <option>Product ID 2</option> </select> </td> <td> <select class="form-control Operator"> <option>-select one-</option> <option>equals</option> <option>not equals</option> <option>less than</option> <option>greater than</option> <option>less than or equal to</option> <option>greater than or equal to</option> </select> </td> <td> <input type="text" class="form-control ConditionValue" /> </td> <td> <select onchange="addCondition(this)" class="form-control Condition"> <option value="0">-select one-</option> <option value="1">AND</option> <option value="2"> OR</option> </select> </td> <td> </td> </tr> <tr class="RuleAction"> <td class="text-right" colspan="2"> </td> <td colspan="3"> <div class="d-flex"> <b class="my-auto mr-2"> DO</b> <input type="text" class="form-control tokenfield-typeahead Do" > <button type="button" onclick="deleteRow(this)" class="btn btn-outline-danger btn-xxx my-auto ml-2"><i class="icon-cross"></i></button> <button type="button" onclick="addDo(this)" class="btn btn-outline-primary btn-xxx my-auto ml-2"><i class="icon-plus2"></i></button> </div> </td> <td></td> <td> </td> </tr> </tbody>';
    // console.log('ddd');
    var tbodyRule = addCommissionRule(tbodyCount + 1,1);
    console.log(tbodyRule);
    $('.table-actions tbody:last').after(tbodyRule);
    LoadVariablesAndProcedures();
    
}
//delete row

function deleteRow(val) {

    var confirmation = confirm("are you sure want to delete ?");
    if (confirmation == true) {
        $(val).closest('tr').remove();
    }
    else {
        return false;
    }
}
function deleteTbody(val) {

    let tbodyCount = $('.table-actions tbody').length;

    if (tbodyCount == 1) {
      //  alert('You must have atleast one rule in commission.');
        AlertPopUp('Error', 'You must have atleast one rule in commission', 'error');
        return false;
    }
    var confirmation = confirm("are you sure want to delete ?");
    if (confirmation == true) {
        $(val).closest('tbody').remove();
    }
    else {
        return false;
    }
    
}

function insertRules(commissionkey) {
   
    $('.table-actions > tbody').each(function () {
        //insert Rule Details
      //  alert('ss');
        var rulename = $('.RuleName', this).val();
        var ruleorder = $('.RuleOrder', this).val();

        var rulekey = addRuleDetails(commissionkey,rulename, ruleorder);
        $('tr.RuleAction ', this).each(function () {
           // var Do = $('.tokenfield').tokenfield('getInput')
          //  var Do = $('.tokenfield .Do', this).val();
            var Do = $('.Do', this).val();
            console.log(Do);
            addDoDetails(rulekey, Do);

        });

        $('.RuleCondition', this).each(function () {
            var tablenames = $('.TableNames', this).val();
            var tablevalues = $('.TableValues', this).val();
            var operator = $('.Operator', this).val();
            var conditionvalues = $('.ConditionValue', this).val();
            var condition = $('.Condition', this).val();
            console.log(tablenames);
            addConditionDetails(rulekey, tablenames, tablevalues, operator, conditionvalues, condition);

        });

       
    });
}
function deleteRules(commissionkey) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/deleteExistingRules",
        data: "{commissionkey:'" + commissionkey + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
              console.log(data.d);
            //return Rule Key
            //  rulekey = data.d;
         //   ret_val = data.d;
           // alert("deleted");

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}
function deleteConditions(commissionkey) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/deleteExistingRecipients",
        data: "{commissionkey:'" + commissionkey + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);
            //return Rule Key
            //  rulekey = data.d;
            //   ret_val = data.d;
           //  alert("deleted");

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}
function addCommissionDetails(commissionkey) {
    var CommissionName = $('#txt_CommissionName').val();
    var CalFeq = $('#dd_CalFeq').val();
    var transaction = $('#dd_transaction').val();
    var CreditOf = $('#dd_CreditOf').val();

    var Percentage = $('#txt_Percentage').val();
    var StartDate = $('#dp_StartDate').val();
    var EndDate = $('#dp_EndDate').val();
    var DescWYSWYG = $('.note-editable').html();
    var assignRecipient = $('#DdAssignRecipient').val();
    var ActiveCommission = 0;
    var isField = 0;
    var onAgree = 0;
    if ($('#chb_SourceOfCredit').is(':checked')) {
        isField = 1;
    }

    if ($('#chb_OnAgree').is(':checked')) {
        onAgree = 1;
    }
    if ($('#chb_ActiveCommission').is(':checked')) {
        ActiveCommission = 1;
    }

    insertCommissionDetails(commissionkey, CommissionName, CalFeq, transaction, CreditOf, Percentage, StartDate, EndDate, DescWYSWYG, isField, onAgree, assignRecipient, ActiveCommission);
   
}
function insertCommissionDetails(commissionkey, CommissionName, CalFeq, transaction, CreditOf, Percentage, StartDate, EndDate, DescWYSWYG, isField, onAgree, assignRecipient, ActiveCommission) {


    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/addCommissionDetails",
        data: "{commissionkey:'" + commissionkey + "', CommissionName:'" + CommissionName + "',CalFeq:'" + CalFeq + "', transaction:'" + transaction + "', CreditOf:'" + CreditOf + "', " +
            " isField: '" + isField + "', onAgree: '" + onAgree + "', assignRecipient: '" + assignRecipient + "', ActiveCommission: '" + ActiveCommission + "', "+
            "Percentage: '" + Percentage + "', StartDate: '" + StartDate + "', EndDate: '" + EndDate + "', DescWYSWYG: '" + DescWYSWYG + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });

}
function addRuleDetails(commissionkey,rulename,ruleorder) {
    //alert(Currency_Key);
    var ret_val;
    
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/addRuleDetails",
        data: "{commissionkey:'" + commissionkey + "',rulename:'" + rulename + "', ruleorder:'" + ruleorder + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //console.log(data.d);
            //return Rule Key
            //  rulekey = data.d;
            ret_val = data.d;
           
        },
        error: function () {
            alert('Error communicating with server');
        }
    });
 //   console.log(ret_val);
 
    return ret_val;
}

function addConditionDetails(rulekey, tablename, tablevalue, operator, conditionvalue, condition) {
    //alert(Currency_Key);
    var ret_val;

    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/addConditionDetails",
        data: "{rulekey:'" + rulekey + "', tablename:'" + tablename + "', tablevalue:'" + tablevalue + "', operatorvalue:'" + operator + "', conditionvalue:'" + conditionvalue + "', condition:'" + condition + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);
            //return Rule Key
            //  rulekey = data.d;
            ret_val = data.d;

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
    //   console.log(ret_val);

    return ret_val;
}
function addDoDetails(rulekey,dovalue) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/addDoDetails",
        data: "{rulekey:'" + rulekey + "', dovalue:'" + dovalue + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);
           
        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}

function addCommissionRecipients(commissionkey,recipientkey) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/addCommissionRecipients",
        data: "{commissionkey:'" + commissionkey + "', recipientkey:'" + recipientkey + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}
function updateCommissionRecipientSelect(commissionkey, value) {
    
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/updateCommissionRecipientSelect",
        data: "{commissionkey:'" + commissionkey + "', value:'" + value + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}
//for commision recipient checkbox
function insertAssignRecipients(commissionkey) {
   // alert('work');
    $('#table-Individual-Recipient input[type=checkbox]:checked').each(function (index) {
        
        console.log(index + ' checkbox has value' + $(this).val());

        addCommissionRecipients(commissionkey, $(this).val());
    });

  
}




function insertRecipientCondition(commissionkey) {
    $('#table-Condition-Recipient tbody tr').each(function (index) {
        var RecipientType = $('.DdRecipientType', this).val();
        var RecipientOperator = $('.DdRecipientOperator', this).val();
        var RecipientRole = $('.DdRecipientRole', this).val();
        var RecipientCondition = $('.DdRecipientCondition', this).val();

        addCommissionRecipientCondition(commissionkey, RecipientType, RecipientOperator, RecipientRole, RecipientCondition);

    });
}
function addCommissionRecipientCondition(commissionkey, RecipientType, RecipientOperator, RecipientRole, RecipientCondition) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/Commission.aspx/addCommissionRecipientCondition",
        data: "{commissionkey:'" + commissionkey + "', RecipientType:'" + RecipientType + "',RecipientOperator:'" + RecipientOperator + "', RecipientRole:'" + RecipientRole + "', RecipientCondition:'" + RecipientCondition + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);

        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}

//typeahead 

function LoadVariablesAndProcedures() {
    // Use Bloodhound engine
    var engine = new Bloodhound({
        local: [
            { value: '#total' },
            { value: '#sum' },
            { value: '#comp' },
            { value: '#loss' },
            { value: '#profit' },
            { value: '@goal(goal 1)' },
            { value: '@goal(goal 2)' },
            { value: '@goal(goal 3)' },
            { value: '@goal(goal 4)' },
            { value: '@split(split 1)' },
            { value: '@split(split 2)' },
            { value: '@split(split 3)' },
            { value: '@split(split 4)' },
            { value: '=' },
            { value: '+' },
            { value: '-' },
            { value: '*' }
        ],
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    // Initialize engine
    engine.initialize();

    // Initialize tokenfield
    $('.tokenfield-typeahead').tokenfield({
        typeahead: [null, {
            displayKey: 'value',
            maxTags: 999,
            source: engine.ttAdapter()
        }]
    });
}



//for split

function addRecipient() {
    var RecipientCount = $('#Split-Recipients tr').length;
    //let btn = '<div class="d-inline-block add-recipient"> <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button> <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(this)"><i class="icon-bin"></i></button> </div>';
    let btn = '<div class="d-inline-block add-recipient"> <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button>';
    var row = $('#Split-Recipients >tr:last').clone(true);
    //console.log(row);
    var existingPercentage = $("td input:text", row).val();
    var existingRecipients = $(" #Split-Recipients >tr:last  select.recipient-dd").children("option:selected").val();
    //console.log(existingRecipients);
    if (existingRecipients == '' || existingPercentage == '') {
        alert('Select recipent & Percentage');
        return false;
    }
    // $("select option:selected", row).attr("selected", false);
    // $("select.recipient-dd",row).children("option[" + existingRecipients + "]").remove();
    $("td input:text", row).val("");
    $("td.tr-btn", row).empty();
    $("td.tr-count", row).empty();
    $("td.tr-count", row).append(RecipientCount + 1);
    $("td.tr-btn", row).append(btn);

    //$('.add-recipient-btn').addClass('d-none');

    let del_btn = '<div class="d-inline-block add-recipient">  <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(this)"><i class="icon-bin"></i></button> </div>';
    $('#Split-Recipients >tr:last td:nth-child(4)').html(del_btn);

    row.insertAfter('#Split-Recipients >tr:last');

    // $(" #Split-Recipients >tr:last  select.recipient-dd").children("option:selected").remove();
    $(" #Split-Recipients >tr:last  select.recipient-dd option[value='" + existingRecipients + "']").remove();
    //let tr = '<tr> <td> <div class="form-group"> <label>Sales Rep ' + (RecipientCount + 1) + '</label> </div> </td> <td> <div class="form-group"> <input placeholder="%" name="RecipientPercentage[]"  type="text" class="form-control"> </div> </td> <td><button type="button" class="btn btn-primary btn-sm add-recipient-btn" onclick="addRecipient()"><i class="icon-plus22"></i></button></td> </tr>';

    // $('#Split-Recipients').append(tr);
}
function saveSplit() {
    var splitName = $('#txt_SplitName').val;
    var splitDesc = $('#txt_SplitDesc').val;
    var RecipientsPercentage = new Array();
    var splitRecipients = {};

    $("#Split-Recipients tr").each(function () {
        console.log($('select', this).val());
        console.log($("input[name='RecipientPercentage[]']", this).val());
        var recipientKey = $('select', this).val();
        var Percentage = $("input[name='RecipientPercentage[]']", this).val();
        if (recipientKey != '' && Percentage != '') {
            splitRecipients[recipientKey] = Percentage;
        }



    });

    console.log(splitRecipients);

    //$("input[name='RecipientPercentage[]']").each(function () {
    //    RecipientsPercentage.push($(this).val());
    //});

    //console.log(RecipientsPercentage);

}

function removeRecipient(val) {

    $(val).closest('tr').remove();

    let count = 1;
    $('#Split-Recipients tr').each(function () {
        $('td:nth-child(1)', this).html(count);
        count++;

    });

    //if (val != 0) {
    //    $("#Split-Recipients tr:nth-child(" + (val + 1) + ")").remove();
    //}
    //var btn = '';

    //if (val == 1) {
    //     btn = '<div class="d-inline-block add-recipient"> <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button> </div>';
    //}
    //else {
    //    btn = '<div class="d-inline-block add-recipient"> <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button> <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(' + (val - 1) + ')"><i class="icon-bin"></i></button> </div>';
    //}

    //$("#Split-Recipients tr:last td:nth-child(4)").append(btn);
}

//for manageroverride

function addOverrideLevel() {
    var LevelCount = $('#Override-Levels tr').length;
    //let btn = '<div class="d-inline-block add-override-level"> <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button> <button type="button" class="btn btn-danger btn-sm add-override-btn btn-xx" onclick="removeOverrideLevel(this)"><i class="icon-bin"></i></button> </div>';
    let btn = '<div class="d-inline-block add-override-level"> <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button>  </div>';
    var row = $('#Override-Levels >tr:first').clone(true);
    //console.log(row);
    var existingLevel = $("td input:text", row).val();
    // var existingRecipients = $(" #Split-Recipients >tr:last  select.recipient-dd").children("option:selected").val();
    //console.log(existingRecipients);
    if (existingLevel == '') {
        alert('Add level first');
        return false;
    }
    // $("select option:selected", row).attr("selected", false);
    // $("select.recipient-dd",row).children("option[" + existingRecipients + "]").remove();
    $("td input:text", row).val("");
    $("td.tr-btn", row).empty();
    $("td.td-level", row).empty();
    $("td.td-level", row).append('<div class="form-group"> <label>Level ' + (LevelCount + 1) + '</label> </div>');
    $("td.tr-btn", row).append(btn);

    // $('.add-override-btn').addClass('d-none');
    let del_btn = '<div class="d-inline-block add-override-level">  <button type="button" class="btn btn-danger btn-sm add-override-btn btn-xx" onclick="removeOverrideLevel(this)"><i class="icon-bin"></i></button> </div>';
    $('#Override-Levels >tr:first td:nth-child(3)').html(del_btn);


    row.insertBefore('#Override-Levels >tr:first');

    // $(" #Split-Recipients >tr:last  select.recipient-dd").children("option:selected").remove();
    // $(" #Override-Levels >tr:last  select.recipient-dd option[value='" + existingRecipients + "']").remove();
    //let tr = '<tr> <td> <div class="form-group"> <label>Sales Rep ' + (RecipientCount + 1) + '</label> </div> </td> <td> <div class="form-group"> <input placeholder="%" name="RecipientPercentage[]"  type="text" class="form-control"> </div> </td> <td><button type="button" class="btn btn-primary btn-sm add-recipient-btn" onclick="addRecipient()"><i class="icon-plus22"></i></button></td> </tr>';

    // $('#Split-Recipients').append(tr);
}

function removeOverrideLevel(val) {
    $(val).closest('tr').remove();
    //alert(val);
    //if (val != 0) {
    //    $("#Override-Levels tr:nth-child(1)").remove();
    //}
    //var btn = '';

    //if (val == 1) {
    //    btn = '<div class="d-inline-block add-override-level"> <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button></div>';
    //}
    //else {
    //    btn = '<div class="d-inline-block add-override-level"> <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button> <button type="button" class="btn btn-danger btn-sm add-override-btn btn-xx" onclick="removeOverrideLevel(' + (val-1) + ')"><i class="icon-bin"></i></button> </div>';
    //}

    //$("#Override-Levels tr:first td:nth-child(3)").append(btn);

    var LevelCount = $('#Override-Levels tr').length;

    $('#Override-Levels tr').each(function () {
        $('td:nth-child(1)', this).html('Level ' + LevelCount);
        LevelCount = LevelCount - 1;

    });
}



//for goal

function addGoal() {
    var goalCount = $('#set-goal tr').length;

    let btn = '<div class="d-flex add-goal"> <button type="button" class="btn btn-primary btn-sm add-goal-btn btn-xx mr-2" onclick="addGoal()"><i class="icon-plus22"></i></button> </div>';
    var row = $('#set-goal >tr:last').clone(true);
    //console.log(row);
    var period = $("td input[name='period']", row).val();

    var goal = $("td input[name='goal']", row).val();
    // var existingRecipients = $(" #set-goal >tr:last  select.recipient-dd").children("option:selected").val();
    //console.log(existingRecipients);
    if (period == '' || goal == '') {
        alert(' enter period & goal');
        return false;
    }
    $("td input:text", row).val("");
    $("td.td-btn", row).empty();
    $("td.tr-count", row).empty();
    $("td.tr-count", row).append(goalCount + 1);
    $("td.td-btn", row).append(btn);

    //$('.add-goal-btn').addClass('d-none');

    let del_btn = '<div class="d-flex add-goal"> <button type="button" class="btn btn-danger btn-sm add-goal-btn btn-xx" onclick="removeGoal(this)"><i class="icon-bin"></i></button> </div>';
    $('#set-goal >tr:last td:nth-child(4)').html(del_btn);


    row.insertAfter('#set-goal >tr:last');


}
function saveGoal() {
    var splitName = $('#txt_SplitName').val;
    var splitDesc = $('#txt_SplitDesc').val;
    var RecipientsPercentage = new Array();
    var splitRecipients = {};

    $("#Split-Recipients tr").each(function () {
        console.log($('select', this).val());
        console.log($("input[name='RecipientPercentage[]']", this).val());
        var recipientKey = $('select', this).val();
        var Percentage = $("input[name='RecipientPercentage[]']", this).val();
        if (recipientKey != '' && Percentage != '') {
            splitRecipients[recipientKey] = Percentage;
        }



    });

    console.log(splitRecipients);

    //$("input[name='RecipientPercentage[]']").each(function () {
    //    RecipientsPercentage.push($(this).val());
    //});

    //console.log(RecipientsPercentage);

}

function removeGoal(val) {
    $(val).closest('tr').remove();

    var goalCount = $('#set-goal tr').length;
    let count = 1;
    $('#set-goal tr').each(function () {
        $('td:nth-child(1)', this).html(count);
        count++;

    });

}


//for range

function addRange() {

    var rangeCount = $('#set-range tr').length;

    let btn = '<div class="d-flex add-range"> <button type="button" class="btn btn-primary btn-sm add-range-btn btn-xx mr-2" onclick="addRange()"><i class="icon-plus22"></i></button> </div>';
    var row = $('#set-range >tr:last').clone(true);
    // console.log(row);
    var from = $("td input[name='from']", row).val();

    var to = $("td input[name='to']", row).val();
    var value = $("td input[name='value']", row).val();

    if (from == '' || to == '' || value == '') {
        alert(' enter From & to & Value');
        return false;
    }
    $("td input:text", row).val("");
    $("td.td-btn", row).empty();
    $("td.td-count", row).empty();
    $("td.td-count", row).append(rangeCount + 1);
    $("td.td-btn", row).append(btn);

    let del_btn = '<div class="d-flex add-range"> <button type="button" class="btn btn-danger btn-sm add-range-btn btn-xx" onclick="removeRange(this)"><i class="icon-bin"></i></button> </div>';
    $('#set-range >tr:last td:nth-child(5)').html(del_btn);


    row.insertAfter('#set-range >tr:last');


}
function saveRange() {
    var splitName = $('#txt_SplitName').val;
    var splitDesc = $('#txt_SplitDesc').val;
    var RecipientsPercentage = new Array();
    var splitRecipients = {};

    $("#Split-Recipients tr").each(function () {
        console.log($('select', this).val());
        console.log($("input[name='RecipientPercentage[]']", this).val());
        var recipientKey = $('select', this).val();
        var Percentage = $("input[name='RecipientPercentage[]']", this).val();
        if (recipientKey != '' && Percentage != '') {
            splitRecipients[recipientKey] = Percentage;
        }



    });

    console.log(splitRecipients);

}

function removeRange(val) {

    $(val).closest('tr').remove();


    let count = 1;
    $('#set-range  tr').each(function () {
        $('td:nth-child(1)', this).html(count);
        count++;

    });
}



//for recipient condition

function addRecipientCondition(val,id1,id2) {

    var conditionTr = getCommissionRecipientCondition(id1+1, id2);
   // var conditionTr = '<tr class="RecipientCondition" > <td> <select class="form-control DdRecipientType"> <option>-- select type --</option> <option>Recipient Type 1</option> <option>Recipient Type 2 -</option> </select> </td> <td> <select class="form-control DdRecipientOperator"> <option>-- select operator --</option> <option>equal</option> <option>not equal</option> </select> </td> <td> <select class="form-control DdRecipientRole"> <option>-- select type --</option> <option>Sales Rep</option> <option>Manager</option> <option>Sales Person</option> </select> </td> <td> <select class="form-control DdRecipientCondition"> <option value="0">-- select condition --</option> <option value="1">AND</option> <option value="2">OR</option> </select> </td> <td><button type="button" onclick="addRecipientCondition(this)" class="btn btn-primary btn-xx"><i class="icon-plus22"></i></button></td> </tr>';
    
    $(val).closest('tr').after(conditionTr);

}

function deleteRecipientCondition(val) {

    var tr = parseInt($('#table-Condition-Recipient tbody tr').length);
    if (tr > 1) {
        var confirmation = confirm("are you sure want to delete ?");
        if (confirmation == true) {
            $(val).closest('tr').remove();
        }
        else {
            return false;
        }
    }
    
}
