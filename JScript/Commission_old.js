﻿function Tab1() {
    $(' a[href="#bordered-justified-tab1"]').tab('show');
}
function Tab2() {
    $(' a[href="#bordered-justified-tab2"]').tab('show');
}
function Tab3() {
    $(' a[href="#bordered-justified-tab3"]').tab('show');

}
function Tab4() {
    $(' a[href="#bordered-justified-tab4"]').tab('show');
}
function Tab5() {
    $(' a[href="#bordered-justified-tab5"]').tab('show');
}
function Tab6() {
    $(' a[href="#bordered-justified-tab6"]').tab('show');
}

//set split type head

$(document).ready(function () {
    // Defining the local dataset
    var splits = ['split 1', 'Split 2', 'Split 3', 'Split 4', 'Split 5', 'Split 6', 'Split 7 ', 'Split 8', 'Split 9', 'Split 10'];

    // Constructing the suggestion engine
    var split = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: splits
    });

    // Initializing the typeahead
    $('.typeahead-split').typeahead({
        hint: true,
        highlight: true, /* Enable substring highlighting */
        minLength: 1 /* Specify minimum characters required for showing result */
    },
        {
            name: 'splits',
            source: split
        });
});  



//for split

function addRecipient() {
    var RecipientCount = $('#Split-Recipients tr').length;
    //let btn = '<div class="d-inline-block add-recipient"> <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button> <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(this)"><i class="icon-bin"></i></button> </div>';
    let btn = '<div class="d-inline-block add-recipient"> <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button>';
    var row = $('#Split-Recipients >tr:last').clone(true);
    //console.log(row);
    var existingPercentage = $("td input:text", row).val();
    var existingRecipients = $(" #Split-Recipients >tr:last  select.recipient-dd").children("option:selected").val();
    //console.log(existingRecipients);
    if (existingRecipients == '' || existingPercentage== '') {
        alert('Select recipent & Percentage');
        return false;
    }
    // $("select option:selected", row).attr("selected", false);
   // $("select.recipient-dd",row).children("option[" + existingRecipients + "]").remove();
    $("td input:text", row).val("");
    $("td.tr-btn", row).empty();
   $("td.tr-count", row).empty();
    $("td.tr-count", row).append(RecipientCount + 1);
    $("td.tr-btn", row).append(btn);
    
    //$('.add-recipient-btn').addClass('d-none');

    let del_btn = '<div class="d-inline-block add-recipient">  <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(this)"><i class="icon-bin"></i></button> </div>';
    $('#Split-Recipients >tr:last td:nth-child(4)').html(del_btn);

    row.insertAfter('#Split-Recipients >tr:last');

    // $(" #Split-Recipients >tr:last  select.recipient-dd").children("option:selected").remove();
    $(" #Split-Recipients >tr:last  select.recipient-dd option[value='" + existingRecipients + "']").remove();
    //let tr = '<tr> <td> <div class="form-group"> <label>Sales Rep ' + (RecipientCount + 1) + '</label> </div> </td> <td> <div class="form-group"> <input placeholder="%" name="RecipientPercentage[]"  type="text" class="form-control"> </div> </td> <td><button type="button" class="btn btn-primary btn-sm add-recipient-btn" onclick="addRecipient()"><i class="icon-plus22"></i></button></td> </tr>';

   // $('#Split-Recipients').append(tr);
}
function saveSplit() {
    var splitName = $('#txt_SplitName').val;
    var splitDesc = $('#txt_SplitDesc').val;
    var RecipientsPercentage = new Array();
    var splitRecipients = {};

    $("#Split-Recipients tr").each(function () {
        console.log($('select', this).val());
        console.log($("input[name='RecipientPercentage[]']", this).val());
        var recipientKey = $('select', this).val();
        var Percentage = $("input[name='RecipientPercentage[]']", this).val();
        if (recipientKey != '' && Percentage != '') {
            splitRecipients[recipientKey] = Percentage;
        }
      
        

    });

    console.log(splitRecipients);

    //$("input[name='RecipientPercentage[]']").each(function () {
    //    RecipientsPercentage.push($(this).val());
    //});

    //console.log(RecipientsPercentage);

}

function removeRecipient(val) {
   
    $(val).closest('tr').remove();

    let count = 1;
    $('#Split-Recipients tr').each(function () {
        $('td:nth-child(1)', this).html(count);
        count++;

    });

    //if (val != 0) {
    //    $("#Split-Recipients tr:nth-child(" + (val + 1) + ")").remove();
    //}
    //var btn = '';

    //if (val == 1) {
    //     btn = '<div class="d-inline-block add-recipient"> <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button> </div>';
    //}
    //else {
    //    btn = '<div class="d-inline-block add-recipient"> <button type="button" class="btn btn-primary btn-sm add-recipient-btn btn-xx mr-2" onclick="addRecipient()"><i class="icon-plus22"></i></button> <button type="button" class="btn btn-danger btn-sm add-recipient-btn btn-xx" onclick="removeRecipient(' + (val - 1) + ')"><i class="icon-bin"></i></button> </div>';
    //}
    
    //$("#Split-Recipients tr:last td:nth-child(4)").append(btn);
}

//for manageroverride

function addOverrideLevel() {
    var LevelCount = $('#Override-Levels tr').length;
    //let btn = '<div class="d-inline-block add-override-level"> <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button> <button type="button" class="btn btn-danger btn-sm add-override-btn btn-xx" onclick="removeOverrideLevel(this)"><i class="icon-bin"></i></button> </div>';
    let btn = '<div class="d-inline-block add-override-level"> <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button>  </div>';
    var row = $('#Override-Levels >tr:first').clone(true);
    //console.log(row);
    var existingLevel = $("td input:text", row).val();
   // var existingRecipients = $(" #Split-Recipients >tr:last  select.recipient-dd").children("option:selected").val();
    //console.log(existingRecipients);
    if (existingLevel == '') {
        alert('Add level first');
        return false;
    }
    // $("select option:selected", row).attr("selected", false);
    // $("select.recipient-dd",row).children("option[" + existingRecipients + "]").remove();
    $("td input:text", row).val("");
    $("td.tr-btn", row).empty();
   $("td.td-level", row).empty();
    $("td.td-level", row).append('<div class="form-group"> <label>Level ' + (LevelCount + 1)+'</label> </div>');
    $("td.tr-btn", row).append(btn);

   // $('.add-override-btn').addClass('d-none');
    let del_btn = '<div class="d-inline-block add-override-level">  <button type="button" class="btn btn-danger btn-sm add-override-btn btn-xx" onclick="removeOverrideLevel(this)"><i class="icon-bin"></i></button> </div>';
    $('#Override-Levels >tr:first td:nth-child(3)').html(del_btn);


    row.insertBefore('#Override-Levels >tr:first');

    // $(" #Split-Recipients >tr:last  select.recipient-dd").children("option:selected").remove();
   // $(" #Override-Levels >tr:last  select.recipient-dd option[value='" + existingRecipients + "']").remove();
    //let tr = '<tr> <td> <div class="form-group"> <label>Sales Rep ' + (RecipientCount + 1) + '</label> </div> </td> <td> <div class="form-group"> <input placeholder="%" name="RecipientPercentage[]"  type="text" class="form-control"> </div> </td> <td><button type="button" class="btn btn-primary btn-sm add-recipient-btn" onclick="addRecipient()"><i class="icon-plus22"></i></button></td> </tr>';

    // $('#Split-Recipients').append(tr);
}

function removeOverrideLevel(val) {
    $(val).closest('tr').remove();
    //alert(val);
    //if (val != 0) {
    //    $("#Override-Levels tr:nth-child(1)").remove();
    //}
    //var btn = '';

    //if (val == 1) {
    //    btn = '<div class="d-inline-block add-override-level"> <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button></div>';
    //}
    //else {
    //    btn = '<div class="d-inline-block add-override-level"> <button type="button" class="btn btn-primary btn-sm add-override-btn btn-xx mr-2" onclick="addOverrideLevel()"><i class="icon-plus22"></i></button> <button type="button" class="btn btn-danger btn-sm add-override-btn btn-xx" onclick="removeOverrideLevel(' + (val-1) + ')"><i class="icon-bin"></i></button> </div>';
    //}

    //$("#Override-Levels tr:first td:nth-child(3)").append(btn);

    var LevelCount = $('#Override-Levels tr').length;

    $('#Override-Levels tr').each(function () {
        $('td:nth-child(1)', this).html('Level ' + LevelCount );
        LevelCount = LevelCount-1;

    });
}



//for goal

function addGoal() {
    var goalCount = $('#set-goal tr').length;

    let btn = '<div class="d-flex add-goal"> <button type="button" class="btn btn-primary btn-sm add-goal-btn btn-xx mr-2" onclick="addGoal()"><i class="icon-plus22"></i></button> </div>';
    var row = $('#set-goal >tr:last').clone(true);
    //console.log(row);
    var period = $("td input[name='period']", row).val();

    var goal = $("td input[name='goal']", row).val();
   // var existingRecipients = $(" #set-goal >tr:last  select.recipient-dd").children("option:selected").val();
    //console.log(existingRecipients);
    if (period == '' || goal == '') {
        alert(' enter period & goal');
        return false;
    }
    $("td input:text", row).val("");
    $("td.td-btn", row).empty();
    $("td.tr-count", row).empty();
    $("td.tr-count", row).append(goalCount + 1);
    $("td.td-btn", row).append(btn);

    //$('.add-goal-btn').addClass('d-none');

    let del_btn = '<div class="d-flex add-goal"> <button type="button" class="btn btn-danger btn-sm add-goal-btn btn-xx" onclick="removeGoal(this)"><i class="icon-bin"></i></button> </div>';
    $('#set-goal >tr:last td:nth-child(4)').html(del_btn);


    row.insertAfter('#set-goal >tr:last');

    
}
function saveGoal() {
    var splitName = $('#txt_SplitName').val;
    var splitDesc = $('#txt_SplitDesc').val;
    var RecipientsPercentage = new Array();
    var splitRecipients = {};

    $("#Split-Recipients tr").each(function () {
        console.log($('select', this).val());
        console.log($("input[name='RecipientPercentage[]']", this).val());
        var recipientKey = $('select', this).val();
        var Percentage = $("input[name='RecipientPercentage[]']", this).val();
        if (recipientKey != '' && Percentage != '') {
            splitRecipients[recipientKey] = Percentage;
        }



    });

    console.log(splitRecipients);

    //$("input[name='RecipientPercentage[]']").each(function () {
    //    RecipientsPercentage.push($(this).val());
    //});

    //console.log(RecipientsPercentage);

}

function removeGoal(val) {
    $(val).closest('tr').remove();

    var goalCount = $('#set-goal tr').length;
    let count = 1;
    $('#set-goal tr').each(function () {
        $('td:nth-child(1)', this).html(count);
        count ++;

    });
    
}


//for range

function addRange() {

    var rangeCount = $('#set-range tr').length;
   
    let btn = '<div class="d-flex add-range"> <button type="button" class="btn btn-primary btn-sm add-range-btn btn-xx mr-2" onclick="addRange()"><i class="icon-plus22"></i></button> </div>';
    var row = $('#set-range >tr:last').clone(true);
   // console.log(row);
    var from = $("td input[name='from']", row).val();

    var to = $("td input[name='to']", row).val();
    var value = $("td input[name='value']", row).val();

    if (from == '' || to == '' || value=='') {
        alert(' enter From & to & Value');
        return false;
    }
    $("td input:text", row).val("");
    $("td.td-btn", row).empty();
    $("td.td-count", row).empty();
    $("td.td-count", row).append(rangeCount + 1);
    $("td.td-btn", row).append(btn);

    let del_btn = '<div class="d-flex add-range"> <button type="button" class="btn btn-danger btn-sm add-range-btn btn-xx" onclick="removeRange(this)"><i class="icon-bin"></i></button> </div>';
    $('#set-range >tr:last td:nth-child(5)').html(del_btn);


    row.insertAfter('#set-range >tr:last');

   
}
function saveRange() {
    var splitName = $('#txt_SplitName').val;
    var splitDesc = $('#txt_SplitDesc').val;
    var RecipientsPercentage = new Array();
    var splitRecipients = {};

    $("#Split-Recipients tr").each(function () {
        console.log($('select', this).val());
        console.log($("input[name='RecipientPercentage[]']", this).val());
        var recipientKey = $('select', this).val();
        var Percentage = $("input[name='RecipientPercentage[]']", this).val();
        if (recipientKey != '' && Percentage != '') {
            splitRecipients[recipientKey] = Percentage;
        }



    });

    console.log(splitRecipients);

}

function removeRange(val) {

    $(val).closest('tr').remove();

   
    let count = 1;
    $('#set-range  tr').each(function () {
        $('td:nth-child(1)', this).html(count);
        count++;

    });
}



//credit transactions

function addCreditTransaction() {
    var card = $('.credit-transaction-card').clone(true);
    $('.credit-transaction-card').after(card);

}