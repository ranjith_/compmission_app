﻿function CallFunction(val) {
    var FunctionType = $(val).val();
    if (FunctionType == 'Split()') {
        $('#PopUpIframe').modal('show');
        var RuleKey = $('#hdn_RuleKey').val();
        $('#iFramePopUp').attr('src', 'FunctionSplit.aspx?RuleKey=' + RuleKey);
        $('#IfFunction').addClass('d-none');
        $('#txt_FunctionDetails').removeAttr('required', true);
    }
    else if (FunctionType == 'Matrix()') {
        $('#PopUpIframe').modal('show');
        var RuleKey = $('#hdn_RuleKey').val();
        $('#IfFunction').addClass('d-none');
        $('#iFramePopUp').attr('src', 'FunctionMatrix.aspx?RuleKey=' + RuleKey);
        $('#txt_FunctionDetails').removeAttr('required', true);
    }
    else if(FunctionType == 'Evaluate()'){
        $('#txt_FunctionDetails').prop("readonly", false);
        $('#IfFunction').removeClass('d-none');
        $('#txt_FunctionDetails').prop('required', true);
       
    }
    else if (FunctionType == 'IF()') {
        $('#PopUpIframe').modal('show');
        var RuleKey = $('#hdn_RuleKey').val();
        $('#iFramePopUp').attr('src', 'FunctionIf.aspx?RuleKey=' + RuleKey);
        $('#IfFunction').addClass('d-none');
        $('#txt_FunctionDetails').removeAttr('required', true);
    }
    else if (FunctionType == 'Goal()') {
        $('#PopUpIframe').modal('show');
        var RuleKey = $('#hdn_RuleKey').val();
        var CommissionKey = $('#hdn_CommissionKey').val();
        $('#IfFunction').addClass('d-none');
        $('#iFramePopUp').attr('src', 'FunctionGoal.aspx?RuleKey=' + RuleKey + '&CommissionKey=' + CommissionKey);
        $('#txt_FunctionDetails').removeAttr('required', true);
    }
    else if (FunctionType == 'Sequelize()') {
        $('#txt_FunctionDetails').prop('readonly', false);
        $('#IfFunction').removeClass('d-none');
        $('#txt_FunctionDetails').prop('required', true);
    }
  

}

function EditFunction(rulekey,functionkey,functiontype) {
    if (functiontype == 'Split()') {
        $('#PopUpIframe').modal('show');

        $('#iFramePopUp').attr('src', 'FunctionSplit.aspx?RuleKey=' + rulekey + '&isEdit=1&FunctionKey=' + functionkey);
    }
    else if (functiontype == 'Matrix()') {
        $('#PopUpIframe').modal('show');

        $('#iFramePopUp').attr('src', 'FunctionMatrix.aspx?RuleKey=' + rulekey + '&isEdit=1&FunctionKey=' + functionkey);
    }
    else if (functiontype == 'IF()') {
        $('#PopUpIframe').modal('show');

        $('#iFramePopUp').attr('src', 'FunctionIf.aspx?RuleKey=' + rulekey + '&isEdit=1&ConditionKey=' + functionkey);
    }
    else if (functiontype == 'Goal()') {
        $('#PopUpIframe').modal('show');
        var CommissionKey = $('#hdn_CommissionKey').val();
        $('#iFramePopUp').attr('src', 'FunctionGoal.aspx?RuleKey=' + rulekey + '&isEdit=1&FunctionKey=' + functionkey + '&CommissionKey=' + CommissionKey);


    }
    else if (functiontype == 'Evaluate()') {
        $('#txt_FunctionDetails').prop('readonly', false);

    }
    else if (functiontype == 'Sequelize()') {
        $('#txt_FunctionDetails').prop('readonly', false);

    }
}
function EditSplit() {
    var RuleKey = $('#hdn_RuleKey').val();
    $('#iFramePopUp').attr('src', 'FunctionSplit.aspx?isEdit=1&RuleKey=' + RuleKey + '&SplitKey=' + getIframeKey());
    $('#PopUpIframe').modal('show');
}
$(document).ready(function () {
    setInterval(AutoIframeHeight, 1000);
    let funcType = $('#ddl_Functions').val();
    if (funcType == 'Evaluate()' || funcType == 'Sequelize()') {
        $('#txt_FunctionDetails').prop('readonly', false);
        $('#btn_EditFunction').addClass('d-none');
        $('#IfFunction').removeClass('d-none');
        $('#IFrameFunc').addClass('d-none');
    }
});

function AutoIframeHeight() {
 //   console.log('XXXX');
    var iFrameID = document.getElementById('iFramePopUp');
    if (iFrameID) {
        var height = iFrameID.height;
        var height2 = iFrameID.contentWindow.document.body.scrollHeight + "px";
      //  console.log(height + "----" + height2);
        if (height != height2) {
            iFrameID.height = (iFrameID.contentWindow.document.body.scrollHeight)+ 3 + "px";
        }
        // here you can make the height, I delete it first, then I make it again
       // iFrameID.height = "";
          //  iFrameID.height = (iFrameID.contentWindow.document.body.scrollHeight + 3) + "px";
        
    }
}

function getIframeKey() {
    var iframe0 = document.getElementById("iFramePopUp");
    var iframe0document = iframe0.contentDocument || iframe0.contentWindow.document;
    var FunctionConditionKey = iframe0document.getElementById("hdn_FunctionConditionKey");
    //alert(FunctionConditionKey.value);

    return FunctionConditionKey.value;
}
function LoadFunctionStatements(FunctionType, FunctionssKey, SplitName) {
    var FunctionKey = getIframeKey();
    alert(FunctionKey);
    $('#FunctionName').text(SplitName);
    $('#hdn_FunctionKey').val(FunctionKey);
    getFunctionDescription(FunctionType, FunctionKey);
   
    $('#btn_EditFunction').removeClass('d-none');
}
function LoadFunctionDescription(FunctionType, FunctionKey, SplitName) {
    //alert(FunctionKey);
    $('#FunctionName').text(SplitName);
    $('#hdn_FunctionKey').val(FunctionKey);
    getFunctionDescription(FunctionType, FunctionKey);

    $('#btn_EditFunction').removeClass('d-none');
}
function getFunctionDescription(FunctionType, FunctionKey){
    $.ajax({
        async: false,
        type: "POST",
        url: "../Web/CommissionRule.aspx/getFunctionDescription",
        data: "{FunctionType:'" + FunctionType + "',FunctionKey:'" + FunctionKey + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data.d);
            console.log('ddd');
            $('#txt_FunctionDetails').val(data.d);
            $('#PopUpIframe').modal('hide');
           
        },
        error: function () {
            alert('Error communicating with server');
        }
    });
}
function SaveMatrix(MatrixKey, MatrixName) {
    $('#FunctionName').text(MatrixName);
    $('#hdn_FunctionKey').val(MatrixKey);
    $('#PopUpIframe').modal('hide');
}
function CancelMatrix() {
    $('#PopUpIframe').modal('hide');
    $('#ddl_Functions').val('');
}
function SaveCondition(type, conditionkey) {
    $('#FunctionName').text(type);
    $('#hdn_FunctionKey').val(conditionkey);
    $('#PopUpIframe').modal('hide');
}
function SaveGoal(GoalKey, GoalName) {
    $('#FunctionName').text(GoalName);
    $('#hdn_FunctionKey').val(GoalKey);
    $('#PopUpIframe').modal('hide');
}

function ValidateRule() {
    let func = $('#ddl_Functions').val();

    var validate = true;

    if (func == 'Evaluate()' || func == 'Sequelize()') {
        let sql = $('#txt_FunctionDetails').val();

        if (sql != '') {
            validate= true;
        }
        else {
            alertNotify('Commission Rule Error', '* Fields are Manditory. Please describe the rule correctly', 'bg-warning border-warning');
            validate =false;

        }
    } else {

        let funcKey = $('#hdn_FunctionKey').val();

        if (funcKey != '') {
            validate= true;
        }
        else {
            alertNotify('Commission Rule Error', '* Fields are Manditory. Please describe the rule correctly', 'bg-warning border-warning');
            alertNotify('Commission Rule Error', 'Its seems like your not properly add the rule function.', 'bg-warning border-warning');
            validate= false;
        }

       
    }
    checkVariableName();
    var variable = checkVariableName();

    if (variable == false) {
        validate = false;
    }

    return validate;
}

$('button.close').click(function () {
    let funcKey = $('#hdn_FunctionKey').val();

    if (funcKey != '') {
        return true;
    }
    else {
       
        alertNotify('Commission Rule Error', 'Its seems like your not properly add the rule function.', 'bg-warning border-warning');
        $('#ddl_Functions').val('');
        $('#ddl_Functions-error').addClass('d-none');
        return false;
    }

})


function checkVariableName() {
    let variable = $('#txt_VariableName').val();
    let CommissionKey = $('#hdn_CommissionKey').val();
    var valid = true;
    if (variable == '') {
      //  alertNotify('Commission Rule Error', 'Variable name must have value.', 'bg-warning border-warning');
    } else {
        
    }

    if (variable.charAt(0) != '#') {
        alertNotify('Commission Rule Error', 'Variable name must start with "#".', 'bg-warning border-warning');
        $('#txt_VariableName-error').removeClass('validation-valid-label');
        $('#txt_VariableName-error').addClass('validation-invalid-label');
        $('#txt_VariableName-error').text('Invalid Variable Name');
        valid = false;
    } else {

        $.ajax({
            async: false,
            type: "POST",
            url: "../Web/CommissionRule.aspx/validateVariableName",
            data: "{CommissionKey:'" + CommissionKey + "',VariableName:'" + variable + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data.d);
                console.log('ddd');
                if (data.d == '0') {
                    alertNotify('Commission Rule Error', 'Variable already exist.', 'bg-warning border-warning');
                    $('#txt_VariableName-error').removeClass('validation-valid-label');
                    $('#txt_VariableName-error').addClass('validation-invalid-label');
                    $('#txt_VariableName-error').text(' Variable Name Already Exists');
                    valid = false;
                }
                if (data.d == '1') {
                    valid = true;
                }
               
            },
            error: function () {
                alert('Error communicating with server');
            }
        });
    }

    return valid;
}
