﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class MasterPages_CMMaster : System.Web.UI.MasterPage
{
    SessionCustom SC;
    DataAccess DA;
    ApplicationCommonFunction acf;
    PhTemplate PHT;
    Common CM;
    //RMaster RM;
    DataSet ds = null;
    string str_Menustring = "";
    public string str_UserKey = "", str_UserName = "", str_Image = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.SC = new SessionCustom(true);
        this.DA = new DataAccess();
        this.acf = new ApplicationCommonFunction();
        this.PHT = new PhTemplate();
        this.CM = new Common();
        //  a_Home.HRef = this.CM.GetDashboardUrl();
        //  a_LogoURL.HRef = this.CM.GetDashboardUrl();
        //if (this.SC.IsDoctor())
        //{
        //    a_Myprofile.HRef = "../Web/MyProfileDoctor.aspx?DoctorKey=" + this.SC.UserKey;
        //    str_Image = this.SC.FilePath;
        //}
        //else if (this.SC.IsHospitalAdmin())
        //{
        //    a_Myprofile.HRef = "../Web/HospitalView.aspx?HospitalKey=" + this.SC.HospitalKey;
        //    str_Image = this.SC.FilePath;
        //}
        //else if (this.SC.IsHospitalAdmin())
        //    a_Myprofile.HRef = "../Web/PatientView.aspx?PatientKey=" + this.SC.UserKey;
        //else
        //    a_Myprofile.HRef = "#";
        //str_Image= "../Robust/app-assets/images/portrait/small/avatar-s-1.png";
        //str_Image = this.SC.FilePath;
        //str_UserKey = this.SC.UserKey;
        //str_UserName = this.SC.UserName;
        lbl_UserName.InnerText = this.SC.Name;
        lbl_RoleName.InnerText = " ("+this.SC.RoleName+")";
        str_Menustring += this.load_menu_Single();
        ph_UserMenu.Controls.Add(new LiteralControl(str_Menustring));

        loadCompanyInfo(this.SC.UserKey);
    }

    private void loadCompanyInfo(string userKey)
    {
        string str_Sql_view = "select *,cl.LogoImageKey,cl.LogoImageName from CompanySetup a left outer join CompanyLogo cl on a.CompanyKey=cl.Companykey where UserKey =@UserKey";
        SqlCommand sqlcmd = new SqlCommand(str_Sql_view);
        sqlcmd.Parameters.AddWithValue("@UserKey", userKey);
        DataTable dt = this.DA.GetDataTable(sqlcmd);
        foreach (DataRow dr in dt.Rows)
        {


            OneTableDml OTD = new OneTableDml("CompanyLogo");

            if (dr["LogoImageName"].ToString() != "")
                CompanyHeaderLogo.Attributes.Add("src", OTD.FileFolder + dr["LogoImageName"].ToString());
            else
                CompanyHeaderLogo.Attributes.Add("src", "../Limitless_Bootstrap_4/Template/global_assets/images/placeholders/placeholder.jpg");


        }
    }

    public string load_menu_Single()
    {
        //<li class="nav-item"><a href="dashboard.aspx" class="navbar-nav-link "><i class="icon-home4 position-left"></i> Dashboard</a></li>

        string str_SingleLink = "<li class='nav-item'><a href='%%TargetUrl%%' class='navbar-nav-link '><i class='%%MenuIcon%% position-left'></i> %%MenuName%%</a></li>";
        //string str_SingleLink = "<li data-menu='dropdown' class='nav-item'><a href='%%TargetUrl%%' class=' nav-link'><i class='%%MenuIcon%% mr-1'></i> %%MenuName%%</a></li>";
        string str_Menu = "", str_load = "";

        //string str_SelectMenu = "select * from Menu where IsActive=1";
        DataTable dt_menu = new DataTable();
        //dt_menu = DA.GetDataTable(str_SelectMenu);
        dt_menu = (DataTable)HttpContext.Current.Session["Menu"];
        foreach (DataRow dr in dt_menu.Select("ParentMenuId is null", "MenuOrder asc"))
        {
            string str_MenuName = dr["MenuName"].ToString();
            string str_Foldericon = dr["MenuIcon"].ToString();
            string str_MenuKey = dr["MenuKey"].ToString();
            string str_TargetUrl = get_navigate_url(dr);
            DataRow[] dr_sub = dt_menu.Select("isnull(ParentMenuId,'')='" + dr["MenuKey"].ToString() + "'", "MenuOrder asc");
            if (dr_sub.Length == 0)
            {
                str_Menu = str_SingleLink.Replace("%%MenuName%%", str_MenuName);
                str_Menu = str_Menu.Replace("%%TargetUrl%%", str_TargetUrl);
                str_Menu = str_Menu.Replace("%%MenuIcon%%", str_Foldericon);
                str_load += str_Menu;
            }
            str_load += load_child_menu_Second(dr["MenuKey"].ToString(), dr["MenuName"].ToString(), dt_menu, str_Foldericon);
        }
        return str_load;

    }

    public string load_child_menu_Second(string ParentMenuId, string str_Parent_MenuName, DataTable dt_menu, string str_ParentFoldericon)
    {
        string str_MenuHTML = "", str_Menuli = "";
        bool bln_Isactive = false;
      //  str_Parent_MenuName = str_Parent_MenuName.Replace(" ", "_");

        string str_Menu = this.PHT.ReadFileToString("MenuChildHeader.txt");

        string str_SubMenu = this.PHT.ReadFileToString("Menuli.txt");
        string str_SubMenuli = "";
        DataRow[] dr = dt_menu.Select("isnull(ParentMenuId,'')='" + ParentMenuId + "'", "MenuOrder asc");
        if (dr.Length > 0)
        {
            foreach (DataRow dr_child in dt_menu.Select("isnull(ParentMenuID,'')='" + ParentMenuId + "'", "MenuOrder"))
            {
                string str_MenuName = dr_child["MenuName"].ToString();
                string str_MenuKey = dr_child["MenuKey"].ToString();
                string str_FolderIcon = dr_child["MenuIcon"].ToString();
                string str_TargetUrl = get_navigate_url(dr_child);
                string str_Target = "";
                if (dr_child["TargetUrl"].ToString() != "")
                {
                    if (acf.convert_to_boolean(dr_child["IsNewWindow"].ToString()))
                        str_Target = "_blank";
                    else str_Target = "";
                }
                DataRow[] dr_grant_child = dt_menu.Select("isnull(ParentMenuId,'')='" + str_MenuKey + "'", "MenuOrder");
                if (dr_grant_child.Length > 0)
                {
                    str_SubMenuli += this.load_child_menu_Second(str_MenuKey, str_MenuName, dt_menu, str_FolderIcon);
                }
                else
                {
                    str_MenuHTML = str_SubMenu;
                    str_Menu = str_Menu.Replace("%%ParentMenuName%%", str_Parent_MenuName);
                    str_Menu = str_Menu.Replace("%%MenuParentIcon%%", str_ParentFoldericon);
                    str_Menuli = str_Menu;
                    //if (str_MenuKey == this.str_MenuId)
                    //{
                    //    str_MenuHTML = str_MenuHTML.Replace("%isactive%", "active");
                    //    bln_Isactive = true;
                    //}
                    //else
                    //{
                    //    str_MenuHTML = str_MenuHTML.Replace("%isactive%", "");
                    //}

                    str_MenuHTML = str_MenuHTML.Replace("%%MenuName%%", str_MenuName);
                    str_MenuHTML = str_MenuHTML.Replace("%%TargetUrl%%", str_TargetUrl);
                    str_MenuHTML = str_MenuHTML.Replace("%MenuKey%", str_MenuKey);
                    str_MenuHTML = str_MenuHTML.Replace("%%MenuIcon%%", str_FolderIcon);
                    str_MenuHTML = str_MenuHTML.Replace("%%Target%%", str_Target);
                    str_SubMenuli += str_MenuHTML;
                }
                str_SubMenuli = str_SubMenuli.Replace("%%MenuParentIcon%%", str_ParentFoldericon);
            }
            if (bln_Isactive == true)
            {
                str_Menuli = str_Menuli.Replace("%%isactive%%", "active");
                str_Menuli = str_Menuli.Replace("%%collapsein%%", "in");
            }
            else
            {
                str_Menuli = str_Menuli.Replace("%%isactive%%", "");
                str_Menuli = str_Menuli.Replace("%%collapsein%%", "");
            }
            return str_Menuli.Replace("%%li_Content%%", str_SubMenuli);
        }
        else
        {
            return "";
        }
    }

    protected string get_navigate_url(DataRow dr_menu)
    {
        string str_navigate_url = this.SC.BaseUrl();
        //string str_navigate_url = "http://localhost:52790";
        if (dr_menu["TargetUrl"].ToString() != "")
        {
            string str_URL = dr_menu["TargetUrl"].ToString();
            string str_MenuId = dr_menu["MenuKey"].ToString();
            if (str_URL.Contains("?"))
                str_URL = str_URL + "&MenuId=" + str_MenuId + "";
            else
                str_URL = str_URL + "?MenuId=" + str_MenuId + "";

            str_navigate_url += str_URL;
            str_navigate_url = acf.replace_variable_with_value(str_navigate_url);
        }
        return str_navigate_url;

    }
}
