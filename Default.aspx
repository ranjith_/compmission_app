﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Raka HRMS</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="../Bootstrap_LIMITLESS/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../Bootstrap_LIMITLESS/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../Bootstrap_LIMITLESS/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../Bootstrap_LIMITLESS/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../Bootstrap_LIMITLESS/assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/core/app.js"></script>
    <%--<script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/pages/login_validation.js"></script>--%>

    <script type="text/javascript" src="../Bootstrap_LIMITLESS/assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /theme JS files -->



   
    <script>

        function validate() {
            var msg = "";
            var username = $("#txt_UserName").val();
            var password = $("#txt_Password").val();
            if (username == "") {
                msg = "Username required";
            }
            else if (password == "") {
                msg = "Password required";
            }
            if (msg != "") {
                $("#warn_Message").html(msg);
                return false;
            }
            else
                return true;
        }

        $(document).ready(function () {
            alert();
        });

    </script>
    <style>
        body {
            background-image: url("Images/loginbg.jpg")
        }
    </style>
</head>
<body class="login-container login-cover">
    <form id="form1" runat="server" class="form-validate">
        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content pb-20">

                        <!-- Form with validation -->
                        
                            <div class="panel panel-body login-form">
                                <div class="text-center">
                                    <img src="../Images/rakatech-logo.png" style="width:100%"/>
                                    <%--<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>--%>
                                    <h5 class="content-group">Login to your account <small class="display-block">Your credentials</small></h5>
                                    <%--<label id="lbl_DisplayMessage" runat="server" style="color:red"><b></b></label>--%>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <%--<input type="text" id="txt_UserName" runat="server" class="form-control" placeholder="Username" name="username" required="required">--%>
                                    <asp:TextBox ID="txt_UserName"  runat="server" type="text" name="username" class="form-control" placeholder="User Name"></asp:TextBox>
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <%--<input type="password" id="txt_Password" runat="server" class="form-control" placeholder="Password" name="password" required="required">--%>
                                    <asp:TextBox ID="txt_Password" runat="server" TextMode="Password"  class="form-control" placeholder="Password"></asp:TextBox>
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>
                                
                                <div class="form-group has-feedback has-feedback-left">
                                    <span id="sp_InvalidCredentials" runat="server" class="text-danger" style="display:none;"></span>
                                </div>

                                <div class="form-group">                                    
                                    <asp:Button ID="btn_Login" class="btn btn-outline-primary btn-glow btn-block" runat="server" Text="Login" OnClick="btn_Login_Click" OnClientClick="return validate()" />
                                    <%--<asp:LinkButton ID="btn_CommonLogin" class="btn btn-primary btn-block" OnClientClick="btnClick();" accesskey=" " runat="server" OnClick="Unnamed_Click">Sign in <i class="icon-circle-right2 position-right"></i></asp:LinkButton>--%>
                                </div>
                            </div>
                        
                        <!-- /form with validation -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
    </form>
    </body>
</html>
