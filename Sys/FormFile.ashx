﻿<%@ WebHandler Language="C#" Class="FormFile" %>

using System;
using System.Web;
using System.Data;
using System.Collections.Generic;
using System.Xml;

public class FormFile : IHttpHandler
{

    DataAccess DA;

    System.Collections.Specialized.NameValueCollection NVC;
    DataSet ds_form_tables = null;
    HttpContext context;
    string str_RootUrl = "";
    string str_FileUploadLocation = "";

    public void ProcessRequest(HttpContext context)
    {
        this.context = context;
        context.Response.ContentType = "text/plain";

        string str_ImageKey = "", str_TableName = "";

        if (context.Request.QueryString["ImageKey"] != null && context.Request.QueryString["ImageKey"] != "")
            str_ImageKey = context.Request.QueryString["ImageKey"];

        if (context.Request.QueryString["TableName"] != null && context.Request.QueryString["TableName"] != "")
            str_TableName = context.Request.QueryString["TableName"];

        //if (str_ImageKey == "")
        //    str_ImageKey = Guid.NewGuid().ToString();

        this.NVC = new System.Collections.Specialized.NameValueCollection();
        this.DA = new DataAccess();

        str_RootUrl = context.Request.Url.AbsoluteUri;
        str_RootUrl = str_RootUrl.Replace(context.Request.Url.Query, "");
        str_RootUrl = str_RootUrl.ToLower().Replace("sys/formfile.ashx", "");

        OneTableDml OTD = new OneTableDml(str_TableName);

        //string str_TargetFilePath = HttpContext.Current.Server.MapPath("~/UploadFile/" + str_FolderName);
        string str_TargetFilePath = HttpContext.Current.Server.MapPath("~" + OTD.FileFolder);
        if (!System.IO.Directory.Exists(str_TargetFilePath)) System.IO.Directory.CreateDirectory(str_TargetFilePath);

        string str_ClientFileWithPath = "";
        string str_ClientFileWithPath_thumb = "";
        string str_ServerFileName = "";

        foreach (string str_File in context.Request.Files)
        {
            HttpPostedFile clientfile = context.Request.Files[str_File];
            string str_FileExt = System.IO.Path.GetExtension(clientfile.FileName);
            if (!string.IsNullOrEmpty(clientfile.FileName))
            {
                str_ServerFileName = str_ImageKey + "_" + clientfile.FileName;
                //if (str_FolderName != "")
                //    str_ServerFileName = str_ImageKey + "_" + clientfile.FileName;
                //else
                //    str_ServerFileName = str_ImageKey + "_" + clientfile.FileName;

                str_ClientFileWithPath = str_TargetFilePath + @"\" + str_ServerFileName;
                if (str_FileExt.ToLower() == ".jpeg" || str_FileExt.ToLower() == ".jpg" || str_FileExt.ToLower() == ".png" || str_FileExt.ToLower() == ".gif")
                {
                    str_ClientFileWithPath = str_TargetFilePath + @"\" + str_ServerFileName;
                    str_ClientFileWithPath_thumb = str_TargetFilePath + @"\" + @"thumb_" + str_ServerFileName;
                    if (!System.IO.Directory.Exists(str_TargetFilePath)) System.IO.Directory.CreateDirectory(str_TargetFilePath);
                    clientfile.SaveAs(str_ClientFileWithPath);

                    //create thumb image
                    System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(str_ClientFileWithPath);
                    System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height);
                    System.Drawing.Bitmap cloned = (System.Drawing.Bitmap)bmp.Clone(); //(rect, bmp.PixelFormat);
                    System.Drawing.Bitmap bmp_thump = new System.Drawing.Bitmap(cloned, new System.Drawing.Size(200, 150));
                    bmp_thump.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipNone);
                    bmp_thump.Save(str_ClientFileWithPath_thumb);

                }
                else if (str_FileExt.ToLower() == ".doc" || str_FileExt.ToLower() == ".docx" || str_FileExt.ToLower() == ".xls" || str_FileExt.ToLower() == ".xlsx" || str_FileExt.ToLower() == ".pdf" || str_FileExt.ToLower() == ".txt")
                {
                    if (!System.IO.Directory.Exists(str_TargetFilePath)) System.IO.Directory.CreateDirectory(str_TargetFilePath);
                    clientfile.SaveAs(str_ClientFileWithPath);
                }
                else
                {
                    if (!System.IO.Directory.Exists(str_TargetFilePath)) System.IO.Directory.CreateDirectory(str_TargetFilePath);
                    clientfile.SaveAs(str_ClientFileWithPath);
                }
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write(str_ImageKey);

    }



    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}