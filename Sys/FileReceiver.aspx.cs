﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FileReceiver : System.Web.UI.Page
{
    DataAccess DA;
    PhTemplate PHT;
    bool bln_AddMode = true;

    string str_TableName = "";
    string str_TableContent = "";
    string str_KeyValue = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        int loop1;
        HttpFileCollection Files;

        Files = Request.Files; // Load File collection into HttpFileCollection variable.
        string [] arr1 = Files.AllKeys;  // This will get names of all files into a string array.
        for (loop1 = 0; loop1 < arr1.Length; loop1++)
        {
            Response.Write("File: " + Server.HtmlEncode(arr1[loop1]) + "<br />");
            Response.Write("  size = " + Files[loop1].ContentLength + "<br />");
            Response.Write("  content type = " + Files[loop1].ContentType + "<br />");

            try
            {
                // Create a new file name.
                string TempFileName = "C:\\Temp\\File_" + Guid.NewGuid().ToString() + loop1.ToString() + ".jpg";
                // Save the file.
                Files[loop1].SaveAs(TempFileName);
            }
            catch (Exception ex)
            { 
            }

        }
    }

    [WebMethod]
    public static string putDivContent(string str_TableName, string str_KeyValue, string str_ControlValue)
    {
        return "";
    }
}