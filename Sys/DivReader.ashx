﻿<%@ WebHandler Language="C#" Class="DivReader"
    WarningLevel="0" %>

using System;
using System.Web;
using System.Web.Services;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;


public class DivReader : IHttpHandler
{

    DataAccess DA;
    PhTemplate PHT;

    bool bln_AddMode = true;

    string str_TableName = "";
    string str_KeyValue = "";
    string str_CallType = "";
    string str_P1 = "", str_P2 = "", str_P3 = "";

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "html/plain";
        HttpRequest Request = context.Request;

        if (Request.QueryString["type"] == null || Request.QueryString["type"].ToString() == "")
        {
            return;
        }
        this.str_CallType = Request.QueryString["type"];

        this.DA = new DataAccess();
        this.PHT = new PhTemplate();
        if (Request.QueryString["tname"] == null || Request.QueryString["tname"].ToString() == "")
        {
            return;
        }
        str_TableName = Request.QueryString["tname"].ToString();
        if (Request.QueryString["P1"] != null && Request.QueryString["P1"].ToString() != "")
        {
            this.str_P1 = Request.QueryString["P1"];
        }
        if (Request.QueryString["P2"] != null && Request.QueryString["P2"].ToString() != "")
        {
            this.str_P2 = Request.QueryString["P2"];
        }
        if (Request.QueryString["P3"] != null && Request.QueryString["P3"].ToString() != "")
        {
            this.str_P3 = Request.QueryString["P3"];
        }

        if (Request.QueryString["key"] != null && Request.QueryString["key"].ToString() != "")
        {
            str_KeyValue = Request.QueryString["key"];
            bln_AddMode = false;
        }

        OneTableDml OTD = new OneTableDml(str_TableName);

        if (this.str_CallType == "get")
            context.Response.Write(this.getDivContent(OTD));


    }


    protected string getDivContent(OneTableDml OTD)
    {
        try
        {
            string str_TemplateContent = "", str_HTML = "";
            str_TemplateContent = this.PHT.ReadFileToString(OTD.DivTemplateFileName);

            string str_Sql = "select * from " + str_TableName + " where " + OTD.PrimaryKeyColumnName + "=@" + OTD.PrimaryKeyColumnName;
            if (this.bln_AddMode == true)
                str_Sql = "select * from " + str_TableName + " where 1=2";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@" + OTD.PrimaryKeyColumnName, str_KeyValue);
            DataTable dt = this.DA.GetDataTable(cmd);
            if (bln_AddMode)
            {
                DataRow dr_New = dt.NewRow();
                dr_New[OTD.PrimaryKeyColumnName] = Guid.NewGuid().ToString();
                dt.Rows.Add(dr_New);
            }

            //work for static variables p1,p2
            if (this.str_P1 != "" && this.str_P1 != "undefined")
                str_TemplateContent = str_TemplateContent.Replace("%%" + str_P1.Split(':')[0] + "%%", str_P1.Split(':')[1]);
            if (this.str_P2 != "" && this.str_P2 != "undefined")
                str_TemplateContent = str_TemplateContent.Replace("%%" + str_P2.Split(':')[0] + "%%", str_P2.Split(':')[1]);
            if (this.str_P3 != "" && this.str_P3 != "undefined")
                str_TemplateContent = str_TemplateContent.Replace("%%" + str_P3.Split(':')[0] + "%%", str_P3.Split(':')[1]);

            //work for checkbox
            str_TemplateContent = this.ProcessCheckBox(OTD, str_TemplateContent, dt);

            //work for radiobox
            str_TemplateContent = this.ProcessRadioBox(OTD, str_TemplateContent, dt);

            //work for dropdown
            str_TemplateContent = this.ProcessDropBox(OTD, str_TemplateContent, dt);

            //add filepath to datatbale
            foreach (string str_FileColumn in OTD.FileColumns)
            {
                if (!dt.Columns.Contains(str_FileColumn)) return "File Column: " + str_FileColumn + " not found in OneTableDml:" + OTD.TableName;
                dt.Columns.Add(str_FileColumn + "_path");
                dt.Columns.Add(str_FileColumn + "_name");
            }

            //label data
            string str_FileRoot = new AppVar().WebRoot + OTD.FileFolder;
            string str_FileNameOriginal = "";
            foreach (DataRow dr in dt.Rows)
            {
                str_FileNameOriginal = "";
                //file data link update to datatable
                foreach (string str_FileColumn in OTD.FileColumns)
                {
                    dr[str_FileColumn + "_path"] = str_FileRoot + dr[str_FileColumn].ToString();
                    if (dr[str_FileColumn].ToString().Length > 37) str_FileNameOriginal = dr[str_FileColumn].ToString().Substring(37);
                    dr[str_FileColumn + "_name"] = str_FileNameOriginal;
                }

                str_HTML += this.PHT.ReplaceVariableWithValue(dr, str_TemplateContent);
            }
            return str_HTML;
        }
        catch (Exception ex)
        {
            return "Exception: " + ex.Message;
        }
    }


    protected string ProcessCheckBox(OneTableDml OTD, string str_Html, DataTable dt_Data)
    {
        if (OTD.CheckBox == null) return str_Html;

        foreach (string str in OTD.CheckBox)
        {
            if (dt_Data.Columns[str].ColumnName.ToLower() == str.ToLower())
            {
                if (dt_Data.Rows[0][str].ToString() == "1" || dt_Data.Rows[0][str].ToString().ToLower() == "true")
                    str_Html = str_Html.Replace("%%" + dt_Data.Columns[str].ColumnName + "%%", "checked");
                else
                    str_Html = str_Html.Replace("%%" + dt_Data.Columns[str].ColumnName + "%%", "");
            }
        }

        return str_Html;
    }

    protected string ProcessRadioBox(OneTableDml OTD, string str_Html, DataTable dt_Data)
    {
        if (OTD.RadioBox == null) return str_Html;

        string str_DropOptions = "";
        DataTable dt = new DataTable();

        foreach (DataRow dr_RadioBox in OTD.RadioBox.Rows)
        {

            dt = this.DA.GetDataTable(dr_RadioBox["sql"].ToString());
            str_DropOptions = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dt_Data.Rows[0][dr_RadioBox["id"].ToString()].ToString() == dr["value"].ToString())
                        str_DropOptions += OTD.RadioBoxOptionTemplate.Replace("%%value%%", dr["value"].ToString()).Replace("%%text%%", dr["text"].ToString()).Replace("%%checked%%", "checked").Replace("%%radiogroupname%%", "txt_" + dr_RadioBox["id"].ToString());
                    else
                        str_DropOptions += OTD.RadioBoxOptionTemplate.Replace("%%value%%", dr["value"].ToString()).Replace("%%text%%", dr["text"].ToString()).Replace("%%checked%%", "").Replace("%%radiogroupname%%", "txt_" + dr_RadioBox["id"].ToString());
                }
            }

            str_Html = str_Html.Replace("%%" + dr_RadioBox["id"].ToString() + "%%", str_DropOptions);

        }
        return str_Html;
    }



    protected string ProcessDropBox(OneTableDml OTD, string str_Html, DataTable dt_Data)
    {
        if (OTD.DropBox == null) return str_Html;

        DataTable dt = new DataTable();

        foreach (DataRow dr_DropBox in OTD.DropBox.Rows)
        {
            string str_Sql = dr_DropBox["sql"].ToString();
            //work for static variables p1,p2
            if (this.str_P1 != "" && this.str_P1 != "undefined")
                str_Sql = str_Sql.Replace("%%" + str_P1.Split(':')[0] + "%%", str_P1.Split(':')[1]);
            if (this.str_P2 != "" && this.str_P2 != "undefined")
                str_Sql = str_Sql.Replace("%%" + str_P2.Split(':')[0] + "%%", str_P2.Split(':')[1]);
            if (this.str_P3 != "" && this.str_P3 != "undefined")
                str_Sql = str_Sql.Replace("%%" + str_P3.Split(':')[0] + "%%", str_P3.Split(':')[1]);

            dt = this.DA.GetDataTable(str_Sql);
            string str_DropOptions = "";
            str_DropOptions = "<option value=''></option>";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dt_Data.Rows[0][dr_DropBox["id"].ToString()].ToString() == dr["value"].ToString())
                        str_DropOptions += "<option selected value='" + dr["value"].ToString() + "'>" + dr["text"].ToString() + "</option>";
                    else
                        str_DropOptions += "<option value='" + dr["value"].ToString() + "'>" + dr["text"].ToString() + "</option>";
                }
            }

            str_Html = str_Html.Replace("%%" + dr_DropBox["id"].ToString() + "%%", str_DropOptions);

        }
        return str_Html;
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}