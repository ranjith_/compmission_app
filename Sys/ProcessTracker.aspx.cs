﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Sys_ProcessTracker : System.Web.UI.Page
{
    DataAccess DA;
    PhTemplate PHT;
    bool bln_AddMode = true;

    string str_TableName = "";
    string str_TableContent = "";
    string str_KeyValue = "";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string StartTracker(string str_TrackType)
    {
        try
        {
            SqlCommand Sc = new SqlCommand("insert into TrackerCurrent (EmployeeKey,TrackerType,StartTime) values (@EmployeeKey,@TrackerType,GetDate())");
            Sc.Parameters.AddWithValue("@EmployeeKey", new SessionCustom().GetUserKey());
            Sc.Parameters.AddWithValue("@TrackerType", str_TrackType);
            new DataAccess().ExecuteNonQuery(Sc);
            return "Tracker Started";
        }
        catch (Exception exe)
        {
            return exe.ToString();
        }
    }


    [WebMethod]
    public static string ResumeWork()
    {
        try
        {
            string str_Sql = "insert into Tracker (TrackerKey,EmployeeKey,TrackerType,StartTime,EndTime) "
                + " select NewId(),EmployeeKey,TrackerType,StartTime,GetDate() from TrackerCurrent where EmployeeKey=@EmployeeKey; "
                + " Delete From TrackerCurrent where EmployeeKey = @EmployeeKey";
            SqlCommand Sc = new SqlCommand(str_Sql);
            Sc.Parameters.AddWithValue("@EmployeeKey", new SessionCustom().GetUserKey());
            new DataAccess().ExecuteNonQuery(Sc);
            return "Success";
        }
        catch (Exception exe)
        {
            return exe.ToString();
        }
    }
}