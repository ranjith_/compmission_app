﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Sys_DivReceiver : System.Web.UI.Page
{
    DataAccess DA;
    PhTemplate PHT;
    bool bln_AddMode = true;

    string str_TableName = "";
    string str_TableContent = "";
    string str_KeyValue = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.DA = new DataAccess();
        this.PHT = new PhTemplate();
        if (Request.QueryString["tname"] == null || Request.QueryString["tname"].ToString() == "")
        {
            return;
        }
        str_TableName = Request.QueryString["tname"].ToString();

        if (Request.QueryString["key"] != null && Request.QueryString["key"].ToString() != "")
        {
            str_KeyValue = Request.QueryString["key"];
            bln_AddMode = false;
        }

        OneTableDml OTD = new OneTableDml(str_TableName);

    }

    [WebMethod]
    public static string putDivContent(string str_TableName, string str_KeyValue, string str_ControlValue)
    {
        if (str_KeyValue.ToLower() == "undefined") str_KeyValue = "";
        try
        {
            OneTableDml OTD = new OneTableDml(str_TableName);
            OTD.KeyValue = str_KeyValue;
            OTD.PostData = str_ControlValue;
            new DataAccessWeb("15702C1E-1447-4C08-9773-7BD324256180").WebSave(OTD);
        }
        catch (Exception ex)
        {
            return "Exception: " + ex.Message;
        }
        return "Successfully Saved";
    }

    [WebMethod]
    public static string deleteContent(string str_TableName, string str_KeyValue, string str_ControlValue)
    {
        if (str_KeyValue.ToLower() == "undefined") str_KeyValue = "";
        try
        {
            OneTableDml OTD = new OneTableDml(str_TableName);
            if (OTD.AllowDelete == false)
                return "Cannot be deleted, contact Admin";
            OTD.KeyValue = str_KeyValue;
            OTD.PostData = str_ControlValue;
            new DataAccessWeb("4A177355-2F74-4CC0-96AE-308740457077").WebDelete(OTD);
        }
        catch (Exception ex)
        {
            return "Exception: " + ex.Message;
        }
        return "Successfully Deleted";
    }

    [WebMethod]
    public static string GetFileName(string ImageKey)
    {
        string str_ImagePath = "";
        try
        {
            string str_Sql = "select * from Image where ImageKey=@ImageKey";
            SqlCommand cmd = new SqlCommand(str_Sql);
            cmd.Parameters.AddWithValue("@ImageKey", ImageKey);
            DataTable dt = new DataAccess().GetDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                str_ImagePath = dt.Rows[0]["ImagePath"].ToString();
            }
        }
        catch (Exception ex)
        {
            str_ImagePath = ex.Message.ToString();
        }
        return str_ImagePath;
    }

    
}